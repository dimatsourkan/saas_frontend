import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SwUpdate } from '@angular/service-worker';
import { TranslateService } from '@ngx-translate/core';
import { HeaderService } from './core/components/header/header.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  updateAvailable$: any;

  constructor(
    private headerService: HeaderService,
    private updates: SwUpdate,
    private translate: TranslateService,
    private titleService: Title,
  ) {

  }

  ngOnInit() {
    if (this.updates.isEnabled) {
      this.updates.available.subscribe(() => {
        if (confirm('New version available. Load New Version?')) {
          window.location.reload();
        }
      });
    }
    this.getFavicon();
    this.titleListener();
  }

  titleListener() {
    this.headerService.pageName$.subscribe((name) => {
      if (name) {
        this.translate.get(name).subscribe(title => {
          this.titleService.setTitle(title);
        });
      }
    });
  }

  getFavicon() {
    this.headerService.getFavicon();
    this.headerService.faviconUrl.subscribe(response => {
      this.setFavicon(response);
    }, () => {
      this.setFavicon('assets/images/logo.png');
    });
  }

  setFavicon(img) {
    const linkElement = document.createElement('link');
    linkElement.setAttribute('rel', 'icon');
    linkElement.setAttribute('type', 'image/x-icon');
    linkElement.setAttribute('href', img);
    const links = document.getElementsByTagName('link');
    if (links.length > 0) {
      links[0].remove();
    }
    document.head.appendChild(linkElement);
  }
}
