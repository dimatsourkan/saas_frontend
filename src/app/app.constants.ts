export const REGEXPS = {
  NUMERIC: /^\d+$/,
  PHONE: /^\+?[0-9]{8,20}$/i,
  LETTERS: /^[a-zа-яё]*$/i,
  NUMERIC_FLOAT: /^[0-9]*[.]?[0-9]*?$/,
  NUMERIC_OR_LETTERS: /^[0-9a-zA-Z]+$/
};
