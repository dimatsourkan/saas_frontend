import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';

import { NgSelectModule } from '@ng-select/ng-select';
import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';

import { APP_ROUTES } from './app.routing';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
// import { TrackJS } from 'trackjs';

// TrackJS.install({
//   token: 'f9a2e24ba8914bce9594b6b88948731f',
//   application: 'saas'
//   // for more configuration options, see https://docs.trackjs.com
// });

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  wheelSpeed: 0.5,
  wheelPropagation: false,
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgSelectModule,
    PerfectScrollbarModule,
    SharedModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(APP_ROUTES, {preloadingStrategy: PreloadAllModules}),
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.serviceWorker})
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    // {
    //   provide: ErrorHandler,
    //   useClass: TrackJsErrorHandler
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}

