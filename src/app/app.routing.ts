import { Routes } from '@angular/router';
import { WrapperComponent } from './core/components/wrapper/wrapper.component';
import { ROLES } from './core/enums/role.enum';
import { FromRoles } from './core/guards/from-role/from-role.service';
import { IsAuthenticated } from './core/guards/is-authenticated/is-authenticated.service';
import { NotAuthenticated } from './core/guards/not-authenticated/not-authenticated.service';

export const APP_ROUTES: Routes = [
  {
    path: '',
    component: WrapperComponent,
    canActivate: [IsAuthenticated],
    children: [
      {
        path: '',
        redirectTo: 'manager',
        pathMatch: 'full'
      },
      {
        path: 'manager',
        canLoad: [IsAuthenticated, FromRoles],
        data: {roles: [ROLES.MANAGER, ROLES.ADMIN]},
        loadChildren: './pages/manager/manager.module#ManagerModule'
      },
      {
        path: 'publisher',
        canLoad: [IsAuthenticated, FromRoles],
        data: {roles: [ROLES.PUBLISHER]},
        loadChildren: './pages/publisher/publisher.module#PublisherModule'
      },
      {
        path: 'test-html',
        loadChildren: './pages/new-html/new-html.module#NewHtmlModule'
      },
      {
        path: 'ui',
        loadChildren: './pages/ui/ui.module#UiModule'
      },
    ]
  },
  {
    path: '',
    canLoad: [NotAuthenticated],
    loadChildren: './pages/auth/auth.module#AuthModule'
  },
  {
    path: '**',
    redirectTo: '/login'
  }
];
