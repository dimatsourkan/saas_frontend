import { AfterContentInit, Component, ElementRef } from '@angular/core';

@Component({
  selector: 'app-check-adblock',
  templateUrl: './check-adblock.component.html',
  styleUrls: ['./check-adblock.component.scss']
})
export class CheckAdblockComponent implements AfterContentInit {

  show: boolean;

  constructor(private _elementRef: ElementRef) {
  }

  ngAfterContentInit() {
    const domElement = this._elementRef.nativeElement.querySelector(`#wrapfabtest`);
    if (domElement.clientHeight <= 0) {
      this.show = true;
    }
  }

}
