import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { SidebarService } from '@app/core/components/sidebar/sidebar.service';
import { DataStore } from '@app/core/entities/data-store.service';
import { modelId } from '@app/core/models/base-model/base.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { LOCALES } from '@app/shared/constants/locales.const';
import { LanguageService } from '@app/shared/services/language/language.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/internal/operators';
import { AuthService } from '../../services/auth/auth.service';
import { HeaderService } from './header.service';
import { CurrencyService } from '@app/core/entities/currency/currency.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output()
  sideBarMinChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  userControlPanel = false;
  locales: any[] = LOCALES;
  loading: boolean;
  success: boolean;
  error: any;
  selectedLocale = this.languageService.getLang().getValue();
  currency$ = DataStore.currency.current.asObservable$;
  pageName$ = this.headerService.pageName$;
  profilePhoto$ = this.headerService.profilePhoto;
  userName$ = DataStore.token.item.asObservable$.pipe(map(token => token.userName));
  logoUrl$ = this.headerService.logoUrl.pipe(map(res => {
    return this.sanitization.bypassSecurityTrustStyle(`url(${res})`);
  }));

  constructor(
    public auth: AuthService,
    public apiUrlService: ApiUrlService,
    private router: Router,
    private currencyService: CurrencyService,
    private translate: TranslateService,
    private languageService: LanguageService,
    private headerService: HeaderService,
    private sanitization: DomSanitizer,
    private sidebarService: SidebarService,
    private swal: SweetAlertService) {
  }

  ngOnInit() {
    this.headerService.getProfilePhoto();
    this.headerService.getLogo();
  }

  goToProfile() {
    this.router.navigate(['/', this.apiUrlService.linkPathFromRole(), 'profile']);
  }

  logout() {
    this.auth.logout(DataStore.token.item.getValue().role);
  }

  toggleUserControlPanel() {
    this.userControlPanel = !this.userControlPanel;
  }

  hideUserControlPanel() {
    this.userControlPanel = false;
  }

  changeLocale() {
    this.languageService.setLang(this.selectedLocale);
  }

  changeCurrency(currencyId: modelId) {
    this.currencyService.setCurrency(currencyId);
    this.translate.get(['header.disclaimerTitle', 'header.currencyDisclaimer']).subscribe((res) => {
      this.swal.info(res['header.disclaimerTitle'], res['header.currencyDisclaimer']).then();
    });
  }

  get roleName() {
    return this.apiUrlService.linkPathFromRole();
  }

  get isManager(): boolean {
    return this.auth.isManager;
  }

  changeSideBarMin() {
    if (this.sidebarService.isSmall) {
      return this.sidebarService.setBig();
    } else {
      return this.sidebarService.setSmall();
    }
  }
}
