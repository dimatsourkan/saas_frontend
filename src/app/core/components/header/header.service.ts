import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { BaseUrlService } from '@app/shared/services/base-url/base-url.service';
import { ReplaySubject } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  public pageName$ = new ReplaySubject<string>(null);
  logoUrl = new ReplaySubject<string>();
  faviconUrl = new ReplaySubject<string>();
  profilePhoto = new ReplaySubject<string>();

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private url: BaseUrlService
  ) {
    this.titleListener();
  }

  private titleListener() {
    this.router.events.pipe(
      filter((event: any) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((currentRoute) => {
        while (currentRoute.firstChild) {
          currentRoute = currentRoute.firstChild;
        }
        return currentRoute;
      }),
      filter((filterRoute) => filterRoute.outlet === 'primary'),
      mergeMap((mergeRoute) => mergeRoute.data)
    ).subscribe((event: any) => {
      this.pageName$.next(event.name);
    });

    let route: any = this.activatedRoute.snapshot;
    while (route.children.length) {
      route = route.children[0];
    }

    this.pageName$.next(route.data.name);
  }

  sendRequest(info) {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/support/send_mail`, info, {headers: {toCamelCase: 'true'}});
  }

  getLogo() {
    return this.http.get(`${this.url.getBaseUrl()}/logo`)
    .subscribe((res: any) => {
      this.logoUrl.next(res);
    }, () => {
      this.logoUrl.next('assets/images/logo.png');
    });
  }

  getProfilePhoto() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/managers/profile-thumbnail`)
    .subscribe((res: any) => {
      this.profilePhoto.next(res);
    }, () => {
      this.profilePhoto.next('//placehold.it/60x60');
    });
  }

  getFavicon() {
    return this.http.get(`${this.url.getBaseUrl()}/favicon`)
    .subscribe((res: any) => {
      this.faviconUrl.next(res);
    }, () => {
      this.faviconUrl.next('assets/images/logo.png');
    });
  }

}
