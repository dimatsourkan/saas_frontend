import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';
import { ManagerInterfaces, SidebarTotal } from './manager.interfaces';
import { ManagerSidebarService } from './manager.service';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'app-manager-sidebar',
  templateUrl: './manager.component.html',
  styleUrls: [
    '../sidebar.component.scss',
    './manager.component.scss'
  ],
  encapsulation: ViewEncapsulation.None,
  providers: [ManagerSidebarService]
})
export class ManagerSidebarComponent implements OnInit, OnDestroy {
  @Input() isSmall: boolean;
  destroy$: Subject<boolean> = new Subject<boolean>();
  time: string;
  sidebarInfo: ManagerInterfaces;
  sidebarTotal: SidebarTotal;
  sidebarLimitUsage: any = {};
  approvalsCount: number;
  public now: Date = new Date();

  constructor (private sidebar: ManagerSidebarService) {}


  ngOnInit() {
    this.sidebar.getSidebarInfo()
      .subscribe((response: ManagerInterfaces) => {
        this.sidebarInfo = response;
      });
    this.sidebar.getLimitUsage().subscribe(res => {
      this.sidebarLimitUsage = res;
    });
    this.sidebar.getApprovalsCount()
      .subscribe((response) => {
        this.approvalsCount = response;
      });
    DataStore.currency.current.asObservable$
      .pipe(takeUntil(this.destroy$))
      .subscribe((currency) => {
        this.sidebar.getTodayTotal(currency)
          .subscribe((response: SidebarTotal) => {
            this.sidebarTotal = response;
          });
      });

    setInterval(() => {
      this.now = new Date();
    }, 30);
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
