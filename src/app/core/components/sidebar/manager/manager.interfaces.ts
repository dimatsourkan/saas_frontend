export interface ManagerInterfaces {
  advertiser: AdvertiserSidebarInfo;
  publisher: PublisherSidebarInfo;
  offer: OfferSidebarInfo;
  timezone: '';
}

export interface SidebarTotal {
  amount: 0;
  currency: '';
}

interface AdvertiserSidebarInfo {
  countActiveAdvertiser: 0;
  percentNewAdvertisersForToday: 0;
}

interface PublisherSidebarInfo {
  countActivePublishers: 0;
  countNewPublishers: 0;
  percentNewPublishersForToday: 0;
}

interface OfferSidebarInfo {
  countActiveOffers: 0;
  countFeaturedOffers: 0;
  percentNewOffersForToday: 0;
  percentPrivateOffersForToday: 0;
}
