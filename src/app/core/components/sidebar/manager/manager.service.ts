import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseUrlService } from '@app/shared/services/base-url/base-url.service';
import { QueryParamsService } from '@app/shared/services/queryParams/query-params.service';
import { Observable } from 'rxjs';
import { ManagerInterfaces, SidebarTotal } from './manager.interfaces';

@Injectable()
export class ManagerSidebarService {
  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getSidebarInfo(): Observable<ManagerInterfaces> {
    return this.http.get<ManagerInterfaces>(`${this.url.getBaseUrlWithRole()}/get_sidebar`, {headers: {toCamelCase: 'true'}});
  }

  getLimitUsage(): Observable<ManagerInterfaces> {
    return this.http.get<ManagerInterfaces>(`${this.url.getBaseUrlWithRole()}/sidebar/limits_usage`, {headers: {toCamelCase: 'true'}});
  }

  getApprovalsCount(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_approval/pending_count`, {headers: {toCamelCase: 'true'}});
  }

  getTodayTotal(currency): Observable<SidebarTotal> {
    const params = this.queryParamsService.paramsObjectToString({currency});
    return this.http.get<SidebarTotal>(`${this.url.getBaseUrlWithRole()}/sidebar/today_total${params}`, {headers: {toCamelCase: 'true'}});
  }
}
