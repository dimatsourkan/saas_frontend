import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseUrlService } from '@app/shared/services/base-url/base-url.service';
import { QueryParamsService } from '@app/shared/services/queryParams/query-params.service';
import { Observable } from 'rxjs';


@Injectable()
export class PublisherSidebarService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getSidebarInfo(currency): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString({currency});
    return this.http.get(`${this.url.getBaseUrlWithRole()}/sidebar/info${params}`, {headers: {toCamelCase: 'true'}});
  }
}
