import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';
import { AuthService } from '../../../services/auth/auth.service';
import { PublisherSidebarService } from './publisher-sidebar.service';
import { DataStore } from '@app/core/entities/data-store.service';


@Component({
  selector: 'app-publisher-sidebar',
  templateUrl: './publisher.component.html',
  styleUrls: [
    '../sidebar.component.scss',
    './publisher.component.scss'
  ],
  encapsulation: ViewEncapsulation.None,
  providers: [PublisherSidebarService]
})
export class PublisherSidebarComponent implements OnInit, OnDestroy {

  @Input() isSmall: boolean;
  destroy$: Subject<boolean> = new Subject<boolean>();
  time: string;
  sidebar: any = {
    manager: {},
    amount: 0,
    today: 0,
    currency: 0,
    summary: {}
  };
  currency: string;
  now: Date;

  constructor(private _sidebarService: PublisherSidebarService,
              private _authService: AuthService,
              private apiUrlService: ApiUrlService) {
  }

  ngOnInit() {
    DataStore.currency.current.asObservable$
    .pipe(takeUntil(this.destroy$))
    .subscribe((currency) => {
      this.currency = currency;
      this.getSidebarInfo(currency);
    });
    setInterval(() => {
      this.now = new Date();
    }, 30);
  }

  getSidebarInfo(currency) {
    this._sidebarService.getSidebarInfo(currency)
    .subscribe((response: any) => {
      this.sidebar = response;
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  logout() {
    this._authService.logout(this.apiUrlService.linkPathFromRole());
  }

}
