import { AfterViewInit, Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sidebarMenu]'
})
export class SidebarMenuDirective implements AfterViewInit {

  element: HTMLElement;

  constructor(elem: ElementRef) {
    this.element = elem.nativeElement;
  }

  ngAfterViewInit(): void {
    this.element.querySelectorAll('.nav-parent > span').forEach((item) => {
      item.addEventListener('click', () => {
        this.removeOpenClass(item);
        item.parentElement.classList.toggle('open');
      });
    });
  }

  removeOpenClass(child: any) {
    this.element.querySelectorAll('.nav-parent').forEach((item) => {
      if (!item.contains(child)) {
        item.classList.remove('open');
      }
    });
  }

}
