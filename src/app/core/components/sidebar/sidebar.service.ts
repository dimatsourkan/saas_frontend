import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  private isSmall$ = new BehaviorSubject(false);

  get isSmall() {
    return this.isSmall$.getValue();
  }

  getIsSmallObservable() {
    return this.isSmall$.asObservable();
  }

  setSmall() {
    this.isSmall$.next(true);
  }

  setBig() {
    this.isSmall$.next(false);
  }
}
