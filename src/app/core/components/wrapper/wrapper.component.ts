import { AfterViewInit, Component } from '@angular/core';
import { SidebarService } from '@app/core/components/sidebar/sidebar.service';
import { AuthService } from '../../services/auth/auth.service';

declare var window: any;

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.scss']
})
export class WrapperComponent implements AfterViewInit {

  isSmall$ = this.sidebarService.getIsSmallObservable();

  constructor(
    private authService: AuthService,
    private sidebarService: SidebarService,
  ) {
  }

  get isManager() {
    return this.authService.isManager;
  }

  get isPublisher() {
    return this.authService.isPublisher;
  }

  get isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  ngAfterViewInit() {
    this.toggleZenDesk();
  }

  toggleZenDesk() {
    setTimeout(() => {
      if (this.isPublisher) {
        window.zE(function () {
          window.zE.hide();
        });
      } else {
        window.zE(function () {
          window.zE.show();
        });
      }
    });
  }

}
