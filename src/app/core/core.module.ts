import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, NgModule, Optional, SkipSelf } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CheckAdblockComponent } from '@app/core/components/check-adblock/check-adblock.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from '../shared/shared.module';
import { HeaderComponent } from './components/header/header.component';
import { ManagerSidebarComponent } from './components/sidebar/manager/manager.component';
import { PublisherSidebarComponent } from './components/sidebar/publisher/publisher.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import { LanguageInterceptor } from './interceprors/language.interceptor';
import { RequestInterceptor } from './interceprors/request.interceptor';
import { TokenInterceptor } from './interceprors/token.interceptor';
import { AppReadyService, init_app } from './services/app-ready/app-ready.service';
import { ThemeModule } from './theme/theme.module';
import { SidebarMenuDirective } from './components/sidebar/sidebar-menu.directive';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ThemeModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    SharedModule
  ],
  declarations: [
    WrapperComponent,
    HeaderComponent,
    PublisherSidebarComponent,
    ManagerSidebarComponent,
    CheckAdblockComponent,
    SidebarMenuDirective
  ],
  exports: [
    WrapperComponent,
    ThemeModule,
    CheckAdblockComponent
  ],
  providers: [
    AppReadyService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LanguageInterceptor,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: init_app,
      deps: [AppReadyService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    },

    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule?: CoreModule) {
    if (parentModule) {
      console.error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}


// required for AOT compilation
export function createTranslateLoader(http: HttpClient) {
  // return new TranslateHttpLoader(http, 'https://translate-test-6781a.web.app/', '.json');
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
