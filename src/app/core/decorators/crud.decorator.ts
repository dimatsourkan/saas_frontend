/**
 * Декоратор для круд сервисов
 * Устанавливает нужные параметры для его работы
 * Пример : @Crud('user', User, DataStore.user)
 * @param path   - Путь по которому сервис должен работать с сервером
 * @param Entity - Модель с которой работает сервис
 * @constructor
 */
import { BaseData } from '@app/core/models/base-data/base.data';

export function Crud(path: string, Entity: Function, dataStore: BaseData<any>): Function {
  return function (target: any) {
    target.prototype.path = path;
    target.prototype.Entity = Entity;
    target.prototype.dataStore = dataStore;
    return target;
  };
}
