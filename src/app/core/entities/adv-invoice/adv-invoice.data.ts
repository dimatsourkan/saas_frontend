import { AdvInv, AdvInvoice } from '@app/core/entities/adv-invoice/adv-invoice.model';
import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';

export class AdvInvoiceData extends BaseData<AdvInvoice, AdvInv.Data> {
  statuses = new StoreItem(new ResultList<Common.Status>(Common.Status));
  sentStatuses = new StoreItem(new ResultList<Common.Status>(Common.Status));
}
