import { FilterParams, FilterService } from '../../services/filter/filter.service';
import { modelId } from '@app/core/models/base-model/base.model';

class AdvInvoiceFilterParams extends FilterParams {
  status: modelId = null;
  sendStatus: number = null;
  totalCurrency: string = 'USD';
  dateFrom: string = null;
  dateTo: string = null;
  invoiceNumber: string = null;
  advertiserOrManagerName: string = null;
  ourAmount: number = null;
  advertiserAmount: number = null;
  discrepancy: number = null;
  invoiceTotal: number = null;
  paid: number = null;
  delay: number = null;
}

export class AdvInvoiceFilter extends FilterService {

  protected params = new AdvInvoiceFilterParams();

  /**
   *
   * @param param
   */
  set status(param: modelId) {
    this.params.status = param;
    this.emitChange();
  }

  get status() {
    return this.params.status;
  }

  /**
   *
   * @param param
   */
  set sendStatus(param: number) {
    this.params.sendStatus = param;
    this.emitChange();
  }

  get sendStatus() {
    return this.params.sendStatus;
  }

  /**
   *
   * @param param
   */
  set totalCurrency(param: string) {
    this.params.totalCurrency = param;
    this.emitChange();
  }

  get totalCurrency() {
    return this.params.totalCurrency;
  }

  /**
   *
   * @param param
   */
  set dateFrom(param: string) {
    this.params.dateFrom = param;
    this.page = 1;
  }

  get dateFrom() {
    return this.params.dateFrom;
  }

  /**
   *
   * @param param
   */
  set dateTo(param: string) {
    this.params.dateTo = param;
    this.page = 1;
  }

  get dateTo() {
    return this.params.dateTo;
  }

  /**
   *
   * @param param
   */
  set invoiceNumber(param: string) {
    this.params.invoiceNumber = param;
    this.params.page = 1;
  }

  get invoiceNumber() {
    return this.params.invoiceNumber;
  }

  /**
   *
   * @param param
   */
  set advertiserOrManagerName(param: string) {
    this.params.advertiserOrManagerName = param;
    this.params.page = 1;
  }

  get advertiserOrManagerName() {
    return this.params.advertiserOrManagerName;
  }

  /**
   *
   * @param param
   */
  set ourAmount(param: number) {
    this.params.ourAmount = param;
    this.params.page = 1;
  }

  get ourAmount() {
    return this.params.ourAmount;
  }

  /**
   *
   * @param param
   */
  set advertiserAmount(param: number) {
    this.params.advertiserAmount = param;
    this.params.page = 1;
  }

  get advertiserAmount() {
    return this.params.advertiserAmount;
  }

  /**
   *
   * @param param
   */
  set discrepancy(param: number) {
    this.params.discrepancy = param;
    this.params.page = 1;
  }

  get discrepancy() {
    return this.params.discrepancy;
  }

  /**
   *
   * @param param
   */
  set invoiceTotal(param: number) {
    this.params.invoiceTotal = param;
    this.params.page = 1;
  }

  get invoiceTotal() {
    return this.params.invoiceTotal;
  }

  /**
   *
   * @param param
   */
  set paid(param: number) {
    this.params.paid = param;
    this.params.page = 1;
  }

  get paid() {
    return this.params.paid;
  }

  /**
   *
   * @param param
   */
  set delay(param: number) {
    this.params.delay = param;
    this.params.page = 1;
  }

  get delay() {
    return this.params.delay;
  }

  public reset() {
    this.params = new AdvInvoiceFilterParams();
  }

}
