import { DataStore } from '@app/core/entities/data-store.service';
import { Common } from '@app/core/models/common-model/common.model';
import { Type } from 'class-transformer';
import * as moment from 'moment';
import { BaseModel, modelId } from '../../models/base-model/base.model';

export namespace AdvInv {

  export class Payment extends BaseModel {
    amount: number = 0;
    notes: string = null;
    date: string = moment().utc().format();
  }

  export class PaidStatus extends BaseModel {

    @Type(() => AdvInv.Payment)
    payments: AdvInv.Payment[] = [];

    paid: number = null;
    status: string = '';
    accountDate: string = '';
    accountNumber: string = '';
  }

  export class Data {

    @Type(() => Common.Amount)
    advertiserAmount = new Common.Amount();

    @Type(() => Common.Amount)
    discrepancy = new Common.Amount();

    @Type(() => Common.Amount)
    ourAmount = new Common.Amount();

    @Type(() => Common.Amount)
    paid = new Common.Amount();

    @Type(() => Common.Amount)
    total = new Common.Amount();
  }

}

export class AdvInvoice extends BaseModel {

  @Type(() => AdvInv.Payment)
  payments: AdvInv.Payment[] = [];

  accountDate: string = null;
  accountNumber: number = null;
  advertiserAmount: number = null;
  advertiserId: modelId = null;
  advertiserName: string = null;
  availableStatuses: number[] = [];
  currency: string = '';
  delay: number = null;
  discrepancy: number = null;
  hasCrossPayment: 1 | 0 = 0;
  invoiceDate: string = null;
  invoiceNumber: string = null;
  managerName: string = null;
  month: string = null;
  ourAmount: number = null;
  paid: number = null;
  sendStatus: string = null;
  status: string = null;
  total: number = null;
  updated: string = null;

  get statusModel() {
    return DataStore.advInvoice.statuses.getValue().items.find(status => status.id === this.status);
  }

  get statusSendModel() {
    return DataStore.advInvoice.sentStatuses.getValue().items.find(status => status.id === this.sendStatus);
  }

  statusIsAvailable(id) {
    return this.availableStatuses.includes(id);
  }

}
