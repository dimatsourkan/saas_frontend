import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AdvInvoiceData } from '@app/core/entities/adv-invoice/adv-invoice.data';
import { AdvInv, AdvInvoice } from '@app/core/entities/adv-invoice/adv-invoice.model';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { DataStore } from '@app/core/entities/data-store.service';
import { parseFileDownloadResult } from '@app/core/helpers/helpers';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { CRUDService } from '../../services/crud/crud.service';

@Injectable({
  providedIn: 'root'
})
@Crud('advertiser_invoices', AdvInvoice, DataStore.advInvoice)
export class AdvInvoiceService extends CRUDService<AdvInvoice, AdvInvoiceData, AdvInv.Data> {

  private advInvoiceStatusesCache$: Observable<ResultList<Common.Status>>;
  private advInvoiceSendStatusesCache$: Observable<ResultList<Common.Status>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.statuses.getValue().items.length) {
        this.getAdvInvoiceStatuses(true).subscribe();
      }
      if (this.dataStore.sentStatuses.getValue().items.length) {
        this.getInvoiceSentStatuses(true).subscribe();
      }
    });
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getAdvInvoiceStatuses(withoutCache = false) {
    if (!this.advInvoiceStatusesCache$ || withoutCache) {
      this.advInvoiceStatusesCache$ = this.httpClient.get(this.getUrl('statuses'), {headers: this.headers})
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), {items: data})),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'statuses', res)),
          shareReplay(1)
        );
    }
    return this.advInvoiceStatusesCache$;
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getInvoiceSentStatuses(withoutCache = false) {
    if (!this.advInvoiceSendStatusesCache$ || withoutCache) {
      this.advInvoiceSendStatusesCache$ = this.httpClient.get(this.getUrl('send_statuses'), {headers: this.headers})
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), {items: data})),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'sentStatuses', res)),
          shareReplay(1)
        );
    }
    return this.advInvoiceSendStatusesCache$;
  }

  changeStatusWithoutInformation(invoiceId: string, statusId: string): Observable<any> {
    return this.httpClient.put(this.getUrl(`${invoiceId}/status`), {status: statusId},
      {headers: this.headers})
      .pipe(tap(() => {
        const inv = DataStoreHelper.getItemByIdFromList(this.dataStore.list.getValue().items, invoiceId);
        if (inv) {
          inv.status = statusId;
          DataStoreHelper.updateItemInList(inv, this.dataStore);
        }
      }));
  }

  changeStatusWithInformation(invoiceId: string, data: AdvInv.PaidStatus): Observable<any> {
    return this.httpClient.put(this.getUrl(`${invoiceId}/paid_status`), data,
      {headers: this.headers})
      .pipe(tap(() => {
        const invoice = DataStoreHelper.getItemByIdFromList(this.dataStore.list.getValue().items, invoiceId);
        if (invoice) {
          DataStoreHelper.updateItemInList(invoice.update(data), this.dataStore);
        }
      }));
  }

  requestForSendInvoices(invoiceIds: string[]): Observable<any> {
    return this.httpClient.post(this.getUrl(`send`), {invoiceIds},
      {headers: this.headers})
      .pipe(tap(() => {
        invoiceIds.forEach(id => {
          const invoice = DataStoreHelper.getItemByIdFromList(this.dataStore.list.getValue().items, id);
          const status = DataStore.advInvoice.sentStatuses.getValue().items.find(s => s.origin === 'preparing_sending');
          invoice.sendStatus = status.id;
          DataStoreHelper.updateItemInList(invoice, this.dataStore);
        });
      }));
  }

  mergeAdvertisers(invoices_ids: string[]): Observable<any> {
    return this.httpClient.post(this.getUrl(`merged`), {invoices_ids},
      {headers: this.headers});
  }

  getDataForExport(invoiceId: string): Observable<any> {
    return this.httpClient.get(this.getUrl(`${invoiceId}/download`), {
      responseType: 'arraybuffer',
      observe: 'response'
    }).pipe(map(res => parseFileDownloadResult(res)));
  }

}
