import { TestBed } from '@angular/core/testing';

import { AdvertiserData } from '@app/core/entities/advertiser/advertiser.data';
import { ResultList } from '@app/core/models/result-model/result.model';

describe('AdvertiserData', () => {

  let service: AdvertiserData;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = new AdvertiserData();
  });

  /**
   *
   */
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   *
   */
  it('should be set statuses', () => {
    const statuses = (new ResultList<any>()).update({
      items: [{a: 1}]
    });

    service.statuses.setValue(statuses);
    expect(service.statuses.getValue()).toEqual(statuses);
    service.statuses.asObservable$.subscribe(res => {
      expect(res).toEqual(statuses);
    });
  });

  /**
   *
   */
  it('should be set terms', () => {
    const terms = (new ResultList<any>()).update({
      items: [{a: 1}]
    });

    service.terms.setValue(terms);
    expect(service.terms.getValue()).toEqual(terms);
    service.terms.asObservable$.subscribe(res => {
      expect(res).toEqual(terms);
    });
  });
});
