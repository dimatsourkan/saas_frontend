import { Adv, Advertiser } from '@app/core/entities/advertiser/advertiser.model';
import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';

export class AdvertiserData extends BaseData<Advertiser> {

  statuses = new StoreItem(new ResultList<Common.Status>(Common.Status));
  terms = new StoreItem(new ResultList<Adv.Term>(Adv.Term));

  clearData() {
    this.statuses.setValue(new ResultList<Common.Status>(Common.Status));
    this.terms.setValue(new ResultList<Adv.Term>(Adv.Term));
    super.clearData();
  }

}
