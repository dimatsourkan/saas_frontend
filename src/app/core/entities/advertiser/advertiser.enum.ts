export enum ADV_STATUS {
  PENDING = 1,
  ACTIVE  = 2,
  FROZEN  = 3,
  BANNED  = 4,
}

export type advStatusType = 1 | 2 | 3 | 4;
