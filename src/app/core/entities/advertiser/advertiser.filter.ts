import { FilterParams, FilterService } from '../../services/filter/filter.service';

class AdvertiserFilterParams extends FilterParams {
  status: number = null;
  'managersIds[]' = [];
  username = '';
  skype = '';
  email = '';
  firstName = '';
  lastName = '';
  address = '';
  'countriesCodes[]' = [];
  zipcode = '';
  phone = '';
  notes = '';
  url = '';
}

export class AdvertiserFilter extends FilterService {

  protected params = new AdvertiserFilterParams();

  /**
   *
   * @param status
   */
  set status(status: number) {
    this.params.status = status;
    this.emitChange();
  }

  get status() {
    return this.params.status;
  }

  /**
   *
   * @param managersIds
   */
  set managersIds(managersIds: string[]) {
    this.params['managersIds[]'] = managersIds;
    this.emitChange();
  }

  get managersIds() {
    return this.params['managersIds[]'];
  }

  /**
   *
   * @param username
   */
  set username(username: string) {
    this.params.username = username;
    this.emitChange();
  }

  get username() {
    return this.params.username;
  }

  /**
   *
   * @param skype
   */
  set skype(skype: string) {
    this.params.skype = skype;
    this.emitChange();
  }

  get skype() {
    return this.params.skype;
  }

  /**
   *
   * @param email
   */
  set email(email: string) {
    this.params.email = email;
    this.emitChange();
  }

  get email() {
    return this.params.email;
  }

  /**
   *
   * @param firstName
   */
  set firstName(firstName: string) {
    this.params.firstName = firstName;
    this.emitChange();
  }

  get firstName() {
    return this.params.firstName;
  }

  /**
   *
   * @param lastName
   */
  set lastName(lastName: string) {
    this.params.lastName = lastName;
    this.emitChange();
  }

  get lastName() {
    return this.params.lastName;
  }

  /**
   *
   * @param address
   */
  set address(address: string) {
    this.params.address = address;
    this.emitChange();
  }

  get address() {
    return this.params.address;
  }

  /**
   *
   * @param countriesCodes
   */
  set countriesCodes(countriesCodes: string[]) {
    this.params['countriesCodes[]'] = countriesCodes;
    this.emitChange();
  }

  get countriesCodes() {
    return this.params['countriesCodes[]'];
  }

  /**
   *
   * @param zipcode
   */
  set zipcode(zipcode: string) {
    this.params.zipcode = zipcode;
    this.emitChange();
  }

  get zipcode() {
    return this.params.zipcode;
  }

  /**
   *
   * @param phone
   */
  set phone(phone: string) {
    this.params.phone = phone;
    this.emitChange();
  }

  get phone() {
    return this.params.phone;
  }

  /**
   *
   * @param notes
   */
  set notes(notes: string) {
    this.params.notes = notes;
    this.emitChange();
  }

  get notes() {
    return this.params.notes;
  }

  /**
   *
   * @param url
   */
  set url(url: string) {
    this.params.url = url;
    this.emitChange();
  }

  get url() {
    return this.params.url;
  }

  public reset() {
    this.params = new AdvertiserFilterParams();
  }

}
