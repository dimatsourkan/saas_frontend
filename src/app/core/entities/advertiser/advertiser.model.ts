import { ADV_STATUS, advStatusType } from '@app/core/entities/advertiser/advertiser.enum';
import { Common } from '@app/core/models/common-model/common.model';
import { Expose, Transform, Type } from 'class-transformer';
import { BaseModel } from '../../models/base-model/base.model';

export namespace Adv {

  export class CrLimit {
    incentLimit: number = null;
    nonIncentLimit: number = null;
  }

  export class InvoiceNotification {
    beforeTermsDays = '';
    beforeDelayDays = '';
    afterDelayDays = '';
  }

  export class InvoiceData {

    // @Type(() => InvoiceNotification)
    // notification: InvoiceNotification = new InvoiceNotification();

    currency = '';
    invoiceCompany = null;
    invoiceEmails: string[] = [];
    invoiceNotes = '';
    paymentTerms = '';
    timezone = '';
    vat = null;
    vatCertificateFileId = '';
    vatPercent = null;
  }

  export class Url {
    advertiserUrl = '';
    offerCheckUrl = '';
    offerPageUrl = '';
    trackingUrlSuffix = '';
  }

  export class Term extends BaseModel {
    name: string = '';
  }
}

export class Advertiser extends BaseModel {

  @Type(() => Common.Address)
  address = new Common.Address();

  @Type(() => Common.Manager)
  manager = new Common.Manager();

  @Type(() => Adv.CrLimit)
  crLimit = new Adv.CrLimit();

  @Type(() => Adv.InvoiceData)
  invoiceData = new Adv.InvoiceData();

  @Type(() => Common.IpWhitelist)
  ipWhitelist = new Common.IpWhitelist();

  @Type(() => Common.Person)
  person = new Common.Person();

  @Type(() => Adv.Url)
  url = new Adv.Url();

  @Transform(value => value.map((v: any) => typeof v === 'object' ? v.id : v))
  publisherAccessList: string[] = [];

  additionalEmails: string[] = [];
  appWhiteList: string[] = [];
  agreementFileId = '';
  email = '';
  notes = '';
  postbackUrl = '';
  resalePostbackUrl = '';
  publisherAccessType = 2;
  securityType = 1;
  skype = '';
  status: advStatusType = ADV_STATUS.PENDING;
  token = '';
  username = '';
  phone = '';
  password?: string;
  confirmPassword?: string;

  @Expose({name: 'managerId'})
  get managerId() {
    return this.manager.id || null;
  }

}
