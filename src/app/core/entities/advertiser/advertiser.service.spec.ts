import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Adv } from '@app/core/entities/advertiser/advertiser.model';
import { DataStore } from '@app/core/entities/data-store.service';
import { testTokenValid } from '@app/core/entities/token/token.service.spec';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { AuthService } from '@app/core/services/auth/auth.service';
import { TranslateModule } from '@ngx-translate/core';

import { AdvertiserService } from './advertiser.service';

describe('AdvertiserService', () => {

  let authService: AuthService;
  let service: AdvertiserService;
  let apiUrlService: ApiUrlService;
  let backend: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot(), RouterTestingModule]
    });
    service = TestBed.get(AdvertiserService);
    apiUrlService = TestBed.get(ApiUrlService);
    backend = TestBed.get(HttpTestingController);
    authService = TestBed.get(AuthService);
    authService.authorizeApp(null, testTokenValid);
  });

  afterEach(() => {
    authService.logoutAll();
  });

  /**
   *
   */
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   *
   */
  it('should be api url', () => {
    expect(service.apiUrl).toBe(apiUrlService.apiUrlWithRole());
  });

  /**
   *
   */
  it('should be custom link', () => {
    expect(service.getAdvApiLink('1')).toBe(apiUrlService.apiUrlWithRole() + '/advertiser/1');
  });

  /**
   *
   */
  it('should be statuses', () => {
    const query = new ResultList<Common.Status>(Common.Status);
    query.items = [new Common.Status()];

    service.getAdvertisersStatuses().subscribe(res => {
      expect(res).toEqual(query);
      expect(DataStore.advertiser.statuses.getValue().toJson()).toEqual(res.toJson());
    });
    backend.expectOne({
      url: service.getUrl('statuses'),
      method: 'GET'
    }).flush(query);
  });

  /**
   *
   */
  it('should be terms', () => {

    const expectRes = new ResultList<Adv.Term>(Adv.Term);
    expectRes.items = [(new Adv.Term()).update({id: 0, name: ''})];

    service.getAdvertisersTerms().subscribe(res => {
      expect(res).toEqual(expectRes);
      expect(DataStore.advertiser.terms.getValue().toJson()).toEqual(res.toJson());
    });
    backend.expectOne({
      url: service.getUrl('payment_terms'),
      method: 'GET'
    }).flush({
      page: 1,
      limit: 25,
      totalPages: 1,
      totalItems: 0,
      items: {0: ''}
    });
  });

  /**
   *
   */
  it('should be change status', () => {

    service.changeStatus('1', 1).subscribe(res => {
      expect(res).toEqual({res: true});
    });
    backend.expectOne({
      url: service.getAdvApiLink('1/status'),
      method: 'PUT'
    }).flush({res: true});
  });
});
