import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AdvertiserData } from '@app/core/entities/advertiser/advertiser.data';
import { advStatusType } from '@app/core/entities/advertiser/advertiser.enum';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { DataStore } from '@app/core/entities/data-store.service';
import { parseFileDownloadResult } from '@app/core/helpers/helpers';
import { modelId } from '@app/core/models/base-model/base.model';
import { Common } from '@app/core/models/common-model/common.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { ResultList } from '../../models/result-model/result.model';
import { CRUDService } from '../../services/crud/crud.service';
import { Adv, Advertiser } from './advertiser.model';

@Injectable({
  providedIn: 'root'
})
@Crud('advertisers', Advertiser, DataStore.advertiser)
export class AdvertiserService extends CRUDService<Advertiser, AdvertiserData> {

  private advStatusesCache$: Observable<ResultList<Common.Status>>;
  private advTermsCache$: Observable<ResultList<Adv.Term>>;

  getAdvApiLink(path: string) {
    return `${this.apiUrl}/advertiser/${path}`;
  }

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.statuses.getValue().items.length) {
        this.getAdvertisersStatuses(true).subscribe();
      }
    });
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getAdvertisersStatuses(withoutCache = false) {
    if (!this.advStatusesCache$ || withoutCache) {
      this.advStatusesCache$ = this.httpClient.get(this.getUrl('statuses'), {headers: this.headers})
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), data as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'statuses', res)),
          shareReplay(1)
        );
    }
    return this.advStatusesCache$;
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getAdvertisersTerms(withoutCache = false) {
    if (!this.advTermsCache$ || withoutCache) {
      this.advTermsCache$ = this.httpClient.get(this.getUrl('payment_terms'), {headers: this.headers})
        .pipe(
          map(transformServerTermsToArray),
          map((data) => plainToClassFromExist(new ResultList<Adv.Term>(Adv.Term), data as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'terms', res)),
          shareReplay(1)
        );
    }
    return this.advTermsCache$;
  }

  /**
   *
   * @param id - model Id
   * @param status - Adv status
   */
  changeStatus(id: modelId, status: advStatusType): Observable<any> {
    return this.httpClient.put(this.getAdvApiLink(`${id}/status`), {id, status})
      .pipe(tap(() => {
        const adv = DataStoreHelper.getItemByIdFromList(this.dataStore.list.getValue().items, id);
        if (adv) {
          adv.status = status;
          DataStoreHelper.updateItemInList(adv, this.dataStore);
        }
      }));
  }

  /**
   *
   * @param id - model Id
   */
  downloadVat(id: modelId): Observable<any> {
    return this.httpClient.get(this.getAdvApiLink(`${id}/vat`), {
      responseType: 'blob',
      observe: 'response'
    }).pipe(map(res => parseFileDownloadResult(res)));
  }

  /**
   *
   * @param id - model Id
   */
  removeVat(id: modelId): Observable<any> {
    return this.httpClient.delete(this.getAdvApiLink(`${id}/vat`));
  }

  /**
   *
   * @param id - model Id
   * @param data - File data
   */
  saveVatToServer(id: modelId, data: any): Observable<any> {
    return this.httpClient.post(
      this.getAdvApiLink(`${id}/vat`), data, {headers: this.headers});
  }

  /**
   *
   * @param id - model Id
   */
  downloadAgreement(id: modelId): Observable<any> {
    return this.httpClient.get(this.getAdvApiLink(`${id}/agreement`), {
      responseType: 'blob',
      observe: 'response'
    }).pipe(map(res => parseFileDownloadResult(res)));
  }

  /**
   *
   * @param id - model Id
   * @param data - File data
   */
  saveAgreementToServer(id: modelId, data: any): Observable<any> {
    return this.httpClient.post(
      this.getAdvApiLink(`${id}/agreement`), data, {headers: this.headers});
  }

  /**
   *
   * @param id - model Id
   */
  removeAgreement(id: modelId): Observable<any> {
    return this.httpClient.delete(this.getAdvApiLink(`${id}/agreement`));
  }

}


function transformServerTermsToArray(data: { items: Object }) {
  const terms = {items: []};
  for (const prop in data.items) {
    if (data.items.hasOwnProperty(prop)) {
      terms.items.push({name: data.items[prop], id: Number(prop)});
    }
  }
  return terms;
}
