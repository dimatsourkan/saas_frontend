import { BaseData } from '@app/core/models/base-data/base.data';
import { Country } from './country.model';

export class CountryData extends BaseData<Country> {
}
