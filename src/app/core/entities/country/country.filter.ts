import { FilterParams, FilterService } from '../../services/filter/filter.service';

class CountryFilterParams extends FilterParams {
}

export class CountryFilter extends FilterService {

  protected params = new CountryFilterParams();

  public reset() {
    this.params = new CountryFilterParams();
  }

}
