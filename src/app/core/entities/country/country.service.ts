import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { LanguageService } from '@app/shared/services/language/language.service';

import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { CRUDService } from '../../services/crud/crud.service';
import { Country } from './country.model';
import { CountryData } from './country.data';

@Injectable({
  providedIn: 'root'
})
@Crud('country', Country, DataStore.country)
export class CountryService extends CRUDService<Country, CountryData> {

  private queryCache$: Observable<ResultList<Country>>;

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.list.getValue().items.length) {
        this.getAll(null, null, true).subscribe();
      }
    });
  }

  getAll(params = new HttpParams(), customLink = null, withoutCache = false) {
    if (!this.queryCache$ || withoutCache) {
      this.queryCache$ = super.getAll(params, this.baseUrl).pipe(
        shareReplay(1)
      );
    }
    return this.queryCache$;
  }

  getKeyList(params = new HttpParams(), customLink = null, withoutCache = false) {
    return this.getAll(params, customLink, withoutCache);
  }
}
