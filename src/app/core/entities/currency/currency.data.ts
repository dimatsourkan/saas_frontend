import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { modelId } from '@app/core/models/base-model/base.model';
import { Currency } from './currency.model';

export class CurrencyData extends BaseData<Currency> {
  public current: StoreItem<modelId> = new StoreItem(this.getLocalCurrency());

  private getLocalCurrency() {
    return localStorage.getItem('currency') || 'USD';
  }
}
