import { BaseModel } from '@app/core/models/base-model/base.model';

export class Currency extends BaseModel {
  name: string = null;
}
