import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';

import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { CRUDService } from '../../services/crud/crud.service';
import { Currency } from './currency.model';
import { CurrencyData } from './currency.data';
import { DataStoreHelper } from '../data-store.helper';

@Injectable({
  providedIn: 'root'
})
@Crud('currencies', Currency, DataStore.currency)
export class CurrencyService extends CRUDService<Currency, CurrencyData> {

  private queryCache$: Observable<ResultList<Currency>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService
  ) {
    super(httpClient, apiUrlService);
  }

  transformCurrencyListResult(res: { [key: string]: string }) {
    return Object.keys(res).map(id => {
      return {id, name: res[id]};
    });
  }

  protected wrapObjects(res: any): ResultList<Currency, CurrencyData> {
    res = this.transformCurrencyListResult(res);
    return plainToClassFromExist(new ResultList<Currency, CurrencyData>(Currency), {items: res} as Object);
  }

  getAll(params = new HttpParams(), customLink = null, withoutCache = false) {
    if (!this.queryCache$ || withoutCache) {
      this.queryCache$ = super.getAll(params, this.baseUrl).pipe(
        shareReplay(1)
      );
    }
    return this.queryCache$;
  }

  getKeyList(params = new HttpParams(), customLink = null, withoutCache = false) {
    return this.getAll(params, customLink, withoutCache);
  }

  setCurrency(currency: string): void {
    localStorage.setItem('currency', currency);
    DataStoreHelper.setDataCustom(this.dataStore, 'current', currency as any);
  }
}
