import { BaseData } from '@app/core/models/base-data/base.data';
import findIndex from 'lodash-es/findIndex';
import { BaseModel, modelId } from '../models/base-model/base.model';
import { ResultList } from '../models/result-model/result.model';


/**
 * Класс хелпер для работы с data-store
 */
export class DataStoreHelper {

  /**
   * Устанавливает список в dataStore
   * @param dataStore
   * @param res
   */
  static setDataList<T extends BaseModel, P extends BaseData<T>>(dataStore: P, res: ResultList<T>) {
    dataStore.list.setValue(res);
  }

  /**
   * Устанавливает список keyList в dataStore
   * @param dataStore
   * @param res
   */
  static setDataKeyList<T extends BaseModel, P extends BaseData<T>>(dataStore: P, res: ResultList<T>) {
    dataStore.keyList.setValue(res);
  }

  /**
   * Устанавливает элемент в dataStore
   * @param dataStore
   * @param res
   */
  static setDataItem<T extends BaseModel, P extends BaseData<T>>(dataStore: P, res: T) {
    dataStore.item.setValue(res);
  }

  /**
   * Устанавливает кастомный элемент в dataStore
   * @param dataStore
   * @param name
   * @param data
   */
  static setDataCustom<T extends BaseModel,
    P extends BaseData<T>,
    B extends keyof P>(dataStore: P, name: B, data: T | ResultList<T>) {

    const item: any = dataStore[name];

    if (item) {
      item.setValue(data);
    }
  }

  /**
   * Добавляет элемент в список в dataStore
   * @param dataStore
   * @param res
   */
  static pushItemToList<T extends BaseModel, P extends BaseData<T>>(dataStore: P, res: T) {
    const list = dataStore.list.getValue();
    list.items.push(res);
    dataStore.list.setValue(list);
  }

  /**
   * Изменяет элемент списка в dataStore
   * @param dataStore
   * @param item
   */
  static updateItemInList<T extends BaseModel, P extends BaseData<T>>(item: T, dataStore: P) {
    const list = dataStore.list.getValue();
    const index = this.getIndexById(list.items, item.id);

    if (index >= 0) {
      list.items[index] = item;
    }

    dataStore.list.setValue(list);
  }

  /**
   * Удаляет из списка в dataStore
   * @param dataStore
   * @param id
   */
  static removeItemFromList<T extends BaseModel, P extends BaseData<T>>(dataStore: P, id: modelId) {
    const list = dataStore.list.getValue();
    const index = this.getIndexById(list.items, id);

    if (index >= 0) {
      list.items.splice(index, 1);
    }

    dataStore.list.setValue(list);
  }

  /**
   * Добавляет id в список select
   * @param dataStore
   * @param id
   */
  static select<T extends BaseModel, P extends BaseData<T>>(dataStore: P, id: modelId) {
    const selected = dataStore.selected.getValue();
    if (!selected.includes(id)) {
      selected.push(id);
    }
  }

  /**
   * Удаляет id из списка select
   * @param dataStore
   * @param id
   */
  static deselect<T extends BaseModel, P extends BaseData<T>>(dataStore: P, id: modelId) {
    const selected = dataStore.selected.getValue();
    const index = selected.indexOf(id);
    if (index >= 0) {
      selected.splice(index, 1);
    }
  }

  /**
   * Удаляет добавляет в зависимости от текущего состояния id из списка select
   * @param dataStore
   * @param id
   */
  static toggleSelect<T extends BaseModel, P extends BaseData<T>>(dataStore: P, id: modelId) {
    const selected = dataStore.selected.getValue();
    const index = selected.indexOf(id);
    if (index >= 0) {
      selected.splice(index, 1);
    } else {
      selected.push(id);
    }

    dataStore.selected.setValue(selected);
  }

  /**
   * Удаляет добавляет в зависимости от текущего состояния id из списка select
   * @param dataStore
   * @param select
   */
  static toggleSelectAll<T extends BaseModel, P extends BaseData<T>>(dataStore: P, select: boolean = true) {
    let selected = dataStore.selected.getValue();
    if (select) {
      selected = dataStore.list.getValue().items.map(i => i.id);
    } else {
      selected = [];
    }

    dataStore.selected.setValue(selected);
  }

  /**
   * Получение индекса элемента в массиве по id
   * @param array
   * @param id
   */
  static getIndexById<T extends BaseModel>(array: T[] = [], id: modelId) {
    return findIndex(array, (i: T) => i.id === id);
  }

  /**
  * Получение индекса элемента в массиве по id
  * @param array
  * @param id
  */
  static getIndexByProp<T extends BaseModel>(array: T[] = [], id: modelId, prop: string) {
    return findIndex(array, (i: T) => i[prop] === id);
  }

  /**
   * Получение индекса элемента в массиве по id
   * @param array
   * @param id
   */
  static getItemByIdFromList<T extends BaseModel>(array: T[] = [], id: string): T {
    const index = DataStoreHelper.getIndexById(array, id);
    let adv = null;
    if (index >= 0) {
      adv = array[index];
    }
    return adv;
  }

  /**
 * Получение индекса элемента в массиве по id
 * @param array
 * @param id
 */
  static getItemByPropFromList<T extends BaseModel>(array: T[] = [], id: string, prop: string): T {
    const index = DataStoreHelper.getIndexByProp(array, id, prop);
    let adv = null;
    if (index >= 0) {
      adv = array[index];
    }
    return adv;
  }

}
