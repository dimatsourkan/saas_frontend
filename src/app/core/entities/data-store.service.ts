import { AdvInvoiceData } from '@app/core/entities/adv-invoice/adv-invoice.data';
import { AdvertiserData } from '@app/core/entities/advertiser/advertiser.data';
import { PublisherData } from '@app/core/entities/publisher/publisher.data';
import { TokenData } from '@app/core/entities/token/token.data';
import { StatisticData } from './reports/statistics-reports/statistics-reports.data';
import { ConversionReportData } from './reports/conversion-reports/conversion-reports.data';
import { ManagerData } from './manager/manager.data';
import { AlgorithmUrlData } from './superlink/algorithms-urls/algorithms-urls.data';
import { GroupUrlData } from './superlink/group-urls/group-urls.data';
import { PublisherUrlData } from './superlink/publisher-urls/publisher-urls.data';
import { SuperlinkConversionStatisticData } from './superlink/reports/conversion-statistics/superlink-conversion-statistics.data';
import { SuperlinkStatisticData } from './superlink/reports/statistics/superlink-statistics.data';
import { SystemResaleUrlData } from './superlink/system-resale-urls/system-resale-urls.data';
import { SuperlinkSystemGroupUrlData } from './superlink/system-group-urls/system-group-urls.data';
import { SuperlinkSystemStatisticData } from './superlink/system-reports/system-statistics/superlink-system-statistics.data';
import {
  SuperlinkSystemConversionStatisticData
} from './superlink/system-reports/system-conversion-statistics/superlink-system-conversion-statistics.data';
import { OfferData } from './offer/offer.data';
import { PubOfferData } from './pub-offer/pub-offer.data';
import { PubStatisticData } from './pub-reports/pub-statistics-reports/pub-statistics-reports.data';
import { PubConversionReportData } from './pub-reports/pub-conversion-reports/pub-conversion-reports.data';
import { PubGroupUrlData } from './pub-superlink/pub-group-urls/pub-group-urls.data';
import { PubSuperlinkStatisticData } from './pub-superlink/pub-statistics/pub-superlink-statistics.data';
import { PubBillingData } from './pub-billing/pub-billing.data';
import { CountryData } from './country/country.data';
import { CurrencyData } from './currency/currency.data';
import { TimezoneData } from './timezone/timezone.data';
import { PlatformsData } from './platforms/platforms.data';
import { OfferLoggerData } from './offer-logger/offer-logger.data';
import { OfferIntegrationData } from './offer-integration/offer-integration.data';
import { ExternalData } from './external/external.data';

/**
 * Сервис для хранения данных
 */
export class DataStore {

  static country = new CountryData();
  static currency = new CurrencyData();
  static timezone = new TimezoneData();
  static advertiser = new AdvertiserData();
  static advInvoice = new AdvInvoiceData();
  static publisher = new PublisherData();
  static manager = new ManagerData();
  static offer = new OfferData();
  static token = new TokenData();
  static statistic = new StatisticData();
  static conversionReport = new ConversionReportData();
  static superlinkAlgorithmsUrls = new AlgorithmUrlData();
  static superlinkGroupUrls = new GroupUrlData();
  static superlinkPublisherUrls = new PublisherUrlData();
  static superlinkConversionStatistic = new SuperlinkConversionStatisticData();
  static superlinkStatistic = new SuperlinkStatisticData();
  static superlinkSystemResaleUrls = new SystemResaleUrlData();
  static superlinkSystemGroupUrlData = new SuperlinkSystemGroupUrlData();
  static superlinkSystemConversionStatistic = new SuperlinkSystemConversionStatisticData();
  static superlinkSystemStatistic = new SuperlinkSystemStatisticData();
  static pubOffer = new PubOfferData();
  static pubStatistic = new PubStatisticData();
  static pubConversionReports = new PubConversionReportData();
  static superlinkPubGroupUrls = new PubGroupUrlData();
  static pubSuperlinkStatistic = new PubSuperlinkStatisticData();
  static pubBilling = new PubBillingData();
  static platforms = new PlatformsData();
  static offerLogger = new OfferLoggerData();
  static offerIntegration = new OfferIntegrationData();
  static externals = new ExternalData();



  static clearData() {
    DataStore.country.clearData();
    DataStore.advertiser.clearData();
    DataStore.advInvoice.clearData();
    DataStore.publisher.clearData();
    DataStore.manager.clearData();
    DataStore.offer.clearData();
    DataStore.token.clearData();
    DataStore.statistic.clearData();
    DataStore.conversionReport.clearData();
    DataStore.superlinkAlgorithmsUrls.clearData();
    DataStore.superlinkGroupUrls.clearData();
    DataStore.superlinkPublisherUrls.clearData();
    DataStore.pubOffer.clearData();
    DataStore.superlinkPubGroupUrls.clearData();
    DataStore.pubSuperlinkStatistic.clearData();
    DataStore.pubBilling.clearData();
    DataStore.platforms.clearData();
    DataStore.offerLogger.clearData();
    DataStore.offerIntegration.clearData();

  }

}
