import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { External } from './external.model';


export class ExternalData extends BaseData<External> {

  maxPrice = new StoreItem();

  clearData() {
    super.clearData();
  }

}
