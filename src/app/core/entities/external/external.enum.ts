export enum External_STATUS {
  PENDING     = 1,
  ACTIVE      = 2,
  DEACTIVATED = 3,
  BANNED      = 4,
  DELETED     = 5
}

export type ExternalsLoggertatusType = 1 | 2 | 3 | 4 | 5;
