import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';
import * as moment from 'moment';

class ExternalFilterParams extends FilterParams {
  dateFrom = moment().startOf('month').format('YYYY-MM-DD');
  dateTo = moment().format('YYYY-MM-DD');

  'advertiserIds[]': string[] = [];
  'platform[]': string[] = [];
  'countriesCodes[]': string[] = [];

  payoutMin: number = 0;
  payoutMax: number = 0;
  appId: string = null;
  realId: string = null;
  branding: number = null;
  incent: number = null;
  externalStatus: number = null;
}

export class ExternalFilter extends FilterService {

  protected params = new ExternalFilterParams();

  public reset() {
    this.params = new ExternalFilterParams();
  }

  /**
      *
      * @param dateFrom
      */
  set dateFrom(dateFrom: any) {
    this.params.dateFrom = dateFrom;
    this.emitChange();
  }

  get dateFrom() {
    return this.params.dateFrom;
  }

  /**
      *
      * @param dateTo
      */
  set dateTo(dateTo: any) {
    this.params.dateTo = dateTo;
    this.emitChange();
  }

  get dateTo() {
    return this.params.dateTo;
  }

  /**
    *
    * @param advertiserIds
    */
  set advertiserIds(advertiserIds: string[]) {
    this.params['advertiserIds[]'] = advertiserIds;
    this.emitChange();
  }

  get advertiserIds() {
    return this.params['advertiserIds[]'];
  }

  /**
   *
   * @param platform
   */
  set platform(platform: string[]) {
    this.params['platform[]'] = platform;
    this.emitChange();
  }

  get platform() {
    return this.params['platform[]'];
  }

  /**
 *
 * @param countriesCodes
 */
  set countriesCodes(countriesCodes: string[]) {
    this.params['countriesCodes[]'] = countriesCodes;
    this.emitChange();
  }

  get countriesCodes() {
    return this.params['countriesCodes[]'];
  }

  /**
      *
      * @param payoutMax
      */
  set payoutMax(payoutMax: any) {
    this.params.payoutMax = payoutMax;
    this.emitChange();
  }

  get payoutMax() {
    return this.params.payoutMax;
  }

  /**
      *
      * @param payoutMin
      */
  set payoutMin(payoutMin: any) {
    this.params.payoutMin = payoutMin;
    this.emitChange();
  }

  get payoutMin() {
    return this.params.payoutMin;
  }

  /**
      *
      * @param appId
      */
  set appId(appId: any) {
    this.params.appId = appId;
    this.emitChange();
  }

  get appId() {
    return this.params.appId;
  }

  /**
      *
      * @param realId
      */
  set realId(realId: any) {
    this.params.realId = realId;
    this.emitChange();
  }

  get realId() {
    return this.params.realId;
  }

  /**
      *
      * @param branding
      */
  set branding(branding: any) {
    this.params.branding = branding;
    this.emitChange();
  }

  get branding() {
    return this.params.branding;
  }

  /**
      *
      * @param incent
      */
  set incent(incent: any) {
    this.params.incent = incent;
    this.emitChange();
  }

  get incent() {
    return this.params.incent;
  }

  /**
      *
      * @param externalStatus
      */
  set externalStatus(externalStatus: any) {
    this.params.externalStatus = externalStatus;
    this.emitChange();
  }

  get externalStatus() {
    return this.params.externalStatus;
  }
}
