
import { Type } from 'class-transformer';
import { BaseModel } from '@app/core/models/base-model/base.model';

export class Platform {
  name: string;
  version: string;
}

export class ExternalItem {
  actualized_info: any;
  advertiser: { id: string, name: string, manager: { id: string, name: string } }
  app_id: string;
  approval_requested_at: any;
  approval_supported: boolean;
  approved: boolean;
  cap: { daily: number, monthly: number, total: number };
  conversion_model: string;
  countries: string[];
  created_at: string;
  creatives: string[]
  currency: string;
  description: string;
  expiration_date: string;
  id: string;
  incent: boolean;
  name: string;
  offer_id: string;
  parsed_at: string;
  payout: number;
  platforms: Platform[];
  preview_url: string;
  require_approval: boolean;
  restrictions: number[];
  tags: number[];
  thumbnail_url: string[];
  tracking_url: string;
  warnings: any[];
  checked: boolean;
}

export class LandingData {
  category_id: number;
  description: string;
  image_url: string;
  size: number;
  title: string;
  version: string;
}

export class External extends BaseModel {

  // @Type(() => LandingData)
  // externals = new LandingData();

  externals: Array<ExternalItem>;


  @Type(() => LandingData)
  landingData = new LandingData();

  externals_count: number;
  max_add_date: string;
  max_payout: number;
  min_add_date: string;
  min_payout: number;
  platforms: string[];


}

