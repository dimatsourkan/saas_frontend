import { HttpParams, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { DataStore } from '../data-store.service';

import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { ExternalData } from './external.data';
import { External } from './external.model';



@Injectable({
  providedIn: 'root'
})
@Crud('externals', External, DataStore.externals)
export class ExternalService extends CRUDService<External, ExternalData> {

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService
  ) {
    super(httpClient, apiUrlService);
    this.getMaxPrice().subscribe();
  }

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }


  getExternalDescription(id): Observable<any> {
    return this.httpClient
      .get(this.getUrl(`${id}/description`), { headers: this.headers })
  }

  getMaxPrice(): Observable<any> {
    return this.httpClient
      .get(this.getUrl('maxPrice'), { headers: this.headers }).pipe(
        tap((res) => DataStoreHelper.setDataCustom(this.dataStore, 'maxPrice', res.maxPrice))
      );
  }

  changeStatus(offerId: string, offerGoalId: string, status): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/offers/${offerId}/goal/${offerGoalId}`, { status });
  }

  requestForApproveExternalOffer(id: string): Observable<any> {
    return this.httpClient.get(this.getUrl(`${id}/request_approval`), { headers: this.headers });
  }

  getEternalDataForActualize(ids: Array<string>, flag: number): Observable<any> {
    const params = new HttpParams({ fromObject: { pre: flag.toString() } });
    return this.httpClient.post(`${this.apiUrl}/offer/actualize`, { ids }, { headers: this.headers, params });
  }

}
