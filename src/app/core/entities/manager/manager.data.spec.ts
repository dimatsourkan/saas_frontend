import { TestBed } from '@angular/core/testing';
import { ManagerData } from '@app/core/entities/manager/manager.data';

import { ResultList } from '@app/core/models/result-model/result.model';

describe('ManagerData', () => {

  let service: ManagerData;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = new ManagerData();
  });

  /**
   *
   */
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   *
   */
  it('should be set statuses', () => {
    const statuses = (new ResultList<any>()).update({
      items: [{a: 1}]
    });

    service.statuses.setValue(statuses);
    expect(service.statuses.getValue()).toEqual(statuses);
    service.statuses.asObservable$.subscribe(res => {
      expect(res).toEqual(statuses);
    });
  });

  /**
   *
   */
  it('should be set roles', () => {
    const roles = (new ResultList<any>()).update({
      items: [{a: 1}]
    });

    service.roles.setValue(roles);
    expect(service.roles.getValue()).toEqual(roles);
    service.roles.asObservable$.subscribe(res => {
      expect(res).toEqual(roles);
    });
  });
});
