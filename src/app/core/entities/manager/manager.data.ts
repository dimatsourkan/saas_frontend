import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Manager } from './manager.model';

export class ManagerData extends BaseData<Manager> {

  statuses = new StoreItem(new ResultList<Common.Status>());
  roles = new StoreItem(new ResultList<Common.Role>());

  clearData() {
    this.statuses.setValue(new ResultList<Common.Status>());
    this.roles.setValue(new ResultList<Common.Role>());
    super.clearData();
  }

}
