export enum MANAGER_STATUS {
  PENDING     = 1,
  ACTIVE      = 2,
  DEACTIVATED = 3,
  BANNED      = 4,
  DELETED     = 5
}

export type managerStatusType = 1 | 2 | 3 | 4 | 5;
