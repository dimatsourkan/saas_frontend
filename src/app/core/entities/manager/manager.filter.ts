import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';

class ManagerFilterParams extends FilterParams {

}

export class ManagerFilter extends FilterService {

  protected params = new ManagerFilterParams();

  public reset() {
    this.params = new ManagerFilterParams();
  }

}
