import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { modelId } from '@app/core/models/base-model/base.model';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { DataStore } from '../data-store.service';
import { ManagerData } from './manager.data';
import { managerStatusType } from './manager.enum';
import { Manager } from './manager.model';

@Injectable({
  providedIn: 'root'
})
@Crud('managers', Manager, DataStore.manager)
export class ManagerService extends CRUDService<Manager, ManagerData> {

  private managerStatusesCache$: Observable<ResultList<Common.Status>>;
  private managerRoleCache$: Observable<ResultList<Common.Role>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.statuses.getValue().items.length) {
        this.getManagerStatuses(true).subscribe();
      }
    });
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getManagerStatuses(withoutCache = false) {
    if (!this.managerStatusesCache$ || withoutCache) {
      this.managerStatusesCache$ = this.httpClient.get(this.getUrl(`statuses`), {headers: this.headers})
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), data as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'statuses', res)),
          shareReplay(1)
        );
    }
    return this.managerStatusesCache$;
  }

  getManagersRoles(withoutCache = false) {
    if (!this.managerRoleCache$ || withoutCache) {
      this.managerRoleCache$ = this.httpClient.get(this.getUrl(`../security/roles`), {headers: this.headers})
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Role>(Common.Role), {items: data} as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'roles', res)),
          shareReplay(1)
        );
    }
    return this.managerRoleCache$;
  }

  /**
   *
   * @param id - Manager id
   * @param status - Manager status
   */
  changeStatus(id: modelId, status: managerStatusType): Observable<any> {
    return this.httpClient.put(this.getUrl(`${id}/status`), {id, status})
      .pipe(tap(() => {
        const man = DataStoreHelper.getItemByIdFromList(this.dataStore.list.getValue().items, id);
        if (man) {
          man.status = status;
          DataStoreHelper.updateItemInList(man, this.dataStore);
        }
      }));
  }

}
