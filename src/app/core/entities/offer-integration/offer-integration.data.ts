import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { OfferIntegration } from './offer-integration.model';

export class OfferIntegrationData extends BaseData<OfferIntegration> {

  keylist = new StoreItem(new ResultList<Common.Status>());
  roles = new StoreItem(new ResultList<Common.Role>());
  statuses = new StoreItem(new ResultList<Common.Status>());
  fields = new StoreItem(new ResultList<Common.Status>());
  actions = new StoreItem(new ResultList<Common.Status>());
  reasons = new StoreItem(new ResultList<Common.Status>());
  platforms = new StoreItem(new ResultList<Common.Status>());
  autoImportOfferStatus = new StoreItem(new ResultList<Common.Status>());
  tags = new StoreItem(new ResultList<Common.Status>());
  restrictions = new StoreItem(new ResultList<Common.Status>());
  platformNames = new StoreItem(new ResultList<Common.Status>());
  isCheckerEnabled = new StoreItem();


  clearData() {
    this.keylist.setValue(new ResultList<Common.Status>());
    this.statuses.setValue(new ResultList<Common.Status>());
    this.roles.setValue(new ResultList<Common.Role>());
    this.platforms.setValue(new ResultList<Common.Status>());

    super.clearData();
  }

}
