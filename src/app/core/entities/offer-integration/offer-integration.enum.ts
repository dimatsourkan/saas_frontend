export enum OfferIntegration_STATUS {
  PENDING     = 1,
  ACTIVE      = 2,
  DEACTIVATED = 3,
  BANNED      = 4,
  DELETED     = 5
}

export type OfferIntegrationsLoggertatusType = 1 | 2 | 3 | 4 | 5;
