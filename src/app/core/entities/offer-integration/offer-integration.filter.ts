import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';

class OfferIntegrationFilterParams extends FilterParams {
  'advertiserIds[]': string[] = [];
  'platforms[]': string[] = [];
  enabled: 1 | 0 = null;
  approvalRequired: 1 | 0 = null;
  autoChanges: 1 | 0 = null;
  autoImport: 1 | 0 = null;
}

export class OfferIntegrationFilter extends FilterService {

  protected params = new OfferIntegrationFilterParams();

  public reset() {
    this.params = new OfferIntegrationFilterParams();
  }


  /**
    *
    * @param advertiserIds
    */
  set advertiserIds(advertiserIds: string[]) {
    this.params['advertiserIds[]'] = advertiserIds;
    this.emitChange();
  }

  get advertiserIds() {
    return this.params['advertiserIds[]'];
  }

  /**
   *
   * @param platforms
   */
  set platforms(platforms: string[]) {
    this.params['platforms[]'] = platforms;
    this.emitChange();
  }

  get platforms() {
    return this.params['platforms[]'];
  }

  /**
      *
      * @param enabled
      */
  set enabled(enabled: any) {
    this.params.enabled = enabled;
    this.emitChange();
  }

  get enabled() {
    return this.params.enabled;
  }

  /**
      *
      * @param approvalRequired
      */
  set approvalRequired(approvalRequired: any) {
    this.params.approvalRequired = approvalRequired;
    this.emitChange();
  }

  get approvalRequired() {
    return this.params.approvalRequired;
  }

  /**
      *
      * @param autoChanges
      */
  set autoChanges(autoChanges: any) {
    this.params.autoChanges = autoChanges;
    this.emitChange();
  }

  get autoChanges() {
    return this.params.autoChanges;
  }

  /**
      *
      * @param autoImport
      */
  set autoImport(autoImport: any) {
    this.params.autoImport = autoImport;
    this.emitChange();
  }

  get autoImport() {
    return this.params.autoImport;
  }
}
