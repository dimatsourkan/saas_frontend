
import { Type, Expose } from 'class-transformer';
import { BaseModel } from '@app/core/models/base-model/base.model';
import { Common } from '@app/core/models/common-model/common.model';


export class Data {
  oldValues: any;
  newValues: any;
}

export class Credentials {
  platform: string;
  login: string;
  password: string;
}

export class AutoSuspend {
  failedChecksCount: number = 5;
  enabled: boolean;
}


export class AutoImport {
  enabled: boolean;
  convertNonIncentToIncent: boolean;
  deleteDescription: boolean;
  addPauseText: boolean;
  offerStatus: number;
  currency: string;
  defaultTagId: number;
  minPrice: number = 0;
  maxPrice: number = 0;
  margin: number = 0;
  geo: string[];
  restrictionsForIncent: number[];
  restrictionsForNonIncent: number[];
  platforms: string[];
}

export class OfferIntegration extends BaseModel {

  @Type(() => Data)
  data = new Data();
  @Type(() => AutoImport)
  autoImport = new AutoImport();
  @Type(() => Credentials)
  credentials = new Credentials();
  @Type(() => AutoSuspend)
  autoSuspend = new AutoSuspend();

  action: string;
  datetime: string;
  manager_id: string;
  offer_id: string;
  reason: string;
  system: boolean;
  status: string | number;

  advertiserId: string;
  advertiserName: string;

  enabled: boolean;
  approvalRequired: boolean;
  autoPausePeriod: boolean;

  autoChanges: boolean;
  period: number = 60;

}

