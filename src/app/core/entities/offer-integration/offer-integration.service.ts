import { Injectable } from '@angular/core';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { DataStore } from '../data-store.service';
import { OfferIntegration } from './offer-integration.model';
import { OfferIntegrationData } from './offer-integration.data';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
@Crud('integrations', OfferIntegration, DataStore.offerIntegration)
export class OfferIntegrationService extends CRUDService<OfferIntegration, OfferIntegrationData> {

  private statusesCache$: Observable<ResultList<Common.Status>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  /**
 *
 * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
 */
  getStatusesCache(withoutCache = false) {
    if (!this.statusesCache$ || withoutCache) {
      this.statusesCache$ = this.httpClient.get(this.getUrl('statuses'), {
        headers: this.headers
      }).pipe(
        map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'statuses', res)),
        shareReplay(1)
      );
    }
    return this.statusesCache$;
  }

  getPlatforms(params = new HttpParams()): Observable<ResultList<any>> {
    params = params.set('context', 'keylist');
    return this.httpClient
      .get(this.getUrl('platforms'), { headers: this.headers, params }).pipe(
        map((res) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), res as Object)),
        tap((res) => DataStoreHelper.setDataCustom(this.dataStore, 'platforms', res))
      );
  }

  getAutoImportOfferStatus(): Observable<ResultList<any>> {
    return this.httpClient
      .get(this.getUrl('autoimport_offer_statuses'), { headers: this.headers }).pipe(
        map((res) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: res } as Object)),
        tap((res) => DataStoreHelper.setDataCustom(this.dataStore, 'autoImportOfferStatus', res))
      );
  }

  getTags(): Observable<ResultList<any>> {
    return this.httpClient
      .get(`${this.apiUrl}/tags`, { headers: this.headers }).pipe(
        map((res) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: res } as Object)),
        tap((res) => DataStoreHelper.setDataCustom(this.dataStore, 'tags', res))
      );
  }

  getRestrictions(): Observable<ResultList<any>> {
    return this.httpClient
      .get(`${this.apiUrl}/restrictions`, { headers: this.headers }).pipe(
        map((res: any) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status),
          { items: res.map(item => ({ name: item.name, id: String(item.id) })) } as Object)),
        tap((res) => DataStoreHelper.setDataCustom(this.dataStore, 'restrictions', res))
      );
  }

  getPlatformNames(): Observable<ResultList<any>> {
    return this.httpClient
      .get(`${this.apiUrl}/platform/names`, { headers: this.headers }).pipe(
        map((res) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), res as Object)),
        tap((res) => DataStoreHelper.setDataCustom(this.dataStore, 'platformNames', res))
      );
  }

  startIntegration(advId: string): Observable<any> {
    return this.httpClient.post(this.getUrl(`${advId}/start`), {},
      { headers: this.headers })
      .pipe(tap(() => {
        const integration = DataStoreHelper.getItemByPropFromList(this.dataStore.list.getValue().items, advId, 'advertiserId');
        integration.status = 2;
        DataStoreHelper.updateItemInList(integration, this.dataStore);
      }));
  }

  getIsCheckerEnabled(): Observable<any> {
    return this.httpClient
      .get(`${this.apiUrl}/settings/checker_enabled`, { headers: this.headers }).pipe(
        tap((res) => DataStoreHelper.setDataCustom(this.dataStore, 'isCheckerEnabled', res.enabled))
      );
  }



  getIntegrationsAdditionalInfo(name) {
    return this.httpClient
      .get(this.getUrl(`platform_api_description`), { headers: this.headers, params: {name} });
  }

  testCredention(credentials) {
    return this.httpClient
      .post(this.getUrl('verify'), credentials, { headers: this.headers });
  }
}
