import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { OfferLogger } from './offer-logger.model';

export class OfferLoggerData extends BaseData<OfferLogger> {

  keylist = new StoreItem(new ResultList<Common.Status>());
  roles = new StoreItem(new ResultList<Common.Role>());
  statuses = new StoreItem(new ResultList<Common.Status>());
  fields = new StoreItem(new ResultList<Common.Status>());
  actions = new StoreItem(new ResultList<Common.Status>());
  reasons = new StoreItem(new ResultList<Common.Status>());

  clearData() {
    this.keylist.setValue(new ResultList<Common.Status>());
    this.statuses.setValue(new ResultList<Common.Status>());
    this.roles.setValue(new ResultList<Common.Role>());

    super.clearData();
  }

}
