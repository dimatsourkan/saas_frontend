import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';
import * as moment from 'moment';

class OfferLoggerFilterParams extends FilterParams {
  dateFrom = moment().startOf('month').format('YYYY-MM-DD');
  dateTo = moment().format('YYYY-MM-DD');

  'actionsIds[]': string[] = [];
  'reasonsIds[]': string[] = [];
  'managersIds[]': string[] = [];
  'fields[]': string[] = [];
  'offersIds[]': string[] = [];
  isSystem: 1 | 0 = null;

}

export class OfferLoggerFilter extends FilterService {

  protected params = new OfferLoggerFilterParams();

  public reset() {
    this.params = new OfferLoggerFilterParams();
  }

  /**
      *
      * @param dateFrom
      */
  set dateFrom(dateFrom: any) {
    this.params.dateFrom = dateFrom;
    this.emitChange();
  }

  get dateFrom() {
    return this.params.dateFrom;
  }

  /**
      *
      * @param dateTo
      */
  set dateTo(dateTo: any) {
    this.params.dateTo = dateTo;
    this.emitChange();
  }

  get dateTo() {
    return this.params.dateTo;
  }

  /**
    *
    * @param actionsIds
    */
  set actionsIds(actionsIds: string[]) {
    this.params['actionsIds[]'] = actionsIds;
    this.emitChange();
  }

  get actionsIds() {
    return this.params['actionsIds[]'];
  }

  /**
   *
   * @param reasonsIds
   */
  set reasonsIds(reasonsIds: string[]) {
    this.params['reasonsIds[]'] = reasonsIds;
    this.emitChange();
  }

  get reasonsIds() {
    return this.params['reasonsIds[]'];
  }

  /**
 *
 * @param managersIds
 */
  set managersIds(managersIds: string[]) {
    this.params['managersIds[]'] = managersIds;
    this.emitChange();
  }

  get managersIds() {
    return this.params['managersIds[]'];
  }

  /**
*
* @param fields
*/
  set fields(fields: string[]) {
    this.params['fields[]'] = fields;
    this.emitChange();
  }

  get fields() {
    return this.params['fields[]'];
  }

  /**
*
* @param offersIds
*/
  set offersIds(offersIds: string[]) {
    this.params['offersIds[]'] = offersIds;
    this.emitChange();
  }

  get offersIds() {
    return this.params['offersIds[]'];
  }

  /**
      *
      * @param isSystem
      */
  set isSystem(isSystem: any) {
    this.params.isSystem = isSystem;
    this.emitChange();
  }

  get isSystem() {
    return this.params.isSystem;
  }

}
