
import { Type } from 'class-transformer';
import { BaseModel } from '@app/core/models/base-model/base.model';


export class Data {
  oldValues: any;
  newValues: any;
}

export class OfferLogger extends BaseModel {

  @Type(() => Data)
  data = new Data();

  action: string;
  datetime: string;
  manager_id: string;
  offer_id: string;
  reason: string;
  system: boolean;


}

