import { HttpParams, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { DataStore } from '../data-store.service';
import { OfferLogger } from './offer-logger.model';
import { OfferLoggerData } from './offer-logger.data';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { LanguageService } from '@app/shared/services/language/language.service';



@Injectable({
  providedIn: 'root'
})
@Crud('offer_action_log', OfferLogger, DataStore.offerLogger)
export class OfferLoggerService extends CRUDService<OfferLogger, OfferLoggerData> {

  private statusesCache$: Observable<ResultList<Common.Status>>;
  private fieldsCache$: Observable<ResultList<Common.Status>>;
  private actionsCache$: Observable<ResultList<Common.Status>>;
  private reasonsCache$: Observable<ResultList<Common.Status>>;

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.statuses.getValue().items.length) {
        this.getFieldsCache(true).subscribe();
        this.getActionsCache(true).subscribe();
        this.getReasonsCache(true).subscribe();
      }
    });
  }

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  getOfferLoggersKeyListCache(params = new HttpParams()): Observable<ResultList<any>> {
    params = params.set('context', 'publisher-panel.OfferLoggers.keylist');
    return this.httpClient
      .get(this.getUrl('key_list'), { headers: this.headers, params }).pipe(
        map((res) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), res as Object)),
        tap((res) => DataStoreHelper.setDataKeyList(this.dataStore, res))
      );
  }

  /**
 *
 * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
 */
  getStatusesCache(withoutCache = false) {
    if (!this.statusesCache$ || withoutCache) {
      this.statusesCache$ = this.httpClient.get(`${this.apiUrl}/OfferLogger/statuses`, {
        headers: this.headers
      }).pipe(
        map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'statuses', res)),
        shareReplay(1)
      );
    }
    return this.statusesCache$;
  }

  /**
 *
 * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
 */
  getFieldsCache(withoutCache = false) {
    if (!this.fieldsCache$ || withoutCache) {
      this.fieldsCache$ = this.httpClient.get(this.getUrl('fields'), {
        headers: this.headers
      }).pipe(
        map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), data as Object)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'fields', res)),
        shareReplay(1)
      );
    }
    return this.fieldsCache$;
  }

  /**
 *
 * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
 */
  getActionsCache(withoutCache = false) {
    if (!this.actionsCache$ || withoutCache) {
      this.actionsCache$ = this.httpClient.get(this.getUrl('actions'), {
        headers: this.headers
      }).pipe(
        map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), data as Object)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'actions', res)),
        shareReplay(1)
      );
    }
    return this.actionsCache$;
  }


  /**
 *
 * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
 */
  getReasonsCache(withoutCache = false) {
    if (!this.reasonsCache$ || withoutCache) {
      this.reasonsCache$ = this.httpClient.get(this.getUrl('reasons'), {
        headers: this.headers
      }).pipe(
        map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), data as Object)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'reasons', res)),
        shareReplay(1)
      );
    }
    return this.reasonsCache$;
  }

}
