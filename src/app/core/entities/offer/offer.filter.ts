import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';

class OfferFilterParams extends FilterParams {

}

export class OfferFilter extends FilterService {

  protected params = new OfferFilterParams();

  public reset() {
    this.params = new OfferFilterParams();
  }

}
