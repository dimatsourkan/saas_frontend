import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DataStore } from '@app/core/entities/data-store.service';
import { ManagerService } from '@app/core/entities/manager/manager.service';
import { testTokenValid } from '@app/core/entities/token/token.service.spec';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { AuthService } from '@app/core/services/auth/auth.service';
import { TranslateModule } from '@ngx-translate/core';

describe('ManagerService', () => {

  let authService: AuthService;
  let service: ManagerService;
  let apiUrlService: ApiUrlService;
  let backend: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot(), RouterTestingModule]
    });
    service = TestBed.get(ManagerService);
    apiUrlService = TestBed.get(ApiUrlService);
    backend = TestBed.get(HttpTestingController);
    authService = TestBed.get(AuthService);
    authService.authorizeApp(null, testTokenValid);
  });

  afterEach(() => {
    authService.logoutAll();
  });

  /**
   *
   */
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   *
   */
  it('should be api url', () => {
    expect(service.apiUrl).toBe(apiUrlService.apiUrlWithRole());
  });

  /**
   *
   */
  it('should be statuses', () => {
    const query = new ResultList<Common.Status>(Common.Status);
    query.items = [new Common.Status()];

    service.getManagerStatuses().subscribe(res => {
      expect(res).toEqual(query);
      expect(DataStore.manager.statuses.getValue().toJson()).toEqual(res.toJson());
    });
    backend.expectOne({
      url: service.getUrl('statuses'),
      method: 'GET'
    }).flush(query);
  });

  /**
   *
   */
  it('should be roles', () => {

    const query = new ResultList<Common.Role>(Common.Role);
    query.items = [new Common.Role()];

    service.getManagersRoles().subscribe(res => {
      expect(res).toEqual(query);
      expect(DataStore.manager.roles.getValue().toJson()).toEqual(res.toJson());
    });
    backend.expectOne({
      url: service.getUrl('../security/roles'),
      method: 'GET'
    }).flush([new Common.Role()]);
  });
});
