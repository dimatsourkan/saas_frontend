import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { modelId } from '@app/core/models/base-model/base.model';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { DataStore } from '../data-store.service';
import { Offer } from './offer.model';
import { OfferData } from './offer.data';


@Injectable({
  providedIn: 'root'
})
@Crud('offers', Offer, DataStore.offer)
export class OfferService extends CRUDService<Offer, OfferData> {

  private statusesCache$: Observable<ResultList<Common.Status>>;


  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  getOffersKeyListCache(params = new HttpParams()): Observable<ResultList<any>> {
    params = params.set('context', 'publisher-panel.offers.keylist');
    return this.httpClient
    .get(this.getUrl('key_list'), {headers: this.headers, params}).pipe(
      map((res) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), res as Object)),
      tap((res) => DataStoreHelper.setDataKeyList(this.dataStore, res))
    );
  }

  /**
 *
 * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
 */
  getStatusesCache(withoutCache = false) {
    if (!this.statusesCache$ || withoutCache) {
      this.statusesCache$ = this.httpClient.get(`${this.apiUrl}/offer/statuses`, {
        headers: this.headers
      }).pipe(
        map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'statuses', res)),
        shareReplay(1)
      );
    }
    return this.statusesCache$;
  }

}
