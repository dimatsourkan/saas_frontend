import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Platform } from './platforms.model';

export class PlatformsData extends BaseData<Platform> {

  statuses = new StoreItem(new ResultList<Common.Status>(Common.Status));

  clearData() {
    this.statuses.setValue(new ResultList<Common.Status>(Common.Status));
    super.clearData();
  }

}
