import { FilterParams, FilterService } from '../../services/filter/filter.service';

class PlatformsFilterParams extends FilterParams {

}

export class PlatformsFilter extends FilterService {

  protected params = new PlatformsFilterParams();

  public reset() {
    this.params = new PlatformsFilterParams();
  }

}
