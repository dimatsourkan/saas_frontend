import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { DataStore } from '@app/core/entities/data-store.service';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { Crud } from '../../decorators/crud.decorator';
import { CRUDService } from '../../services/crud/crud.service';
import { PlatformsData } from './platforms.data';
import { Platform } from './platforms.model';

@Injectable({
  providedIn: 'root'
})
@Crud('platform', Platform, DataStore.platforms)
export class PlatformsService extends CRUDService<Platform, PlatformsData> {


  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
  }

}

