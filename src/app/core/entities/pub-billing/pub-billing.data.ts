import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { PubBilling } from './pub-billing.model';

export class PubBillingData extends BaseData<PubBilling> {

  keylist = new StoreItem(new ResultList<Common.Status>());
  roles = new StoreItem(new ResultList<Common.Role>());
  statuses = new StoreItem(new ResultList<Common.Status>());
  paymentTerms = new StoreItem(new ResultList<Common.Status>());
  partType = new StoreItem(new ResultList<Common.Status>());


  clearData() {
    this.keylist.setValue(new ResultList<Common.Status>());
    this.statuses.setValue(new ResultList<Common.Status>());
    this.roles.setValue(new ResultList<Common.Role>());
    this.partType.setValue(new ResultList<Common.Status>());

    super.clearData();
  }

}
