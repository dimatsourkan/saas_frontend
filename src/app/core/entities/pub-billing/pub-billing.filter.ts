import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';
import * as moment from 'moment';

class PubBillingFilterParams extends FilterParams {
  from = moment().format('YYYY-MM-DD');
  to = moment().format('YYYY-MM-DD');
  status: number = null;
}

export class PubBillingFilter extends FilterService {

  protected params = new PubBillingFilterParams();

  public reset() {
    this.params = new PubBillingFilterParams();
  }

  /**
      *
      * @param from
      */
  set from(from: any) {
    this.params.from = from;
    this.emitChange();
  }

  get from() {
    return this.params.from;
  }

  /**
      *
      * @param to
      */
  set to(to: any) {
    this.params.to = to;
    this.emitChange();
  }

  get to() {
    return this.params.to;
  }

  /**
      *
      * @param status
      */
      set status(status: any) {
          this.params.status = status;
          this.emitChange();
      }

      get status() {
          return this.params.status;
      }

}
