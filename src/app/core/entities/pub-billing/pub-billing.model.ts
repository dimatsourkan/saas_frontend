
import { Transform, Type } from 'class-transformer';
import { BaseModel } from '@app/core/models/base-model/base.model';
import { Common } from '@app/core/models/common-model/common.model';

export namespace Man {

}

export class PubBilling extends BaseModel {

  @Type(() => Common.Address)
  address = new Common.Address();

  @Type(() => Common.Person)
  person = new Common.Person();

  @Transform(value => typeof value === 'number' ? [value] : value)
  roles: number[] = [];

  additionalEmails: string[] = [];
  email: string = '';
  notes: string = '';
  phone: string = '';
  skype: string = '';
  username: string = '';
  emailNotificationEnabled: 0 | 1 = 0;
  password?: string;
  confirmPassword?: string;

  get role() {
    return this.roles[0] || null;
  }

  get fullName() {
    return `${this.person.firstName} ${this.person.lastName}`;
  }
}

