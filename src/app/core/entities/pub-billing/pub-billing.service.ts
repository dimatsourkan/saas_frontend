import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { DataStore } from '../data-store.service';
import { PubBilling } from './pub-billing.model';
import { PubBillingData } from './pub-billing.data';
import { parseFileDownloadResult } from '@app/core/helpers/helpers';


@Injectable({
  providedIn: 'root'
})
@Crud('billing/invoice', PubBilling, DataStore.pubBilling)
export class PubBillingService extends CRUDService<PubBilling, PubBillingData> {

  private statusesCache$: Observable<ResultList<Common.Status>>;
  private paymentTermsCache$: Observable<ResultList<Common.Status>>;
  private partTypesCache$: Observable<ResultList<Common.Status>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  /**
 *
 * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
 */
  getStatusesCache(withoutCache = false) {
    if (!this.statusesCache$ || withoutCache) {
      this.statusesCache$ = this.httpClient.get(this.getUrl('status'), {
        headers: this.headers
      }).pipe(
        map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'statuses', res)),
        shareReplay(1)
      );
    }
    return this.statusesCache$;
  }


  /**
*
* @param withoutCache - Если false то данные из кеша, если true то данные с сервера
*/
  getPaymentTerms(withoutCache = false) {
    if (!this.paymentTermsCache$ || withoutCache) {
      this.paymentTermsCache$ = this.httpClient.get(`${this.apiUrl}/publisher/payment_terms`, {
        headers: this.headers
      }).pipe(
        map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), data)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'paymentTerms', res)),
        shareReplay(1)
      );
    }
    return this.paymentTermsCache$;
  }


  getPartType(withoutCache = false) {
    if (!this.partTypesCache$ || withoutCache) {
      this.partTypesCache$ = this.httpClient.get(`${this.apiUrl}/billing/part_type`, {
        headers: this.headers
      }).pipe(
        map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'partType', res)),
        shareReplay(1)
      );
    }
    return this.partTypesCache$;
  }

  downloadInvoice(id: string) {
    return this.httpClient.get(this.getUrl(`download/${id}`), {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  }


}
