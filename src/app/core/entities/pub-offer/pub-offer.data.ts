import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { PubOffer } from './pub-offer.model';

export class PubOfferData extends BaseData<PubOffer> {

  keylist = new StoreItem(new ResultList<Common.Status>());
  roles = new StoreItem(new ResultList<Common.Role>());
  statuses = new StoreItem(new ResultList<Common.Status>());
  trafficTypes = new StoreItem(new ResultList<Common.Status>());


  clearData() {
    this.keylist.setValue(new ResultList<Common.Status>());
    this.statuses.setValue(new ResultList<Common.Status>());
    this.roles.setValue(new ResultList<Common.Role>());
    this.trafficTypes.setValue(new ResultList<Common.Status>());

    super.clearData();
  }

}
