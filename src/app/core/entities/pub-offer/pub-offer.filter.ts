import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';

class PubOfferFilterParams extends FilterParams {
  'countries[]' = [];
  'tags[]' = [];
  'platforms[]' = [];
  'trafficType[]' = [];

  newOffers: 1 | 0 = 0;
  approved: 1 | 0  = null;

}

export class PubOfferFilter extends FilterService {

  protected params = new PubOfferFilterParams();

  public reset() {
    this.params = new PubOfferFilterParams();
  }

  /**
      *
      * @param newOffers
      */
  set newOffers(newOffers: 1 | 0 ) {
    this.params.newOffers = newOffers;
    this.emitChange();
  }

  get newOffers() {
    return this.params.newOffers;
  }

  /**
      *
      * @param approved
      */
  set approved(approved: 1 | 0 ) {
    this.params.approved = approved;
    this.emitChange();
  }

  get approved() {
    return this.params.approved;
  }

  /**
   *
   * @param countries
   */
  set countries(countries: string[]) {
    this.params['countries[]'] = countries;
    this.emitChange();
  }

  get countries() {
    return this.params['countries[]'];
  }

  /**
 *
 * @param tags
 */
  set tags(tags: string[]) {
    this.params['tags[]'] = tags;
    this.emitChange();
  }

  get tags() {
    return this.params['tags[]'];
  }

  /**
*
* @param platforms
*/
  set platforms(platforms: string[]) {
    this.params['platforms[]'] = platforms;
    this.emitChange();
  }

  get platforms() {
    return this.params['platforms[]'];
  }


  /**
*
* @param trafficType
*/
  set trafficType(trafficType: string[]) {
    this.params['trafficType[]'] = trafficType;
    this.emitChange();
  }

  get trafficType() {
    return this.params['trafficType[]'];
  }

}
