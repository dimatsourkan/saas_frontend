import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';
import * as moment from 'moment';
import { DataStore } from '../../data-store.service';

class PubConversionReportsFilterParams extends FilterParams {
  datetimeFrom = moment().format('YYYY-MM-DD');
  datetimeTo = moment().format('YYYY-MM-DD');
  timezone: string = DataStore.timezone.current.getValue();

  publisherClickId: string = null;
  affSub: string = null;
  affSub2: string = null;
  affSub3: string = null;
  affSub4: string = null;
  platforms: 1 | 0 = null;

  'countries[]' = [];
  'offers[]' = [];
  'platforms[]' = [];
}

export class PubConversionReportsFilter extends FilterService {

  protected params = new PubConversionReportsFilterParams();

  public reset() {
    this.params = new PubConversionReportsFilterParams();
  }

  /**
   *
   * @param datetimeFrom
   */
  set datetimeFrom(datetimeFrom: string) {
    this.params.datetimeFrom = datetimeFrom;
    this.emitChange();
  }

  get datetimeFrom() {
    return this.params.datetimeFrom;
  }

  /**
  *
  * @param datetimeTo
  */
  set datetimeTo(datetimeTo: string) {
    this.params.datetimeTo = datetimeTo;
    this.emitChange();
  }

  get datetimeTo() {
    return this.params.datetimeTo;
  }


  /**
   *
   * @param countries
   */
  set countries(countries: string[]) {
    this.params['countries[]'] = countries;
    this.emitChange();
  }

  get countries() {
    return this.params['countries[]'];
  }

  /**
  *
  * @param offers
  */
  set offers(offers: string[]) {
    this.params['offers[]'] = offers;
    this.emitChange();
  }

  get offers() {
    return this.params['offers[]'];
  }

  /**
*
* @param platforms
*/
  set platforms(platforms: string[]) {
    this.params['platforms[]'] = platforms;
    this.emitChange();
  }

  get platforms() {
    return this.params['platforms[]'];
  }


  /**
      *
      * @param timezone
      */
  set timezone(timezone: string) {
    this.params.timezone = timezone;
    this.emitChange();
  }

  get timezone() {
    return this.params.timezone;
  }


  /**
      *
      * @param publisherClickId
      */
  set publisherClickId(publisherClickId: any) {
    this.params.publisherClickId = publisherClickId;
    this.emitChange();
  }

  get publisherClickId() {
    return this.params.publisherClickId;
  }

  /**
      *
      * @param affSub
      */
  set affSub(affSub: any) {
    this.params.affSub = affSub;
    this.emitChange();
  }

  get affSub() {
    return this.params.affSub;
  }

  /**
      *
      * @param affSub2
      */
  set affSub2(affSub2: any) {
    this.params.affSub2 = affSub2;
    this.emitChange();
  }

  get affSub2() {
    return this.params.affSub2;
  }

  /**
      *
      * @param affSub3
      */
  set affSub3(affSub3: any) {
    this.params.affSub3 = affSub3;
    this.emitChange();
  }

  get affSub3() {
    return this.params.affSub3;
  }

  /**
      *
      * @param affSub4
      */
  set affSub4(affSub4: any) {
    this.params.affSub4 = affSub4;
    this.emitChange();
  }

  get affSub4() {
    return this.params.affSub4;
  }
}
