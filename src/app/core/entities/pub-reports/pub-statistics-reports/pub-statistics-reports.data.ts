
import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';
import { PubStatistic } from './pub-statistics-reports.model';

export class PubStatisticData extends BaseData<PubStatistic> {

  statuses = new StoreItem(new ResultList<Common.Status>());
  trafficTypes = new StoreItem(new ResultList<Common.TrafficType>());


  clearData() {
    this.statuses.setValue(new ResultList<Common.Status>());
    this.trafficTypes.setValue(new ResultList<Common.TrafficType>());
    super.clearData();
  }

}
