import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';
import * as moment from 'moment';
import { DataStore } from '../../data-store.service';

class PubStatisticsFilterParams extends FilterParams {
  datetimeFrom = moment().format('YYYY-MM-DD');
  datetimeTo = moment().format('YYYY-MM-DD');
  timezone: string = DataStore.timezone.current.getValue();
  hour: 1 | 0 = 0;
  date: 1 | 0 = 1;
  clicks: 1 | 0 = 1;
  backUrlClicks: 1 | 0 = 0;
  offerId: 1 | 0 = 0;
  offerName: 1 | 0 = 0;
  conversions: 1 | 0 = 1;
  countries: 1 | 0 = 0;
  'countriesCodes[]': string[] = [];
  'offersIds[]': string[] = [];
  'offersplatformNamesIds[]': string[] = [];
  cpa: 1 | 0 = 0;
  cpc: 1 | 0 = 0;
  cps: 1 | 0 = 1;
  cr: 1 | 0 = 1;
  goalName: 1 | 0 = 0;
  platforms: 1 | 0 = 0;
  revenues: 1 | 0 = 1;
  subAffId: string = null;
}

export class PubStatisticsFilter extends FilterService {

  protected params = new PubStatisticsFilterParams();

  public reset() {
    this.params = new PubStatisticsFilterParams();
  }

  /**
   *
   * @param datetimeFrom
   */
  set datetimeFrom(datetimeFrom: string) {
    this.params.datetimeFrom = datetimeFrom;
    this.emitChange();
  }

  get datetimeFrom() {
    return this.params.datetimeFrom;
  }

  /**
  *
  * @param datetimeTo
  */
  set datetimeTo(datetimeTo: string) {
    this.params.datetimeTo = datetimeTo;
    this.emitChange();
  }

  get datetimeTo() {
    return this.params.datetimeTo;
  }

  /**
      *
      * @param date
  */
  set date(date: 1 | 0 ) {
    this.params.date = date;
    this.emitChange();
  }

  get date() {
    return this.params.date;
  }

  /**
      *
      * @param clicks
      */
  set clicks(clicks: 1 | 0 ) {
    this.params.clicks = clicks;
    this.emitChange();
  }

  get clicks() {
    return this.params.clicks;
  }

  /**
      *
      * @param countries
      */
  set countries(countries: any) {
    this.params.countries = countries;
    this.emitChange();
  }

  get countries() {
    return this.params.countries;
  }

  /**
      *
      * @param timezone
      */
  set timezone(timezone: string) {
    this.params.timezone = timezone;
    this.emitChange();
  }

  get timezone() {
    return this.params.timezone;
  }

  /**
      *
      * @param hour
      */
  set hour(hour: 1 | 0 ) {
    this.params.hour = hour;
    this.emitChange();
  }

  get hour() {
    return this.params.hour;
  }

  /**
      *
      * @param backUrlClicks
      */
  set backUrlClicks(backUrlClicks: any) {
    this.params.backUrlClicks = backUrlClicks;
    this.emitChange();
  }

  get backUrlClicks() {
    return this.params.backUrlClicks;
  }

  /**
      *
      * @param offerId
      */
  set offerId(offerId: any) {
    this.params.offerId = offerId;
    this.emitChange();
  }

  get offerId() {
    return this.params.offerId;
  }
  /**
      *
      * @param offerName
      */
  set offerName(offerName: any) {
    this.params.offerName = offerName;
    this.emitChange();
  }

  get offerName() {
    return this.params.offerName;
  }

  /**
      *
      * @param conversions
      */
  set conversions(conversions: any) {
    this.params.conversions = conversions;
    this.emitChange();
  }

  get conversions() {
    return this.params.conversions;
  }

  /**
      *
      * @param cpa
      */
  set cpa(cpa: any) {
    this.params.cpa = cpa;
    this.emitChange();
  }

  get cpa() {
    return this.params.cpa;
  }

  /**
      *
      * @param cpc
      */
  set cpc(cpc: any) {
    this.params.cpc = cpc;
    this.emitChange();
  }

  get cpc() {
    return this.params.cpc;
  }

  /**
      *
      * @param cps
      */
  set cps(cps: any) {
    this.params.cps = cps;
    this.emitChange();
  }

  get cps() {
    return this.params.cps;
  }

  /**
      *
      * @param cr
      */
  set cr(cr: any) {
    this.params.cr = cr;
    this.emitChange();
  }

  get cr() {
    return this.params.cr;
  }

  /**
      *
      * @param goalName
      */
  set goalName(goalName: any) {
    this.params.goalName = goalName;
    this.emitChange();
  }

  get goalName() {
    return this.params.goalName;
  }

  /**
      *
      * @param platforms
      */
  set platforms(platforms: any) {
    this.params.platforms = platforms;
    this.emitChange();
  }

  get platforms() {
    return this.params.platforms;
  }

  /**
      *
      * @param revenues
      */
  set revenues(revenues: any) {
    this.params.revenues = revenues;
    this.emitChange();
  }

  get revenues() {
    return this.params.revenues;
  }

  /**
      *
      * @param subAffId
      */
  set subAffId(subAffId: any) {
    this.params.subAffId = subAffId;
    this.emitChange();
  }

  get subAffId() {
    return this.params.subAffId;
  }

  /**
      *
      * @param countriesCodes
      */
  set countriesCodes(countriesCodes: string[]) {
    this.params['countriesCodes[]'] = countriesCodes;
    this.emitChange();
  }

  get countriesCodes() {
    return this.params['countriesCodes[]'];
  }

  /**
  *
  * @param offersIds
  */
  set offersIds(offersIds: string[]) {
    this.params['offersIds[]'] = offersIds;
    this.emitChange();
  }

  get offersIds() {
    return this.params['offersIds[]'];
  }

  /**
 *
 * @param platformNames
 */
  set platformNames(platformNames: string[]) {
    this.params['platformNames[]'] = platformNames;
    this.emitChange();
  }

  get platformNames() {
    return this.params['platformNames[]'];
  }

}
