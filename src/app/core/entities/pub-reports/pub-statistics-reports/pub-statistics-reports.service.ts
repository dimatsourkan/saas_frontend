import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Crud } from '@app/core/decorators/crud.decorator';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';

import { DataStoreHelper } from '../../data-store.helper';
import { DataStore } from '../../data-store.service';
import { PubStatisticData } from './pub-statistics-reports.data';
import { PubStatistic } from './pub-statistics-reports.model';

@Injectable({
  providedIn: 'root'
})
@Crud('statistics', PubStatistic, DataStore.pubStatistic)
export class PubStatisticsService extends CRUDService<PubStatistic, PubStatisticData> {

  private trafficTypesCache$: Observable<ResultList<Common.TrafficType>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }


  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.trafficTypes.getValue().items.length) {
        this.getTrafficTypes(true).subscribe();
      }
    });
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getTrafficTypes(withoutCache = false) {
    if (!this.trafficTypesCache$ || withoutCache) {
      this.trafficTypesCache$ = this.httpClient.get(this.getUrl(`../offer/traffic_types`), { headers: { toCamelCase: 'true' } })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.TrafficType>(Common.TrafficType), { items: data } as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'trafficTypes', res)),
          shareReplay(1)
        );
    }
    return this.trafficTypesCache$;
  }

}
