import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';

class PubGroupUrlsFilterParams extends FilterParams {
  publisherId: string = null;
}

export class PubGroupUrlsFilter extends FilterService {

  protected params = new PubGroupUrlsFilterParams();

  set publisherId(publisherId: string) {
    this.params.publisherId = publisherId;
    this.emitChange();
  }

  get publisherId() {
    return this.params.publisherId;
  }

  public reset() {
    this.params = new PubGroupUrlsFilterParams();
  }

}
