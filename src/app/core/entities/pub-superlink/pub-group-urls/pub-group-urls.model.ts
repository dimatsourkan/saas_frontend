
import { Type, Expose } from 'class-transformer';
import { BaseModel } from '@app/core/models/base-model/base.model';
import { Common } from '@app/core/models/common-model/common.model';


export class PubGroupUrl extends BaseModel {

  @Type(() => Common.User)
  publisher = new Common.User();

  name: string = '';
  url: string = '';
  distribution: number = null;
  dynamicType: number = null;
  contentType: number = null;
  trafficType: number = null;
  monetizationModel: number = null;

  @Expose({ name: 'publisherId' })
  get publisherId() {
    return this.publisher.id;
  }
}
