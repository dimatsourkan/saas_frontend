import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';

import { Crud } from '@app/core/decorators/crud.decorator';
import { DataStore } from '../../data-store.service';
import { PubGroupUrl } from './pub-group-urls.model';
import { PubGroupUrlData } from './pub-group-urls.data';


@Injectable({
  providedIn: 'root'
})
@Crud('superlink/group', PubGroupUrl, DataStore.superlinkPubGroupUrls)
export class PubGroupUrlService extends CRUDService<PubGroupUrl, PubGroupUrlData> {

  private modeList$: Observable<ResultList<Common.Status>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.modeList.getValue().items.length) {
        this.getModeList(true).subscribe();
      }
    });
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getModeList(withoutCache = false) {
    if (!this.modeList$ || withoutCache) {
      this.modeList$ = this.httpClient.get(this.getUrl('mode_list'), { headers: this.headers })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'modeList', res)),
          shareReplay(1)
        );
    }
    return this.modeList$;
  }

}
