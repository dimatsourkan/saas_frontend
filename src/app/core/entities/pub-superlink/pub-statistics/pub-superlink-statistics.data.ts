import { BaseData } from '@app/core/models/base-data/base.data';
import { PubSuperlinkStatistic } from './pub-superlink-statistics.model';


export class PubSuperlinkStatisticData extends BaseData<PubSuperlinkStatistic> {

  clearData() {
    super.clearData();
  }

}
