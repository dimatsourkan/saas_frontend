import { TestBed } from '@angular/core/testing';
import { PublisherData } from '@app/core/entities/publisher/publisher.data';

import { ResultList } from '@app/core/models/result-model/result.model';

describe('PublisherData', () => {

  let service: PublisherData;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = new PublisherData();
  });

  /**
   *
   */
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   *
   */
  it('should be set statuses', () => {
    const statuses = (new ResultList<any>()).update({
      items: [{a: 1}]
    });

    service.statuses.setValue(statuses);
    expect(service.statuses.getValue()).toEqual(statuses);
    service.statuses.asObservable$.subscribe(res => {
      expect(res).toEqual(statuses);
    });
  });
});
