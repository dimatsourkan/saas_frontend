import { Publisher } from '@app/core/entities/publisher/publisher.model';
import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';

export class PublisherData extends BaseData<Publisher> {

  statuses = new StoreItem(new ResultList<Common.Status>(Common.Status));
  redirectTypes = new StoreItem(new ResultList<Common.RedirectType>(Common.RedirectType));
  paymentTerms = new StoreItem(new ResultList<Common.Status>(Common.Status));
  paymentTermsSuperlink = new StoreItem(new ResultList<Common.Status>(Common.Status));

  clearData() {
    this.statuses.setValue(new ResultList<Common.Status>(Common.Status));
    this.redirectTypes.setValue(new ResultList<Common.RedirectType>(Common.RedirectType));
    this.paymentTerms.setValue(new ResultList<Common.Status>(Common.Status));
    this.paymentTermsSuperlink.setValue(new ResultList<Common.Status>(Common.Status));

    super.clearData();
  }

}
