export enum PUB_STATUS {
  PENDING = 0,
  ACTIVE  = 1,
  BANNED  = 2,
}

export type pubStatusType = 0 | 1 | 2;
