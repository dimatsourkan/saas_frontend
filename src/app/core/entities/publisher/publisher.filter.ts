import { FilterParams, FilterService } from '../../services/filter/filter.service';

class PublisherFilterParams extends FilterParams {
  status: number = null;
  'managersIds[]' = [];
  antiFraud = null;
  badTraffic = null;
  externalId = null;
  username = null;
  email = null;
  skype = null;
  firstName = null;
  lastName = null;
  address = null;
  'countriesCodes[]' = [];
  zipcode = null;
  phone = null;
  notes = null;
  postbackUrl = null;
  paymentTerms = null;
  paymentTermsSuperlink = null;
}

export class PublisherFilter extends FilterService {

  protected params = new PublisherFilterParams();

  /**
   *
   * @param status
   */
  set status(status: number) {
    this.params.status = status;
    this.emitChange();
  }

  get status() {
    return this.params.status;
  }

  /**
   *
   * @param managersIds
   */
  set managersIds(managersIds: string[]) {
    this.params['managersIds[]'] = managersIds;
    this.emitChange();
  }

  get managersIds() {
    return this.params['managersIds[]'];
  }

  /**
   *
   * @param antiFraud
   */
  set antiFraud(antiFraud: string) {
    this.params.antiFraud = antiFraud;
    this.emitChange();
  }

  get antiFraud() {
    return this.params.antiFraud;
  }

  /**
   *
   * @param badTraffic
   */
  set badTraffic(badTraffic: string) {
    this.params.badTraffic = badTraffic;
    this.emitChange();
  }

  get badTraffic() {
    return this.params.badTraffic;
  }

  /**
   *
   * @param externalId
   */
  set externalId(externalId: string) {
    this.params.externalId = externalId;
    this.emitChange();
  }

  get externalId() {
    return this.params.externalId;
  }

  /**
   *
   * @param username
   */
  set username(username: string) {
    this.params.username = username;
    this.emitChange();
  }

  get username() {
    return this.params.username;
  }

  /**
   *
   * @param email
   */
  set email(email: string) {
    this.params.email = email;
    this.emitChange();
  }

  get email() {
    return this.params.email;
  }

  /**
   *
   * @param skype
   */
  set skype(skype: string) {
    this.params.skype = skype;
    this.emitChange();
  }

  get skype() {
    return this.params.skype;
  }

  /**
   *
   * @param firstName
   */
  set firstName(firstName: string) {
    this.params.firstName = firstName;
    this.emitChange();
  }

  get firstName() {
    return this.params.firstName;
  }

  /**
   *
   * @param lastName
   */
  set lastName(lastName: string) {
    this.params.lastName = lastName;
    this.emitChange();
  }

  get lastName() {
    return this.params.lastName;
  }

  /**
   *
   * @param address
   */
  set address(address: string) {
    this.params.address = address;
    this.emitChange();
  }

  get address() {
    return this.params.address;
  }

  /**
   *
   * @param countriesCodes
   */
  set countriesCodes(countriesCodes: string[]) {
    this.params['countriesCodes[]'] = countriesCodes;
    this.emitChange();
  }

  get countriesCodes() {
    return this.params['countriesCodes[]'];
  }

  /**
   *
   * @param zipcode
   */
  set zipcode(zipcode: string) {
    this.params.zipcode = zipcode;
    this.emitChange();
  }

  get zipcode() {
    return this.params.zipcode;
  }

  /**
   *
   * @param phone
   */
  set phone(phone: string) {
    this.params.phone = phone;
    this.emitChange();
  }

  get phone() {
    return this.params.phone;
  }

  /**
   *
   * @param notes
   */
  set notes(notes: string) {
    this.params.notes = notes;
    this.emitChange();
  }

  get notes() {
    return this.params.notes;
  }

  /**
   *
   * @param postbackUrl
   */
  set postbackUrl(postbackUrl: string) {
    this.params.postbackUrl = postbackUrl;
    this.emitChange();
  }

  get postbackUrl() {
    return this.params.postbackUrl;
  }

  /**
   *
   * @param paymentTerms
   */
  set paymentTerms(paymentTerms: string) {
    this.params.paymentTerms = paymentTerms;
    this.emitChange();
  }

  get paymentTerms() {
    return this.params.paymentTerms;
  }

  /**
   *
   * @param paymentTermsSuperlink
   */
  set paymentTermsSuperlink(paymentTermsSuperlink: string) {
    this.params.paymentTermsSuperlink = paymentTermsSuperlink;
    this.emitChange();
  }

  get paymentTermsSuperlink() {
    return this.params.paymentTermsSuperlink;
  }


  public reset() {
    this.params = new PublisherFilterParams();
  }

}
