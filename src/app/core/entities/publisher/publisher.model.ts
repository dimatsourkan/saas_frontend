import { PUB_STATUS, pubStatusType } from '@app/core/entities/publisher/publisher.enum';
import { Common } from '@app/core/models/common-model/common.model';
import { Expose, Type } from 'class-transformer';
import { BaseModel } from '../../models/base-model/base.model';

export namespace Pub {

  export class Counter {
    conversion = 0;
    skipConversion = 0;
  }

  export class Resale {
    enabled = false;
    percent = 0;
  }

  export class RequestLimit {
    offerRequestLeft = null;
    offerRequestLimit = null;
  }

  export class PaymentInfo {
    bankAddress = '';
    bankName = '';
    beneficiaryAccount = '';
    beneficiaryAddress = '';
    beneficiaryName = '';
    iban = '';
    routing = '';
    swift = '';
    wmz = '';
    epaymentsId = null;
    paypalEmail = null;
  }

  export class EventPostback {
    goalId: number;
    Url: string;
  }

  export class OfferPostback {
    offerGoalId: string;
    offerId: string;
    url: string;
  }

  export class Check extends BaseModel {
    affId = '';
    affSub = '';
    affSub2 = '';
    affSub3 = '';
    affSub4 = '';
    clickId = '';
    offerId = '';
    postbackCode = '';
    redirect = '';
    redirects: string[] = [];
    response = '';
    send = '';
    status = '';
    statusMessage = '';
  }
}

export class Publisher extends BaseModel {

  @Type(() => Common.Address)
  address = new Common.Address();

  @Type(() => Pub.Counter)
  counter = new Pub.Counter();

  @Type(() => Common.IpWhitelist)
  ipWhitelist = new Common.IpWhitelist();

  @Type(() => Common.Manager)
  manager = new Common.Manager();

  @Type(() => Pub.Resale)
  resale = new Pub.Resale();

  @Type(() => Pub.RequestLimit)
  offerRequestLimit = new Pub.RequestLimit();

  @Type(() => Pub.PaymentInfo)
  paymentInfo = new Pub.PaymentInfo();

  @Type(() => Common.Person)
  person = new Common.Person();

  @Type(() => Pub.EventPostback)
  eventPostbacks: Pub.EventPostback[] = [];

  @Type(() => Pub.OfferPostback)
  offerPostbacks: Pub.OfferPostback[] = [];

  additionalEmails: string[] = [];
  affiliateSource = '';
  agreementFileId = '';
  invoiceNotes = '';
  isOwnPublisher: 0 | 1 = 0;
  antiFraud: 0 | 1 = 0;
  autoApprove: 0 | 1 = 0;
  badTraffic: 0 | 1 = 0;
  redirectType: 0 | 1 = 0;
  blockBadTraffic = false;
  email = '';
  externalId = '';
  notes = '';

  paymentTerms = null;
  paymentTermsSuperlink = null;
  percentPay = null;
  percentSkip = null;
  postbackSuperlinkUrl: string = null;
  postbackUrl: string = null;
  skype = '';
  status: pubStatusType = PUB_STATUS.PENDING;
  skipAffiliate = '';
  tds = 0;
  token = '';
  username = '';
  w8w9FileId = null;

  @Expose({ name: 'managerId' })
  get managerId() {
    return this.manager.id;
  }


  @Expose({ name: 'resaleEnabled' })
  get resaleEnabled() {
    return this.resale.enabled;
  }


  @Expose({ name: 'resalePercent' })
  get resalePercent() {
    return this.resale.percent;
  }

}
