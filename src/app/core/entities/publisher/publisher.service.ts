import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { DataStore } from '@app/core/entities/data-store.service';
import { PublisherData } from '@app/core/entities/publisher/publisher.data';
import { pubStatusType } from '@app/core/entities/publisher/publisher.enum';
import { parseFileDownloadResult } from '@app/core/helpers/helpers';
import { modelId } from '@app/core/models/base-model/base.model';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { CRUDService } from '../../services/crud/crud.service';
import { Pub, Publisher } from './publisher.model';

@Injectable({
  providedIn: 'root'
})
@Crud('publisher', Publisher, DataStore.publisher)
export class PublisherService extends CRUDService<Publisher, PublisherData> {

  private pubStatusesCache$: Observable<ResultList<Common.Status>>;
  private redirectTypesCache$: Observable<ResultList<Common.RedirectType>>;
  private paymentTermsSuperlinkCache$: Observable<ResultList<Common.Status>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.statuses.getValue().items.length) {
        this.getPublisherStatuses(true).subscribe();
      }
      if (this.dataStore.redirectTypes.getValue().items.length) {
        this.getRedirectTypes(true).subscribe();
      }
    });
  }

  /**
   * TODO - Нужно на сервере сделать правильный метод для получения (GET publisher)
   * GET запрос для получения списка моделей
   * @param params - Объект с гет параметрами
   * @returns {Observable<Object>}
   */
  getAll(params = new HttpParams()): Observable<ResultList<Publisher>> {
    return super.getAll(params, `${this.apiUrl}/publishers`);
  }

  /**
   * TODO - Нужно на сервере сделать правильный метод для получения (GET publisher)
   * GET запрос для получения списка моделей c параметрами id и name
   * @param params - Объект с гет параметрами
   * @returns {Observable<Object>}
   */
  getKeyList(params = new HttpParams()): Observable<ResultList<Publisher>> {
    return super.getKeyList(params, `${this.apiUrl}/publishers`);
  }

  /**
   * TODO - Нужно на сервере сделать правильный метод для получения (POST publisher)
   * Сохранение новой модели
   * @param data - Данные модели
   * @returns {Observable<Object>}
   */
  save(data: Publisher): Observable<Publisher> {
    return super.save(data, this.getUrl('create'));
  }

  /**
   * GET запрос для получения списка моделей c параметрами id и name
   * @param id - ID
   * @param params - Объект с гет параметрами
   * @returns {Observable<Object>}
   */
  getApprovedPublishers(id: modelId, params = new HttpParams()): Observable<any> {
    return super.getKeyList(params, `${this.apiUrl}/offer/${id}/approvedPublishers`);
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getPublisherStatuses(withoutCache = false) {
    if (!this.pubStatusesCache$ || withoutCache) {
      this.pubStatusesCache$ = this.httpClient.get(this.getUrl(`statuses`), { headers: this.headers })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), data as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'statuses', res)),
          shareReplay(1)
        );
    }
    return this.pubStatusesCache$;
  }

  /**
  *
  * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
  */
  getRedirectTypes(withoutCache = false) {
    if (!this.redirectTypesCache$ || withoutCache) {
      this.redirectTypesCache$ = this.httpClient.get(this.getUrl(`redirect_types`), { headers: this.headers })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.RedirectType>(Common.RedirectType), data as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'redirectTypes', res)),
          shareReplay(1)
        );
    }
    return this.redirectTypesCache$;
  }

  /**
// *
// * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
// */
//   getPaymentTerms(withoutCache = false) {
//     if (!this.paymentTermsCache$ || withoutCache) {
//       this.paymentTermsCache$ = this.httpClient.get(this.getUrl(`payment_terms`), { headers: this.headers })
//         .pipe(
//           map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), data as Object)),
//           tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'paymentTerms', res)),
//           shareReplay(1)
//         );
//     }
//     return this.paymentTermsCache$;
//   }


//   /**
// *
// * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
// */
//   getPaymentTermsSuperlink(withoutCache = false) {
//     if (!this.paymentTermsSuperlinkCache$ || withoutCache) {
//       this.paymentTermsSuperlinkCache$ = this.httpClient.get(this.getUrl(`payment_terms_superlink`), { headers: this.headers })
//         .pipe(
//           map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), data as Object)),
//           tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'paymentTerms', res)),
//           shareReplay(1)
//         );
//     }
//     return this.paymentTermsSuperlinkCache$;
//   }


  getPaymentTerms(params = new HttpParams()): Observable<ResultList<any>> {
    return this.httpClient
    .get(this.getUrl('payment_terms'), {headers: this.headers, params}).pipe(
      map((res) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), res as Object)),
      tap((res) => DataStoreHelper.setDataKeyList(this.dataStore, res))
    );
  }

  getPaymentTermsSuperlink(params = new HttpParams()): Observable<ResultList<any>> {
    return this.httpClient
    .get(this.getUrl('payment_terms_superlink'), {headers: this.headers, params}).pipe(
      map((res) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), res as Object)),
      tap((res) => DataStoreHelper.setDataCustom(this.dataStore, 'paymentTermsSuperlink', res))
    );
  }


  /**
   *
   * @param id - Publisher id
   * @param status - Publisher status
   */
  changeStatus(id: modelId, status: pubStatusType): Observable<any> {
    return this.httpClient.put(this.getUrl(`${id}/status`), { id, status })
      .pipe(tap(() => {
        const pub = DataStoreHelper.getItemByIdFromList(this.dataStore.list.getValue().items, id);
        if (pub) {
          pub.status = status;
          DataStoreHelper.updateItemInList(pub, this.dataStore);
        }
      }));
  }

  /**
   *
   * @param id - Publisher id
   * @param url - url string
   * @param offerId - Offer id
   */
  submitCheckForm(id: modelId, url: string, offerId: string): Observable<any> {
    return this.httpClient.post(this.getUrl(`${id}/check`), { url, offerId }, { headers: this.headers })
      .pipe(map((data) => Pub.Check.plainToClass(data)));
  }

  /**
   *
   * @param id - Publisher id
   * @param dateFrom - date string
   * @param dateTo - date string
   */
  removeApprovals(id: modelId, dateFrom: string, dateTo: string): Observable<any> {
    return this.httpClient.put(this.getUrl(`${id}/removeApprovals`), { dateFrom, dateTo });
  }

  /**
   *
   */
  getIsCheckerEnabled(): Observable<{ enabled: boolean }> {
    return this.httpClient.get<{ enabled: boolean  }>(`${this.apiUrlService.apiUrl()}/checker_status`);
  }

  /**
   *
   * @param id - Publisher id
   */
  getPublisherToken(id: modelId): Observable<{ token: string }> {
    return this.httpClient.get<{ token: string }>(this.getUrl(`${id}/login`));
  }

  /**
   *
   * @param id - Publisher id
   */
  downloadAgreement(id: modelId): Observable<any> {
    return this.httpClient.get(this.getUrl(`${id}/agreement`), {
      responseType: 'blob',
      observe: 'response'
    }).pipe(map(res => parseFileDownloadResult(res)));
  }

  /**
   *
   * @param id - Publisher id
   * @param data - File data
   */
  saveAgreementToServer(id: modelId, data: any): Observable<any> {
    return this.httpClient.post(this.getUrl(`${id}/agreement`), data, { headers: this.headers });
  }

  /**
   *
   * @param id - Publisher id
   */
  removeAgreement(id: modelId): Observable<any> {
    return this.httpClient.delete(this.getUrl(`${id}/agreement`));
  }

  /**
   *
   * @param id - Publisher id
   */
  downloadW8W9(id: modelId): Observable<any> {
    return this.httpClient.get(this.getUrl(`${id}/w8w9`), {
      responseType: 'blob',
      observe: 'response'
    }).pipe(map(res => parseFileDownloadResult(res)));
  }

  /**
   *
   * @param id - Publisher id
   * @param data - File data
   */
  saveW8W9FileIdToServer(id: modelId, data: any): Observable<any> {
    return this.httpClient.post(this.getUrl(`${id}/w8w9`), data, { headers: this.headers });
  }

  /**
   *
   * @param id - Publisher id
   */
  removeW8W9(id: modelId): Observable<any> {
    return this.httpClient.delete(this.getUrl(`${id}/w8w9`));
  }


}
