
import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';
import { ConversionReport, ConversionReportListData } from './conversion-reports.model';

export class ConversionReportData extends BaseData<ConversionReport, ConversionReportListData> {

  statuses = new StoreItem(new ResultList<Common.Status>());
  approveStatuses = new StoreItem(new ResultList<Common.Status>());
  isShowUniqueClick = new StoreItem();

  clearData() {
    this.statuses.setValue(new ResultList<Common.Status>());
    super.clearData();
  }

}
