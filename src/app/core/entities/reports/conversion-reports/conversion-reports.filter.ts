import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';
import * as moment from 'moment';
import { DataStore } from '../../data-store.service';

class ConversionReportFilterParams extends FilterParams {
  total_currency: string = 'USD';
  from: string = moment(new Date()).subtract(3, 'month').startOf('month').format('YYYY-MM-DD');
  to: string = moment(new Date()).format('YYYY-MM-DD');
  timezone: string = DataStore.timezone.current.getValue();
  'offers[]': string[] = [];
  'advertisers[]': string[] = [];
  'publishers[]': string[] = [];
  'countries[]': string[] = [];
  unique: 1 | 0  = null;
  'platforms[]': string[] = [];
  minFraud: 1 | 0  = null;
  approveStatus: number = null;
  'statuses[]': number[] = [];
  sent: 1 | 0  = null;
  ip: string = '';
  conversionIp: string = '';
  transactionId: string = '';
  test: 1 | 0  = null;
  duplicate: 1 | 0  = null;
  affSub: string = '';
  affSub2: string = '';
  affSub3: string = '';
  affSub4: string = '';
  advAffSub: string = '';
  advAffSub2: string = '';
  advAffSub3: string = '';
  advAffSub4: string = '';
}

export class ConversionReportFilter extends FilterService {

  protected params = new ConversionReportFilterParams();

  public reset() {
    this.params = new ConversionReportFilterParams();
  }

  /**
    *
    * @param timezone
    */
  set timezone(timezone: string) {
    this.params.timezone = timezone;
    this.emitChange();
  }

  get timezone() {
    return this.params.timezone;
  }

  /**
      *
      * @param from
      */
  set from(from: string) {
    this.params.from = from;
    this.emitChange();
  }

  get from() {
    return this.params.from;
  }

  /**
      *
      * @param to
      */
  set to(to: string) {
    this.params.to = to;
    this.emitChange();
  }

  get to() {
    return this.params.to;
  }

  /**
      *
      * @param sent
      */
  set sent(sent: 1 | 0 ) {
    this.params.sent = sent;
    this.emitChange();
  }

  get sent() {
    return this.params.sent;
  }

  /**
      *
      * @param total_currency
      */
  set total_currency(total_currency: string) {
    this.params.total_currency = total_currency;
    this.emitChange();
  }

  get total_currency() {
    return this.params.total_currency;
  }

  /**
      *
      * @param unique
      */
  set unique(unique: 1 | 0 ) {
    this.params.unique = unique;
    this.emitChange();
  }

  get unique() {
    return this.params.unique;
  }

  /**
      *
      * @param minFraud
      */
  set minFraud(minFraud: 1 | 0 ) {
    this.params.minFraud = minFraud;
    this.emitChange();
  }

  get minFraud() {
    return this.params.minFraud;
  }

  /**
      *
      * @param approveStatus
      */
  set approveStatus(approveStatus: number) {
    this.params.approveStatus = approveStatus;
    this.emitChange();
  }

  get approveStatus() {
    return this.params.approveStatus;
  }

  /**
      *
      * @param ip
      */
  set ip(ip: string) {
    this.params.ip = ip;
    this.emitChange();
  }

  get ip() {
    return this.params.ip;
  }

  /**
      *
      * @param conversionIp
      */
  set conversionIp(conversionIp: string) {
    this.params.conversionIp = conversionIp;
    this.emitChange();
  }

  get conversionIp() {
    return this.params.conversionIp;
  }

  /**
      *
      * @param transactionId
      */
  set transactionId(transactionId: string) {
    this.params.transactionId = transactionId;
    this.emitChange();
  }

  get transactionId() {
    return this.params.transactionId;
  }

  /**
      *
      * @param test
      */
  set test(test: 1 | 0 ) {
    this.params.test = test;
    this.emitChange();
  }

  get test() {
    return this.params.test;
  }

  /**
      *
      * @param duplicate
      */
  set duplicate(duplicate: 1 | 0 ) {
    this.params.duplicate = duplicate;
    this.emitChange();
  }

  get duplicate() {
    return this.params.duplicate;
  }

  /**
      *
      * @param affSub
      */
  set affSub(affSub: string) {
    this.params.affSub = affSub;
    this.emitChange();
  }

  get affSub() {
    return this.params.affSub;
  }

  /**
      *
      * @param affSub2
      */
  set affSub2(affSub2: string) {
    this.params.affSub2 = affSub2;
    this.emitChange();
  }

  get affSub2() {
    return this.params.affSub2;
  }

  /**
      *
      * @param affSub3
      */
  set affSub3(affSub3: string) {
    this.params.affSub3 = affSub3;
    this.emitChange();
  }

  get affSub3() {
    return this.params.affSub3;
  }

  /**
      *
      * @param affSub4
      */
  set affSub4(affSub4: string) {
    this.params.affSub4 = affSub4;
    this.emitChange();
  }

  get affSub4() {
    return this.params.affSub4;
  }

  /**
      *
      * @param advAffSub
      */
  set advAffSub(advAffSub: string) {
    this.params.advAffSub = advAffSub;
    this.emitChange();
  }

  get advAffSub() {
    return this.params.advAffSub;
  }

  /**
      *
      * @param advAffSub2
      */
  set advAffSub2(advAffSub2: string) {
    this.params.advAffSub2 = advAffSub2;
    this.emitChange();
  }

  get advAffSub2() {
    return this.params.advAffSub2;
  }

  /**
      *
      * @param advAffSub3
      */
  set advAffSub3(advAffSub3: string) {
    this.params.advAffSub3 = advAffSub3;
    this.emitChange();
  }

  get advAffSub3() {
    return this.params.advAffSub3;
  }

  /**
      *
      * @param advAffSub4
      */
  set advAffSub4(advAffSub4: string) {
    this.params.advAffSub4 = advAffSub4;
    this.emitChange();
  }

  get advAffSub4() {
    return this.params.advAffSub4;
  }

  /**
      *
      * @param offers
      */
  set offers(offers: string[]) {
    this.params['offers[]'] = offers;
    this.emitChange();
  }

  get offers() {
    return this.params['offers[]'];
  }

  /**
      *
      * @param advertisers
      */
  set advertisers(advertisers: string[]) {
    this.params['advertisers[]'] = advertisers;
    this.emitChange();
  }

  get advertisers() {
    return this.params['advertisers[]'];
  }

  /**
      *
      * @param publishers
      */
  set publishers(publishers: string[]) {
    this.params['publishers[]'] = publishers;
    this.emitChange();
  }

  get publishers() {
    return this.params['publishers[]'];
  }

  /**
      *
      * @param countries
      */
  set countries(countries: string[]) {
    this.params['countries[]'] = countries;
    this.emitChange();
  }

  get countries() {
    return this.params['countries[]'];
  }

  /**
      *
      * @param platforms
      */
  set platforms(platforms: string[]) {
    this.params['platforms[]'] = platforms;
    this.emitChange();
  }

  get platforms() {
    return this.params['platforms[]'];
  }

  /**
      *
      * @param statuses
      */
  set statuses(statuses: number[]) {
    this.params['statuses[]'] = statuses;
    this.emitChange();
  }

  get statuses() {
    return this.params['statuses[]'];
  }

}
