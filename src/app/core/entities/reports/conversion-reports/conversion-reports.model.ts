import { BaseModel, modelId } from '@app/core/models/base-model/base.model';

export class ConversionReport extends BaseModel {

    backUrlClicks: string = '';
    clicks: string = '';
    conversions: string = '';
    costs: number = null;
    cr: number = null;
    currency: string = '';
    date: string = '';
    minFraudPercent: number = null;
    offerId: string = '';
    offerName: string = '';
    offerRealId: string = '';
    profit: number = null;
    pubConversions: string = '';
    publisherCr: number = null;
    publisherName: string = '';
    revenues: number = null;
    status: number = null;
    approveStatus: { status: number, availableStatuses: modelId[] } = { status: null, availableStatuses: [] };

}

export class ConversionReportListData {
    earn: Array<{ amount: number, currency: string }>;
    paid: Array<{ amount: number, currency: string }>;
}

export class ApproveStatus extends BaseModel {
    id: modelId = null;
    value: string = '';
}
