import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Crud } from '@app/core/decorators/crud.decorator';
import { parseFileDownloadResult } from '@app/core/helpers/helpers';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { DataStoreHelper } from '../../data-store.helper';
import { DataStore } from '../../data-store.service';
import { ConversionReportData } from './conversion-reports.data';
import { ConversionReport, ConversionReportListData } from './conversion-reports.model';

@Injectable({
  providedIn: 'root'
})
@Crud('reports/conversions', ConversionReport, DataStore.conversionReport)
export class ConversionReportService extends CRUDService<ConversionReport, ConversionReportData, ConversionReportListData> {

  private statusesCache$: Observable<ResultList<Common.Status>>;
  private approveStatuses$: Observable<ResultList<Common.Status>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }


  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.statuses.getValue().items.length) {
        this.getStatuses(true).subscribe();
      }
      if (this.dataStore.approveStatuses.getValue().items.length) {
        this.getApproveStatuses(true).subscribe();
      }
    });
  }

  getAll(params = new HttpParams()) {
    return super.getAll(params, this.getUrl('/'));
  }

  isShowUniqueClick() {
    return this.httpClient.get(`${this.apiUrl}/settings/unique_clicks_enabled`, { headers: this.headers })
      .pipe(
        map((data: { enabled: boolean  }) => plainToClassFromExist(Boolean, data.enabled as Boolean)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'isShowUniqueClick', res))
      );
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getStatuses(withoutCache = false) {
    if (!this.statusesCache$ || withoutCache) {
      this.statusesCache$ = this.httpClient.get(this.getUrl('statuses'), { headers: this.headers })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'statuses', res)),
          shareReplay(1)
        );
    }
    return this.statusesCache$;
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getApproveStatuses(withoutCache = false) {
    if (!this.approveStatuses$ || withoutCache) {
      this.approveStatuses$ = this.httpClient.get(this.getUrl('approve_statuses'), { headers: this.headers })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'approveStatuses', res)),
          shareReplay(1)
        );
    }
    return this.approveStatuses$;
  }


  changeStatus(id: string, status: number): Observable<any> {
    return this.httpClient.put(this.getUrl(`${id}/approve_status`), { status },
      { headers: this.headers })
      .pipe(tap((res) => {
        const index = DataStoreHelper.getIndexById<ConversionReport>(this.dataStore.list.getValue().items, id);
        if (index >= 0) {
          const report = this.dataStore.list.getValue().items[index];
          report.approveStatus.status = status;
          report.approveStatus.availableStatuses = res.body.availableStatuses;
          DataStoreHelper.updateItemInList(report, this.dataStore);
        }
      }));
  }

  sendToEmail(params = new HttpParams()): Observable<HttpResponse<any>> {
    return this.httpClient.get(this.getUrl('send'), { observe: 'response', params });
  }

  exportCSV(params = new HttpParams()) {
    return this.httpClient.get(this.getUrl('export'), {
      responseType: 'arraybuffer',
      observe: 'response', params
    }).pipe(map(res => parseFileDownloadResult(res)));
  }
}
