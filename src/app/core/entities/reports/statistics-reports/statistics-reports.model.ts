import { BaseModel } from '@app/core/models/base-model/base.model';

export class Statistic extends BaseModel {
    id: string = null;
    backUrlClicks: number = 0;
    clicks: number = 0;
    conversions: number = 0;
    costs: number = 0;
    cr: number = 0;
    currency: string = '';
    date: string = '';
    minFraudPercent: number = 0;
    offerId: string = '';
    offerName: string = '';
    offerRealId: string = '';
    profit: number = 0;
    pubConversions: number = 0;
    publisherCr: number = 0;
    publisherName: string = '';
    revenues: number = 0;
    month: number = 0;
    hour: number = 0;
    hash: string = '';
    countryCode: string = '';
    platformName: string = '';
    advertiserId: string = '';
    advertiserName: string = '';
    publisherId: string = '';
    subAffiliateId: string = '';
    publisherManagerName: string = '';
    advertiserManagerName: string = '';
    cpa: number = 0;
    publisherCpa: number = 0;
    cpc: number = 0;
    rpa: number = 0;
    rpc: number = 0;
    duplicates: number = 0;
    duplicatesPercent: number = 0;
    minFraud: number = 0;
    overPaid: number = 0;
}
