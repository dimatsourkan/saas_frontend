import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { AlgorithmUrl } from './algorithms-urls.model';

export class AlgorithmUrlData extends BaseData<AlgorithmUrl> {

  contentTypeList = new StoreItem(new ResultList<Common.Status>());
  dynamicTypeList = new StoreItem(new ResultList<Common.Status>());
  distributionList = new StoreItem(new ResultList<Common.Status>());
  monetizationList = new StoreItem(new ResultList<Common.Status>());
  trafficTypeList = new StoreItem(new ResultList<Common.Status>());

  clearData() {
    this.contentTypeList = new StoreItem(new ResultList<Common.Status>());
    this.dynamicTypeList = new StoreItem(new ResultList<Common.Status>());
    this.distributionList = new StoreItem(new ResultList<Common.Status>());
    this.monetizationList = new StoreItem(new ResultList<Common.Status>());
    this.trafficTypeList = new StoreItem(new ResultList<Common.Status>());
    super.clearData();
  }

}
