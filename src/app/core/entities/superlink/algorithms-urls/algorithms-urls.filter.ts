import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';

class AlgorithmsUrlsFilterParams extends FilterParams {

}

export class AlgorithmsUrlsFilter extends FilterService {

  protected params = new AlgorithmsUrlsFilterParams();




  public reset() {
    this.params = new AlgorithmsUrlsFilterParams();
  }

}
