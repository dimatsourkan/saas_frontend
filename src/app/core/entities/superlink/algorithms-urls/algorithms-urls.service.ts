import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Crud } from '@app/core/decorators/crud.decorator';
import { AlgorithmUrl } from './algorithms-urls.model';
import { DataStore } from '../../data-store.service';
import { AlgorithmUrlData } from './algorithms-urls.data';

@Injectable({
  providedIn: 'root'
})
@Crud('resale_url', AlgorithmUrl, DataStore.superlinkAlgorithmsUrls)
export class AlgorithmUrlService extends CRUDService<AlgorithmUrl, AlgorithmUrlData> {

  private contentTypeList$: Observable<ResultList<Common.Status>>;
  private dynamicTypeList$: Observable<ResultList<Common.Status>>;
  private distributionList$: Observable<ResultList<Common.Status>>;
  private monetizationList$: Observable<ResultList<Common.Status>>;
  private trafficTypeList$: Observable<ResultList<Common.Status>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.contentTypeList.getValue().items.length) {
        this.getContentTypeList(true).subscribe();
      }
    });
  }

  /**
   *
   * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
   */
  getContentTypeList(withoutCache = false) {
    if (!this.contentTypeList$ || withoutCache) {
      this.contentTypeList$ = this.httpClient.get(this.getUrl(`content_type_list`), { headers: this.headers })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'contentTypeList', res)),
          shareReplay(1)
        );
    }
    return this.contentTypeList$;
  }

  getDynamicTypeList(withoutCache = false) {
    if (!this.dynamicTypeList$ || withoutCache) {
      this.dynamicTypeList$ = this.httpClient.get(this.getUrl(`dynamic_type_list`), { headers: this.headers })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'dynamicTypeList', res)),
          shareReplay(1)
        );
    }
    return this.dynamicTypeList$;
  }

  getDistributionList(withoutCache = false) {
    if (!this.distributionList$ || withoutCache) {
      this.distributionList$ = this.httpClient.get(this.getUrl(`distribution_list`), { headers: this.headers })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'distributionList', res)),
          shareReplay(1)
        );
    }
    return this.distributionList$;
  }

  getMonetizationList(withoutCache = false) {
    if (!this.monetizationList$ || withoutCache) {
      this.monetizationList$ = this.httpClient.get(this.getUrl(`monetization_model_list`), { headers: this.headers })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'monetizationList', res)),
          shareReplay(1)
        );
    }
    return this.monetizationList$;
  }

  getTrafficTypeList(withoutCache = false) {
    if (!this.trafficTypeList$ || withoutCache) {
      this.trafficTypeList$ = this.httpClient.get(this.getUrl(`traffic_type_list`), { headers: this.headers })
        .pipe(
          map((data) => plainToClassFromExist(new ResultList<Common.Status>(Common.Status), { items: data } as Object)),
          tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'trafficTypeList', res)),
          shareReplay(1)
        );
    }
    return this.trafficTypeList$;
  }

}
