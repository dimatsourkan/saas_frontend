import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';

class GroupUrlsFilterParams extends FilterParams {
  publisherId: string = null;
}

export class GroupUrlsFilter extends FilterService {

  protected params = new GroupUrlsFilterParams();

  set publisherId(publisherId: string) {
    this.params.publisherId = publisherId;
    this.emitChange();
  }

  get publisherId() {
    return this.params.publisherId;
  }

  public reset() {
    this.params = new GroupUrlsFilterParams();
  }

}
