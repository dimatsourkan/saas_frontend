import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { PublisherUrl } from './publisher-urls.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';

export class PublisherUrlData extends BaseData<PublisherUrl> {


  algotithmUrlNames = new StoreItem(new ResultList<Common.Status>());

  clearData() {
    this.algotithmUrlNames = new StoreItem(new ResultList<Common.Status>());

    super.clearData();
  }

}
