import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';

class PublisherUrlFilterParams extends FilterParams {
  publisherId: string = null;
}

export class PublisherUrlFilter extends FilterService {

  protected params = new PublisherUrlFilterParams();

  set publisherId(publisherId: string) {
    this.params.publisherId = publisherId;
    this.emitChange();
  }

  get publisherId() {
    return this.params.publisherId;
  }

  public reset() {
    this.params = new PublisherUrlFilterParams();
  }

}
