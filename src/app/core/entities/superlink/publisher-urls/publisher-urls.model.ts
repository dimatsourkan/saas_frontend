
import { BaseModel } from '@app/core/models/base-model/base.model';
import { Common } from '@app/core/models/common-model/common.model';
import { Type, Expose } from 'class-transformer';


export class PublisherUrl extends BaseModel {

  @Type(() => Common.User)
  publisher = new Common.User();
  @Type(() => Common.User)
  resaleUrl = new Common.User();
  name: string = 'Algorithm #';

  @Expose({ name: 'publisherId' })
  get publisherId() {
    return this.publisher.id;
  }

  @Expose({ name: 'resaleUrlId' })
  get resaleUrlId() {
    return this.resaleUrl.id;
  }
}
