import { Injectable } from '@angular/core';

import { Crud } from '@app/core/decorators/crud.decorator';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { DataStore } from '../../data-store.service';
import { PublisherUrlData } from './publisher-urls.data';
import { PublisherUrl } from './publisher-urls.model';
import { HttpClient } from '@angular/common/http';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { Observable } from 'rxjs';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';
import { plainToClassFromExist } from 'class-transformer';
import { DataStoreHelper } from '../../data-store.helper';
import { shareReplay, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
@Crud('publisher_resale_url', PublisherUrl, DataStore.superlinkPublisherUrls)
export class PublisherUrlService extends CRUDService<PublisherUrl, PublisherUrlData> {

  private algotithmUrlNamesCache$: Observable<ResultList<PublisherUrl>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService,
    protected languageService: LanguageService,
  ) {
    super(httpClient, apiUrlService);
    this.languageService.getLang().subscribe(() => {
      if (this.dataStore.algotithmUrlNames.getValue().items.length) {
        this.getAlgotithmUrlNames(true).subscribe();
      }
    });
  }

 /**
 *
 * @param withoutCache - Если false то данные из кеша, если true то данные с сервера
 */
getAlgotithmUrlNames(withoutCache = false) {
  if (!this.algotithmUrlNamesCache$ || withoutCache) {
    this.algotithmUrlNamesCache$ = this.httpClient.get(`${this.apiUrl}/resale_url/key_list`, {
      headers: this.headers,
    })
      .pipe(
        map((data) => plainToClassFromExist(new ResultList<PublisherUrl>(PublisherUrl), data as Object)),
        tap((res: any) => DataStoreHelper.setDataCustom(this.dataStore, 'algotithmUrlNames', res)),
        shareReplay(1)
      );
  }
  return this.algotithmUrlNamesCache$;
}

}
