import { BaseData } from '@app/core/models/base-data/base.data';
import { SuperlinkConversionStatistic } from './superlink-conversion-statistics.model';


export class SuperlinkConversionStatisticData extends BaseData<SuperlinkConversionStatistic> {

  clearData() {
    super.clearData();
  }

}
