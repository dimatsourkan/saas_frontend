import { HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Crud } from '@app/core/decorators/crud.decorator';
import { DataStore } from '@app/core/entities/data-store.service';
import { parseFileDownloadResult } from '@app/core/helpers/helpers';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SuperlinkConversionStatisticData } from './superlink-conversion-statistics.data';
import { SuperlinkConversionStatistic } from './superlink-conversion-statistics.model';


@Injectable({
  providedIn: 'root'
})
@Crud('reports/conversions/superlink', SuperlinkConversionStatistic, DataStore.superlinkConversionStatistic)
export class SuperlinkConversionStatisticService extends CRUDService<SuperlinkConversionStatistic, SuperlinkConversionStatisticData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  sendToEmail(params = new HttpParams()): Observable<HttpResponse<any>> {
    return this.httpClient.get(this.getUrl('send'), {observe: 'response', params});
  }

  exportCSV(params = new HttpParams()) {
    return this.httpClient.get(this.getUrl('export'), {
      responseType: 'arraybuffer',
      observe: 'response', params
    }).pipe(map(res => parseFileDownloadResult(res)));
  }

}
