import { BaseData } from '@app/core/models/base-data/base.data';
import { SuperlinkStatistic } from './superlink-statistics.model';


export class SuperlinkStatisticData extends BaseData<SuperlinkStatistic> {

  clearData() {
    super.clearData();
  }

}
