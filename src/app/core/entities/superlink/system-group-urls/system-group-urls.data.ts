import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { SuperlinkSystemGroupUrl } from './system-group-urls.model';

export class SuperlinkSystemGroupUrlData extends BaseData<SuperlinkSystemGroupUrl> {

  modeList = new StoreItem(new ResultList<Common.Status>());

  clearData() {
    this.modeList = new StoreItem(new ResultList<Common.Status>());
    super.clearData();
  }

}
