import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';

class SuperlinkSystemGroupUrlsFilterParams extends FilterParams {
  publisherId: string = null;
}

export class SuperlinkSystemGroupUrlsFilter extends FilterService {

  protected params = new SuperlinkSystemGroupUrlsFilterParams();

  set publisherId(publisherId: string) {
    this.params.publisherId = publisherId;
    this.emitChange();
  }

  get publisherId() {
    return this.params.publisherId;
  }

  public reset() {
    this.params = new SuperlinkSystemGroupUrlsFilterParams();
  }

}
