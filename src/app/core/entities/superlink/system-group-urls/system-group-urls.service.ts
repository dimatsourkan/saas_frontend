import { Injectable } from '@angular/core';
import { Crud } from '@app/core/decorators/crud.decorator';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { DataStore } from '../../data-store.service';
import { SuperlinkSystemGroupUrlData } from './system-group-urls.data';
import { SuperlinkSystemGroupUrl } from './system-group-urls.model';


@Injectable({
  providedIn: 'root'
})
@Crud('system_group', SuperlinkSystemGroupUrl, DataStore.superlinkSystemGroupUrlData)
export class SystemGroupUrlService extends CRUDService<SuperlinkSystemGroupUrl, SuperlinkSystemGroupUrlData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

}
