import { BaseData } from '@app/core/models/base-data/base.data';
import { SuperlinkSystemConversionStatistic } from './superlink-system-conversion-statistics.model';


export class SuperlinkSystemConversionStatisticData extends BaseData<SuperlinkSystemConversionStatistic> {

  clearData() {
    super.clearData();
  }

}
