import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';
import * as moment from 'moment';
import { DataStore } from '@app/core/entities/data-store.service';


class SuperlinkSystemConversionStatisticFilterParams extends FilterParams {
  date: string = '';
  timezone: string = DataStore.timezone.current.getValue();
  from: string = moment().subtract(3, 'month').startOf('month').format('YYYY-MM-DD');
  to: string = moment().format('YYYY-MM-DD');
  'advertisers[]': string[] = [];
  'publishers[]': string[] = [];
  'countries[]': string[] = [];
  'platforms[]': string[] = [];
  'offers_ids[]': string[] = [];
  'offers[]': string[] = [];
  ip: string = '';
  conversionIp: string = '';
  transactionId: string = '';
  duplicate: 1 | 0  = null;
  affSub: string = '';
  affSub2: string = '';
  affSub3: string = '';
  affSub4: string = '';
}

export class SuperlinkSystemConversionStatisticFilter extends FilterService {

  protected params = new SuperlinkSystemConversionStatisticFilterParams();

  /**
       *
       * @param timezone
       */
  set timezone(timezone: string) {
    this.params.timezone = timezone;
    this.emitChange();
  }

  get timezone() {
    return this.params.timezone;
  }

  /**
      *
      * @param from
      */
  set from(from: string) {
    this.params.from = from;
    this.emitChange();
  }

  get from() {
    return this.params.from;
  }

  /**
      *
      * @param to
      */
  set to(to: string) {
    this.params.to = to;
    this.emitChange();
  }

  get to() {
    return this.params.to;
  }


  /**
      *
      * @param ip
      */
  set ip(ip: string) {
    this.params.ip = ip;
    this.emitChange();
  }

  get ip() {
    return this.params.ip;
  }

  /**
      *
      * @param conversionIp
      */
  set conversionIp(conversionIp: string) {
    this.params.conversionIp = conversionIp;
    this.emitChange();
  }

  get conversionIp() {
    return this.params.conversionIp;
  }

  /**
      *
      * @param transactionId
      */
  set transactionId(transactionId: string) {
    this.params.transactionId = transactionId;
    this.emitChange();
  }

  get transactionId() {
    return this.params.transactionId;
  }

  /**
      *
      * @param duplicate
      */
  set duplicate(duplicate: 1 | 0 ) {
    this.params.duplicate = duplicate;
    this.emitChange();
  }

  get duplicate() {
    return this.params.duplicate;
  }

  /**
      *
      * @param affSub
      */
  set affSub(affSub: string) {
    this.params.affSub = affSub;
    this.emitChange();
  }

  get affSub() {
    return this.params.affSub;
  }

  /**
      *
      * @param affSub2
      */
  set affSub2(affSub2: string) {
    this.params.affSub2 = affSub2;
    this.emitChange();
  }

  get affSub2() {
    return this.params.affSub2;
  }

  /**
      *
      * @param affSub3
      */
  set affSub3(affSub3: string) {
    this.params.affSub3 = affSub3;
    this.emitChange();
  }

  get affSub3() {
    return this.params.affSub3;
  }

  /**
      *
      * @param affSub4
      */
  set affSub4(affSub4: string) {
    this.params.affSub4 = affSub4;
    this.emitChange();
  }

  get affSub4() {
    return this.params.affSub4;
  }

  /**
      *
      * @param offers
      */
  set offers(offers: string[]) {
    this.params['offers[]'] = offers;
    this.emitChange();
  }

  get offers() {
    return this.params['offers[]'];
  }

  /**
      *
      * @param advertisers
      */
  set advertisers(advertisers: string[]) {
    this.params['advertisers[]'] = advertisers;
    this.emitChange();
  }

  get advertisers() {
    return this.params['advertisers[]'];
  }

  /**
    *
    * @param offers_ids
    */
  set offers_ids(offers_ids: string[]) {
    this.params['offers_ids[]'] = offers_ids;
    this.emitChange();
  }

  get offers_ids() {
    return this.params['offers_ids[]'];
  }


  /**
      *
      * @param publishers
      */
  set publishers(publishers: string[]) {
    this.params['publishers[]'] = publishers;
    this.emitChange();
  }

  get publishers() {
    return this.params['publishers[]'];
  }

  /**
      *
      * @param countries
      */
  set countries(countries: string[]) {
    this.params['countries[]'] = countries;
    this.emitChange();
  }

  get countries() {
    return this.params['countries[]'];
  }

  /**
      *
      * @param platforms
      */
  set platforms(platforms: string[]) {
    this.params['platforms[]'] = platforms;
    this.emitChange();
  }

  get platforms() {
    return this.params['platforms[]'];
  }

  /**
      *
      * @param statuses
      */
  set statuses(statuses: number[]) {
    this.params['statuses[]'] = statuses;
    this.emitChange();
  }

  get statuses() {
    return this.params['statuses[]'];
  }


  public reset() {
    this.params = new SuperlinkSystemConversionStatisticFilterParams();
  }

}
