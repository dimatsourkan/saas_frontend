import { HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Crud } from '@app/core/decorators/crud.decorator';
import { DataStore } from '@app/core/entities/data-store.service';
import { parseFileDownloadResult } from '@app/core/helpers/helpers';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SuperlinkSystemConversionStatisticData } from './superlink-system-conversion-statistics.data';
import { SuperlinkSystemConversionStatistic } from './superlink-system-conversion-statistics.model';


@Injectable({
  providedIn: 'root'
})
@Crud('reports/conversions/superlink/system', SuperlinkSystemConversionStatistic, DataStore.superlinkSystemConversionStatistic)
export class SuperlinkSystemConversionStatisticService extends CRUDService<SuperlinkSystemConversionStatistic,
  SuperlinkSystemConversionStatisticData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  sendToEmail(params = new HttpParams()): Observable<HttpResponse<any>> {
    return this.httpClient.get(this.getUrl('send'), {observe: 'response', params});
  }

  exportCSV(params = new HttpParams()) {
    return this.httpClient.get(this.getUrl('export'), {
      responseType: 'arraybuffer',
      observe: 'response', params
    }).pipe(map(res => parseFileDownloadResult(res)));
  }

}
