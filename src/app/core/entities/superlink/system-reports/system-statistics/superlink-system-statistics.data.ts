import { BaseData } from '@app/core/models/base-data/base.data';
import { SuperlinkSystemStatistic } from './superlink-system-statistics.model';


export class SuperlinkSystemStatisticData extends BaseData<SuperlinkSystemStatistic> {

  clearData() {
    super.clearData();
  }

}
