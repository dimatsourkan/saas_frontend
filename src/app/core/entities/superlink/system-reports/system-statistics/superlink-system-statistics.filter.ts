import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';
import * as moment from 'moment';
import { DataStore } from '@app/core/entities/data-store.service';

class SuperlinkSystemStatisticFilterParams extends FilterParams {
  datetime_from = moment().subtract(3, 'month').format('YYYY-MM-DD');
  datetime_to = moment().format('YYYY-MM-DD');
  date: 1 | 0 = 1;
  clicks: 1 | 0 = 1;
  back_url_clicks: 1 | 0  = null;
  conv: 1 | 0 = 1;
  pub_conv: 1 | 0 = 1;
  offer_id: 1 | 0 = 1;
  offer_real_id: 1 | 0 = 1;
  pub_cr: 1 | 0 = 1;
  cr: 1 | 0 = 1;
  revenues: 1 | 0 = 1;
  costs: 1 | 0 = 1;
  pub_name: 1 | 0 = 1;
  min_fraud_perc: 1 | 0  = null;
  profit: 1 | 0 = 1;
  group_name: string = '';
  group_id: 1 | 0 = 1;
  offer_name: 1 | 0 = 1;
  timezone: string = DataStore.timezone.current.getValue();
  resale_name: string = '';
  resale_id: 1 | 0 = 1;
  month: 1 | 0  = null;
  hour: 1 | 0  = null;
  sub_aff: 1 | 0  = null;
  adv_man: 1 | 0  = null;
  pub_man: 1 | 0  = null;
  non_zero_revenue: 1 | 0  = null;
  hash: 1 | 0  = null;
  adv_id: 1 | 0  = null;
  platforms: 1 | 0  = null;
  countries: 1 | 0  = null;
  adv_name: 1 | 0  = null;
  pub_id: 1 | 0  = null;
  rpa: 1 | 0  = null;
  rpc: 1 | 0  = null;
  over_paid: 1 | 0  = null;
  cpa: 1 | 0  = null;
  pub_cpa: 1 | 0  = null;
  cpc: 1 | 0  = null;
  min_fraud: 1 | 0  = null;
  duplicates_perc: 1 | 0  = null;
  duplicates: 1 | 0  = null;
  'countries_codes[]': string[] = [];
  'platforms_names[]': string[] = [];
  'groups_ids[]': string[] = [];
  sub_aff_id: string = '';
  'offers_ids[]': string[] = [];
  traffic_type: 1 | 2 = null;
  'categories_ids[]': string[] = [];
  'adv_ids[]': string[] = [];
  'pub_ids[]': string[] = [];
  'man_ids[]': string[] = [];
  total_currency: string = 'USD';
}

export class SuperlinkSystemStatisticFilter extends FilterService {

  protected params = new SuperlinkSystemStatisticFilterParams();

  public reset() {
    this.params = new SuperlinkSystemStatisticFilterParams();
  }

  /**
   *
   * @param datetime_from
   */
  set datetime_from(datetime_from: string) {
    this.params.datetime_from = datetime_from;
    this.emitChange();
  }

  get datetime_from() {
    return this.params.datetime_from;
  }

  /**
  *
  * @param datetime_to
  */
  set datetime_to(datetime_to: string) {
    this.params.datetime_to = datetime_to;
    this.emitChange();
  }

  get datetime_to() {
    return this.params.datetime_to;
  }

  /**
      *
      * @param date
  */
  set date(date: 1 | 0 ) {
    this.params.date = date;
    this.emitChange();
  }

  get date() {
    return this.params.date;
  }

  /**
      *
      * @param clicks
      */
  set clicks(clicks: 1 | 0 ) {
    this.params.clicks = clicks;
    this.emitChange();
  }

  get clicks() {
    return this.params.clicks;
  }
  /**
      *
      * @param back_url_clicks
      */
  set back_url_clicks(back_url_clicks: 1 | 0 ) {
    this.params.back_url_clicks = back_url_clicks;
    this.emitChange();
  }

  get back_url_clicks() {
    return this.params.back_url_clicks;
  }
  /**
      *
      * @param conv
      */
  set conv(conv: 1 | 0 ) {
    this.params.conv = conv;
    this.emitChange();
  }

  get conv() {
    return this.params.conv;
  }

  /**
      *
      * @param pub_conv
      */
  set pub_conv(pub_conv: 1 | 0 ) {
    this.params.pub_conv = pub_conv;
    this.emitChange();
  }

  get pub_conv() {
    return this.params.pub_conv;
  }

  /**
      *
      * @param offer_id
      */
  set offer_id(offer_id: 1 | 0 ) {
    this.params.offer_id = offer_id;
    this.emitChange();
  }

  get offer_id() {
    return this.params.offer_id;
  }

  /**
      *
      * @param offer_real_id
      */
  set offer_real_id(offer_real_id: 1 | 0 ) {
    this.params.offer_real_id = offer_real_id;
    this.emitChange();
  }

  get offer_real_id() {
    return this.params.offer_real_id;
  }

  /**
      *
      * @param pub_cr
      */
  set pub_cr(pub_cr: 1 | 0 ) {
    this.params.pub_cr = pub_cr;
    this.emitChange();
  }

  get pub_cr() {
    return this.params.pub_cr;
  }


  /**
      *
      * @param cr
      */
  set cr(cr: 1 | 0 ) {
    this.params.cr = cr;
    this.emitChange();
  }

  get cr() {
    return this.params.cr;
  }

  /**
      *
      * @param revenues
      */
  set revenues(revenues: 1 | 0 ) {
    this.params.revenues = revenues;
    this.emitChange();
  }

  get revenues() {
    return this.params.revenues;
  }
  /**
      *
      * @param costs
      */
  set costs(costs: 1 | 0 ) {
    this.params.costs = costs;
    this.emitChange();
  }

  get costs() {
    return this.params.costs;
  }
  /**
      *
      * @param pub_name
      */
  set pub_name(pub_name: 1 | 0 ) {
    this.params.pub_name = pub_name;
    this.emitChange();
  }

  get pub_name() {
    return this.params.pub_name;
  }
  /**
      *
      * @param min_fraud_perc
      */
  set min_fraud_perc(min_fraud_perc: 1 | 0 ) {
    this.params.min_fraud_perc = min_fraud_perc;
    this.emitChange();
  }

  get min_fraud_perc() {
    return this.params.min_fraud_perc;
  }

  /**
      *
      * @param profit
      */
  set profit(profit: 1 | 0 ) {
    this.params.profit = profit;
    this.emitChange();
  }

  get profit() {
    return this.params.profit;
  }
  /**
      *
      * @param offer_name
      */
  set offer_name(offer_name: 1 | 0 ) {
    this.params.offer_name = offer_name;
    this.emitChange();
  }

  get offer_name() {
    return this.params.offer_name;
  }

  /**
      *
      * @param timezone
      */
  set timezone(timezone: string) {
    this.params.timezone = timezone;
    this.emitChange();
  }

  get timezone() {
    return this.params.timezone;
  }

  /**
      *
      * @param month
      */
  set month(month: 1 | 0 ) {
    this.params.month = month;
    this.emitChange();
  }

  get month() {
    return this.params.month;
  }
  /**
      *
      * @param hour
      */
  set hour(hour: 1 | 0 ) {
    this.params.hour = hour;
    this.emitChange();
  }

  get hour() {
    return this.params.hour;
  }

  /**
      *
      * @param sub_aff
      */
  set sub_aff(sub_aff: 1 | 0 ) {
    this.params.sub_aff = sub_aff;
    this.emitChange();
  }

  get sub_aff() {
    return this.params.sub_aff;
  }

  /**
      *
      * @param adv_man
      */
  set adv_man(adv_man: 1 | 0 ) {
    this.params.adv_man = adv_man;
    this.emitChange();
  }

  get adv_man() {
    return this.params.adv_man;
  }

  /**
      *
      * @param pub_man
      */
  set pub_man(pub_man: 1 | 0 ) {
    this.params.pub_man = pub_man;
    this.emitChange();
  }

  get pub_man() {
    return this.params.pub_man;
  }

  /**
      *
      * @param non_zero_revenue
      */
  set non_zero_revenue(non_zero_revenue: 1 | 0 ) {
    this.params.non_zero_revenue = non_zero_revenue;
    this.emitChange();
  }

  get non_zero_revenue() {
    return this.params.non_zero_revenue;
  }

  /**
      *
      * @param hash
      */
  set hash(hash: 1 | 0 ) {
    this.params.hash = hash;
    this.emitChange();
  }

  get hash() {
    return this.params.hash;
  }

  /**
      *
      * @param adv_id
      */
  set adv_id(adv_id: 1 | 0 ) {
    this.params.adv_id = adv_id;
    this.emitChange();
  }

  get adv_id() {
    return this.params.adv_id;
  }

  /**
      *
      * @param platforms
      */
  set platforms(platforms: 1 | 0 ) {
    this.params.platforms = platforms;
    this.emitChange();
  }

  get platforms() {
    return this.params.platforms;
  }

  /**
      *
      * @param countries
      */
  set countries(countries: 1 | 0 ) {
    this.params.countries = countries;
    this.emitChange();
  }

  get countries() {
    return this.params.countries;
  }

  /**
      *
      * @param adv_name
      */
  set adv_name(adv_name: 1 | 0 ) {
    this.params.adv_name = adv_name;
    this.emitChange();
  }

  get adv_name() {
    return this.params.adv_name;
  }

  /**
      *
      * @param pub_id
      */
  set pub_id(pub_id: 1 | 0 ) {
    this.params.pub_id = pub_id;
    this.emitChange();
  }

  get pub_id() {
    return this.params.pub_id;
  }

  /**
      *
      * @param rpa
      */
  set rpa(rpa: 1 | 0 ) {
    this.params.rpa = rpa;
    this.emitChange();
  }

  get rpa() {
    return this.params.rpa;
  }

  /**
      *
      * @param rpc
      */
  set rpc(rpc: 1 | 0 ) {
    this.params.rpc = rpc;
    this.emitChange();
  }

  get rpc() {
    return this.params.rpc;
  }

  /**
      *
      * @param over_paid
      */
  set over_paid(over_paid: 1 | 0 ) {
    this.params.over_paid = over_paid;
    this.emitChange();
  }

  get over_paid() {
    return this.params.over_paid;
  }

  /**
      *
      * @param cpa
      */
  set cpa(cpa: 1 | 0 ) {
    this.params.cpa = cpa;
    this.emitChange();
  }

  get cpa() {
    return this.params.cpa;
  }

  /**
      *
      * @param pub_cpa
      */
  set pub_cpa(pub_cpa: 1 | 0 ) {
    this.params.pub_cpa = pub_cpa;
    this.emitChange();
  }

  get pub_cpa() {
    return this.params.pub_cpa;
  }

  /**
      *
      * @param cpc
      */
  set cpc(cpc: 1 | 0 ) {
    this.params.cpc = cpc;
    this.emitChange();
  }

  get cpc() {
    return this.params.cpc;
  }

  /**
      *
      * @param min_fraud
      */
  set min_fraud(min_fraud: 1 | 0 ) {
    this.params.min_fraud = min_fraud;
    this.emitChange();
  }

  get min_fraud() {
    return this.params.min_fraud;
  }

  /**
      *
      * @param duplicates_perc
      */
  set duplicates_perc(duplicates_perc: 1 | 0 ) {
    this.params.duplicates_perc = duplicates_perc;
    this.emitChange();
  }

  get duplicates_perc() {
    return this.params.duplicates_perc;
  }

  /**
      *
      * @param duplicates
      */
  set duplicates(duplicates: 1 | 0 ) {
    this.params.duplicates = duplicates;
    this.emitChange();
  }

  get duplicates() {
    return this.params.duplicates;
  }

  /**
      *
      * @param sub_aff_id
      */
  set sub_aff_id(sub_aff_id: string) {
    this.params.sub_aff_id = sub_aff_id;
    this.emitChange();
  }

  get sub_aff_id() {
    return this.params.sub_aff_id;
  }

  /**
      *
      * @param traffic_type
      */
  set traffic_type(traffic_type: 1 | 2) {
    this.params.traffic_type = traffic_type;
    this.emitChange();
  }

  get traffic_type() {
    return this.params.traffic_type;
  }

  /**
      *
      * @param countries_ids
      */
  set countries_ids(countries_ids: string[]) {
    this.params['countries_ids[]'] = countries_ids;
    this.emitChange();
  }

  get countries_ids() {
    return this.params['countries_ids[]'];
  }

  /**
      *
      * @param platform_names
      */
  set platform_names(platform_names: string[]) {
    this.params['platform_names[]'] = platform_names;
    this.emitChange();
  }

  get platform_names() {
    return this.params['platform_names[]'];
  }

  /**
      *
      * @param offers_ids
      */
  set offers_ids(offers_ids: string[]) {
    this.params['offers_ids[]'] = offers_ids;
    this.emitChange();
  }

  get offers_ids() {
    return this.params['offers_ids[]'];
  }

  /**
      *
      * @param categories_ids
      */
  set categories_ids(categories_ids: string[]) {
    this.params['categories_ids[]'] = categories_ids;
    this.emitChange();
  }

  get categories_ids() {
    return this.params['categories_ids[]'];
  }

  /**
      *
      * @param adv_ids
      */
  set adv_ids(adv_ids: string[]) {
    this.params['adv_ids[]'] = adv_ids;
    this.emitChange();
  }

  get adv_ids() {
    return this.params['adv_ids[]'];
  }

  /**
      *
      * @param pub_ids
      */
  set pub_ids(pub_ids: string[]) {
    this.params['pub_ids[]'] = pub_ids;
    this.emitChange();
  }

  get pub_ids() {
    return this.params['pub_ids[]'];
  }

  /**
      *
      * @param man_ids
      */
  set man_ids(man_ids: string[]) {
    this.params['man_ids[]'] = man_ids;
    this.emitChange();
  }

  get man_ids() {
    return this.params['man_ids[]'];
  }

  /**
      *
      * @param resale_name
      */
  set resale_name(resale_name: any) {
    this.params.resale_name = resale_name;
    this.emitChange();
  }

  get resale_name() {
    return this.params.resale_name;
  }

  /**
      *
      * @param resale_id
      */
  set resale_id(resale_id: any) {
    this.params.resale_id = resale_id;
    this.emitChange();
  }

  get resale_id() {
    return this.params.resale_id;
  }
  /**
      *
      * @param group_name
      */
  set group_name(group_name: any) {
    this.params.group_name = group_name;
    this.emitChange();
  }

  get group_name() {
    return this.params.group_name;
  }

  /**
      *
      * @param group_id
      */
  set group_id(group_id: any) {
    this.params.group_id = group_id;
    this.emitChange();
  }

  get group_id() {
    return this.params.group_id;
  }
  /**
      *
      * @param countries_codes
      */
  set countries_codes(countries_codes: any) {
    this.params['countries_codes[]'] = countries_codes;
    this.emitChange();
  }

  get countries_codes() {
    return this.params['countries_codes[]'];
  }

  /**
     *
     * @param groups_ids
     */
  set groups_ids(groups_ids: any) {
    this.params['groups_ids[]'] = groups_ids;
    this.emitChange();
  }

  get groups_ids() {
    return this.params['groups_ids[]'];
  }

  /**
  *
  * @param platforms_names
  */
  set platforms_names(platforms_names: any) {
    this.params['platforms_names[]'] = platforms_names;
    this.emitChange();
  }

  get platforms_names() {
    return this.params['platforms_names[]'];
  }

  /**
      *
      * @param total_currency
      */
  set total_currency(total_currency: any) {
    this.params.total_currency = total_currency;
    this.emitChange();
  }

  get total_currency() {
    return this.params.total_currency;
  }

}
