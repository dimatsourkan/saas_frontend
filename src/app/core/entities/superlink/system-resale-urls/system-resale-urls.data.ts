import { BaseData } from '@app/core/models/base-data/base.data';
import { SystemResaleUrl } from './system-resale-urls.model';

export class SystemResaleUrlData extends BaseData<SystemResaleUrl> {

  clearData() {
    super.clearData();
  }

}
