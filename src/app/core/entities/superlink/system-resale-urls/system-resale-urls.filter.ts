import { FilterParams, FilterService } from '@app/core/services/filter/filter.service';

class SystemResaleUrlsFilterParams extends FilterParams {

}

export class SystemResaleUrlsFilter extends FilterService {

  protected params = new SystemResaleUrlsFilterParams();


  public reset() {
    this.params = new SystemResaleUrlsFilterParams();
  }

}
