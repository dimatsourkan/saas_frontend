
import { Type, Expose } from 'class-transformer';
import { BaseModel } from '@app/core/models/base-model/base.model';
import { Common } from '@app/core/models/common-model/common.model';


export class SystemResaleUrl extends BaseModel {

  @Type(() => Common.ResaleUrl)
  resaleUrl = new Common.ResaleUrl();

  name: string = 'Algorithm #';
  url: string = '';

  @Expose({ name: 'resaleUrlId' })
  get resaleUrlId() {
    return this.resaleUrl.id;
  }
}

