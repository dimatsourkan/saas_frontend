import { Injectable } from '@angular/core';

import { Crud } from '@app/core/decorators/crud.decorator';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { DataStore } from '../../data-store.service';
import { SystemResaleUrlData } from './system-resale-urls.data';
import { SystemResaleUrl } from './system-resale-urls.model';

@Injectable({
  providedIn: 'root'
})
@Crud('system_resale_url', SystemResaleUrl, DataStore.superlinkSystemResaleUrls)
export class SystemResaleUrlService extends CRUDService<SystemResaleUrl, SystemResaleUrlData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

}
