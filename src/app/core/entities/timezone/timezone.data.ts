import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { Timezone } from './timezone.model';

export class TimezoneData extends BaseData<Timezone> {
  public current: StoreItem<string> = new StoreItem();
}
