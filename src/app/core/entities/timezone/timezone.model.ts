import { BaseModel } from '@app/core/models/base-model/base.model';

export class Timezone extends BaseModel {
  identifier: string;
  timezone: string;
}
