import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { ResultList } from '@app/core/models/result-model/result.model';

import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { shareReplay, tap } from 'rxjs/operators';
import { Crud } from '../../decorators/crud.decorator';
import { CRUDService } from '../../services/crud/crud.service';
import { Timezone } from './timezone.model';
import { TimezoneData } from './timezone.data';
import { DataStoreHelper } from '../data-store.helper';

@Injectable({
  providedIn: 'root'
})
@Crud('timezones', Timezone, DataStore.timezone)
export class TimezoneService extends CRUDService<Timezone, TimezoneData> {

  private queryCache$: Observable<ResultList<Timezone>>;

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  protected wrapObjects(res: any): ResultList<Timezone, TimezoneData> {
    return plainToClassFromExist(new ResultList<Timezone, TimezoneData>(Timezone), {items: res});
  }

  getAll(params = new HttpParams(), customLink = null, withoutCache = false) {
    if (!this.queryCache$ || withoutCache) {
      this.queryCache$ = super.getAll(params).pipe(
        shareReplay(1)
      );
    }
    return this.queryCache$;
  }

  getKeyList(params = new HttpParams(), customLink = null, withoutCache = false) {
    return this.getAll(params, customLink, withoutCache);
  }

  current(): Observable<Timezone> {
    return this.httpClient
    .get<Timezone>(`${this.apiUrl}/timezone`, {headers: this.headers}).pipe(
      tap((res: any) => this.wrapObject(res)),
      tap((res) => DataStoreHelper.setDataCustom(this.dataStore, 'current', res.timezone as any))
    );
  }
}
