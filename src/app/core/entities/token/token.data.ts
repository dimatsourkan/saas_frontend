import { TokenModel } from '@app/core/entities/token/token.model';
import { BaseData } from '@app/core/models/base-data/base.data';

export class TokenData extends BaseData<TokenModel> {
}
