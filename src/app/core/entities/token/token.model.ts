import { BaseModel } from '@app/core/models/base-model/base.model';
import { Transform } from 'class-transformer';


export class TokenModel extends BaseModel {

  @Transform(value => value ? value : [])
  roles: string[] = [];

  @Transform(value => value ? value : [])
  permissions: string[] = [];

  iat: number = null;
  exp: number = null;
  name: string = null;
  username: string = null;
  special: boolean = false;
  token: string = null;

  get userName() {
    return this.name || this.username;
  }

  get role() {
    if (this.roles.length) {
      return this.roles[0];
    } else {
      return null;
    }
  }

  get isTokenExpired(): boolean {
    const tokenDate = this.getTokenExpirationDate();
    if (tokenDate == null) {
      return true;
    }
    const currentDate = (new Date().valueOf());
    return tokenDate.valueOf() < currentDate;
  }

  private getTokenExpirationDate(): Date {
    if (!this.exp) {
      return null;
    }
    const date = new Date(0);
    date.setUTCSeconds(this.exp);
    return date;
  }
}
