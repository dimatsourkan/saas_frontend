import { TestBed } from '@angular/core/testing';
import { DataStore } from '@app/core/entities/data-store.service';
import { ROLES } from '../../enums/role.enum';

import { TokenService } from './token.service';

function getUtcSeconds(date: Date, addDate: number) {
  date.setMonth(addDate);
  return parseInt(`${date.valueOf() / 1000}`, 10);
}

/**
 * Валидный токен
 */
const expDate = getUtcSeconds(new Date(), 10);
const tokenName = 'Manager';
const tokenRoles = [ROLES.MANAGER];
const tokenPermission = ['permission'];
const tokenSpecial = true;
const tokenData = {
  roles: tokenRoles,
  name: tokenName,
  username: tokenName,
  permissions: tokenPermission,
  special: tokenSpecial,
  iat: expDate,
  exp: expDate,
};
export const testTokenValid = 'testToken.' + window.btoa(
  JSON.stringify(tokenData)
);

/**
 * Токен с истекшим временем
 */
const invalidExpDate = getUtcSeconds(new Date(), -1);
tokenData.exp = invalidExpDate;
tokenData.iat = invalidExpDate;
export const testTokenInvalid = 'testToken.' + window.btoa(
  JSON.stringify(tokenData)
);

describe('TokenService', () => {

  let service: TokenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(TokenService);
  });

  afterEach(() => {
    service.deleteTokens();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be token in localStorage only', () => {
    service.setTokenOnly(ROLES.MANAGER, testTokenValid);
    expect(localStorage.getItem('token_manager')).toBe(testTokenValid);
    expect(DataStore.token.item.getValue().token).toBeNull();
  });

  it('should be token in localStorage and DataStore', () => {
    service.setToken(null, testTokenValid);
    expect(localStorage.getItem('token_manager')).toBe(testTokenValid);
    expect(DataStore.token.item.getValue().token).toBe(testTokenValid);
    expect(DataStore.token.item.getValue().special).toBe(tokenSpecial);
    expect(DataStore.token.item.getValue().isTokenExpired).toBeFalsy();
    expect(DataStore.token.item.getValue().permissions).toEqual(tokenPermission);
    expect(DataStore.token.item.getValue().roles).toEqual(tokenRoles);
    expect(DataStore.token.item.getValue().role).toBe(tokenRoles[0]);
    expect(DataStore.token.item.getValue().exp).toBe(expDate);
    expect(DataStore.token.item.getValue().iat).toBe(expDate);
    expect(DataStore.token.item.getValue().name).toBe(tokenName);
    expect(DataStore.token.item.getValue().username).toBe(tokenName);
  });
});
