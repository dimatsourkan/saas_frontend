import { Injectable } from '@angular/core';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { DataStore } from '@app/core/entities/data-store.service';
import { TokenModel } from '@app/core/entities/token/token.model';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { plainToClass } from 'class-transformer';

export enum TOKENS {
  MANAGER   = 'token_manager',
  PUBLISHER = 'token_publisher',
}

@Injectable({
  providedIn: 'root',
})
export class TokenService {

  constructor(private apiUrlService: ApiUrlService) {
    this.initToken();
  }

  setToken(role: string, token: string) {
    this.initToken(token);
    this.setTokenOnly(role, token);
  }

  setTokenOnly(role: string, token: string) {
    localStorage.setItem(`token_${this.apiUrlService.linkPathFromRole(role)}`, token);
  }

  deleteToken(role: string) {
    localStorage.removeItem(`token_${this.apiUrlService.linkPathFromRole(role)}`);
    DataStore.clearData();
    this.initToken();
  }

  deleteTokens() {
    localStorage.removeItem(TOKENS.MANAGER);
    localStorage.removeItem(TOKENS.PUBLISHER);
    DataStore.clearData();
  }

  tokenIsValid(token: string) {
    return !!this.getTokenData(token).token;
  }

  private initToken(token?) {
    const localToken = this.getToken();
    const tokenData = this.getTokenData((token) ? token : localToken);
    DataStoreHelper.setDataItem(DataStore.token, tokenData);
  }

  private getToken() {
    return this.getTokenByUrl() || this.getLocalStorageTokens()[0];
  }

  private getTokenByUrl() {

    const path = document.location.pathname.split('/');
    const pathName: string = path.find((p: string): boolean => {
      return p === 'publisher' || p === 'manager';
    });

    return localStorage.getItem(`token_${pathName}`);
  }

  private getLocalStorageTokens() {

    const tokens = [];
    const MANAGER_TOKEN = localStorage.getItem(TOKENS.MANAGER);
    const PUBLISHER_TOKEN = localStorage.getItem(TOKENS.PUBLISHER);

    if (MANAGER_TOKEN) {
      tokens.push(MANAGER_TOKEN);
    }

    if (PUBLISHER_TOKEN) {
      tokens.push(PUBLISHER_TOKEN);
    }

    return tokens;
  }

  private getTokenData(token: string): TokenModel {
    try {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      const tokenData = plainToClass(TokenModel, JSON.parse(window.atob(base64)) as Object);
      tokenData.token = token;
      return tokenData;
    } catch (e) {
      return plainToClass(TokenModel, {});
    }
  }

}
