import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { testTokenValid } from '@app/core/entities/token/token.service.spec';
import { ROLES } from '@app/core/enums/role.enum';
import { AuthService } from '@app/core/services/auth/auth.service';

import { FromRoles } from './from-role.service';

describe('FromRoleGuard', () => {

  let service: FromRoles;
  let authService: AuthService;

  const routeMockManager = {
    data: {
      roles: [ROLES.MANAGER]
    }
  } as any;

  const routeMockPublisher = {
    data: {
      roles: [ROLES.PUBLISHER]
    }
  } as any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    });
    service = TestBed.get(FromRoles);
    authService = TestBed.get(AuthService);
  });

  afterEach(() => {
    authService.logoutAll();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be not Activate', () => {
    expect(service.canActivate(routeMockManager)).toBeFalsy();
    authService.authorizeApp(null, testTokenValid);
    expect(service.canActivate(routeMockPublisher)).toBeFalsy();
  });

  it('should be not Load', () => {
    expect(service.canLoad(routeMockManager)).toBeFalsy();
    authService.authorizeApp(null, testTokenValid);
    expect(service.canLoad(routeMockPublisher)).toBeFalsy();
  });

  it('should be Activate', () => {
    authService.authorizeApp(null, testTokenValid);
    expect(service.canActivate(routeMockManager)).toBeTruthy();
  });

  it('should be Load', () => {
    authService.authorizeApp(null, testTokenValid);
    expect(service.canLoad(routeMockManager)).toBeTruthy();
  });
});
