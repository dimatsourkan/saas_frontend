import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router } from '@angular/router';
import { DataStore } from '../../entities/data-store.service';
import { ApiUrlService } from '../../services/api-url/api-url.service';
import { AuthService } from '../../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class FromRoles implements CanActivate, CanLoad {

  roles: string[];

  constructor(
    private router: Router,
    private auth: AuthService,
    private apiUrlService: ApiUrlService,
  ) {
  }

  private activate(route: ActivatedRouteSnapshot | Route) {
    const role = DataStore.token.item.getValue().role;
    if (role && route.data.roles.indexOf(role) >= 0) {
      return true;
    }

    this.router.navigate(['/', this.apiUrlService.linkPathFromRole()]);
    return false;
  }

  canActivate(route: ActivatedRouteSnapshot) {
    return this.activate(route);
  }

  canLoad(route: Route) {
    return this.activate(route);
  }
}
