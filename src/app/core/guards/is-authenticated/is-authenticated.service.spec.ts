import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { testTokenValid } from '@app/core/entities/token/token.service.spec';
import { IsAuthenticated } from '@app/core/guards/is-authenticated/is-authenticated.service';
import { AuthService } from '@app/core/services/auth/auth.service';

describe('IsAuthenticatedGuard', () => {

  let service: IsAuthenticated;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    });
    service = TestBed.get(IsAuthenticated);
    authService = TestBed.get(AuthService);
  });

  afterEach(() => {
    authService.logoutAll();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be not Activate', () => {
    expect(service.canActivate()).toBeFalsy();
  });

  it('should be not Load', () => {
    expect(service.canLoad()).toBeFalsy();
  });

  it('should be Activate', () => {
    authService.authorizeApp(null, testTokenValid);
    expect(service.canActivate()).toBeTruthy();
  });

  it('should be Load', () => {
    authService.authorizeApp(null, testTokenValid);
    expect(service.canLoad()).toBeTruthy();
  });
});
