import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class IsAuthenticated implements CanActivate, CanLoad {
  constructor(private router: Router, private auth: AuthService) {
  }

  private activate() {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['/login']);
      return false;
    }

    return true;
  }

  canActivate() {
    return this.activate();
  }

  canLoad() {
    return this.activate();
  }
}
