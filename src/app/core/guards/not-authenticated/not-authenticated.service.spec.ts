import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { testTokenValid } from '@app/core/entities/token/token.service.spec';
import { NotAuthenticated } from '@app/core/guards/not-authenticated/not-authenticated.service';
import { AuthService } from '@app/core/services/auth/auth.service';

describe('NotAuthenticatedGuard', () => {

  let service: NotAuthenticated;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    });
    service = TestBed.get(NotAuthenticated);
    authService = TestBed.get(AuthService);
  });

  afterEach(() => {
    authService.logoutAll();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be not Activate', () => {
    expect(service.canActivate()).toBeTruthy();
  });

  it('should be not Load', () => {
    expect(service.canLoad()).toBeTruthy();
  });

  it('should be Activate', () => {
    authService.authorizeApp(null, testTokenValid);
    expect(service.canActivate()).toBeFalsy();
  });

  it('should be Load', () => {
    authService.authorizeApp(null, testTokenValid);
    expect(service.canLoad()).toBeFalsy();
  });
});
