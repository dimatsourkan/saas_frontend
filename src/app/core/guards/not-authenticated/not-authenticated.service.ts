import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { ApiUrlService } from '../../services/api-url/api-url.service';
import { AuthService } from '../../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class NotAuthenticated implements CanActivate, CanLoad {
  constructor(
    private router: Router,
    private apiUrlService: ApiUrlService,
    private auth: AuthService
  ) {
  }

  canActivate() {
    return this.activate();
  }

  canLoad() {
    return this.activate();
  }

  activate() {
    if (this.auth.isAuthenticated()) {
      this.router.navigate(['/', this.apiUrlService.linkPathFromRole() || 'manager']);
    }
    return !this.auth.isAuthenticated();
  }
}
