import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { BaseModel } from '@app/core/models/base-model/base.model';
import { ClassTransformOptions, plainToClass } from 'class-transformer';
import { forEach } from 'lodash-es';

export class Helpers {

  /**
   * Функция добавляется к циклам в массиве для элементов у которых есть ID
   * Ускоряет перерисовку интерфейса при изменении данных
   * Почитать можно тут:
   * https://medium.freecodecamp.org/best-practices-for-a-clean-and-performant-angular-application-288e7b39eb6f
   * @param index
   * @param item
   */
  trackById(index: number, item: BaseModel) {
    return item.id;
  }

}

/**
 * Обновляет данные формы из переданного объекта
 * @param form
 * @param data
 */
export function patchFormValues(form: FormGroup, data: Object) {

  form.patchValue(data);

  forEach(data, (dataItem, key) => {

    const control: AbstractControl = form.controls[key];

    if (control instanceof FormGroup) {
      patchFormValues(control, dataItem);
    }

    if (control instanceof FormArray) {

      forEach(dataItem, (value, i) => {
        if (control[i]) {
          patchFormValues(control[i], value);
        }
      });
    }

  });

  return form;
}

/**
 * Проверяет наличие ключа в объекте
 */
export function hasKey(object: Object, key: string) {
  return Object.prototype.hasOwnProperty.call(object, key);
}

/**
 * Отдает ширину скроллбара
 */
export const scrollbarWidth: number = (function () {
  const outer = document.createElement('div');
  outer.style.visibility = 'hidden';
  outer.style.width = '100px';
  outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps

  document.body.appendChild(outer);

  const widthNoScroll = outer.offsetWidth;
  // force scrollbars
  outer.style.overflow = 'scroll';

  // add innerdiv
  const inner = document.createElement('div');
  inner.style.width = '100%';
  outer.appendChild(inner);

  const widthWithScroll = inner.offsetWidth;

  // remove divs
  outer.parentNode.removeChild(outer);

  return widthNoScroll - widthWithScroll;
})();

/**
 * Отдает URLSearchParams с текущими параметрами из урл
 */
export function UrlSearchParams() : URLSearchParams {
  return new URLSearchParams(location.search);
}

/**
 * Парсит строку вида key[key2][key3] в массив вида [key, key2, key3]
 * @param key
 */
export function parseValidationKeys(key: string): string[] {

  const keys: string[] = key.split('[');

  for (let i = 0; i < keys.length; i++) {
    keys[i] = keys[i].replace(/\]/, '');
  }

  return keys;

}

export function myPlainToClass<T>(
  this: new (...args: any[]) => T,
  plain: Object = {},
  options: ClassTransformOptions = {}) {
  return <T>plainToClass(this, plain, options);
}

export function getFinalString(string: string, start: number, end: number, value: string) {
  const beforeSubStr = string.substring(0, start);
  const afterSubStr = string.substring(end, string.length);
  return beforeSubStr + value + afterSubStr;

}

export function caretPosition(input) {
  const start = input.selectionStart;
  const end = input.selectionEnd;

  return {
    start, end
  };
}

export function checkKeyboardEventByRegexp(event: KeyboardEvent, regex: RegExp | RegExp[]) {

  if (!regex || event.key.length > 1) {
    return true;
  }

  const target = event.target as any;
  const position = caretPosition(target);
  const value: string = getFinalString(target.value, position.start, position.end, event.key);

  if (regex instanceof RegExp) {
    if (!regex.test(value)) {
      return false;
    }
  } else {

    return regex.filter(regexp => {
      return regexp.test(value);
    }).length === this.regexp.length;

  }

}

export function parseFileDownloadResult(res: any) {
  const type = res.headers.get('content-type');
  const fileInfo = type.split('/');
  if (fileInfo[0] !== 'image') {
    res.body = new Blob([res.body], {type: `${type};charset=utf-8`});
  }
  return res;
}
