import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/internal/operators';
import { LanguageService } from '../../shared/services/language/language.service';

@Injectable()
export class LanguageInterceptor implements HttpInterceptor {
  lang: string = 'en';

  constructor(private languageService: LanguageService) {
    if (this.languageService) {
      this.languageService.lang$.subscribe((lang) => {
        this.lang = lang;
      });
    }
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        'Accept-Language': this.lang
      }
    });
    return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        return event;
      }
    }, (err: any) => {
    }));
  }
}
