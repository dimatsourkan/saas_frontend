import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/internal/operators';
import { TransformToCamelCaseService } from '../../shared/services/transform-to-camel-case/transform-to-camel-case.service';

@Injectable({
  providedIn: 'root'
})
export class RequestInterceptor implements HttpInterceptor {

  constructor(private camelCase: TransformToCamelCaseService) {

  }

  mapCamelCase(response: any) {
    response.body = this.camelCase.map(response.body);
    return response;
  }

  parseErrorBlob(err: HttpErrorResponse): Observable<any> {
    const reader: any = new FileReader();

    const obs = new Observable((observer: any) => {
      reader.onloadend = (e) => {
        observer.error(JSON.parse(reader.result));
        observer.complete();
      };
    });
    reader.readAsText(err.error);
    return obs;
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const toCamelCase: any = request.headers.get('toCamelCase');
    const clonedRequest = request.clone({
      headers: request.headers.delete('toCamelCase')
    });
    return next.handle(clonedRequest)
      .pipe(
        map((response: any) => {
          if (toCamelCase) {
            return this.mapCamelCase(response);
          } else {
            return response;
          }
        }),
        catchError((error: any) => {
          if (request.responseType === 'blob') {
            return this.parseErrorBlob(error);
          } else {
            return observableThrowError(error.error || 'Server error');
          }
        })
      );
  }
}
