import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DataStore } from '@app/core/entities/data-store.service';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/internal/operators';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService,
              private router: Router,
              private apiUrlService: ApiUrlService,
              private swal: SweetAlertService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = DataStore.token.item.getValue().token;
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }

    return next.handle(request).pipe(tap(
      (event: any) => event,
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          const initedToken = DataStore.token.item.getValue().token;
          if (err.status === 401) {
            if (initedToken && token === initedToken) {
              this.auth.logout(this.apiUrlService.linkPathFromRole());
            }
          } else if (err.status === 500 && err.error.target.responseType !== 'arraybuffer') {
            this.router.navigate(['dashboard']);
            this.swal.error('Please, contact support.');
          } else if (err.status === 423) {
            this.swal.error(err.error.errors);
            if (initedToken && token === initedToken) {
              this.auth.logout(this.apiUrlService.linkPathFromRole());
            }
          }
        }
      }));
  }
}
