import { TestBed } from '@angular/core/testing';
import { BaseData, StoreItem } from '@app/core/models/base-data/base.data';
import { ResultList } from '../result-model/result.model';

class BaseDataTest extends BaseData<any> {
}

describe('BaseDataTest', () => {

  let service: BaseDataTest;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = new BaseDataTest();
  });

  /**
   *
   */
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   *
   */
  it('should be set list', () => {
    const list = new ResultList<any>();
    list.items = [{a: 1}];

    service.list.setValue(list);
    expect(service.list.getValue()).toEqual(list);
    service.list.asObservable$.subscribe(res => {
      expect(res).toEqual(list);
    });
  });

  /**
   *
   */
  it('should be set keyList', () => {
    const list = new ResultList<any>();
    list.items = [{a: 1}];

    service.keyList.setValue(list);
    expect(service.keyList.getValue()).toEqual(list);
    service.keyList.asObservable$.subscribe(res => {
      expect(res).toEqual(list);
    });
  });

  /**
   *
   */
  it('should be set item', () => {
    const item = {a: 1};

    service.item.setValue(item);
    expect(service.item.getValue()).toEqual(item);
    service.item.asObservable$.subscribe(res => {
      expect(res).toEqual(item);
    });
  });

  /**
   *
   */
  it('should be set selected', () => {
    const selected = ['1', '2', '3'];

    service.selected.setValue(selected);
    expect(service.selected.getValue()).toEqual(selected);
    expect(service.isSelected('1')).toBeTruthy();
    expect(service.isSelected('4')).toBeFalsy();
    service.selected.asObservable$.subscribe(res => {
      expect(res).toEqual(selected);
    });
  });
});


describe('StoreItem', () => {

  let service: StoreItem<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = new StoreItem();
  });

  /**
   *
   */
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   *
   */
  it('should be set item', () => {
    const item = {a: 1};

    service.setValue(item);
    expect(service.getValue()).toEqual(item);
    service.asObservable$.subscribe(res => {
      expect(res).toEqual(item);
    });
  });
});
