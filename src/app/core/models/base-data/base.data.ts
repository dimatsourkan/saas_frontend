import { classToClass } from 'class-transformer';
import { Observable, ReplaySubject } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';
import { BaseModel, modelId } from '../base-model/base.model';
import { ResultList } from '../result-model/result.model';

export class StoreItem<T extends any> {

  private value: T = null;
  private valueActions$ = new ReplaySubject<T>(1);

  public asObservable$: Observable<T>;

  constructor(initialValue?: T) {
    this.value = initialValue;
    this.valueActions$.next(initialValue);
    this.asObservable$ = this.valueActions$.pipe(
      map(value => classToClass(value)),
      publishReplay(1),
      refCount()
    );
  }

  setValue(value: T) {
    this.value = value;
    this.valueActions$.next(value);
  }

  getValue(): T {
    return classToClass(this.value);
  }
}

/**
 * Базовая модель хранения данных
 */
export abstract class BaseData<T extends BaseModel, S = {}> {

  /**
   * ResultList с списком моделей в основном для селектов
   */
  public keyList: StoreItem<ResultList<T>> = new StoreItem(new ResultList<T>());

  /**
   * Полноценный ResultList с списком моделей для страниц
   */
  public list: StoreItem<ResultList<T, S>> = new StoreItem(new ResultList<T, S>());

  /**
   * Одна выбранная модель
   */
  public item: StoreItem<T> = new StoreItem({} as any);

  /**
   * Хранит массив IDS
   */
  public selected: StoreItem<modelId[]> = new StoreItem([]);

  clearData() {
    this.keyList.setValue(new ResultList<T>());
    this.list.setValue(new ResultList<T, S>());
    this.item.setValue({} as any);
  }

  isSelected(id: modelId) {
    return this.selected.getValue().includes(id);
  }
}
