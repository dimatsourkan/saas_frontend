import { TestBed } from '@angular/core/testing';
import { BaseModel } from '@app/core/models/base-model/base.model';
import { classToPlain } from 'class-transformer';

class TestModel extends BaseModel {
}

describe('BaseModel', () => {

  let model: TestModel;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    model = new TestModel();
  });

  /**
   *
   */
  it('should be created', () => {
    expect(model).toBeTruthy();
  });

  /**
   *
   */
  it('should be toJson', () => {
    expect(model.toJson()).toEqual(classToPlain(model));
  });

  /**
   *
   */
  it('should be update', () => {
    const newModel = new TestModel();
    newModel.id = '1';
    expect(model.update({id: '1'})).toEqual(newModel);
  });

  /**
   *
   */
  it('should be clone', () => {
    expect(model.clone()).toEqual(model);
  });
});
