import { classToClass, classToPlain, plainToClassFromExist } from 'class-transformer';
import { myPlainToClass } from '../../helpers/helpers';

export type modelId = string;

export abstract class BaseModel {

  static plainToClass = myPlainToClass;

  createdAt: string = null;

  updatedAt: string = null;

  updated: string = null;

  id: modelId = null;

  /**
   * Эти методы иммутабельны, т.е. текущую сущность они не изменяют а возвращают новую
   */
  toJson() {
    return classToPlain(this);
  }

  update(data: any) {
    return plainToClassFromExist(this.clone(), data);
  }

  clone() {
    return classToClass(this);
  }
}
