import { Transform, Type } from 'class-transformer';
import { BaseModel } from '../base-model/base.model';

export namespace Common {

  export class Amount {
    amount: number = null;
    currency: string = null;
  }

  export class IpWhitelist {
    enabledIpWhitelist: 0 | 1 = 0;
    ipWhitelist: string[] = [];
  }

  export class ResaleUrl {
    id = null;
    name = '';
  }

  export class Manager {
    id = null;
    username = '';
  }

  export class User {
    id = null;
    username = '';
  }

  export class Address {
    @Type(() => Address)
    @Transform(value => value ? value : [])
    additionalAddress?: Address[] = [];
    address = '';
    city = '';
    countryCode = '';
    state = '';
    zipcode = '';
  }

  export class Person {
    firstName = '';
    lastName = '';
  }

  export class Status extends BaseModel {
    name: string = '';
    value: string = '';
    origin: string = '';
    original: string = '';
  }

  export class Role extends BaseModel {
    name: string;
  }


  export class TrafficType extends BaseModel {
    id: string;
    value: string;
  }

  export class RedirectType extends BaseModel {
    name: string;
    original: string;
  }

}
