import { TestBed } from '@angular/core/testing';
import { Pagination } from '@app/core/models/pagination-model/pagination.model';
import { classToPlain } from 'class-transformer';

describe('Pagination', () => {

  let model: Pagination<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    model = new Pagination();
  });

  /**
   *
   */
  it('should be created', () => {
    expect(model).toBeTruthy();
  });

  /**
   *
   */
  it('should be toJson', () => {
    expect(model.toJson()).toEqual(classToPlain(model));
  });

  /**
   *
   */
  it('should be update', () => {
    const newModel = new Pagination();
    newModel.totalPages = 30;
    expect(model.update({totalPages: 30})).toEqual(newModel);
  });

  /**
   *
   */
  it('should be clone', () => {
    expect(model.clone()).toEqual(model);
  });
});
