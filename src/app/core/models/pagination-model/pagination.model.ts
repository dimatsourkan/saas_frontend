import { classToClass, classToPlain, plainToClassFromExist } from 'class-transformer';
import { myPlainToClass } from '../../helpers/helpers';
import { BaseModel } from '../base-model/base.model';

export class Pagination<T extends BaseModel> {

  static plainToClass = myPlainToClass;

  public page = 1;
  public limit = 25;
  public totalPages = 1;
  public totalItems = 0;

  /**
   * Эти методы иммутабельны, т.е. текущую сущность они не изменяют а возвращают новую
   */
  toJson() {
    return classToPlain(this);
  }

  update(data: any) {
    return plainToClassFromExist(this.clone(), data);
  }

  clone() {
    return classToClass(this);
  }
}
