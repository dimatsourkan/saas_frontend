import { TestBed } from '@angular/core/testing';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Pagination } from '../pagination-model/pagination.model';

describe('ResultList', () => {

  let model: ResultList<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    model = (new ResultList());
    model.items = [{a: 1}];
  });

  /**
   *
   */
  it('should be created', () => {
    expect(model).toBeTruthy();
  });

  /**
   *
   */
  it('should be instanceof Pagination', () => {
    expect(model instanceof Pagination).toBeTruthy();
  });

  /**
   *
   */
  it('should be has items', () => {
    expect(model.items).toEqual([{a: 1}]);
  });
});
