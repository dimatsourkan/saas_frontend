import { Exclude, Transform, Type } from 'class-transformer';
import { BaseModel } from '../base-model/base.model';
import { Pagination } from '../pagination-model/pagination.model';

export class ResultList<T extends BaseModel, S = {}> extends Pagination<T> {

  @Exclude()
  private type: Function;

  @Type(options => (options.newObject as ResultList<T, S>).type)
  items: T[] = [];

  data: S[]|T = null;

  constructor(type ?: Function) {
    super();
    this.type = type;
  }
}
