import { TestBed } from '@angular/core/testing';
import { environment } from '../../../../environments/environment';
import { ROLES } from '../../enums/role.enum';

import { ApiUrlService } from './api-url.service';

describe('ApiUrlService', () => {

  let service: ApiUrlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(ApiUrlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('linkPathFromRole', () => {
    expect(service.linkPathFromRole(ROLES.MANAGER)).toBe('manager');
    expect(service.linkPathFromRole(ROLES.ADMIN)).toBe('manager');
    expect(service.linkPathFromRole(ROLES.PUBLISHER)).toBe('publisher');
    expect(service.linkPathFromRole(null)).toBe('');
  });

  it('apiUrl', () => {
    expect(service.apiUrl()).toBe(environment.baseURL);
  });

  it('apiUrlWithRole', () => {
    expect(service.apiUrlWithRole(ROLES.MANAGER)).toBe(`${environment.baseURL}/manager`);
    expect(service.apiUrlWithRole(ROLES.ADMIN)).toBe(`${environment.baseURL}/manager`);
    expect(service.apiUrlWithRole(ROLES.PUBLISHER)).toBe(`${environment.baseURL}/publisher`);
    expect(service.apiUrlWithRole(null)).toBe(`${environment.baseURL}/`);
  });
});
