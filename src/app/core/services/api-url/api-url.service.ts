import { Injectable } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { ROLES } from '@app/core/enums/role.enum';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiUrlService {

  private BASE_URL = environment.baseURL;

  linkPathFromRole(role ?: string) {

    const currentRole = role || DataStore.token.item.getValue().role;

    switch (currentRole) {
      case ROLES.MANAGER:
        return 'manager';
      case ROLES.ADMIN:
        return 'manager';
      case ROLES.PUBLISHER:
        return 'publisher';
      default:
        return '';
    }
  }

  apiUrl() {
    return `${this.BASE_URL}`;
  }

  apiUrlWithRole(role ?: string) {
    return `${this.apiUrl()}/${this.linkPathFromRole(role)}`;
  }

}
