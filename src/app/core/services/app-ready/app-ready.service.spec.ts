import { TestBed } from '@angular/core/testing';
import { AppReadyService } from './app-ready.service';

describe('AppReadyService', () => {

  let service: AppReadyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppReadyService]
    });
    service = TestBed.get(AppReadyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
