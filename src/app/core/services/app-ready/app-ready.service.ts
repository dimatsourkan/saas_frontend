import { Injectable, Injector } from '@angular/core';
import { ROLES } from '@app/core/enums/role.enum';
import { UrlSearchParams } from '@app/core/helpers/helpers';
import { AuthService } from '@app/core/services/auth/auth.service';
import { zip } from 'rxjs';
import { TokenService } from '@app/core/entities/token/token.service';
import { TimezoneService } from '@app/core/entities/timezone/timezone.service';

@Injectable()
export class AppReadyService {

  constructor(
    private injector: Injector,
    private tokenService: TokenService,
    private timezoneService: TimezoneService
  ) {

  }

  private getTimezone() {
    return this.timezoneService.current();
  }

  getTokenByQueryParams(): boolean|string {
    const params = UrlSearchParams();
    const token = params.get('global_admin_access_token');

    if (!token) {
      return false;
    }

    if (!this.tokenService.tokenIsValid(token)) {
      return false;
    }

    return token;
  }

  initializeApp(): Promise<any> {

    const authService = this.injector.get(AuthService);

    return new Promise((resolve) => {

      const token = this.getTokenByQueryParams();

      if (token) {
        authService.authorizeApp(ROLES.MANAGER, token as string);
      }

      if (authService.isAuthenticated()) {
        zip(
          this.getTimezone()
        ).subscribe(
          () => resolve(true),
          () => {
            authService.logoutAll();
            return resolve(true);
          }
        );
      } else {
        authService.logoutAll();
        return resolve(true);
      }
    });
  }
}

export function init_app(appLoadService: AppReadyService) {
  return () => appLoadService.initializeApp();
}
