import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { testTokenInvalid, testTokenValid } from '@app/core/entities/token/token.service.spec';

import { AuthService } from './auth.service';

describe('AuthService', () => {

  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    });
    service = TestBed.get(AuthService);
    service.authorizeApp(null, testTokenValid);
  });

  afterEach(() => {
    service.logoutAll();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be set Token', () => {
    expect(localStorage.getItem('token_manager')).toBe(testTokenValid);
  });

  it('should be Authenticated', () => {
    expect(service.isAuthenticated()).toBeTruthy();
  });

  it('should be Authenticated Manager', () => {
    expect(service.isManager).toBeTruthy();
    expect(service.isPublisher).toBeFalsy();
  });

  it('should be Authenticated with exp date true', () => {
    service.authorizeApp(null, testTokenInvalid);
    expect(service.isManager).toBeTruthy();
    expect(service.isPublisher).toBeFalsy();
    expect(service.isAuthenticated()).toBeFalsy();
  });

  it('should be not Authenticated', () => {
    service.logoutAll();
    expect(service.isAuthenticated()).toBeFalsy();
    expect(service.isManager).toBeFalsy();
    expect(service.isPublisher).toBeFalsy();
  });

  it('should be Logout', () => {
    expect(service.logout()).toBeTruthy();
  });

});
