import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DataStore } from '@app/core/entities/data-store.service';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { LanguageService } from '@app/shared/services/language/language.service';
import { ROLES } from '../../enums/role.enum';
import { TokenService } from '@app/core/entities/token/token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
    private apiUrlService: ApiUrlService,
    private tokenService: TokenService,
    private languageService: LanguageService,
  ) {

  }

  public isAuthenticated(): boolean {
    const token: any = this.tokenModel.token;
    if (!token || this.tokenModel.isTokenExpired) {
      this.logoutAll();
      return false;
    }
    return true;
  }

  get isManager() {
    const role = this.tokenModel.role;
    return role === ROLES.MANAGER || role === ROLES.ADMIN;
  }

  get isPublisher() {
    return this.tokenModel.role === ROLES.PUBLISHER;
  }

  logout(role?: string) {
    this.tokenService.deleteToken(role || DataStore.token.item.getValue().role);
    if (this.tokenModel.token) {

      if (this.isManager) {
        this.router.navigate(['/manager']);
      }

      if (this.isPublisher) {
        this.router.navigate(['/publisher']);
      }

    } else {
      // this.languageService.setLangOnly('en');
      this.router.navigate(['/login']);
    }

    return true;
  }

  authorizeApp(role: string, token: string) {
    this.tokenService.setToken(role, token);
  }

  authorizeUser(role: string, token: string) {
    this.tokenService.setTokenOnly(role, token);
  }

  logoutAll() {
    this.tokenService.deleteTokens();
  }

  private get tokenModel() {
    return DataStore.token.item.getValue();
  }
}
