import { HttpParams } from '@angular/common/http';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Injectable } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Crud } from '@app/core/decorators/crud.decorator';
import { BaseData } from '@app/core/models/base-data/base.data';
import { BaseModel } from '@app/core/models/base-model/base.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { TranslateModule } from '@ngx-translate/core';

class TestModel extends BaseModel {
}

class DataTest extends BaseData<any> {
}

const dataTest = new DataTest();

@Injectable()
@Crud('', TestModel, dataTest)
export class TestCrudService extends CRUDService<TestModel, DataTest> {
}

describe('TestCrudService', () => {

  let service: TestCrudService;
  let backend: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [TestCrudService]
    });
    service = TestBed.get(TestCrudService);
    backend = TestBed.get(HttpTestingController);
  });

  /**
   *
   */
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   *
   */
  it('should be list', () => {
    const query = new ResultList<TestModel>();

    service.getAll().subscribe(res => {
      expect(res.toJson()).toEqual(query.toJson());
      expect(dataTest.list.getValue().toJson()).toEqual(query.toJson());
    });
    backend.expectOne({
      url: service.getUrl(),
      method: 'GET'
    }).flush(query);
  });

  /**
   *
   */
  it('should be key list', () => {
    const query = new ResultList<TestModel>();

    service.getKeyList().subscribe(res => {
      expect(res.toJson()).toEqual(query.toJson());
      expect(dataTest.keyList.getValue().toJson()).toEqual(query.toJson());
    });
    backend.expectOne({
      url: service.getUrl() + '?' + new HttpParams({fromObject: {context: 'keylist'}}),
      method: 'GET'
    }).flush(query);
  });

  /**
   *
   */
  it('should be one item', () => {
    const query = new TestModel();

    service.get('1').subscribe(res => {
      expect(res.toJson()).toEqual(query.toJson());
      expect(dataTest.item.getValue().toJson()).toEqual(res.toJson());
    });
    backend.expectOne({
      url: service.getUrl('1'),
      method: 'GET'
    }).flush(query);
  });

  /**
   *
   */
  it('should be save', () => {
    const query = new TestModel();

    service.save(query).subscribe(res => {
      expect(res.toJson()).toEqual(query.toJson());
    });
    backend.expectOne({
      url: service.getUrl(),
      method: 'POST'
    }).flush(query);
  });

  /**
   *
   */
  it('should be update', () => {
    const query = new TestModel();

    service.update('1', query).subscribe(res => {
      expect(res.toJson()).toEqual(query.toJson());
    });
    backend.expectOne({
      url: service.getUrl('1'),
      method: 'PUT'
    }).flush(query.toJson());
  });

  /**
   *
   */
  it('should be delete', () => {
    service.delete('1').subscribe(res => {
      expect(res.res).toBeTruthy();
    });
    backend.expectOne({
      url: service.getUrl('1'),
      method: 'DELETE'
    }).flush({res: true});
  });

});
