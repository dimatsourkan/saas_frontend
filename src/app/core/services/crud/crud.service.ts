import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { plainToClassFromExist } from 'class-transformer';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/internal/operators';
import { Crud } from '../../decorators/crud.decorator';
import { DataStoreHelper } from '../../entities/data-store.helper';
import { BaseData } from '../../models/base-data/base.data';
import { BaseModel, modelId } from '../../models/base-model/base.model';
import { ResultList } from '../../models/result-model/result.model';
import { ApiUrlService } from '../api-url/api-url.service';

/**
 * Абстрактный CRUD Сервис для работы с сервером
 * Его должны унаследовать все crud сервисы
 */
@Injectable()
@Crud('', BaseModel, <any>BaseData)
export abstract class CRUDService<T extends BaseModel, B extends BaseData<T>, S = {}> {

  /**
   * Модель с которой работает сервис (УСТАРАВЛИВАЕТСЯ В ДЕКОРАТОРЕ CRUD)
   */
  private Entity: any;

  /**
   * Путь для круд операций (УСТАРАВЛИВАЕТСЯ В ДЕКОРАТОРЕ CRUD)
   */
  protected path: string;

  /**
   * Модель для хранения данных (УСТАРАВЛИВАЕТСЯ В ДЕКОРАТОРЕ CRUD)
   */
  protected dataStore: B;

  /**
   * Хедеры которые будут оправляться со всеми запросами
   */
  protected headers: HttpHeaders = new HttpHeaders({toCamelCase: 'true'});

  /**
   * Базовая ссылка на апи, возможно с учетом роли
   */
  protected get apiUrl() {
    return this.apiUrlService.apiUrl();
  }

  /**
   * Базовый урл по которому отправляются запросы
   */
  protected get baseUrl() {
    if (this.path) {
      return `${this.apiUrl}/${this.path}`;
    } else {
      return this.apiUrl;
    }
  }

  /**
   * @ignore
   */
  constructor(
    protected httpClient: HttpClient,
    protected apiUrlService: ApiUrlService
  ) {
  }

  /**
   * Функция которая оборачивает данные с сервера в объект с которым работает сервис
   * @param data - данные для обертки
   */
  protected createEntity(data: T): T {
    return this.Entity.plainToClass(data);
  }

  /**
   * Оборачивает данные с помощью функции createEntity для возврящяемых данных ResultList
   * @param res - Ответ сервера
   * @returns {ResultList<T>}
   */
  protected wrapObjects(res: any): ResultList<T, S> {
    return plainToClassFromExist(new ResultList<T, S>(this.Entity), res as Object);
  }

  /**
   * Оборачивает данные с помощью функции createEntity для возврящяемых данных Result
   * @param res - Ответ сервера
   * @returns {Result<T>}
   */
  protected wrapObject(res: T): T {
    return this.createEntity(res);
  }

  /**
   * Метод для установки apiUrl
   * @param path - Строка (вида 'custom-request' или пустая) по которому должен работать запрос
   */
  getUrl(path: modelId | string | number = null): string {
    if (path) {
      return `${this.baseUrl}/${path}`;
    } else {
      return this.baseUrl;
    }
  }

  /**
   * GET запрос для получения списка моделей
   * @param params - Объект с гет параметрами
   * @param customLink - Кастомная ссылка на параметры
   * @returns {Observable<Object>}
   */
  getAll(params = new HttpParams(), customLink?: string): Observable<ResultList<T, S>> {
    return this.httpClient
      .get(customLink || this.getUrl(), {headers: this.headers, params}).pipe(
        map((res) => this.wrapObjects(res)),
        tap((res) => DataStoreHelper.setDataList(this.dataStore, res))
      );
  }

  /**
   * GET запрос для получения списка моделей c параметрами id и name
   * @param params - Объект с гет параметрами
   * @param customLink - Объект с гет параметрами
   * @returns {Observable<Object>}
   */
  getKeyList(params = new HttpParams(), customLink?: string): Observable<ResultList<T, S>> {
    params = params.set('context', 'keylist');
    return this.httpClient
      .get(customLink || this.getUrl(), {headers: this.headers, params}).pipe(
        map((res) => this.wrapObjects(res)),
        tap((res) => DataStoreHelper.setDataKeyList(this.dataStore, res))
      );
  }

  /**
   * Получение модели по id
   * @param id - ID модели
   * @param params - Объект с гет параметрами
   * @param customLink - Объект с гет параметрами
   * @returns {Observable<Object>}
   */
  get(id: modelId, params = new HttpParams(), customLink?: string): Observable<T> {
    return this.httpClient
      .get(customLink || this.getUrl(id), {headers: this.headers, params}).pipe(
        map((res: any) => this.wrapObject(res)),
        tap((res) => DataStoreHelper.setDataItem(this.dataStore, res))
      );
  }

  /**
   * Сохранение новой модели
   * @param data - Данные модели
   * @param customLink - Данные модели
   * @returns {Observable<Object>}
   */
  save(data: T, customLink?: string): Observable<T> {
    return this.httpClient
      .post(customLink || this.getUrl(), data.toJson(), {headers: this.headers}).pipe(
        map((res: any) => this.wrapObject(res))
      );
  }

  /**
   * Изменение существующей модели
   * @param id - ID модели
   * @param data - Данные модели
   * @param customLink - Данные модели
   * @returns {Observable<Object>}
   */
  update(id: modelId, data: T, customLink?: string): Observable<T> {
    return this.httpClient
      .put(customLink || this.getUrl(id), data.toJson(), {headers: this.headers}).pipe(
        map((res: any) => this.wrapObject(res)),
        tap((res: T) => DataStoreHelper.updateItemInList(data, this.dataStore))
      );
  }

  /**
   * Удаление существующей модели
   * @param id - Id модели
   * @param customLink - Id модели
   * @returns {Observable<Object>}
   */
  delete(id: modelId, customLink?: string): Observable<any> {
    return this.httpClient
      .delete(customLink || this.getUrl(id), {headers: this.headers}).pipe(
        tap((res) => DataStoreHelper.removeItemFromList(this.dataStore, id))
      );
  }
}
