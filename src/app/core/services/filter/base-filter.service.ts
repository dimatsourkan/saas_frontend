import { HttpParams } from '@angular/common/http';
import { BehaviorSubject, Subject } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';
import { UrlSearchParams } from '../../helpers/helpers';

export class BaseFilterParams {
}

export abstract class BaseFilterService {

  private _excluded = [];
  private cancelChange = false;
  private subscriber$ = new Subject();
  private withQueryParams = false;
  private inited = false;
  protected params = new BaseFilterParams();
  public onChangeFilter$ = new BehaviorSubject(this);

  constructor(withQueryParams: boolean = false) {
    this.withQueryParams = withQueryParams;
  }

  /**
   * Методы для проверок
   * @param param
   */
  private canSetParam(param: string) {
    return !this.isEmpty(this.params[param]) && !this.isEmptyArray(this.params[param]);
  }

  private isEmpty(param: any) {
    return (param === '' || param === null || param === undefined);
  }

  private isEmptyArray(param: any) {
    return typeof param === 'object' && !param.length;
  }

  private isExcluded(param: string) {
    return this._excluded.includes(param);
  }

  /***/

  /**
   * Если параметр withQueryParams установлен в значение true
   * То метод устанавливает значения фильтров из гет параметров
   */
  protected setQueryParamsToLocal() {
    if (this.withQueryParams) {
      const queryParams = UrlSearchParams();
      Object.keys(this.params).forEach(param => {
        if (queryParams.get(param) && !this.isExcluded(param)) {
          this.params[param] = queryParams.get(param);
          try {
            this.params[param] = JSON.parse(queryParams.get(param));
          } catch (err) {
            this.params[param] = queryParams.get(param);
          }
        }
      });
    }

    return this;
  }

  /**
   * Если параметр withQueryParams установлен в значение true
   * То метод устанавливает гет параметры c фильтрами
   */
  protected updateQueryParams() {
    if (this.withQueryParams) {
      const queryParams = UrlSearchParams();
      Object.keys(this.params).forEach(param => {

        if (this.isExcluded(param)) {
          return;
        }

        if (this.canSetParam(param)) {
          this.setQueryParam(queryParams, param);
        } else {
          queryParams.delete(param);
        }
      });
      window.history.replaceState({}, '', `${location.pathname}?${queryParams}`);
    }
  }

  /**
   * Метод устанавливает значение фильтра в объект URLSearchParams
   * Если значением будет объект, то в параметр будет установлен json этого объекта
   * @param queryParams
   * @param param
   */
  protected setQueryParam(queryParams: URLSearchParams, param: any) {
    if (typeof this.params[param] === 'object') {
      queryParams.set(param, JSON.stringify(this.params[param]));
    } else if (typeof this.params[param] === 'boolean') {
      queryParams.set(param, String(this.params[param] ? 1 : 0));
    } else {
      queryParams.set(param, this.params[param]);
    }
  }

  /**
   * Метод отдает заполненые значения фильтров
   */
  public getAvailableParams() {
    const params = {};
    Object.keys(this.params).forEach(param => {
      if (this.canSetParam(param) && !this.isExcluded(param)) {
        params[param] = this.params[param];
      }
    });
    return params;
  }

  /**
   * Метод подтягивает данные фильтра с гет параметров и слушает subscriber$ Observable
   * В случае возникновения события в subscriber$ идет ожидание в 100 мс
   * Что бы можно было отменить изменение фильтра
   * Если событие не отменилось то обновляются параметры фильтра
   * И создается событие в onChangeFilter$ для того что бы уведомить слушателей о изменении фильтра
   */
  private _init() {

    if (this.inited) {
      return this;
    }

    this.setQueryParamsToLocal();
    this.updateQueryParams();
    this.subscriber$
      .pipe(debounceTime(100))
      .pipe(filter(() => {
        if (this.cancelChange) {
          return this.cancelChange = false;
        } else {
          return true;
        }
      }))
      .subscribe(() => {
        this.updateQueryParams();
        this.onChangeFilter$.next(this);
      });

    this.inited = true;
    return this;

  }

  /**
   * Метод инициирует фильтр, обязательно выполнить после создания инстанса
   */
  public init() {
    return this._init();
  }

  /**
   * Сбрасывает фильтр в значения по умолчанию
   */
  public reset() {
    this.params = new BaseFilterParams();
  }

  /**
   * Исключает из обработки фильтра какие либо параметры
   * @param params - Массив строк с названиями параметров для исключения
   */
  public exclude(params: string[]) {
    this._excluded = params;
  }

  /**
   * Отменяет последный вызов события, если оно еще не выполнилось
   */
  public cancelCurrentChangeEvent() {
    this.cancelChange = true;
  }

  /**
   * Создает событие в subscriber$
   */
  public emitChange() {
    this.subscriber$.next();
  }

  /**
   * Обновление фильтра
   * @param innerFilter
   */
  public update(innerFilter: BaseFilterService) {
    Object.keys(this.params).forEach(key => {
      if (!innerFilter.isExcluded(key)) {
        this.params[key] = innerFilter.params[key];
      }
    });
  }

  /**
   * Отдаются HttpParams параметры с заполнеными фильтрами
   */
  public get filter(): HttpParams {
    return new HttpParams({
      fromObject: this.getAvailableParams()
    });
  }

}
