import { HttpParams } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { FilterService } from './filter.service';

describe('FilterService', () => {

  let service: FilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = (new FilterService()).init();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be page', () => {
    service.page = 1;
    expect(service.page).toBe(1);
  });

  it('should be limit', () => {
    service.page = 2;
    service.limit = 10;
    expect(service.page).toBe(1);
    expect(service.limit).toBe(10);
    expect(service.filter).toEqual(new HttpParams({
      fromObject: {
        page: '1',
        limit: '10'
      }
    }));
  });

  it('should be search', () => {
    service.search = 'search';
    expect(service.page).toBe(1);
    expect(service.search).toBe('search');
    expect(service.filter).toEqual(new HttpParams({
      fromObject: {
        page: '1',
        limit: '25',
        search: 'search'
      }
    }));
  });

  it('should be context', () => {
    service.context = 'context';
    expect(service.context).toBe('context');
  });

  it('should be sort', () => {

    service.sort = 'adv';
    expect(service.sort).toBe('adv');
    expect(service.order).toBe('ASC');
    expect(service.filter).toEqual(new HttpParams({
      fromObject: {
        page: '1',
        limit: '25',
        sort: 'adv',
        order: 'ASC'
      }
    }));

    service.sort = 'adv';
    expect(service.sort).toBe('adv');
    expect(service.order).toBe('DESC');
    expect(service.filter).toEqual(new HttpParams({
      fromObject: {
        page: '1',
        limit: '25',
        sort: 'adv',
        order: 'DESC'
      }
    }));

    service.sort = 'adv';
    expect(service.sort).toBe('');
    expect(service.order).toBe('');
    expect(service.filter).toEqual(new HttpParams({
      fromObject: {
        page: '1',
        limit: '25'
      }
    }));
  });

  it('should be update', () => {
    const secondFilter: FilterService = (new FilterService()).init();
    secondFilter.context = 'context';
    secondFilter.search = 'search';
    secondFilter.sort = 'adv';
    secondFilter.page = 10;
    service.update(secondFilter);

    expect(service.context).toBe('context');
    expect(service.search).toBe('search');
    expect(service.order).toBe('ASC');
    expect(service.sort).toBe('adv');
    expect(service.page).toBe(10);
  });

  it('should be exclude', () => {
    service.exclude(['page', 'limit']);
    service.search = 'search';
    service.limit = 10;
    service.page = 20;

    expect(service.filter).toEqual(new HttpParams({
      fromObject: {
        search: 'search'
      }
    }));
  });
});
