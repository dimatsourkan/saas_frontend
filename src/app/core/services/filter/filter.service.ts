import { BaseFilterParams, BaseFilterService } from './base-filter.service';

type order = 'ASC' | 'DESC' | '';

export class FilterParams extends BaseFilterParams {
  limit = 10;
  page = 1;
  search = '';
  context = '';
  sort = '';
  order: order = '';
}

export class FilterService extends BaseFilterService {

  protected params = new FilterParams();

  /**
   * Page filter
   * @param page
   */
  set page(page: number) {
    this.params.page = page;
    this.emitChange();
  }

  get page() {
    return this.params.page;
  }

  /**
   * Limit filter
   * @param limit
   */
  set limit(limit: number) {
    if (limit > 100) {
      limit = 100;
    }
    this.params.limit = limit;
    this.params.page = 1;
    this.emitChange();
  }

  get limit() {
    return this.params.limit;
  }

  /**
   * Search filter
   * @param search
   */
  set search(search: string) {
    this.params.search = search;
    this.params.page = 1;
    this.emitChange();
  }

  get search() {
    return this.params.search;
  }

  /**
   * Context search filter
   * @param context
   */
  set context(context: string) {
    this.params.context = context;
    this.emitChange();
  }

  get context() {
    return this.params.context;
  }

  /**
   * Search filter
   * @param sort
   */
  set sort(sort: string) {
    this.params.sort = sort;
    if (this.order === 'ASC') {
      this.order = 'DESC';
    } else if (this.order === 'DESC') {
      this.order = '';
      this.params.sort = '';
    } else if (this.order === '') {
      this.order = 'ASC';
    }
    this.emitChange();
  }

  get sort() {
    return this.params.sort;
  }

  /**
   * Search filter
   * @param newOrder
   */
  set order(newOrder: order) {
    this.params.order = newOrder;
    this.emitChange();
  }

  get order() {
    return this.params.order;
  }

  public reset() {
    this.params = new FilterParams();
  }

}
