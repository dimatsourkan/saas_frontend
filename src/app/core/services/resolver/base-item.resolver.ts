import { Injectable, Injector } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseModel } from '../../models/base-model/base.model';
import { CRUDService } from '../crud/crud.service';

@Injectable({
  providedIn: 'root'
})
export abstract class BaseItemResolver<T extends BaseModel> implements Resolve<T> {

  protected service: CRUDService<T, any>;

  constructor(protected injector: Injector,
              protected router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

    const id: any = this.getParam(route);

    this.onStart();

    return this.query(id).pipe(
      map<any, any>(res => {
        return this.confirmData(res);
      }),
      catchError<any, any>((err: any, caught: Observable<any>): never => {
        this.onError(err);
        throw caught;
      })
    );
  }

  protected getParam(route: ActivatedRouteSnapshot) {
    return route.paramMap.get('id');
  }

  protected query(id: string) {
    return this.service.get(id);
  }

  protected confirmData(res: T) {
    if (res) {
      this.onSuccess();
      return res;
    }

    this.onError(res);
    return null;
  }

  protected onStart() {
    this.showLoader();
  }

  protected onSuccess() {
    this.hideLoader();
  }

  protected onError(error: any) {
    this.router.navigate(['/']);
    this.hideLoader();
  }

  protected showLoader() {
    const loader = document.getElementById('resolver-loader');
    if (loader) {
      loader.style.display = 'flex';
    }
  }

  protected hideLoader() {
    const loader = document.getElementById('resolver-loader');
    if (loader) {
      loader.style.display = 'none';
    }
  }
}
