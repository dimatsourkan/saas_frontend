import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-theme-generator',
  templateUrl: './theme-generator.component.html',
  styleUrls: ['./theme-generator.component.scss']
})
export class ThemeGeneratorComponent implements OnInit {
  themeForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.themeForm = this.fb.group({
      background: [],
      mainTestColor: [],
      primaryTestColor: [],
    });
  }

  saveTheme() {
    console.log('themeForm', this.themeForm.value);
  }

}
