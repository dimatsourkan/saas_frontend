import { Directive, ElementRef, Inject, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/platform-browser';
import { ThemeService } from './theme.service';
import { themes } from './themes.const';

@Directive({
  selector: '[appTheme]'
})
export class ThemeDirective implements OnInit, OnDestroy {

  private themeName = 'oceanBlueThemProps';
  private themServiceSubscription: Subscription;
  constructor(private elementRef: ElementRef,
    // tslint:disable-next-line: deprecation
    @Inject(DOCUMENT) private document: any,
    private themService: ThemeService) { }

  ngOnInit() {
    this.updateTheme(this.themeName);
    this.themService.getActiveTheme()
      .subscribe(themeName => {
        this.themeName = themeName;
        this.updateTheme(this.themeName);

      });
  }

  updateTheme(themeName) {
    const element = this.elementRef.nativeElement;
    const them = themes[themeName];
    // tslint:disable-next-line: forin
    for (const key in them) {
      element.style.setProperty(key, them[key]);
      this.document.body.style.setProperty(key, them[key]);
    }
  }

  ngOnDestroy() {
    if (this.themServiceSubscription) { this.themServiceSubscription.unsubscribe(); }
  }

}
