import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeDirective } from './theme.directive';
import { ThemeGeneratorComponent } from './theme-generator/theme-generator.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';

const THEME_ROUTING = [
  { path: 'theme-generator', component: ThemeGeneratorComponent }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(THEME_ROUTING)
  ],
  declarations: [ThemeDirective, ThemeGeneratorComponent],
  exports:      [ThemeDirective]
})
export class ThemeModule { }
