import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private activeThem = new BehaviorSubject(localStorage.getItem('theme'));

  constructor() {
    if (!localStorage.getItem('theme')) {
      this.setActiveThem('defaultSaasTheme');
    }
  }

  public getActiveTheme() {
    return this.activeThem.asObservable();
  }

  public setActiveThem(name) {
    localStorage.setItem('theme', name);
    this.activeThem.next(name);
  }
}
