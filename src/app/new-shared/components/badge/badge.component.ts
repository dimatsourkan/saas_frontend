import { Component, Input } from '@angular/core';

@Component({
  selector: 'badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss']
})
export class BadgeComponent {
  @Input() size: 'xs' | 'sm' | 'md' | 'lg' = 'md';

  @Input() primary: boolean = false;
  @Input() info: boolean = false;
  @Input() success: boolean = false;
  @Input() warning: boolean = false;
  @Input() danger: boolean = false;
  @Input() secondary: boolean = false;
  @Input() simple: boolean = false;
  @Input() contrast: boolean = false;

  @Input() block: boolean = false;
  @Input() truncate: boolean = false;

  @Input() textTransform: 'initial' | 'uppercase' | 'lowercase' = 'initial';
}
