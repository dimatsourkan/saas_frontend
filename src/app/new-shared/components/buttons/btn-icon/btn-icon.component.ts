import { Component, Input } from '@angular/core';

type btnType = 'info' | 'danger' | 'success' | 'warning' | 'primary' | 'light' | 'dark' | 'secondary';

@Component({
  selector: 'btn-icon',
  templateUrl: './btn-icon.component.html',
  styleUrls: ['./btn-icon.component.scss']
})
export class BtnIconComponent {
  @Input() type: btnType = 'info';
  @Input() icon: string = 'eye';
  @Input() opposite: boolean = false;
  @Input() outline: boolean = false;
  @Input() disabled: boolean = false;


  get classByType() {
    if (this.opposite) {
      return `btn-icon-outline-opposite-${this.type}`;
    } else if (this.outline) {
      return `btn-icon-outline-${this.type}`;
    } else {
      return `btn-icon-${this.type}`;
    }
  }
}
