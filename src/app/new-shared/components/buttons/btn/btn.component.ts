import { Component, Input } from '@angular/core';

@Component({
  selector: 'btn',
  templateUrl: './btn.component.html',
  styleUrls: ['./btn.component.scss']
})
export class BtnComponent {

  @Input() size: 'xs' | 'sm' | 'md' | 'lg' | 'xl' = 'md';
  @Input() type: 'button' | 'submit' = 'button';
  @Input() disabled: boolean = false;
  @Input() outline: boolean = false;
  @Input() success: boolean = false;
  @Input() warning: boolean = false;
  @Input() danger: boolean = false;
  @Input() simple: boolean = false;
  @Input() secondary: boolean = false;
  @Input() info: boolean = false;
  @Input() shadow: boolean = false;
  @Input() block: boolean = false;
  @Input() link: boolean = false;

}
