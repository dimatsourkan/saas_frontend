import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BtnComponent } from './btn/btn.component';
import { BtnIconComponent } from './btn-icon/btn-icon.component';

@NgModule({
  declarations: [
    BtnComponent,
    BtnIconComponent
  ],
  exports: [
    BtnComponent,
    BtnIconComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ButtonsModule { }
