import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardFooterComponent } from './card-footer/card-footer.component';
import { CardComponent } from './card/card.component';
import { CardBodyComponent } from './card-body/card-body.component';
import { CardHeaderComponent } from './card-header/card-header.component';

@NgModule({
  declarations: [CardComponent, CardBodyComponent, CardHeaderComponent, CardFooterComponent],
  exports: [CardComponent, CardBodyComponent, CardHeaderComponent, CardFooterComponent],
  imports: [
    CommonModule
  ]
})
export class CardModule { }
