import { EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FilterService } from '@app/core/services/filter/filter.service';
import { FilterComponent } from '@app/new-shared/components/filter/filter.component';
import { Subscription } from 'rxjs';

export abstract class AbstractFilterComponent<T extends FilterService> implements OnInit, OnDestroy {

  // tslint:disable-next-line:no-input-rename
  @Input('filter') protected outerFilter: T;
  @Output() onApply = new EventEmitter<T>();
  @Output() onCancel = new EventEmitter<T>();

  @ViewChild('filterComponent') private filterComponent: FilterComponent;

  filter: T;
  isOpen = false;
  protected subscription$: Subscription;

  ngOnInit() {
    this.subscription$ = this.outerFilter.onChangeFilter$.subscribe(filter => {
      this.filter.update(filter);
    });
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  public open() {
    this.filterComponent.open();
  }

  public close() {
    this.filterComponent.close();
  }

  public toggle() {
    this.filterComponent.toggle();
  }

  public applyFilters() {
    this.close();
    this.onApply.emit(this.filter);
  }

  public resetFilters() {
    this.close();
    this.filter.reset();
    this.onCancel.emit(this.filter);
  }

}
