import { Component, forwardRef, Injector } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';

@Component({
  selector: 'area-input',
  templateUrl: './area-input.component.html',
  styleUrls: ['./area-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AreaInputComponent),
      multi: true
    }
  ]
})
export class AreaInputComponent extends BaseControlValueAccessor {

  constructor(protected injector: Injector) {
    super(injector);
  }

}
