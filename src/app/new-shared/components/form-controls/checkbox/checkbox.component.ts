import { Component, ElementRef, forwardRef, Injector, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';

@Component({
  selector: 'checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    }
  ]
})
export class CheckboxComponent extends BaseControlValueAccessor {

  @ViewChild('check') private check: ElementRef;

  @Input() icon: string = 'check';
  @Input() trueValue: any = null;
  @Input() falseValue: any = null;

  set checked(checked: boolean) {
    this.check.nativeElement.checked = checked;
  }

  constructor(protected injector: Injector) {
    super(injector);
  }

  changeValue(value: any) {

    let outerValue = value;

    if (this.trueValue !== null && value) {
      outerValue = this.trueValue;
    }

    if (this.falseValue !== null && !value) {
      outerValue = this.falseValue;
    }

    super.changeValue(outerValue);
  }

  writeValue(value: any) {

    let innerValue = value;

    if (this.trueValue === value) {
      innerValue = true;
    }

    if (this.falseValue === value) {
      innerValue = false;
    }

    super.writeValue(innerValue);
    this.checked = innerValue;
  }
}
