import { AfterViewInit, Injector, Input, OnDestroy } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl, NgModel } from '@angular/forms';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

export abstract class BaseControlValueAccessor implements ControlValueAccessor, OnDestroy, AfterViewInit {

  /**
   * Приватное значение value
   */
  value: any = '';

  /**
   * Максимальная длина value
   */
  @Input()
  maxlength: number = null;

  /**
   * Значение для того что бы принудительно полю задать стили invalid
   */
  @Input()
  isInvalid: boolean = false;

  /**
   * Можно отсюда и убрать, но сделано для того что бы типизация не подсвечивалась
   * когда компонента будет подключаться к форме
   */
  @Input()
  formControlName: string;

  /**
   * Для передачи FormControl
   * @param formControl
   */
  @Input()
  set formControl(formControl: FormControl) {
    this.control = formControl;
  }

  /**
   * Placeholder который будет отображаться в поле
   */
  @Input()
  placeholder: string = '';

  /**
   * Передача value извне
   * @param value
   */
  @Input('value')
  set setValue(value: any) {
    this.writeValue(value);
  }

  /**
   * Установка disabled для поля
   * @param disabled
   */
  @Input()
  set disabled(disabled: boolean) {
    if (disabled) {
      this.control.disable({onlySelf: true});
    } else {
      this.control.enable({onlySelf: true});
    }
  }


  get disabled() {
    return this.control.disabled;
  }

  get invalid() {
    return this.isInvalid || this.control.invalid;
  }

  get touched() {
    return this.control.touched;
  }

  control = new FormControl('');

  protected subscription$: Subscription;

  constructor(protected injector: Injector) {
  }

  markAsTouched() {
    this.control.markAsTouched({onlySelf: true});
  }

  writeValue(value: any) {
    this.value = value;
  }

  propagateChange(_: any) {
    this.value = _;
  }

  propagateTouch(_: any) {
    this.value = _;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  changeValue(value: any) {
    this.value = value;
    this.propagateChange(value);
    this.propagateTouch(value);
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const ngControl: NgControl = this.injector.get(NgControl, null);
      if (ngControl && ngControl.control) {
        this.control = ngControl.control as FormControl;
      }

      /**
       * Костыль для того что бы работал ngModel
       */
      let changed = false;
      this.subscription$ = this.control.valueChanges
      .pipe(
        filter(value => {
          return changed ? changed = false : changed = true;
        })
      )
      .subscribe(value => {
        if (ngControl instanceof NgModel) {
          this.propagateChange(value);
          this.propagateTouch(value);
        }
      });
    });
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }
}
