import { Component, ElementRef, EventEmitter, HostListener, Injector, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { SHOW_DATE_FORMATS } from '@app/core/enums/date-formats';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';
import { LanguageService } from '@app/shared/services/language/language.service';
import * as moment from 'moment';
import { Moment } from 'moment';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { Subscription } from 'rxjs';

/**
 * Компонента должна использоваться как абстракция
 */
@Component({
  selector: 'app-base-date-picker',
  template: ''
})
export class BaseDatePickerComponent extends BaseControlValueAccessor implements OnInit, OnDestroy {

  @ViewChild('dateInput') dateInput: ElementRef;
  @ViewChild('pickerHtml') pickerHtml: ElementRef;
  @Output() onApply = new EventEmitter<string>();

  @Input() disabled = false;
  @Input() maxDateTime: Moment;
  @Input() position: 'left' | 'right' = 'left';

  /**
   * Формат с которым компонента будет отдавать и принимать дату
   */
  @Input() workingFormat = null;

  /**
   * Формат для отображения выбранных дат в зависимости от языка
   */
  get showFormat() {
    return SHOW_DATE_FORMATS[this.locale].DATE_PICKER;
  }

  isOpened = false;
  dateTime = this.momentValueTime(moment());

  protected subscription$: Subscription;
  protected locale: string;

  constructor(
    private dateTimeAdapter: DateTimeAdapter<any>,
    private languageService: LanguageService,
    protected injector: Injector
  ) {
    super(injector);
  }

  /**
   * При изменении языка меняет локализацию календаря
   */
  ngOnInit() {
    this.subscription$ = this.languageService.lang$.subscribe((locale: string) => {
      this.locale = locale;
      this.dateTimeAdapter.setLocale(locale);
    });
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  /**
   * Сброс даты в календаре, до предыдущей выбранной
   */
  resetPicker() {
    this.dateTime = this.Value ? moment(this.Value) : moment();
    this.closePicker();
  }

  /**
   * Применение выбраной даты
   */
  applyChanges() {
    this.Value = this.momentValueTime(this.dateTime).format(this.workingFormat);
    this.onApply.emit(this.Value);
    this.closePicker();
  }

  /**
   * Get и Set для работы с внутренним value
   */
  get Value(): string {
    return this.value;
  }

  set Value(v: string) {
    this.changeValue(v);
  }

  /**
   * Дата которая отображается в input
   */
  get inputValue() {
    return this.value ? moment(this.value).format(this.showFormat) : null;
  }

  /**
   * Обновляет дату по изменению ее в input руками
   * @param value
   */
  setInputValue(value: string) {

    const date = moment(value, this.showFormat);

    if (value && date.isValid()) {
      this.dateTime.date(date.date());
      this.dateTime.month(date.month());
      this.dateTime.year(date.year());
      this.dateTime.hour(date.hour());
      this.dateTime.minutes(date.minutes());
      this.dateTime.seconds(date.seconds());
      this.Value = this.momentValueTime(this.dateTime).format();
      this.onApply.emit(this.Value);
    }

    this.dateInput.nativeElement.value = this.value ? moment(this.value).format(this.showFormat) : null;
  }

  /**
   * Обновление данных компоненты
   * @param value
   */
  writeValue(value: string) {
    if (value) {
      this.dateTime = this.momentValueTime(value);
      this.Value = this.momentValueTime(value).format(this.workingFormat);
    } else {
      this.Value = null;
    }
  }

  showPicker() {
    this.isOpened = true;
  }

  closePicker() {
    this.isOpened = false;
  }

  /**
   * Конвертирует дату для работы по utc, используется и при получении данных и при отдаче
   * @param date
   */
  momentValueTime(date: Moment | string) {
    return moment(date, this.workingFormat).utc(true).startOf('day');
  }

  /**
   * Слушает клики на странице, в случае срабатывания не на календаре, закрывает его
   * @param target
   */
  @HostListener('document:click', ['$event.target'])
  private clickOverlay(target: any) {
    if (!this.pickerHtml.nativeElement.contains(target) && document.contains(target)) {
      this.resetPicker();
    }
  }

}
