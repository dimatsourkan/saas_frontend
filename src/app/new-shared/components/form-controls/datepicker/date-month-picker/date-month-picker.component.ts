import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { SHOW_DATE_FORMATS } from '@app/core/enums/date-formats';
import { BaseDatePickerComponent } from '@app/new-shared/components/form-controls/datepicker/base-date-picker.component';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';

const CUSTOM_FORMATS = {
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'DD-MM-YYYY HH:mm:ss',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'DD-MM-YYYY HH:mm:ss',
  monthYearA11yLabel: 'DD-MM-YYYY HH:mm:ss'
};

@Component({
  selector: 'date-month-picker',
  templateUrl: './date-month-picker.component.html',
  styleUrls: ['./date-month-picker.component.scss'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: CUSTOM_FORMATS},
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateMonthPickerComponent),
      multi: true
    }
  ]
})
export class DateMonthPickerComponent extends BaseDatePickerComponent {

  /**
   * Формат для отображения выбранных дат в зависимости от языка
   */
  get showFormat() {
    return SHOW_DATE_FORMATS[this.locale].DATEMONTH_PICKER;
  }

  chosenMonthHandler($event: any) {
    this.dateTime = $event;
    this.applyChanges();
  }

}
