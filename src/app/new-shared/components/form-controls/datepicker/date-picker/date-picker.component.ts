import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseDatePickerComponent } from '@app/new-shared/components/form-controls/datepicker/base-date-picker.component';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';

const CUSTOM_FORMATS = {
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'DD-MM-YYYY HH:mm:ss',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'DD-MM-YYYY HH:mm:ss',
  monthYearA11yLabel: 'DD-MM-YYYY HH:mm:ss'
};

@Component({
  selector: 'date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: CUSTOM_FORMATS},
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerComponent),
      multi: true
    }
  ]
})
export class DatePickerComponent extends BaseDatePickerComponent {

  /**
   * Если true то будет показан input, иначе текстовое поле
   */
  @Input() inputType = true;

}
