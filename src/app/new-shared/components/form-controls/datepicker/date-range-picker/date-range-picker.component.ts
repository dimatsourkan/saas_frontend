import { Component, ElementRef, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { SHOW_DATE_FORMATS } from '@app/core/enums/date-formats';
import { LanguageService } from '@app/shared/services/language/language.service';
import * as moment from 'moment';
import { Moment } from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';
import { Subscription } from 'rxjs';

export enum DATEPICKER_FILTERS {
  TODAY        = 'today',
  YESTERDAY    = 'yesterday',
  LAST_WEEK    = 'last week',
  LAST_MONTH   = 'last month',
  LAST_3_MONTH = 'last 3 month',
}

const CUSTOM_FORMATS = {
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'DD-MM-YYYY HH:mm:ss',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'DD-MM-YYYY HH:mm:ss',
  monthYearA11yLabel: 'DD-MM-YYYY HH:mm:ss'
};

@Component({
  selector: 'date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.scss'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: CUSTOM_FORMATS}
  ]
})
export class DateRangePickerComponent implements OnDestroy, OnInit {

  @ViewChild('pickerHtml') pickerHtml: ElementRef;
  isOpen = false;
  FILTERS = DATEPICKER_FILTERS;
  filteredBy: string;

  /**
   * Формат для отображения выбранных дат
   */
  get showFormat() {
    return SHOW_DATE_FORMATS[this.locale].FILTER_CALENDAR;
  }

  /**
   * Формат с которым компонента принимает и отдает даты
   */
  dateFormat = 'YYYY-MM-DD';

  @Output() onApply = new EventEmitter<{ startDate: string, endDate: string }>();

  /**
   * Параметры которые принимает компонента
   */
  private _startDate: Moment;
  @Input() set startDate(start: string) {
    this._startDate = moment(start, this.dateFormat);
    this.dateTimeRange[0] = moment(start, this.dateFormat);
  }

  get startDateMoment() {
    return this._startDate;
  }

  private _endDate: Moment;
  @Input() set endDate(end: string) {
    this._endDate = moment(end, this.dateFormat);
    this.dateTimeRange[1] = moment(end, this.dateFormat);
  }

  get endDateMoment() {
    return this._endDate;
  }

  /**
   * Нужны для более удобной работы
   */
  private _dateTimeRange: [Moment, Moment] = [moment(), moment()];

  get dateTimeRange() {
    return this._dateTimeRange;
  }

  set dateTimeRange(dates: [Moment, Moment]) {
    this._dateTimeRange = dates;
    this.filteredBy = null;
  }

  private subscription$: Subscription;
  private locale: string;

  constructor(
    private dateTimeAdapter: DateTimeAdapter<any>,
    private languageService: LanguageService
  ) {

  }

  /**
   * При изменении языка, меняет язык календаря
   */
  ngOnInit() {
    this.subscription$ = this.languageService.lang$.subscribe((locale: string) => {
      this.locale = locale;
      this.dateTimeAdapter.setLocale(locale);
      this.dateTimeAdapter.setLocale(locale);
    });
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  /**
   * Слушает клики на странице, в случае срабатывания не на календаре, закрывает его
   * @param target
   */
  @HostListener('document:click', ['$event.target'])
  private clickOverlay(target: any) {
    if (!this.pickerHtml.nativeElement.contains(target) && document.contains(target)) {
      this.resetPicker();
    }
  }

  /**
   * Применяет выбранные даты, и отдает наружу
   */
  applyChanges() {
    this.hidePicker();
    this.onApply.emit({
      startDate: this.dateTimeRange[0].format(this.dateFormat),
      endDate: this.dateTimeRange[1].format(this.dateFormat)
    });
  }

  /**
   * Обрабатывают фильтры
   */
  private setTodayFilter() {
    this.dateTimeRange = [
      moment(),
      moment()
    ];
  }

  private setYesterdayFilter() {
    this.dateTimeRange = [
      moment().subtract(1, 'day'),
      moment().subtract(1, 'day')
    ];
  }

  private setLastWeekFilter() {
    this.dateTimeRange = [
      moment().subtract(6, 'days').startOf('isoWeek'),
      moment().subtract(6, 'days').endOf('isoWeek')
    ];
  }

  private setLastMonthFilter() {
    this.dateTimeRange = [
      moment().subtract(1, 'month').startOf('month'),
      moment().subtract(1, 'month').endOf('month')
    ];
  }

  private setLast3MonthFilter() {
    this.dateTimeRange = [
      moment().subtract(3, 'month').startOf('month'),
      moment().subtract(1, 'month').endOf('month')
    ];
  }

  public setFilter(filter: string) {
    switch (filter) {
      case this.FILTERS.TODAY :
        this.setTodayFilter();
        break;
      case this.FILTERS.YESTERDAY :
        this.setYesterdayFilter();
        break;
      case this.FILTERS.LAST_WEEK :
        this.setLastWeekFilter();
        break;
      case this.FILTERS.LAST_MONTH :
        this.setLastMonthFilter();
        break;
      case this.FILTERS.LAST_3_MONTH :
        this.setLast3MonthFilter();
        break;
    }
    this.filteredBy = filter;
    this.applyChanges();
  }

  /**
   * Сбрасывает все изменения
   */
  public resetPicker() {
    this.dateTimeRange = [this._startDate, this._endDate];
    this.hidePicker();
  }

  hidePicker() {
    this.isOpen = false;
  }

  showPicker() {
    this.isOpen = true;
  }

  togglePicker() {
    this.isOpen = !this.isOpen;
  }

}
