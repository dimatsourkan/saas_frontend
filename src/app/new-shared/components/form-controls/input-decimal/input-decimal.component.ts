import { Component, forwardRef, Injector, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';
import { InputTextComponent } from '@app/new-shared/components/form-controls/input-text/input-text.component';

@Component({
  selector: 'input-decimal',
  templateUrl: './input-decimal.component.html',
  styleUrls: ['./input-decimal.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputDecimalComponent),
      multi: true
    }
  ]
})
export class InputDecimalComponent extends BaseControlValueAccessor {

  @ViewChild('inputComponent') private inputComponent: InputTextComponent;

  options = {
    alias: 'decimal',
    min: null,
    max: null,
    digits: '*',
    rightAlign: false,
    greedy: true,
    unmaskAsNumber: true
  };

  @Input()
  set max(max: number) {
    this.options.max = max;
  }

  @Input()
  set min(min: number) {
    this.options.min = min;
  }

  @Input()
  set digits(digits: number) {
    this.options.digits = digits.toString();
  }

  constructor(protected injector: Injector) {
    super(injector);

  }

  changeValue(value: string) {
    const val = value ? parseFloat(value) : null;
    super.changeValue(val);
  }

}
