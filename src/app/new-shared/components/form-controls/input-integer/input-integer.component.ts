import { Component, forwardRef, Injector, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';
import { InputTextComponent } from '@app/new-shared/components/form-controls/input-text/input-text.component';

@Component({
  selector: 'input-integer',
  templateUrl: './input-integer.component.html',
  styleUrls: ['./input-integer.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputIntegerComponent),
      multi: true
    }
  ]
})
export class InputIntegerComponent extends BaseControlValueAccessor {

  @ViewChild('inputComponent') private inputComponent: InputTextComponent;

  options = {
    alias: 'integer',
    min: null,
    max: null,
    rightAlign: false
  };

  @Input()
  set max(max: number) {
    this.options.max = max;
  }

  @Input()
  set min(min: number) {
    this.options.min = min;
  }

  constructor(protected injector: Injector) {
    super(injector);
  }

  changeValue(value: string) {
    const val = value ? parseInt(value, 10) : null;
    super.changeValue(val);
  }
}
