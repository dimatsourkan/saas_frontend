import { AfterViewInit, Component, forwardRef, Injector, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { checkKeyboardEventByRegexp } from '@app/core/helpers/helpers';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';
import * as Inputmask from 'inputmask/dist/inputmask/inputmask';
import 'inputmask/dist/inputmask/inputmask.extensions';
import 'inputmask/dist/inputmask/inputmask.numeric.extensions';

@Component({
  selector: 'input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputTextComponent),
      multi: true
    }
  ]
})
export class InputTextComponent extends BaseControlValueAccessor implements AfterViewInit, OnChanges {

  @Input() type: string = 'text';

  @Input() mask: string = null;

  @Input() regexp: RegExp | RegExp[] = null;

  @Input() options: any = {};

  @ViewChild('input') private input: any;

  inputMasked: Inputmask;

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.maskInput();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ((changes.mask && !changes.mask.firstChange)) {
      this.maskInput();
    }
    if ((changes.options && !changes.options.firstChange)) {
      this.maskInput();
    }
  }

  maskInput() {

    if (this.inputMasked) {
      this.inputMasked.remove();
    }

    if (this.mask) {
      this.inputMasked = Inputmask(this.mask, this.options).mask(this.input.nativeElement);
    }

    if (this.options.alias && !this.inputMasked) {
      this.inputMasked = Inputmask(this.options).mask(this.input.nativeElement);
    }
  }

  onKeyPress(event: KeyboardEvent) {
    return checkKeyboardEventByRegexp(event, this.regexp);
  }
}
