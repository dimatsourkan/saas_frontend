import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitcherBtnComponent } from './switcher-btn.component';

describe('SwitcherBtnComponent', () => {
  let component: SwitcherBtnComponent;
  let fixture: ComponentFixture<SwitcherBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwitcherBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitcherBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
