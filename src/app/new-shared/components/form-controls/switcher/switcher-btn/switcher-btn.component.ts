import { AfterViewInit, Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';

@Component({
  selector: 'switcher-btn',
  templateUrl: './switcher-btn.component.html',
  styleUrls: ['./switcher-btn.component.scss']
})
export class SwitcherBtnComponent extends BaseControlValueAccessor implements AfterViewInit {

  @ViewChild('check') private check: ElementRef;

  @Input() name = '';
  @Input() type: 'danger' = null;

  get isChecked() {
    return this.check.nativeElement.checked;
  }

  get isDanger() {
    return this.type === 'danger';
  }

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngAfterViewInit() {
    const ngControl: NgControl = this.injector.get(NgControl, null);
    if (ngControl) {
      this.control = ngControl.control as FormControl;
    }
  }

}
