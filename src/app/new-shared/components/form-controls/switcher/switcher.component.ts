import { Component, forwardRef, Injector, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';

@Component({
  selector: 'switcher',
  templateUrl: './switcher.component.html',
  styleUrls: ['./switcher.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitcherComponent),
      multi: true
    }
  ]
})
export class SwitcherComponent extends BaseControlValueAccessor {

  @Input() display: 'inline' | 'block' = 'block';
  @Input() size: 'sm' | 'md' = 'md';

  constructor(protected injector: Injector) {
    super(injector);
  }

  get isInline() {
    return this.display === 'inline';
  }

}
