import { AfterViewInit, Component, ElementRef, forwardRef, Injector, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { checkKeyboardEventByRegexp } from '@app/core/helpers/helpers';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';
import * as Inputmask from 'inputmask/dist/inputmask/inputmask';
import 'inputmask/dist/inputmask/inputmask.extensions';

@Component({
  selector: 'tags-input',
  templateUrl: './tags-input.component.html',
  styleUrls: ['./tags-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TagsInputComponent),
      multi: true
    }
  ]
})
export class TagsInputComponent extends BaseControlValueAccessor implements AfterViewInit {

  value: string[] = [];
  currentInputText = '';
  isFocused = false;

  @Input() regexp: RegExp | RegExp[] = null;
  @Input() mask: string = null;
  @Output() removed = new EventEmitter<number>();
  @ViewChild('tagInput') private tagInput: ElementRef;

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();

    if (this.mask) {
      Inputmask(this.mask, {}).mask(this.tagInput.nativeElement);
    }
  }

  addTag() {
    const validMask = this.mask ? Inputmask.isValid(this.currentInputText, this.mask) : true;
    if (this.currentInputText && validMask) {
      this.value.push(this.currentInputText);
      this.currentInputText = '';
      this.changeValue(this.value);
    }
  }

  removeLastTag() {
    if (!this.currentInputText) {
      this.value.splice(this.value.length - 1, 1);
      this.removed.emit(this.value.length - 1);
      this.changeValue(this.value);
    }
  }

  removeByIndex(index: number) {
    this.value.splice(index, 1);
    this.removed.emit(index);
    this.changeValue(this.value);
  }

  onKeyPress(event: KeyboardEvent) {
    return checkKeyboardEventByRegexp(event, this.regexp);
  }

}
