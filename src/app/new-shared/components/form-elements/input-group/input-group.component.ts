import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'input-group',
  templateUrl: './input-group.component.html',
  styleUrls: ['./input-group.component.scss']
})
export class InputGroupComponent {

  /**
   * Указывает на то где будет находиться иконка или кнопка
   */
  @Input()
  order: 'toLeft' = null;

  /**
   * Указывает на текущий стиль группы, либо с кнопкой, либо с наложеной иконкой
   */
  @Input()
  styleType: 'symbol' | 'button' = 'button';

  /**
   * Название иконки font-awesome без приставки fa
   */
  @Input()
  iconName: string = '';

  /**
   * Отрабатывает клик при нажатии на кнопку
   */
  @Output()
  onClick = new EventEmitter<any>();

  get toLeft() {
    return this.order === 'toLeft';
  }

  get isSymbol() {
    return this.styleType === 'symbol';
  }

  click() {
    this.onClick.emit();
  }

}
