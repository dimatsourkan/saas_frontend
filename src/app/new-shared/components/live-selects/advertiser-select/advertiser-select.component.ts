import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { AdvertiserService } from '@app/core/entities/advertiser/advertiser.service';
import { Advertiser } from '@app/core/entities/advertiser/advertiser.model';

@Component({
  selector: 'advertiser-select',
  template: `
    <div class="position-relative">
      <ng-select #ngSelect [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [clearable]="clearable"
                 [multiple]="multiple"
                 [typeahead]="typeahead"
                 [virtualScroll]="true"
                 (scrollToEnd)="onScroll()"
                 bindValue="id"
                 bindLabel="username">
      </ng-select>
      <validation-message [control]="control" [target]="ngSelect?.filterInput?.nativeElement"></validation-message>
    </div>
  `,
  styles: [`
    :host {
      display: block;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AdvertiserSelectComponent),
      multi: true
    }
  ]
})
export class AdvertiserSelectComponent extends BaseLiveSelectComponent<Advertiser> {
  dataService = this.injector.get(AdvertiserService, null);
}
