import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertiserTermsSelectComponent } from './advertiser-terms-select.component';

describe('SelectComponent', () => {
  let component : AdvertiserTermsSelectComponent;
  let fixture : ComponentFixture<AdvertiserTermsSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations : [AdvertiserTermsSelectComponent]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserTermsSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
