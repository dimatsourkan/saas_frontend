import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { BaseData } from '@app/core/models/base-data/base.data';
import { BaseModel } from '@app/core/models/base-model/base.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { CRUDService } from '@app/core/services/crud/crud.service';
import { FilterService } from '@app/core/services/filter/filter.service';
import { FormControlsComponent } from '@app/shared/components/form-controls/base-components/form-controls.component';
import findIndex from 'lodash-es/findIndex';
import { Subscription } from 'rxjs';
import { debounceTime, finalize, tap } from 'rxjs/operators';

/**
 * Прямое использование этой компоненты недопустимо, она используется только для наследования
 */
@Component({
  selector: '',
  template: ''
})
export class BaseLiveSelectComponent<T extends BaseModel> extends FormControlsComponent implements OnInit, OnDestroy {

  loading = false;
  typeahead = new EventEmitter<string>();
  items: T[] = [];
  subcription$: Subscription;
  dataService: CRUDService<T, BaseData<T>>;
  totalPages = 1;
  filterFirstChange = false;

  @Input() limit = 10;
  @Input() searchable = true;
  @Input() initOnStart = false;
  @Input() multiple = false;
  @Input() clearable = false;
  @Input() filter = (new FilterService()).init();
  @Output() change = new EventEmitter();

  ngOnInit() {
    this.initTypehead();
    this.initFilterListener();
    if (this.initOnStart) {
      this.getData().subscribe();
    }
  }

  ngOnDestroy() {
    if (this.subcription$) {
      this.subcription$.unsubscribe();
    }
  }

  /**
   * Получение списка данных с сервера
   */
  getData() {
    this.loading = true;
    return this.dataService.getKeyList(this.filter.filter)
    .pipe(finalize(() => this.loading = false))
    .pipe(tap(res => this.pushItems(res)));
  }

  /**
   * Получение одной модели по id
   * @param id
   */
  getOne(id: string) {
    this.loading = true;
    return this.dataService.get(id)
    .pipe(finalize(() => this.loading = false));
  }

  /**
   * При записи данных в контрол проверяется если элемент есть в списке то ничего не надо делать
   * Если его нет, то запрашивается с сервера
   * @param id
   */
  setOne(id) {
    if (this.getItemIndex(id) < 0) {
      this.getOne(id).subscribe(res => this.items = [...this.items, res]);
    }
  }

  /**
   * При получении дополнительных страниц добавляет данные в текущий массив
   * @param result
   */
  pushItems(result: ResultList<T>) {
    this.totalPages = result.totalPages;
    if (this.filter.page === 1) {
      this.items = result.items;
    } else {
      this.items = [...this.items, ...result.items];
    }
  }

  /**
   * Вызывается при изменении в селекте
   */
  onChange() {
    this.value = this.control.value;
    this.changeValue(this.value);
    this.change.emit(this.value);
  }

  /**
   * Вызывается при открытии селекта
   */
  onOpen() {
    this.filter.limit = this.limit;
    this.filter.search = '';
    this.filter.page = 1;
    this.items = [];
  }

  /**
   * Инициирует прослушивание изменений в фильтре
   */
  initFilterListener() {
    this.filter.onChangeFilter$.subscribe(() => {
      if (this.filterFirstChange) {
        this.getData().subscribe();
      }
      this.filterFirstChange = true;
    });
  }

  /**
   * Инициирует поиск в селекте
   */
  initTypehead() {
    this.typeahead
    .pipe(debounceTime(200))
    .subscribe(term => {
      this.filter.search = term;
      this.filter.page = 1;
    });
  }

  /**
   * Записывает данные в контрол
   * @param value
   */
  writeValue(value: any) {

    // Если массив, то каждый id запрашивается с сервера
    // Если одно значение то будет только один запрос
    if (Array.isArray(value)) {
      if (value.length) {
        value.forEach(this.setOne.bind(this));
      }
    } else {
      if (value && value !== this.value) {
        this.setOne(value);
      }
    }

    super.writeValue(value);
  }

  /**
   * Получает индекс элемента в текущем списке данных, если нет то -1
   * @param id
   */
  getItemIndex(id: string) {
    return findIndex(this.items, (i: BaseModel) => i.id === id);
  }

  /**
   * Вызывается при скролле списка
   */
  onScroll() {
    if (this.totalPages > this.filter.page) {
      this.filter.page = this.filter.page + 1;
    }
  }
}
