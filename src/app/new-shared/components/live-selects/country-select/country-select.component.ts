import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { BaseLiveSelectComponent } from '../base-live-select.component';
import { Country } from '@app/core/entities/country/country.model';
import { CountryService } from '@app/core/entities/country/country.service';

@Component({
  selector: 'country-select',
  template: `
    <div class="position-relative">
      <ng-select #ngSelect [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [clearable]="clearable"
                 [multiple]="multiple"
                 [searchFn]="customSearchFn"
                 bindValue="code"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control" [target]="ngSelect?.filterInput?.nativeElement"></validation-message>
    </div>
  `,
  styles: [`
    :host {
      display: block;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CountrySelectComponent),
      multi: true
    }
  ]
})
export class CountrySelectComponent extends BaseLiveSelectComponent<Country> {

  dataService = this.injector.get(CountryService, null);

  private isArray(value: any) {
    return Array.isArray(value) && value.length;
  }

  private isNotArray(value: any) {
    return !Array.isArray(value) && value;
  }

  writeValue(value: any): any {

    if ((this.isArray(value) || this.isNotArray(value)) && value !== this.value) {
      this.getData().subscribe();
    }

    this.value = value;
  }

  customSearchFn(term: string, item: any) {
    term = term.toUpperCase();
    if (term.length === 2) {
      return item.code === term;
    } else {
      return item.name.toUpperCase().indexOf(term) > -1 || item.code.toUpperCase() === term;
    }
  }

}
