import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { Currency } from '@app/core/entities/currency/currency.model';
import { CurrencyService } from '@app/core/entities/currency/currency.service';

@Component({
  selector: 'currency-select',
  template: `
    <div class="position-relative">
      <ng-select #ngSelect [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [clearable]="clearable"
                 [multiple]="multiple"
                 [searchable]="searchable"
                 bindValue="id"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control" [target]="ngSelect?.filterInput?.nativeElement"></validation-message>
    </div>
  `,
  styles: [`
    :host {
      display: block;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CurrencySelectComponent),
      multi: true
    }
  ]
})
export class CurrencySelectComponent extends BaseLiveSelectComponent<Currency> {

  dataService = this.injector.get(CurrencyService, null);

  onOpen() {
    this.getData().subscribe();
  }

  writeValue(value: any): any {

    if (
      (
        (Array.isArray(value) && value.length) ||
        (!Array.isArray(value) && value)
      ) && value !== this.value
    ) {
      this.getData().subscribe();
    }

    this.value = value;
  }

}
