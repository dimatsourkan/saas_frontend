import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { finalize, tap } from 'rxjs/operators';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { ManagerService } from '@app/core/entities/manager/manager.service';

@Component({
  selector: 'manager-roles-select',
  template: `
    <div class="position-relative">
      <ng-select #ngSelect [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [clearable]="clearable"
                 [multiple]="multiple"
                 bindValue="id"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control" [target]="ngSelect?.filterInput?.nativeElement"></validation-message>
    </div>
  `,
  styles: [`
    :host {
      display: block;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ManagerRolesSelectComponent),
      multi: true
    }
  ]
})
export class ManagerRolesSelectComponent extends BaseLiveSelectComponent<any> {
  dataService = this.injector.get(ManagerService, null);

  getData() {
    this.loading = true;
    return this.dataService.getManagersRoles()
    .pipe(finalize(() => this.loading = false))
    .pipe(tap(res => this.pushItems(res)));
  }

  writeValue(value: any): any {

    if (value) {
      this.getData().subscribe();
    }

    this.value = value;
  }
}
