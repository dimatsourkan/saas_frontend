import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerSelectComponent } from './manager-select.component';

describe('SelectComponent', () => {
  let component : ManagerSelectComponent;
  let fixture : ComponentFixture<ManagerSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations : [ManagerSelectComponent]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
