import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { BaseLiveSelectComponent } from '../base-live-select.component';
import { ManagerService } from '@app/core/entities/manager/manager.service';
import { Manager } from '@app/core/entities/manager/manager.model';

@Component({
  selector: 'manager-select',
  template: `
    <div class="position-relative">
      <ng-select #ngSelect [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [clearable]="clearable"
                 [multiple]="multiple"
                 [typeahead]="typeahead"
                 [virtualScroll]="true"
                 (scrollToEnd)="onScroll()"
                 bindValue="id"
                 bindLabel="username">
      </ng-select>
      <validation-message [control]="control" [target]="ngSelect?.filterInput?.nativeElement"></validation-message>
    </div>
  `,
  styles: [`
    :host {
      display: block;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ManagerSelectComponent),
      multi: true
    }
  ]
})
export class ManagerSelectComponent extends BaseLiveSelectComponent<Manager> {
  dataService = this.injector.get(ManagerService, null);
}
