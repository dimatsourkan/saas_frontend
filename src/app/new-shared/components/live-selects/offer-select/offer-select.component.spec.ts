import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ManagerRolesSelectComponent } from '../manager-roles-select/manager-roles-select.component';


describe('SelectComponent', () => {
  let component: ManagerRolesSelectComponent;
  let fixture: ComponentFixture<ManagerRolesSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManagerRolesSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerRolesSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
