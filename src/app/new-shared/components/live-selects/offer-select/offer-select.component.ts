import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { finalize, tap } from 'rxjs/operators';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { OfferService } from '@app/core/entities/offer/offer.service';
import { Component, forwardRef } from '@angular/core';

@Component({
  selector: 'offer-select',
  template: `
    <div class="position-relative">
      <ng-select #ngSelect [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [clearable]="clearable"
                 [multiple]="multiple"
                 [typeahead]="typeahead"
                 [virtualScroll]="true"
                 (scrollToEnd)="onScroll()"
                 bindValue="id"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control" [target]="ngSelect?.filterInput?.nativeElement"></validation-message>
    </div>
  `,
  styles: [`
    :host {
      display: block;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => OfferSelectComponent),
      multi: true
    }
  ]
})
export class OfferSelectComponent extends BaseLiveSelectComponent<any> {
  dataService = this.injector.get(OfferService, null);

  getData() {
    this.loading = true;
    return this.dataService.getOffersKeyListCache(this.filter.filter)
    .pipe(finalize(() => this.loading = false))
    .pipe(tap(res => this.pushItems(res)));
  }

  writeValue(value: any): any {
    if (value) {
      this.getData().subscribe();
    }
    this.value = value;
  }
}
