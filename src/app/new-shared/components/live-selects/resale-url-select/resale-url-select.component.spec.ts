import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PublisherSelectComponent } from '../publisher-select/publisher-select.component';


describe('SelectComponent', () => {
  let component: PublisherSelectComponent;
  let fixture: ComponentFixture<PublisherSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PublisherSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublisherSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
