import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { AdvertiserService } from '@app/core/entities/advertiser/advertiser.service';
import { Advertiser } from '@app/core/entities/advertiser/advertiser.model';
import { PublisherUrl } from '@app/core/entities/superlink/publisher-urls/publisher-urls.model';
import { PublisherUrlService } from '@app/core/entities/superlink/publisher-urls/publisher-urls.service';
import { finalize, tap } from 'rxjs/operators';
import { Common } from '@app/core/models/common-model/common.model';

@Component({
  selector: 'resale-url-select',
  template: `
    <div class="position-relative">
      <ng-select #ngSelect [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [clearable]="clearable"
                 [multiple]="multiple"
                 [typeahead]="typeahead"
                 [virtualScroll]="true"
                 (scrollToEnd)="onScroll()"
                 bindValue="id"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control" [target]="ngSelect?.filterInput?.nativeElement"></validation-message>
    </div>
  `,
  styles: [`
    :host {
      display: block;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ResaleUrlSelectComponent),
      multi: true
    }
  ]
})
export class ResaleUrlSelectComponent extends BaseLiveSelectComponent<PublisherUrl> {
  dataService = this.injector.get(PublisherUrlService, null);

  getData() {
    this.loading = true;
    return this.dataService.getAlgotithmUrlNames()
      .pipe(finalize(() => this.loading = false))
      .pipe(tap(res => this.pushItems(res)));
  }

  writeValue(value: any): any {
    if (value) {
      this.getData().subscribe();
    }
    this.value = value;
  }
}
