import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { TimezoneService } from '@app/core/entities/timezone/timezone.service';
import { Timezone } from '@app/core/entities/timezone/timezone.model';

@Component({
  selector: 'timezone-select',
  template: `
    <div class="position-relative">
      <ng-select #ngSelect [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [clearable]="clearable"
                 [multiple]="multiple"
                 [searchFn]="searchTimezone"
                 bindValue="identifier">
        <ng-template ng-option-tmp let-item="item">
          {{item.timezone}} &mdash; {{item.identifier}}
        </ng-template>
        <ng-template ng-label-tmp let-item="item">
          {{item.timezone}} {{item.identifier}}
        </ng-template>
      </ng-select>
      <validation-message [control]="control" [target]="ngSelect?.filterInput?.nativeElement"></validation-message>
    </div>
  `,
  styles: [`
    :host {
      display: block;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TimezoneSelectComponent),
      multi: true
    }
  ]
})
export class TimezoneSelectComponent extends BaseLiveSelectComponent<Timezone> {

  dataService = this.injector.get(TimezoneService, null);

  writeValue(value: any): any {

    if (
      (
        (Array.isArray(value) && value.length) ||
        (!Array.isArray(value) && value)
      ) && value !== this.value
    ) {
      this.getData().subscribe();
    }

    this.value = value;
  }

  searchTimezone(term: string, item: any) {
    term = term.toUpperCase();
    const timezone = item.timezone.toUpperCase();
    const identifier = item.identifier.toUpperCase();
    return timezone.indexOf(term) >= 0 || identifier.indexOf(term) >= 0;
  }

}
