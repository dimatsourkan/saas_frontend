import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';

@Component({
  selector: 'confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent {

  @Input() title: string;
  @Input() loading = false;
  @Input() cancelText: string;
  @Input() applyText: string;
  @Output() onSuccess: EventEmitter<boolean> = new EventEmitter();
  @Output() onCancel: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('modal') private modal: ModalComponent;
  successCallback: Function;
  cancelCallback: Function;
  everCallback: Function;

  cancel() {
    this.close();
    this.onCancel.emit();
    if (this.cancelCallback) {
      this.cancelCallback();
    }
    if (this.everCallback) {
      this.everCallback();
    }
  }

  success() {
    this.onSuccess.emit();
    if (this.successCallback) {
      this.successCallback();
    }
    if (this.everCallback) {
      this.everCallback();
    }
  }

  open(successCallback ?: Function, cancelCallback ?: Function, everCallback ?: Function) {
    this.successCallback = successCallback;
    this.cancelCallback = cancelCallback;
    this.everCallback = everCallback;
    this.modal.open();
  }

  close() {
    this.modal.close();
  }

}
