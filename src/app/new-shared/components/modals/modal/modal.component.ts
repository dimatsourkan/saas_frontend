import { Component, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core';
import { modalFadeAnimation } from '@app/new-shared/components/modals/modal/modal.animation';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  animations: [modalFadeAnimation]
})
export class ModalComponent implements OnDestroy {

  @Input() size: 'sm' | 'md' | 'lg' | 'xlg' = 'md';

  @Output() onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('modal') private modal: ElementRef;
  @ViewChild('modalMove') private modalMove: ElementRef;
  @ViewChild('modalContainer') private modalContainer: ElementRef;

  _showed = false;

  get isOpen() {
    return this._showed;
  }

  /**
   * Костыли для правильного отображения, что бы попап не прыгал в стороны
   */
  private setHidden() {
    if (this.modal) {
      // this.modal.nativeElement.classList.add('overflow-hidden');
    }
  }

  private setVisible() {
    if (this.modal) {
      // this.modal.nativeElement.classList.remove('overflow-hidden');
    }
  }

  /**
   * Добавление паддинга к странице что бы не прыгал контент
   */
  private addBodyStyles() {
    document.body.classList.add('open-modal');
  }

  private removeBodyStyles() {
    if (!this.hasModalsOpen) {
      document.body.classList.remove('open-modal');
    }
  }

  /**
   * Перемежение попапа в body и обратно
   */
  private moveToBody() {
    document.querySelector('body').appendChild(this.modalMove.nativeElement);
  }

  private moveBack() {
    this.modalContainer.nativeElement.appendChild(this.modalMove.nativeElement);
  }

  _animationStart(event) {
    if (event.fromState === true) {
      this.setHidden();
    }
  }

  _animationDone(event) {
    this.setVisible();
    if (event.fromState === true) {
      this.moveBack();
      this.onClose.emit();
      this.removeBodyStyles();
    }
  }

  open() {
    this._showed = true;
    this.moveToBody();
    this.addBodyStyles();

  }

  close() {
    this.setHidden();
    this._showed = false;
  }

  toggle() {
    this._showed ? this.close() : this.open();
  }

  clickOverlay(target: HTMLElement) {
    if (target.classList[0] === 'modal-container') {
      this.close();
    }
  }

  get hasModalsOpen() {
    return document.querySelectorAll('body > .modal-container-move-js').length > 1;
  }

  ngOnDestroy() {
    this.moveBack();
    this.removeBodyStyles();
  }

}
