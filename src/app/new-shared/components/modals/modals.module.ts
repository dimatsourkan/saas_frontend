import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonsModule } from '@app/new-shared/components/buttons/buttons.module';
import { CardModule } from '@app/new-shared/components/card/card.module';
import { ConfirmComponent } from '@app/new-shared/components/modals/confirm/confirm.component';
import { ModalHeaderComponent } from '@app/new-shared/components/modals/modal/modal-header/modal-header.component';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { TranslateModule } from '@ngx-translate/core';
import { ModalBodyComponent } from './modal/modal-body/modal-body.component';
import { ModalFooterComponent } from './modal/modal-footer/modal-footer.component';

@NgModule({
  declarations: [
    ModalComponent,
    ModalHeaderComponent,
    ModalBodyComponent,
    ModalFooterComponent,
    ConfirmComponent
  ],
  exports: [
    ModalComponent,
    ModalHeaderComponent,
    ModalBodyComponent,
    ModalFooterComponent,
    ConfirmComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    ButtonsModule,
    TranslateModule
  ]
})
export class ModalsModule { }
