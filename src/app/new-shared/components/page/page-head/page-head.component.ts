import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'page-head',
  templateUrl: './page-head.component.html',
  styleUrls: ['./page-head.component.scss']
})
export class PageHeadComponent implements OnInit {

  @Input() horisontalAligment: string = '';
  @Input() verticalAligment: string = '';

  constructor() { }

  ngOnInit() {
  }

}
