import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageScrollableContainerComponent } from './page-scrollable-container.component';

describe('PageScrollableContainerComponent', () => {
  let component: PageScrollableContainerComponent;
  let fixture: ComponentFixture<PageScrollableContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageScrollableContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageScrollableContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
