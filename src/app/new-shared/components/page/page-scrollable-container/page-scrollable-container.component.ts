import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'page-scrollable-container',
  templateUrl: './page-scrollable-container.component.html',
  styleUrls: ['./page-scrollable-container.component.scss']
})
export class PageScrollableContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
