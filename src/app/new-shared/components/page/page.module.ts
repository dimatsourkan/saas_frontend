import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullPageComponent } from './full-page/full-page.component';
import { PageHeadComponent } from './page-head/page-head.component';
import { PageBodyComponent } from './page-body/page-body.component';
import { PageScrollableContainerComponent } from './page-scrollable-container/page-scrollable-container.component';

@NgModule({
  declarations: [
    FullPageComponent,
    PageHeadComponent,
    PageBodyComponent,
    PageScrollableContainerComponent
  ],
  exports: [
    FullPageComponent,
    PageHeadComponent,
    PageBodyComponent,
    PageScrollableContainerComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PageModule { }
