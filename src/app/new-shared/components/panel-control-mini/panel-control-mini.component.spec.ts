import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelControlMiniComponent } from './panel-control-mini.component';

describe('PanelControlMiniComponent', () => {
  let component: PanelControlMiniComponent;
  let fixture: ComponentFixture<PanelControlMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelControlMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelControlMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
