import { Component, Input } from '@angular/core';

@Component({
  selector: 'table-body',
  templateUrl: './table-body.component.html',
  styleUrls: ['./table-body.component.scss']
})
export class TableBodyComponent {
  @Input() striped: boolean = false;
  @Input() separated: boolean = false;
  @Input() row: boolean = false;
  @Input() bordered: boolean = false;
}
