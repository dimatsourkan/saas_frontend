import { Component, Input } from '@angular/core';
import { Pagination } from '@app/core/models/pagination-model/pagination.model';
import { FilterService } from '@app/core/services/filter/filter.service';

@Component({
  selector: 'table-control',
  templateUrl: './table-control.component.html',
  styleUrls: ['./table-control.component.scss']
})
export class TableControlComponent {

  @Input() filter: FilterService;
  @Input() pagination: Pagination<any>;

}
