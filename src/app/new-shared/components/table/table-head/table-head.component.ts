import {Component, Input} from '@angular/core';
import { scrollbarWidth } from '@app/core/helpers/helpers';

@Component({
  selector: 'table-head',
  templateUrl: './table-head.component.html',
  styleUrls: ['./table-head.component.scss']
})
export class TableHeadComponent {
  @Input() striped: boolean = false;
  @Input() separated: boolean = false;
  @Input() row: boolean = false;
  @Input() bordered: boolean = false;

  scrollbarWidth = scrollbarWidth;
}
