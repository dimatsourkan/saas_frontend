import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FilterService } from '../../../../core/services/filter/filter.service';
import { LanguageService } from '../../../../shared/services/language/language.service';

@Component({
  selector: 'table-limit',
  templateUrl: './table-limit.component.html',
  styleUrls: ['./table-limit.component.scss']
})
export class TableLimitComponent implements OnInit {

  @Input() limit: number;
  @Input() filter: FilterService;
  @Output() changedLimit: EventEmitter<number> = new EventEmitter<number>();
  pageLimits: any = [10, 25, 50, 75, 100];

  ngOnInit() {
    if (this.filter) {
      this.limit = this.filter.limit;
    }
  }

  tableLimit() {
    this.changedLimit.emit(this.limit);
    if (this.filter) {
      this.filter.limit = this.limit;
    }
  }
}
