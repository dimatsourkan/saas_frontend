import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FilterService } from '../../../../core/services/filter/filter.service';

@Component({
  selector: 'table-pagination',
  templateUrl: './table-pagination.component.html',
  styleUrls: ['./table-pagination.component.scss']
})
export class TablePaginationComponent implements OnInit, OnChanges {
  @Input() filter: FilterService;
  @Input() totalPages: number;
  @Input() page: number;
  @Output() currentPage: EventEmitter<number> = new EventEmitter<number>();
  inputPage: number;

  ngOnInit() {
    this.inputPage = this.page;
    if (this.filter) {
      this.page = this.filter.page;
      this.inputPage = this.filter.page;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.page) {
      this.inputPage = changes.page.currentValue;
    }
  }

  goToPage(page: number) {
    this.currentPage.emit(page);
    if (this.filter) {
      this.filter.page = page;
    }
  }

  submitOnBlur(inputPage: number) {
    if (this.page !== inputPage && inputPage !== 0 && inputPage <= this.totalPages) {
      this.goToPage(inputPage);
    } else {
      this.inputPage = this.page;
    }
  }

  submitOnEnter(event) {
    const code = (event.keyCode ? event.keyCode : event.which);
    if (code === 13) {
      const currentPage: number = this.inputPage > this.totalPages ? this.totalPages : this.inputPage;
      if (this.page !== currentPage && currentPage !== 0) {
        this.page = currentPage;
        this.goToPage(this.page);
      }
    }
  }
}
