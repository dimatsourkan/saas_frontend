import { Component } from '@angular/core';
import { scrollbarWidth } from '@app/core/helpers/helpers';

@Component({
  selector: 'table-total',
  templateUrl: './table-total.component.html',
  styleUrls: ['./table-total.component.scss']
})
export class TableTotalComponent {
  scrollbarWidth = scrollbarWidth;
}
