import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableLimitComponent } from '@app/new-shared/components/table/table-limit/table-limit.component';
import { TablePaginationComponent } from '@app/new-shared/components/table/table-pagination/table-pagination.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { TableBodyComponent } from './table-body/table-body.component';
import { TableHeadComponent } from './table-head/table-head.component';
import { TableControlComponent } from './table-control/table-control.component';
import { TableTotalComponent } from './table-total/table-total.component';

@NgModule({
  declarations: [
    TableHeadComponent,
    TableBodyComponent,
    TableTotalComponent,
    TableControlComponent,
    TableLimitComponent,
    TablePaginationComponent
  ],
  exports: [
    TableHeadComponent,
    TableBodyComponent,
    TableTotalComponent,
    TableControlComponent,
    TableLimitComponent,
    TablePaginationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ]
})
export class TableModule {
}
