import { AfterViewInit, Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import ValidationText from '@app/shared/components/validation/validations';
import { LanguageService } from '@app/shared/services/language/language.service';

@Component({
  selector: 'validation-message',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})

export class ValidationComponent implements AfterViewInit, OnDestroy, OnChanges {

  @Input() trigger: string = 'focus';
  @Input() target: Element = null;
  @Input() control: FormControl|AbstractControl;
  @Input() showed = false;

  lang: string;
  validationText = ValidationText;

  constructor(private languageService: LanguageService) {
    this.languageService.getLang().subscribe((lang) => {
      this.lang = lang;
    });
  }

  ngAfterViewInit() {
    this.initTargetListener();
  }

  ngOnChanges() {
    this.initTargetListener();
  }

  ngOnDestroy() {
    if (this.target) {
      this.target.removeEventListener('focus', () => {
      });
    }
  }

  get validationErrors() {

    if (!this.control || !this.control.errors) {
      return null;
    }

    return Object.keys(this.control.errors).map((key) => {
      if (typeof this.control.errors[key] === 'object') {
        return this.parseObjectError(key, this.control.errors[key]);
      } else {
        return this.getError(key);
      }
    });
  }

  parseObjectError(errorType: string, error: any) {

    let errText: string = this.validationText[this.lang][errorType];

    if (errText) {
      errText = errText.replace(`{{${errorType}}}`, error[errorType]);
      return errText;
    } else {
      return errorType;
    }
  }

  getError(errCode: string) {

    if (!this.validationText[this.lang][errCode]) {
      return errCode;
    }

    return this.validationText[this.lang][errCode];
  }

  private initTargetListener() {
    /**
     * Костыль для ng-select что бы ng-touched устанавливался при первом открытии селекта
     * По умолчанию он ставится после потери фокуса
     * На всех остальных элементах ng-touched ставится при получении фокуса
     */
    if (this.target) {
      this.target.removeEventListener('focus', () => {});
      this.target.addEventListener('focus', (event) => {
        this.control.markAsTouched({ onlySelf : true });
        this.showed = true;
      });
    }
  }

}
