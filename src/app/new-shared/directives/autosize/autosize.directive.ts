import { AfterViewInit, Directive, ElementRef, EventEmitter, HostListener, OnDestroy, Output } from '@angular/core';
import * as autosize from 'autosize/dist/autosize.js';

@Directive({
  selector: '[autosize]'
})

export class AutosizeDirective implements AfterViewInit, OnDestroy {

  el: any;

  @Output() init: EventEmitter<any> = new EventEmitter<any>();

  timeInterval: any;

  constructor(private elem: ElementRef) {
    this.el = elem.nativeElement;

    this.timeInterval = setInterval(() => {
      autosize.update(this.el);
    }, 1000);
  }

  @HostListener('(ngModelChange)')
  onChangeArea() {
    autosize.update(this.el);
  }

  ngOnDestroy() {
    clearInterval(this.timeInterval);
  }

  ngAfterViewInit() {
    autosize(this.el);
  }

}
