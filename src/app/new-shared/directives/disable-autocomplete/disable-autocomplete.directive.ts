import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[disableAutocomplete]'
})
export class DisableAutocompleteDirective {

  private el: HTMLFormElement;

  constructor(private elem: ElementRef) {
    this.el = this.elem.nativeElement;
    this.setFakeInputs();
  }

  setFakeInputs() {
    const email: HTMLInputElement = document.createElement('input');
          email.className = 'fake-auth-input';
          email.type = 'email';

    const pass: HTMLInputElement = document.createElement('input');
          pass.className = 'fake-auth-input';
          pass.type = 'password';

    this.el.prepend(email);
    this.el.prepend(pass);
  }

}
