import { Directive, ElementRef, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { PermissionService } from '@app/shared/services/permission/permission.service';
import { DataStore } from '@app/core/entities/data-store.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[hasPermission]'
})
export class HasPermissionDirective implements OnInit, OnDestroy {

  private permissions = [];
  private subscription$: Subscription;

  @Input()
  set hasPermission(val) {
    this.permissions = val;
    this.updateView();
  }

  constructor(
    private permissionService: PermissionService,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {
  }

  ngOnInit(): void {
    this.subscription$ = DataStore.token.item.asObservable$.subscribe(() => {
      this.updateView();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  private updateView() {

    this.viewContainer.clear();

    if (this.checkPermission()) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }

  }

  private checkPermission() {
    return this.permissionService.includesPermissions(this.permissions);
  }
}
