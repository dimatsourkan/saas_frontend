import {
  Directive,
  ElementRef,
  EmbeddedViewRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Renderer2,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import tippy, { Instance, Options, Placement } from 'tippy.js';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[popover]',
  exportAs: 'popover'
})
export class PopoverDirective implements OnInit, OnDestroy, OnChanges {

  public $event: any;

  /**
   * Popover content
   */
  @Input() private popover: string | number | TemplateRef<any>;
  @Input() private context: Object | null;

  /**
   * Передается если требуется динамическое изменение контента
   */
  @Input() private changeDetectionData: any;

  /**
   * Tippy input options
   */
  @Input() private animation: 'fade' | 'scale' | 'shift-toward' | 'perspective' | 'shift-away' = 'shift-toward';
  @Input() private duration: number | [number, number] = [null, null];
  @Input() private delay: number | [number, number] = [null, null];
  @Input() private trigger: string = 'mouseenter focus';
  @Input() private triggerTarget: Element = null;
  @Input() private placement: Placement = 'auto';
  @Input() private hideOnClick: boolean = true;
  @Input() private showOnInit: boolean = false;
  @Input() private distance: number = 10;
  @Input() private appendTo: HTMLElement = document.body;
  @Input() private arrow: boolean = true;
  @Input() private theme: 'light' | 'error' | string = 'light';

  private tippy: Instance;
  private embeddedViewRef: EmbeddedViewRef<any>;
  private defaultOptions: Options = {
    ignoreAttributes: true,
    animateFill: false,
    arrowType: 'round',
    interactive: true,
    boundary: 'window'
  };

  get isOpen() {
    if (this.tippy) {
      return this.tippy.state.isVisible;
    } else {
      return false;
    }
  }

  constructor(
    private viewContainerRef: ViewContainerRef,
    private renderer2: Renderer2,
    private el: ElementRef
  ) {
    this.el = el;
  }

  ngOnInit() {

    this.tippy = tippy(this.el.nativeElement, {
      ...this.defaultOptions,
      triggerTarget: this.triggerTarget,
      hideOnClick: this.hideOnClick,
      showOnInit: this.showOnInit,
      animation: this.animation,
      placement: this.placement,
      appendTo: this.appendTo,
      duration: this.duration,
      distance: this.distance,
      trigger: this.trigger,
      arrow: this.arrow,
      theme: this.theme,
      delay: this.delay,
      onMount: (instance: Instance) => {
        this.changeContent(instance);
      },
      onHidden: () => {
        this.destroyView();
      },
    }) as Instance;

  }

  ngOnChanges() {
    this.changeContent(this.tippy);
  }

  ngOnDestroy() {
    this.destroyView();
    if (this.tippy) {
      this.tippy.destroy();
      this.tippy = null;
    }
  }

  changeContent(instance: Instance) {
    if (instance) {
      this.setStringContent(instance);
      this.setTemplateContent(instance);
      if (instance.popperInstance) {
        instance.popperInstance.scheduleUpdate();
      }
    }
  }

  public show($event) {
    this.$event = $event;
    this.tippy.show();
  }

  public hide() {
    this.tippy.hide();
  }

  /**
   * Устанавливает контент если передана строка
   */
  private setStringContent(instance: Instance) {
    if (typeof this.popover === 'string') {
      instance.setContent(this.popover);
    }

    if (typeof this.popover === 'number') {
      instance.setContent(this.popover.toString());
    }
  }

  /**
   * Устанавливает контент если передан ng-template
   */
  private setTemplateContent(instance: Instance) {
    if (this.popover instanceof TemplateRef) {
      const div = this.renderer2.createElement('div');
      this.embeddedViewRef = this.viewContainerRef.createEmbeddedView(this.popover, this.context || null);
      this.embeddedViewRef.rootNodes.forEach(n => div.appendChild(n));
      instance.setContent(div);
    }
  }

  /**
   * Очищает и удаляет отрендереный шаблон
   */
  private destroyView() {
    if (this.embeddedViewRef) {
      this.viewContainerRef.clear();
      this.embeddedViewRef.destroy();
      this.embeddedViewRef = null;
    }
  }

}
