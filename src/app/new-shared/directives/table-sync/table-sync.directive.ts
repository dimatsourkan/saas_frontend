import { AfterViewInit, Directive, ElementRef, Input, OnChanges, OnDestroy } from '@angular/core';
import { scrollbarWidth } from '@app/core/helpers/helpers';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Directive({
  selector: '[tableSync]'
})
export class TableSyncDirective implements AfterViewInit, OnChanges, OnDestroy {

  @Input() data: any;

  private tableTotalRow: HTMLTableRowElement;
  private tableHeadRow: HTMLTableRowElement;
  private resizeObservable$: Subscription;

  constructor(private element: ElementRef) {

  }

  ngAfterViewInit() {
    this.setHeaderCellWidth();
    this.resizeObservable$ = fromEvent(window, 'resize')
    .pipe(debounceTime(50))
    .subscribe(this.setHeaderCellWidth.bind(this));
  }

  ngOnChanges() {
    setTimeout(this.setHeaderCellWidth.bind(this));
  }

  ngOnDestroy() {
    if (this.resizeObservable$) {
      this.resizeObservable$.unsubscribe();
    }
  }

  setHeaderCellWidth() {

    if (!this.element) {
      return;
    }

    this.tableHeadRow = this.element.nativeElement.querySelector('.table-head-tableSync tr');
    this.tableTotalRow = this.element.nativeElement.querySelector('.table-total-tableSync tr');

    if (!this.tableHeadRow) {
      return;
    }

    const tableTr = this.element.nativeElement.querySelector('.table-body-tableSync tr');

    if (!tableTr) {
      return;
    }

    tableTr.querySelectorAll('td')
    .forEach((item, index: number) => {
      this.tableHeadRow.cells.item(index).style.width = `${item.clientWidth}px`;
      if (this.tableTotalRow) {
        this.tableTotalRow.cells.item(index).style.width = `${item.clientWidth}px`;
      }
    });
  }

}
