import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule } from '@app/new-shared/components/buttons/buttons.module';
import { FilterComponent } from '@app/new-shared/components/filter/filter.component';
import { BaseDatePickerComponent } from '@app/new-shared/components/form-controls/datepicker/base-date-picker.component';
import { DateMonthPickerComponent } from '@app/new-shared/components/form-controls/datepicker/date-month-picker/date-month-picker.component';
import { DatePickerComponent } from '@app/new-shared/components/form-controls/datepicker/date-picker/date-picker.component';
import { DateRangePickerComponent } from '@app/new-shared/components/form-controls/datepicker/date-range-picker/date-range-picker.component';
import { DateTimePickerComponent } from '@app/new-shared/components/form-controls/datepicker/date-time-picker/date-time-picker.component';
import { TagsInputComponent } from '@app/new-shared/components/form-controls/tags-input/tags-input.component';
// tslint:disable-next-line:max-line-length
import { AdvertiserTermsSelectComponent } from '@app/new-shared/components/live-selects/advertiser-terms-select/advertiser-terms-select.component';
import { BaseLiveSelectComponent } from '@app/new-shared/components/live-selects/base-live-select.component';
import { CountrySelectComponent } from '@app/new-shared/components/live-selects/country-select/country-select.component';
import { CurrencySelectComponent } from '@app/new-shared/components/live-selects/currency-select/currency-select.component';
import { ManagerRolesSelectComponent } from '@app/new-shared/components/live-selects/manager-roles-select/manager-roles-select.component';
import { ManagerSelectComponent } from '@app/new-shared/components/live-selects/manager-select/manager-select.component';
import { PublisherSelectComponent } from '@app/new-shared/components/live-selects/publisher-select/publisher-select.component';
import { TimezoneSelectComponent } from '@app/new-shared/components/live-selects/timezone-select/timezone-select.component';
import { ModalsModule } from '@app/new-shared/components/modals/modals.module';
import { PageModule } from '@app/new-shared/components/page/page.module';
import { TableModule } from '@app/new-shared/components/table/table.module';
import { ValidationComponent } from '@app/new-shared/components/validation/validation.component';
import { HasPermissionDirective } from '@app/new-shared/directives/has-permission/has-permission.directive';
import { PopoverDirective } from '@app/new-shared/directives/popover/popover.directive';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { OwlDateTimeModule } from 'ng-pick-datetime';
import { BadgeComponent } from './components/badge/badge.component';
import { CardModule } from './components/card/card.module';
import { AreaInputComponent } from './components/form-controls/area-input/area-input.component';
import { CheckboxComponent } from './components/form-controls/checkbox/checkbox.component';
import { InputDecimalComponent } from './components/form-controls/input-decimal/input-decimal.component';
import { InputIntegerComponent } from './components/form-controls/input-integer/input-integer.component';
import { InputTextComponent } from './components/form-controls/input-text/input-text.component';
import { PasswordComponent } from './components/form-controls/password/password.component';
import { SwitcherBtnComponent } from './components/form-controls/switcher/switcher-btn/switcher-btn.component';
import { SwitcherComponent } from './components/form-controls/switcher/switcher.component';
import { FormLabelComponent } from './components/form-elements/form-label/form-label.component';
import { InputGroupComponent } from './components/form-elements/input-group/input-group.component';
import { AutosizeDirective } from './directives/autosize/autosize.directive';
import { DisableAutocompleteDirective } from './directives/disable-autocomplete/disable-autocomplete.directive';
import { TableSyncDirective } from './directives/table-sync/table-sync.directive';
import { OfferSelectComponent } from './components/live-selects/offer-select/offer-select.component';
import { AdvertiserSelectComponent } from './components/live-selects/advertiser-select/advertiser-select.component';
import { ResaleUrlSelectComponent } from './components/live-selects/resale-url-select/resale-url-select.component';
import { PaymentTermsSelectComponent } from './components/live-selects/payment-terms-select/payment-terms-select.component';
import { PaymentTermsSuperlinkSelectComponent } from './components/live-selects/payment-terms-superlink-select/payment-terms-superlink-select.component';
import { PublisherInvoicesStatusPipe } from '@app/shared/pipes/publisher-invoices-status/publisher-invoices-status.pipe';
import { PublisherPaymentTermsPipe } from '@app/shared/pipes/publisher-payment-terms/publisher-payment-terms.pipe';
import { PlatformSelectComponent } from './components/live-selects/platform-select/platform-select.component';
import { PanelControlComponent } from './components/panel-control/panel-control.component';
import { PanelControlMiniComponent } from './components/panel-control-mini/panel-control-mini.component';
import { IntegrationPlatformSelectComponent } from './components/live-selects/integrations-platforms-select/integrations-platforms-select.component';
import { PageControlComponent } from './components/page-control/page-control.component';

const LIVE_SELECTS = [
  BaseLiveSelectComponent,
  CountrySelectComponent,
  ManagerSelectComponent,
  CurrencySelectComponent,
  TimezoneSelectComponent,
  PublisherSelectComponent,
  ManagerRolesSelectComponent,
  AdvertiserTermsSelectComponent,
  OfferSelectComponent,
  AdvertiserSelectComponent,
  ResaleUrlSelectComponent,
  PaymentTermsSelectComponent,
  PaymentTermsSuperlinkSelectComponent,
  PlatformSelectComponent,
  IntegrationPlatformSelectComponent
];

const FORM_CONTROLS = [
  BaseDatePickerComponent,
  DateMonthPickerComponent,
  DatePickerComponent,
  DateRangePickerComponent,
  DateTimePickerComponent,
  InputTextComponent,
  CheckboxComponent,
  PasswordComponent,
  SwitcherComponent,
  SwitcherBtnComponent,
  AreaInputComponent,
  TagsInputComponent,
  InputIntegerComponent,
  InputDecimalComponent,
];

const FORM_ELEMENTS = [
  FormLabelComponent,
  InputGroupComponent
];

const DIRECTIVES = [
  AutosizeDirective,
  PopoverDirective,
  TableSyncDirective,
  HasPermissionDirective,
  DisableAutocompleteDirective
];

const ANY_COMPONENTS = [
  FilterComponent,
  ValidationComponent,
  BadgeComponent,
  PanelControlComponent,
  PanelControlMiniComponent,
  PageControlComponent
];

const ANY_PIPES = [
  PublisherInvoicesStatusPipe,
  PublisherPaymentTermsPipe
];

@NgModule({
  imports: [
    CardModule,
    FormsModule,
    CommonModule,
    ButtonsModule,
    ModalsModule,
    NgSelectModule,
    OwlDateTimeModule,
    TranslateModule,
    ReactiveFormsModule
  ],
  declarations: [
    ...ANY_COMPONENTS,
    ...FORM_CONTROLS,
    ...LIVE_SELECTS,
    ...FORM_ELEMENTS,
    ...DIRECTIVES,
    ...ANY_PIPES
  ],
  exports: [
    CardModule,
    ButtonsModule,
    ModalsModule,
    NgSelectModule,
    PageModule,
    TableModule,
    ...ANY_COMPONENTS,
    ...FORM_CONTROLS,
    ...LIVE_SELECTS,
    ...FORM_ELEMENTS,
    ...DIRECTIVES,
    ...ANY_PIPES
  ]
})
export class NewSharedModule {
}
