import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

import { LOGIN_ROUTING } from './auth.routing';
import { AuthPageService } from './auth.service';
import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';

import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { ResetPassComponent } from './reset-pass/reset-pass.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LOGIN_ROUTING),
    ReactiveFormsModule,

    SharedModule
  ],
  declarations: [LoginComponent, ForgotPassComponent, ResetPassComponent, RegistrationComponent],
  providers: [AuthPageService]
})
export class AuthModule {

}
