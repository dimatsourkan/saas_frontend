import { Routes } from '@angular/router';
import { NotAuthenticated } from '../../core/guards/not-authenticated/not-authenticated.service';
import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { ResetPassComponent } from './reset-pass/reset-pass.component';

export const LOGIN_ROUTING: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    canLoad: [NotAuthenticated],
    component: LoginComponent,
    data: {
      name: 'titles.auth.login'
    }
  },
  {
    path: 'login/:role',
    canLoad: [NotAuthenticated],
    component: LoginComponent,
    data: {
      name: 'titles.auth.login'
    }
  },
  {
    path: 'forgot-password',
    canLoad: [NotAuthenticated],
    component: ForgotPassComponent,
    data: {
      name: 'titles.auth.forgot'
    }
  },
  {
    path: 'reset-password/:role',
    canLoad: [NotAuthenticated],
    component: ResetPassComponent,
    data: {
      name: 'titles.auth.reset'
    }
  },
  {
    path: 'registration',
    canLoad: [NotAuthenticated],
    component: RegistrationComponent,
    data: {
      name: 'titles.auth.registration'
    }
  }
];
