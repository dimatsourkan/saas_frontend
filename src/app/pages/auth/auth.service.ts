import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../shared/services/base-url/base-url.service';

@Injectable()
export class AuthPageService {

  constructor(private http: HttpClient, private url: BaseUrlService) {
  }

  getLogo(): Observable<any> {
    return this.http.get(
      `${this.url.getBaseUrl()}/logo`,
      {headers: new HttpHeaders().set('Content-Type', 'application/json')}
    );
  }

  login(role: string, _username: string, _password: string, _tos: boolean): Observable<any> {
    return this.http.post(
      `${this.url.getBaseUrl()}/${role}/login_check`,
      {_username, _password, _tos},
      {headers: new HttpHeaders().set('Content-Type', 'application/json')}
    );
  }

  forgotPass(role: string, email: string): Observable<any> {
    return this.http.post(
      `${this.url.getBaseUrl()}/request_password_reset/${role}`,
      {email},
      {headers: new HttpHeaders().set('Content-Type', 'application/json')}
    );
  }

  resetPass(role: string, token: string, password: string, confirmPassword: string): Observable<any> {
    return this.http.post(
      `${this.url.getBaseUrl()}/reset_password/${role}`,
      {token, password, confirmPassword},
      {headers: new HttpHeaders().set('Content-Type', 'application/json')}
    );
  }

  registration(role: string, user: any): Observable<any> {
    return this.http.post(
      `${this.url.getBaseUrl()}/register/${role}`,
      user,
      {headers: new HttpHeaders().set('Content-Type', 'application/json')}
    );
  }

}
