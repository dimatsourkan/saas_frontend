import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { finalize } from 'rxjs/operators';
import { AuthPageService } from '../auth.service';

@Component({
  selector: 'app-forgot-pass',
  templateUrl: './forgot-pass.component.html',
  styleUrls: [
    '../auth.component.scss',
    './forgot-pass.component.scss'
  ]
})
export class ForgotPassComponent implements OnInit {
  forgotPass: FormGroup;
  loading = false;

  constructor(private fb: FormBuilder,
              private router: Router,
              private loginService: AuthPageService,
              private changeDetection: ChangeDetectorRef,
              private validatorService: ValidatorService) {
  }

  ngOnInit() {
    this.forgotPass = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      role: ['manager', Validators.required]
    });
    this.changeDetection.detectChanges();
  }

  submitForgotPassForm() {

    if (this.forgotPass.invalid) {
      return this.validatorService.setTouchToControls(this.forgotPass);
    }

    this.loading = true;
    this.loginService.forgotPass(this.forgotPass.value.role, this.forgotPass.value.email)
    .pipe(finalize(() => this.loading = false))
    .subscribe(response => {
      this.router.navigate(['/login']);
    }, error => {
      this.validatorService.addErrorToForm(this.forgotPass, error);
    });
  }

}
