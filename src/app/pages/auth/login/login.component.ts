import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { AppReadyService } from '@app/core/services/app-ready/app-ready.service';
import { AuthService } from '@app/core/services/auth/auth.service';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import { finalize } from 'rxjs/operators';
import { AuthPageService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    '../auth.component.scss',
    './login.component.scss'
  ],
  animations: [
    trigger('openClose', [
      state('open', style({
        opacity: 1,
      })),
      state('closed', style({
        opacity: 0,
      })),
      transition('open => closed', [
        animate('1s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ]),
  ]
})
export class LoginComponent implements OnInit {

  loading = false;
  login: FormGroup;
  showTerms: boolean;
  errMessage: string = null;
  logo: any;
  defaultRole: string = 'manager';
  withParam: boolean;

  constructor(private fb: FormBuilder,
              private loginService: AuthPageService,
              private auth: AuthService,
              private apiUrlService: ApiUrlService,
              private router: Router,
              private validatorService: ValidatorService,
              private route: ActivatedRoute,
              private appReadyService: AppReadyService,
              private swal: SweetAlertService) {
  }

  ngOnInit() {
    this.getLogo();
    this.route.paramMap.subscribe(params => {
      const role = params.get('role');
      if (role === 'manager' || role === 'publisher') {
        this.withParam = true;
        this.defaultRole = role;
      }
    });
    this.login = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      role: [this.defaultRole, Validators.required],
      _tos: [false]
    });

    this.login.valueChanges.subscribe(() => {
      this.errMessage = null;
    });
  }

  getLogo() {
    this.loading = true;
    this.loginService.getLogo()
    .pipe(finalize(() => this.loading = false))
    .subscribe(response => {
      this.logo = response;
    }, () => {
      this.logo = 'assets/images/logo.png';
    });
  }

  onSignIn() {

    if (this.login.invalid) {
      return this.validatorService.setTouchToControls(this.login);
    }

    this.loading = true;
    this.loginService.login(this.login.value.role,
      this.login.value.email,
      this.login.value.password,
      this.login.value._tos)
    .subscribe(response => {
      this.onSuccess(response);
    }, error => {
      this.loading = false;
      this.validatorService.addErrorToForm(this.login, error);
      this.swal.error(error.message).then(() => {
        if (error.code === 403) {
          this.showTerms = true;
        }
      }).catch(() => {
        if (error.code === 403) {
          this.showTerms = true;
        }
      });
    });
  }

  onSuccess(response) {
    this.auth.authorizeApp(null, response.token);
    this.appReadyService.initializeApp().then(() => {
      this.loading = false;
      this.router.navigate(['/', this.apiUrlService.linkPathFromRole()]);
    });
  }

}
