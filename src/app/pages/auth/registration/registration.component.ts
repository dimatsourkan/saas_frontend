import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { finalize } from 'rxjs/operators';
import { ValidatorService } from '../../../shared/components/validation/validation.service';
import { SweetAlertService } from '../../../shared/services/sweet-alert/sweet-alert.service';
import { CustomValidators } from '../../../shared/validators/custom-validators.validator';
import { AuthPageService } from '../auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: [
    '../auth.component.scss',
    './registration.component.scss'
  ]
})
export class RegistrationComponent implements OnInit {

  user: FormGroup;
  role = 'publisher';
  loading = false;
  logo: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private loginService: AuthPageService,
              private _auth: AuthPageService,
              private swal: SweetAlertService,
              private validatorService: ValidatorService) {
  }

  ngOnInit() {
    this.getLogo();
    this.user = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required]],
      phone: ['', [Validators.required, CustomValidators.phone]],
      countryCode: ['', [Validators.required]],
      zipcode: ['', [Validators.required]],
      state: ['', [Validators.required]],
      city: ['', [Validators.required]],
      address: ['', [Validators.required]],
      affiliateSource: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required, RxwebValidators.compare({fieldName: 'password'})]],
      aggreements: [false, [Validators.required]]
    });
  }


  getLogo() {
    this.loginService.getLogo().subscribe(response => {
      this.logo = response;
    }, () => {
      this.logo = 'assets/images/logo.png';
    });
  }

  submitRegistration() {

    if (this.user.invalid) {
      return this.validatorService.setTouchToControls(this.user);
    }

    this.loading = true;
    this._auth.registration(this.role, this.user.value)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => {
        if (this.role === 'publisher') {
          this.swal.success('Thanks for request', 'Our manager will contact you soon');
        }
        this.router.navigate(['/']);
      }, error => {
        this.validatorService.setTouchToControls(this.user);
        this.validatorService.addErrorToForm(this.user, error);

        if (!this.user.get('aggreements').value) {
          this.swal.error(`Sorry, we can not provide service until you agree with terms and conditions and privacy policy`);
        }
      });
  }
}
