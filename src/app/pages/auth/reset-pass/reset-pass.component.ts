import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { finalize } from 'rxjs/operators';
import { AuthPageService } from '../auth.service';

@Component({
  selector: 'app-reset-pass',
  templateUrl: './reset-pass.component.html',
  styleUrls: [
    '../auth.component.scss',
    './reset-pass.component.scss'
  ]
})
export class ResetPassComponent implements OnInit {
  resetPass: FormGroup;
  role: string;
  token: string;
  loading = false;

  constructor(private fb: FormBuilder,
              private loginService: AuthPageService,
              private auth: AuthPageService,
              private router: Router,
              private route: ActivatedRoute,
              private validatorService: ValidatorService) {
  }

  ngOnInit() {
    this.role = this.route.snapshot.paramMap.get('role');
    this.token = this.route.snapshot.queryParamMap.get('token');
    this.resetPass = this.fb.group({
      password: ['', Validators.required],
      confirmPassword: ['', [Validators.required, RxwebValidators.compare({fieldName: 'password'})]]
    });
  }

  resetPassword() {

    if (this.resetPass.invalid) {
      return this.validatorService.setTouchToControls(this.resetPass);
    }

    this.loading = true;
    this.validatorService.setTouchToControls(this.resetPass);
    this.loginService.resetPass(this.role, this.token, this.resetPass.value.password, this.resetPass.value.confirmPassword)
    .pipe(finalize(() => this.loading = false))
    .subscribe(() => {
      this.router.navigate(['/login']);
    }, error => {
      this.validatorService.addErrorToForm(this.resetPass, error);
    });
  }

}
