import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as FileSaver from 'file-saver';
import { BillingAdvertisersService } from '../billing-advertisers.service';

@Component({
  selector: 'app-advertisers-invoices-info',
  templateUrl: './advertisers-invoices-info.component.html',
  styleUrls: ['./advertisers-invoices-info.component.scss']
})
export class AdvertisersInvoicesInfoComponent implements OnInit {
  invoice: any;
  advertisersId: string;
  invoiceId: string;

  constructor(private activatedRoute: ActivatedRoute,
              private _billingAdvertisersServices: BillingAdvertisersService) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.advertisersId = params['id'];
      this.getInvoice(this.advertisersId);
    });

  }

  getInvoice(id) {
    this._billingAdvertisersServices.getInvoicePreview(id).subscribe(response => {
      this.invoice = response;
    });
  }

  downloadInvoice() {
    return this._billingAdvertisersServices.downloadInvoice(this.advertisersId).subscribe((response) => {
      const file = new Blob([response.body], {type: 'application/pdf;charset=utf-8'});
      FileSaver.saveAs(file, 'invoice.pdf', true);
    }, error => error);
  }

}

