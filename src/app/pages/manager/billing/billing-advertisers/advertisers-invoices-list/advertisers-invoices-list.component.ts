import { Component, OnInit, ViewChild } from '@angular/core';
import { AdvInvoiceFilter } from '@app/core/entities/adv-invoice/adv-invoice.filter';
import { AdvInvoice } from '@app/core/entities/adv-invoice/adv-invoice.model';
import { AdvInvoiceService } from '@app/core/entities/adv-invoice/adv-invoice.service';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';
import { DataStore } from '@app/core/entities/data-store.service';
import { modelId } from '@app/core/models/base-model/base.model';
import {
  PaymentApproveComponent
} from '@app/pages/manager/billing/billing-advertisers/advertisers-invoices-list/payment-approve/payment-approve.component';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';

import { BillingAdvertisersService } from '../billing-advertisers.service';

@Component({
  selector: 'app-advertisers-invoices-list',
  templateUrl: './advertisers-invoices-list.component.html',
  styleUrls: ['./advertisers-invoices-list.component.scss']
})
export class AdvertisersInvoicesListComponent implements OnInit {

  @ViewChild('paymentApprove') paymentApprove: PaymentApproveComponent;
  loading: boolean;
  dateFormat: string = 'YYYY-MM-DD';

  filter = (new AdvInvoiceFilter(true)).init();
  invoices$ = DataStore.advInvoice.list.asObservable$;
  invoiceStatuses$ = DataStore.advInvoice.statuses.asObservable$;
  invoiceSendStatuses$ = DataStore.advInvoice.sentStatuses.asObservable$;

  constructor(private swal: SweetAlertService,
    private _billingAdvertisersService: BillingAdvertisersService,
    private advInvoiceService: AdvInvoiceService) {
  }

  ngOnInit() {

    if (!this.filter.dateFrom) {
      this.filter.dateFrom = moment().subtract(30, 'day').format(this.dateFormat);
      this.filter.cancelCurrentChangeEvent();
    }
    if (!this.filter.dateTo) {
      this.filter.dateTo = moment().format(this.dateFormat);
      this.filter.cancelCurrentChangeEvent();
    }

    this.advInvoiceService.getAdvInvoiceStatuses().subscribe();
    this.advInvoiceService.getInvoiceSentStatuses().subscribe();

    this.filter.onChangeFilter$.subscribe(filter => {
      this.loading = true;
      DataStoreHelper.toggleSelectAll(DataStore.advInvoice, false);
      this.advInvoiceService.getAll(filter.filter)
        .pipe(finalize(() => this.loading = false))
        .subscribe();
    });
  }

  applyCalendar(event: { startDate: string, endDate: string }) {
    this.filter.dateFrom = event.startDate;
    this.filter.dateTo = event.endDate;
  }

  filterByStatus(status: modelId) {
    this.filter.status = status;
  }

  get allIsChecked() {
    return this.selectedLengh === DataStore.advInvoice.list.getValue().items.length;
  }

  get selectedLengh() {
    return DataStore.advInvoice.selected.getValue().length;
  }

  isChecked(invoiceId: modelId) {
    return DataStore.advInvoice.selected.getValue().includes(invoiceId);
  }

  selectInvoice(invoiceId: modelId) {
    DataStoreHelper.toggleSelect(DataStore.advInvoice, invoiceId);
  }

  selectAllInvoices(select: boolean) {
    DataStoreHelper.toggleSelectAll(DataStore.advInvoice, select);
  }

  changeInvoiceStatus(invoice: AdvInvoice, statusId: string) {
    const status = DataStore.advInvoice.statuses.getValue().items.find((el) => el.id === statusId);
    if (status.origin === 'paid' || status.origin === 'paid_partially') {
      this.paymentApprove.openModal(invoice, status);
    } else {
      this.changeStatus(invoice, statusId);
    }
  }

  changeStatus(invoice: AdvInvoice, statusId: string) {
    this.swal.confirm('Are you sure?').then(isConfirmed => {
      if (isConfirmed.value) {
        this.advInvoiceService.changeStatusWithoutInformation(invoice.id, statusId).subscribe((res) => {
          this.swal.success('Done!');
        }, (err) => {
          if (typeof err.errors === 'string') {
            this.swal.error(err.errors);
          } else {
            this.swal.error(err.errors.status[0] || 'Has some errors');
          }
        });
      }
    });
  }

  mergeAdvertisers() {
    this.swal.confirm('Are you sure?').then(isConfirmed => {
      if (isConfirmed.value) {
        const selected = DataStore.advInvoice.selected.getValue();
        this.advInvoiceService.mergeAdvertisers(selected).subscribe((res) => {
          this.selectAllInvoices(false);
          this.swal.success('Success!');
        }, (err) => {
          this.swal.error(err.errors || 'Something went wrong!');
        });
      } else {
        this.swal.error('Canceled!');
      }
    });
  }

  sendInvoices() {
    this.swal.confirm('Are you sure?').then(isConfirmed => {
      if (isConfirmed.value) {
        this.requestForSendInvoices(DataStore.advInvoice.selected.getValue());
      }
    });
  }

  sendInvoice(id) {
    this.swal.confirm('Are you sure?').then(isConfirmed => {
      if (isConfirmed.value) {
        this.requestForSendInvoices([id]);
      }
    });
  }

  requestForSendInvoices(data: string[]) {
    this.advInvoiceService.requestForSendInvoices(data).subscribe((res) => {
      this.selectAllInvoices(false);
      this.swal.success('Success!');
    }, (err) => {
      this.swal.error(err.errors || 'Something went wrong!');
    });
  }

  removeInvoice(id: modelId) {
    this.swal.confirm('Are you sure?').then(isConfirmed => {
      if (isConfirmed.value) {
        this.advInvoiceService.delete(id).subscribe(() => {
          this.swal.success('Success!');
        }, (err) => this.swal.error(err.errors || 'Something went wrong!'));
      }
    });
  }

  downloadInvoice(invoiceId: modelId) {
    return this.advInvoiceService.getDataForExport(invoiceId).subscribe((response) => {
      FileSaver.saveAs(response.body, 'invoice.pdf', true);
    }, error => error);
  }

}
