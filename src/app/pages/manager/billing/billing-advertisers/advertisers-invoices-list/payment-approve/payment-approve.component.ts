import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { AdvInv, AdvInvoice } from '@app/core/entities/adv-invoice/adv-invoice.model';
import { AdvInvoiceService } from '@app/core/entities/adv-invoice/adv-invoice.service';
import { Common } from '@app/core/models/common-model/common.model';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-payment-approve',
  templateUrl: './payment-approve.component.html',
  styleUrls: ['./payment-approve.component.scss']
})
export class PaymentApproveComponent implements OnInit {

  @ViewChild('invoiceModal') private invoiceModal: ModalComponent;
  invoiceForModal: AdvInvoice;
  form: FormGroup;
  loading = false;

  @Output()
  onSuccess = new EventEmitter();

  constructor(
    private advInvoiceService: AdvInvoiceService,
    private validatorService: ValidatorService,
    private formBuilder: FormBuilder,
    private swal: SweetAlertService,
  ) {

  }

  get payments() {
    return this.form.get('payments') as FormArray;
  }

  getPaymentsForm() {
    return this.formBuilder.group({
      amount: [0],
      date: [moment().format()],
      notes: [''],
      id: [],
    });
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      id: [],
      paid: [{ value : '', disabled: true }],
      status: [],
      accountDate: [],
      accountNumber: [],
      payments: this.formBuilder.array([])
    });
  }

  openModal(invoice: AdvInvoice, newStatus: Common.Status) {
    this.invoiceModal.open();
    this.payments.controls = [];
    this.invoiceForModal = invoice;
    invoice.payments.forEach(() => this.payments.push(this.getPaymentsForm()));
    this.form.patchValue({
      id : invoice.id,
      paid : invoice.paid,
      status: newStatus.id,
      payments : invoice.payments,
      accountDate: moment().format(),
      accountNumber : invoice.accountNumber,
    });
    this.payments.push(this.getPaymentsForm());
  }

  changeStatus() {

    if (this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    const data = AdvInv.PaidStatus.plainToClass(this.form.getRawValue());
    this.advInvoiceService.changeStatusWithInformation(data.id, data)
      .pipe(finalize(() => this.loading = false))
      .subscribe((res) => {
        this.swal.success('Done!');
        this.invoiceModal.close();
      }, (err) => {
        if (typeof err.errors === 'string') {
          this.swal.error(err.errors);
        } else {
          this.validatorService.addErrorToForm(this.form, err);
        }
      });
  }

  removePayment(index) {
    this.payments.removeAt(index);
  }

}
