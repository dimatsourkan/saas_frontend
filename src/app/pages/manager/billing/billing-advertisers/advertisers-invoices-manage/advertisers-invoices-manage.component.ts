import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { BillingAdvertisersService } from '../billing-advertisers.service';

@Component({
  selector: 'app-advertisers-invoices-manage',
  templateUrl: './advertisers-invoices-manage.component.html',
  styleUrls: ['./advertisers-invoices-manage.component.scss']
})
export class AdvertisersInvoicesManageComponent implements OnInit {
  invoice: {
    items: any[],
    deductions: any[],
    ourAmount: number,
    notes: boolean,
    info: string,
    id: string,
    invoiceDate: any,
    advertiserId: any,
    advertiserAmount: number,
    month: any,
    amount: number,
    total: number,
    status: number,
    invoiceMonth: any,
    currency: string
  };
  invoiceId: string;
  totalPromotionsAmount: number;
  errors: any;
  form: FormGroup;
  loading = false;

  constructor(
    private swal: SweetAlertService,
    private route: ActivatedRoute,
    private router: Router,
    private _billingAdvertisersService: BillingAdvertisersService,
    private validatorService: ValidatorService,
    private _fb: FormBuilder) {
  }

  ngOnInit() {
    this.invoice = {
      items: [],
      deductions: [],
      ourAmount: 0,
      notes: false,
      info: '',
      id: '',
      invoiceDate: {},
      advertiserId: '',
      advertiserAmount: 0,
      month: {},
      amount: 0,
      total: 0,
      status: 0,
      invoiceMonth: {},
      currency: ''
    };

    this.form = this._fb.group({
      id: [],
      advertiserAmount: [],
      advertiserId: [, Validators.required],
      currency: ['USD', Validators.required],
      info: [, [Validators.maxLength(255)]],
      invoiceDate: [moment().utc().format(), Validators.required],
      invoiceMonth: [moment().utc().format(), Validators.required],
      ourAmount: [],
      items: this._fb.array([this.createItem()]),
      deductions: this._fb.array([])
    });

    this.totalPromotionsAmount = 0;
    this.route.paramMap.subscribe((params) => {
      if (params.get('id')) {
        this.invoiceId = params.get('id');
        this.getInvoice(this.invoiceId);
      }
    });

    this.form.controls.items.valueChanges.subscribe((val) => {
      let total = 0;
      val.forEach(e => {
        e.total = e.unitPrice * e.quantity;
        total += e.total;
      });
      this.totalPromotionsAmount = Number(total.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
    });
  }

  get formItems() {
    return this.form.get('items') as FormArray;
  }

  get formDeductions() {
    return this.form.get('deductions') as FormArray;
  }

  createItem(item?): FormGroup {
    return this._fb.group({
      id: [(item) ? item.id : ''],
      name: [(item) ? item.name : '', [Validators.required, Validators.maxLength(255)]],
      unitPrice: [(item) ? item.unitPrice : '', Validators.required],
      quantity: [(item) ? item.quantity : '', Validators.required],
      total: [(item) ? item.total : '', Validators.required]
    });
  }

  addItem(item?): void {
    this.formItems.push(this.createItem(item));
  }

  createDeductions(item?): FormGroup {
    return this._fb.group({
      id: [(item) ? item.id : ''],
      name: [(item) ? item.name : '', Validators.required],
      month: [(item) ? moment.unix(item.month).format('YYYY-MM') : moment().format('YYYY-MM'), Validators.required],
      amount: [(item) ? item.amount : '', Validators.required]
    });
  }

  addDeductions(item?): void {
    this.formDeductions.push(this.createDeductions(item));
  }

  submitInvoiceForm() {

    if (this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    if (this.invoiceId) {
      this.updateInvoice();
    } else {
      this.createInvoice();
    }
  }

  createInvoice() {
    const invoice = this.form.getRawValue();
    invoice.deductions.filter((item) => delete item.id);
    delete invoice.id;
    this._billingAdvertisersService.createInvoice(invoice)
    .pipe(finalize(() => this.loading = false))
    .subscribe((res) => {
      this.router.navigate(['/', 'manager', 'billing', 'advertisers']);
    }, (err) => {
      this.validatorService.addErrorToForm(this.form, err);
    });
  }

  updateInvoice() {
    this.invoice = this.form.getRawValue();
    if (typeof this.invoice.advertiserId === 'object') {
      this.invoice.advertiserId = this.invoice.advertiserId.id;
    }
    this.invoice.items.forEach(item => {
      if (!item.id) {
        delete item.id;
      }
    });
    this.invoice.deductions.filter((item) => (item.id === '') ? delete item.id : item);
    this._billingAdvertisersService.updateInvoice(this.invoice, this.invoiceId)
    .pipe(finalize(() => this.loading = false))
    .subscribe((res) => {
      this.router.navigate(['/', 'manager', 'billing', 'advertisers']);
    }, (err) => {
      this.validatorService.addErrorToForm(this.form, err);
    });
  }

  calendarCallback(date, key) {
    this[key] = date.date;
  }

  onChange(errName) {
    if (this.errors && this.errors.hasOwnProperty(errName)) {
      delete this.errors[errName];
    }
  }

  getInvoice(invoiceId) {
    return this._billingAdvertisersService.getInvoice(invoiceId).subscribe((response) => {
      if (response) {
        this.invoice = response;
        this.getInvoiceAdvertiser(this.invoice.advertiserId);
        this.invoice.advertiserAmount = response.advertiserAmount.amount;
        this.invoice.ourAmount = response.ourAmount.amount;
        this.invoice.total = response.total.amount;
        this.invoice.invoiceMonth = this.invoice.month;
        this.invoice.items.map((el) => {
          el.unitPrice = el.unitPrice.amount;
          el.total = el.total.amount;
        });
        this.invoice.deductions.map((el) => el.amount = el.amount.amount);
        this.form.patchValue(response);
        if (this.invoice.status !== 1) {
          this.form.get('invoiceMonth').disable();
        }
        this.form.get('invoiceDate').disable();
        this.setFormArrayValue('items', response.items);
        this.setFormArrayValue('deductions', response.deductions);
      } else {
        this.swal.error('Not Found');
      }
    });
  }

  removeFromFormArray(name: string, index: number): void {
    (<FormArray>this.form.get(name)).removeAt(index);
  }

  setFormArrayValue(listName: string, array: any[]): void {
    this.removeFromFormArray(listName, 0);
    if (listName === 'items') {
      array.map((item) => this.addItem(item));
    } else if (listName === 'deductions') {
      array.map((item) => this.addDeductions(item));
    }
  }

  getInvoiceAdvertiser(id) {
    this._billingAdvertisersService.getInvoiceAdvertiser(id).subscribe((response) => {
      this.invoice.advertiserId = {
        id: response.items[0].id,
        text: response.items[0].username
      };
    });
  }
}
