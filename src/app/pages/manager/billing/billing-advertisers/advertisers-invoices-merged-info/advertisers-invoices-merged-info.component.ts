import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as FileSaver from 'file-saver';
import { DateFormattedService } from '../../../../../shared/services/date-formatted/date-formatted.service';
import { BillingAdvertisersService } from '../billing-advertisers.service';

@Component({
  selector: 'app-advertisers-invoices-merged-info',
  templateUrl: './advertisers-invoices-merged-info.component.html',
  styleUrls: ['./advertisers-invoices-merged-info.component.scss']
})
export class AdvertisersInvoicesMergedInfoComponent implements OnInit {
  invoice: any;
  advertisersId: string;
  invoiceId: string;

  constructor(private dateFormattedService: DateFormattedService,
              private route: ActivatedRoute,
              private _billingAdvertisersServices: BillingAdvertisersService,
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.advertisersId = params['id'];
      this.getInvoice(params['id']).subscribe(response => {
        this.dataReady(response);
      });
    });
  }

  getInvoice(id) {
    return this._billingAdvertisersServices.getInvoiceMergedPreview(id);
  }

  dataReady(invoice) {
    this.invoice = invoice;
  }

  downloadInvoice() {
    return this._billingAdvertisersServices.downloadInvoiceAdvertiser(this.advertisersId).subscribe((response) => {
      const file = new Blob([response.body], {type: 'application/pdf;charset=utf-8'});
      FileSaver.saveAs(file, 'invoice.pdf', true);
    }, error => error);
  }

}
