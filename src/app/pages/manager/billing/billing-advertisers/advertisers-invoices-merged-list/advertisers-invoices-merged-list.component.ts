import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { forkJoin } from 'rxjs';
import { DateFormattedService } from '../../../../../shared/services/date-formatted/date-formatted.service';
import { LanguageService } from '../../../../../shared/services/language/language.service';
import { QueryParamsService } from '../../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { BillingAdvertisersService } from '../billing-advertisers.service';
import { AdvInvoiceFilter } from '@app/core/entities/adv-invoice/adv-invoice.filter';


@Component({
  selector: 'app-advertisers-invoices-merged-list',
  templateUrl: './advertisers-invoices-merged-list.component.html',
  styleUrls: ['./advertisers-invoices-merged-list.component.scss']
})
export class AdvertisersInvoicesMergedListComponent implements OnInit {
  invoices: any = {
    items: []
  };
  params: any;
  loading: boolean;
  order: string;
  date: string;
  dateFormat: string;
  invoiceSendStatus: any;

  filter = (new AdvInvoiceFilter(true)).init();

  constructor(private dateFormattedService: DateFormattedService,
              private swal: SweetAlertService,
              private _billingAdvertisersService: BillingAdvertisersService,
              private queryParamsService: QueryParamsService
  ) {
  }

  ngOnInit() {
    this.loading = true;
    this.params = this.queryParamsService.getParams();
    this.dateFormat = this.dateFormattedService.dateFormat;
    this.params.date_from = this.params.date_from || moment().subtract(30, 'day').format(this.dateFormattedService.dateFormat);
    this.params.date_to = this.params.date_to || moment().format(this.dateFormattedService.dateFormat);

    forkJoin([this.getMergedInvoices(this.params), this.getSentStatuses()])
      .subscribe(res => {
        this.dataReady(res[0], res[1]);
      });
  }

  getMergedInvoices(params) {
    return this._billingAdvertisersService.getMergedInvoices(params);
  }

  getSentStatuses() {
    return this._billingAdvertisersService.getSentStatuses();
  }


  dataReady(invoices, sendStatuses?) {
    if (sendStatuses) {
      this.invoiceSendStatus = sendStatuses;
    }
    this.invoices = invoices;
    this.params.page = this.invoices.page;
    this.params.limit = this.invoices.limit;

    this.invoices.items.forEach(invoice => {
      invoice.mergedEndMonth = moment(invoice.mergedEndMonth, this.dateFormattedService.dateFormat).format(this.dateFormat);
      invoice.mergedStartMonth = moment(invoice.mergedStartMonth, this.dateFormattedService.dateFormat).format(this.dateFormat);
      this.invoiceSendStatus.forEach((status) => {
        if (status.id === invoice.sendStatus) {
          invoice.sendStatusOrigin = status.origin;
          invoice.sendStatusValue = status.value;
        }
      });
    });
    this.queryParamsService.setParams(this.params);
    this.loading = false;
  }

  sortByField(field) {
    this.params.sort = field;
    this.params.page = 1;
    this.params.order = this.order;
    this.order = this.order === 'ASC' ? 'DESC' : 'ASC';
    this.loading = true;
    this.getMergedInvoices(this.params).subscribe(res => this.dataReady(res));
  }

  goToPage(page) {
    this.loading = true;
    this.params.page = page;
    this.getMergedInvoices(this.params).subscribe(res => this.dataReady(res));
  }

  tableLimit(number) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = number;
    this.getMergedInvoices(this.params).subscribe(res => this.dataReady(res));
  }

  calendarCallback({startDate, endDate}) {
    this.loading = true;
    this.params.date_from = startDate;
    this.params.date_to = endDate;
    this.params.page = 1;
    this.getMergedInvoices(this.params).subscribe(res => this.dataReady(res));
  }

  downloadMergedInvoices(id) {
    return this._billingAdvertisersService.downloadMergedInvoices(id).subscribe((response) => {
      const file = new Blob([response.body], {type: 'application/pdf;charset=utf-8'});
      FileSaver.saveAs(file, 'invoice.pdf', true);
    }, (err) => {
      this.swal.error(err.errors);
    });
  }

  sendMergedInvoice(id) {
    return this._billingAdvertisersService.sendMergedInvoice(id).subscribe((response) => {
      this.invoices.items.forEach((invoice) => {
        if (id === invoice.id) {
          this.invoiceSendStatus.map((status) => {
            if (status.origin === 'preparing_sending') {
              invoice.sendStatusOrigin = status.origin;
              invoice.sendStatusValue = status.value;
            }
          });
        }
      });
      this.swal.success('Success!');
    }, (err) => {
      this.swal.error(err.errors || 'Something went wrong!');
    });
  }

  deleteMergedInvoice(id, index) {
    this.swal.confirm('Are you sure?').then(isConfirmed => {
      if (isConfirmed.value) {
        this._billingAdvertisersService.deleteMergedInvoice(id).subscribe(res => {
          this.invoices.items.splice(index, 1);
          this.swal.success('Success!');
        }, (err) => {
          this.swal.error(err.errors || 'Something went wrong!');
        });
      } else {
        this.swal.error('Canceled!');
      }
    });
  }

}
