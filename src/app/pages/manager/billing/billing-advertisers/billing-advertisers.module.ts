import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InvAdvertisersResolver } from '@app/pages/manager/billing/billing-advertisers/billing-advertisers.resolver';
import { BILLING_ADV_ROUTES } from '@app/pages/manager/billing/billing-advertisers/billing-advertisers.routing';
import { SharedModule } from '@app/shared/shared.module';
import { AdvertisersInvoicesInfoComponent } from './advertisers-invoices-info/advertisers-invoices-info.component';
import { AdvertisersInvoicesListComponent } from './advertisers-invoices-list/advertisers-invoices-list.component';
import { PaymentApproveComponent } from './advertisers-invoices-list/payment-approve/payment-approve.component';
import { AdvertisersInvoicesLoggerListComponent } from './advertisers-invoices-logger-list/advertisers-invoices-logger-list.component';
import { AdvertisersInvoicesManageComponent } from './advertisers-invoices-manage/advertisers-invoices-manage.component';
import { AdvertisersInvoicesMergedInfoComponent } from './advertisers-invoices-merged-info/advertisers-invoices-merged-info.component';
import { AdvertisersInvoicesMergedListComponent } from './advertisers-invoices-merged-list/advertisers-invoices-merged-list.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(BILLING_ADV_ROUTES)
  ],
  declarations: [
    AdvertisersInvoicesListComponent,
    AdvertisersInvoicesManageComponent,
    AdvertisersInvoicesInfoComponent,
    AdvertisersInvoicesMergedInfoComponent,
    AdvertisersInvoicesMergedListComponent,
    AdvertisersInvoicesLoggerListComponent,
    PaymentApproveComponent
  ],
  providers: [
    InvAdvertisersResolver
  ]
})
export class BillingAdvertisersModule {
}
