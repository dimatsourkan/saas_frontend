import { Injectable } from '@angular/core';
import { AdvInvoice } from '@app/core/entities/adv-invoice/adv-invoice.model';
import { AdvInvoiceService } from '@app/core/entities/adv-invoice/adv-invoice.service';
import { BaseItemResolver } from '@app/core/services/resolver/base-item.resolver';

@Injectable()
export class InvAdvertisersResolver extends BaseItemResolver<AdvInvoice> {

  protected service: AdvInvoiceService = this.injector.get(AdvInvoiceService, null);

  protected onError(error: any) {
    this.router.navigate(['/manager/invoices/list']);
    this.hideLoader();
  }

}
