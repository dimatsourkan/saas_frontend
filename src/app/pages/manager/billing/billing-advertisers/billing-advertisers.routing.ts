import { Routes } from '@angular/router';
import { InvAdvertisersResolver } from '@app/pages/manager/billing/billing-advertisers/billing-advertisers.resolver';
import { AdvertisersInvoicesInfoComponent } from './advertisers-invoices-info/advertisers-invoices-info.component';
import { AdvertisersInvoicesListComponent } from './advertisers-invoices-list/advertisers-invoices-list.component';
import { AdvertisersInvoicesLoggerListComponent } from './advertisers-invoices-logger-list/advertisers-invoices-logger-list.component';
import { AdvertisersInvoicesManageComponent } from './advertisers-invoices-manage/advertisers-invoices-manage.component';
import { AdvertisersInvoicesMergedInfoComponent } from './advertisers-invoices-merged-info/advertisers-invoices-merged-info.component';
import { AdvertisersInvoicesMergedListComponent } from './advertisers-invoices-merged-list/advertisers-invoices-merged-list.component';

export const BILLING_ADV_ROUTES: Routes = [
  {path: '', redirectTo: 'invoices/list', pathMatch: 'full'},
  {
    path : 'invoices',
    children : [
      {
        path: '', component: AdvertisersInvoicesListComponent,
        data: {
          name: 'titles.bill.adv.invoices',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'list', component: AdvertisersInvoicesListComponent,
        data: {
          name: 'titles.bill.adv.invoices',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'create', component: AdvertisersInvoicesManageComponent,
        data: {
          name: 'titles.bill.adv.create',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'edit/:id', component: AdvertisersInvoicesManageComponent,
        data: {
          name: 'titles.bill.adv.edit',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'info/:id', component: AdvertisersInvoicesInfoComponent,
        data: {
          name: 'titles.bill.adv.info',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
    ]
  },
  {
    path : 'invoices-merged',
    children: [
      {
        path: '', component: AdvertisersInvoicesMergedListComponent,
        data: {
          name: 'titles.bill.adv.merged',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'list', component: AdvertisersInvoicesMergedListComponent,
        data: {
          name: 'titles.bill.adv.merged',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'info/:id', component: AdvertisersInvoicesMergedInfoComponent,
        data: {
          name: 'titles.bill.adv.merged',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
    ]
  },
  {
    path: 'invoices-logger/list', component: AdvertisersInvoicesLoggerListComponent,
    data: {
      name: 'titles.bill.adv.invoicesLogger',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }
];
