import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { BaseUrlService } from '@app/shared/services/base-url/base-url.service';
import { QueryParamsService } from '@app/shared/services/queryParams/query-params.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BillingAdvertisersService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getInvoice(invoiceId: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/${invoiceId}`,
      {headers: {toCamelCase: 'true'}});
  }

  getInvoiceAdvertiser(id: string): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString({context: 'keylist', active: true, search: id});
    return this.http.get(`${this.url.getBaseUrlWithRole()}/advertisers${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  getMergedInvoices(queryParams: Params): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/merged${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  getSentStatuses(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/send_statuses`,
      {headers: {toCamelCase: 'true'}});
  }

  createInvoice(data: any): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/advertiser_invoices`, data,
      {headers: {toCamelCase: 'true'}});
  }

  updateInvoice(data: any, id: string): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/${id}`, data,
      {headers: {toCamelCase: 'true'}});
  }

  sendMergedInvoice(id: any): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/merged/send`, {mergeIds: [id]},
      {headers: {toCamelCase: 'true'}});
  }

  deleteMergedInvoice(id: string): Observable<any> {
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/merged/${id}`,
      {headers: {toCamelCase: 'true'}});
  }

  downloadMergedInvoices(id: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/merged/${id}/download`, {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  }

  downloadInvoice(id: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/${id}/download`, {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  }

  downloadInvoiceAdvertiser(id: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/merged/${id}/download`, {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  }

  getInvoicePreview(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/${id}/preview`,
      {headers: {toCamelCase: 'true'}});
  }

  getInvoiceMergedPreview(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/merged/${id}/preview`,
      {headers: {toCamelCase: 'true'}});
  }

  getLoggerList(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing_logger/advertiser_invoices${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  getAdvertiserActions(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing_logger/advertiser_invoices/actions`,
      {headers: {toCamelCase: 'true'}});
  }


}
