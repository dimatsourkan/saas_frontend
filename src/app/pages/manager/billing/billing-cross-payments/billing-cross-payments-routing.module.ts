import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingCrossPaymentsViewComponent } from './billing-cross-payments-view/billing-cross-payments-view.component';
import { CrossPaymentsInvoicesManageComponent } from './cross-payments-invoices-manage/cross-payments-invoices-manage.component';
import { CrossPaymentsListComponent } from './cross-payments-list/cross-payments-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'cross-payments',
    pathMatch: 'full'
  },
  {
    path: 'list', component: CrossPaymentsListComponent,
    data: {
      name: 'titles.bill.cross-payments.list',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'manage', component: CrossPaymentsInvoicesManageComponent,
    data: {
      name: 'titles.bill.cross-payments.create-cross',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'view/:id', component: BillingCrossPaymentsViewComponent,
    data: {
      name: 'titles.bill.cross-payments.info-cross',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingCrossPaymentsRoutingModule {
}
