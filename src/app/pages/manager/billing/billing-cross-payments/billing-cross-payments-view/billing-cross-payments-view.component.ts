import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as FileSaver from 'file-saver';
import { CrossPaymentsService } from '../cross-payments.service';

@Component({
  selector: 'app-billing-cross-payments-view',
  templateUrl: './billing-cross-payments-view.component.html',
  styleUrls: ['./billing-cross-payments-view.component.scss']
})
export class BillingCrossPaymentsViewComponent implements OnInit {

  loading: boolean;
  invoice: any = {};
  id: string;

  constructor(private _services: CrossPaymentsService, private route: ActivatedRoute, private swal: SweetAlertService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.getCrossPayment(this.id);
    });

  }

  getCrossPayment(id) {
    this._services.getCrossPayment(id).subscribe(res => {
      this.invoice = res;
    });
  }

  downloadPayment() {
    this._services.downloadPayment(this.id).subscribe((response) => {
      const file = new Blob([response.body], {type: 'application/pdf;charset=utf-8'});
      FileSaver.saveAs(file, `cross-payment-${this.id}.pdf`, true);
    }, error => error);
  }

}
