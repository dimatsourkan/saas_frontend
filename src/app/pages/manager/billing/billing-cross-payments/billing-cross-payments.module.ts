import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';

import { BillingCrossPaymentsRoutingModule } from './billing-cross-payments-routing.module';
import { BillingCrossPaymentsViewComponent } from './billing-cross-payments-view/billing-cross-payments-view.component';
import {
  CrossPaymentsInvoicesManageComponent,
} from './cross-payments-invoices-manage/cross-payments-invoices-manage.component';
import {
  CrossPaymentsInvoicesPreviewComponent,
} from './cross-payments-invoices-preview/cross-payments-invoices-preview.component';
import { CrossPaymentsListComponent } from './cross-payments-list/cross-payments-list.component';

@NgModule({
  declarations: [CrossPaymentsListComponent,
    CrossPaymentsInvoicesManageComponent,
    CrossPaymentsInvoicesPreviewComponent, BillingCrossPaymentsViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    BillingCrossPaymentsRoutingModule
  ]
})
export class BillingCrossPaymentsModule {
}
