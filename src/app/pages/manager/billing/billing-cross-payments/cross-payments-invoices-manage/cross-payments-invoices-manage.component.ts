import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QueryParamsService } from '@app/shared/services/queryParams/query-params.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { CrossPaymentsService } from '../cross-payments.service';

@Component({
  selector: 'app-cross-payments-invoices-manage',
  templateUrl: './cross-payments-invoices-manage.component.html',
  styleUrls: ['./cross-payments-invoices-manage.component.scss']
})
export class CrossPaymentsInvoicesManageComponent implements OnInit {

  params$ = new Subject<any>();
  params: any = {};
  loading: boolean;
  invoices: any;
  comment: string = '';

  constructor(private _services: CrossPaymentsService,
              private swal: SweetAlertService,
              private router: Router,
              private queryParamsService: QueryParamsService) {
  }

  ngOnInit() {
    this.params = this.queryParamsService.getParams();
    if (!Object.prototype.hasOwnProperty.call(this.params, 'dateFrom')) {
      this.params.dateFrom = moment().subtract(90, 'day').startOf('month').format('YYYY-MM-DD');
      this.params.dateTo = moment().format('YYYY-MM-DD');
    }

    this.params$.pipe(
      debounceTime(500)
    ).subscribe(params => {
      this.queryParamsService.setParams(params);
      this.getCrossPaymentsInvoices(params);
    });
  }

  getCrossPaymentsInvoices(params) {
    this._services.getCrossPaymentsInvoices(params).subscribe(res => {
      this.loading = false;
      this.invoices = res;
      if (this.invoices.length === 0) {
        this.swal.info('No matching invoices');
      }
    }, err => {
      this.invoices = {};
      this.loading = false;
    });
  }

  calculateTotal(invoice) {
    const sum = invoice.filter(item => item.checked).map(amount => amount.amount.amount).reduce((a, b) => a + b, 0);
    return sum.toFixed(2);
  }

  savePayment() {
    const payment = {
      advertiserInvoiceIds: this.getSelectedInvoices('advertiserInvoices'),
      publisherInvoiceIds: this.getSelectedInvoices('publisherInvoices'),
      comment: this.comment,
      periodFrom: this.params.dateFrom,
      periodTo: this.params.dateTo,
    };

    this._services.saveCrossPayments(payment).subscribe(() => {
      this.swal.success('Done!').then(() => {
        this.router.navigate([`/manager/billing/cross-payments/list`]);
      });
    }, err => {
      this.swal.error(this.parseError(err.errors) || 'Has some errors');
    });
  }

  getSelectedInvoices(key: string) {
    let ids = [];
    // tslint:disable-next-line:forin
    for (const prop in this.invoices) {
      if (this.invoices[prop][key]) {
        ids = [...ids, ...this.invoices[prop][key].filter(item => item.checked).map(el => el.id)];
      }
    }
    return ids;
  }

  calendarCallback({startDate, endDate}) {
    this.applyParams({dateFrom: startDate, dateTo: endDate});
  }

  tableLimit(limit) {
    this.applyParams({page: 1, limit});
  }

  goToPage(page) {
    this.applyParams({page});
  }

  applyParams(params?) {
    this.loading = true;
    this.params = {...this.params, ...params};
    this.params$.next(this.params);
  }

  parseError(err) {
    let newMsg = '';
    if (typeof err === 'string') {
      newMsg = err;
      return newMsg;
    }
    for (const prop in err) {
      if (err[prop]) {
        newMsg += `${prop}: ${err[prop].join()}\n`;
      }
    }

    return newMsg;
  }

}
