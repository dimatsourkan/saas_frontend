import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CrossPaymentsService } from '../cross-payments.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';

@Component({
  selector: 'app-cross-payments-invoices-preview',
  templateUrl: './cross-payments-invoices-preview.component.html',
  styleUrls: ['./cross-payments-invoices-preview.component.scss']
})
export class CrossPaymentsInvoicesPreviewComponent implements OnInit {
  @Input() id: string;
  @Output()
  updateCrossPaymentEvent: EventEmitter<{ id: string, comment: string }> = new EventEmitter<{ id: '', comment: '' }>();
  @Output()
  close: EventEmitter<boolean> = new EventEmitter<boolean>();
  crossPaymentInfo: any = {};
  constructor(private _services: CrossPaymentsService, private swal: SweetAlertService) { }

  ngOnInit() {
    this.getCrossPayment(this.id);
  }

  getCrossPayment(id) {
    this._services.getCrossPayment(id).subscribe(res => {
      this.crossPaymentInfo = res;
    });
  }

  updateCrossPayment() {
    this._services.updateCrossPayment({ id: this.crossPaymentInfo.id, comment: this.crossPaymentInfo.comment }).subscribe(res => {
      this.updateCrossPaymentEvent.emit({ id: this.crossPaymentInfo.id, comment: this.crossPaymentInfo.comment });
      this.closeModal();
      this.swal.success('Done!');
    });
  }

  closeModal() {
    this.close.emit(true);
  }

}
