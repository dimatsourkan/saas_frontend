import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { QueryParamsService } from '@app/shared/services/queryParams/query-params.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { CrossPaymentsService } from '../cross-payments.service';

@Component({
  selector: 'app-cross-payments-list',
  templateUrl: './cross-payments-list.component.html',
  styleUrls: ['./cross-payments-list.component.scss']
})
export class CrossPaymentsListComponent implements OnInit {

  @ViewChild('editCommentModal') editCommentModal: ModalComponent;
  payments: any = {items: []};
  params$ = new Subject<any>();
  params: any = {};
  selectedPaymentId: string;
  loading: boolean;

  constructor(private _services: CrossPaymentsService,
              private swal: SweetAlertService,
              private queryParamsService: QueryParamsService) {
  }

  ngOnInit() {
    this.params = this.queryParamsService.getParams();
    if (!Object.prototype.hasOwnProperty.call(this.params, 'startDate')) {
      this.params.startDate = moment().subtract(90, 'day').startOf('month').format('YYYY-MM-DD');
      this.params.endDate = moment().format('YYYY-MM-DD');
    }

    this.params$.pipe(
      debounceTime(500)
    ).subscribe(params => {
      this.queryParamsService.setParams(params);
      this.getCrossPayments(params);
    });

    this.getCrossPayments(this.params);
  }

  getCrossPayments(params) {
    this._services.getCrossPayments(params).subscribe(res => {
      this.loading = false;
      this.payments = res;
    });
  }

  removeInvoice(id, index) {
    this.swal.confirm('Are you sure?').then(isConfirmed => {
      if (isConfirmed.value) {
        this._services.deleteCrossPayment(id).subscribe((res) => {
          this.payments.items.splice(index, 1);
          this.swal.success('Success!');
        }, (err) => this.swal.error(err.errors || 'Something went wrong!'));
      } else {
        this.swal.error('Canceled!');
      }
    });
  }

  sendPaid(id) {
    this.swal.confirm('Are you sure send pay?').then(isConfirmed => {
      if (isConfirmed.value) {
        this._services.sendPaid(id).subscribe((res) => {
          this.swal.success('Success!');
        }, (err) => this.swal.error(err.errors || 'Something went wrong!'));
      } else {
        this.swal.error('Canceled!');
      }
    });
  }

  downloadPayment(id) {
    this._services.downloadPayment(id).subscribe((response) => {
      const file = new Blob([response.body], {type: 'application/pdf;charset=utf-8'});
      FileSaver.saveAs(file, `cross-payment-${id}.pdf`, true);
    }, error => error);
  }

  editComment(id) {
    this.selectedPaymentId = id;
    this.editCommentModal.toggle();
  }

  calendarCallback({startDate, endDate}) {
    this.applyParams({startDate, endDate});
  }

  tableLimit(limit) {
    this.applyParams({page: 1, limit});
  }

  goToPage(page) {
    this.applyParams({page});
  }

  applyParams(params?) {
    this.loading = true;
    this.params = {...this.params, ...params};
    this.params$.next(this.params);
  }

}
