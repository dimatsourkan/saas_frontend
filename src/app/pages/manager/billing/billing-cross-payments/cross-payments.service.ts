import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { BaseUrlService } from '@app/shared/services/base-url/base-url.service';
import { QueryParamsService } from '@app/shared/services/queryParams/query-params.service';
import { AsyncSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CrossPaymentsService {

  statuses$ = new AsyncSubject();

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {

    setTimeout(() => {
      this.getCrossPaymentsStatuses();
    }, 5000);

  }

  getCrossPayments(queryParams: Params): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/cross_payment${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  getCrossPaymentsInvoices(queryParams: Params): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/cross_payment/invoices${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  getCrossPaymentsStatuses() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/cross_payment/statuses`,
      {headers: {toCamelCase: 'true'}}).subscribe((statuses: any) => {
      this.statuses$.next(statuses);
      this.statuses$.complete();
    });
  }

  getCrossPayment(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/cross_payment/${id}/preview`,
      {headers: {toCamelCase: 'true'}});
  }

  deleteCrossPayment(id): Observable<any> {
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/cross_payment/${id}`,
      {headers: {toCamelCase: 'true'}});
  }

  sendPaid(id): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/cross_payment/${id}/set_paid`,
      {headers: {toCamelCase: 'true'}});
  }

  downloadPayment(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/cross_payment/${id}/download`, {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  }

  updateCrossPayment(payment): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/cross_payment/${payment.id}`, payment,
      {headers: {toCamelCase: 'true'}});
  }

  saveCrossPayments(payment): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/cross_payment`, payment,
      {headers: {toCamelCase: 'true'}});
  }

}
