import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublishersInvoicesListComponent } from './publishers-invoices-list/publishers-invoices-list.component';
import { PublishersInvoicesManageComponent } from './publishers-invoices-manage/publishers-invoices-manage.component';
import { PublishersInvoicesInfoComponent } from './publishers-invoices-info/publishers-invoices-info.component';
import { PublishersInvoicesMergedListComponent } from './publishers-invoices-merged-list/publishers-invoices-merged-list.component';
import { PublishersInvoicesMergedInfoComponent } from './publishers-invoices-merged-info/publishers-invoices-merged-info.component';
import { PublishersScrubsListComponent } from './publishers-scrubs-list/publishers-scrubs-list.component';
import { PublishersScrubsManageComponent } from './publishers-scrubs-manage/publishers-scrubs-manage.component';
import { PublishersInvoicesLoggerListComponent } from './publishers-invoices-logger-list/publishers-invoices-logger-list.component';
import { PublishersScrubsLoggerComponent } from './publishers-scrubs-logger/publishers-scrubs-logger.component';
import { PublishersInvoicesCreateComponent } from './publishers-invoices-create/publishers-invoices-create.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'invoices/list',
    pathMatch: 'full'
  },
  {
    path: 'invoices',
    children : [
      {
        path: '', component: PublishersInvoicesListComponent,
        data: {
          name: 'titles.bill.pub.invoices',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'list', component: PublishersInvoicesListComponent,
        data: {
          name: 'titles.bill.pub.invoices',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'create', component: PublishersInvoicesCreateComponent,
        data: {
          name: 'titles.bill.pub.create',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'edit/:id', component: PublishersInvoicesManageComponent,
        data: {
          name: 'titles.bill.pub.edit',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'info/:id', component: PublishersInvoicesInfoComponent,
        data: {
          name: 'titles.bill.pub.info',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
    ]
  },
  {
    path: 'invoices-merged',
    children : [
      {
        path: '', component: PublishersInvoicesMergedListComponent,
        data: {
          name: 'titles.bill.pub.merged',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'list', component: PublishersInvoicesMergedListComponent,
        data: {
          name: 'titles.bill.pub.merged',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'info/:id', component: PublishersInvoicesMergedInfoComponent,
        data: {
          name: 'titles.bill.pub.merged-info',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
    ]
  },
  {
    path: 'scrubs',
    children : [
      {
        path: '', component: PublishersScrubsListComponent,
        data: {
          name: 'titles.bill.pub.scrubs',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'list', component: PublishersScrubsListComponent,
        data: {
          name: 'titles.bill.pub.scrubs',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'create', component: PublishersScrubsManageComponent,
        data: {
          name: 'titles.bill.pub.create-scrub',
          menuList: ['billing', 'publisherBilling'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
    ]
  },
  {
    path: 'invoices-logger/list', component: PublishersInvoicesLoggerListComponent,
    data: {
      name: 'titles.bill.pub.invoicesLogger',
      menuList: ['billing', 'publisherBilling'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'scrub-logger/list', component: PublishersScrubsLoggerComponent,
    data: {
      name: 'titles.bill.pub.scrubsLogger',
      menuList: ['billing', 'publisherBilling'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingPublishersRoutingModule {
}
