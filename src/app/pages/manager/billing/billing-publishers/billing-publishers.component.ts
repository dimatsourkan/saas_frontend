import { Component } from '@angular/core';

@Component({
  selector: 'app-billing-publishers',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class BillingPublishersComponent {
}
