import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingPublishersRoutingModule } from './billing-publishers-routing.module';
import { BillingPublishersComponent } from './billing-publishers.component';
import { PublishersInvoicesListComponent } from './publishers-invoices-list/publishers-invoices-list.component';
import { PublishersInvoicesManageComponent } from './publishers-invoices-manage/publishers-invoices-manage.component';
import { PublishersInvoicesInfoComponent } from './publishers-invoices-info/publishers-invoices-info.component';
import { PublishersInvoicesMergedInfoComponent } from './publishers-invoices-merged-info/publishers-invoices-merged-info.component';
import { PublishersInvoicesMergedListComponent } from './publishers-invoices-merged-list/publishers-invoices-merged-list.component';
import { PublishersScrubsListComponent } from './publishers-scrubs-list/publishers-scrubs-list.component';
import { PublishersScrubsManageComponent } from './publishers-scrubs-manage/publishers-scrubs-manage.component';
import { SharedModule } from '../../../../shared/shared.module';
import { PublishersInvoicesLoggerListComponent } from './publishers-invoices-logger-list/publishers-invoices-logger-list.component';
import { PublishersScrubsLoggerComponent } from './publishers-scrubs-logger/publishers-scrubs-logger.component';
import { PublishersInvoicesCreateComponent } from './publishers-invoices-create/publishers-invoices-create.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BillingPublishersRoutingModule
  ],
  declarations: [BillingPublishersComponent,
    PublishersInvoicesListComponent,
    PublishersInvoicesManageComponent,
    PublishersInvoicesInfoComponent, PublishersInvoicesMergedInfoComponent,
    PublishersInvoicesMergedListComponent, PublishersScrubsListComponent,
    PublishersScrubsManageComponent,
    PublishersInvoicesLoggerListComponent,
    PublishersScrubsLoggerComponent,
    PublishersInvoicesCreateComponent]
})
export class BillingPublishersModule {
}
