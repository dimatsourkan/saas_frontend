import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class BillingPublishersService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getPartsTypes(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/part/type`,
      {headers: {toCamelCase: 'true'}});
  }

  getInvoicesType(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/type`,
      {headers: {toCamelCase: 'true'}});
  }

  getInvoices(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  getInvoiceInfo(id: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/${id}`,
      {headers: {toCamelCase: 'true'}});
  }

  getInvoice(id: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/${id}/info`,
      {headers: {toCamelCase: 'true'}});
  }

  getInvoiceMerge(id: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/merge/${id}`,
      {headers: {toCamelCase: 'true'}});
  }

  getStatutes(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/status`,
      {headers: {toCamelCase: 'true'}});
  }

  getTotalUnpaid(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/total_unpaid${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  changeOfferStatus(id, data): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/publisher_invoice/${id}/status`, data,
      {headers: {toCamelCase: 'true'}});
  }

  mergeInvoices(data): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/publisher_invoice/merge`, data,
      {headers: {toCamelCase: 'true'}});
  }

  saveInvoiceFileToServer(tempInvoiceId, data): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/publisher_invoice/${tempInvoiceId}/file`, data,
      {headers: {toCamelCase: 'true'}});
  }

  getInvoiceStatuses(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/advertiser_invoices/statuses`,
      {headers: {toCamelCase: 'true'}});
  }

  getInvoiceStatusesPublisherMerged(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/merge/status`,
      {headers: {toCamelCase: 'true'}});
  }

  getMergedInvoices(queryParams: Params): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/merge${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  changeInvoiceStatus(id, data): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/publisher_invoice/merge/${id}/status`, data,
      {headers: {toCamelCase: 'true'}});
  }


  createPublisherInvoice(data): Observable<any> {
    data.periodTo = moment(data.periodTo).format('YYYY-MM-DD');
    data.periodFrom = moment(data.periodFrom).format('YYYY-MM-DD');
    return this.http.post(`${this.url.getBaseUrlWithRole()}/publisher_invoice`, data,
      { headers: { toCamelCase: 'true' } });
  }

  editPublisherInvoice(id, data): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/publisher_invoice/${id}`, data,
      {headers: {toCamelCase: 'true'}});
  }


  deleteMergedInvoice(id): Observable<any> {
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/publisher_invoice/merge/${id}`,
      {headers: {toCamelCase: 'true'}});
  }

  getFilesToDownload(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/${id}/file`, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  downloadInvoice(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/${id}/download`, {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  }

  downloadInvoiceMerge(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/merge/${id}/download`, {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  }

  getLoggerList(queryParams: Params): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing_logger/publisher_invoice${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  getActions(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing_logger/publisher_invoice/actions`,
      {headers: {toCamelCase: 'true'}});
  }

}
