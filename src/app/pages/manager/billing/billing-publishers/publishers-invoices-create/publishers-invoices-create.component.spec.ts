import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishersInvoicesCreateComponent } from './publishers-invoices-create.component';

describe('PublishersInvoicesCreateComponent', () => {
  let component: PublishersInvoicesCreateComponent;
  let fixture: ComponentFixture<PublishersInvoicesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishersInvoicesCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishersInvoicesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
