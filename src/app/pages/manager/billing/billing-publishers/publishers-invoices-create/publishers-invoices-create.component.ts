import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { BillingPublishersService } from '../billing-publishers.service';


@Component({
  selector: 'app-publishers-invoices-create',
  templateUrl: './publishers-invoices-create.component.html',
  styleUrls: ['./publishers-invoices-create.component.scss']
})
export class PublishersInvoicesCreateComponent implements OnInit {

  errors: any;
  invoiceForm: FormGroup;
  items: FormArray;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private swal: SweetAlertService,
    private validatorService: ValidatorService,
    private _publishersServices: BillingPublishersService) {
  }

  get formItems() {
    return this.invoiceForm.get('items') as FormArray;
  }

  ngOnInit() {
    this.invoiceForm = this.fb.group({
      publisherId: [null],
      periodFrom: [],
      periodTo: [],
      currency: [],
      fee: [0],
      notes: [],
      items: this.fb.array([this.createItem()]),
    });
  }

  createItem(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      amount: this.fb.group({
        amount: []
      })
    });
  }

  createPublisherInvoice() {
    this._publishersServices.createPublisherInvoice(this.invoiceForm.getRawValue()).subscribe((response) => {
      this.router.navigate(['/', 'manager', 'billing', 'publishers']);
    }, (err) => {
      if (typeof err.errors === 'string') {
        this.swal.error(err.errors);
      } else {
        this.validatorService.addErrorToForm(this.invoiceForm, err);
      }
    });
  }

  addItem(): void {
    this.items = this.invoiceForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  removeFromFormArray(name: string, index: number): void {
    (<FormArray>this.invoiceForm.get(name)).removeAt(index);
  }

}
