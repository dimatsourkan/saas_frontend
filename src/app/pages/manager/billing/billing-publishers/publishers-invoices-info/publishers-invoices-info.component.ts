import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { forkJoin } from 'rxjs';
import { BillingPublishersService } from '../billing-publishers.service';

@Component({
  selector: 'app-publishers-invoices-info',
  templateUrl: './publishers-invoices-info.component.html',
  styleUrls: ['./publishers-invoices-info.component.scss']
})
export class PublishersInvoicesInfoComponent implements OnInit {
  dateFormat: string;
  partTypes: any;
  invoice: any = {
    offers: [],
    invoiceCalculation: [],
    company: {name: '', country: '', phone: '', email: ''},
    publisher: {name: ''},
    paymentInfo: {
      address: '',
      phone: '',
      email: ''
    },
    period: {
      from: '',
      to: ''
    },
    amount: {
      amount: 0,
      currency: ''
    },
    amountDetails: {
      compensation: {
        amount: 0,
        currency: ''
      },
      rejected: {
        amount: 0,
        currency: ''
      },
      hold: {
        amount: 0,
        currency: ''
      },
      total: {
        amount: 0,
        currency: ''
      }
    }
  };
  invoiceId: string;

  constructor(
    private route: ActivatedRoute,
    private _billingPublishersService: BillingPublishersService) {
  }

  ngOnInit() {
    this.dateFormat = moment.localeData().longDateFormat('L');

    this.route.params.subscribe(params => {
      this.invoiceId = params['id'];
      forkJoin([this.getPartsTypes(), this.getInvoice(params['id'])])
        .subscribe(response => {
          this.dataReady(response[0], response[1]);
        });
    });
  }

  getPartsTypes() {
    return this._billingPublishersService.getPartsTypes();
  }

  getInvoice(id) {
    return this._billingPublishersService.getInvoice(id);
  }

  dataReady(types, invoice) {
    this.partTypes = types;
    this.invoice = invoice;
    // tslint:disable-next-line:forin
    for (const partId in this.invoice.amountDetails.parts) {
      const part = this.invoice.amountDetails.parts[partId];
      this.partTypes.forEach((type) => {
        if (Number(partId) === type.id) {
          part.action = type.original === 'compensation' ? '+' : '-';
          part.name = type.name;
        }
      });
    }
  }

  downloadInvoice() {
    return this._billingPublishersService.downloadInvoice(this.invoiceId).subscribe((response) => {
      const file = new Blob([response.body], {type: 'application/pdf;charset=utf-8'});
      FileSaver.saveAs(file, 'invoice.pdf', true);
    });
  }

}

