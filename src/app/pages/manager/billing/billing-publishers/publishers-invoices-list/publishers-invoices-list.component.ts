import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { DateFormattedService } from '../../../../../shared/services/date-formatted/date-formatted.service';
import { QueryParamsService } from '../../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { BillingPublishersService } from '../billing-publishers.service';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'app-publishers-invoices-list',
  templateUrl: './publishers-invoices-list.component.html',
  styleUrls: ['./publishers-invoices-list.component.scss']
})
export class PublishersInvoicesListComponent implements OnInit {

  @ViewChild('fileUploadModal') private fileUploadModal: ModalComponent;
  @ViewChild('invoiceModal') private invoiceModal: ModalComponent;

  params: any;
  loading: boolean;
  order: string;
  dateFormat: string;
  invoiceStatuses: any;
  scrollFlag: boolean;
  filterIsVisible: boolean;
  selectedInvoices: any[];
  currency: string;
  invoiceFile: { name: string; extension: string; content: string } = { name: '', extension: '', content: '' };
  main: any;
  invoiceInfo: any = {
    invoiceNumber: 0,
    publisher: {
      name: ''
    },
    paymentInfo: {
      address: '',
      phone: '',
      email: ''
    },
    amount: {
      amount: 0
    },
    amountDetails: {
      compensation: {
        amount: 0
      },
      rejected: {
        amount: 0
      },
      hold: {
        amount: 0
      },
      total: {
        amount: 0
      }
    }
  };
  totalUnpaid: any = { amount: 0, currency: '' };
  invoices: any = { items: [] };
  tempInvoiceId: any;
  partsTypes: any;
  search: any;
  invoicesType: any;

  constructor(private dateFormattedService: DateFormattedService,
    private swal: SweetAlertService,
    private _billingPublishersService: BillingPublishersService,
    private queryParamsService: QueryParamsService) {
  }

  ngOnInit() {
    this.loading = true;
    this.scrollFlag = false;
    this.filterIsVisible = false;
    this.order = 'ASC';
    this.selectedInvoices = [];
    this.params = this.queryParamsService.getParams();
    this.getInvoicesType();
    if (this.params.payment_terms) {
      this.params.payment_terms = this.params.payment_terms.map(item => Number(item));
    }
    this.getPartsTypes().subscribe((res) => {
      this.partsTypes = res;
    });
    this.dateFormat = moment.localeData().longDateFormat('L');
    DataStore.currency.current.asObservable$.subscribe(currency => this.currency = currency);
    if (Object.prototype.hasOwnProperty.call(this.params, 'id')) {
      this.getInvoiceInfo(this.params.id);
    }

    if (!Object.prototype.hasOwnProperty.call(this.params, 'date_from')) {
      this.params.date_from = moment().startOf('month').subtract(1, 'month').format(this.dateFormattedService.dateFormat);
      this.params.date_to = moment().format(this.dateFormattedService.dateFormat);
    }

    this.getStatutes().subscribe((res) => {
      this.invoiceStatuses = res;
      this.getInvoices(this.params);
    });
  }

  getInvoicesType() {
    this._billingPublishersService.getInvoicesType().subscribe(res => {
      this.invoicesType = res;
    });
  }

  getPartsTypes() {
    return this._billingPublishersService.getPartsTypes();
  }

  getStatutes() {
    return this._billingPublishersService.getStatutes();
  }

  getInvoices(params) {
    return this._billingPublishersService.getInvoices(params).subscribe((response) => {
      this.getTotalUnpaid(params);
      this.invoices = response;
      this.invoices.items.forEach((invoice) => {
        invoice.availableStatusesArr = [];
        this.invoiceStatuses.forEach((status) => {
          if (invoice.status !== status.id) {
            invoice.availableStatusesArr.push(status);
          }
        });

        this.invoicesType.forEach((item) => {
          if (invoice.type === item.id) {
            invoice.type = item.name;
          }
        });

        for (const partId in invoice.amountDetails.parts) {
          if (invoice.amountDetails.parts.hasOwnProperty(partId)) {
            const part = invoice.amountDetails.parts[partId];
            this.partsTypes.forEach((type) => {
              if (Number(partId) === type.id) {
                part.name = type.name;
                part.action = type.original === 'compensation' ? '+' : '-';
              }
            });
          }
        }
      });
      this.scrollFlag = !this.scrollFlag;
      this.params.page = this.invoices.page;
      this.params.limit = this.invoices.limit;
      this.queryParamsService.setParams(this.params);
      this.loading = false;
    });
  }

  getInvoiceInfo(id) {
    this.loading = true;
    return this._billingPublishersService.getInvoiceInfo(id).subscribe((response) => {
      this.invoiceInfo = response;
      this.invoiceStatuses.forEach((status) => {
        if (this.invoiceInfo.status === status.id) {
          this.invoiceInfo.statusOriginal = status.original;
        }
      });

      for (const partId in this.invoiceInfo.amountDetails.parts) {
        if (this.invoiceInfo.amountDetails.parts.hasOwnProperty(partId)) {
          const part = this.invoiceInfo.amountDetails.parts[partId];
          this.partsTypes.forEach((type) => {
            if (Number(partId) === type.id) {
              this.invoiceInfo.amountDetails[type.original] = part;
            }
          });
        }
      }

      this.params.id = id;
      this.queryParamsService.setParams(this.params);
      this.loading = false;
      this.toggleInvoiceModal('open');
    });

  }

  getTotalUnpaid(params) {
    const data = Object.assign({ currency: this.currency }, params);
    return this._billingPublishersService.getTotalUnpaid(data).subscribe((response) => {
      this.totalUnpaid = response;
    });
  }


  changeOfferStatus(invoice, status) {
    const data = {
      id: invoice.id,
      status: status
    };
    return this._billingPublishersService.changeOfferStatus(invoice.id, data).subscribe((response) => {
      const item = this.invoices.items.find(v => v.id === invoice.id);
      item.status = response.status;
      item.availableStatuses = response.availableStatuses;
    }, (err) => {
      this.swal.error(this.parseError(err.errors));
    });
  }

  parseError(err) {
    if (typeof err === 'string') {
      return err;
    } else if (Array.isArray(err)) {
      return err[0];
    } else {
      return 'Has error';
    }
  }

  mergeInvoices() {
    const data = {
      ids: this.selectedInvoices
    };
    return this._billingPublishersService.mergeInvoices(data).subscribe((response) => {
      this.invoices.items.forEach((item) => {
        this.selectedInvoices.forEach((invoice) => {
          if (invoice === item.id) {
            item.isChecked = false;
            this.invoiceStatuses.forEach((statusObj) => {
              if (statusObj.original === 'merged') {
                item.availableStatuses = [];
                item.status = statusObj.id;
              }
            });
          }
        });
      });
      this.selectedInvoices = [];
      this.swal.success('Merged Successful');
    }, (err) => {
      this.swal.error(err.errors);
    });
  }

  checkingInvoice(flag, id) {
    if (!flag) {
      this.unCheckingInvoices(id);
      return false;
    }
    this.selectedInvoices.push(id);
  }

  unCheckingInvoices(id) {
    let invoiceIndex = 0;
    this.selectedInvoices.forEach((invoiceId, index) => {
      if (invoiceId === id) {
        invoiceIndex = index;
      }
    });
    this.selectedInvoices.splice(invoiceIndex, 1);
  }

  resetFilters() {
    this.loading = true;
    this.params.publisher_id = null;
    this.params.manager_id = null;
    this.params.payment_terms = null;
    this.params.target = null;
    this.params.status = null;
    this.params.search = null;
    this.params.type = null;
    this.getInvoices(this.params);
    this.toggleFilterVisibility('close');
  }

  resetFilter(filter) {
    this.params[filter] = null;
  }

  applyFilters() {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = this.invoices.limit;
    this.getInvoices(this.params);
    this.toggleFilterVisibility('close');
  }

  applySearch(search) {
    this.params.search = search;
    this.loading = true;
    this.params.page = 1;
    this.params.limit = this.invoices.limit;
    this.getInvoices(this.params);
  }

  sortByField(field) {
    this.params.sort = field;
    this.params.page = 1;
    this.params.order = this.order;
    this.order = this.order === 'ASC' ? 'DESC' : 'ASC';
    this.loading = true;
    this.getInvoices(this.params);
  }

  calendarCallback({ startDate, endDate }) {
    this.loading = true;
    this.params.date_from = startDate;
    this.params.date_to = endDate;
    this.getInvoices(this.params);
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  toggleInvoiceModal(flag) {
    if (flag === 'open') {
      this.invoiceModal.open();
    } else {
      this.invoiceModal.close();
      this.params.id = null;
      this.queryParamsService.setParams(this.params);
    }
  }

  goToPage(page) {
    this.loading = true;
    this.params.page = page;
    this.getInvoices(this.params);
  }

  tableLimit(number) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = number;
    this.getInvoices(this.params);
  }

  toggleFilterVisibility(flag?) {
    if (flag === 'close') {
      this.filterIsVisible = false;
    } else {
      this.filterIsVisible = !this.filterIsVisible;
    }
  }

  toggleUploadModal(flag, id?) {
    flag === 'open' ? this.fileUploadModal.open() : this.fileUploadModal.close();
    this.tempInvoiceId = id;
  }

  clearFn(directiveFn, flag) {
    this[flag] = directiveFn;
  }

  clearInvoiceFile() {
    this.invoiceFile = { name: '', extension: '', content: '' };
  }

  uploadInvoiceFile(file) {
    this.invoiceFile = file;
  }

  saveInvoiceFileToServer() {
    const data = { fileName: this.invoiceFile.name, content: this.invoiceFile.content };
    return this._billingPublishersService.saveInvoiceFileToServer(this.tempInvoiceId, data).subscribe((response) => {
      this.swal.success('Success');
      this.invoices.items.find(item => item.id === this.tempInvoiceId).fileId = response.fileId;
      this.tempInvoiceId = null;
      this.clearInvoiceFile();
      this.toggleUploadModal('close');
    }, (err) => {
      this.swal.error(err.errors);
    });
  }

  downloadInvoiceFile(id) {
    this._billingPublishersService.getFilesToDownload(id).subscribe((response) => {
      const type = response.headers.get('content-type');
      const fileInfo = type.split('/');
      if (fileInfo[0] === 'image') {
        FileSaver.saveAs(response.body, `invoice-${id}.${fileInfo[1]}`);
      } else {
        const file = new Blob([response.body], { type: `${type};charset=utf-8` });
        FileSaver.saveAs(file, `invoice-${id}.${fileInfo[1]}`);
      }
      this.swal.success('Success');
    }, (err) => {
      this.swal.error(err.errors);
    });
  }

}
