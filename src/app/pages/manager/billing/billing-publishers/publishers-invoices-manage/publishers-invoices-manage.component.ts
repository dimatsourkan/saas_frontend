import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { DefaultList } from '../../../../../shared/interfaces/default-list';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { BillingPublishersService } from '../billing-publishers.service';

@Component({
  selector: 'app-publishers-invoices-manage',
  templateUrl: './publishers-invoices-manage.component.html',
  styleUrls: ['./publishers-invoices-manage.component.scss']
})
export class PublishersInvoicesManageComponent implements OnInit {
  dateFormat: string;
  invoiceId: string | null;
  invoice: any;
  invoiceStatuses: any;
  invoiceDate: any;
  errors: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private swal: SweetAlertService,
    private _publishersServices: BillingPublishersService) {
  }

  ngOnInit() {
    this.invoice = {
      items: [],
      crossPayments: [],
      deductions: [],
      availableStatuses: [],
      ourAmount: 0,
      notes: false,
      info: '',
      invoiceDate: {},
      advertiserId: '',
      advertiserAmount: 0,
      month: {},
      publisher: {
        id: '',
        name: '',
        managerId: ''
      },
      amount: {
        amount: 0,
        currency: ''
      },
      total: {
        amount: 0,
        currency: ''
      },
      paymentInfo: {
        paymentTermsName: ''
      },
      invoiceMonth: {}
    };
    this.dateFormat = moment.localeData().longDateFormat('L');

    this.route.paramMap.subscribe((params) => {
      if (params.get('id')) {
        this.invoiceId = params.get('id');
        this.getInvoiceStatuses();
      }
    });
  }

  getInvoiceStatuses() {
    return this._publishersServices.getStatutes().subscribe((response) => {
      this.invoiceStatuses = response;
      this.getPublisherInvoice();
    });
  }

  getPublisherInvoice() {
    return this._publishersServices.getInvoiceInfo(this.invoiceId).subscribe((response) => {
      this.invoice = { ...this.invoice, ...response };
      this.invoice.periodForFront = `${this.formatDate(this.invoice.period.from)} - ${this.formatDate(this.invoice.period.to)}`;
      this.invoice.availableStatusesArr = [];
      this.invoiceStatuses.forEach((status) => {
        if (this.invoice.status === status.id) {
          this.invoice.statusName = status.name;
        } else {
          this.invoice.availableStatusesArr.push(status);
        }
      });
    });
  }

  editPublisherInvoice() {
    const data = {
      id: this.invoice.id,
      fee: this.invoice.fee,
      status: this.invoice.status,
      notes: this.invoice.notes,
      items: this.invoice.items
    };
    this._publishersServices.editPublisherInvoice(this.invoiceId, data).subscribe((response) => {
      this.router.navigate(['/', 'manager', 'billing', 'publishers']);
    }, (err) => {

      if (typeof err.errors === 'string') {
        this.swal.error(err.errors);
      }
    });
  }

  addItem() {
    this.invoice.items.push({ name: '', amount: { amount: null } });
  }

  removeItems(index) {
    this.invoice.items.splice(index, 1);
  }

  formatDate(date) {
    if (date) {
      return moment(date).format(this.dateFormat);
    } else {
      return '';
    }
  }
}
