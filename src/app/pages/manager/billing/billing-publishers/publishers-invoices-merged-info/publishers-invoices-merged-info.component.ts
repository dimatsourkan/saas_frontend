import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as FileSaver from 'file-saver';
import { forkJoin } from 'rxjs';
import { BillingPublishersService } from '../billing-publishers.service';


@Component({
  selector: 'app-publishers-invoices-merged-info',
  templateUrl: './publishers-invoices-merged-info.component.html',
  styleUrls: ['./publishers-invoices-merged-info.component.scss']
})
export class PublishersInvoicesMergedInfoComponent implements OnInit {
  invoiceId: string;
  partTypes: any;
  invoice: any = {};

  constructor(
    private route: ActivatedRoute,
    private _billingPublishersService: BillingPublishersService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.invoiceId = params['id'];
      forkJoin([this.getPartsTypes(), this.getInvoice(this.invoiceId)])
        .subscribe(response => {
          this.dataReady(response[0], response[1]);
        });
    });
  }

  getPartsTypes() {
    return this._billingPublishersService.getPartsTypes();
  }

  getInvoice(id) {
    return this._billingPublishersService.getInvoiceMerge(id);
  }

  dataReady(types, invoice) {
    this.partTypes = types;
    this.invoice = invoice;
    this.invoice.total = 0;
    this.invoice.invoices.forEach((invoiceObj) => {
      this.invoice.total += invoiceObj.amountDetails.total.amount;
      invoiceObj.parts.forEach((part) => {
        this.partTypes.forEach((type) => {
          if (part.type === type.id) {
            part.typeName = type.name;
            part.action = type.original === 'compensation' ? '+' : '-';
          }
        });
      });
    });
  }

  downloadInvoice() {
    return this._billingPublishersService.downloadInvoiceMerge(this.invoiceId).subscribe((response) => {
      const file = new Blob([response.body], {type: 'application/pdf;charset=utf-8'});
      FileSaver.saveAs(file, 'invoice.pdf', true);
    });
  }

}
