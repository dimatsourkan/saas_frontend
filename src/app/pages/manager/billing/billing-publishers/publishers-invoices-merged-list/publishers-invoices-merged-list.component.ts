import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { forkJoin } from 'rxjs';
import { QueryParamsService } from '../../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { BillingPublishersService } from '../billing-publishers.service';

export interface Invoice {
  statusName: any;
  dateEnd: string;
  items: any[];
  crossPayments: any[];
  deductions: any[];
  availableStatuses: any[];
  ourAmount: number;
  notes: boolean;
  info: string;
  status: string;
  frontName: string;
  dateStart: string;
  invoiceDate: any;
  advertiserId: string;
  advertiserAmount: number;
  month: any;
  createdAt: any;
  id: string;
  period: {
    from: string;
    to: string;
  };
  publisher: {
    id: string,
    name: string,
    managerId: string,
    managerName: string
  };
  amount: {
    amount: number,
    currency: string
  };
  total: {
    amount: number,
    currency: string
  };
  paymentInfo: {
    paymentTermsName: string
  };
  invoiceMonth: any;
  availableStatusesArr: any[];
}

@Component({
  selector: 'app-publishers-invoices-merged-list',
  templateUrl: './publishers-invoices-merged-list.component.html',
  styleUrls: ['./publishers-invoices-merged-list.component.scss']
})
export class PublishersInvoicesMergedListComponent implements OnInit {
  dateFormat: string;
  loading: boolean;
  scrollFlag: boolean;
  order: string;
  createdAt: string;
  params: any;
  invoiceStatuses: any;
  invoices: {
    page: any;
    limit: any;
    totalPages: any;
    items: Invoice[]
  } = {
    items: [],
    page: 1,
    totalPages: 1,
    limit: 10
  };

  constructor(
    private swal: SweetAlertService,
    private _billingPublishersService: BillingPublishersService,
    private queryParamsService: QueryParamsService) {
  }

  ngOnInit() {
    this.dateFormat = moment.localeData().longDateFormat('L');
    this.loading = true;
    this.scrollFlag = false;
    this.order = 'ASC';
    this.params = this.queryParamsService.getParams();

    forkJoin([this.getInvoiceStatuses(), this.getMergedInvoices(this.params)])
      .subscribe(response => {
        this.dataReady(response[0], response[1]);
      });
  }

  getInvoiceStatuses() {
    return this._billingPublishersService.getInvoiceStatusesPublisherMerged();
  }

  getMergedInvoices(params) {
    return this._billingPublishersService.getMergedInvoices(params);
  }

  dataReady(statuses, invoices) {
    if (statuses) {
      this.invoiceStatuses = statuses;
    }
    this.invoices = invoices;
    this.invoices.items.map((invoice) => {
      invoice.dateStart = this.formatDate(invoice.period.from);
      invoice.dateEnd = this.formatDate(invoice.period.to);
      invoice.frontName = `${invoice.publisher.name} ${invoice.dateStart} - ${invoice.dateEnd}`;
      invoice.availableStatusesArr = [];
      this.invoiceStatuses.map((status) => {
        if (invoice.status === status.id) {
          invoice.statusName = status.name;
        } else {
          invoice.availableStatusesArr.push(status);
        }
      });
    });
    this.scrollFlag = !this.scrollFlag;
    this.params.page = this.invoices.page;
    this.params.limit = this.invoices.limit;
    this.queryParamsService.setParams(this.params);
    this.loading = false;
  }

  changeInvoiceStatus(invoice, status) {
    const data = {
      id: invoice.id,
      status: status
    };
    return this._billingPublishersService.changeInvoiceStatus(invoice.id, data).subscribe((response) => {
      invoice.availableStatuses = [];
      this.invoiceStatuses.map((statusObj) => {
        if (status === statusObj.id) {
          invoice.statusName = statusObj.name;
        }
      });
    }, (err) => {
      this.swal.error(err.errors[0]);
    });
  }

  deleteMergedInvoice(id, index) {
    return this._billingPublishersService.deleteMergedInvoice(id).subscribe((response) => {
      this.swal.success('');
      this.invoices.items.splice(index, 1);
    }, (err) => {
      this.swal.error(err.errors[0]);
    });
  }

  formatDate(data) {
    return moment(data).format(this.dateFormat);
  }

  goToPage(page) {
    this.loading = true;
    this.params.page = page;
    this.getMergedInvoices(this.params).subscribe(response => this.dataReady(null, response));
  }

  tableLimit(number) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = number;
    this.getMergedInvoices(this.params).subscribe(response => this.dataReady(null, response));
  }

  sortByField(field) {
    this.params.sort = field;
    this.params.page = 1;
    this.params.order = this.order;
    this.order = this.order === 'ASC' ? 'DESC' : 'ASC';
    this.loading = true;
    this.getMergedInvoices(this.params).subscribe(response => this.dataReady(null, response));
  }

  applySearch(search) {
    this.loading = true;
    this.params.page = 1;
    this.params.search = search;
    this.params.limit = this.invoices.limit;
    this.params.search = this.params.search === '' ? null : this.params.search;
    this.getMergedInvoices(this.params).subscribe(response => this.dataReady(null, response));
  }
}
