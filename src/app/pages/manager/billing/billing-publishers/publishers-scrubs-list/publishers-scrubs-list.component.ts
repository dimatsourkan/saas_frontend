import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { forkJoin } from 'rxjs';
import { DateFormattedService } from '../../../../../shared/services/date-formatted/date-formatted.service';

import { QueryParamsService } from '../../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { ScrubsService } from '../scrubs.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-publishers-scrubs-list',
  templateUrl: './publishers-scrubs-list.component.html',
  styleUrls: ['./publishers-scrubs-list.component.scss']
})
export class PublishersScrubsListComponent implements OnInit {
  loading: boolean;
  filterOpen: boolean;
  searchFilter: boolean;
  scrollFlag: boolean;
  order: string;
  dateFormat: string;
  date: string;
  params: any;
  scrubStatuses: any;
  statusFilter: string;
  partTypes: any = [];
  partStatuses: any = [];
  scrubs: any = { item: [], items: [] };

  constructor(private dateFormattedService: DateFormattedService,
    private swal: SweetAlertService,
    private _scrubService: ScrubsService,
    private queryParamsService: QueryParamsService) {
  }

  ngOnInit() {
    this.loading = true;
    this.filterOpen = false;
    this.searchFilter = false;
    this.scrollFlag = false;
    this.order = 'ASC';

    this.dateFormat = moment.localeData().longDateFormat('L');

    this.params = this.queryParamsService.getParams();
    if (!Object.prototype.hasOwnProperty.call(this.params, 'dateFrom')) {
      this.setDefaultParams();
    }
    this.initData();
  }

  initData() {
    forkJoin([this.getScrubs(this.params), this.getScrubStatuses(), this.getPartsTypes(), this.getPartsStatuses()])
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe(response => {
        this.dataReady(response[0], response[1], response[2], response[3]);
      }, err => {
        this.setDefaultParams();
        this.params.page = 1;
        this.initData();
      });
  }

  getScrubStatuses() {
    return this._scrubService.getScrubStatuses();
  }

  setDefaultParams() {
    this.params.dateFrom = moment().subtract(1, 'months').startOf('month').format(this.dateFormattedService.dateFormat);
    this.params.dateTo = moment().subtract(1, 'months').endOf('month').format(this.dateFormattedService.dateFormat);
  }

  getScrubs(params) {
    return this._scrubService.getScrubs(params);
  }

  getPartsTypes() {
    return this._scrubService.getPartsTypes();
  }

  getPartsStatuses() {
    return this._scrubService.getPartsStatuses();
  }

  dataReady(scrubs, scrubStatuses, partTypes, partStatuses) {
    if (scrubStatuses) {
      this.scrubStatuses = scrubStatuses;
      if (!Object.prototype.hasOwnProperty.call(this.params, 'status')) {
        this.params.status = null;
        this.statusFilter = 'All';
      } else {
        this.setStatusFilterOnInit();
      }
    }
    if (partTypes) {
      this.partTypes = partTypes;
    }
    if (partStatuses) {
      this.partStatuses = partStatuses.filter((item) => item.id = String(item.id));
    }
    this.scrollFlag = !this.scrollFlag;
    this.dataReadyForScrubs(scrubs);
  }

  dataReadyForScrubs(scrubs) {
    this.loading = false;
    this.scrubs = scrubs;
    this.scrubs.items.forEach((scrub) => {
      this.scrubDataManipulate(scrub);
    });
    this.params.page = this.scrubs.page;
    this.params.limit = this.scrubs.limit;
    this.queryParamsService.setParams(this.params);
  }

  calendarCallback({
    startDate,
    endDate
  }) {
    this.loading = true;
    this.params.page = 1;
    this.params.dateFrom = startDate;
    this.params.dateTo = endDate;
    this.getScrubs(this.params)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.dataReadyForScrubs(response));
  }

  resetFilters() {
    this.params = {};
    this.searchFilter = false;
    this.order = null;
    this.loading = true;
    this.setDefaultParams();
    this.setStatusFilter();
  }

  applyFilters() {
    this.searchFilter = false;
    this.loading = true;
    if (!this.params.total) {
      delete this.params.total;
    }
    this.getScrubs(this.params)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.dataReadyForScrubs(response));
  }

  sortByField(field) {
    this.params.sort = field;
    this.params.page = 1;
    this.params.order = this.order;
    this.order = this.order === 'ASC' ? 'DESC' : 'ASC';
    this.loading = true;
    this.getScrubs(this.params)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.dataReadyForScrubs(response));
  }

  findAncestor(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls)) {
    }
    return el;
  }

  expandTable(e, scrub) {
    scrub.expandTable = !scrub.expandTable;

  }

  toggleSearchFilter() {
    this.searchFilter = !this.searchFilter;
  }

  toggleFilter() {
    this.filterOpen = !this.filterOpen;
  }

  goToPage(page) {
    this.loading = true;
    this.params.page = page;
    this.getScrubs(this.params)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.dataReadyForScrubs(response));
  }

  tableLimit(number) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = number;
    this.getScrubs(this.params)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.dataReadyForScrubs(response));
  }

  updateScrub(scrub, index) {
    const data = {
      notes: scrub.notes,
      status: scrub.status,
      costs: scrub.costs.amount
    };
    this._scrubService.updateScrub(scrub.id, data).subscribe((response) => {
      this.scrubs.items[index] = response;
      this.scrubDataManipulate(this.scrubs.items[index]);
      this.swal.success('Ok');
    }, (err) => {
      scrub.hasError = true;
      this.swal.error(this.generateError(err, true));
    });
  }

  addNewPart(parts) {
    const data = {
      editing: true,
      invoiceNumber: null,
      publisher: {
        id: null
      },
      amount: {
        amount: null
      },
      notes: '',
      type: null,
      status: null
    };
    parts.push(data);
  }

  setEditingAmount(part) {
    part.editingAmount = true;
  }

  isPartValid(parts) {
    const temp = parts.filter(part => part.editing || part.editingAmount).map((item) => {
      if ((!item.amount || !item.amount.amount) || !item.status || !item.type) {
        item.hasError = true;
      } else {
        item.hasError = false;
      }
      return item;
    });
    return temp.every((part) => !part.hasError);
  }

  updateParts(id, parts) {
    const isValid = this.isPartValid(parts);
    if (isValid) {
      this.loading = true;
      const data = [];
      parts.forEach((part) => {
        const obj: any = {
          notes: part.notes,
          amount: part.amount.amount,
          type: Number(part.type),
          status: Number(part.status)
        };
        if (part.id) {
          obj.id = part.id;
          obj.invoiceNumber = Number(part.invoiceNumber) || null;
          obj.publisherId = part.publisher.id || null;
        } else {
          obj.publisherId = part.publisher.id || null;
          obj.invoiceNumber = Number(part.invoiceNumber) || null;
        }
        data.push(obj);
      });
      this._scrubService.updateParts(id, { parts: data }).subscribe((response) => {
        this.loading = false;
        this.scrubs.items.forEach((scrub) => {
          if (scrub.id === id) {
            scrub.parts = response.parts;
            this.scrubDataManipulate(scrub);
          }
        });
      }, (err) => {
        this.swal.error(this.generateError(err, true)).then(() => {
          this.loading = false;
        });
      });
    } else {
      this.swal.error('Required field is empty!');
    }

  }

  deletePart(scrub, partId, index) {
    this.swal.confirm('Are you sure to delete this item?').then((isConfirm) => {
      if (isConfirm.value) {
        if (partId) {
          this._scrubService.deletePart(scrub.id, partId).subscribe((response) => {
            scrub.parts.splice(index, 1);
            this.swal.success('Done!');
          }, err => {
            this.swal.error('Error!');
          });
        } else {
          scrub.parts.splice(index, 1);
          this.swal.success('Done!');
        }
      } else {
        this.swal.error('Canceled!');
      }
    });
  }

  checkForPartBinding(part, flag) {
    if (flag === 'invoice') {
      part.publisher.id = '';
    } else {
      part.invoiceNumber = '';
    }
  }

  setStatusFilter(obj?) {
    if (typeof (obj) === 'object') {
      this.params.status = obj.id;
      this.statusFilter = obj.name;
    } else {
      this.params.status = null;
      this.statusFilter = 'All';
    }
    this.filterOpen = false;
    this.loading = true;
    this.getScrubs(this.params)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.dataReadyForScrubs(response));
  }

  setStatusFilterOnInit() {
    this.scrubStatuses.forEach((status) => {
      if (status.id === this.params.status) {
        this.statusFilter = status.name;
      }
    });
  }

  changeStatus(item, id, idName, flag) {
    item[flag] = id;
    item[`${flag}Name`] = idName;
  }

  scrubDataManipulate(scrub) {
    scrub.parts.forEach((part) => {
      part.status = part.status.toString();
      part.availableTypesArr = [];
      this.partTypes.forEach((type) => {
        if (part.type === type.id) {
          part.typeName = type.name;
        } else {
          part.availableTypesArr.push(type);
        }
      });
    });
    scrub.availableStatusesArr = [];
    this.scrubStatuses.forEach((status) => {
      if (scrub.status === status.id) {
        scrub.statusName = status.name;
      } else {
        scrub.availableStatusesArr.push(status);
      }
    });
  }

  sendEmail(scrub) {
    this._scrubService.sendEmail(scrub.id).subscribe(() => {
      scrub.emailSent = true;
    }, (err) => {
      this.swal.error(err.errors);
    });
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  generateError(err, isTitle) {
    let newMsg = '';
    if (typeof err === 'string') {
      return err;
    }
    if (typeof err.errors === 'string') {
      return err.errors;
    }
    if (typeof err.message === 'string' && isTitle) {
      newMsg = `${err.message} \n`;
    }
    if (Array.isArray(err.errors) && !isTitle) {
      err.errors.map(error => {
        newMsg += `${error.message}: ${error.items.join()} \n`;
      });
    }
    if (typeof err.errors === 'object') {
      for (const prop in err.errors) {
        if (err.errors.hasOwnProperty(prop)) {
          newMsg += `${prop}: ${err.errors[prop]} \n`;
        }
      }
    }
    return newMsg;
  }
}
