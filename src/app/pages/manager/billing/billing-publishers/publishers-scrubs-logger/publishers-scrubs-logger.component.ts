import { Component, OnInit, ViewChild } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { DateFormattedService } from '@app/shared/services/date-formatted/date-formatted.service';
import { QueryParamsService } from '@app/shared/services/queryParams/query-params.service';
import * as moment from 'moment';
import { ScrubsService } from '../scrubs.service';

@Component({
  selector: 'app-publishers-scrubs-logger',
  templateUrl: './publishers-scrubs-logger.component.html',
  styleUrls: ['./publishers-scrubs-logger.component.scss']
})
export class PublishersScrubsLoggerComponent implements OnInit {

  @ViewChild('detailsModal') detailsModal: ModalComponent;
  params: any;
  loading: boolean;
  order: string;
  searchFilter: boolean;
  logs: any = {items: []};
  logs$: any;
  reasons: any;
  fields: any[];
  actions: any;
  scrollFlag: boolean;
  oldData: SafeHtml;
  newData: SafeHtml;


  constructor(private dateFormattedService: DateFormattedService,
              private _scrubsService: ScrubsService,
              private queryParamsService: QueryParamsService) {
  }

  ngOnInit() {
    this.searchFilter = false;
    this.loading = true;
    this.scrollFlag = false;
    this.order = 'ASC';
    this.params = this.queryParamsService.getParams();
    this.fields = [];
    if (!Object.keys(this.params).length) {
      this.params = {
        dateFrom: this.dateFormattedService.formatDateForBackend(moment().startOf('month')),
        dateTo: this.dateFormattedService.formatDateForBackend(moment())
      };
    }

    this.getLogs(this.params);
  }


  calendarCallback({startDate, endDate}) {
    this.loading = true;
    this.params.page = 1;
    this.params.dateFrom = startDate;
    this.params.dateTo = endDate;
    this.getLogs(this.params);
  }

  getLogs(params) {
    this._scrubsService.getLoggerList(params).subscribe(logs => {
        this.logs = logs;
        this.queryParamsService.setParams(params);
        this.loading = false;
      },
      (err) => err,
      () => {
        this.loading = false;
      });
  }

  toggleSearchFilter(flag?) {
    if (flag) {
      this.searchFilter = false;
    } else {
      this.searchFilter = !this.searchFilter;
    }
  }

  goToPage(page) {
    this.loading = true;
    this.params.page = page;
    this.getLogs(this.params);
  }

  tableLimit(number) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = number;
    this.getLogs(this.params);
  }

  sortByField(field) {
    this.params.sort = field;
    this.params.page = 1;
    this.params.order = this.order;
    this.order = this.order === 'ASC' ? 'DESC' : 'ASC';
    this.loading = true;
    this.getLogs(this.params);
  }

  resetFilters() {
    this.params.actionsIds = null;
    this.params.managersIds = null;
    this.params.advertisersIds = null;
    this.params.offersIds = null;
    this.params.isSystem = null;
    this.params.reasonsIds = null;
    this.params.fields = null;
    this.loading = true;
    this.params.page = 1;
    this.toggleSearchFilter('close');
    this.queryParamsService.setParams(this.params);
    this.getLogs(this.params);
  }

  applyFilters() {
    this.loading = true;
    this.params.page = 1;
    this.toggleSearchFilter('close');
    this.getLogs(this.params);
  }

  showDetailsPopup(action, log: any = {}) {
    if (action === 'open') {
      this.detailsModal.open();
    } else {
      this.detailsModal.close();
    }
    if (Object.keys(log).length) {
      this.oldData = log.data.oldValues;
      this.newData = log.data.newValues;
    }
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  generateFieldsOptions(fields) {
    const modifyObject = (obj) => {
      const keys = Object.keys(obj);
      return keys.reduce((result, key) => {
        if (typeof obj[key] === 'string') {
          result.push({id: key, text: obj[key]});
          return result;
        } else {
          const children = modifyObject(obj[key]);
          result.push({text: key.toUpperCase(), children});
          return result;
        }
      }, []);
    };
    return modifyObject(fields);
  }

}
