import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import * as moment from 'moment';
import { Moment } from 'moment';
import { DateFormattedService } from '../../../../../shared/services/date-formatted/date-formatted.service';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { ScrubsService } from '../scrubs.service';

@Component({
  selector: 'app-publishers-scrubs-manage',
  templateUrl: './publishers-scrubs-manage.component.html',
  styleUrls: ['./publishers-scrubs-manage.component.scss']
})
export class PublishersScrubsManageComponent implements OnInit {

  errors: any;
  maxDate: Moment;
  scrub: { month: string, offerId: string, status: string };
  scrubForm: FormGroup;
  submit: boolean;
  monthFormat = 'YYYY-MM';

  constructor(private dateFormattedService: DateFormattedService,
              private swal: SweetAlertService,
              private router: Router,
              private _scrubService: ScrubsService,
              private validatorService: ValidatorService,
              private _fb: FormBuilder) {

    this.maxDate = moment().subtract(1, 'months').endOf('month');

  }

  ngOnInit() {
    this.scrubForm = this._fb.group({
      month: [moment().subtract(1, 'months').endOf('month').format(this.monthFormat), Validators.required],
      offerId: [, Validators.required],
      status: [1, Validators.required]
    });
  }

  createScrub() {
    this.submit = true;
    if (this.scrubForm.valid) {
      this._scrubService.createScrub(this.scrubForm.value).subscribe((response) => {
        this.router.navigate(['/', 'manager', 'billing', 'publishers', 'scrubs', 'list']);
      }, (err) => {
        this.validatorService.addErrorToForm(this.scrubForm, err);
        this.swal.error(this.errorsToString(err.errors));
      });
    }
  }

  calendarCallback(date, key) {
    this.scrub[key] = date;
  }

  onChange(errName) {
    if (this.errors && this.errors.hasOwnProperty(errName)) {
      delete this.errors[errName];
    }
  }

  errorsToString(errObject) {
    let err = '';
    this.errors = errObject;
    if (typeof errObject !== 'string') {
      for (const prop in errObject) {
        if (errObject.hasOwnProperty(prop)) {
          err += errObject[prop].join();
          this.scrubForm.controls[prop].setErrors({'incorrect': true});
        }
      }
    } else {
      err = errObject;
    }
    return err;
  }

}
