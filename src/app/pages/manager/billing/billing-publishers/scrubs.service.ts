import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AsyncSubject, Observable } from 'rxjs';
import { BaseUrlService } from '../../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';

@Injectable({
  providedIn: 'root'
})
export class ScrubsService {
  actionNames$ = new AsyncSubject();

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
    this.getActionNames();
  }

  getActionNames() {
    this.http.get(`${this.url.getBaseUrlWithRole()}/billing_logger/scrub/actions`,
      {headers: {toCamelCase: 'true'}}).subscribe((response: any) => {
      this.actionNames$.next(response.items);
      this.actionNames$.complete();
    });
  }


  getLoggerList(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing_logger/scrub${params}`, {headers: {toCamelCase: 'true'}});
  }

  getScrubStatuses(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/scrub/status`, {headers: {toCamelCase: 'true'}});
  }

  getPartsTypes(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/part/type`, {headers: {toCamelCase: 'true'}});
  }

  getPartsStatuses(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/part/status`, {headers: {toCamelCase: 'true'}});
  }

  getScrubs(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_invoice/scrub${params}`, {headers: {toCamelCase: 'true'}});
  }

  updateScrub(id, data): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/publisher_invoice/scrub/${id}`, data, {headers: {toCamelCase: 'true'}});
  }

  updateParts(id, data): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/publisher_invoice/scrub/${id}/parts`, data, {headers: {toCamelCase: 'true'}});
  }

  deletePart(scrubId, partId): Observable<any> {
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/publisher_invoice/scrub/${scrubId}/part/${partId}`
      , {headers: {toCamelCase: 'true'}});
  }

  sendEmail(scrubId): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/publisher_invoice/scrub/${scrubId}/email`, {}
      , {headers: {toCamelCase: 'true'}});
  }

  createScrub(scrub): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/publisher_invoice/scrub`, scrub, {headers: {toCamelCase: 'true'}});
  }
}
