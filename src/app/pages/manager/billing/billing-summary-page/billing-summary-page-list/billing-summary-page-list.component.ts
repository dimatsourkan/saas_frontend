import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { QueryParamsService } from '../../../../../shared/services/queryParams/query-params.service';
import { BillingSummaryPageService } from '../billing-summary-page.service';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'app-billing-summary-page-list',
  templateUrl: './billing-summary-page-list.component.html',
  styleUrls: ['./billing-summary-page-list.component.scss']
})
export class BillingSummaryPageListComponent implements OnInit {
  params: any = {};
  propertyName: string;
  reverse: boolean;
  yearsList: any;
  statistics: any = {items: []};
  loading: boolean;

  constructor(
    private billingSummaryService: BillingSummaryPageService,
    private queryParamsService: QueryParamsService) {
  }

  ngOnInit() {
    this.loading = true;
    DataStore.currency.current.asObservable$
    .subscribe(currency => {
      this.params.currency = currency;

      if (this.yearsList) {
        this.getStatistics(this.params);
      }
    });
    this.getYears();
    this.propertyName = 'month';
    this.reverse = true;
  }

  getYears() {
    return this.billingSummaryService.getYears().subscribe(response => {
      if (response.length) {
        this.yearsList = response.map((val) => {
          return {val};
        });
        this.params.year = this.yearsList[this.yearsList.length - 1].val;
      }
      this.getStatistics(this.params);
    });
  }

  getStatistics(params?) {
    return this.billingSummaryService.getStatistics(params).subscribe((response) => {
      this.statistics = response;
      this.queryParamsService.setParams(this.params);
      this.loading = false;
    });
  }

  changeYear() {
    this.getStatistics(this.params);
  }

  sortBy(propertyName) {
    this.reverse = (this.propertyName === propertyName) ? !this.reverse : this.reverse;
    this.propertyName = propertyName;
  }

  formatMonth(month) {
    return moment.utc(month, 'M').format('MMMM');
  }
}
