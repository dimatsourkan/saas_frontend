import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingSummaryPageListComponent } from './billing-summary-page-list/billing-summary-page-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'billing-summary',
    pathMatch: 'full'
  },
  {
    path: 'billing-summary', component: BillingSummaryPageListComponent,
    data: {
      name: 'titles.bill.sum',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingSummaryPageRoutingModule {
}
