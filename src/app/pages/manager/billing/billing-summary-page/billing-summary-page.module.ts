import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { BillingSummaryPageListComponent } from './billing-summary-page-list/billing-summary-page-list.component';

import { BillingSummaryPageRoutingModule } from './billing-summary-page-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    BillingSummaryPageRoutingModule
  ],
  declarations: [BillingSummaryPageListComponent]
})
export class BillingSummaryPageModule {
}
