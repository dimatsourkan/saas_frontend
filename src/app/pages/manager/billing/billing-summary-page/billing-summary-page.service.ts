import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';

@Injectable({
  providedIn: 'root'
})
export class BillingSummaryPageService {


  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getYears(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/statistics/invoices/years`, {headers: {toCamelCase: 'true'}});
  }

  getStatistics(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/statistics/invoices${params}`, {headers: {toCamelCase: 'true'}});
  }
}
