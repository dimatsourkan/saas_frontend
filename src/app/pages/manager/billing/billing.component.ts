import { Component } from '@angular/core';

@Component({
  selector: 'app-billing',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class BillingComponent {
}
