import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BillingAdvertisersModule } from './billing-advertisers/billing-advertisers.module';
import { BillingPublishersModule } from './billing-publishers/billing-publishers.module';
import { BillingSummaryPageModule } from './billing-summary-page/billing-summary-page.module';
import { BillingComponent } from './billing.component';

import { BILING_ROUTING } from './billing.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BILING_ROUTING),
    BillingAdvertisersModule,
    BillingPublishersModule,
    BillingSummaryPageModule
  ],
  declarations: [BillingComponent]
})
export class BillingModule {
}
