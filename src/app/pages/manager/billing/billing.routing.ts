import { Routes } from '@angular/router';

export const BILING_ROUTING: Routes = [
  {
    path: 'advertisers', loadChildren: './billing-advertisers/billing-advertisers.module#BillingAdvertisersModule'
  },
  {
    path: 'publishers', loadChildren: './billing-publishers/billing-publishers.module#BillingPublishersModule'
  },
  {
    path: 'summary', loadChildren: './billing-summary-page/billing-summary-page.module#BillingSummaryPageModule'
  },
  {
    path: 'cross-payments', loadChildren: './billing-cross-payments/billing-cross-payments.module#BillingCrossPaymentsModule'
  }
];
