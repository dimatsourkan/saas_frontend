import { Component, OnInit } from '@angular/core';
import { DashboardIncomes, DashboardOffers, DashboardRevenues } from './dashboard';
import { DashboardService } from './dashboard.service';
import { DataStore } from '@app/core/entities/data-store.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  currency: string;
  incomes: DashboardIncomes;
  revenues: DashboardRevenues;
  offers: DashboardOffers;
  loading = false;
  nextPageExists: any;
  superlinkPublisherIncomes: DashboardIncomes;
  superlinkSystemProfit: DashboardIncomes;


  constructor(private dashboardService: DashboardService) {
  }

  ngOnInit() {
    DataStore.currency.current.asObservable$
    .subscribe((currency) => {
      this.currency = currency;
      this.loading = true;
      this.getOffers();
      this.getIncomes(currency);
      this.getRevenues(currency);
      this.getSuperlinkPublisherIncomes(currency);
      this.getSuperlinkSystemIncomes(currency);
    });

  }

  getOffers() {
    this.dashboardService.getOffers().subscribe(res => {
      this.loading = false;
      this.offers = res;
    });
  }

  getIncomes(currency) {
    this.dashboardService.getIncomes(currency).subscribe(res => {
      this.loading = false;
      this.incomes = res;
    });
  }

  getRevenues(currency) {
    this.dashboardService.getRevenues(currency).subscribe(res => {
      this.loading = false;
      this.revenues = res;
    });
  }

  getSuperlinkPublisherIncomes(currency) {
    this.dashboardService.getSuperlinkPublisherIncomes(currency).subscribe(res => {
      this.loading = false;
      this.superlinkPublisherIncomes = res;
    });
  }

  getSuperlinkSystemIncomes(currency) {
    this.dashboardService.getSuperlinkSystemIncomes(currency).subscribe(res => {
      this.loading = false;
      this.superlinkSystemProfit = res;
    });
  }
}
