import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { DASHBOARD_ROUTING } from './dashboard.routing';
import { DashboardService } from './dashboard.service';
import { PayoutsComponent } from './payouts/payouts.component';
import { TopRankedComponent } from './top-ranked/top-ranked.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild(DASHBOARD_ROUTING)
  ],
  providers: [DashboardService],
  declarations: [DashboardComponent, TopRankedComponent, PayoutsComponent]
})
export class DashboardModule {
}
