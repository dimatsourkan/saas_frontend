import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

export const DASHBOARD_ROUTING: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data: {
      name: 'titles.dashboard',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }
];
