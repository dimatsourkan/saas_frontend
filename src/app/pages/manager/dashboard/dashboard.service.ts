import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseUrlService } from '@app/shared/services/base-url/base-url.service';
import * as moment from 'moment';
import { Moment } from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DashboardIncomes, DashboardOffers, DashboardPeriods, DashboardRevenues } from './dashboard';

export class IncomeChartItem {
  amount: number;
  currency: string;
  momentDate: Moment;

  constructor(data: any) {
    this.momentDate = moment(data.date);
    this.currency = data.currency;
    this.amount = data.amount;
  }

  get date() {
    return moment(this.momentDate.format()).format('DD MMM');
  }
}

@Injectable()
export class DashboardService {

  constructor(private http: HttpClient,
              private url: BaseUrlService) {
  }

  getIncomes(currency): Observable<DashboardIncomes> {
    const params = new HttpParams({fromObject: {currency}});
    return this.http.get<DashboardIncomes>(`${this.url.getBaseUrlWithRole()}/dashboard/incomes`, {
      params: params,
      headers: {toCamelCase: 'true'}
    });
  }

  getSuperlinkPublisherIncomes(currency): Observable<DashboardIncomes> {
    const params = new HttpParams({fromObject: {currency}});
    return this.http.get<DashboardIncomes>(`${this.url.getBaseUrlWithRole()}/dashboard/superlink_incomes`, {
      params: params,
      headers: {toCamelCase: 'true'}
    });
  }

  getSuperlinkSystemIncomes(currency): Observable<DashboardIncomes> {
    const params = new HttpParams({fromObject: {currency}});
    return this.http.get<DashboardIncomes>(`${this.url.getBaseUrlWithRole()}/dashboard/superlink_system_incomes`, {
      params: params,
      headers: {toCamelCase: 'true'}
    });
  }

  getPeriods(): Observable<DashboardPeriods> {
    return this.http.get<DashboardPeriods>(`${this.url.getBaseUrlWithRole()}/dashboard/periods`, {
      headers: {toCamelCase: 'true'}
    });
  }

  getRevenues(currency): Observable<DashboardRevenues> {
    const params = new HttpParams({fromObject: {currency}});
    return this.http.get<DashboardRevenues>(`${this.url.getBaseUrlWithRole()}/dashboard/revenues`, {
      params: params,
      headers: {toCamelCase: 'true'}
    });
  }

  getOffers(): Observable<DashboardOffers> {
    return this.http.get<DashboardOffers>(`${this.url.getBaseUrlWithRole()}/dashboard/offers`, {
      headers: {toCamelCase: 'true'}
    });
  }

  getIncomesChart(currency): Observable<IncomeChartItem[]> {
    const params = new HttpParams({fromObject: {currency}});
    return this.http.get<IncomeChartItem[]>(`${this.url.getBaseUrlWithRole()}/dashboard/incomes_chart`, {
      params: params, headers: {toCamelCase: 'true'}
    })
      .pipe(map(res => res.map(i => new IncomeChartItem(i))))
      .pipe(map(res => res.sort((a, b) => a.momentDate.isAfter(b.momentDate) ? 1 : -1)));
  }

  getTopRankedItems(currency, page, period, item): Observable<any> {
    const params = new HttpParams({fromObject: {currency, page, period}});
    return this.http.get(`${this.url.getBaseUrlWithRole()}/dashboard/top_ranked_${item}`, {
      params: params,
      headers: {toCamelCase: 'true'}
    });
  }

}
