export interface DashboardIncomes {
  today: LabelData;
  yesterday: LabelData;
  lastWeek: LabelData;
  thisMonth: LabelData;
  lastMonth: LabelData;
  allTime: LabelData;
}

export interface DashboardRevenues {
  today: LabelData;
  yesterday: LabelData;
  lastWeek: LabelData;
  thisMonth: LabelData;
  lastMonth: LabelData;
  allTime: LabelData;
}

export interface DashboardPeriods {
  [key: number]: string;
}

export interface DashboardOffers {
  active: number;
  incent: number;
  nonIncent: number;
  perDay: number;
  private: number;
  suspended: number;
  total: number;
}

export interface DashboardIncomeChart {
  date: string;
  amount: number;
}

interface LabelData {
  currency: string;
  amount: number;
}
