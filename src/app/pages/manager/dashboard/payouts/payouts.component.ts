import { Component, OnInit } from '@angular/core';
import { LinearChartDataset } from '@app/shared/components/charts/linear-chart/linear-chart-dataset.model';
import { LanguageService } from '@app/shared/services/language/language.service';
import { DataStore } from '@app/core/entities/data-store.service';
import { ChartTooltipItem } from 'chart.js';
import { DashboardService, IncomeChartItem } from '../dashboard.service';

@Component({
  selector: 'app-payouts',
  templateUrl: './payouts.component.html',
  styleUrls: ['./payouts.component.scss']
})
export class PayoutsComponent implements OnInit {

  incomesChart: IncomeChartItem[] = [];
  chartData = new LinearChartDataset<IncomeChartItem>();
  chartLabels: string[] = [];
  currency: string;

  constructor(
    private dashboardService: DashboardService,
    private languageService: LanguageService
  ) {

  }

  ngOnInit() {

    this.languageService.getLang().subscribe(() => {
      this.setChartData();
    });

    DataStore.currency.current.asObservable$
    .subscribe((currency) => {
      this.currency = currency;
      this.getIncomesChart();
    });
  }

  getIncomesChart() {
    this.dashboardService.getIncomesChart(this.currency).subscribe(incomesChart => {
      this.incomesChart = incomesChart;
      this.setChartData();
    });
  }

  setChartData() {
    this.chartData.fullData = this.incomesChart;
    this.chartData.data = this.incomesChart.map(d => d.amount);
    this.chartLabels = this.incomesChart.map(d => d.date);
  }

  labelTransformCallback(item: ChartTooltipItem, data: { datasets: LinearChartDataset<IncomeChartItem> }) {
    const index = item.index;
    const dataseIndex = item.datasetIndex;
    const fullData: IncomeChartItem = data.datasets[dataseIndex].fullData[index];
    return [
      `${fullData.amount} ${fullData.currency}`
    ];
  }

}
