import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { finalize } from 'rxjs/operators';
import { DashboardPeriods } from '../dashboard';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-top-ranked',
  templateUrl: './top-ranked.component.html',
  styleUrls: ['./top-ranked.component.scss']
})
export class TopRankedComponent implements OnInit {

  @ViewChild('cardBody') cardBody: ElementRef;

  objectKeys = Object.keys;
  currency: string;
  topRankedItems: any = {advertisers: [], publishers: [], offers: []};
  selectedPeriod = '1';
  topRankedInfoPage = 1;
  nextPageExists: any;
  rankedActive = 'advertisers';
  periods: DashboardPeriods;
  loading = false;

  constructor(private dashboardService: DashboardService) {
  }

  ngOnInit() {
    DataStore.currency.current.asObservable$
    .subscribe((currency) => {
      this.currency = currency;
      this.changeTopRankedInfo(0);
      this.getPeriods();
    });

  }

  getPeriods() {
    this.dashboardService.getPeriods().subscribe((periods) => this.periods = periods);
  }

  changeTopRankedInfo(page) {
    this.topRankedInfoPage = ++page;
    this.loading = true;
    this.dashboardService.getTopRankedItems(this.currency, page, this.selectedPeriod, this.rankedActive)
    .pipe(finalize(() => this.loading = false))
    .subscribe(response => {
      this.nextPageExists = response.nextPageExists;
      this.topRankedItems[this.rankedActive] = [...this.topRankedItems[this.rankedActive], ...response.items];
      this.topRankedItems[this.rankedActive] = this.removeDuplicates(this.topRankedItems[this.rankedActive], 'id');
    });
  }

  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

  showMoreTopRankedItems() {
    this.changeTopRankedInfo(this.topRankedInfoPage);
  }

  setFilterPeriod() {
    this.topRankedItems = {advertisers: [], publishers: [], offers: []};
    this.changeTopRankedInfo(0);
  }

}
