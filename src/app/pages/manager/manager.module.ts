import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

import { MANAGER_ROUTES } from './manager.routing';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild(MANAGER_ROUTES),
  ],
  providers: [],
  declarations: []
})
export class ManagerModule {
}
