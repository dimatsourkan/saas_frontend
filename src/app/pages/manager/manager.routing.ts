import { Routes } from '@angular/router';

export const MANAGER_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'managers',
    loadChildren: './users-managers/users-managers.module#UsersManagersModule',
    data: {
      name: 'UsersManagersModule'
    }
  },
  {
    path: 'profile',
    loadChildren: './profile/profile.module#ProfileModule',
    data: {
      name: 'titles.profile'
    }
  },
  {
    path: 'advertisers',
    loadChildren: './users-advertisers/users-advertisers.module#UsersAdvertisersModule',
    data: {
      name: 'UsersAdvertisersModule'
    }
  },
  {
    path: 'publishers',
    loadChildren: './users-publishers/users-publishers.module#UsersPublishersModule',
    data: {
      name: 'UsersPublishersModule'
    }
  },
  {
    path: 'offers',
    loadChildren: './offers/offers.module#OffersModule',
  },
  {
    path: 'reports',
    loadChildren: './reports/reports.module#ReportsModule',
    data: {
      name: 'Reports'
    }
  },
  {
    path: 'billing',
    loadChildren: './billing/billing.module#BillingModule',
  },
  {
    path: 'superlink',
    loadChildren: './superlink/superlink.module#SuperlinkModule',
  },
  {
    path: 'settings',
    loadChildren: './settings/settings.module#SettingsModule',
  }
];
