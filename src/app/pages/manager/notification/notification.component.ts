import { Component, OnInit } from '@angular/core';
import { NotificationService } from './notification.service';
import { timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  notificationCount: any;
  notificationIsShown: any;
  noMoreNotifications: boolean;
  notifications: any = { items: [] };
  notificationLoading: boolean;

  constructor(private _notificationService: NotificationService) {
  }

  ngOnInit() {
    this.notificationIsShown = false;
    this.noMoreNotifications = false;
    this.notificationLoading = true;
    this.getNotificationCount();
    this.getNotification(1);
  }

  getNotificationCount() {
    timer(0, 60000).pipe(
      switchMap(() => this._notificationService.getNotificationCount())
    ).subscribe(result => this.notificationCount = result);
  }

  getNotification(page) {
    this._notificationService.getNotification({
      page,
      limit: 10
    }).subscribe((response) => {
      if (page !== 1 && Object.prototype.hasOwnProperty.call(this.notifications, 'items') && this.notifications.items.length) {
        const responseData = response;
        this.notifications.items = this.notifications.items.concat(responseData.items);
        this.notifications.page = responseData.page;
      } else {
        this.notifications = response;
      }

      this.notifications.items.forEach((item) => {
        if (item.category.name === 'checker' && /{{offerId}}/.test(item.message.name)) {
          item.message.name = `${item.payload.offerId} ${item.message.name.split('{{offerId}}')[1]}`;
        }
      });

      if (this.notifications.page === this.notifications.totalPages) {
        this.noMoreNotifications = true;
      }
      this.notificationLoading = false;
    });

  }

  showNotification() {
    if (!this.notificationIsShown) {
      this.notificationIsShown = true;
      this.noMoreNotifications = false;
    } else {
      this.closeNotification({
        close: true
      });
    }
  }

  closeNotification({
    close
  }) {
    if (close && this.notificationIsShown) {
      this.notificationIsShown = false;
      const ids = this.notifications.items.reduce((arr, item) => {
        if (item.read === false) {
          arr.push(item.id);
        }
        return arr;
      }, []);
      if (ids.length !== 0) {
        this._notificationService.closeNotification(ids).subscribe((response) => {
          if (response.status === 200) {
            this.getNotificationCount();
          }
        });
      }
      this.getNotification(1);
    }
  }

  getMoreNotifications() {
    if (Object.prototype.hasOwnProperty.call(this.notifications, 'page') && this.notifications.page !== this.notifications.totalPages) {
      this.getNotification(this.notifications.page + 1);
    }
  }

}
