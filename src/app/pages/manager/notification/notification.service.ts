import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../shared/services/queryParams/query-params.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getNotificationCount(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/ringer/count`, {headers: {toCamelCase: 'true'}});
  }

  getNotification(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/ringer${params}`, {headers: {toCamelCase: 'true'}});
  }

  closeNotification(ids): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/ringer/set_read`, {ids}, {headers: {toCamelCase: 'true'}});
  }
}
