import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';

@Injectable({
  providedIn: 'root'
})
export class ApprovalsService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getOfferApprovalsStatuses() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_approval/statuses`);
  }

  getFilterList(list: string, page: number): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/${list}?page=${page}&active=true&context=keylist`);
  }

  getOfferApprovals(queryParams: any): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_approval${params}`, {headers: {toCamelCase: 'true'}});
  }

  deleteOffer(id: any): Observable<any> {
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/offer_approval/${id}`, {headers: {toCamelCase: 'true'}});
  }

  unApproveOffer(id: any): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/offer_approval/${id}/unapprove`, null, {headers: {toCamelCase: 'true'}});
  }

  OfferApprovalsApprove(data: any): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/offer_approval/approve`, data, {headers: {toCamelCase: 'true'}});
  }

  OfferApprovalsDisapprove(data: any): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/offer_approval/disapprove`, data, {headers: {toCamelCase: 'true'}});
  }

  getOfferInfo(id: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/info/${id}`, {headers: {toCamelCase: 'true'}});
  }

  getGoals(offerID: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_approval/${offerID}/goals`, {headers: {toCamelCase: 'true'}});
  }

  getOfferStatuses(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/statuses`, {headers: {toCamelCase: 'true'}});
  }
}
