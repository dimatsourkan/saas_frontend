import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Params } from '@angular/router';
import * as moment from 'moment';

import { forkJoin } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';

import { Countries } from '../../../../../shared/interfaces/countries';
import { CountriesService } from '../../../../../shared/services/countries/countries.service';
import { DateFormattedService } from '../../../../../shared/services/date-formatted/date-formatted.service';
import { QueryParamsService } from '../../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { ApprovalsService } from '../approvals.service';

export interface Filter {
  publisherIds: string[];
  publisherManagerIds: string[];
  advertiserIds: string[];
  advertiserManagerIds: string[];
  offerIds: string[];
  statuses: string[];
  search: string;
  page: string;
  limit: string;
  sort: string;
  order: string;
}

export interface FilterResponse {
  loading: boolean;
  items: any[];
  limit: number;
  page: number;
  total_items: number;
  total_pages: number;
}

export interface Offers {
  id: number;
  sensitive: string;
  tags: any[];
  statusName: string;
  restrictions: any[];
  realId: string;
  name: string;
  thumbUrl: string;
  statusOrigin: string;
  description: string;
  activationDate: any;
  expirationDate: any;
  updatedAt: any;
  comment: string;
  approvalsQuantity: string;
  approvalRequired: boolean;
  isChecked: boolean;
  goals: any[];
  allowedTraffic: any[];
  restrictedTraffic: any[];
  checkResults: any;
  status: string;
  publisher: any;
  offer: Offers;
  offerGoalPublishers: any[];
  advertiser: {
    id: string;
    username: string;
    manager: {
      id: string;
      username: string;
    }
  };
}

export interface OfferInfo {
  publisherResume: any;
  links: {
    advertiserLinks: any[];
    publisherLink: string;
    testLink: string;
  };

  offer: Offers;
  resume: {
    name: string;
    description: string;
    previewUrl: string;
    allowedTraffic: string;
    restrictedTraffic: string;
    thumbnail: string;
    creatives: any[];
    setupResumeTrafficUrl: string;
    trackingUrl: string;
    goals: any[];
    setupTrafficString: string;
  };
}

@Component({
  selector: 'app-approvals',
  templateUrl: './approvals.component.html',
  styleUrls: ['./approvals.component.scss']
})
export class ApprovalsComponent implements OnInit, OnDestroy {

  @ViewChild('offerInfoModal') offerInfoModal: ModalComponent;
  @ViewChild('approveModal') approveModal: ModalComponent;
  @ViewChild('perfectScrollbarContainer') perfectScrollbarContainer: ElementRef;
  destroy$: Subject<boolean> = new Subject<boolean>();
  loading: boolean;
  filterIsVisible: boolean;
  allOffersChecked: boolean;
  params: Filter = <Filter>{};
  queryParams: Params;
  countries: Array<Countries> = [];
  offerApprovals: { items: Offers[], page: any, totalPages: number, limit: any } = {
    items: [],
    page: 0,
    totalPages: 0,
    limit: 0
  };
  aff_id: any;
  aff_sub: any;
  aff_sub2: any;
  aff_sub3: any;
  aff_sub4: any;
  sensitives = [
    { key: 1, label: 'Sensitive' },
    { key: 0, label: 'Not Sensitive' }
  ];
  selectedOffers: number[] = [];
  scrollFlag: boolean;
  order: string;
  search: string;
  offerInfoPubResumePublisherId: string;
  offerApprovalStatuses: any;
  timezone: string;
  oferName: string;
  approvalID: any;
  offerGoals: { goals: any[] } = { goals: [] };
  offerInfo: OfferInfo = <OfferInfo>{};
  offerStatuses: any;
  dateFormat: any;
  parent: any;
  tooltipTop: boolean;
  filterForm: FormGroup;
  $publishers: FilterResponse = {
    loading: false,
    items: [],
    limit: null,
    page: 1,
    total_items: null,
    total_pages: null
  };

  $managers: FilterResponse = {
    loading: false,
    items: [],
    limit: null,
    page: 1,
    total_items: null,
    total_pages: null
  };

  $advertisers: FilterResponse = {
    loading: false,
    items: [],
    limit: null,
    page: 1,
    total_items: null,
    total_pages: null
  };
  $offersKey_list: FilterResponse = {
    loading: false,
    items: [],
    limit: null,
    page: 1,
    total_items: null,
    total_pages: null
  };
  errors: any;


  constructor(private fb: FormBuilder,
    private _approvalsService: ApprovalsService,
    private dateFormattedService: DateFormattedService,
    private queryParamsService: QueryParamsService,
    private countriesService: CountriesService,
    private swal: SweetAlertService) {

  }

  ngOnInit() {
    this.scrollFlag = false;
    this.order = 'ASC';
    this.filterIsVisible = false;
    this.selectedOffers = [];
    this.initSearchFilter(this.queryParamsService.getParams());
    this.countriesService.getCountries().subscribe(res => {
      this.countries = res;
      if (res.length) {
        this.callDataReady();
      }
    });
    this.loading = true;
    this.getOfferStatuses();
    this.dateFormat = moment.localeData().longDateFormat('L');
  }

  callDataReady() {
    forkJoin([this.getOfferApprovals(this.filterForm.value), this.getOfferApprovalsStatuses()])
      .subscribe(response => {
        this.dataReady(response[0], response[1]);
      });
  }

  toFirstPage() {
    this.filterForm.patchValue({
      page: '1'
    });
  }

  getFilterList(list: string) {
    let filter;
    if (list === 'offers/key_list') {
      filter = this.$offersKey_list;
    } else {
      filter = this[`$${list}`];
    }
    if (!filter.total_pages || filter.page < filter.total_pages) {
      filter.loading = true;
      this._approvalsService.getFilterList(list, filter.page + 1).subscribe((res) => {
        if (res) {
          (filter.page === 1) ? filter.items = res.items : filter.items = filter.items.concat(res.items);
          filter.page = res.page;
          filter.total_pages = res.total_pages;
          filter.loading = !filter.loading;
          if (list === 'offers/key_list') {
            filter = this.$offersKey_list;
          } else {
            filter = this[`$${list}`];
          }
        }
      });
    }
  }

  toggleFilterVisibility(action?: string) {
    this.filterIsVisible = action === 'show';
  }

  dataReady(offers, statuses?) {
    this.loading = false;
    this.scrollFlag = !this.scrollFlag;
    this.allOffersChecked = false;
    if (statuses) {
      this.offerApprovalStatuses = statuses.statuses;
    }
    this.offerApprovals = offers;
    this.timezone = this.dateFormattedService.getTimeZone();
    this.offerApprovals.items.forEach((offer) => {
      this.offerApprovalStatuses.forEach((status) => {
        if (offer.status === status.id) {
          offer.statusName = status.name;
        }
      });
      this.applyCountryNames(offer);
    });


    this.params.page = this.offerApprovals.page;
    this.params.limit = this.offerApprovals.limit;

  }

  goToPage(page) {
    this.loading = true;
    this.filterForm.patchValue({
      page
    });
    this.getOfferApprovals(this.filterForm.value).subscribe((response => {
      this.dataReady(response);
    }));
  }

  getOfferStatuses() {
    this._approvalsService.getOfferStatuses().subscribe((response) => {
      this.offerStatuses = response;
    });
  }

  changeOffersLink(flag) {
    const link = this.offerInfo.links.publisherLink.split('?');
    const params = this.getParams(link[1]);
    params[flag] = this[flag];
    this.offerInfo.links.publisherLink = `${link[0]}?${this.serialize(params)}`;
  }

  serialize(obj: any): URLSearchParams {
    const params: URLSearchParams = new URLSearchParams();

    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const element = obj[key];
        params.set(key, element);
      }
    }
    return params;
  }

  checkingOffer(flag, id) {
    if (!flag) {
      this.unCheckingOffer(id);
      return false;
    }
    this.selectedOffers.push(id);
  }

  tooltipPosition(e) {
    const pageHeight = this.perfectScrollbarContainer.nativeElement.clientHeight;
    const clickOffsetTop = e.pageY;
    pageHeight / 2 > clickOffsetTop - 220 ? this.tooltipTop = false : this.tooltipTop = true;
  }

  getOfferApprovalsStatuses() {
    return this._approvalsService.getOfferApprovalsStatuses();
  }

  getOfferApprovals(params?) {
    this.queryParamsService.setParams(params);
    return this._approvalsService.getOfferApprovals(params);
  }

  setupPublisherResumeInOfferInfo(publisher?) {
    if (!publisher) {
      this.offerInfo.publisherResume = `
    ${this.offerInfo.resume.name}
    ===========================================================
    ${this.offerInfo.resume.description}
    ===========================================================
    Preview URL:  ${this.offerInfo.resume.previewUrl}
    Tracking URL: ${this.setupResumeTrafficUrl('PUBLISHER_ID')}

    Allowed traffic: ${this.setupTrafficString(this.offerInfo.resume.allowedTraffic)}
    Restricted traffic: ${this.setupTrafficString(this.offerInfo.resume.restrictedTraffic)}

    Thumbnail: ${this.offerInfo.resume.thumbnail}
    Creatives:
    ${this.offerInfo.resume.creatives.join(', \n')}
        `;
    } else {
      this.offerInfo.publisherResume = `
    ${this.offerInfo.resume.name}
    ===========================================================
    ${this.offerInfo.resume.description}
    ===========================================================
    Preview URL:  ${this.offerInfo.resume.previewUrl}
    Tracking URL: ${this.setupResumeTrafficUrl(publisher)}

    Goals:
    ${this.setupGoalsString(this.offerInfo.resume.goals)}
    Allowed traffic: ${this.setupTrafficString(this.offerInfo.resume.allowedTraffic)}
    Restricted traffic: ${this.setupTrafficString(this.offerInfo.resume.restrictedTraffic)}

    Thumbnail: ${this.offerInfo.resume.thumbnail}
    Creatives:
    ${this.offerInfo.resume.creatives.join(', \n')}
        `;
    }
  }

  setupResumeTrafficUrl(param) {
    const link = this.offerInfo.resume.trackingUrl.split('?');
    const params = this.getParams(link[1]);
    params['aff_id'] = param;
    return `${link[0]}?${this.serialize(params)}`;
  }

  getParams(query) {
    if (!query) {
      return {};
    }
    return (/^[?]/.test(query) ? query.slice(1) : query).split('&').reduce((params, param) => {
      const [key, value] = param.split('=');
      if (value) {
        try {
          params[key] = decodeURIComponent(value.replace(/\+/g, ' '));
        } catch (e) {
          params[key] = value;
        }
      } else {
        params[key] = '';
      }
      return params;
    }, {});
  }

  setupTrafficString(traffic) {
    const keys = Object.keys(traffic);
    if (!keys.length) {
      return '';
    }
    return keys.reduce((string, key, index) => {
      if (index === keys.length - 1) {
        return string += `${traffic[key].name}`;
      } else {
        return string += `${traffic[key].name}, `;
      }
    }, '');
  }

  setupGoalsString(goals) {
    return goals.reduce((string, goal) => {
      return string += `
Name: ${goal.name}
Payout: ${this.calculateFloat(goal.payout.amount / 100 * goal.payPercent)} ${goal.dynamicPayout ? '%' : goal.payout.currency}
Daily Cap: ${goal.cap.daily}
Platforms: ${(() => goal.platforms.map(platform => platform.platform).join(', '))()}
Countries: ${goal.countries.join(', ')}

      `;
    }, '');
  }

  getOfferInfo(id) {
    this.offerInfoModal.close();
    this._approvalsService.getOfferInfo(id).subscribe((response) => {
      this.offerInfo = response;
      this.offerInfo.offer.restrictedTraffic = [];
      this.offerInfo.offer.allowedTraffic = [];
      this.offerStatuses.forEach((status) => {
        if (status.id === this.offerInfo.offer.status) {
          this.offerInfo.offer.statusName = status.name;
        }
      });
      this.offerInfo.offer.goals.forEach((goal) => {
        goal.pubPayout = this.calculateFloat(goal.payout.amount / 100 * goal.payPercent);
      });
      this.offerInfo.offer.restrictions.forEach((obj) => {
        if (obj.state === -1) {
          this.offerInfo.offer.restrictedTraffic.push(obj);
        } else if (obj.state === 1) {
          this.offerInfo.offer.allowedTraffic.push(obj);
        }
      });
      this.applyCountryNames(this.offerInfo);
      this.setupPublisherResumeInOfferInfo();
      this.offerInfo.offer.goals = this.offerInfo.offer.goals.filter(item => item.enabled);
      this.offerInfoModal.open();
    });
  }

  applyCountryNames(offer) {
    if (offer.countries) {
      offer.countries = offer.countries.map((el) => this.countries.find(item => item.code === el));
    }
  }

  declineOffer(selectId) {
    this._approvalsService.OfferApprovalsDisapprove({ ids: [selectId] }).subscribe((res) => {
      this.offerApprovals.items.forEach((offer) => {
        if (offer.id === selectId) {
          this.offerApprovalStatuses.forEach((status) => {
            if (status.original === 'disapproved') {
              offer.statusName = status.name;
              offer.statusOrigin = status.original;
            }
          });
        }
      });
      this.allOffersChecked = false;
      this.checkingAllOffer();
    });
  }

  deleteOffer(id, index) {
    this.swal.confirm('Are you sure to delete this item?').then(isConfirmed => {
      if (isConfirmed.value) {
        this._approvalsService.deleteOffer(id).subscribe((res) => {
          this.offerApprovals.items.splice(index, 1);
          this.swal.success('Done');
        });
      } else {
        this.swal.error('Canceled');
      }
    });

  }

  unApproveOffer(id) {
    this._approvalsService.unApproveOffer(id).subscribe((res) => {
      this.offerApprovals.items.forEach((offer) => {
        if (offer.id === id) {
          offer.statusName = 'Pending';
        }
      });
    }, err => {
      this.errors = err.errors;
      this.swal.error(this.generateError(err, true), this.generateError(err, false));
    });
  }

  getGoals(approvalID, offerID, offerName) {
    this.oferName = offerName;
    this.approvalID = approvalID;
    this._approvalsService.getGoals(offerID).subscribe((res) => {
      this.offerGoals = res;
      this.offerGoals.goals.forEach((goal) => {
        goal.enabled = true;
        goal.capClick.dailyForView = goal.capClick.daily;
        goal.capClick.monthlyForView = goal.capClick.monthly;
        goal.cap.dailyForView = goal.cap.daily;
        goal.price = this.calculateFloat(goal.payout.amount / 100 * goal.payPercent);
      });
      this.approveModal.open();
    });
  }

  changeGoals(goal, flag) {
    if (flag === 'price') {
      if (goal.price) {
        goal.payPercent = Math.round(goal.price * 100 / goal.payout.amount);
      } else {
        goal.payPercent = 0;
      }
    } else if (flag === 'percent') {
      if (goal.payPercent === null || goal.payPercent === 0) {
        goal.price = this.calculateFloat(goal.payout.amount / 100 * goal.payPercent);
      } else {
        goal.price = this.calculateFloat(goal.payout.amount / 100 * goal.payPercent);
      }
    }
  }

  calculateFloat(number: number, roundTo?: number) {
    if (typeof number === 'number') {
      const newNumber = Math.floor(number * 1000) / 1000;
      if (this.countDecimals(newNumber) > 2) {
        const numberInString: string = newNumber.toString();
        return Number(numberInString.substring(0, numberInString.length - 1));
      } else {
        return newNumber;
      }
    }
  }


  countDecimals(number) {
    if (!number) {
      return 0;
    }
    if (Math.floor(number.valueOf()) === number.valueOf()) {
      return 0;
    }
    return number.toString().split('.')[1].length || 0;
  }

  applyFilter(search?: string) {
    this.loading = true;
    this.toFirstPage();
    this.selectedOffers = [];
    this.filterForm.patchValue({ search });
    this.getOfferApprovals(this.filterForm.value).subscribe((response => {
      this.dataReady(response);
    }));
    this.toggleFilterVisibility();
  }

  resetFilters() {
    this.loading = true;
    this.params = <Filter>{};
    this.filterForm.reset();
    this.filterForm.patchValue({
      limit: '10'
    });
    this.getOfferApprovals().subscribe((response => {
      this.dataReady(response);
    }));

    this.toggleFilterVisibility();
    this.selectedOffers = [];
  }

  resetFilter(filter) {
    this.filterForm.controls[filter].reset();
  }

  massApprove() {
    const data = {
      ids: this.selectedOffers,
      goals: []
    };
    this._approvalsService.OfferApprovalsApprove(data).subscribe((res => {
      this.offerApprovals.items.forEach((offer) => {
        this.selectedOffers.forEach((id) => {
          if (offer.id === id) {
            offer.statusName = 'Approve';
          }
        });
      });
      this.allOffersChecked = false;
      this.checkingAllOffer();
    }), err => {
      this.swal.error(this.generateError(err, true), this.generateError(err, false));
    });
  }

  massDecline() {
    const data = {
      ids: this.selectedOffers
    };
    this._approvalsService.OfferApprovalsDisapprove(data).subscribe((res) => {
      this.offerApprovals.items.forEach((offer) => {
        this.selectedOffers.forEach((id) => {
          if (offer.id === id) {
            offer.statusName = 'Disapproved';
          }
        });
      });
      this.allOffersChecked = false;
      this.checkingAllOffer();
    });
  }

  approveOffer() {
    const data = {
      ids: [this.approvalID],
      goals: []
    };
    this.offerGoals.goals.forEach(
      (goal) => {
        data.goals.push({
          id: goal.id,
          capDaily: goal.cap.daily,
          capMonthly: 0,
          capClickDaily: goal.capClick.daily,
          capClickMonthly: goal.capClick.monthly || 0,
          payPercent: goal.payPercent || 0,
          enabled: goal.enabled ? 1 : 0
        });
      });
    this._approvalsService.OfferApprovalsApprove(data).subscribe((res => {
      this.swal.success('Done!');
      this.errors = null;
      this.offerApprovals.items.forEach((offer) => {
        if (offer.id === this.approvalID) {
          this.offerApprovalStatuses.forEach((status) => {
            if (status.name === 'Approve') {
              offer.status = status.id;
              offer.statusName = status.name;
            }
          });
        }
      });
      this.approvalID = '';
      this.toggleApproveModal();
    }), err => {
      this.errors = err.errors;
      if (this.generateError(err, true).length > 0) {
        this.swal.error(this.generateError(err, true), this.generateError(err, false));
      }
    });
  }

  checkingAllOffer() {
    this.offerApprovals.items.forEach((offer) => {
      offer.isChecked = this.allOffersChecked;
      if (this.allOffersChecked) {
        this.selectedOffers.push(offer.id);
      } else {
        this.unCheckingOffer(offer.id);
      }
    });
  }

  unCheckingOffer(id) {
    let offerIndex;
    this.selectedOffers.forEach((offId, index) => {
      if (offId === id) {
        offerIndex = index;
      }
    });
    this.selectedOffers.splice(offerIndex, 1);
  }

  sortByField(sort) {
    this.filterForm.patchValue({
      sort,
      order: this.filterForm.value.order === 'ASC' ? 'DESC' : 'ASC'
    });
    this.toFirstPage();
    this.loading = true;
    this.getOfferApprovals(this.filterForm.value).subscribe((response => {
      this.dataReady(response);
    }));
  }

  filterByField(field, id, text) {
    this.filterForm.patchValue({
      [field]: (this.filterForm.value[field]) ? [...this.filterForm.value[field], id] : [id]
    });
    this.applyFilter();
  }

  tableLimit(limit) {
    this.loading = true;
    this.filterForm.patchValue({
      limit
    });
    this.toFirstPage();
    this.getOfferApprovals(this.filterForm.value).subscribe((response => {
      this.dataReady(response);
    }));
  }

  formattingDate(date) {
    return moment(date).format(this.dateFormat.L);
  }

  formattingDateWithTime(date) {
    if (!date) {
      return '';
    }
    return moment(date).format(this.dateFormat.LLLL);
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  toggleModal() {
    this.offerInfoModal.toggle();
  }

  toggleApproveModal() {
    this.errors = null;
    this.approveModal.toggle();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  private initSearchFilter(params?) {
    this.filterForm = this.fb.group({
      publisherIds: [],
      publisherManagerIds: [],
      advertiserIds: [],
      advertiserManagerIds: [],
      offerIds: [],
      sensitive: [],
      statuses: [],
      search: [],
      page: [],
      limit: [],
      sort: [],
      order: []
    });

    if (Object.keys(params).length) {
      const keys = Object.keys(params);
      for (const prop of keys) {
        if (prop.includes('[]') && typeof params[prop] === 'string') {
          params[prop] = [params[prop]];
        }
        if (Array.isArray(params[prop])) {
          params[prop] = params[prop].map(item => item);
        }
      }
      this.filterForm.patchValue(params);
    } else {
      this.filterForm.patchValue({
        page: '1',
        limit: '10'
      });
    }

  }


  generateError(err, isTitle) {
    let newMsg = '';
    if (typeof err === 'string') {
      return err;
    }
    if (typeof err.errors === 'string') {
      return err.errors;
    }
    if (typeof err.message === 'string' && isTitle) {
      newMsg = `${err.message} \n`;
    }
    if (Array.isArray(err.errors) && !isTitle) {
      err.errors.map(error => {
        newMsg += `${error.message}: ${error.items.join()} \n`;
      });
    }
    return newMsg;
  }
}
