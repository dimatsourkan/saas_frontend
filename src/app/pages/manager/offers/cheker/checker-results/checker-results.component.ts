import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { CountriesService } from '../../../../../shared/services/countries/countries.service';
import { DateFormattedService } from '../../../../../shared/services/date-formatted/date-formatted.service';
import { LanguageService } from '../../../../../shared/services/language/language.service';
import { QueryParamsService } from '../../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { OffersService } from '../../offers.service';
import { CheckerService } from '../checker.service';

export interface CheckList {
  offerName: any;
  offerStatusName: any;
  offerStatus: any;
  offerStatusOrigin: any;
  checks: Array<any>;
}

@Component({
  selector: 'app-checker-results',
  templateUrl: './checker-results.component.html',
  styleUrls: ['./checker-results.component.scss']
})

export class CheckerResultsComponent implements OnInit {

  @ViewChild('checkerModal') checkerModal: ModalComponent;
  destroy$: Subject<boolean> = new Subject<boolean>();
  loading: boolean;
  dateList: any[];
  offerId: string;
  itemsOnPage: number;
  whiteCirclesArray: any[];
  params: any;
  dateRangeIndexes: number[];
  checkOfferId: string;
  countries: any;
  checkInfo: any;
  actionsTooltip: boolean;
  checkList: CheckList = {
    offerName: '',
    offerStatus: '',
    offerStatusName: '',
    offerStatusOrigin: '',
    checks: []
  };
  offerStatuses: any;

  constructor(
    private _checkerService: CheckerService,
    private route: ActivatedRoute,
    private dateFormatService: DateFormattedService,
    private countriesService: CountriesService,
    private languageService: LanguageService,
    private translate: TranslateService,
    private offersService: OffersService,
    private queryParamsService: QueryParamsService,
    private swal: SweetAlertService) {
  }

  ngOnInit() {
    this.loading = true;
    this.dateList = [];
    this.offerId = this.route.snapshot.paramMap.get('id');
    this.params = this.queryParamsService.getParams();
    this.itemsOnPage = 3;
    this.whiteCirclesArray = new Array(24);

    this.countriesService.getCountries().subscribe(res => {
      this.countries = res;
    });
    this.dateRangeIndexes = [this.itemsOnPage, -1];

    this.checkOfferId = '';
    this.offersService.getOfferStatus()
      .pipe(takeUntil(this.destroy$))
      .subscribe(statuses => {
        this.offerStatuses = statuses;
        this.loading = false;
      });

    this.getOfferCheckList(this.offerId);
  }

  applyDateRanges() {
    this.dateList = [];
    for (let i = this.dateRangeIndexes[0]; i >= this.dateRangeIndexes[1]; i--) {
      this.dateList.push(this.formatDateWithOutTime(this.getSubtractedDateWithOutFormat(i)));
    }
  }

  getOfferCheck(id) {
    this.loading = true;
    this._checkerService.getOfferCheck(id).subscribe((res) => {
      this.checkInfo = res;
      this.toggleModal();
      this.loading = false;
    });
  }

  getOfferCheckList(offerId) {
    this.loading = true;
    this._checkerService.getOfferCheckList(offerId)
      .pipe(finalize(() => this.loading = false))
      .subscribe((res) => {
        this.checkList = res;
        this.splitByDate(this.checkList);
        this.applyDateRanges();
        this.offerStatuses.forEach((status) => {
          if (this.checkList.offerStatus === status.id) {
            this.checkList.offerStatusOrigin = status.origin;
            this.checkList.offerStatusName = status.name;
          }
        });
      });
  }

  splitByDate(list) {
    const arr = [];
    const dateKeys = [];
    let platform;
    let device;
    let key;
    let country;
    for (const prop in list.checks) {
      if (list.checks.hasOwnProperty(prop)) {
        device = prop;
        platform = list.checks[prop];
        for (const prop1 in list.checks[prop]) {
          if (list.checks[prop].hasOwnProperty(prop1)) {
            key = prop1;
            country = list.checks[prop][prop1];
            platform[key] = Object.keys(country).reduce((acc, item) => {
              const date = moment(item).format(this.dateFormatService.dateFormat);
              if (!dateKeys.includes(date)) {
                dateKeys.push(date);
              }
              if (acc.hasOwnProperty(date)) {
                return Object.assign(acc, {
                  [date]: Object.assign(acc[date], {
                    [item]: country[item]
                  })
                });
              }
              return Object.assign(acc, {
                [date]: {[item]: country[item]}
              });
            }, {});
            Object.keys(dateKeys).forEach((i) => {
              const newObj = {};
              const date = moment(dateKeys[i]).format(this.dateFormatService.dateFormat);
              for (let k = 0; k < 24; k++) {
                const newKey = this.formatDate(moment(date).add(k, 'hours'));
                newObj[newKey] = country[newKey] ? country[newKey] : null;
              }
              platform[key][dateKeys[i]] = newObj;
            });
          }
        }
        arr.push({name: device, data: platform});
      }
    }
    list.checks = arr;
  }

  onArrowClick(action) {
    const valueToAdd = action === 'prev' ? this.itemsOnPage : -this.itemsOnPage;
    this.loading = true;

    this.dateRangeIndexes[0] = this.dateRangeIndexes[0] + valueToAdd;
    this.dateRangeIndexes[1] = this.dateRangeIndexes[1] + valueToAdd;
    this.applyDateRanges();
    this.getOfferCheckList(this.offerId);
  }

  disableChecking() {
    this.translate.get('checker.askToDisable').subscribe((translation) => {
      this.swal.confirm(translation).then(isConfirmed => {
        if (isConfirmed.value) {
          this._checkerService.disableChecking(this.offerId).subscribe(() => {
            this.offerStatuses = this.offerStatuses.map((status) => {
              if (status.origin === 'suspended') {
                this.checkList.offerStatus = status.id;
                this.checkList.offerStatusName = status.name;
                this.checkList.offerStatusOrigin = status.origin;
              }
            });
          }, (err) => {
            this.swal.error(err.errors[0] || 'Something went wrong');
          });
        }
      });
    });

  }

  enableChecking() {
    this.translate.get('checker.askToEnable').subscribe((translation) => {
      this.swal.confirm(translation).then(isConfirmed => {
        if (isConfirmed.value) {
          this._checkerService.enableChecking(this.offerId).subscribe(res => {
            this.offerStatuses = this.offerStatuses.map((status) => {
              if (status.origin === 'active') {
                this.checkList.offerStatus = status.id;
                this.checkList.offerStatusName = status.name;
                this.checkList.offerStatusOrigin = status.origin;
              }
            });
          }, (err) => {
            this.swal.error(err.errors[0] || 'Something went wrong');
          });
        }
      });
    });
  }

  toggleModal() {
    this.checkerModal.toggle();
  }

  checkOfferManually() {
    this.loading = true;
    this._checkerService.checkOfferManually(this.offerId)
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        () => this.swal.success('Success!'),
        () => this.swal.error('Something went wrong')
      );
  }

  formatDate(date) {
    return moment(date).format('YYYY-MM-DD HH:mm:ss');
  }

  formatDateWithOutTime(date) {
    return moment(date).format('YYYY-MM-DD');
  }

  formatUTCDate(date) {
    return moment(date).parseZone().format('YYYY-MM-DD HH:mm:ss');
  }

  getCountryName(code) {
    const country = this.countries.find(item => item.code === code);
    if (country) {
      return country.name;
    }
  }

  getSubtractedDate(n) {
    return moment().subtract(n, 'day').format('MM/DD/YYYY');
  }

  getSubtractedDateWithOutFormat(n) {
    return moment().subtract(n, 'day');
  }

  hideActionsTooltip() {
    this.actionsTooltip = false;
  }

}
