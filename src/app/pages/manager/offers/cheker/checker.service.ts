import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../../shared/services/base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class CheckerService {
  constructor(private http: HttpClient,
              private url: BaseUrlService) {
  }

  getManualCheck(id: any): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/manual_check/${id}`, {headers: {toCamelCase: 'true'}});
  }

  getOfferCheck(id: any): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_check/${id}`, {headers: {toCamelCase: 'true'}});
  }

  getOfferCheckList(offerId: string, params?): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_check/offer/${offerId}`,
      {headers: {toCamelCase: 'true'}});
  }

  manualCheckResult(id: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/manual_check/check_result/${id}`, {headers: {toCamelCase: 'true'}});
  }

  checkOfferManually(offerId: string): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/manual_check/request/${offerId}`, {offerId},
      {headers: {toCamelCase: 'true'}});
  }

  enableChecking(offerId: string): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/offers/activate/${offerId}`, {id: offerId},
      {headers: {toCamelCase: 'true'}});
  }

  linkCheck(linkCheck: any): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/link_check`, linkCheck,
      {headers: {toCamelCase: 'true'}});
  }

  disableChecking(offerId: string): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/offers/suspend/${offerId}`, {id: offerId},
      {headers: {toCamelCase: 'true'}});
  }
}
