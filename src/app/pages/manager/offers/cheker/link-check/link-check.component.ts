import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import { ValidatorService } from '../../../../../shared/components/validation/validation.service';
import { CheckerService } from '../checker.service';

@Component({
  selector: 'app-link-check',
  templateUrl: './link-check.component.html',
  styleUrls: ['./link-check.component.scss']
})
export class LinkCheckComponent implements OnInit {

  loading: boolean;
  errors: any = {};
  linkForm: FormGroup;
  submitted: boolean;
  response: any;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private validatorService: ValidatorService,
              private swal: SweetAlertService,
              private _checkerService: CheckerService) {

  }

  ngOnInit() {
    this.linkForm = this.fb.group({
      url: [, [Validators.required]],
      externalDeviceId: [, [Validators.required]],
      externalLocationId: [, [Validators.required]]
    });

    this.route.paramMap.subscribe(params => {
      if (params.get('url')) {
        const url = params.get('url');
        const externalDeviceId = params.get('externalDeviceId');
        let externalLocationId = params.get('externalLocationId');
        if (externalLocationId === 'XX') {
          externalLocationId = 'US';
        }
        this.linkForm.setValue({url, externalDeviceId, externalLocationId});
        this.checkLink();
      }
    });
  }

  checkLink() {
    this.loading = true;
    this.response = null;
    this._checkerService.linkCheck(this.linkForm.value).subscribe((res) => {
      this.response = res;
      this.loading = false;
    }, (err) => {
      this.loading = false;
      this.errors = err.errors;
      if (typeof err.errors === 'string') {
        this.swal.error(err.errors);
      }
      this.validatorService.setTouchToControls(this.linkForm);
      this.validatorService.addErrorToForm(this.linkForm, err);
    }, () => {
      this.loading = false;
    });
  }

  reset() {
    delete this.response;
    this.linkForm.reset();
  }

}
