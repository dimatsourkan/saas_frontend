import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { CountriesService } from '../../../../../shared/services/countries/countries.service';
import { ApprovalsService } from '../../approvals/approvals.service';
import { CheckerService } from '../checker.service';

@Component({
  selector: 'app-single-check',
  templateUrl: './single-check.component.html',
  styleUrls: ['./single-check.component.scss']
})
export class SingleCheckComponent implements OnInit {
  checks: any;
  loading: boolean;
  id: string;
  modal: boolean;
  countries: any[];
  checkInfo: any;
  offerStatuses: any;

  constructor(
    private _checkerService: CheckerService,
    private route: ActivatedRoute,
    private countriesService: CountriesService,
    private _approvalsService: ApprovalsService) {
  }

  ngOnInit() {
    this.loading = true;
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.getManualCheck(this.id);
    });
    this.checks = {};
    this.modal = false;
    this.countriesService.getCountries().subscribe(res => {
      this.countries = res;
    });
    this.getOfferStatuses();
  }

  getOfferStatuses() {
    this._approvalsService.getOfferStatuses().subscribe((response) => {
      this.offerStatuses = response;
    });
  }

  getManualCheck(id) {
    this.loading = true;
    this._checkerService.getManualCheck(id)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        this.checks = res;
      });
  }

  formatUTCDate(date) {
    return moment(date).parseZone().format('YYYY-MM-DD HH:mm:ss');
  }

  getOfferCheck(id) {
    this.loading = true;
    this._checkerService.manualCheckResult(id).subscribe(res => {
      this.checkInfo = res;
      this.checkInfo.redirectUrls.reverse();
      this.toggleModal();
      this.loading = false;
    });
  }

  toggleModal() {
    this.modal = !this.modal;
  }

}
