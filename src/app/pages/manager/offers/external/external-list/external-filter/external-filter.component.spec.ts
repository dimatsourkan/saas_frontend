import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalFilterComponent } from './external-filter.component';

describe('ExternalFilterComponent', () => {
  let component: ExternalFilterComponent;
  let fixture: ComponentFixture<ExternalFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
