import { Component, OnInit } from '@angular/core';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { ExternalFilter } from '@app/core/entities/external/external.filter';
import { DataStore } from '@app/core/entities/data-store.service';
import { ExternalService } from '@app/core/entities/external/external.service';

@Component({
  selector: 'external-filter',
  templateUrl: './external-filter.component.html',
  styleUrls: ['./external-filter.component.scss']
})
export class ExternalFilterComponent extends AbstractFilterComponent<ExternalFilter> implements OnInit {

  filter = (new ExternalFilter()).init();
  maxPayout$ = DataStore.externals.maxPrice.asObservable$;
  maxPayout: number;
  externalStatuses = [
    { name: 'No tracking', id: 1 },
    { name: 'With tracking', id: 2 },
    { name: 'Actualized', id: 3 },
    { name: 'All', id: 4 }
  ];

  constructor() {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.maxPayout$.subscribe((res: any) => {
      this.filter.payoutMax = res;
      this.maxPayout = res;
      this.applyFilters();
    });
    this.filter.exclude(['page', 'limit', 'search', 'dateFrom', 'dateTo']);
  }

  newValueMin(event) {
    this.filter.payoutMin = event;
  }

  newValueMax(event) {
    this.filter.payoutMax = event;
  }

  resetFilters() {
    this.filter = new ExternalFilter();
    this.filter.payoutMax = this.maxPayout;
  }

}
