import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ExternalFilter } from '@app/core/entities/external/external.filter';
import { DataStore } from '@app/core/entities/data-store.service';
import { ExternalService } from '@app/core/entities/external/external.service';
import { finalize } from 'rxjs/operators';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { Router } from '@angular/router';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import { modelId } from '@app/core/models/base-model/base.model';
import { DataStoreHelper } from '@app/core/entities/data-store.helper';

@Component({
  selector: 'external-list',
  templateUrl: './external-list.component.html',
  styleUrls: ['./external-list.component.scss']
})
export class ExternalListComponent implements OnInit {

  subscription$: Subscription;
  filter: ExternalFilter;
  externals$ = DataStore.externals.list.asObservable$;
  loading: boolean;
  isCheckerEnabled$ = DataStore.offerIntegration.isCheckerEnabled.asObservable$;
  externalDescription: any = {};
  actualizeData: any = {};
  actualizeIds: Array<string> = [];

  @ViewChild('externalActualize') externalActualize: ModalComponent;
  @ViewChild('externalDescriptionModal') externalDescriptionModal: ModalComponent;

  constructor(
    private sanitizer: DomSanitizer,
    private router: Router,
    private swal: SweetAlertService,
    private service: ExternalService) {
  }

  isChecked(invoiceId: modelId) {
    return DataStore.externals.selected.getValue().includes(invoiceId);
  }

  selectInvoice(invoiceId: modelId) {
    DataStoreHelper.toggleSelect(DataStore.externals, invoiceId);
  }

  ngOnInit() {
    this.loading = true;

    this.filter = (new ExternalFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getExternals(filter));
  }


  getExternals(filter: ExternalFilter) {
    this.loading = true;
    this.service.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: ExternalFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  calendarCallback({ startDate, endDate }) {
    this.filter.dateFrom = startDate;
    this.filter.dateTo = endDate;
  }

  filterByAppId(appId: string) {
    this.filter.appId = appId;
  }

  massActualizeExternals() {
    this.loading = true;
    if (!this.actualizeIds.length) {
      this.loading = false;
      return this.swal.info('Choose externals');
    }
    this.service.getEternalDataForActualize(this.actualizeIds, 1).subscribe(response => {
      this.actualizeData = response;
      this.toggleGoalsModal('open');
    }, error => {
      this.actualizeIds = [];
      this.swal.error('Error', error.errors).then(() => {
        this.loading = false;
      });
    }, () => {
      this.loading = false;
    });
  }

  openDependent(advertiserId, groupId) {
    const queryParams = { ...this.filter.getAvailableParams(), advertiserId, groupId };
    this.router.navigate([`/manager/offers/externals/dependent`], { queryParams });
  }

  changeStatusForActualiseOffer(data, flag: boolean) {
    this.service.changeStatus(data.offerId, data.offerGoalId, flag).subscribe(
      () => {
        data.enabled = flag;
        this.swal.success('Done');
      },
      error => this.swal.error('Error!', error.errors)
    );
  }

  selectExternal(external) {
    external.checked = !external.checked;
    if (external.checked) {
      this.actualizeIds.push(external.id);
    } else {
      this.actualizeIds = this.actualizeIds.filter((val) => val !== external.id);
    }
    this.actualizeIds = [...this.actualizeIds];
  }

  approveOffer(id: string) {
    this.service.requestForApproveExternalOffer(id).subscribe(
      () => this.swal.success('Request was send'),
      error => this.swal.error('Error', error.errors)
    );
  }

  getEternalDataForActualize(external: any) {
    this.actualizeIds = [external.id];
    this.loading = true;
    this.service.getEternalDataForActualize(this.actualizeIds, 1).subscribe(response => {
      this.actualizeData = response;
      this.toggleGoalsModal('open');
    }, error => {
      this.actualizeIds = [];
      this.swal.error('Error', error.errors).then(() => {
        this.loading = false;
      });
    }, () => {
      this.loading = false;
    });
  }

  openDescriptionModal(id: string, name: string) {
    this.externalDescriptionModal.open();
    this.externalDescription.advertiserDescription = '';
    this.externalDescription.developerDescription = '';
    this.getExternalDescription(id, name);
  }

  safeSce(desc: string) {
    return this.sanitizer.bypassSecurityTrustHtml(desc);
  }

  getExternalDescription(id: string, name: string) {
    this.service.getExternalDescription(id).subscribe((response) => {
      this.externalDescription = response;
      this.externalDescription.title = name;
      if (typeof this.externalDescription.advertiserDescription === 'string') {
        this.externalDescription.advertiserDescription =
          this.sanitizer.bypassSecurityTrustHtml(this.externalDescription.advertiserDescription) as string;
      }
      if (typeof this.externalDescription.developerDescription === 'string') {
        this.externalDescription.developerDescription =
          this.sanitizer.bypassSecurityTrustHtml(this.externalDescription.developerDescription) as string;
      }
    }, err => {
      console.error(err);
    })
  }

  private toggleGoalsModal(flag: 'open' | 'close') {
    if (flag === 'open') {
      this.externalActualize.open();
    } else {
      this.externalActualize.close();
      this.actualizeIds = [];
    }
  }

  undoOfferActualization() {
    DataStoreHelper.setDataCustom(DataStore.externals, 'selected', [] as any);
    this.toggleGoalsModal('close');
  }

}
