import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalViewComponent } from './external-view.component';

describe('ExternalViewComponent', () => {
  let component: ExternalViewComponent;
  let fixture: ComponentFixture<ExternalViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
