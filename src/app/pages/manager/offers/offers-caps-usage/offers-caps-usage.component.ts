import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';

import { OffersService } from '../offers.service';


@Component({
  selector: 'app-offers-caps-usage',
  templateUrl: './offers-caps-usage.component.html',
  styleUrls: ['./offers-caps-usage.component.scss']
})
export class OffersCapsUsageComponent implements OnInit {

  @ViewChild('approveModal') approveModal: ModalComponent;
  loading: boolean;
  offerId: any;
  publisherStatus: number;
  offer: any = {goals: []};
  publisherApprovals: any = {items: []};
  approvalId: any;
  showPublisherSelector: boolean;
  publisherId: any;
  offerGoals: any;
  errors: any = {};
  formSubmitted: boolean;
  queryParams: any = {};

  constructor(
    private route: ActivatedRoute,
    private _offersService: OffersService,
    private queryParamsService: QueryParamsService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.loading = true;
    this.route.params.subscribe((res) => {
      this.offerId = res['id'];
    });
    this.publisherStatus = 2;
    this.queryParams = this.queryParamsService.getParams();
    this.getData();
  }

  getData() {
    forkJoin([
      this.getOfferApprovalsGoals(this.offerId),
      this.getPublisherApprovals(this.offerId)
    ])
      .subscribe(response => {
        this.loading = false;
        this.offer = response[0];
        this.transformGoals();
        this.publisherApprovals = response[1];
      }, err => {
        this.loading = false;
        this.queryParams = {};
      });
  }

  getOfferApprovalsGoals(offerId): Observable<any> {
    return this._offersService.getOfferApprovalsGoals(offerId);
  }

  getPublisherApprovals(offerId): Observable<any> {
    return this._offersService.getPublisherApprovals(offerId, this.publisherStatus, this.queryParams);
  }

  getPublisherApprovalsByStatus(status) {
    this.loading = true;
    this.publisherStatus = status;
    this.getPublisherApprovals(this.offerId)
      .subscribe(response => {
        this.loading = false;
        this.queryParams.page = response.page;
        this.publisherApprovals = response;
      });
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  toggleApprovePublisherModal() {
    this.approveModal.toggle();
  }

  showApprovePublisherModal(action, publisherApproval) {
    this.setupModalData(action, publisherApproval);
    this.toggleApprovePublisherModal();
  }

  setupModalData(action, publisherApproval) {
    if (publisherApproval) {
      this.approvalId = publisherApproval.id;
      this.showPublisherSelector = false;
    } else {
      this.approvalId = undefined;
      this.publisherId = undefined;
      this.showPublisherSelector = true;
    }

    this.offerGoals = Object.assign(this.offer.goals, {});
    this.offerGoals.forEach((goal) => {
      goal.enabled = true;
      goal.payPercent = '';

      switch (action) {
        case 'editCaps':
          publisherApproval.offerGoalPublishers.forEach((pubGoal) => {
            if (goal.id === pubGoal.offerGoalId) {
              goal.enabled = pubGoal.enabled;
              if (pubGoal.payPercent || pubGoal.payPercent === 0) {
                goal.payPercent = pubGoal.payPercent;
              }
              if (pubGoal.cap) {
                if (pubGoal.cap.daily) {
                  goal.cap.daily = pubGoal.cap.daily;
                  goal.newDaily = pubGoal.cap.daily;
                }
                if (pubGoal.cap.monthly) {
                  goal.cap.monthly = pubGoal.cap.monthly;
                  goal.newMonthly = pubGoal.cap.monthly;
                }
              }
              if (pubGoal.capClick) {
                if (pubGoal.capClick.daily) {
                  goal.newClickDaily = pubGoal.capClick.daily;
                }
                if (pubGoal.capClick.monthly) {
                  goal.newClickMonthly = pubGoal.capClick.monthly;
                }
              }
            }
          });
          this.calculatePersonalPercent(goal);
          break;
        case 'approvePublisher':
          this.offerGoals.forEach((pubGoal) => {
            if (pubGoal.freeClickDailyCaps) {
              goal.newClickDaily = pubGoal.freeClickDailyCaps;
            }
            if (pubGoal.freeClickMonthlyCaps) {
              goal.newClickMonthly = pubGoal.freeClickMonthlyCaps;
            }
          });
          break;
        case 'approvePendingPublisher':
          goal.cap.daily = 0;
          goal.cap.monthly = 0;
          break;
        default:
      }
    });
  }

  calculatePersonalPayout(goal) {
    if (goal.payout.share) {
      goal.payPercent = Math.round(goal.payout.share * 100 / goal.payout.amount);
    } else {
      goal.payPercent = 0;
    }
  }

  calculatePersonalPercent(goal) {
    goal.payout.share = this.calculateFloat(goal.payout.amount / 100 * goal.payPercent);
  }

  transformApprovalData() {
    const data = {
      ids: [this.approvalId],
      goals: []
    };

    this.offerGoals.forEach(goal => {
      const item = {
        id: goal.id,
        enabled: goal.enabled ? 1 : 0,
        capDaily: goal.newDaily && goal.newDaily > 0 ? goal.newDaily : 0,
        capMonthly: goal.newMonthly && goal.newMonthly > 0 ? goal.newMonthly : 0,
        capClickDaily: goal.newClickDaily && goal.newClickDaily > 0 ? goal.newClickDaily : 0,
        capClickMonthly: goal.newClickMonthly && goal.newClickMonthly > 0 ? goal.newClickMonthly : 0,
        payPercent: goal.payPercent ? goal.payPercent : 0
      };
      data.goals.push(item);
    });
    return data;
  }

  approve() {
    const data = this.transformApprovalData();
    this._offersService.approve(data.ids[0], data.goals).subscribe((response) => {
      this.errors = {};
      this.toggleApprovePublisherModal();
      this.getPublisherApprovalsByStatus(2);
      this.sweetAlertService.success('Done!');
      this.getOfferApprovalsGoals(this.offerId).subscribe((res) => {
        this.offer = res;
        this.transformGoals();
      });
    }, (err) => {
      this.errors = err.errors;
      if (typeof err.errors === 'string') {
        this.sweetAlertService.error('Error!', err.errors);
      } else {
        let errors = [];
        for (const prop in err.errors) {
          if (err.errors.hasOwnProperty(prop)) {
            errors = [...errors, ...err.errors[prop]];
          }
        }
        this.sweetAlertService.error('Error!', errors.join());
      }
    });
  }

  approveAnother() {
    const data = {
      publisherId: this.publisherId,
      offers: [{
        offerId: this.offerId,
        goals: this.transformApprovalData().goals
      }]
    };
    this._offersService.approveAnother(data)
      .subscribe((res) => {
        this.toggleApprovePublisherModal();
        this.getPublisherApprovalsByStatus(2);
        this.sweetAlertService.success('Done!');
        this.getOfferApprovalsGoals(this.offerId).subscribe((response) => {
          this.offer = response;
          this.transformGoals();
        });
      }, (err) => {
        this.errors = err.errors;
        if (typeof err.errors === 'string') {
          const errorMessage = !data.publisherId ? 'Choose the publisher' : err.errors;
          this.sweetAlertService.error('Error!', errorMessage);
        } else {
          let errors = [];
          for (const prop in err.errors) {
            if (err.errors.hasOwnProperty(prop)) {
              errors = [...errors, ...err.errors[prop]];
            }
          }
          this.sweetAlertService.error('Error!', errors.join());
        }
      });
  }

  reApprovePublisher(publisherApproval, index) {
    this.sweetAlertService.confirm('Are you sure to re-approve ' + publisherApproval.publisher.username + ' with current caps?')
      .then((isConfirm) => {
        if (isConfirm.value) {
          this.setupModalData('editCaps', publisherApproval);
          const data = this.transformApprovalData();
          this._offersService.reApprovePublisher(data).subscribe(response => {
            this.publisherApprovals.items.splice(index, 1);
            this.sweetAlertService.success('Done!');
            this.getOfferApprovalsGoals(this.offerId).subscribe((offer) => {
              this.offer = offer;
              this.transformGoals();
            });
          }, (err) => {
            this.sweetAlertService.error(err.message, this.transformError(err));
          });
        } else {
          this.sweetAlertService.error('Cancelled!');
        }
      });
  }

  disapprovePublisher(publisherApproval, index) {
    this.sweetAlertService.confirm('Are you sure to disapprove ' + publisherApproval.publisher.username + ' ?')
      .then((isConfirm) => {
        if (isConfirm.value) {
          const data = {
            ids: [publisherApproval.id]
          };
          this._offersService.disapprovePublisher(data).subscribe(response => {
            this.publisherApprovals.items.splice(index, 1);
            this.sweetAlertService.success('Done!');
            this.getOfferApprovalsGoals(this.offerId).subscribe((offer) => {
              this.offer = offer;
              this.transformGoals();
            });
          }, (err) => {
            this.sweetAlertService.error(err.message, this.transformError(err));
          });
        } else {
          this.sweetAlertService.error('Cancelled!');
        }
      });
  }

  submitApprovalForm() {
    if (this.showPublisherSelector) {
      this.approveAnother();
    } else {
      this.approve();
    }
  }

  transformGoals() {
    this.offer.goals.forEach((goal) => {
      goal.newMonthly = goal.freeMonthlyCaps;
      goal.newDaily = goal.freeDailyCaps;
      goal.newClickMonthly = goal.capClickMonthly;
      goal.newClickDaily = goal.capClickDaily;
    });
  }

  goToPage(page) {
    this.queryParams.page = page;
    this.queryParamsService.setParams(this.queryParams);
    this.getData();
  }

  tableLimitChange(limit: number) {
    this.queryParams.limit = limit;
    this.queryParams.page = 1;
    this.queryParamsService.setParams(this.queryParams);
    this.getData();
  }

  transformError(errors) {
    let errorMsg = '';
    if (Array.isArray(errors.errors)) {
      errors.errors.map(error => {
        errorMsg += `${error.message} ${error.items.join()}`;
      });
    } else if (errors.hasOwnProperty('message')) {
      errorMsg = errors.message;
    } else {
      errorMsg = 'Has some errors';
    }
    return errorMsg;
  }


  calculateFloat(number: number, roundTo?: number) {
    if (typeof number === 'number') {
      const newNumber = Math.floor(number * 1000) / 1000;
      if (this.countDecimals(newNumber) > 2) {
        const numberInString: string = newNumber.toString();
        return Number(numberInString.substring(0, numberInString.length - 1));
      } else {
        return newNumber;
      }
    }
  }


  countDecimals(number) {
    if (!number) {
      return 0;
    }
    if (Math.floor(number.valueOf()) === number.valueOf()) {
      return 0;
    }
    return number.toString().split('.')[1].length || 0;
  }

}
