import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Params, Router } from '@angular/router';

import * as moment from 'moment';
import { Subject } from 'rxjs/index';
import { takeUntil } from 'rxjs/operators';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { DateFormattedService } from '../../../../shared/services/date-formatted/date-formatted.service';
import { MaxPayoutService } from '../../../../shared/services/max-payout/max-payout.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';
import { TagsService } from '../../../../shared/services/tags/tags.service';
import { OffersService } from '../offers.service';
import { ExternalActualizeData, ExternalObject } from '../shared/interfaces/externals';

@Component({
  selector: 'app-offers-external',
  templateUrl: './offers-external.component.html',
  styleUrls: ['./offers-external.component.scss']
})
export class OffersExternalComponent implements OnInit, OnDestroy {

  @ViewChild('perfectScrollbar') directiveRef?: ElementRef;
  @ViewChild('externalActualize') externalActualize: ModalComponent;
  @ViewChild('externalDescriptionModal') externalDescriptionModal: ModalComponent;
  destroy$: Subject<boolean> = new Subject<boolean>();
  queryParams: any;
  search: string | null;
  order = 'ASC';
  searchFilterIsOpen = false;
  searchFilter: FormGroup;
  maxPayout: number;
  loading = true;
  actualizeData: Array<ExternalActualizeData> = [];
  actualizeIds: Array<string> = [];
  externalDescription = {
    advertiserDescription: '',
    developerDescription: ''
  };
  externals: any = {
    data: [],
    items: [],
    limit: 0,
    page: 0,
    totalItems: 0,
    totalPages: 0
  };
  payoutMax: any;
  offerTags: any[] = [];

  externalStatuses = [
    {name: 'No tracking', id: 1},
    {name: 'With tracking', id: 2},
    {name: 'Actualized', id: 3},
    {name: 'All', id: 4}
  ];
  isCheckerEnabled: any;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private swal: SweetAlertService,
              private maxPayoutService: MaxPayoutService,
              private offerService: OffersService,
              private tagsService: TagsService,
              private router: Router,
              private dateFormattedService: DateFormattedService,
              private queryParamsService: QueryParamsService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.queryParams = this.queryParamsService.getParams();
    this.getIsCheckerSettingsEnabled();
    this.tagsService.getTags()
      .subscribe((items) => {
        this.offerTags = [...items];
      });

    if (!Object.keys(this.queryParams).length) {
      this.setDefaultParams();
    }


    this.queryParamsService.setParams(this.queryParams);
    this.search = this.queryParams.search;
    this.initSearchFilter(this.queryParams);
    this.subscribeToQueryParamsChange();

    this.maxPayoutService.getMaxPayout()
      .pipe(takeUntil(this.destroy$))
      .subscribe(maxPayout => {
        this.maxPayout = maxPayout;
        if (this.queryParams.payoutMax) {
          this.searchFilter.patchValue({payoutMax: this.queryParams.payoutMax});
        } else {
          this.queryParams.payoutMax = maxPayout;
          this.searchFilter.patchValue({payoutMax: maxPayout});
        }
        this.queryParamsService.setParams(this.queryParams);
      });
  }

  scrollToTop() {
    this.directiveRef.nativeElement.scrollTop = 0;
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  setDefaultParams(apply?) {
    this.queryParams = {
      payoutMin: 0,
      payoutMax: this.maxPayout,
      dateFrom: this.dateFormattedService.formatDateForBackend(moment().startOf('month')),
      dateTo: this.dateFormattedService.formatDateForBackend(moment())
    };
    if (apply) {
      this.queryParamsService.setParams(this.queryParams);
      this.search = this.queryParams.search;
      this.initSearchFilter(this.queryParams);
    }
  }

  getIsCheckerSettingsEnabled() {
    this.offerService.getIsCheckerSettingsEnabled().subscribe(res => {
      this.isCheckerEnabled = res.enabled;
    });
  }


  setSearchValue($event: string) {
    this.queryParams.page = 1;
    this.queryParams.search = $event;
    this.queryParamsService.setParams(this.queryParams);
  }

  calendarCallback({startDate, endDate}) {
    this.queryParams.page = 1;
    this.queryParams.dateFrom = startDate;
    this.queryParams.dateTo = endDate;
    this.queryParamsService.setParams(this.queryParams);
  }

  goToPage(page: number) {
    this.queryParams.page = page;
    this.queryParamsService.setParams(this.queryParams);
  }

  tableLimitChange(limit: number) {
    this.queryParams.limit = limit;
    this.queryParams.page = 1;
    this.queryParamsService.setParams(this.queryParams);
  }

  toggleFilterMode(flag: string, flagForUpdate?: boolean): void {
    if (flag === 'close' && flagForUpdate) {
      this.searchFilterIsOpen = false;
      this.patchSearchFilter(this.queryParams);
    } else if (flag === 'close') {
      this.searchFilterIsOpen = false;
    } else if (flag === 'open') {
      this.searchFilterIsOpen = true;
    }
  }

  resetFilter(item) {
    this.searchFilter.get(item).reset();
  }

  resetFilters() {
    this.searchFilter.reset();
    this.setDefaultParams(true);
    this.toggleFilterMode('close');
  }

  applyFilters() {
    this.queryParams = {...this.queryParams, ...this.searchFilter.value};
    this.queryParamsService.setParams(this.queryParams);
    this.toggleFilterMode('close');
  }

  sortByField(field: string) {
    this.queryParams.sort = field;
    this.queryParams.page = 1;
    this.queryParams.order = this.order;
    this.order = this.order === 'ASC' ? 'DESC' : 'ASC';
    this.queryParamsService.setParams(this.queryParams);
  }

  filterByAppId(id: string) {
    this.searchFilter.get('appId').setValue(id);
    this.applyFilters();
  }

  changeStatusForActualiseOffer(data, flag: boolean) {
    this.offerService.changeStatusForActualiseOffer(data.offerId, data.offerGoalId, flag).subscribe(
      () => {
        data.enabled = flag;
        this.swal.success('Done');
      },
      error => this.swal.error('Error!', error.errors)
    );
  }

  selectExternal(external) {
    external.checked = !external.checked;
    if (external.checked) {
      this.actualizeIds.push(external.id);
    } else {
      this.actualizeIds = this.actualizeIds.filter((val) => val !== external.id);
    }
    this.actualizeIds = [...this.actualizeIds];
  }

  massActualizeExternals() {
    this.loading = true;
    if (!this.actualizeIds.length) {
      this.loading = false;
      return this.swal.info('Choose externals');
    }
    this.offerService.getEternalDataForActualize(this.actualizeIds, 1).subscribe(response => {
      this.actualizeData = response;
      this.setTagsNameForActualizeData();
      this.toggleGoalsModal('open');
    }, error => {
      this.actualizeIds = [];
      this.swal.error('Error', error.errors).then(() => {
        this.externals.items.forEach(item => {
          item.externals.forEach(external => {
            external.checked = false;
          });
        });
        this.loading = false;
      });
    }, () => {
      this.loading = false;
    });
  }

  setTagsNameForActualizeData() {
    this.actualizeData.forEach(item => {
      item.tags.forEach(tag => {
        this.offerTags.forEach(obj => {
          if (obj.id === tag.id) {
            tag.name = obj.name;
          }
        });
      });
    });
  }

  approveOffer(id: string) {
    this.offerService.requestForApproveExternalOffer(id).subscribe(
      () => this.swal.success('Request was send'),
      error => this.swal.error('Error', error.errors)
    );
  }

  getEternalDataForActualize(external: ExternalObject) {
    this.actualizeIds = [external.id];
    this.loading = true;
    this.offerService.getEternalDataForActualize(this.actualizeIds, 1).subscribe(response => {
      this.actualizeData = response;
      this.setTagsNameForActualizeData();
      this.toggleGoalsModal('open');
    }, error => {
      this.actualizeIds = [];
      this.swal.error('Error', error.errors).then(() => {
        this.loading = false;
      });
    }, () => {
      this.loading = false;
    });
  }

  openDescriptionModal(id: string) {
    this.externalDescriptionModal.open();
    this.externalDescription.advertiserDescription = '';
    this.externalDescription.developerDescription = '';
    this.getExternalDescription(id);
  }

  safeSce(desc: string) {
    return this.sanitizer.bypassSecurityTrustHtml(desc);
  }

  undoOfferActualization() {
    this.externals.items.forEach(item => {
      item.externals.forEach(external => {
        external.checked = false;
      });
    });
    this.toggleGoalsModal('close');
  }

  private toggleGoalsModal(flag: 'open' | 'close') {
    if (flag === 'open') {
      this.externalActualize.open();
    } else {
      this.externalActualize.close();
      this.actualizeIds = [];
    }
  }

  private initSearchFilter(params: Params) {
    this.searchFilter = this.fb.group({
      appId: this.fb.control(params.appId),
      realId: this.fb.control(params.realId),
      advertiserIds: this.fb.control(params['advertiserIds']),
      platform: this.fb.control(params['platform']),
      countriesCodes: this.fb.control(params['countriesCodes']),
      branding: this.fb.control(params.branding),
      incent: this.fb.control(params.incent),
      externalStatus: [],
      payoutMin: this.fb.control(params.payoutMin),
      payoutMax: this.fb.control(params.payoutMax)
    });
  }

  private patchSearchFilter(queryParams: Params) {
    this.searchFilter.patchValue({
      payoutMin: queryParams.payoutMin ? +queryParams.payoutMin : null,
      payoutMax: queryParams.payoutMax ? +queryParams.payoutMax : null,
      ...queryParams
    });
  }

  openDependent(advertiserId, groupId) {
    const queryParams = {...this.queryParams, advertiserId, groupId};
    delete queryParams.advertiserIds;
    this.router.navigate([`/manager/offers/externals/dependent`], {queryParams});
  }

  private subscribeToQueryParamsChange() {
    this.route.queryParams.subscribe(() => {
      this.loading = true;
      const link = 'externals';
      this.offerService.getExternals(link, this.queryParams).subscribe(
        response => {
          this.loading = false;
          this.externals = response;
          // this.scrollToTop();
        }, () => {
          this.loading = false;
        },
        () => {
          this.loading = false;
        }
      );
    });
  }

  private getExternalDescription(id: string) {
    this.offerService.getExternalDescription(id).subscribe(response => {
      this.externalDescription = response;
      if (typeof this.externalDescription.advertiserDescription === 'string') {
        this.externalDescription.advertiserDescription =
          this.sanitizer.bypassSecurityTrustHtml(this.externalDescription.advertiserDescription) as string;
      }
      if (typeof this.externalDescription.developerDescription === 'string') {
        this.externalDescription.developerDescription =
          this.sanitizer.bypassSecurityTrustHtml(this.externalDescription.developerDescription) as string;
      }
    });
  }
}
