import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import { forkJoin } from 'rxjs';
import { DateFormattedService } from '../../../../../shared/services/date-formatted/date-formatted.service';
import { QueryParamsService } from '../../../../../shared/services/queryParams/query-params.service';

import { OffersIntegrationsService } from '../offers-integrations.service';

@Component({
  selector: 'app-integrations-list',
  templateUrl: './integrations-list.component.html',
  styleUrls: ['./integrations-list.component.scss']
})
export class IntegrationsListComponent implements OnInit {
  searchFilter: boolean;
  loading: boolean;
  scrollFlag: boolean;
  order: string;
  params: any;
  enabledOptions: { id: number; name: string; }[];
  HTTPService: any;
  statuses: any;
  integrations: any = {items: []};

  constructor(private queryParamsService: QueryParamsService,
              private dateFormattedService: DateFormattedService,
              private swal: SweetAlertService,
              private _offersIntegrationsService: OffersIntegrationsService) {
  }

  ngOnInit() {
    this.searchFilter = false;
    this.loading = true;
    this.scrollFlag = false;
    this.order = 'ASC';
    this.params = this.queryParamsService.getParams();
    this.enabledOptions = [{
      id: 1,
      name: 'Enabled'
    }, {
      id: 0,
      name: 'Disabled'
    }];

    forkJoin([this._offersIntegrationsService.getIntegrations(this.params), this._offersIntegrationsService.getStatuses()])
      .subscribe(response => {
        this.dataReady(response[0], response[1]);
        this.getIntegrations(this.params);
      });
  }


  getStatuses() {
    return this._offersIntegrationsService.getStatuses().subscribe(response => response);
  }

  getIntegrations(params): void {
    this._offersIntegrationsService.getIntegrations(params).subscribe(response => {
      this.dataReady(response);
    });
  }

  startIntegration(integration: any) {
    this._offersIntegrationsService.startIntegration(integration.advertiserId).subscribe(() => {
      this.swal.info('Integration running');
      integration.status = 2;
      this.dataReady(this.integrations);
    });
  }

  dataReady(integrations, statuses?) {
    if (statuses) {
      this.statuses = statuses;
    }
    this.integrations = integrations;
    this.integrations.items.forEach(item => {
      this.statuses.forEach(status => {
        if (item.status === status.id) {
          item.statusName = status.name;
          item.statusOriginal = status.original;
        }
      });
    });
    this.params.page = this.integrations.page;
    this.params.limit = this.integrations.limit;
    this.queryParamsService.setParams(this.params);
    this.scrollFlag = !this.scrollFlag;
    this.loading = false;
  }

  toggleSearchFilter(flag?: any) {
    if (flag) {
      this.searchFilter = false;
    } else {
      this.searchFilter = !this.searchFilter;
    }
  }

  goToPage(page) {
    this.loading = true;
    this.params.page = page;
    this.getIntegrations(this.params);
  }

  tableLimit(number) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = number;
    this.getIntegrations(this.params);
  }

  sortByField(field) {
    this.params.sort = field;
    this.params.page = 1;
    this.params.order = this.order;
    this.order = this.order === 'ASC' ? 'DESC' : 'ASC';
    this.loading = true;
    this.getIntegrations(this.params);
  }

  resetFilters() {
    this.params.advertiserIds = null;
    this.params.enabled = null;
    this.params.autoChanges = null;
    this.params.autoImport = null;
    this.params.platforms = null;
    this.params.approvalRequired = null;
    this.params.sort = null;
    this.params.order = null;
    this.loading = true;
    this.toggleSearchFilter('close');
    this.getIntegrations(this.params);
  }

  applyFilters() {
    this.loading = true;
    this.toggleSearchFilter('close');
    this.getIntegrations(this.params);
  }

}
