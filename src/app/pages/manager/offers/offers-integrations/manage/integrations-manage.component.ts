import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ValidatorService } from '../../../../../shared/components/validation/validation.service';
import { Countries } from '../../../../../shared/interfaces/countries';
import { CountriesService } from '../../../../../shared/services/countries/countries.service';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { SettingsService } from '../../../settings/settings.service';
import { OffersService } from '../../offers.service';

import { OffersIntegrationsService } from '../offers-integrations.service';

@Component({
  selector: 'app-integrations-manage',
  templateUrl: './integrations-manage.component.html',
  styleUrls: ['./integrations-manage.component.scss']
})
export class IntegrationsManageComponent implements OnInit {
  integrationId: any;
  restrictions: any;
  parent: any;
  tags: any;
  errors: any = {};
  integration: any;
  loading: boolean;
  HTTPService: any;
  offerId: any;
  CoreService: any;
  integrationVerify: any;
  $state: any;
  offerStatuses: any;
  countries: Countries[];
  integrationForm: any;
  formSubmitted: boolean;
  additionalInfo: any;
  platformNames: string[] = [];
  autoDisabledPeriod: any = [
    {
      label: 'Never',
      value: null
    },
    {
      label: 'Immediately',
      value: 0
    }
  ];
  isCheckerEnabled: boolean;
  maxPrice: any;

  constructor(
    private swal: SweetAlertService,
    private router: Router,
    private _fb: FormBuilder,
    private route: ActivatedRoute,
    private _settingsService: SettingsService,
    private countriesService: CountriesService,
    private validatorService: ValidatorService,
    private _offersService: OffersService,
    private _offersIntegrationsService: OffersIntegrationsService) {
  }

  ngOnInit() {
    this.getOfferStatuses();
    this.getOfferRestrictions();
    for (let i = 1; i < 25; i++) {
      this.autoDisabledPeriod.push({
        label: i,
        value: i
      });
    }
    this.getTags();
    this.getPlatformNames();
    this.getIsCheckerSettingsEnabled();
    this.route.params.subscribe(params => {
      this.integrationId = params['id'];
    });

    this.integrationForm = this._fb.group({
      advertiserId: [, [Validators.required]],
      enabled: [false],
      credentials: this._fb.group({
        platform: [, [Validators.required]],
        login: [, [Validators.required]],
        password: [, [Validators.required]]
      }),
      approvalRequired: [true],
      autoPausePeriod: [null],
      autoSuspendFailedChecksCount: [5, [Validators.required]],
      autoSuspendEnabled: [false],
      autoImport: this._fb.group({
        autoImportEnabled: [false],
        convertNonIncentToIncent: [false],
        deleteDescription: [false],
        addPauseText: [false],
        offerStatus: [1],
        currency: ['USD'],
        defaultTagId: [2],
        minPrice: [, [Validators.min(0)]],
        maxPrice: [, [Validators.min(0)]],
        margin: [],
        geo: [],
        restrictionsForIncent: [],
        restrictionsForNonIncent: [],
        platforms: []
      }),
      autoChanges: [false],
      period: [60, [Validators.required]]
    });

    this.integrationForm.get('autoSuspendEnabled').valueChanges.subscribe((val) => {
      if (val && (!this.integrationForm.get('autoSuspendFailedChecksCount').value)
        || this.integrationForm.get('autoSuspendFailedChecksCount').value === 0) {
        this.integrationForm.patchValue({autoSuspendFailedChecksCount: 5});
      }
    });

    this.integrationForm.controls.credentials.controls.platform.valueChanges.subscribe((val) => {
      this.getAdditionalInfo(val);
    });

    this.errors = {};

    if (this.integrationId) {
      this.getIntegration();
      this.integrationForm.get('advertiserId').disable();
    }
  }

  changeAutoSuspendEnabled(autoSuspendEnabled: any, autoSuspendCount: any) {
    if (autoSuspendEnabled.value === false) {
      autoSuspendCount.enable();
      autoSuspendCount.setValidators([Validators.required]);
      autoSuspendCount.updateValueAndValidity();
    } else {
      autoSuspendCount.setValue('');
      autoSuspendCount.disable();
      autoSuspendCount.clearValidators();
      autoSuspendCount.updateValueAndValidity();
    }
  }

  getCountries() {
    this.countriesService.getCountries().subscribe(res => {
      this.countries = res;
    });
  }

  getPlatformNames() {
    this._offersIntegrationsService.getPlatformNames().subscribe((res: any) => {
      this.platformNames = res.items;
    });
  }

  getIsCheckerSettingsEnabled() {
    this._offersService.getIsCheckerSettingsEnabled().subscribe(res => {
      this.isCheckerEnabled = res.enabled;
    });
  }

  getAdditionalInfo(name) {
    this._offersIntegrationsService.getIntegrationsAdditionalInfo({name}).subscribe((response) => {
      this.additionalInfo = response;
    });
  }

  getTags() {
    return this._settingsService.getTags().subscribe((response) => {
      this.loading = false;
      this.tags = response;
    });
  }

  getOfferRestrictions() {
    this._offersIntegrationsService.getOfferRestrictions().subscribe((response) => {
      this.restrictions = response;
      if (!this.offerId) {
        this.restrictions.forEach((item) => {
          item.id = String(item.id);
          if (item.name === 'Adult traffic' || item.name === 'Incentivized traffic') {
            item.state = -1;
          }
        });
      }
    });
  }

  getIntegration() {
    this._offersIntegrationsService.getIntegration(this.integrationId).subscribe((response) => {
      const integration: any = response;
      integration.autoImport.autoImportEnabled = integration.autoImport.enabled;
      integration.autoSuspendEnabled = (integration.autoSuspend) ? integration.autoSuspend.enabled : false;
      integration.autoSuspendFailedChecksCount = (integration.autoSuspend) ? integration.autoSuspend.failedChecksCount : null;
      if (!integration.autoImport.platforms.length) {
        integration.autoImport.platforms = this.platformNames;
      }
      this.integrationForm.patchValue(integration);
    });
  }

  testCredentials(credentials) {
    this.loading = true;
    return this._offersIntegrationsService.testCredentials(credentials).subscribe((response) => {
      this.integrationVerify = response;
      this.loading = false;
    }, (err) => {
      this.integrationVerify = false;
      this.loading = false;
      this.showError(err.errors);
    });
  }

  manageIntegration() {
    this.formSubmitted = true;
    const integration = this.removeEmpty(this.integrationForm.getRawValue());
    integration.autoImport.enabled = integration.autoImport.autoImportEnabled;
    if (integration.autoSuspendFailedChecksCount) {
      integration.autoSuspendFailedChecksCount = Number(integration.autoSuspendFailedChecksCount);
    }
    delete integration.autoImport.autoImportEnabled;
    if (this.integrationVerify && this.integrationForm.controls.enabled.value) {
      if (this.integrationId) {
        this.updateIntegration(integration);
      } else {
        this.createIntegration(integration);
      }
    } else if (!this.integrationForm.controls.enabled.value) {
      if (this.integrationId) {
        this.updateIntegration(integration);
      } else {
        this.createIntegration(integration);
      }
    } else {
      this.swal.error('Integration Verify Error');
      this.formSubmitted = false;
    }
  }

  removeEmpty(obj) {
    Object.keys(obj).forEach((key) => {
      if (obj[key] == null || obj[key] === '') {
        delete obj[key];
      }
    });
    return obj;
  }

  createIntegration(integration) {
    this._offersIntegrationsService.createIntegration(integration).subscribe((response) => {
      this.router.navigate(['/', 'manager', 'offers', 'integrations']);
    }, (err) => {
      this.errors = err.errors;
      if (typeof err.errors === 'string') {
        this.swal.error(err.errors);
        return;
      }
      this.validatorService.setTouchToControls(this.integrationForm);
      this.validatorService.addErrorToForm(this.integrationForm, err);
    });
  }

  updateIntegration(integration) {
    this._offersIntegrationsService.updateIntegration(integration, this.integrationId).subscribe((response) => {
      this.router.navigate(['/', 'manager', 'offers', 'integrations']);
    }, (err) => {
      this.errors = err.errors;
      if (typeof err.errors === 'string') {
        this.swal.error(err.errors);
        return;
      }
      this.validatorService.setTouchToControls(this.integrationForm);
      this.validatorService.addErrorToForm(this.integrationForm, err);
    });
  }

  checkForUpdate() {
    if (this.integrationVerify) {
      this.integrationVerify = null;
    }
  }

  getOfferStatuses() {
    return this._offersIntegrationsService.getOfferStatuses().subscribe((response) => {
      this.offerStatuses = response;
    });
  }

  showError(errors) {
    if (typeof errors === 'string') {
      this.swal.error(errors);
      return;
    }
    this.swal.error('Has some errors').then(() => {
      this.errors = errors;
    });
  }
}
