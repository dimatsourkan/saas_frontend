import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffersIntegrationsFilterComponent } from './offers-integrations-filter.component';

describe('OffersIntegrationsFilterComponent', () => {
  let component: OffersIntegrationsFilterComponent;
  let fixture: ComponentFixture<OffersIntegrationsFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffersIntegrationsFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffersIntegrationsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
