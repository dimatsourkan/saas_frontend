import { Component, OnInit } from '@angular/core';
import { OfferIntegrationFilter } from '@app/core/entities/offer-integration/offer-integration.filter';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { OfferIntegrationService } from '@app/core/entities/offer-integration/offer-integration.service';

@Component({
  selector: 'offers-integrations-filter',
  templateUrl: './offers-integrations-filter.component.html',
  styleUrls: ['./offers-integrations-filter.component.scss']
})
export class OffersIntegrationsFilterComponent extends AbstractFilterComponent<OfferIntegrationFilter> implements OnInit {

  filter = (new OfferIntegrationFilter()).init();
  // trafficTypes$ = DataStore.statistic.trafficTypes.asObservable$;
  enabledOptions: { id: number; name: string; }[] = [{
    id: 1,
    name: 'Enabled'
  }, {
    id: 0,
    name: 'Disabled'
  }];

  constructor(private statisticsService: OfferIntegrationService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.filter.exclude(['page', 'limit', 'search']);
  }
}
