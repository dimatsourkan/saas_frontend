import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffersIntegrationsListComponent } from './offers-integrations-list.component';

describe('OffersIntegrationsListComponent', () => {
  let component: OffersIntegrationsListComponent;
  let fixture: ComponentFixture<OffersIntegrationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffersIntegrationsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffersIntegrationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
