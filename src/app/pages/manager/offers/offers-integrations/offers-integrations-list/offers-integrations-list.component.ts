import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OfferIntegrationFilter } from '@app/core/entities/offer-integration/offer-integration.filter';
import { DataStore } from '@app/core/entities/data-store.service';
import { OfferIntegrationService } from '@app/core/entities/offer-integration/offer-integration.service';
import { finalize } from 'rxjs/operators';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';

@Component({
  selector: 'offers-integrations-list',
  templateUrl: './offers-integrations-list.component.html',
  styleUrls: ['./offers-integrations-list.component.scss']
})
export class OffersIntegrationsListComponent implements OnInit {

  subscription$: Subscription;
  filter: OfferIntegrationFilter;
  integrations$ = DataStore.offerIntegration.list.asObservable$;
  loading: boolean;

  constructor(
    private swal: SweetAlertService,
    private service: OfferIntegrationService) {
  }

  ngOnInit() {
    this.filter = (new OfferIntegrationFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getIntegration(filter));
  }

  getIntegration(filter: OfferIntegrationFilter) {
    this.loading = true;
    this.service.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: OfferIntegrationFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  startIntegration(id: string) {
    this.service.startIntegration(id).subscribe(res => {
      this.swal.info('Integration running');
    });
  }

}
