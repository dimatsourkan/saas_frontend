import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffersIntegrationsManageComponent } from './offers-integrations-manage.component';

describe('OffersIntegrationsManageComponent', () => {
  let component: OffersIntegrationsManageComponent;
  let fixture: ComponentFixture<OffersIntegrationsManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffersIntegrationsManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffersIntegrationsManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
