import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { OfferIntegration } from '@app/core/entities/offer-integration/offer-integration.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { patchFormValues } from '@app/core/helpers/helpers';
import { OfferIntegrationService } from '@app/core/entities/offer-integration/offer-integration.service';
import { DataStore } from '@app/core/entities/data-store.service';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { finalize } from 'rxjs/operators';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';

@Component({
  selector: 'offers-integrations-manage',
  templateUrl: './offers-integrations-manage.component.html',
  styleUrls: ['./offers-integrations-manage.component.scss']
})
export class OffersIntegrationsManageComponent implements OnInit {

  loading = false;
  integration: OfferIntegration;
  form: FormGroup;
  integrationVerify: any;
  additionalInfo: any;
  autoDisabledPeriod: any = [
    {
      label: 'Never',
      value: null
    },
    {
      label: 'Immediately',
      value: 0
    }
  ];
  isCheckerEnabled: boolean;
  autoImportOfferStatus$ = DataStore.offerIntegration.autoImportOfferStatus.asObservable$;
  tags$ = DataStore.offerIntegration.tags.asObservable$;
  restrictions$ = DataStore.offerIntegration.restrictions.asObservable$;
  platformNames$ = DataStore.offerIntegration.platformNames.asObservable$;
  isCheckerEnabled$ = DataStore.offerIntegration.isCheckerEnabled.asObservable$;


  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private _fb: FormBuilder,
    private validatorService: ValidatorService,
    private swal: SweetAlertService,
    private service: OfferIntegrationService) {

  }

  ngOnInit() {
    this.service.getAutoImportOfferStatus().subscribe();
    this.service.getTags().subscribe();
    this.service.getRestrictions().subscribe();
    this.service.getPlatformNames().subscribe();
    this.service.getIsCheckerEnabled().subscribe();
    for (let i = 1; i < 25; i++) {
      this.autoDisabledPeriod.push({
        label: i,
        value: i
      });
    }

    this.form = this._fb.group({
      advertiserId: [, [Validators.required]],
      enabled: [false],
      credentials: this._fb.group({
        platform: [, [Validators.required]],
        login: [, [Validators.required]],
        password: [, [Validators.required]]
      }),
      approvalRequired: [true],
      autoPausePeriod: [null],
      autoSuspend: this._fb.group({
        failedChecksCount: [5, [Validators.required]],
        enabled: [false]
      }),
      autoImport: this._fb.group({
        enabled: [false],
        convertNonIncentToIncent: [false],
        deleteDescription: [false],
        addPauseText: [false],
        offerStatus: [1],
        currency: ['USD'],
        defaultTagId: [2],
        minPrice: [, [Validators.min(0)]],
        maxPrice: [, [Validators.min(0)]],
        margin: [],
        geo: [],
        restrictionsForIncent: [],
        restrictionsForNonIncent: [],
        platforms: []
      }),
      autoChanges: [false],
      period: [60, [Validators.required]]
    });

    this.activatedRoute.data.subscribe(res => {
      this.integration = res.integration ? res.integration : new OfferIntegration();
      patchFormValues(this.form, this.integration.toJson());
    });

    this.form.get('credentials').valueChanges.subscribe((res) => {
      this.checkForUpdate();
    });

    this.form.get('credentials').get('platform').valueChanges.subscribe((res) => {
      this.getAdditionalInfo(res);
    });

    this.form.get('autoSuspend').get('enabled').valueChanges.subscribe((res) => {
      if (res === false) {
        this.form.get('autoSuspend').get('failedChecksCount').enable();
        this.form.get('autoSuspend').get('failedChecksCount').setValidators([Validators.required]);
        this.form.get('autoSuspend').get('failedChecksCount').updateValueAndValidity();
      } else {
        this.form.get('autoSuspend').get('failedChecksCount').setValue(0);
        this.form.get('autoSuspend').get('failedChecksCount').disable();
        this.form.get('autoSuspend').get('failedChecksCount').clearValidators();
        this.form.get('autoSuspend').get('failedChecksCount').updateValueAndValidity();
      }
    });

  }

  testCredentials() {
    this.loading = true;
    this.service.testCredention(this.form.get('credentials').value).subscribe((response) => {
      this.integrationVerify = response;
      this.loading = false;
    }, () => {
      this.integrationVerify = false;
      this.loading = false;
    });
  }

  checkForUpdate() {
    if (this.integrationVerify) {
      this.integrationVerify = null;
    }
  }

  getAdditionalInfo(name) {
    this.service.getIntegrationsAdditionalInfo(name).subscribe((response) => {
      this.additionalInfo = response;
    });
  }


  submitform() {
    this.loading = true;
    this.submitMethod(this.integration.update(this.form.getRawValue()))
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        res => this.router.navigate(['/manager/offers/integrations']),
        err => {
          if (typeof err.errors === 'string') {
            return this.swal.error(err.errors);
          }
          this.validatorService.addErrorToForm(this.form, err);
        });

  }

  private submitMethod(integration: OfferIntegration) {
    if ((this.integrationVerify && this.form.get('enabled').value) || !this.form.get('enabled').value) {
      return this.integration.id ?
        this.service.update(this.integration.id, integration) :
        this.service.save(integration);
    } else {
      this.swal.error('Integration Verify Error');
      this.loading = false;
    }
  }
}
