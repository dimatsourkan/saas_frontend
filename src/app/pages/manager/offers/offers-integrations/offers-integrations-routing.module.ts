import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntegrationsListComponent } from './list/integrations-list.component';
import { IntegrationsManageComponent } from './manage/integrations-manage.component';
import { OffersIntegrationsListComponent } from './offers-integrations-list/offers-integrations-list.component';
import { OffersIntegrationsResolver } from './offers-integrations.resolver';
import { OffersIntegrationsManageComponent } from './offers-integrations-manage/offers-integrations-manage.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'list', pathMatch: 'full',
    data: {
      name: 'titles.offers.integrations'
    }
  },
  {
    path: 'list', component: OffersIntegrationsListComponent,
    data: {
      name: 'titles.offers.integrations'
    }
  },
  {
    path: 'create', component: OffersIntegrationsManageComponent,
    data: {
      name: 'titles.offers.integrations'
    }
  },
  {
    path: 'edit/:id', component: OffersIntegrationsManageComponent,
    data: {
      name: 'titles.offers.integrations'
    },
    resolve: {
      integration: OffersIntegrationsResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffersIntegrationsRoutingModule {
}
