import { Component } from '@angular/core';

@Component({
  selector: 'app-offers-integrations',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class OffersIntegrationsComponent {
}

