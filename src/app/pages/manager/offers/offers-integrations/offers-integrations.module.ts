import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { IntegrationsListComponent } from './list/integrations-list.component';
import { IntegrationsManageComponent } from './manage/integrations-manage.component';

import { OffersIntegrationsRoutingModule } from './offers-integrations-routing.module';
import { OffersIntegrationsComponent } from './offers-integrations.component';
import { OffersIntegrationsListComponent } from './offers-integrations-list/offers-integrations-list.component';
import {
  OffersIntegrationsFilterComponent
} from './offers-integrations-list/offers-integrations-filter/offers-integrations-filter.component';
import { OffersIntegrationsManageComponent } from './offers-integrations-manage/offers-integrations-manage.component';
import { OffersIntegrationsResolver } from './offers-integrations.resolver';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    OffersIntegrationsRoutingModule
  ],
  declarations: [IntegrationsListComponent,
    IntegrationsManageComponent,
    OffersIntegrationsComponent,
    OffersIntegrationsListComponent,
    OffersIntegrationsFilterComponent,
     OffersIntegrationsManageComponent],
  providers: [OffersIntegrationsResolver]
})
export class OffersIntegrationsModule {
}
