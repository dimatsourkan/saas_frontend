import { Injectable } from '@angular/core';
import { BaseItemResolver } from '@app/core/services/resolver/base-item.resolver';
import { OfferIntegrationService } from '@app/core/entities/offer-integration/offer-integration.service';
import { OfferIntegration } from '@app/core/entities/offer-integration/offer-integration.model';

@Injectable()
export class OffersIntegrationsResolver extends BaseItemResolver<OfferIntegration> {

  protected service: OfferIntegrationService = this.injector.get(OfferIntegrationService, null);

  protected onError(error: any) {
    this.router.navigate(['/manager/offers/integrations/list']);
    this.hideLoader();
  }

}
