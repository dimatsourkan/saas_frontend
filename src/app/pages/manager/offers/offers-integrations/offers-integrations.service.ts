import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseUrlService } from '../../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';

@Injectable({
  providedIn: 'root'
})
export class OffersIntegrationsService {

  constructor(private http: HttpClient,
    private url: BaseUrlService,
    private queryParamsService: QueryParamsService) {
  }

  getStatuses() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/integrations/statuses`, { headers: { toCamelCase: 'true' } });
  }

  getPlatformNames() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/platform/names`, { headers: { toCamelCase: 'true' } });
  }

  getIntegrations(queryParams) {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/integrations${params}`, { headers: { toCamelCase: 'true' } });
  }

  getIntegrationsAdditionalInfo(queryParams) {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/integrations/platform_api_description${params}`,
      { headers: { toCamelCase: 'true' } });
  }


  getOfferRestrictions() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/restrictions`, { headers: { toCamelCase: 'true' } });
  }

  getIntegration(id) {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/integrations/${id}`, { headers: { toCamelCase: 'true' } });
  }

  testCredentials(credentials) {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/integrations/verify`, credentials, { headers: { toCamelCase: 'true' } });
  }

  createIntegration(credentials) {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/integrations`, credentials, { headers: { toCamelCase: 'true' } });
  }

  updateIntegration(credentials, id) {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/integrations/${id}`, credentials, { headers: { toCamelCase: 'true' } });
  }

  getOfferStatuses() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/integrations/autoimport_offer_statuses`, { headers: { toCamelCase: 'true' } });
  }

  startIntegration(advId: string) {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/integrations/${advId}/start`, {});
  }

}
