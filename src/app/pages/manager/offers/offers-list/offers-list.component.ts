import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';
import { saveAs } from 'file-saver';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { CountriesService } from '../../../../shared/services/countries/countries.service';
import { DateFormattedService } from '../../../../shared/services/date-formatted/date-formatted.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';
import { OffersService } from '../offers.service';

@Component({
  selector: 'app-offers-list',
  templateUrl: './offers-list.component.html',
  styleUrls: ['./offers-list.component.scss']
})
export class OffersListComponent implements OnInit, OnDestroy {

  @ViewChild('offerInfoModal') offerInfoModal: ModalComponent;
  @ViewChild('shortInfoModal') shortInfoModal: ModalComponent;
  @ViewChild('postbackModal') postbackModal: ModalComponent;
  destroy$: Subject<boolean> = new Subject<boolean>();
  search: string | null;
  queryParams: any;
  searchFilterIsOpen: boolean;
  searchFilter: FormGroup;
  sortFilter: FormGroup;
  offerStatuses: Array<any> = [];
  loading = false;
  allOffersChecked = false;
  selectedOffers: string[] = [];
  offerInfoPubResumePublisherId: any;
  filterSchema = {
    order: [
      {key: 'ASC', value: 'ascending', className: 'fa-sort-alpha-asc'},
      {key: 'DESC', value: 'descending', className: 'fa-sort-alpha-desc'}
    ]
  };

  offers = {
    data: [],
    items: [],
    limit: 0,
    page: 0,
    totalItems: 0,
    totalPages: 0
  };
  offerTypes: any[] = [];
  offerInfo: any = {};
  countries: any[] = [];
  shortInfoContent: any;
  offersShortInfo: any[] = [];
  postbackUrls: any = [];
  pixelPostbackUrls: any;
  aff_id: any;
  click_id: any;
  aff_sub: any;
  aff_sub2: any;
  aff_sub3: any;
  aff_sub4: any;
  isCheckerEnabled: any;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private dateFormattedService: DateFormattedService,
              private swal: SweetAlertService,
              private sanitizer: DomSanitizer,
              private countriesService: CountriesService,
              private queryParamsService: QueryParamsService,
              private offersService: OffersService) {
  }

  ngOnInit() {
    this.queryParams = this.queryParamsService.getParams();
    if (!Object.prototype.hasOwnProperty.call(this.queryParams, 'statuses')) {
      this.queryParams.statuses = ['2', '6'];
    }
    this.search = this.queryParams.search;
    this.initFilters(this.queryParams);
    this.offersService.getOfferStatus()
    .pipe(takeUntil(this.destroy$))
    .subscribe(statuses => this.offerStatuses = statuses);
    this.countriesService.getCountries().subscribe(res => {
      this.countries = res;
    });
    this.route.queryParams.subscribe((params) => {
      this.search = params.search;
      if (params.featured) {
        this.queryParams.featured = params.featured;
      }
      this.getOffers(this.queryParams);
    });
    this.getOfferTypes();
    this.getIsCheckerEnabled();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  isToggleElem(elem: any) {
    elem.toggleElem = !elem.toggleElem;
  }

  setSearchValue(search: string) {
    this.queryParams.page = 1;
    this.queryParams.search = search;
    this.queryParamsService.setParams(this.queryParams);
  }

  goToPage(page) {
    this.queryParams.page = page;
    this.queryParamsService.setParams(this.queryParams);
  }

  tableLimitChange(limit: number) {
    this.queryParams.limit = limit;
    this.queryParams.page = 1;
    this.queryParamsService.setParams(this.queryParams);
  }

  formattingDate(date) {
    return this.dateFormattedService.formatDateForFront(date, 'default');
  }

  formattingDateWithTime(date) {
    if (!date) {
      return '';
    }
    return this.dateFormattedService.formatDateForFront(date, 'time');
  }

  applySearchFilters() {
    this.queryParams = {...this.queryParams, ...this.sortFilter.value, ...this.searchFilter.value, page: 1};
    this.queryParamsService.setParams(this.queryParams);
    this.toggleFilterMode('close');
  }

  applySortFilters() {
    this.queryParams = {...this.queryParams, ...this.sortFilter.value, ...this.searchFilter.value, page: 1};
    this.queryParamsService.setParams(this.queryParams);
  }

  resetSearchFilters() {
    this.searchFilter.reset();
    this.queryParams = {...this.queryParams, ...this.sortFilter.value, ...this.searchFilter.value, page: 1};
    this.queryParamsService.setParams(this.queryParams);
    this.toggleFilterMode('close');
  }

  resetSortFilters() {
    this.sortFilter.reset();
    this.queryParams = {...this.queryParams, ...this.sortFilter.value, ...this.searchFilter.value, page: 1};
    this.queryParamsService.setParams(this.queryParams);
  }

  getIsCheckerEnabled() {
    this.offersService.getIsCheckerEnabled().subscribe((resolve) => {
      this.isCheckerEnabled = resolve.enabled;
    });
  }

  resetLinkParams() {
    this.click_id = null;
    this.aff_id = null;
    this.aff_sub = null;
    this.aff_sub2 = null;
    this.aff_sub3 = null;
    this.aff_sub4 = null;
  }

  changeOffersLink(flag) {
    const link = this.offerInfo.links.publisherLink.split('?');
    const params = this.getParams(link[1]);
    params[flag] = this[flag];
    if (flag === 'aff_id' && !this[flag]) {
      params[flag] = 'PUB_ID';
    }
    this.offerInfo.links.publisherLink = `${link[0]}?${this.serialize(params)}`;
  }

  toggleFilterMode(flag: string, flagForUpdate?: boolean): void {
    if (flag === 'close' && flagForUpdate) {
      this.searchFilterIsOpen = false;
      this.patchSearchFilter(this.queryParams);
    } else if (flag === 'close') {
      this.searchFilterIsOpen = false;
    } else if (flag === 'open') {
      this.searchFilterIsOpen = true;
    }
  }

  getOfferTypes() {
    this.offersService.getOfferTypes().subscribe(res => {
      this.offerTypes = res;
    });
  }

  sortByStatuses(flag: string) {
    this.offerStatuses.forEach((status) => {
      if (status.origin === flag) {
        if (!Object.prototype.hasOwnProperty.call(this.queryParams, 'statuses') || !this.queryParams['statuses'].length) {
          this.queryParams['statuses'] = [String(status.id)];
        } else {
          const arr = this.queryParams['statuses'].filter(id => id !== String(status.id));
          if (arr.length !== this.queryParams['statuses'].length) {
            this.queryParams['statuses'] = arr.length === 0 ? null : arr;
          } else {
            this.queryParams['statuses'].push(String(status.id));
          }
        }
      }
    });
    this.allOffersChecked = false;
    this.queryParams.page = 1;
    this.queryParams.limit = this.offers.limit;
    this.queryParamsService.setParams(this.queryParams);
  }

  getSelectedStatusClass(flag: string): boolean {
    if (!Object.prototype.hasOwnProperty.call(this.queryParams, 'statuses') || !this.queryParams['statuses'].length) {
      return false;
    }
    return this.offerStatuses.reduce((state, status) => {
      if (flag === status.origin) {
        this.queryParams['statuses'].forEach((id) => {
          if (Number(id) === status.id) {
            state = true;
          }
        });
      }
      return state;
    }, false);

  }

  checkingAllOffer(flag: boolean): void {
    this.selectedOffers = this.offers.items.reduce((arr, offer) => {
      offer.isChecked = flag;
      if (flag) {
        arr.push(offer.offer.id);
      } else {
        arr.splice(arr.indexOf(offer.offer.id), 1);
      }
      return arr;
    }, this.selectedOffers);
  }

  changeOffersStatus(status: string) {
    this.swal.confirm('Change offers status?')
    .then((result) => {
      if (result.value) {
        this.offersService.setOffersStatus(status, {'ids': this.selectedOffers}).subscribe((response) => {
          if (response.status === 200) {
            if (this.offers.page === this.offers.totalPages && this.selectedOffers.length === this.offers.items.length) {
              this.queryParams.page = this.offers.page - 1;
            }
            this.getOffers(this.queryParams);
            this.selectedOffers = [];
            this.allOffersChecked = false;
            this.swal.success('Done!');
          } else if (response.status === 422) {
            this.swal.error('Error!', response.data.errors[0]);
          } else if (response.status === 400) {
            this.swal.error(response.data.errors[0]);
          }
        });
      } else {
        this.swal.error('Canceled!');
      }
    });
  }

  exportOffers() {
    this.offersService.getOffersDataForExport({'ids[]': this.selectedOffers}).subscribe((response) => {
      if (response.status === 200) {
        const file = new Blob([response.body], {type: 'text/csv;charset=utf-8'});
        saveAs(file, 'offers.csv', true);
      }
    });
  }

  getChartData() {
    const data = this.offers.items.map(offer => offer.offer.id);
    this.offersService.getChartData(data).subscribe(response => {
      const chartData = response;
      this.offers.items.forEach(offer => {
        chartData.forEach(item => {
          if (offer.offer.id === item.offerId) {
            offer.chart = {
              data: item.items,
              name: item.goalName
            };
          }
        });
      });
    });
  }

  showShortInfo() {
    // TODO open modal
    this.shortInfoModal.open();
    this.generateShortInfo();
  }

  toggleDescriptionModal() {
    this.shortInfoModal.toggle();
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  checkingOffer(flag: boolean, id: string) {
    if (!flag) {
      this.selectedOffers.splice(this.selectedOffers.indexOf(id), 1);
      return false;
    }
    this.selectedOffers.push(id);
  }

  getOfferInfo(offer: any) {
    this.offersService.getOfferInfo(offer.offer.id).subscribe(res => {
      this.offerInfo = {...res};
      this.offerInfo.offer.restrictedTraffic = [];
      this.offerInfo.offer.allowedTraffic = [];
      this.offerInfo.offer.description = this.sanitizer.bypassSecurityTrustHtml(this.offerInfo.offer.description);
      this.offerInfo.publisherApprovalsList = [];
      this.getPublisherApprovals(offer.offer.id);
      this.offerInfo.offer.statusInfo = this.offerStatuses.find(status => status.id === this.offerInfo.offer.status);
      this.offerInfo.offer.goals.forEach((goal) => {
        goal.pubPayout = (goal.payout.amount / 100 * goal.payPercent).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
      });
      this.offerInfo.offer.restrictions.forEach((obj) => {
        if (obj.state === -1) {
          this.offerInfo.offer.restrictedTraffic.push(obj);
        } else if (obj.state === 1) {
          this.offerInfo.offer.allowedTraffic.push(obj);
        }
      });

      this.applyCountryNames(this.offerInfo);
      this.setupPublisherResumeInOfferInfo();
      this.offerInfo.offer.goals = this.offerInfo.offer.goals.filter(item => item.enabled);
      if (this.offerInfo.links && this.offerInfo.links.publisherLink) {
        this.changeOffersLink('aff_id');
      }
      this.offerInfoModal.open();
    });
  }

  showPostbackUrlPopup(action, goal) {
    if (action === 'open') {
      this.postbackModal.open();
    } else {
      this.postbackModal.close();
    }
    if (goal.postbackUrls) {
      this.postbackUrls = goal.postbackUrls;
    }

    this.pixelPostbackUrls = goal.pixelPostbackUrls.map(url => url);
  }

  applyCountryNames(offer) {
    if (offer.countries) {
      offer.countries = offer.countries.map((el) => this.countries.find(item => item.code === el));
    }
    offer.offer.goals.map(item => {
      item.countries = item.countries.map((el) => this.countries.find(elem => elem.code === el));
    });
  }

  setupPublisherResumeInOfferInfo(publisher?) {
    if (!publisher) {
      this.offerInfo.publisherResume = `
${this.offerInfo.resume.name}
===========================================================
${this.offerInfo.resume.description}
===========================================================
Preview URL:  ${this.offerInfo.resume.previewUrl}
Tracking URL: ${this.setupResumeTrafficUrl('PUBLISHER_ID')}

Allowed traffic: ${this.setupTrafficString(this.offerInfo.resume.allowedTraffic)}
Restricted traffic: ${this.setupTrafficString(this.offerInfo.resume.restrictedTraffic)}

Thumbnail: ${this.offerInfo.resume.thumbnail}
Creatives:
${this.offerInfo.resume.creatives.join(', \n')}
    `;
    } else {
      const publisherSelected = this.offerInfo.publisherApprovalsList.find(item => item.publisher.id === publisher);
      this.offerInfo.resume.goals.forEach((item, index) => {
        item.publisherPayout = publisherSelected.offerGoalPublishers[index].publisherPayout;
      });

      this.offerInfo.publisherResume = `
${this.offerInfo.resume.name}
===========================================================
${this.offerInfo.resume.description}
===========================================================
Preview URL:  ${this.offerInfo.resume.previewUrl}
Tracking URL: ${this.setupResumeTrafficUrl(publisher)}

Goals:
${this.setupGoalsString(this.offerInfo.resume.goals, true)}
Allowed traffic: ${this.setupTrafficString(this.offerInfo.resume.allowedTraffic)}
Restricted traffic: ${this.setupTrafficString(this.offerInfo.resume.restrictedTraffic)}

Thumbnail: ${this.offerInfo.resume.thumbnail}
Creatives:
${this.offerInfo.resume.creatives.join(', \n')}
    `;
    }
  }

  setupResumeTrafficUrl(param) {
    const link = this.offerInfo.resume.trackingUrl.split('?');
    const params = this.getParams(link[1]);
    params['aff_id'] = param;
    return `${link[0]}?${this.serialize(params)}`;
  }

  setupGoalsString(goals, fromPublisher?) {
    return goals.reduce((string, goal) => {
      return string += `
Name: ${goal.name}
Payout: ${(fromPublisher) ?
        goal.publisherPayout.amount : goal.payout.amount} ${(fromPublisher) ? goal.publisherPayout.currency : goal.payout.currency}
Daily Cap: ${goal.cap.daily}
Platforms: ${(() => goal.platforms.map(platform => platform.platform).join(', '))()}
Countries: ${goal.countries.join(', ')}

      `;
    }, '');
  }

  getPublisherApprovals(offerId) {
    this.offersService.getPublisherApprovals(offerId, 2, {page: 1, limit: 100}).subscribe((res: any) => {
      this.offerInfo.publisherApprovalsList = res.items;
    });
  }

  setupTrafficString(traffic) {
    const keys = Object.keys(traffic);
    if (!keys.length) {
      return '';
    }
    return keys.reduce((string, key, index) => {
      if (index === keys.length - 1) {
        return string += `${traffic[key].name}`;
      } else {
        return string += `${traffic[key].name}, `;
      }
    }, '');
  }


  serialize(obj: any): URLSearchParams {
    const params: URLSearchParams = new URLSearchParams();

    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const element = obj[key];
        params.set(key, element);
      }
    }
    return params;
  }

  getParams(query) {
    if (!query) {
      return {};
    }
    return (/^[?]/.test(query) ? query.slice(1) : query).split('&').reduce((params, param) => {
      const [key, value] = param.split('=');
      params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
      return params;
    }, {});
  }

  private initFilters(params?: Params) {
    this.searchFilter = this.fb.group({
      offerIds: [params['offerIds']],
      appId: [params.appId],
      platforms: [params['platforms']],
      realId: [params.realId],
      minPayout: [params.minPayout, Validators.min(0)],
      maxPayout: [params.maxPayout, Validators.min(0)],
      currency: [params.currency],
      advertiser: [params['advertiser']],
      countries: [params['countries']],
      distribution: [params.distribution],
      manager: [params['manager']],
      tags: [params['tag]']]
    });

    this.searchFilter.controls.minPayout.valueChanges.subscribe(minPayout => {
      if (!this.searchFilter.controls.currency.value) {
        this.searchFilter.patchValue({
          currency: 'USD'
        });
      }
    });

    this.searchFilter.controls.maxPayout.valueChanges.subscribe(maxPayout => {
      if (!this.searchFilter.controls.currency.value) {
        this.searchFilter.patchValue({
          currency: 'USD'
        });
      }
    });

    this.sortFilter = this.fb.group({
      approval: [isNaN(Number(params.approval)) ? null : Number(params.approval)],
      adult: [isNaN(Number(params.adult)) ? null : Number(params.adult)],
      dynamic: [isNaN(Number(params.dynamic)) ? null : Number(params.dynamic)],
      featured: [isNaN(Number(params.featured)) ? null : Number(params.featured)],
      sensitive: [isNaN(Number(params.sensitive)) ? null : Number(params.sensitive)],
      trafficType: [isNaN(Number(params.trafficType)) ? null : Number(params.trafficType)],
      visibility: [isNaN(Number(params.visibility)) ? null : Number(params.visibility)],
      sort: [params.sort],
      order: this.fb.control(params.order)
    });
  }

  private patchSearchFilter(queryParams: Params) {
    this.searchFilter.patchValue({...queryParams});
  }

  private getOffers(params: any) {
    this.loading = true;
    this.offersService.getOffers(params)
    .subscribe(
      response => {
        this.offers = response;
        this.offers.items.forEach((offer) => {
          this.extendOffer(offer);
          this.applyCountryNames(offer);
          this.applySuspendedRanges(offer.offer);
          offer.offer.goals = offer.offer.goals.map(goal => {
            return Object.assign(goal, {limit: 10});
          });
        });
        this.getChartData();
        this.queryParamsService.setParams(params);
      },
      (err) => {
        // tslint:disable-next-line:forin
        for (const prop in err.errors) {
          delete this.queryParams[prop];
        }
        this.getOffers(this.queryParams);
      },
      () => this.loading = false
    );
  }

  toggleGoalLimit(goal) {
    goal.limit = goal.limit === 10 ? goal.countries.length : 10;
  }

  applySuspendedRanges(offer) {
    if (offer.suspendedAt) {
      const hoursDiff = moment().startOf('hour').diff(offer.suspendedAt, 'hours');
      offer.lastTimeChecked = Math.floor(hoursDiff / 24) + 'D ' + hoursDiff % 24 + 'H';
    }
  }

  extendOffer(offer) {
    this.offerStatuses.forEach((status) => {
      if (offer.offer.status === status.id) {
        offer.offer.statusName = status.name;
        offer.offer.statusOrigin = status.origin;
      }
    });
    this.offerTypes.forEach((type) => {
      if (offer.offer.trafficType === type.id) {
        offer.offer.trafficTypeName = type.value;
      }
    });
    offer.offer.goals.forEach((goal) => {
      goal.pubPayout = ((goal.payout.amount * goal.payPercent) / 100).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
    });
  }

  cleanEmty(obj) {
    for (const propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
  }

  private generateShortInfo() {
    this.offersShortInfo = [];
    this.selectedOffers = Array.from(new Set(this.selectedOffers.map((itemInArray) => itemInArray)));
    this.offers.items.forEach((offer) => {
      this.selectedOffers.forEach((selectedItem) => {
        if (selectedItem === offer.offer.id) {
          const trafficType = this.offerTypes.find(item => item.id === offer.offer.trafficType);
          const data = {
            name: offer.offer.name,
            type: trafficType && trafficType.value,
            id: offer.offer.id,
            goals: []
          };
          offer.offer.goals.forEach((goal) => {
            const goalObj = {
              name: goal.name,
              platforms: goal.platforms,
              countries: goal.countries,
              countriesFlag: goal.excludeCountries,
              caps: goal.cap.daily,
              capClick: goal.capClick.daily,
              payout: goal.pubPayout,
              payoutFlag: goal.dynamicPayout ? '%' : goal.payout.currency
            };
            data.goals.push(goalObj);
          });
          this.offersShortInfo.push(data);
        }
      });
    });
    this.generateInfoToCopy(this.offersShortInfo);
  }

  private generateInfoToCopy(info) {
    this.shortInfoContent = info.reduce((text, item) => {
      return `${text}
                    Offer name: ${item.name} Traffic Type: ${item.type}
                    Offer id: ${item.id}

                    Goals: ${this.getGoalsContent(item.goals)}`;
    }, '');
  }

  private getGoalsContent(goals) {
    return goals.reduce((text, item) => {
      return `${text}
                        Goal name: ${item.name}
                        Countries: ${item.countries.map(el => {
          if (item.countriesFlag) {
            return (el) ? `-${el.code}` : '';
          } else {
            return (el) ? el.code : '';
          }
        }
      ).join(', ')}
                        Daily cap: ${item.caps}
                        Payouts: ${item.payout} ${item.payoutFlag}
                    `;
    }, '');
  }
}
