import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffersLoggerFilterComponent } from './offers-logger-filter.component';

describe('OffersLoggerFilterComponent', () => {
  let component: OffersLoggerFilterComponent;
  let fixture: ComponentFixture<OffersLoggerFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffersLoggerFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffersLoggerFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
