import { Component, OnInit } from '@angular/core';
import { OfferLoggerFilter } from '@app/core/entities/offer-logger/offer-logger.filter';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { DataStore } from '@app/core/entities/data-store.service';
import { OfferLoggerService } from '@app/core/entities/offer-logger/offer-logger.service';

@Component({
  selector: 'offers-logger-filter',
  templateUrl: './offers-logger-filter.component.html',
  styleUrls: ['./offers-logger-filter.component.scss']
})
export class OffersLoggerFilterComponent extends AbstractFilterComponent<OfferLoggerFilter> implements OnInit {

  filter = (new OfferLoggerFilter()).init();
  systems = [{
    id: 1,
    name: 'System'
  },
  {
    id: 0,
    name: 'Manager'
  }
  ];

  fields$ = DataStore.offerLogger.fields.asObservable$;
  actions$ = DataStore.offerLogger.actions.asObservable$;
  reasons$ = DataStore.offerLogger.reasons.asObservable$;

  constructor(private service: OfferLoggerService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.service.getFieldsCache().subscribe();
    this.service.getActionsCache().subscribe();
    this.service.getReasonsCache().subscribe();

    this.filter.exclude(['page', 'limit', 'search', 'dateFrom', 'dateTo']);
  }

}
