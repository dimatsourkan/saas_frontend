import { Component, OnInit, ViewChild } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { OffersService } from '../offers.service';
import { OfferLoggerService } from '@app/core/entities/offer-logger/offer-logger.service';
import { OfferLoggerFilter } from '@app/core/entities/offer-logger/offer-logger.filter';
import { finalize } from 'rxjs/operators';
import { DataStore } from '@app/core/entities/data-store.service';
import { OfferLogger } from '@app/core/entities/offer-logger/offer-logger.model';

@Component({
  selector: 'app-offers-logger',
  templateUrl: './offers-logger.component.html',
  styleUrls: ['./offers-logger.component.scss']
})
export class OffersLoggerComponent implements OnInit {

  @ViewChild('detailsModal') detailsModal: ModalComponent;
  loading: boolean;

  subscription$: Subscription;
  filter: OfferLoggerFilter;
  loggs$ = DataStore.offerLogger.list.asObservable$;

  oldData: SafeHtml;
  newData: SafeHtml;
  constructor(
    private service: OfferLoggerService,
    private _service: OffersService) {
  }

  ngOnInit() {
    this.loading = true;

    this.filter = (new OfferLoggerFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getLogs(filter));
  }


  getLogs(filter: OfferLoggerFilter) {
    this.loading = true;
    this.service.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: OfferLoggerFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  calendarCallback({ startDate, endDate }) {
    this.filter.dateFrom = startDate;
    this.filter.dateTo = endDate;
  }


  getOffersNames(ids) {
    return this._service.getOffersNames({ ids });
  }

  getManagers(ids) {
    return this._service.getManagers({ ids });
  }


  dataReady(logs, fields?) {
    // if (fields) {
    //   this.fields = this.generateFieldsOptions(fields.items);
    // }

    // this.logs = logs;
    // const offersIds = this.logs.items.map(item => item.offerId);
    // const managersIds = this.logs.items.map(item => item.managerId).filter(item => item !== null);
    // if (offersIds.length) {
    //   this.getOffersNames(offersIds).subscribe(offers => {
    //     this.logs.items.forEach(item => {
    //       offers.items.forEach(offer => (item.offerId === offer.id) ? item.offerName = offer.name : '');
    //     });
    //   });
    // }
    // if (managersIds.length) {
    //   this.getManagers(managersIds).subscribe(managers => {
    //     this.logs.items.forEach(item => {
    //       managers.items.forEach(manager => (item.managerId === manager.id) ? item.managerName = manager.name : '');
    //     });
    //   });
    // }
  }

  showDetailsPopup(action, log: OfferLogger) {
    if (action === 'open') {
      this.detailsModal.open();
    } else {
      this.detailsModal.close();
    }
    if (Object.keys(log).length) {
      this.oldData = log.data.oldValues;
      this.newData = log.data.newValues;
    }
  }


  generateFieldsOptions(fields) {
    const modifyObject = (obj) => {
      const keys = Object.keys(obj);
      return keys.reduce((result, key) => {
        if (typeof obj[key] === 'string') {
          result.push({ id: key, text: obj[key] });
          return result;
        } else {
          const children = modifyObject(obj[key]);
          result.push({ text: key.toUpperCase(), children });
          return result;
        }
      }, []);
    };
    return modifyObject(fields);
  }

}
