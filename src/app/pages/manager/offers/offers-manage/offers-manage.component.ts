import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import * as moment from 'moment';

import { combineLatest } from 'rxjs';
import { zip } from 'rxjs/internal/observable/zip';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import swal from 'sweetalert2';

import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { ValidatorService } from '../../../../shared/components/validation/validation.service';
import { BACKEND_DATE_FORMAT } from '../../../../shared/constants/date-format.const';
import { DefaultList, DefaultValueList } from '../../../../shared/interfaces/default-list';
import { Restrictions } from '../../../../shared/interfaces/restrictions';
import { GoalsService } from '../../../../shared/services/goals/goals.service';
import { PlatformsService } from '../../../../shared/services/platforms/platforms.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';
import { RestrictionsService } from '../../../../shared/services/restrictions/restrictions.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';
import { TrafficTypeService } from '../../../../shared/services/traffic-type/traffic-type.service';
import { OffersService } from '../offers.service';
import { DistributionTypeService } from '../shared/service/distribution-type/distribution-type.service';
import { PublisherPayPercentService } from '../shared/service/publisher-pay-percent/publisher-pay-percent.service';
import { DataStore } from '@app/core/entities/data-store.service';


export function ValidateUrlPattern(control: AbstractControl) {
  if (!(/^(http|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:/~+#-{}]*[\w@?^=%&amp;/~+#-{}])?$/).test(control.value)) {
    return { validUrl: true };
  }
  return null;
}

@Component({
  selector: 'app-offers-manage',
  templateUrl: './offers-manage.component.html',
  styleUrls: ['./offers-manage.component.scss']
})
export class OffersManageComponent implements OnInit, OnDestroy {

  @ViewChild('ignoreListModal') private ignoreListModal: ModalComponent;
  destroy$: Subject<boolean> = new Subject<boolean>();
  queryParams: any = {};
  errorsLength: number[] = [0, 0, 0];
  offerForm: FormGroup;
  loading = false;
  publisherId: any;
  skipPercentFlag: boolean;
  distributionTypes: Array<DefaultValueList>;
  restrictions: Array<Restrictions>;
  trafficTypes: Array<DefaultValueList>;
  publishersIgnoreList: FormGroup;
  goalsTypes: Array<DefaultList>;
  goals: Array<DefaultList>;
  toNumber = Number;
  publisherPayPercent: number;
  platforms: any[];
  errors: any;
  creativeErrors: any;
  creativeTypes = [
    { value: 'file', label: 'File' },
    { value: 'url', label: 'Url' }
  ];
  creatives: FormArray;
  thumbnailLink: any;
  isCheckerEnabled: any;
  publisherIgnoreList: FormArray;
  publisherIgnoreListTemp: FormArray;
  submitIgnoreList: boolean;
  availableCreativeExtentions = ['png', 'jpg', 'jpeg', 'webp'];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private _swal: SweetAlertService,
    private offersService: OffersService,
    private distributionTypeService: DistributionTypeService,
    private restrictionsService: RestrictionsService,
    private trafficTypesService: TrafficTypeService,
    private validatorService: ValidatorService,
    private goalsService: GoalsService,
    private queryParamsService: QueryParamsService,
    private publisherPayPercentService: PublisherPayPercentService,
    private platformsService: PlatformsService,
    private location: Location) {
  }

  get goalsFormArray(): FormArray {
    return this.offerForm.get('goals') as FormArray;
  }

  get restrictionsFormArray(): FormArray {
    return this.offerForm.get('restrictions') as FormArray;
  }

  get landingsFormArray(): FormArray {
    return this.offerForm.get('landings') as FormArray;
  }

  ngOnInit() {
    this.initOfferForm();
    this.initGoalsInfoSubscriber();
    this.getIsCheckerSettingsEnabled();
    this.queryParams = this.queryParamsService.getParams();
    this.skipPercentFlag = DataStore.token.item.getValue().special;

    if (!Object.prototype.hasOwnProperty.call(this.queryParams, 'step')) {
      this.goToStep(1);
    }
    this.initSubscribers();
    this.initRestrictionsSubscriber();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  changeAutoSuspendEnabled(autoSuspendEnabled: any, autoSuspendCount: any) {
    if (autoSuspendEnabled.value === true) {
      autoSuspendCount.enable();
      autoSuspendCount.setValidators([Validators.required]);
      autoSuspendCount.updateValueAndValidity();
    } else {
      autoSuspendCount.setValue(0);
      autoSuspendCount.disable();
      autoSuspendCount.clearValidators();
      autoSuspendCount.updateValueAndValidity();
    }
  }

  goToStep(step: number): void {
    this.queryParams.step = step;
    this.queryParamsService.setParams(this.queryParams);
  }

  getOfferInfoByPreviewUrl() {
    this.loading = true;
    this.offersService.getOfferByPreviewUrl(this.offerForm.get('previewUrl').value)
      .subscribe(
        res => {
          this.offerForm.patchValue({ appId: res.appId });
          this.loading = false;
          if (this.offerForm.get('description').value.replace(/&nbsp;/g, '').replace(/(<([^>]+)>)/ig, '').replace(' ', '').length === 0) {
            this.offerForm.patchValue({
              thumbUrl: res.thumbUrl,
              name: res.name,
              description: res.description,
              tags: res.tags
            });
          } else {
            let answer = '';
            swal({
              text: 'What do with description?',
              type: 'question',
              showCancelButton: false,
              showConfirmButton: false,
              footer: `
              <div class="swal2-actions" style="display: flex;">
                <button type="button" id="cancel" class="swal2-cancel btn btn-danger cancel"  style="display: inline-block;">Cancel</button>
                <button type="button" id="merge" class="swal2-confirm btn btn-success btn-merge" aria-label="">Merge</button>
                <button type="button" id="overwrite" class="swal2-confirm btn btn-success btn-overwrite" aria-label="">Overwrite</button>
              </div>
              `,
              onBeforeOpen: () => {
                answer = '';
                const footer = swal.getFooter();
                const cancel = footer.getElementsByClassName('cancel');
                const merge = footer.getElementsByClassName('btn-merge');
                const overwrite = footer.getElementsByClassName('btn-overwrite');
                cancel[0].addEventListener('click', () => {
                  answer = 'close';
                  swal.close();
                });

                merge[0].addEventListener('click', () => {
                  answer = 'merge';
                  swal.clickConfirm();
                });

                overwrite[0].addEventListener('click', () => {
                  answer = 'overwrite';
                  swal.clickConfirm();
                });
              }
            }).then((result) => {
              if (!result.value) {
                return false;
              }
              switch (answer) {
                // eslint-disable-next-line no-case-declarations
                case 'merge':
                  const newDescriptions = `${this.offerForm.get('description').value} ${res.description}`;
                  this.offerForm.patchValue({
                    thumbUrl: res.thumbUrl,
                    name: res.name,
                    description: newDescriptions,
                    tags: res.tags
                  });
                  this._swal.success('Done');
                  break;
                case 'overwrite':
                  this.offerForm.patchValue({
                    thumbUrl: res.thumbUrl,
                    name: res.name,
                    description: res.description,
                    tags: res.tags
                  });
                  this._swal.success('Done');
                  break;
                // eslint-disable-next-line no-case-declarations
                default:
                  const oldDescription = `${this.offerForm.get('description').value}`;
                  this.offerForm.patchValue({
                    thumbUrl: res.thumbUrl,
                    name: res.name,
                    description: oldDescription,
                    tags: res.tags
                  });
                  this._swal.success('Done');
              }
            });
          }

        },
        error => {
          this.loading = false;
          if (typeof error.errors === 'string') {
            this._swal.error(error.errors);
          } else {
            this._swal.error(error.errors.previewUrl[0]);
          }
        });
  }

  getOfferInfoByExternalId() {
    const advertiserId = this.offerForm.get('advertiserId').value;
    const realId = this.offerForm.get('realId').value;
    if (!advertiserId) {
      this._swal.error('Choose advertiser');
      return false;
    } else if (!realId) {
      this._swal.error('Real ID can`t be empty');
      return false;
    } else {
      this.loading = true;
      this.offersService.getOfferByExternalId(advertiserId, realId).subscribe(
        response => {
          this.loading = false;
          const data: any = response;
          this.thumbnailLink = data.thumbUrl;
          data.goal.cap = {
            daily: data.goal.capDaily,
            monthly: data.goal.capMonthly,
            total: data.goal.capTotal
          };
          data.goal.capClick = {
            daily: data.goal.capClickDaily,
            monthly: data.goal.capClickMonthly,
            total: data.goal.capClickTotal
          };
          if (data.autoSuspend) {
            this.offerForm.patchValue({
              autoSuspendEnabled: data.autoSuspend.enabled,
              autoSuspendFailedChecksCount: data.autoSuspend.failedChecksCount
            });
          }
          data.goal.pubPayout = +(data.goal.payout.amount * (data.goal.payPercent / 100)).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
          this.offerForm.patchValue({
            description: data.description,
            name: data.name,
            previewUrl: data.previewUrl,
            thumbUrl: data.thumbUrl,
            expirationDate: data.expirationDate,
            tags: data.tags
          });
          this.setGoalToForm(data.goal, 0, true);
          (this.offerForm.get('restrictions') as FormArray).controls.forEach(control => {
            control.get('state').setValue(0);
            for (const prop in data.restrictions) {
              if (control.value.id === Number(prop)) {
                control.get('state').setValue(data.restrictions[prop]);
              }
            }
          });

          this.loading = false;
        },
        (error) => {
          this._swal.error(error.errors.real_id || 'Error');
          this.loading = false;
        },
        () => this.loading = false
      );
    }
  }


  addGoal(type: any) {
    const goalFormGroup: FormGroup = this.fb.group({
      type: [type.id],
      typeName: [type.name],
      id: [],
      goal: [],
      goalSpecId: [, Validators.required],
      name: [, Validators.required],
      enabled: [true],
      expanded: [true],
      externalData: [],
      freeDailyCaps: [],
      freeClickDailyCaps: [],
      trackingUrl: [, [
        Validators.required
      ]],
      excludeCountries: [false],
      excludeCities: [false],
      countries: [[], Validators.required],
      cities: [[]],
      browsers: [[]],
      isps: [[]],
      cap: this.fb.group({
        daily: [0, Validators.required],
        monthly: [0, Validators.required],
        total: [0, Validators.required]
      }),
      capClick: this.fb.group({
        daily: [0, Validators.required],
        monthly: [0, Validators.required],
        total: [0, Validators.required]
      }),
      payout: [, [Validators.required, Validators.min(0)]],
      pubPayout: [, [Validators.required, Validators.min(0)]],
      payPercent: [this.publisherPayPercent, Validators.required],
      connections: [[]],
      dynamicPayout: [false],
      allowDuplicate: [false],
      postbackEnabled: [true],
      platforms: this.fb.array([])
    });
    this.setPlatformsForGoal(goalFormGroup);
    this.initGoalValueSubscribes(goalFormGroup);
    (this.offerForm.get('goals') as FormArray).push(goalFormGroup);
  }

  deleteGoal(index: number) {
    this.goalsFormArray.removeAt(index);
  }

  getGoal(index: number): FormControl {
    return this.offerForm.get(`goals.${index}`) as FormControl;
  }

  deleteCreative(creativeId, index) {

  }

  getPlatformFormArray(goal: FormControl): FormArray {
    return goal.get('platforms') as FormArray;
  }

  transformStringToLowerCase(str: string): string {
    return str.toLowerCase();
  }

  togglePlatformVisibility(platform: FormGroup) {
    platform.get('shown').setValue(!platform.get('shown').value);
  }

  selectPlatform(platform: FormGroup) {
    platform.get('selected').setValue(!platform.get('selected').value);
  }

  addLanding() {
    (this.offerForm.get('landings') as FormArray).push(this.fb.group({
      trackingUrl: [],
      previewUrl: []
    }));
  }

  deleteLanding(index: number) {
    (this.offerForm.get('landings') as FormArray).removeAt(index);
  }

  submitOfferForm() {
    this.loading = true;
    if (this.offerForm.get('goals').invalid) {
      this.validatorService.setTouchToControls(this.offerForm);
      // this._swal.error(this.calculateErrorMsg('Has some errors'));
      // this.loading = false;
      // return;
    }
    const id = this.offerForm.get('id').value;
    if (id) {
      this.updateOffer();
    } else {
      this.createOffer();
    }
  }

  private initOfferForm() {
    const form = this.fb.group({
      id: [],
      appId: [],
      version: [],
      advertiser: [],
      advertiserId: [, Validators.required],
      thumbUrl: [, [Validators.required]],
      previewUrl: ['', [
        Validators.pattern(/^(https?):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-{}]*[\w@?^=%&amp;\/~+#-{}])?$/),
        Validators.required]],
      realId: [],
      autoSuspendFailedChecksCount: [{ value: 5, disabled: true }],
      autoSuspendEnabled: [false],
      approvalsQuantity: [],
      creativesQuantity: [],
      createdAt: [moment().utc().format(BACKEND_DATE_FORMAT)],
      activationDate: [moment().utc().format()],
      expirationDate: [moment().utc().add(1, 'year').format()],
      redirectId: [''],
      percentSkip: [],
      name: ['', Validators.required],
      description: [''],
      trackingUrlSuffix: [],
      updatedAt: [''],
      showUserFlow: [''],
      showCapsMessage: [''],
      comment: ['', [Validators.maxLength(1000)]],
      tags: [[], Validators.required],
      approvalRequired: [true],
      status: [1],
      distribution: [, Validators.required],
      currency: ['USD', Validators.required],
      deletedAt: [],
      dynamic: [false],
      sensitive: [false],
      autoActivateEnabled: [true],
      featured: [false],
      adult: [false],
      visible: [true],
      trafficType: [2],
      cookieLifetime: ['', Validators.required],
      publisherIgnoreList: this.fb.array([]),
      suspendedAt: [],
      restrictionsGlobalStatus: ['uncheck'],
      restrictions: this.fb.array([]),
      goals: this.fb.array([]),
      landings: this.fb.array([]),
      creatives: this.fb.array([])
    });
    this.offerForm = new FormGroup(form.controls);
    this.publishersIgnoreList = this.fb.group({
      publishers: this.fb.array([])
    });
    this.subscribeForFormValuesChange();
    this.offerForm.get('status').valueChanges.subscribe(res => {
      if (res !== 1) {
        this.offerForm.patchValue({ activationDate: null });
      }
    });

    this.offerForm.get('autoSuspendEnabled').valueChanges.subscribe((val) => {
      if (val && (!this.offerForm.get('autoSuspendFailedChecksCount').value)
        || this.offerForm.get('autoSuspendFailedChecksCount').value === 0) {
        this.offerForm.patchValue({ autoSuspendFailedChecksCount: 5 }, { emitEvent: false, onlySelf: true });
      }
    });

  }

  getIsCheckerSettingsEnabled() {
    this.offersService.getIsCheckerSettingsEnabled().subscribe(res => {
      this.isCheckerEnabled = res.enabled;
    });
  }

  private subscribeForFormValuesChange() {
    this.offerForm.get('distribution')
      .valueChanges
      .subscribe(value => this.distributionTypeChange(value));
    this.offerForm.get('advertiserId')
      .valueChanges
      .subscribe(value => this.getTrackingUrlSuffix(value));
    this.offerForm.get('adult')
      .valueChanges
      .subscribe(() => this.changeAdultFlag());
    this.offerForm.get('trafficType')
      .valueChanges
      .subscribe(() => this.changeTrafficTypeFlag());
    this.offerForm.get('restrictionsGlobalStatus')
      .valueChanges
      .subscribe(value => this.changeRestrictionsGlobal(value));
  }

  getTrackingUrlSuffix(advertiserId) {
    this.offersService.getAdvertiserInfo(advertiserId).subscribe((response: any) => {
      this.offerForm.patchValue({ trackingUrlSuffix: response.items[0].url.trackingUrlSuffix });
    });
  }

  setFormArrayValue(listName: string, array: any[]): void {
    this.removeFromFormArray(listName, 0);
    if (listName === 'creatives') {
      array.map((item) => this.addCreative(item));
    } else if (listName === 'publisherIgnoreList') {
      array.map((item) => this.addPublisherIgnore(item));
    }
  }

  getExtension(content) {
    if (content) {
      const str = content.replace(/^.*\/\/[^/]+/, '');
      // tslint:disable-next-line:no-bitwise
      return str.slice((str.lastIndexOf('.') - 1 >>> 0) + 2);
    } else {
      return '';
    }
  }

  extensionIsNotAvailable(url: string) {
    return !this.availableCreativeExtentions.includes(this.getExtension(url));
  }

  extensionIsAvailable(url: string) {
    return this.availableCreativeExtentions.includes(this.getExtension(url));
  }

  calculateErrors() {
    setTimeout(() => {
      const steps: any = document.querySelectorAll('.createOffer');
      steps.forEach((el, index) => {
        this.errorsLength[index] = el.querySelectorAll('.createOffer__input_invalid').length;
      });
      // TODO: Check and improve creative validation part
      const creativeErrors = document.querySelectorAll('.creative-modal .createOffer__input_invalid').length;
      this.errorsLength[2] += creativeErrors;
    });
  }

  createCreative(item?): FormGroup {
    return this.fb.group({
      name: [(item) ? item.name : null, Validators.required],
      id: [(item) ? item.id : null],
      height: [(item) ? item.height : null],
      width: [(item) ? item.width : null],
      size: [(item) ? item.size : null],
      url: [(item) ? item.url : null],
      type: [(item) ? item.type : null],
      content: [(item) ? item.content : null],
      extension: [(item) ? item.extension : null]
    });
  }

  addCreative(item?): void {
    this.creatives = this.offerForm.get('creatives') as FormArray;
    this.creatives.push(this.createCreative(item));
  }

  setCreativeExtensionByContent(index: any) {
    const creative: FormControl = this.offerForm.get('creatives')['controls'][index];
    const content = creative.get('content').value;
    creative.patchValue({
      extension: content.split('.').pop().split(/\#|\?/)[0]
    });
  }

  getIndex(array, element, prop) {
    return array.value.findIndex(x => x[prop] === element.value[prop]);
  }

  removeFromFormArray(name: string, index: number): void {
    (<FormArray>this.offerForm.get(name)).removeAt(index);
  }

  private distributionTypeChange(type: number) {
    const cookieLifetime = this.offerForm.get('cookieLifetime');
    if (cookieLifetime.value === 30 || cookieLifetime.value === 7 || !cookieLifetime.value) {
      cookieLifetime.setValue((type > 1) ? 30 : 7);
    }
  }

  private changeAdultFlag() {
    const arr: FormArray = (this.offerForm.get('restrictions') as FormArray);
    arr.controls.forEach(item => {
      if (item.value.name === 'Adult traffic') {
        item.get('state').setValue(this.offerForm.get('adult').value ? 1 : -1);
      }
    });
  }

  private changeTrafficTypeFlag() {
    const arr: FormArray = (this.offerForm.get('restrictions') as FormArray);
    arr.controls.forEach(item => {
      if (item.value.name === 'Incentivized traffic') {
        item.get('state').setValue(this.offerForm.get('trafficType').value ? 1 : -1);
      }
    });
  }

  private setRestrictionsForFormArray() {
    const data: FormArray = this.fb.array([]);
    this.restrictions.forEach(item => {
      data.push(this.fb.group({
        name: item.name,
        id: item.id,
        state: (item.name === 'Adult traffic' || item.name === 'Incentivized traffic') ? -1 : 0
      }));
    });
    this.offerForm.setControl('restrictions', data);
  }

  toggleGoal(goal: FormGroup) {
    goal.patchValue({
      expanded: !goal.value.expanded
    });
  }

  private setPlatformsForGoal(goal) {
    const data: FormArray = this.fb.array([], this.platformValidation);
    this.platforms.forEach(item => {
      data.push(this.fb.group({
        name: [item.name, Validators.required],
        version: [],
        shown: false,
        selected: false
      }));
    });
    goal.setControl('platforms', data);
  }

  platformValidation(control: AbstractControl) {
    if (!control.value.some((element) => element.selected)) {
      return { notSelectedPlatform: true };
    }
    return null;
  }

  private changeRestrictionsGlobal(value: string) {
    const arr: FormArray = (this.offerForm.get('restrictions') as FormArray);
    switch (value) {
      case 'allow':
        arr.controls.forEach((item) => {
          item.get('state').setValue(1);
        });
        break;
      case 'disallow':
        arr.controls.forEach((item) => {
          item.get('state').setValue(-1);
        });
        break;
      default:
        arr.controls.forEach((item) => {
          item.get('state').setValue(0);
        });
    }
  }

  calculateFloat(number: number, roundTo?: number) {
    if (typeof number === 'number') {
      const newNumber = Math.floor(number * 1000) / 1000;
      if (this.countDecimals(newNumber) > 2) {
        const numberInString: string = newNumber.toString();
        return Number(numberInString.substring(0, numberInString.length - 1));
      } else {
        return newNumber;
      }
    }
  }

  private initGoalValueSubscribes(goalFormGroup: FormGroup) {
    goalFormGroup.get('goalSpecId')
      .valueChanges
      .subscribe(value => {
        goalFormGroup.get('name').setValue(this.goals.find(item => item.id === Number(value)).name);
      });
    goalFormGroup.get('payout')
      .valueChanges
      .subscribe(value => {
        const pubPayout = this.calculateFloat(value * (goalFormGroup.get('payPercent').value / 100));
        goalFormGroup.get('pubPayout').setValue(pubPayout, { emitEvent: false });
      });
    goalFormGroup.get('payPercent')
      .valueChanges
      .subscribe(value => {
        const payPercent = value < 0 ? 0 : (value > 100 ? 100 : value);
        const pubPayout = this.calculateFloat(goalFormGroup.get('payout').value * (value / 100));
        goalFormGroup.get('payPercent').setValue(payPercent, { emitEvent: false });
        goalFormGroup.get('pubPayout').setValue(pubPayout, { emitEvent: false });
      });
    goalFormGroup.get('pubPayout')
      .valueChanges
      .subscribe(value => {
        if (goalFormGroup.get('payout').value > value) {
          goalFormGroup.get('payPercent')
            .setValue((100 / (goalFormGroup.get('payout').value / goalFormGroup.get('pubPayout').value))
              .toFixed(2), { emitEvent: false });
        } else {
          if (!goalFormGroup.get('payout').value) {
            goalFormGroup.get('payout')
              .setValue((goalFormGroup.get('pubPayout').value * (goalFormGroup.get('payPercent').value / 100))
                .toFixed(2), { emitEvent: false });
            goalFormGroup.get('payPercent').setValue(this.publisherPayPercent, { emitEvent: false });
          } else {
            goalFormGroup.get('payout').setValue(goalFormGroup.get('pubPayout').value, { emitEvent: false });
            goalFormGroup.get('payPercent').setValue(100, { emitEvent: false });
          }
        }
      });
  }

  countDecimals(number) {
    if (!number) {
      return 0;
    }
    if (Math.floor(number.valueOf()) === number.valueOf()) {
      return 0;
    }
    return number.toString().split('.')[1].length || 0;
  }

  private initRestrictionsSubscriber() {
    this.restrictionsService.getRestrictions()
      .pipe(takeUntil(this.destroy$))
      .subscribe(restrictions => {
        this.restrictions = restrictions;
        this.setRestrictionsForFormArray();
      });
  }

  private initSubscribers() {
    this.trafficTypesService.getTrafficTypes()
      .pipe(takeUntil(this.destroy$))
      .subscribe(trafficTypes => this.trafficTypes = trafficTypes);

    zip(this.goalsService.getGoals(), this.distributionTypeService.getDistributionTypes())
      .pipe(takeUntil(this.destroy$))
      .subscribe(([goals, distributionTypes]) => {
        this.goals = goals;
        this.distributionTypes = distributionTypes;
      });
  }


  private initGoalsInfoSubscriber() {
    combineLatest(this.goalsService.getGoalsType(),
      this.publisherPayPercentService.getPublisherPayPercent(),
      this.platformsService.getPlatforms()).subscribe((res: any) => {
        this.goalsTypes = res[0];
        this.publisherPayPercent = res[1];
        this.platforms = res[2].items;
        if (!(this.offerForm.get('goals') as FormArray).length && this.goalsTypes.length) {
          this.addGoal({ id: 1, name: 'Main' });
          this.checkUrlForOfferId();
        }
      });
  }


  private checkUrlForOfferId() {
    if (this.router.isActive('/manager/offers/create-from-external', false)) {
      this.getExternalByIds('create');
    } else if (this.router.isActive('/manager/offers/update-from-external', false)) {
      this.getExternalByIds('update');
    } else if (this.router.isActive('/manager/offers/clone', false)) {
      this.getRelativeOfferById();
    } else if (this.router.isActive('/manager/offers/edit', false)) {
      this.getOfferById();
    }
  }

  private getOfferById() {
    const id = this.route.snapshot.paramMap.get('id');
    this.offersService.getOffer(id).subscribe(response => {
      this.setupOfferForm(response);
    },
      error => this._swal.error('Error', error).then(() => this.location.back())
    );
  }

  private getRelativeOfferById() {
    const id = this.route.snapshot.paramMap.get('id');
    this.offersService.getRelativeOffer(id).subscribe(response => {
      // tslint:disable-next-line:max-line-length
      const offer: any = response;
      this.setupOfferForm(offer);
    }, error => {
      this._swal.error('Error', error).then(() => this.location.back());
    });
  }

  private getExternalByIds(actionType: 'create' | 'update') {
    const params: any = this.queryParamsService.getParams();
    const ids = params.ids as string[];
    const updateId = params.updateId as string;
    this.offersService.getEternalDataForActualize(ids, 0).subscribe(
      response => {
        if (actionType === 'create') {
          response.create.status = 1;
          response.create.visible = true;
          response.create.approvalRequired = false;
          this.setupOfferForm(response.create, true);
        } else if (actionType === 'update') {
          if (!updateId) {
            this._swal.error('Offer not find').then(() => this.router.navigate(['/manager/offers/list']));
            return;
          }
          const offer = response.update.find(item => item.id === updateId);
          offer.status = 1;
          offer.goals = offer.goals.concat(response.goals);
          this.setupOfferForm(offer);
        }
      },
      error => this._swal.error('Error', error).then(() => this.location.back())
    );
  }

  private setupOfferForm(offer: any, isActualize?: boolean) {
    this.offerForm.patchValue({
      id: offer.id,
      appId: offer.appId,
      advertiser: offer.advertiser,
      autoSuspendEnabled: (offer.autoSuspend) ? offer.autoSuspend.enabled : false,
      autoSuspendFailedChecksCount: (offer.autoSuspend) ? offer.autoSuspend.failedChecksCount : null,
      advertiserId: offer.advertiser.id,
      approvalsQuantity: offer.approvalsQuantity,
      thumbUrl: offer.thumbUrl,
      previewUrl: offer.previewUrl,
      realId: offer.realId,
      creativesQuantity: offer.creativesQuantity,
      createdAt: offer.createdAt,
      activationDate: offer.activationDate,
      expirationDate: offer.expirationDate,
      redirectId: offer.redirectId,
      percentSkip: offer.percentSkip,
      name: offer.name,
      description: offer.description,
      updatedAt: offer.updatedAt,
      trackingUrlSuffix: offer.trackingUrlSuffix,
      showUserFlow: offer.showUserFlow,
      showCapsMessage: offer.showCapsMessage,
      comment: offer.comment,
      tags: offer.tags.map(tag => tag.id),
      approvalRequired: offer.approvalRequired,
      status: Number(offer.status),
      distribution: offer.distribution,
      currency: offer.currency,
      deletedAt: offer.deletedAt,
      dynamic: offer.dynamic,
      sensitive: offer.sensitive,
      featured: offer.featured,
      adult: offer.adult,
      visible: offer.visible,
      suspendedAt: offer.suspendedAt,
      version: offer.version,
      trafficType: offer.trafficType,
      cookieLifetime: offer.cookieLifetime,
      autoActivateEnabled: offer.autoActivateEnabled,
      landings: (offer.landings) ? offer.landings : []
    });
    if (offer.publisherIgnoreList) {
      this.setFormArrayValue('publisherIgnoreList', offer.publisherIgnoreList);
    }

    this.setFormArrayValue('creatives', offer.creatives.map(creative => ({
      ...creative,
      content: (isActualize) ? creative.content : creative.url, type: 'url'
    })));

    this.changeAutoSuspendEnabled(this.offerForm.get('autoSuspendEnabled'), this.offerForm.get('autoSuspendFailedChecksCount'));
    (this.offerForm.get('restrictions') as FormArray).controls.forEach(control => {
      control.get('state').setValue(0);
      offer.restrictions.forEach(restriction => {
        if (control.value.id === restriction.id) {
          control.get('state').setValue(restriction.state);
        }
      });
    });
    offer.goals.forEach((goal, index) => this.setGoalToForm(goal, index));
  }

  private setGoalToForm(goal, index: number, fromRealId?: boolean) {
    const goalFormArray = (this.offerForm.get('goals') as FormArray).at(index);
    if (goalFormArray === undefined) {
      this.addGoal({ id: 1, name: 'Main' });
      this.patchGoalValue(goal, (this.offerForm.get('goals') as FormArray).at(index));
    } else {
      this.patchGoalValue(goal, goalFormArray, fromRealId);
    }
  }

  private patchGoalValue(goal, goalFormGroup, fromRealId?: boolean) {
    if (fromRealId) {
      goalFormGroup.patchValue(goal);
    } else {
      goalFormGroup.patchValue({
        id: goal.id,
        type: goal.type,
        typeName: this.goalsTypes.find(type => type.id === goal.type).name,
        name: goal.name,
        goal: goal.goal,
        enabled: goal.enabled,
        externalData: goal.externalData,
        freeDailyCaps: goal.freeDailyCaps,
        freeClickDailyCaps: goal.freeClickDailyCaps,
        expanded: true,
        trackingUrl: goal.trackingUrl,
        excludeCountries: goal.excludeCountries,
        excludeCities: goal.excludeCities,
        countries: goal.countries,
        cities: goal.cities.map(city => city.geonameId),
        browsers: goal.browsers,
        isps: goal.isps.map(isps => isps.ispId),
        connections: goal.connections,
        dynamicPayout: goal.dynamicPayout,
        allowDuplicate: goal.allowDuplicate,
        postbackEnabled: goal.postbackEnabled
      });
    }

    goalFormGroup.get('goalSpecId').setValue(goal.goalSpecId, { emitEvent: false });
    goalFormGroup.get('payout').setValue(goal.payout.amount, { emitEvent: false });
    goalFormGroup.get('payPercent').setValue(goal.payPercent, { emitEvent: false });

    const pubPayout = this.calculateFloat(goalFormGroup.get('payout').value * (goal.payPercent / 100));
    goalFormGroup.get('pubPayout').setValue(pubPayout, {
      emitEvent: false
    });
    if (goal.cap) {
      goalFormGroup.get('cap').patchValue({
        daily: goal.cap.daily,
        monthly: goal.cap.monthly,
        total: goal.cap.total
      });
    }
    if (goal.capClick) {
      goalFormGroup.get('capClick').patchValue({
        daily: goal.capClick.daily,
        monthly: goal.capClick.monthly,
        total: goal.capClick.total
      });
    }
    (goalFormGroup.get('platforms') as FormArray).controls.forEach(control => {
      goal.platforms.forEach(platform => {
        if (platform.platform === control.value.name) {
          control.get('selected').setValue(true);
          if (platform.version) {
            control.get('version').setValue(platform.version);
            control.get('shown').setValue(true);
          }
        }
      });
    });
  }

  private updateOffer() {
    const offer = this.transformGoalData();
    this.offersService.updateOffer(offer, offer.id).subscribe(res => {
      this.router.navigate(['/manager/offers/list']);
    }, error => {
      if (typeof error === 'object') {
        this.parsingError(error.errors);
        this._swal.error(this.calculateErrorMsg(error.errors)).then(() => {
          this.errors = error.errors;
          this.calculateErrors();
          this.validatorService.addErrorToForm(this.offerForm, error);
          return this.validatorService.setTouchToControls(this.offerForm);
        });
      }
    });
  }

  private createOffer() {
    const data = this.route.data as any;
    const offer = this.prepareOffer(this.offerForm.getRawValue());
    offer.isExternal = data.value.isExternal;
    this.offersService.createOffer(offer).subscribe(res => {
      this.router.navigate(['/manager/offers/list']);
    }, error => {
      if (typeof error === 'object') {
        this.parsingError(error.errors);
        this._swal.error(this.calculateErrorMsg(error.errors)).then(() => {
          this.errors = error.errors;
          this.calculateErrors();
          this.validatorService.addErrorToForm(this.offerForm, error);
          return this.validatorService.setTouchToControls(this.offerForm);
        });
      }
    });
  }

  calculateErrorMsg(error) {
    let errMsg;
    if (Array.isArray(error)) {
      errMsg = error.join(',');
    } else {
      errMsg = 'Has some errors';
    }
    return errMsg;
  }

  useCreatives(modal) {
    modal.close();
  }

  parsingError(errors) {
    this.loading = false;
    for (const prop in errors) {
      if (errors.hasOwnProperty(prop) && prop.includes('trackingUrl')) {
        const arrays = prop.match(/\[(.*?)\]/g);
        const props = arrays.map(el => {
          return el.substr(1).slice(0, -1);
        });
        this.offerForm.get('goals')['controls'][props[0]]['controls'][props[1]].setErrors({ invalidUrl: true });
      }
    }
  }

  private prepareOffer(offer) {
    const objectToSend = Object.assign({}, offer);
    Object.keys(objectToSend).forEach((key) => {
      if (objectToSend[key] == null) {
        delete objectToSend[key];
      }
    });
    objectToSend.goals.forEach(goal => {
      goal.platforms = goal.platforms.filter(platform => platform.selected).map((item => {
        return { name: item.name, version: item.version ? item.version : null };
      }));
      goal.isps = goal.isps.map(isps => Number(isps));
    });
    objectToSend.restrictions = objectToSend.restrictions.filter(restriction => restriction.state && restriction.state !== 0).map(restr => {
      return { state: restr.state, id: restr.id };
    });
    this.deleteObjProp(offer.restrictions, 'name');
    if (offer.publisherIgnoreList) {
      this.deleteObjProp(offer.publisherIgnoreList, 'name');
      this.deleteObjProp(offer.publisherIgnoreList, 'publisherName');
    }
    let i = offer.restrictions.length;
    while (i--) {
      if (!offer.restrictions[i].state || offer.restrictions[i].state === 0) {
        offer.restrictions.splice(i, 1);
      }
    }
    return objectToSend;
  }

  transformGoalData() {
    let offer = this.offerForm.getRawValue();
    offer = this.clean(offer);
    offer.goals.forEach((goal) => {
      const filtered = [];

      goal.payPercent = +(goal.payPercent.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
      goal.payout = (goal.payout) ? +(goal.payout.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]) : 0;
      goal.pubPayout = (goal.pubPayout) ? +(goal.pubPayout.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]) : 0;
      goal.isps = goal.isps.map(isps => Number(isps));
      goal.platforms.forEach((platform) => {
        if (platform.selected) {
          filtered.push({
            name: platform.name,
            version: platform.version ? platform.version : null
          });
        }
      });
      goal.platforms = filtered;
    });

    this.deleteObjProp(offer.restrictions, 'name');
    if (offer.publisherIgnoreList) {
      this.deleteObjProp(offer.publisherIgnoreList, 'publisherName');

    }
    // Getting rid of restrictions with status 0
    offer.restrictions = Object.values(offer.restrictions);
    let i = offer.restrictions.length;
    while (i--) {
      if (!offer.restrictions[i].state || offer.restrictions[i].state === 0) {
        offer.restrictions.splice(i, 1);
      }
    }
    return offer;
  }

  clean(obj) {
    for (const propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj;
  }

  uploadCreative(event, index) {
    this.offerForm.get('creatives')['controls'][index].patchValue({
      name: event.name,
      content: event.content,
      extension: event.type
    });
  }

  deleteObjProp(arr, prop) {
    for (const key in arr) {
      if (arr.hasOwnProperty(key)) {
        delete arr[key][prop];
      }
    }
  }

  removeEmpty(obj) {
    Object.keys(obj).forEach((key) => {
      if (obj[key] == null || obj[key] === '') {
        delete obj[key];
      }
    });
    return obj;
  }

  getBg() {
    return `url(${this.offerForm.get('thumbUrl').value})`;
  }


  removeDublicate(ary, prop) {
    const seen = new Set();
    return ary.filter(item => !seen.has(item[prop]) && seen.add(item[prop]));
  }


  removeInvalidItemInIgnoreList() {
    this.publishersIgnoreList.controls['publishers']['controls'].forEach((element, index) => {
      if (element.invalid) {
        const formArrayObj = (this.publishersIgnoreList.get('publishers') as FormArray);
        formArrayObj.removeAt(index);
      }
    });
  }

  updatePublishersIgnoreList(id: string) {
    if (this.publishersIgnoreList.valid) {
      this.offerForm.patchValue({
        'publisherIgnoreList': this.removeDublicate([...this.offerForm.get('publisherIgnoreList').value,
        ...this.publishersIgnoreList.get('publishers').value], 'publisherId')
      });
    }
  }


  createPublisherIgnore(item?): FormGroup {
    return this.fb.group({
      subPublisherId: [(item && item.subPublisherId) ? item.subPublisherId : null],
      publisherId: [(item) ? item.publisherId : null, Validators.compose([Validators.required, RxwebValidators.unique()])],
      publisherName: [''],
      affSub2: [(item) ? item.affSub2 : ''],
      affSub3: [(item) ? item.affSub3 : ''],
      affSub4: [(item) ? item.affSub4 : '']
    });
  }

  addPublisherIgnore(item?): void {
    this.publisherIgnoreList = this.offerForm.get('publisherIgnoreList') as FormArray;
    this.publisherIgnoreList.push(this.createPublisherIgnore(item));
  }

  addPublisherIgnoreTemp(item?): void {
    this.publisherIgnoreListTemp = this.publishersIgnoreList.get('publishers') as FormArray;
    this.publisherIgnoreListTemp.push(this.createPublisherIgnore(item));
  }

  removePublisherIgnore(index) {
    (this.publishersIgnoreList.get('publishers') as FormArray).removeAt(index);
  }

  openIgnoreListModal(modal) {
    this.submitIgnoreList = false;
    this.cloningIgnoreForm(true);

    if (this.offerForm.get('publisherIgnoreList').value.length === 0) {
      this.addPublisherIgnoreTemp();
    }
    modal.open();
  }

  closeIgnoreListModal(isSave: boolean) {
    if (isSave) {
      this.submitIgnoreList = true;
      if (this.publishersIgnoreList.get('publishers').valid) {
        this.cloningIgnoreForm();
        this.ignoreListModal.close();
      }
    } else {
      this.ignoreListModal.close();
    }
  }

  cloningIgnoreForm(toTemp?: boolean) {
    if (toTemp) {
      this.publishersIgnoreList.setControl('publishers', this.fb.array([]));
      this.offerForm.get('publisherIgnoreList').value.map((item) => this.addPublisherIgnoreTemp(item));
    } else {
      this.offerForm.setControl('publisherIgnoreList', this.fb.array([]));
      this.publishersIgnoreList.get('publishers').value.map((item) => this.addPublisherIgnore(item));
    }
  }
}


