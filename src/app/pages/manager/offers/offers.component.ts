import { Component } from '@angular/core';

@Component({
  selector: 'app-offers',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class OffersComponent {
}
