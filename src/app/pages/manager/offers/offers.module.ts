import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { CheckerResultsComponent } from './cheker/checker-results/checker-results.component';
import { LinkCheckComponent } from './cheker/link-check/link-check.component';
import { SingleCheckComponent } from './cheker/single-check/single-check.component';
import { OffersCapsUsageComponent } from './offers-caps-usage/offers-caps-usage.component';
import { OffersExternalViewComponent } from './offers-external-view/offers-external-view.component';
import { OffersExternalComponent } from './offers-external/offers-external.component';
import { OffersListComponent } from './offers-list/offers-list.component';
import { OffersLoggerComponent } from './offers-logger/offers-logger.component';
import { OffersManageComponent } from './offers-manage/offers-manage.component';
import { OffersComponent } from './offers.component';

import { OFFERS_ROUTING } from './offers.routing';
import { OffersSharedModule } from './shared/offers-shared.module';
import { ApprovalsComponent } from './approvals/approvals.component';
import { OffersLoggerFilterComponent } from './offers-logger/offers-logger-filter/offers-logger-filter.component';
import { ExternalListComponent } from './external/external-list/external-list.component';
import { ExternalFilterComponent } from './external/external-list/external-filter/external-filter.component';
import { ExternalViewComponent } from './external/external-view/external-view.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(OFFERS_ROUTING),
    SharedModule,

    OffersSharedModule
  ],
  providers: [],
  declarations: [OffersComponent, OffersListComponent, OffersManageComponent,
    OffersExternalComponent,
    CheckerResultsComponent,
    SingleCheckComponent,
    ApprovalsComponent,
    OffersCapsUsageComponent,
    OffersLoggerComponent,
    LinkCheckComponent,
    OffersExternalViewComponent,
    OffersLoggerFilterComponent,
    ExternalListComponent,
    ExternalFilterComponent,
    ExternalViewComponent]
})
export class OffersModule {
}
