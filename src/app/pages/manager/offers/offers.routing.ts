import { Routes } from '@angular/router';
import { CheckerResultsComponent } from './cheker/checker-results/checker-results.component';
import { LinkCheckComponent } from './cheker/link-check/link-check.component';
import { SingleCheckComponent } from './cheker/single-check/single-check.component';
import { OffersCapsUsageComponent } from './offers-caps-usage/offers-caps-usage.component';
import { OffersExternalViewComponent } from './offers-external-view/offers-external-view.component';
import { OffersExternalComponent } from './offers-external/offers-external.component';
import { OffersListComponent } from './offers-list/offers-list.component';
import { OffersLoggerComponent } from './offers-logger/offers-logger.component';
import { OffersManageComponent } from './offers-manage/offers-manage.component';
import { ApprovalsComponent } from './approvals/approvals.component';
import { ExternalListComponent } from './external/external-list/external-list.component';

export const OFFERS_ROUTING: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: 'list', component: OffersListComponent,
    data: {
      name: 'titles.offers.manage'
    }
  },
  {
    path: 'create', component: OffersManageComponent,
    data: {
      isExternal: false,
      name: 'titles.offers.create'
    }
  },
  {
    path: 'edit/:id', component: OffersManageComponent,
    data: {
      name: 'titles.offers.edit'
    }
  },
  {
    path: 'clone/:id', component: OffersManageComponent,
    data: {
      name: 'titles.offers.clone'
    }
  },
  {
    path: 'create-from-external', component: OffersManageComponent,
    data: {
      isExternal: true,
      name: 'titles.offers.external'
    }
  },
  {
    path: 'update-from-external', component: OffersManageComponent,
    data: {
      name: 'titles.offers.external'
    }
  },
  {
    path: 'single-check/:id', component: SingleCheckComponent,
    data: {
      name: 'titles.offers.checker'
    }
  },
  {
    path: 'link-check', component: LinkCheckComponent,
    data: {
      name: 'titles.offers.link-checker'
    }
  },
  {
    path: 'link-check/:url/:externalDeviceId/:externalLocationId', component: LinkCheckComponent,
    data: {
      name: 'titles.offers.link-checker'
    }
  },
  {
    path: 'externals/dependent', component: OffersExternalViewComponent,
    data: {
      name: 'titles.offers.external'
    }
  },
  {
    path: 'checker/:id', component: CheckerResultsComponent,
    data: {
      name: 'titles.offers.checker'
    }
  },
  {
    path: 'single-check/:id', component: SingleCheckComponent,
    data: {
      name: 'titles.offers.checker'
    }
  },
  {
    path: 'approvals', component: ApprovalsComponent,
    data: {
      name: 'titles.offers.approvals'
    }
  },
  {
    path: 'externals', component: ExternalListComponent,
    data: {
      name: 'titles.offers.external'
    }
  },
  {
    path: 'caps-usage/:id', component: OffersCapsUsageComponent,
    data: {
      name: 'titles.offers.capabilities'
    }
  },
  {
    path: 'logger', component: OffersLoggerComponent,
    data: {
      name: 'titles.offers.logger'
    }
  },
  {
    path: 'integrations',
    loadChildren: './offers-integrations/offers-integrations.module#OffersIntegrationsModule',
    data: {
      name: 'titles.offers.integrations'
    }
  }
];
