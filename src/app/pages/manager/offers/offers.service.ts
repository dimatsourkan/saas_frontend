import { BehaviorSubject, Observable, throwError as observableThrowError, AsyncSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { DefaultList } from '../../../shared/interfaces/default-list';
import { BaseUrlService } from '../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../shared/services/queryParams/query-params.service';
import { TransformToCamelCaseService } from '../../../shared/services/transform-to-camel-case/transform-to-camel-case.service';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OffersService {
  offerStatuses$ = new BehaviorSubject([]);

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService,
              private camelCase: TransformToCamelCaseService) {
    this.loadOffersStatuses();
  }

  getLogs(queryParams: Params): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_action_log${params}`, {headers: {toCamelCase: 'true'}});
  }

  getIsCheckerEnabled(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrl()}/checker_status`, {headers: {toCamelCase: 'true'}});
  }

  getIsCheckerSettingsEnabled(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/settings/checker_enabled`, {headers: {toCamelCase: 'true'}});
  }

  getMaxPrice(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/externals/maxPrice`, {headers: {toCamelCase: 'true'}});
  }

  getOfferTypes(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/traffic_types`, {headers: {toCamelCase: 'true'}});
  }

  getFields(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_action_log/fields`);
  }

  getActions(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_action_log/actions`, {headers: {toCamelCase: 'true'}});
  }

  getReasons(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_action_log/reasons`, {headers: {toCamelCase: 'true'}});
  }

  getOffersNames(ids: { 'ids': string[] }): Observable<any> {
    return this.http.post(
      `${this.url.getBaseUrlWithRole()}/offer_action_log/offers`, ids, {headers: {toCamelCase: 'true'}});
  }

  getManagers(ids: { 'ids': string[] }): Observable<any> {
    return this.http.post(
      `${this.url.getBaseUrlWithRole()}/offer_action_log/managers`, ids, {headers: {toCamelCase: 'true'}});
  }

  getOfferStatus(): Observable<DefaultList[]> {
    return this.offerStatuses$.asObservable();
  }

  getOffers(queryParams: Params): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer${params}`, {headers: {toCamelCase: 'true'}});
  }

  getOffer(id: string) {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/${id}`, {headers: {toCamelCase: 'true'}});
  }

  getOfferInfo(id: string) {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/info/${id}`, {headers: {toCamelCase: 'true'}});
  }

  getRelativeOffer(id: string) {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/${id}/clone`, {headers: {toCamelCase: 'true'}});
  }

  getOffersKeyList(queryParams: Params): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offers/key_list${params}`, {headers: {toCamelCase: 'true'}});
  }

  setOffersStatus(status: string, ids: { 'ids': string[] }): Observable<any> {
    return this.http.post(
      `${this.url.getBaseUrlWithRole()}/offers/${status}`,
      {...ids},
      {observe: 'response', headers: {toCamelCase: 'true'}}
    );
  }

  getOffersDataForExport(queryParams: Params): Observable<any> {
    const params = new HttpParams({fromObject: queryParams});
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offers/csv`, {
      params,
      responseType: 'arraybuffer',
      observe: 'response'
    });
  }

  getOfferByPreviewUrl(link: string): Observable<any> {
    // const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offers/magic_wand_store`,
      {
        params: {previewUrl: link},
        headers: {toCamelCase: 'true'}
      });
  }

  getOfferByExternalId(advertiserId: string, realId: string) {
    const params = this.queryParamsService.paramsObjectToString({advertiserId, realId});
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offers/magic_wand_external${params}`, {headers: {toCamelCase: 'true'}});
  }

  getOfferApprovalsGoals(offerId) {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_approval/${offerId}/goals`, {headers: {toCamelCase: 'true'}});
  }

  getPublisherApprovals(offerId, status, queryParams?) {
    const params = this.queryParamsService.paramsObjectToString({...queryParams, status});
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_approval/${offerId}/publisher_approvals${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  getAdvertiserInfo(advertiserId) {
    const params = this.queryParamsService.paramsObjectToString({search: advertiserId});
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offers/advertiser${params}`,
      {headers: {toCamelCase: 'true'}});
  }

  approve(id, goals) {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/offer_approval/${id}/approve`, {goals}, {headers: {toCamelCase: 'true'}});
  }

  approveAnother(data) {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/offer_approval/publisher_approve`, data, {headers: {toCamelCase: 'true'}});
  }

  reApprovePublisher(data) {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/offer_approval/approve`, data, {headers: {toCamelCase: 'true'}});
  }

  disapprovePublisher(data) {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/offer_approval/disapprove`, data, {headers: {toCamelCase: 'true'}});
  }

  getExternals(link: string, queryParams: Params): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/${link}${params}`, {headers: {toCamelCase: 'true'}});
  }

  changeStatusForActualiseOffer(offerId: string, offerGoalId: string, status: boolean) {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/offers/${offerId}/goal/${offerGoalId}`,
      {status});
  }

  requestForApproveExternalOffer(id: string) {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/externals/${id}/request_approval`);
  }

  getEternalDataForActualize(ids: Array<string>, flag: number): Observable<any> {
    const params = new HttpParams({fromObject: {pre: flag.toString()}});
    return this.http.post(
      `${this.url.getBaseUrlWithRole()}/offer/actualize`,
      {ids},
      {
        params,
        headers: {toCamelCase: 'true'}
      }
    );
  }

  getExternalDescription(id: string): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/externals/${id}/description`, {headers: {toCamelCase: 'true'}});
  }

  getChartData(offersIds): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/offers/statistics`, {offersIds}, {headers: {toCamelCase: 'true'}});
  }

  createOffer(offer) {
    return this.http.post(
      `${this.url.getBaseUrlWithRole()}/offer`,
      {...offer},
      {headers: {toCamelCase: 'true'}}
    );
  }

  updateOffer(offer, id: string) {
    return this.http.put(
      `${this.url.getBaseUrlWithRole()}/offer/${id}`,
      {...offer},
      {
        observe: 'response',
        headers: {toCamelCase: 'true'}
      });
  }

  deleteCreatives(offerId: string, creativeId: string) {
    return this.http.delete(
      `${this.url.getBaseUrlWithRole()}/offer/${offerId}/creative/${creativeId}`,
      {headers: {toCamelCase: 'true'}}
    );
  }

  private loadOffersStatuses() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/statuses`)
      .subscribe((response: Array<DefaultList>) => {
        this.offerStatuses$.next(response);
      });
  }
}
