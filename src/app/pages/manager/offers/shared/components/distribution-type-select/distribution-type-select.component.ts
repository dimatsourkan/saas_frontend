import { ChangeDetectionStrategy, Component, forwardRef, Input, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { BaseSelectComponent } from '../../../../../../shared/components/base-select/base-select.component';
import { DefaultValueList } from '../../../../../../shared/interfaces/default-list';
import { DistributionTypeService } from '../../service/distribution-type/distribution-type.service';

@Component({
  selector: 'app-distribution-type-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="value"
               bindValue="id"
               [multiple]="multiple"
               [clearable]="clearable"
               [(ngModel)]="selectedItems"
               (change)="onSelect()"
               [virtualScroll]="true"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DistributionTypeSelectComponent),
    multi: true
  }]
})
export class DistributionTypeSelectComponent extends BaseSelectComponent implements OnInit {
  @Input() multiple: boolean;

  constructor(private distributionTypes: DistributionTypeService) {
    super();
  }

  ngOnInit() {
    this.distributionTypes.getDistributionTypes()
      .pipe(takeUntil(this.destroy$))
      .subscribe((items: DefaultValueList[]) => {
        this.itemsArray = [...items];
      });
  }

}
