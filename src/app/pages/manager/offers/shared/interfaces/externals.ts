export interface ExternalData {
  Id: {
    advertiserId: string;
    groupId: string;
  };
  externals: Array<ExternalObject>;
  landingData: {
    description: string;
    imageUrl: string
    size: number;
    title: string;
    version: string;
    categoryId: number;
  };
  maxAddDate: DateObj;
  minAddDate: DateObj;
  maxPayout: number;
  minPayout: number;
  platforms: Array<number>;
  showMore?: number | null;
}

export interface ExternalObject {
  actualizedInfo: {
    offerGoalId: string;
    offerId: string;
  };

  [p: string]: any;
}

export interface ExternalActualizeData {
  description: string;
  id: string;
  name: string;
  tags: Array<any>;
}

interface DateObj {
  date: Date;
  timezone: string;
  timezoneType: number;
}
