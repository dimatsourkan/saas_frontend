import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { DistributionTypeService } from './service/distribution-type/distribution-type.service';

@NgModule({
  imports: [
    CommonModule,
    NgSelectModule,
    FormsModule
  ],
  declarations: [],
  exports: [],
  providers: [DistributionTypeService]
})
export class OffersSharedModule {
}
