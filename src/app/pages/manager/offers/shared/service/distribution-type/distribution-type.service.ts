import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DefaultValueList } from '../../../../../../shared/interfaces/default-list';
import { BaseUrlService } from '../../../../../../shared/services/base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class DistributionTypeService {
  distributionTypes$ = new BehaviorSubject([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getDistributionTypes(): Observable<DefaultValueList[]> {
    return this.distributionTypes$.asObservable();
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/distribution`)
      .subscribe((response: Array<DefaultValueList>) => {
        this.distributionTypes$.next(response);
      });
  }
}
