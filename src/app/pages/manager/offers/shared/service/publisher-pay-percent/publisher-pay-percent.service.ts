import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs/index';
import { BaseUrlService } from '../../../../../../shared/services/base-url/base-url.service';
import { OffersSharedModule } from '../../offers-shared.module';

@Injectable({
  providedIn: OffersSharedModule
})
export class PublisherPayPercentService {
  publisherPayPercent$ = new BehaviorSubject(0);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getPublisherPayPercent() {
    return this.publisherPayPercent$.asObservable() as Observable<number>;
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/settings/publisher_pay_percent`)
      .subscribe((response: number) => {
        this.publisherPayPercent$.next(response);
      });
  }
}
