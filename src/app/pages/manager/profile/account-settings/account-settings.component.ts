import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../../shared/components/validation/validation.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';
import { ProfileService } from '../profile.service';
import { ThemeService } from '@app/core/theme/theme.service';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {

  loading: boolean;
  errors: any = {};
  passwordForm: FormGroup;
  submitted: boolean;
  themes = [{ label: 'Default Saas Theme', value: 'defaultSaasTheme' }, { label: 'Test Theme', value: 'deepPurpleThem' }];

  constructor(private fb: FormBuilder,
    private themService: ThemeService,
    private swal: SweetAlertService,
    private validatorService: ValidatorService,
    private profileService: ProfileService) {

  }

  ngOnInit() {
    this.passwordForm = this.fb.group({
      theme: [localStorage.getItem('theme')],
      currentPassword: [, [Validators.required]],
      newPassword: [, [Validators.required]],
      confirmPassword: [, [Validators.required]]
    });

    this.passwordForm.get('theme').valueChanges.subscribe((theme) => {
      this.themService.setActiveThem(theme);
    });
  }


  updatePassword() {
    this.submitted = true;
    this.profileService.updatePassword(this.passwordForm.value).subscribe((response) => {
      this.swal.success('Success!');
    }, err => {
      this.errors = err.errors;
      this.validatorService.setTouchToControls(this.passwordForm);
      this.validatorService.addErrorToForm(this.passwordForm, err);
    });
  }

}
