import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PROFILE_ROUTES } from './profile.routing';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PROFILE_ROUTES),

    SharedModule
  ],
  declarations: [ProfileComponent, AccountSettingsComponent]
})
export class ProfileModule {
}
