import { Routes } from '@angular/router';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { ProfileComponent } from './profile/profile.component';

export const PROFILE_ROUTES: Routes = [
  {
    path: '',
    component: ProfileComponent
  },
  {
    path: 'account-settings',
    component: AccountSettingsComponent,
    data: {
      name: 'Account Settings'
    }
  }
];
