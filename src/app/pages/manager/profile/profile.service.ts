import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseUrlService } from '../../../shared/services/base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient,
    private url: BaseUrlService) {
  }

  getManagerInfo(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/managers/profile`, { headers: { toCamelCase: 'true' } });
  }

  refreshToken(): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/refresh_auth_token`, {}, { headers: { toCamelCase: 'true' } });
  }

  updateManager(manager): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/managers/profile`, manager, { headers: { toCamelCase: 'true' } });
  }

  updatePassword(passwords): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/account/password`, passwords, { headers: { toCamelCase: 'true' } });
  }

  downloadPersonalData(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/managers/download`, {
      responseType: 'arraybuffer',
      observe: 'response'
    });
  }

  uploadImage(content): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/managers/profile-thumbnail`, { content },
      { headers: { toCamelCase: 'true' } });
  }

  removeProfilePhoto() {
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/managers/profile-thumbnail`,
      { headers: { toCamelCase: 'true' } });
  }

}
