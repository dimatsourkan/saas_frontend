import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HeaderService } from '@app/core/components/header/header.service';
import { TokenService } from '@app/core/entities/token/token.service';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import * as FileSaver from 'file-saver';
import { finalize } from 'rxjs/operators';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  loading: boolean;
  managerForm: FormGroup;

  constructor(private fb: FormBuilder,
              private validatorService: ValidatorService,
              private router: Router,
              private tokenService: TokenService,
              private headerService: HeaderService,
              private profileService: ProfileService) {

  }

  ngOnInit() {
    this.loading = true;
    this.getManagerInfo();
    this.managerForm = this.fb.group({
      username: [, [Validators.required]],
      person: this.fb.group({
        firstName: [, [Validators.required, Validators.pattern(/^[a-zA-Z0-9]+([-_ '.]?[a-zA-Z0-9])*$/)]],
        lastName: [, [Validators.required, Validators.pattern(/^[a-zA-Z0-9]+([-_ '.]?[a-zA-Z0-9])*$/)]]
      }),
      id: [],
      additionalEmails: [],
      updatedAt: [],
      status: [],
      notes: [],
      roles: [],
      email: [, [Validators.required]],
      emailNotificationEnabled: [],
      skype: [],
      thumbnailUrl: [],
      phone: [, Validators.pattern(/^\+?[0-9]{8,20}$/i)],
      address: this.fb.group({
        address: [],
        city: [],
        state: [],
        countryCode: [],
        zipcode: []
      }),
    });
  }

  getManagerInfo() {
    this.profileService.getManagerInfo().subscribe((response) => {
      this.managerForm.patchValue(response);
      this.managerForm.get('email').disable();
      this.loading = false;
    }, (err) => this.loading = false);
  }

  updateManager() {
    this.profileService.updateManager(this.managerForm.getRawValue()).subscribe((response) => {
      this.refreshToken();
      this.router.navigate(['/', 'manager', 'dashboard']);
    }, err => {
      this.validatorService.addErrorToForm(this.managerForm, err);
    });
  }

  refreshToken() {
    this.profileService.refreshToken().subscribe(response => {
      this.loading = false;
      this.tokenService.setToken('manager', response.token);
    });
  }

  setLogo(event) {
    this.loading = true;
    this.profileService.uploadImage(event)
    .pipe(finalize(() => this.loading = false))
    .subscribe(res => {
      this.managerForm.patchValue({thumbnailUrl: res.url});
      this.headerService.getProfilePhoto();
    });
  }

  removeProfilePhoto() {
    this.loading = true;
    this.profileService.removeProfilePhoto()
    .pipe(finalize(() => this.loading = false))
    .subscribe(res => {
      this.managerForm.patchValue({thumbnailUrl: ''});
      this.headerService.getProfilePhoto();
    });
  }

  downloadPersonalData() {
    return this.profileService.downloadPersonalData().subscribe((response) => {
      const file = new Blob([response.body], {type: 'application/pdf;charset=utf-8'});
      FileSaver.saveAs(file, 'Personal Data.pdf', true);
    });
  }

}
