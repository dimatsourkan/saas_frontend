import { Component, OnInit } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { ConversionReportFilter } from '@app/core/entities/reports/conversion-reports/conversion-reports.filter';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';

@Component({
  selector: 'app-converion-reports-filter',
  templateUrl: './converion-reports-filter.component.html',
  styleUrls: ['./converion-reports-filter.component.scss']
})
export class ConverionReportsFilterComponent extends AbstractFilterComponent<ConversionReportFilter> implements OnInit {

  filter = (new ConversionReportFilter()).init();
  approveStatuses$ = DataStore.conversionReport.approveStatuses.asObservable$;
  filterUrls = {
    advertisers: `/reports/conversions/advertiser`,
    publishers: `/reports/conversions/publisher`
  };

  ngOnInit() {
    super.ngOnInit();

    DataStore.timezone.current.asObservable$
    .subscribe(timezone => {
      if (!this.filter.timezone) {
        this.filter.timezone = timezone;
      }
    });
    this.filter.exclude(['page', 'limit', 'search', 'from', 'to']);
  }

}
