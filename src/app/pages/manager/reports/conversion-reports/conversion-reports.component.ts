import { Component, OnInit } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { ConversionReportFilter } from '@app/core/entities/reports/conversion-reports/conversion-reports.filter';
import { ConversionReportService } from '@app/core/entities/reports/conversion-reports/conversion-reports.service';
import * as FileSaver from 'file-saver';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';

@Component({
  selector: 'app-conversion-reports',
  templateUrl: './conversion-reports.component.html',
  styleUrls: ['./conversion-reports.component.scss']
})
export class ConversionReportsComponent implements OnInit {

  subscription$: Subscription;
  filter: ConversionReportFilter;
  conversionReport$ = DataStore.conversionReport.list.asObservable$;
  isShowUniqueClick$ = DataStore.conversionReport.isShowUniqueClick.asObservable$;
  loading: boolean;


  constructor(private swal: SweetAlertService,
              private conversionReportService: ConversionReportService
  ) {
  }

  ngOnInit() {
    this.loading = true;
    this.conversionReportService.isShowUniqueClick().subscribe();
    this.filter = (new ConversionReportFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getStatistics(filter));
  }

  getStatistics(filter: ConversionReportFilter) {
    this.loading = true;
    this.conversionReportService.getAll(filter.filter)
    .pipe(finalize(() => this.loading = false))
    .subscribe();
  }

  calendarCallback({startDate, endDate}) {
    this.filter.from = startDate;
    this.filter.to = endDate;
  }

  applyFilters(filter: ConversionReportFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  changeReportStatus(reportId: string, status: number) {
    this.swal.confirm('Do you really want to change status?').then((isConfirm) => {
      if (isConfirm.value) {
        this.conversionReportService.changeStatus(reportId, status).subscribe((response) => {
          this.swal.success('Status successfully changed!');
        }, (err) => {
          this.swal.error('Ooops! Something went wrong!', err.errors[0]);
        });
      } else {
        this.swal.error('Cancelled');
      }
    });
  }

  exportCSV() {
    this.loading = true;
    this.conversionReportService.exportCSV(this.filter.filter)
    .pipe(finalize(() => this.loading = false))
    .subscribe((response) => {
      FileSaver.saveAs(response.body, `report-${Date.now()}.csv`, true);
    }, (err) => {
      this.swal.confirm('Report file size is too big to download', `Try to use 'Send to email'`).then(res => {
        if (res.value) {
          this.sendToEmail();
        }
      });
    });
  }

  sendToEmail() {
    this.loading = true;
    this.conversionReportService.sendToEmail(this.filter.filter)
    .pipe(finalize(() => this.loading = false))
    .subscribe(() => {
      this.swal.success('Complete');
    }, (err) => {
      this.swal.error(err.errors || 'Ooops! Something went wrong!');
    });
  }

}
