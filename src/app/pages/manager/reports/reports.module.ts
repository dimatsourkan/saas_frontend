import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../shared/shared.module';
import {
  ConverionReportsFilterComponent,
} from './conversion-reports/converion-reports-filter/converion-reports-filter.component';
import { ConversionReportsComponent } from './conversion-reports/conversion-reports.component';
import { REPORT_ROUTING } from './reports.routing';
import { StatisticsFilterComponent } from './statistics/statistics-filter/statistics-filter.component';
import { StatisticsComponent } from './statistics/statistics.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(REPORT_ROUTING)
  ],
  declarations: [ConversionReportsComponent,
    StatisticsComponent,
    StatisticsFilterComponent,
    ConverionReportsFilterComponent]
})
export class ReportsModule {
}
