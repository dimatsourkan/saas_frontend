import { Routes } from '@angular/router';

import { ConversionReportsComponent } from './conversion-reports/conversion-reports.component';
import { StatisticsComponent } from './statistics/statistics.component';

export const REPORT_ROUTING: Routes = [
  {
    path: 'statistics',
    component: StatisticsComponent,
    data: {
      name: 'titles.reports.statistics',
      menuList: 'reports',
      roles: ['ROLE_MANAGER']
    }
  },
  {
    path: 'conversion-reports',
    component: ConversionReportsComponent,
    data: {
      name: 'titles.reports.conversion',
      menuList: 'reports',
      roles: ['ROLE_MANAGER']
    }
  }

];
