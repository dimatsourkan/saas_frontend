import { Component, OnInit } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { StatisticsFilter } from '@app/core/entities/reports/statistics-reports/statistics-reports.filter';
import { StatisticsService } from '@app/core/entities/reports/statistics-reports/statistics-reports.service';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';

@Component({
  selector: 'app-statistics-filter',
  templateUrl: './statistics-filter.component.html',
  styleUrls: ['./statistics-filter.component.scss']
})
export class StatisticsFilterComponent extends AbstractFilterComponent<StatisticsFilter> implements OnInit {

  filter = (new StatisticsFilter()).init();
  trafficTypes$ = DataStore.statistic.trafficTypes.asObservable$;
  additionalFilterIsOpen: boolean;
  filterUrls = {
    advertisers: `/statistics/advertiser`,
    publishers: `/statistics/publisher`,
    managers: `/statistics/manager`
  };

  constructor(private statisticsService: StatisticsService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.statisticsService.getTrafficTypes().subscribe();
    this.filter.exclude(['page', 'limit', 'search', 'datetime_from', 'datetime_to']);
    DataStore.timezone.current.asObservable$.subscribe(timezone => {
      if (!this.filter.timezone) {
        this.filter.timezone = timezone;
      }
    });
  }

  toggleAdditionalFilter() {
    this.additionalFilterIsOpen = !this.additionalFilterIsOpen;
  }

}
