import { Component, OnInit } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { StatisticsFilter } from '@app/core/entities/reports/statistics-reports/statistics-reports.filter';
import { StatisticsService } from '@app/core/entities/reports/statistics-reports/statistics-reports.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as FileSaver from 'file-saver';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  subscription$: Subscription;
  filter: StatisticsFilter;
  statistics$ = DataStore.statistic.list.asObservable$;
  loading: boolean;

  constructor(
    private swal: SweetAlertService,
    private statisticsService: StatisticsService) {
  }

  ngOnInit() {
    this.filter = (new StatisticsFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getStatistics(filter));
  }

  getStatistics(filter: StatisticsFilter) {
    this.loading = true;
    this.statisticsService.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: StatisticsFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  calendarCallback({ startDate, endDate }) {
    this.filter.datetime_from = startDate;
    this.filter.datetime_to = endDate;
  }

  checkSecret(hash: string) {
    if (hash) {
      this.statisticsService.checkSercret(hash).subscribe((res: any) => {
        this.swal.info('Hash info', `
        Publisher id: ${res.publisherId}
        Aff sub id: ${res.affSubId}
        `);
      }, err => {
        this.swal.error(err.errors);
      });
    }
  }

  exportCSV() {
    this.loading = true;
    this.statisticsService.exportCSV(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe((response) => {
        FileSaver.saveAs(response.body, `report-${Date.now()}.csv`, true);
      }, (err) => {
        this.swal.confirm('Report file size is too big to download', `Try to use 'Send to email'`).then(res => {
          if (res.value) {
            this.sendToEmail();
          }
        });
      });
  }

  sendToEmail() {
    this.loading = true;
    this.statisticsService.sendToEmail(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => {
        this.swal.success('Complete');
      }, (err) => {
        this.swal.error(err.errors || 'Ooops! Something went wrong!');
      });
  }
}

