import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from '../../../../shared/services/language/language.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';

import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings-email',
  templateUrl: './settings-email.component.html',
  styleUrls: ['./settings-email.component.scss']
})
export class SettingsEmailComponent implements OnInit {
  mailSetting: any;
  loading: boolean;
  mailType: any;
  savedSettings: any = {};
  mailSettings: any;
  mailConnectionStatus: any;
  mailTypes: { id: number, value: string }[] = [];
  mailSettingsForm: FormGroup;

  constructor(private translate: TranslateService,
              private languageService: LanguageService,
              private _settingsService: SettingsService,
              private _fb: FormBuilder,
              private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.initForm();
    this.getMailTypes();
    this.getMailSettings();
    this.loading = false;
  }

  initForm() {
    this.mailSettingsForm = this._fb.group({
      type: [1],
      host: [],
      port: [],
      encryption: [],
      username: [],
      password: [],
      domain: [],
      apiKey: [],
      senderEmail: [],
      senderName: []
    }, {validator: this.validationByType});
  }

  validationByType(group: FormGroup) {
    const type = group.get('type').value;
    const type1 = [
      group.get('host'),
      group.get('port'),
      group.get('encryption'),
      group.get('username'),
      group.get('password')
    ];
    const type2 = [
      group.get('domain'),
      group.get('apiKey')
    ];
    const type3 = [
      group.get('senderEmail'),
      group.get('apiKey'),
      group.get('senderName')
    ];

    if (type === 1) {
      type1.map((item => item.setValidators([Validators.required])));
      return null;
    } else if (type === 2) {
      type2.map((item => item.setValidators([Validators.required])));
      return null;
    } else if (type === 3) {
      type3.map((item => item.setValidators([Validators.required])));
      return null;
    }
    return null;
  }

  getMailTypes() {
    return this._settingsService.getMailTypes()
      .subscribe(response => {
        this.mailTypes = [];
        for (const prop in response) {
          if (response.hasOwnProperty(prop)) {
            this.mailTypes.push({id: Number(prop), value: response[prop]});
          }
        }
      });
  }

  getMailSettings() {
    return this._settingsService.getMailSettings()
      .subscribe(response => {
        this.mailSettingsForm.patchValue(response.serviceData);
      });
  }

  saveMailSettings() {
    this.sweetAlertService.confirm('Save changes?')
      .then(isConfirmed => {
        if (isConfirmed.value) {
          this.putMailSettings();
        } else {
          this.sweetAlertService.error('Canceled');
        }
      });

  }

  putMailSettings() {
    this._settingsService.putMailSettings(this.mailSettingsForm.value)
      .subscribe(response => {
        this.savedSettings.serviceData = this.mailSettingsForm.value;
        this.sweetAlertService.success('Settings was successfully changed!');
      }, err => this.sweetAlertService.error('Something went wrong'));
  }

  checkConnection() {
    this.sweetAlertService.confirm('Check connection?')
      .then(isConfirmed => {
        if (isConfirmed.value) {
          this.postConnection();
        } else {
          this.sweetAlertService.error('Canceled');
        }
      });
  }

  postConnection() {
    this.loading = true;
    const message = {type: 'error', content: ''};
    this._settingsService.postConnection(this.mailSettingsForm.value)
      .subscribe((res) => {
        this.loading = false;
        this.mailConnectionStatus = res;
        if (res) {
          message.content = 'Connection is good';
          message.type = 'success';
        } else {
          message.content = 'Connection is bad';
        }
        this.sweetAlertService[message.type](message.content);
      }, err => {
        this.sweetAlertService[message.type](message.content);
        this.loading = true;
      });
  }

}
