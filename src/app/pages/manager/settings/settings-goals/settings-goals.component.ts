import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';

import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings-goals',
  templateUrl: './settings-goals.component.html',
  styleUrls: ['./settings-goals.component.scss']
})
export class SettingsGoalsComponent implements OnInit {

  @ViewChild('editModal') editModal: ModalComponent;
  loading: boolean;
  goals: any[] = [];
  manageGoalObj: { name: string; id?: string; postbackAlias?: string };
  errors: any;

  constructor(
    private _settingsService: SettingsService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.loading = true;
    this.getGoals();

  }

  getGoals() {
    return this._settingsService.getGoals().subscribe((response) => {
      this.loading = false;
      this.goals = response;
    });
  }

  stopPropagation(e) {
    e.stopPropagation();
    if (e.srcElement.id === 'submit') {
      this.manageGoal();
    }
  }

  toggleModalBox() {
    this.errors = {};
    this.editModal.toggle();
    this.errors = {};
  }

  createNewGoal() {
    this.manageGoalObj = {
      name: '',
      postbackAlias: ''
    };
    this.toggleModalBox();
  }

  updateCurrentGoal(goal) {
    this.manageGoalObj = goal;
    this.toggleModalBox();
  }

  manageGoal() {
    if (Object.prototype.hasOwnProperty.call(this.manageGoalObj, 'id')) {
      return this.updateGoal();
    } else {
      return this.createGoal();
    }
  }

  updateGoal() {
    return this._settingsService.updateGoal(this.manageGoalObj.id,
      { name: this.manageGoalObj.name, postbackAlias: this.manageGoalObj.postbackAlias }).subscribe((response) => {
        this.goals.forEach((goal) => {
          if (goal.id === this.manageGoalObj.id) {
            goal.name = this.manageGoalObj.name;
            goal.postbackAlias = this.manageGoalObj.postbackAlias;
          }
        });
        this.toggleModalBox();
        this.sweetAlertService.success('Done');
      }, (err) => {
        this.errors = err.errors;
        this.getGoals();
        this.sweetAlertService.error('Error', this.parseError(err.errors));
      });
  }

  createGoal() {
    return this._settingsService.createGoal(
      { name: this.manageGoalObj.name, postbackAlias: this.manageGoalObj.postbackAlias })
      .subscribe((response) => {
        this.goals.push({ ...this.manageGoalObj, ...response });
        this.toggleModalBox();
        this.sweetAlertService.success('Done');
      }, (err) => {
        this.errors = err.errors;
        this.getGoals();
        this.sweetAlertService.error('Error', this.parseError(err.errors));
      });
  }

  parseError(errors) {
    if (typeof errors === 'string') {
      return errors;
    }
    let errorsMsg = errors[Object.keys(errors)[0]];
    if (Array.isArray(errorsMsg)) {
      errorsMsg = errorsMsg[0];
    }
    return errorsMsg;
  }

}
