import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';

import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings-roles-manage',
  templateUrl: './settings-roles-manage.component.html',
  styleUrls: ['./settings-roles-manage.component.scss']
})
export class SettingsRolesManageComponent implements OnInit {
  roleID: any;
  role: { name: string; availablePermissions: any; permissions?: any[]; weight?: any; } = {
    name: '',
    availablePermissions: []
  };
  submit: boolean;
  errors: any = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _settingsService: SettingsService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.roleID = params['id'];
      if (this.roleID) {
        this.getRole(this.roleID);
      } else {
        this.getPermissions();
      }
    });
  }

  getPermissions() {
    return this._settingsService.getPermissions().subscribe((response) => {
      const availablePermissions = response.items;
      this.role = {
        name: '',
        availablePermissions
      };
    });
  }

  selectAll(group) {
    group.value.permissions.forEach(el => el.checked = group.value.checked);
  }

  getRole(roleID) {
    return this._settingsService.getRole(roleID).subscribe((response) => {
      this.role = response;

      // tslint:disable-next-line:forin
      for (const prop in this.role.permissions) {
        this.role.permissions[prop].permissions.map(val => this.role.availablePermissions[prop].permissions.forEach(el => {
          if (el.id === val.id) {
            el.checked = true;
          }
        }));
        const permissionsLength = this.role.availablePermissions[prop].permissions.length;
        const availablePermissionLength = this.role.availablePermissions[prop].permissions.filter(el => el.checked).length;
        this.role.availablePermissions[prop].checked = permissionsLength === availablePermissionLength;
      }
    });
  }

  calculateSelected(group) {
    return 'all';
  }

  manageRole() {
    this.submit = true;
    if (this.roleID) {
      this.updateRole();
    } else {
      this.createRole();
    }
  }

  updateRole() {
    const role = this.generateRoleObject();
    return this._settingsService.updateRole(this.roleID, role).subscribe((response) => {
      this.router.navigate(['/', 'manager', 'settings', 'roles']);
    }, (err) => {
      this.showErrors(err.errors);
    });
  }

  createRole() {
    const role = this.generateRoleObject();
    return this._settingsService.createRole(role).subscribe((response) => {
      this.router.navigate(['/', 'manager', 'settings', 'roles']);
    }, (err) => {
      this.showErrors(err.errors);
    });
  }

  showErrors(err) {
    this.errors = err;

    // TODO - Очень жесткий костыль который надо будет убрать при рефакторинге убрать
    setTimeout(() => {
      const errorField = document.querySelector('.createOffer__input_invalid');
      if (errorField) {
        errorField.querySelector('input').focus();
      }
    }, 100);
  }

  generateRoleObject() {
    const role = {
      name: this.role.name,
      permissions: [],
      weight: this.role.weight
    };
    // tslint:disable-next-line:forin
    for (const prop in this.role.availablePermissions) {
      const selectedId = this.role.availablePermissions[prop].permissions.filter(el => el.checked).map(item => item.id);
      role.permissions = [...role.permissions, ...selectedId];
    }
    return role;
  }

}
