import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';

import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings-roles',
  templateUrl: './settings-roles.component.html',
  styleUrls: ['./settings-roles.component.scss']
})
export class SettingsRolesComponent implements OnInit {
  roles: any;
  loading: boolean;

  constructor(
    private _settingsService: SettingsService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.loading = true;
    this.getRoles();
  }

  getRoles() {
    return this._settingsService.getRoles().subscribe((response) => {
      this.roles = response;
      this.loading = false;
    });
  }

  removeRole(id, name) {
    const message = `Do you really want to remove role - ${name}`;
    this.sweetAlertService.confirm(message).then((isConfirm) => {
      if (isConfirm.value) {
        this._settingsService.deleteRoles(id).subscribe((res) => {
          this.sweetAlertService.success('Done!');
        }, (err) => {
          this.sweetAlertService.error(err.errors[0]);
        });
      } else {
        this.sweetAlertService.error('Cancelled!');
      }
    });
  }

}
