import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { LanguageService } from '../../../../shared/services/language/language.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';

import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings-scheduler',
  templateUrl: './settings-scheduler.component.html',
  styleUrls: ['./settings-scheduler.component.scss']
})
export class SettingsSchedulerComponent implements OnInit {

  @ViewChild('argumentsModal') argumentsModal: ModalComponent;
  addSchedule: boolean;
  loading: boolean;
  newSchedule: {
    command: string;
    name: string;
    cronArray: any[];
    arguments: string;
    disabled: boolean;
    repeatAfterFail: boolean;
    id?: string;
    status?: number;
  };
  scheduleCommands: any;
  schedule: any;
  extendsArgumentsId: any;
  extendsArguments: { data: any; };

  constructor(private translate: TranslateService,
              private languageService: LanguageService,
              private _settingsService: SettingsService,
              private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.addSchedule = false;
    this.newSchedule = {
      command: '',
      name: '',
      cronArray: [],
      arguments: '',
      disabled: false,
      repeatAfterFail: false
    };

    forkJoin([this.getScheduleCommands(), this.getSchedule()])
      .subscribe(response => {
        this.dataReady(response[0], response[1]);
      });
  }

  getSchedule() {
    return this._settingsService.getSchedule();
  }

  getScheduleCommands() {
    return this._settingsService.getScheduleCommands();
  }

  dataReady(commands, list) {
    if (commands) {
      this.scheduleCommands = [];
      for (const prop in commands.items) {
        if (commands.items.hasOwnProperty(prop)) {
          this.scheduleCommands.push({id: prop});
        }
      }
    }
    this.schedule = list.items;
    this.schedule.forEach((obj) => {
      obj.cronArray = obj.cronExpression.split(' ');
    });
  }

  newScheduleRow() {
    this.addSchedule = !this.addSchedule;
  }

  clearNewSchedule() {
    this.newSchedule = {
      command: '',
      name: '',
      cronArray: [],
      arguments: '',
      disabled: false,
      repeatAfterFail: false
    };
    this.addSchedule = false;
  }

  saveNewSchedule() {
    this.convertCronExpression(this.newSchedule);
    this.newSchedule.repeatAfterFail = this.newSchedule.repeatAfterFail || false;
    this.newSchedule.disabled = this.newSchedule.disabled || false;
    this._settingsService.saveNewSchedule(this.newSchedule).subscribe((response) => {
      this.newSchedule.id = response.id;
      this.newSchedule.status = 2;
      this.schedule.push(this.newSchedule);
      this.clearNewSchedule();
    }, (err) => {
      this.showError(err.errors);
    });
  }


  updateSchedule(obj) {
    this.convertCronExpression(obj);
    const data = Object.keys(obj).reduce((acc, key) => {
      if (['cronArray', 'lastExecution', 'status', 'type'].includes(key)) {
        return acc;
      }
      return Object.assign(acc, {[key]: obj[key]});
    }, {});
    this.sweetAlertService.confirm('Are you sure?').then(isConfirmed => {
      if (isConfirmed.value) {
        this._settingsService.updateSchedule(data).subscribe((res) => {
          this.sweetAlertService.success('Success!');
        }, (err) => this.showError(err.errors));
      } else {
        this.sweetAlertService.error('Canceled!');
      }
    });
  }

  runSchedule(id) {
    this.sweetAlertService.confirm('Are you sure?').then(isConfirmed => {
      if (isConfirmed.value) {
        this._settingsService.runSchedule(id).subscribe(res => {
          this.sweetAlertService.success('Success!');
        }, (err) => this.showError(err.errors));
      } else {
        this.sweetAlertService.error('Canceled!');
      }
    });
  }

  deleteSchedule(id, index) {
    this.sweetAlertService.confirm('Are you sure?').then(isConfirmed => {
      if (isConfirmed.value) {
        this._settingsService.deleteSchedule(id).subscribe((res) => {
          this.sweetAlertService.success('Success!');
          this.schedule.splice(index, 1);
        }, err => this.showError(err.error));
      } else {
        this.sweetAlertService.error('Canceled!');
      }
    });
  }

  convertCronExpression(obj) {
    obj.cronArray.forEach((str, i) => {
      obj.cronArray[i] = str.replace(/\s+/g, '');
    });
    obj.cronExpression = obj.cronArray.join(' ');
  }

  openArgumentsModal(obj = null, id = null) {
    this.argumentsModal.open();
    this.extendsArgumentsId = id;
    const data = obj.split('data=');
    if (obj && data[1] !== '[]') {
      this.extendsArguments = {
        data: JSON.parse(data[1])
      };
    } else {
      this.extendsArguments = {data: [{publisherIds: [], advertiserIds: []}]};
    }
  }

  closeArgumentsModal() {
    this.argumentsModal.close();
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  updateArguments() {
    const allPubsAreSet = this.extendsArguments.data.filter(obj => !obj.publisherIds.length);
    if (allPubsAreSet.length) {
      this.sweetAlertService.error('Publishers are required');
    } else {
      const index = this.schedule.findIndex(obj => obj.id === this.extendsArgumentsId);
      this.schedule[index].arguments = `data=${JSON.stringify(this.extendsArguments.data)}`;
      this.closeArgumentsModal();
    }
  }

  addPairArguments() {
    this.extendsArguments.data.push({publisherIds: [], advertiserIds: []});
  }

  removePairArguments(i) {
    this.extendsArguments.data.splice(i, 1);
  }

  showError(error) {
    let err = '';
    if (error instanceof Array) {
      err = error.join();
    }
    if (typeof error === 'object') {
      for (const prop in error) {
        if (error.hasOwnProperty(prop)) {
          err += `\n${prop}: ${error[prop][0]}`;
        }
      }
    } else if (typeof error === 'string') {
      err = error;
    }
    this.sweetAlertService.error(err || 'Something went wrong!');
  }
}
