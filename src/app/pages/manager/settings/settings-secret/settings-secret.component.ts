import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';

import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings-secret',
  templateUrl: './settings-secret.component.html',
  styleUrls: ['./settings-secret.component.scss']
})
export class SettingsSecretComponent implements OnInit {
  loading: boolean;
  params: any;
  sercrets: { items: any[]; page?: any; limit?: any; totalPages?: any } = {items: []};
  scrollFlag: boolean;
  searchFilter: boolean;

  constructor(
    private _settingsService: SettingsService,
    private queryParamsService: QueryParamsService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.loading = true;
    this.params = this.queryParamsService.getParams();
    this.getSecrets(this.params).subscribe();
  }


  getSecrets(params) {
    return this._settingsService.getSecrets(params).pipe(tap((response) => {
      this.loading = false;
      this.sercrets = response;
      this.scrollFlag = !this.scrollFlag;

      this.params.page = this.sercrets.page;
      this.params.limit = this.sercrets.limit;
      this.queryParamsService.setParams(this.params);
    }));
  }

  toggleFilterMode(flag?) {
    this.searchFilter = flag === 'close' ? false : !this.searchFilter;
  }

  applyFilters() {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = this.sercrets.limit;
    this.params.aff_subs = (this.params.aff_subs) ? [this.params.aff_subs] : '';
    this.params.hashes = (this.params.hashes) ? [this.params.hashes] : '';
    this.getSecrets(this.params).subscribe();
    this.toggleFilterMode('close');
  }

  resetFilters() {
    this.loading = true;
    this.toggleFilterMode('close');
    this.params.pub_ids = null;
    this.params.aff_subs = null;
    this.params.hashes = null;
    this.getSecrets(this.params).subscribe();
  }

  goToPage(page) {
    this.loading = true;
    this.params.page = page;
    this.getSecrets(this.params).subscribe();
  }

  tableLimit(number) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = number;
    this.getSecrets(this.params).subscribe();
  }

  removeSecret(id, index) {
    const message = `Do you really want to remove secret - ${id}`;
    this.sweetAlertService.confirm(message).then((isConfirm) => {
      if (isConfirm.value) {
        this._settingsService.removeSecret({ids: [id]}).subscribe((res) => {
          this.sercrets.items.splice(index, 1);
          this.sweetAlertService.success('Done!');
        }, err => this.sweetAlertService.error(err.errors));
      } else {
        this.sweetAlertService.error('Cancelled!');
      }
    });
  }

}
