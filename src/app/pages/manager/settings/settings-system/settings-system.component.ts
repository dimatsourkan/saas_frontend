import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { HeaderService } from '../../../../core/components/header/header.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';

import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings-system',
  templateUrl: './settings-system.component.html',
  styleUrls: ['./settings-system.component.scss']
})
export class SettingsSystemComponent implements OnInit {
  settingsTypes: ({ id: number; typeName: string; step: string; } | { id: number; typeName: string; step?: undefined; })[];
  settings: any = { items: [] };
  today = moment();
  loading: boolean;
  errors: any = {};

  constructor(private translate: TranslateService,
    private _headerService: HeaderService,
    private _settingsService: SettingsService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.getSettings();
    this.settingsTypes = [
      { id: 1, typeName: 'number', step: '1' },
      { id: 2, typeName: 'number', step: '0.01' },
      { id: 3, typeName: 'text' },
      { id: 4, typeName: 'password' },
      { id: 5, typeName: 'date' },
      { id: 6, typeName: 'checkbox' },
      { id: 7, typeName: 'select' },
      { id: 10, typeName: 'email' },
      { id: 11, typeName: 'multiSelect' },
      { id: 12, typeName: 'datetime' },
    ];

  }

  uploadLogo(file, item, isFavicon) {
    this._settingsService.uploadImage(file, isFavicon).subscribe((response) => {
      item.value = response;
      this.loading = false;
      this.emitChangeLogos(isFavicon);
      this.sweetAlertService.success('Done!');
    }, err => {
      this.sweetAlertService.error('Cancelled!');
      this.loading = false;
      return;
    });
  }

  setLogo(file, item, isFavicon) {
    this.loading = true;
    if (item.value) {
      this.translate.get('core.sureRewriteCompanyLogo').subscribe((translation) => {
        this.sweetAlertService.confirm(translation).then((isConfirm) => {
          if (isConfirm.value) {
            this.uploadLogo(file, item, isFavicon);
          }
        });
      });
    } else {
      this.uploadLogo(file, item, isFavicon);
    }
  }

  removeLogo(item, isFavicon) {
    this.loading = true;
    this.translate.get('core.sureRemoveCompanyLogo').subscribe((translation) => {
      this.sweetAlertService.confirm(translation).then((isConfirm) => {
        if (isConfirm.value) {
          this._settingsService.removeLogo(isFavicon).subscribe((response) => {
            this.loading = false;
            item.value = '';
            this.emitChangeLogos(isFavicon);
            this.sweetAlertService.success('Done!');
          }, err => {
            this.sweetAlertService.error('Cancelled!');
            this.loading = false;
            return;
          });
        }
      });
    });
  }

  emitChangeLogos(isFavicon) {
    if (isFavicon) {
      this._headerService.getFavicon();
    } else {
      this._headerService.getLogo();
    }
  }

  getSettings() {
    return this._settingsService.getSettings().subscribe((response) => {
      this.settings = response;
      this.settings.items.forEach((form) => {
        form.settings.forEach((item) => {
          this.settingsTypes.forEach((type) => {
            if (item.type === type.id) {
              item.typeValue = type.typeName;
              item.typeExtra = type.step || null;
            }

          });
          if (item.typeValue === 'number') {
            item.value = Number(item.value);
          } else if (item.type === 11) {
            item.value = JSON.parse(item.value);
          }
        });
      });
      console.log('settings', this.settings);
    });
  }

  saveSettings(form) {
    const arr = {
      data: form.settings.map((item) => {
        const obj = {
          id: item.id,
          value: item.value
        };
        if (item.type === 11) {
          obj.value = JSON.stringify(obj.value);
        }
        return obj;
      })
    };

    return this._settingsService.saveSettings(arr).subscribe(() => {
      this.errors = {};
      this.sweetAlertService.success('Save');
    }, err => {
      this.errors = err.errors;
      this.sweetAlertService.error('Has some error');
    });
  }

  calendarCallback(date, key) {
    this[key] = date.date;
  }

  getPermissions(name) {
    return name === 'Company settings' ? ['edit_admin_settings'] : [];
  }

}
