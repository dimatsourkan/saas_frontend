import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { ValidatorService } from '../../../../shared/components/validation/validation.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';

import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings-tags',
  templateUrl: './settings-tags.component.html',
  styleUrls: ['./settings-tags.component.scss']
})
export class SettingsTagsComponent implements OnInit {

  @ViewChild('editModal') editModal: ModalComponent;
  loading: boolean;
  tags: any;
  errors: any;

  form: FormGroup;


  constructor(
    private _settingsService: SettingsService,
    private sweetAlertService: SweetAlertService,
    private validatorService: ValidatorService) {
    this.form = new FormGroup({
      name: new FormControl(''),
      id: new FormControl(''),
    });
  }

  ngOnInit() {
    this.loading = true;
    this.getTags();
  }


  getTags() {
    return this._settingsService.getTags().subscribe((response) => {
      this.loading = false;
      this.tags = response;
    });
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  toggleModalBox() {
    this.editModal.toggle();
  }

  createNewTag() {
    this.toggleModalBox();
    this.form.patchValue({
      name: '',
      id: null
    });
  }

  updateCurrentTag(tag) {
    this.toggleModalBox();
    this.form.patchValue(tag);
  }

  manageTag() {
    if (this.form.value.id) {
      return this.updateTag();
    } else {
      return this.createTag();
    }
  }

  updateTag() {
    const data = this.form.value;
    return this._settingsService.updateTag(data.id, data).subscribe(() => {
      this.tags.forEach((tag) => {
        if (tag.id === data.id) {
          tag.name = data.name;
        }
      });
      this.toggleModalBox();
      this.sweetAlertService.success('Done');
    }, err => {
      this.validatorService.setTouchToControls(this.form);
      this.validatorService.addErrorToForm(this.form, err);
    });
  }

  createTag() {
    return this._settingsService.createTag(this.form.value).subscribe((result) => {
      this.tags.push({
        name: this.form.value.name,
        id: result.id
      });
      this.toggleModalBox();
      this.sweetAlertService.success('Done');
    }, err => {
      this.validatorService.setTouchToControls(this.form);
      this.validatorService.addErrorToForm(this.form, err);
    });
  }

}
