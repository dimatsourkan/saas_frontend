import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { SettingsEmailComponent } from './settings-email/settings-email.component';
import { SettingsGoalsComponent } from './settings-goals/settings-goals.component';
import { SettingsRestrictionsComponent } from './settings-restrictions/settings-restrictions.component';
import { SettingsRolesManageComponent } from './settings-roles-manage/settings-roles-manage.component';
import { SettingsRolesComponent } from './settings-roles/settings-roles.component';
import { SettingsSchedulerComponent } from './settings-scheduler/settings-scheduler.component';
import { SettingsSecretComponent } from './settings-secret/settings-secret.component';
import { SettingsSystemComponent } from './settings-system/settings-system.component';
import { SettingsTagsComponent } from './settings-tags/settings-tags.component';
import { SettingsComponent } from './settings.component';

import { SETTINGS_ROUTING } from './settings.routing';
import { SettingsService } from './settings.service';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(SETTINGS_ROUTING)
  ],
  declarations: [
    SettingsComponent,
    SettingsEmailComponent,
    SettingsSchedulerComponent,
    SettingsSystemComponent,
    SettingsRolesComponent,
    SettingsGoalsComponent,
    SettingsTagsComponent,
    SettingsRestrictionsComponent,
    SettingsSecretComponent,
    SettingsRolesManageComponent
  ],
  providers: [
    SettingsService
  ]
})
export class SettingsModule {
}
