import { Routes } from '@angular/router';
import { SettingsEmailComponent } from './settings-email/settings-email.component';
import { SettingsGoalsComponent } from './settings-goals/settings-goals.component';
import { SettingsRestrictionsComponent } from './settings-restrictions/settings-restrictions.component';
import { SettingsRolesManageComponent } from './settings-roles-manage/settings-roles-manage.component';
import { SettingsRolesComponent } from './settings-roles/settings-roles.component';
import { SettingsSchedulerComponent } from './settings-scheduler/settings-scheduler.component';
import { SettingsSecretComponent } from './settings-secret/settings-secret.component';
import { SettingsSystemComponent } from './settings-system/settings-system.component';
import { SettingsTagsComponent } from './settings-tags/settings-tags.component';


export const SETTINGS_ROUTING: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'system'},
  {
    path: 'system', component: SettingsSystemComponent,
    data: {
      name: 'titles.settings.core',
      menuList: 'settings',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'email', component: SettingsEmailComponent,
    data: {
      name: 'titles.settings.mail',
      menuList: 'settings',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'scheduler', component: SettingsSchedulerComponent,
    data: {
      name: 'titles.settings.schedule',
      menuList: 'settings',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'roles', children: [
      {
        path: '', component: SettingsRolesComponent,
        data: {
          name: 'titles.settings.roles',
          menuList: 'settings',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'create', component: SettingsRolesManageComponent,
        data: {
          name: 'titles.settings.create-role',
          menuList: 'settings',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'edit/:id', component: SettingsRolesManageComponent,
        data: {
          name: 'titles.settings.edit-role',
          menuList: 'settings',
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      }
    ]
  },
  {
    path: 'goals', component: SettingsGoalsComponent,
    data: {
      name: 'titles.settings.goals',
      menuList: 'settings',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'tags', component: SettingsTagsComponent,
    data: {
      name: 'titles.settings.tags',
      menuList: 'settings',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'restrictions', component: SettingsRestrictionsComponent,
    data: {
      name: 'titles.settings.restrictions',
      menuList: 'settings',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'secrets', component: SettingsSecretComponent,
    data: {
      name: 'titles.settings.hash',
      menuList: 'settings',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }

];
