import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../shared/services/queryParams/query-params.service';

@Injectable({
  providedIn: 'root'
})

export class SettingsService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getSchedule(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/scheduler`, {headers: {toCamelCase: 'true'}});
  }

  getScheduleCommands(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/scheduler/commands`, {headers: {toCamelCase: 'true'}});
  }

  saveNewSchedule(newSchedule): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/scheduler`, newSchedule, {headers: {toCamelCase: 'true'}});
  }

  updateSchedule(schedule): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/scheduler/${schedule.id}`, schedule, {headers: {toCamelCase: 'true'}});
  }

  uploadImage(content, isFavicon): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/public_storage/${isFavicon ? 'favicon' : 'logo'}`, {content},
      {headers: {toCamelCase: 'true'}});
  }

  removeLogo(isFavicon): Observable<any> {
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/public_storage/${isFavicon ? 'favicon' : 'logo'}`,
      {headers: {toCamelCase: 'true'}});
  }

  deleteSchedule(id): Observable<any> {
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/scheduler/${id}`, {headers: {toCamelCase: 'true'}});
  }

  runSchedule(id): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/scheduler/${id}/run`, {id}, {headers: {toCamelCase: 'true'}});
  }

  getMailTypes(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/settings/mail_settings/types`, {headers: {toCamelCase: 'true'}});
  }

  getMailSettings(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/settings/mail_settings`, {headers: {toCamelCase: 'true'}});
  }

  postConnection(mailSettings): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/settings/mail_settings/connection`, mailSettings,
      {headers: {toCamelCase: 'true'}});
  }

  putMailSettings(mailSettings): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/settings/mail_settings`, mailSettings, {headers: {toCamelCase: 'true'}});
  }

  getSettings(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/settings`, {headers: {toCamelCase: 'true'}});
  }

  saveSettings(arr): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/settings/bulk`, arr, {headers: {toCamelCase: 'true'}});
  }

  getRoles(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/security/roles`, {headers: {toCamelCase: 'true'}});
  }

  deleteRoles(id): Observable<any> {
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/security/roles/${id}`, {headers: {toCamelCase: 'true'}});
  }

  getPermissions(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/security/permissions`, {headers: {toCamelCase: 'true'}});
  }

  getRole(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/security/roles/${id}`, {headers: {toCamelCase: 'true'}});
  }

  createRole(role): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/security/roles`, role, {headers: {toCamelCase: 'true'}});
  }

  updateRole(id, role): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/security/roles/${id}`, role, {headers: {toCamelCase: 'true'}});
  }

  getGoals(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/goal`, {headers: {toCamelCase: 'true'}});
  }

  createGoal(goal): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/goal`, goal, {headers: {toCamelCase: 'true'}});
  }

  updateGoal(id, goal): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/goal/${id}`, goal, {headers: {toCamelCase: 'true'}});
  }

  getTags(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/tags`, {headers: {toCamelCase: 'true'}});
  }

  createTag(goal): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/tags/create`, goal, {headers: {toCamelCase: 'true'}});
  }

  updateTag(id, tag): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/tags/update/${id}`, tag, {headers: {toCamelCase: 'true'}});
  }

  getRestrictions(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/restrictions`, {headers: {toCamelCase: 'true'}});
  }

  createRestriction(goal): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/restrictions`, goal, {headers: {toCamelCase: 'true'}});
  }

  updateRestriction(id, tag): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/restrictions/${id}`, tag, {headers: {toCamelCase: 'true'}});
  }

  getSecrets(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/settings/secrets${params}`, {headers: {toCamelCase: 'true'}});
  }

  removeSecret(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/settings/secrets${params}`, {headers: {toCamelCase: 'true'}});
  }

}
