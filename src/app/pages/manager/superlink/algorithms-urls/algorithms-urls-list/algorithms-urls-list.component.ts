import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AlgorithmUrlService } from '@app/core/entities/superlink/algorithms-urls/algorithms-urls.service';
import { DataStore } from '@app/core/entities/data-store.service';
import { AlgorithmsUrlsFilter } from '@app/core/entities/superlink/algorithms-urls/algorithms-urls.filter';


@Component({
  selector: 'app-algorithms-urls-list',
  templateUrl: './algorithms-urls-list.component.html',
  styleUrls: ['./algorithms-urls-list.component.scss']
})
export class AlgorithmsUrlsListComponent implements OnInit {
  loading: boolean;
  subscription$: Subscription;
  filter: AlgorithmsUrlsFilter;
  algorithmsUrls$ = DataStore.superlinkAlgorithmsUrls.list.asObservable$;

  constructor(private _algorithmsUrlsService: AlgorithmUrlService) {
  }

  ngOnInit() {
    this.filter = (new AlgorithmsUrlsFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getAlgorithmsUrls(filter));
  }

  getAlgorithmsUrls(filter: AlgorithmsUrlsFilter) {
    this.loading = true;
    this._algorithmsUrlsService.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

}
