import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { AlgorithmUrl } from '@app/core/entities/superlink/algorithms-urls/algorithms-urls.model';
import { DataStore } from '@app/core/entities/data-store.service';
import { AlgorithmUrlService } from '@app/core/entities/superlink/algorithms-urls/algorithms-urls.service';
import { patchFormValues } from '@app/core/helpers/helpers';
import { ValidatorService } from '@app/shared/components/validation/validation.service';


@Component({
  selector: 'app-algorithms-urls-manage',
  templateUrl: './algorithms-urls-manage.component.html',
  styleUrls: ['./algorithms-urls-manage.component.scss']
})
export class AlgorithmsUrlsManageComponent implements OnInit {

  algorithmUrl = new AlgorithmUrl();
  algorithmUrlForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  errors: any;
  contentTypeList$ = DataStore.superlinkAlgorithmsUrls.contentTypeList.asObservable$;
  dynamicTypeList$ = DataStore.superlinkAlgorithmsUrls.dynamicTypeList.asObservable$;
  distributionList$ = DataStore.superlinkAlgorithmsUrls.distributionList.asObservable$;
  monetizationList$ = DataStore.superlinkAlgorithmsUrls.monetizationList.asObservable$;
  trafficTypeList$ = DataStore.superlinkAlgorithmsUrls.trafficTypeList.asObservable$;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private validatorService: ValidatorService,
    private _algorithmsUrlsService: AlgorithmUrlService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this._algorithmsUrlsService.getDistributionList().subscribe();
    this._algorithmsUrlsService.getContentTypeList().subscribe();
    this._algorithmsUrlsService.getDynamicTypeList().subscribe();
    this._algorithmsUrlsService.getMonetizationList().subscribe();
    this._algorithmsUrlsService.getTrafficTypeList().subscribe();
    this.initForm();
    this.activatedRoute.data.subscribe(res => {
      this.algorithmUrl = res.url ? res.url : new AlgorithmUrl();
      this.patchFormValue();
    });
  }

  private patchFormValue() {
    patchFormValues(this.algorithmUrlForm, this.algorithmUrl.toJson());
  }

  initForm() {
    this.algorithmUrlForm = this._fb.group({
      advertiser: this._fb.group({
        id: [, Validators.required]
      }),
      name: [, Validators.required],
      url: [, Validators.required],
      distribution: [, Validators.required],
      dynamicType: [, Validators.required],
      contentType: [, Validators.required],
      trafficType: [, Validators.required],
      monetizationModel: [, Validators.required]
    });
  }

  submitUrlForm() {
    if (this.algorithmUrlForm.invalid) {
      return this.validatorService.setTouchToControls(this.algorithmUrlForm);
    }

    this.submitMethod(this.algorithmUrl.update(this.algorithmUrlForm.getRawValue())).subscribe(
      res => this.router.navigate(['/manager/superlink/algorithms-urls/list']),
      err => {
        this.validatorService.addErrorToForm(this.algorithmUrlForm, err);
        this.errors = err.errors;
        if (Object.prototype.hasOwnProperty.call(this.errors, 'publisherGroups') && this.errors.publisherGroups.length) {
          const counts = {};
          this.errors.publisherGroups.forEach((x) => {
            counts[x.id] = (counts[x.id] || 0) + 1;
          });
          this.sweetAlertService.error(err.errors.message,
            `Group ID${Object.keys(counts).length ? 's' : ''}: ${Object.keys(counts).reduce((str, key) => str + `${key} (${counts[key]}) `,
              '')}`);
        }
      });
  }

  private submitMethod(algorithmUrl: AlgorithmUrl) {
    return this.algorithmUrl.id ?
      this._algorithmsUrlsService.update(this.algorithmUrl.id, algorithmUrl) :
      this._algorithmsUrlsService.save(algorithmUrl);
  }

}
