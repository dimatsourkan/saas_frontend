import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';
import { AlgorithmsUrlsListComponent } from './algorithms-urls-list/algorithms-urls-list.component';
import { AlgorithmsUrlsManageComponent } from './algorithms-urls-manage/algorithms-urls-manage.component';

import { ALGORITHMS_URLS_ROUTING } from './algorithms-urls.routing';
import { AlgorithmsUrlsResolver } from './algorithms-urls.resolver';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ALGORITHMS_URLS_ROUTING),
    SharedModule
  ],
  providers: [AlgorithmsUrlsResolver],
  declarations: [AlgorithmsUrlsListComponent, AlgorithmsUrlsManageComponent]
})
export class AlgorithmsUrlsModule {
}
