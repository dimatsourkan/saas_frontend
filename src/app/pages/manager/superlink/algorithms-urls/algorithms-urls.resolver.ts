import { Injectable } from '@angular/core';
import { BaseItemResolver } from '@app/core/services/resolver/base-item.resolver';
import { AlgorithmUrl } from '@app/core/entities/superlink/algorithms-urls/algorithms-urls.model';
import { AlgorithmUrlService } from '@app/core/entities/superlink/algorithms-urls/algorithms-urls.service';

@Injectable()
export class AlgorithmsUrlsResolver extends BaseItemResolver<AlgorithmUrl> {

  protected service: AlgorithmUrlService = this.injector.get(AlgorithmUrlService, null);

  protected onError(error: any) {
    this.router.navigate(['/manager/superlink/algorithms-urls/list']);
    this.hideLoader();
  }

}
