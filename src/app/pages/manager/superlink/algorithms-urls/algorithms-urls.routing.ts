import { Routes } from '@angular/router';
import { AlgorithmsUrlsListComponent } from './algorithms-urls-list/algorithms-urls-list.component';
import { AlgorithmsUrlsManageComponent } from './algorithms-urls-manage/algorithms-urls-manage.component';
import { AlgorithmsUrlsResolver } from './algorithms-urls.resolver';

export const ALGORITHMS_URLS_ROUTING: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'list'},
  {
    path: 'list', component: AlgorithmsUrlsListComponent,
    data: {
      name: 'titles.superlink.alg-urls.list',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'create', component: AlgorithmsUrlsManageComponent,
    data: {
      name: 'titles.superlink.alg-urls.manage',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'edit/:id', component: AlgorithmsUrlsManageComponent,
    resolve: {
      url: AlgorithmsUrlsResolver
    },
    data: {
      name: 'titles.superlink.alg-urls.manage',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }
];
