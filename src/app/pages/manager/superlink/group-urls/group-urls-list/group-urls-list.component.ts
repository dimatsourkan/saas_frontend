import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { GroupUrlService } from '@app/core/entities/superlink/group-urls/group-urls.service';
import { GroupUrlsFilter } from '@app/core/entities/superlink/group-urls/group-urls.filter';
import { DataStore } from '@app/core/entities/data-store.service';


@Component({
  selector: 'app-group-urls-list',
  templateUrl: './group-urls-list.component.html',
  styleUrls: ['./group-urls-list.component.scss']
})
export class GroupUrlsListComponent implements OnInit {

  subscription$: Subscription;
  filter: GroupUrlsFilter;
  groupUrls$ = DataStore.superlinkGroupUrls.list.asObservable$;
  loading: boolean;

  constructor(
    private _groupUrlsService: GroupUrlService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.filter = (new GroupUrlsFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getGroupUrls(filter));
  }

  getGroupUrls(filter: GroupUrlsFilter) {
    this.loading = true;
    this._groupUrlsService.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  delete(id) {
    this.sweetAlertService.confirm('Do you want to remove link?').then((isConfirm) => {
      if (isConfirm.value) {
        this._groupUrlsService.delete(id).subscribe();
      }
    });
  }

}
