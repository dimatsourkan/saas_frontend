import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';

import { patchFormValues } from '@app/core/helpers/helpers';

import { GroupUrlService } from '@app/core/entities/superlink/group-urls/group-urls.service';
import { AlgorithmUrlService } from '@app/core/entities/superlink/algorithms-urls/algorithms-urls.service';
import { GroupUrl } from '@app/core/entities/superlink/group-urls/group-urls.model';
import { DataStore } from '@app/core/entities/data-store.service';
import { ValidatorService } from '@app/shared/components/validation/validation.service';

@Component({
  selector: 'app-group-urls-manage',
  templateUrl: './group-urls-manage.component.html',
  styleUrls: ['./group-urls-manage.component.scss']
})
export class GroupUrlsManageComponent implements OnInit {

  groupUrl = new GroupUrl();
  form: FormGroup;
  submitted: boolean;
  loading: boolean;
  errors: any;
  contentTypeList$ = DataStore.superlinkAlgorithmsUrls.contentTypeList.asObservable$;
  dynamicTypeList$ = DataStore.superlinkAlgorithmsUrls.dynamicTypeList.asObservable$;
  distributionList$ = DataStore.superlinkAlgorithmsUrls.distributionList.asObservable$;
  monetizationList$ = DataStore.superlinkAlgorithmsUrls.monetizationList.asObservable$;
  trafficTypeList$ = DataStore.superlinkAlgorithmsUrls.trafficTypeList.asObservable$;
  pubID: any;

  constructor(
    private _groupUrlsService: GroupUrlService,
    private validatorService: ValidatorService,
    private _algorithmsUrlsService: AlgorithmUrlService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.initForm();
    this._algorithmsUrlsService.getDistributionList().subscribe();
    this._algorithmsUrlsService.getContentTypeList().subscribe();
    this._algorithmsUrlsService.getDynamicTypeList().subscribe();
    this._algorithmsUrlsService.getMonetizationList().subscribe();
    this._algorithmsUrlsService.getTrafficTypeList().subscribe();
    this.activatedRoute.data.subscribe(res => {
      if (res.title === 'create') {
        this.groupUrl = new GroupUrl();
        this.groupUrl.publisher.id = this.activatedRoute.snapshot.params['publisherId'] || null;
      } else if (res.title === 'edit') {
        this.groupUrl = res.url;
        this.pubID = this.activatedRoute.snapshot.params['publisherId'];
      }
      this.patchFormValue();
    });
  }

  initForm() {
    this.form = this._fb.group({
      publisher: this._fb.group({
        id: [, Validators.required]
      }),
      name: [, Validators.required],
      distribution: [, Validators.required],
      dynamicType: [, Validators.required],
      contentType: [, Validators.required],
      trafficType: [, Validators.required],
      monetizationModel: [, Validators.required]
    });
  }

  private patchFormValue() {
    patchFormValues(this.form, this.groupUrl.toJson());
  }

  submitUrlForm() {

    if (this.form.invalid) {
      this.validatorService.setTouchToControls(this.form);
      return;
    }

    this.submitMethod(this.groupUrl.update(this.form.getRawValue())).subscribe(
      res => {
        this.router.navigate(['/', 'manager', 'superlink', 'group-urls', 'mode', res.id]);
      },
      err => {
        if (typeof err.errors === 'string') {
          this.sweetAlertService.error(err.errors);
        }
        this.validatorService.addErrorToForm(this.form, err);
      });
  }

  private submitMethod(groupUrl: GroupUrl) {
    return this.groupUrl.id ?
      this._groupUrlsService.update(this.groupUrl.id, groupUrl) :
      this._groupUrlsService.save(groupUrl);
  }

  backToList() {
    if (this.groupUrl.publisherId) {
      this.router.navigate(['/', 'manager', 'superlink', 'group-urls', 'mode', this.groupUrl.publisherId]);
    } else {
      this.router.navigate(['/', 'manager', 'superlink', 'group-urls']);
    }
  }
}
