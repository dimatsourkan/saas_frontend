import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { GroupUrlsService } from '../group-urls.service';


@Component({
  selector: 'app-group-urls-mode',
  templateUrl: './group-urls-mode.component.html',
  styleUrls: ['./group-urls-mode.component.scss']
})
export class GroupUrlsModeComponent implements OnInit {
  loading: boolean;
  countries: any;
  modeList: any;
  groupID: any;
  pubID: any;
  mode: any = { mode: 1 };
  errors: any;
  submitted: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _groupUrlsService: GroupUrlsService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.getModeList();
    this.route.params.subscribe((params) => {
      this.groupID = params['id'];

      if (this.groupID) {
        this.getModeGroupUrl();
      } else {
        this.backToPublisherList();
      }
    });

  }

  getModeList() {
    return this._groupUrlsService.getModeList().subscribe((response) => {
      this.modeList = response;
    });
  }

  getModeGroupUrl() {
    this.loading = true;
    return this._groupUrlsService.getPublisherGroup(this.groupID).subscribe((response) => {
      this.mode = response;
      this.loading = false;
      this.extendData();
    }, err => {
    });
  }

  submitModeForm() {
    switch (this.mode.mode) {
      case 1:
        this.setDynamicMode();
        break;
      case 2:
        this.setManualMode();
        break;
      case 3:
        this.setManualCountryMode();
        break;
      default:
        return false;
    }
  }

  extendData() {
    if (this.mode.urls.length) {
      if (this.mode.allowUrls.length !== this.mode.urls.length) {
        this.mode.urls = this.merge(this.mode.allowUrls, this.mode.urls, 'publisherResaleUrlId');
      }
      this.mode.urls.forEach((url) => {
        this.mode.allowUrls.forEach((allowUrl) => {
          if (url.publisherResaleUrlId === allowUrl.publisherResaleUrlId) {
            url.name = allowUrl.name;
            url.percent = url.percent || 0;
          }
        });
      });
    } else if (!this.mode.urls.length) {
      this.mode.allowUrls.forEach((url) => {
        const obj = Object.assign({}, url);
        obj.percent = 0;
        this.mode.urls.push(obj);
      });
    }

    if (this.mode.groupCountryUrls.length) {
      if (this.mode.allowUrls.length !== this.mode.groupCountryUrls[0].urls.length) {
        this.mode.groupCountryUrls.forEach((groupCountryUrl) => {
          groupCountryUrl.urls = this.merge(this.mode.allowUrls, groupCountryUrl.urls, 'publisherResaleUrlId');
        });
      }
      this.mode.groupCountryUrls.forEach((groupCountryUrl) => {
        groupCountryUrl.urls.forEach((url) => {
          this.mode.allowUrls.forEach((allowUrl) => {
            if (url.publisherResaleUrlId === allowUrl.publisherResaleUrlId) {
              url.name = allowUrl.name;
              url.percent = url.percent || 0;
            }
          });
        });
      });
    } else if (!this.mode.groupCountryUrls.length) {
      const obj = {
        countryCode: null,
        urls: []
      };
      this.mode.allowUrls.forEach((url) => {
        const urlData = Object.assign({}, url);
        urlData.percent = 0;
        obj.urls.push(urlData);
      });
      this.mode.groupCountryUrls.push(obj);
    }
  }

  merge(a, b, prop) {
    const reduced = a.filter(aItem => !b.find(bItem => aItem[prop] === bItem[prop]));
    return reduced.concat(b);
  }

  addNewCountryAlgorithm() {
    const obj = {
      countryCode: null,
      urls: []
    };
    this.mode.allowUrls.forEach((url) => {
      const urlData = Object.assign({}, url);
      urlData.percent = 0;
      obj.urls.push(urlData);
    });
    this.mode.groupCountryUrls.push(obj);
  }

  removeNewCountryAlgorithm(index) {
    this.mode.groupCountryUrls.splice(index, 1);
  }

  setDynamicMode() {
    return this._groupUrlsService.setDynamicMode(this.groupID, this.pubID)
      .subscribe(() => {
        this.backToPublisherList();
      }, (err) => {
        this.sweetAlertService.error(err.errors.urls);
      });
  }

  setManualMode() {
    const data = this.mode.urls.map(url => ({
      publisherResaleUrlId: url.publisherResaleUrlId,
      percent: Number(url.percent)
    }));
    return this._groupUrlsService.setManualMode(this.groupID, this.pubID, data).subscribe(() => {
      this.backToPublisherList();
    }, (err) => {
      this.sweetAlertService.error(err.errors.urls);
    });
  }

  setManualCountryMode() {
    this.submitted = true;
    const data = this.mode.groupCountryUrls.map(countryUrl => ({
      countryCode: countryUrl.countryCode,
      urls: countryUrl.urls.map(url => ({
        publisherResaleUrlId: url.publisherResaleUrlId,
        percent: Number(url.percent)
      }))
    }));
    return this._groupUrlsService.setManualCountryMode(this.groupID, this.pubID, data).subscribe((response) => {
      this.backToPublisherList();
      this.submitted = false;
    }, (err) => {
      this.errors = err.errors;
      if (err.errors.urls && typeof err.errors.urls === 'string') {
        this.sweetAlertService.error(err.errors.urls);
      }
    });
  }

  onPercentChange(model, flag) {
    if (model[flag] > 100) {
      model[flag] = 100;
    } else if (model[flag] < 0) {
      model[flag] = 0;
    } else {
      if (model[flag]) {
        model[flag] = Number(model[flag].toFixed(2));
      } else {
        model[flag] = model[flag];
      }
    }
  }

  backToPublisherList() {
    this.router.navigate(['/', 'manager', 'superlink', 'group-urls', 'list'], {
      queryParams: { publisherId: this.pubID }
    });
  }
}
