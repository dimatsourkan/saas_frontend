import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';
import { GroupUrlsListComponent } from './group-urls-list/group-urls-list.component';
import { GroupUrlsManageComponent } from './group-urls-manage/group-urls-manage.component';
import { GroupUrlsModeComponent } from './group-urls-mode/group-urls-mode.component';

import { GROUP_URLS_ROUTING } from './group-urls.routing';
import { GroupUrlsService } from './group-urls.service';
import { GroupUrlsResolver } from './group-urls.resolver';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GROUP_URLS_ROUTING),
    SharedModule
  ],
  providers: [GroupUrlsService, GroupUrlsResolver],
  declarations: [GroupUrlsListComponent, GroupUrlsManageComponent, GroupUrlsModeComponent]
})
export class GroupUrlsModule {
}
