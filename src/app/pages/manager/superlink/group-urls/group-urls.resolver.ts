import { Injectable } from '@angular/core';
import { BaseItemResolver } from '@app/core/services/resolver/base-item.resolver';
import { GroupUrl } from '@app/core/entities/superlink/group-urls/group-urls.model';
import { GroupUrlService } from '@app/core/entities/superlink/group-urls/group-urls.service';

@Injectable()
export class GroupUrlsResolver extends BaseItemResolver<GroupUrl> {

  protected service: GroupUrlService = this.injector.get(GroupUrlService, null);

  protected onError(error: any) {
    this.router.navigate(['/manager/superlink/group-urls/list']);
    this.hideLoader();
  }

}
