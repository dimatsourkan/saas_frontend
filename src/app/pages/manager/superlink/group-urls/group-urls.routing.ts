import { Routes } from '@angular/router';
import { GroupUrlsListComponent } from './group-urls-list/group-urls-list.component';
import { GroupUrlsManageComponent } from './group-urls-manage/group-urls-manage.component';
import { GroupUrlsModeComponent } from './group-urls-mode/group-urls-mode.component';
import { GroupUrlsResolver } from './group-urls.resolver';

export const GROUP_URLS_ROUTING: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'list' },
  {
    path: 'list', component: GroupUrlsListComponent,
    data: {
      name: 'titles.superlink.group-urls.list',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'create', component: GroupUrlsManageComponent,
    data: {
      title: 'create',
      name: 'titles.superlink.group-urls.manage',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'create/:publisherId', component: GroupUrlsManageComponent,
    data: {
      title: 'create',
      name: 'titles.superlink.group-urls.manage',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'edit/:id', component: GroupUrlsManageComponent,
    resolve: {
      url: GroupUrlsResolver
    },
    data: {
      title: 'edit',
      name: 'titles.superlink.group-urls.manage',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'mode/:id', component: GroupUrlsModeComponent,
    data: {
      title: 'edit',
      name: 'titles.superlink.group-urls.manage-mode',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }
];
