import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';

@Injectable({
  providedIn: 'root'
})
export class GroupUrlsService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getGroupUrls(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_group${params}`, {headers: {toCamelCase: 'true'}});
  }

  getPublisherGroup(groupID): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_group/${groupID}/get_mode`, {headers: {toCamelCase: 'true'}});
  }

  deleteAlgorithmUrl(id): Observable<any> {
    return this.http.delete(`${this.url.getBaseUrlWithRole()}/publisher_group/${id}`, {headers: {toCamelCase: 'true'}});
  }

  getContentTypeList(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/resale_url/content_type_list`, {headers: {toCamelCase: 'true'}});
  }

  getDynamicTypeList(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/resale_url/dynamic_type_list`, {headers: {toCamelCase: 'true'}});
  }

  getDistributionList(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/resale_url/distribution_list`, {headers: {toCamelCase: 'true'}});
  }

  getMonetizationList(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/resale_url/monetization_model_list`, {headers: {toCamelCase: 'true'}});
  }

  getTrafficTypeList(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/resale_url/traffic_type_list`, {headers: {toCamelCase: 'true'}});
  }

  getModeList(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_group/mode_list`, {headers: {toCamelCase: 'true'}});
  }

  getGroupUrl(groupID, pubID): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_group/${groupID}`, {headers: {toCamelCase: 'true'}});
  }

  requestForUpdateAlgorithmUrl(groupID, group): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/publisher_group/${groupID}`, group, {headers: {toCamelCase: 'true'}});
  }

  requestForCreateAlgorithmUrl(group): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/publisher_group`, group, {headers: {toCamelCase: 'true'}});
  }

  getModeGroupUrl(groupId, pubId): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher_group/${groupId}/get_mode`, {headers: {toCamelCase: 'true'}});
  }

  setDynamicMode(groupId, pubId): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/publisher_group/${groupId}/set_dynamic_mode`, {},
      {headers: {toCamelCase: 'true'}});
  }

  setManualMode(groupId, pubId, urls): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/publisher_group/${groupId}/set_manual_mode`, {urls},
      {headers: {toCamelCase: 'true'}});
  }

  setManualCountryMode(groupId, pubId, urls): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/publisher_group/${groupId}/set_manual_country_mode`, {urls},
      {headers: {toCamelCase: 'true'}});
  }

}
