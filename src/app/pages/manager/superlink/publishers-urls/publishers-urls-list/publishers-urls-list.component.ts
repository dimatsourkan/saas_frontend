import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { PublisherUrlService } from '@app/core/entities/superlink/publisher-urls/publisher-urls.service';
import { PublisherUrlFilter } from '@app/core/entities/superlink/publisher-urls/publisher-urls.filter';
import { Subscription } from 'rxjs';
import { DataStore } from '@app/core/entities/data-store.service';
import { finalize } from 'rxjs/operators';


@Component({
  selector: 'app-publishers-urls-list',
  templateUrl: './publishers-urls-list.component.html',
  styleUrls: ['./publishers-urls-list.component.scss']
})
export class PublishersUrlsListComponent implements OnInit {
  errors: any;
  subscription$: Subscription;
  filter: PublisherUrlFilter;
  publisherUrls$ = DataStore.superlinkPublisherUrls.list.asObservable$;
  loading: boolean;

  constructor(private _publishersUrlsService: PublisherUrlService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.filter = (new PublisherUrlFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getPublisherUrls(filter));
  }

  getPublisherUrls(filter: PublisherUrlFilter) {
    this.loading = true;
    this._publishersUrlsService.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  delete(id) {
    this._publishersUrlsService.delete(id).subscribe(res => {
      this.sweetAlertService.success('Done');
    }, err => {
      this.sweetAlertService.error('Error');
      this.errors = err.errors;
      if (Object.prototype.hasOwnProperty.call(this.errors, 'publisherGroups') && this.errors.publisherGroups.length) {
        const counts = {};
        this.errors.publisherGroups.forEach((x) => {
          counts[x.id] = (counts[x.id] || 0) + 1;
        });
        this.sweetAlertService.error(err.errors.message,
          `Group ID${Object.keys(counts).length ? 's' : ''}:
                ${Object.keys(counts).reduce((str, key) => str + `${key} (${counts[key]}) `, '')}`);
      } else {
        this.sweetAlertService.error(err.errors.message);
      }
    });
  }

}
