import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { patchFormValues } from '@app/core/helpers/helpers';
import { PublisherUrl } from '@app/core/entities/superlink/publisher-urls/publisher-urls.model';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { PublisherUrlService } from '@app/core/entities/superlink/publisher-urls/publisher-urls.service';

@Component({
  selector: 'app-publishers-urls-manage',
  templateUrl: './publishers-urls-manage.component.html',
  styleUrls: ['./publishers-urls-manage.component.scss']
})
export class PublishersUrlsManageComponent implements OnInit {

  publisherUrl = new PublisherUrl();
  publisherUrlForm: FormGroup;
  errors: any;
  loading: boolean;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private _publishersUrlsService: PublisherUrlService,
    private validatorService: ValidatorService) {
  }

  ngOnInit() {
    this.initForm();

    this.activatedRoute.data.subscribe(res => {
      this.publisherUrl = res.url ? res.url : new PublisherUrl();
      this.patchFormValue();
    });
  }

  initForm() {
    this.publisherUrlForm = this._fb.group({
      publisher: this._fb.group({
        id: [, Validators.required]
      }),
      resaleUrl: this._fb.group({
        id: [, Validators.required]
      }),
      name: ['Algorithm #', Validators.required]
    });
  }

  private patchFormValue() {
    patchFormValues(this.publisherUrlForm, this.publisherUrl.toJson());
  }

  submitUrlForm() {
    if (this.publisherUrlForm.invalid) {
      return this.validatorService.setTouchToControls(this.publisherUrlForm);
    }
    this.submitMethod(this.publisherUrl.update(this.publisherUrlForm.getRawValue())).subscribe(
      res => this.router.navigate(['/manager/superlink/publishers-urls/list']),
      err => {
        this.validatorService.addErrorToForm(this.publisherUrlForm, err);
        this.errors = err;
      });
  }

  private submitMethod(publisherUrl: PublisherUrl) {
    return this.publisherUrl.id ?
      this._publishersUrlsService.update(this.publisherUrl.id, publisherUrl) :
      this._publishersUrlsService.save(publisherUrl);
  }
}
