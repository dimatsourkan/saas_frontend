import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';
import { SystemModule } from '../system/system.module';
import { PublishersUrlsListComponent } from './publishers-urls-list/publishers-urls-list.component';
import { PublishersUrlsManageComponent } from './publishers-urls-manage/publishers-urls-manage.component';

import { PUBLISHERS_URLS_ROUTING } from './publishers-urls.routing';
import { PublisherUrlsResolver } from './publishers-urls.resolver';


@NgModule({
  imports: [
    CommonModule,
    SystemModule,
    RouterModule.forChild(PUBLISHERS_URLS_ROUTING),
    SharedModule
  ],
  providers: [PublisherUrlsResolver],
  declarations: [PublishersUrlsListComponent, PublishersUrlsManageComponent]
})
export class PublishersUrlsModule {
}
