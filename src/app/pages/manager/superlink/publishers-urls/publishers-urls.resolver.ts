import { Injectable } from '@angular/core';
import { BaseItemResolver } from '@app/core/services/resolver/base-item.resolver';
import { PublisherUrl } from '@app/core/entities/superlink/publisher-urls/publisher-urls.model';
import { PublisherUrlService } from '@app/core/entities/superlink/publisher-urls/publisher-urls.service';

@Injectable()
export class PublisherUrlsResolver extends BaseItemResolver<PublisherUrl> {

  protected service: PublisherUrlService = this.injector.get(PublisherUrlService, null);

  protected onError(error: any) {
    this.router.navigate(['/manager/superlink/publishers-urls/list']);
    this.hideLoader();
  }

}
