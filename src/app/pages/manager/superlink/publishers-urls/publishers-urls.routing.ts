import { Routes } from '@angular/router';
import { PublishersUrlsListComponent } from './publishers-urls-list/publishers-urls-list.component';
import { PublishersUrlsManageComponent } from './publishers-urls-manage/publishers-urls-manage.component';
import { PublisherUrlsResolver } from './publishers-urls.resolver';

export const PUBLISHERS_URLS_ROUTING: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'list'},
  {
    path: 'list', component: PublishersUrlsListComponent,
    data: {
      name: 'titles.superlink.pub-urls.list',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'create', component: PublishersUrlsManageComponent,
    data: {
      name: 'titles.superlink.pub-urls.manage',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'edit/:id', component: PublishersUrlsManageComponent,
    resolve: {
      url: PublisherUrlsResolver
    },
    data: {
      name: 'titles.superlink.pub-urls.manage',
      menuList: ['superlink'],
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }
];
