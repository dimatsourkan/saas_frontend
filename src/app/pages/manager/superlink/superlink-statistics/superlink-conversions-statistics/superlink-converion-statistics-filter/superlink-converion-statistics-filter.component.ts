import { Component, OnInit } from '@angular/core';
import { SuperlinkConversionStatisticFilter } from '@app/core/entities/superlink/reports/conversion-statistics/superlink-conversion-statistics.filter';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'app-superlink-converion-statistics-filter',
  templateUrl: './superlink-converion-statistics-filter.component.html',
  styleUrls: ['./superlink-converion-statistics-filter.component.scss']
})
export class SuperlinkConverionStatisticsFilterComponent extends AbstractFilterComponent<SuperlinkConversionStatisticFilter> implements OnInit {

  filter = (new SuperlinkConversionStatisticFilter()).init();
  filterUrls = {
    managers: '/reports/conversions/superlink/managers',
    advertisers: '/reports/conversions/superlink/advertisers',
    publishers: '/reports/conversions/superlink/publishers'
  };

  duplicateList = [
    {value: null, label: 'All'},
    {value: 0, label: 'Not Duplicate'},
    {value: 1, label: 'Duplicate'}
  ];

  ngOnInit() {
    super.ngOnInit();
    this.filter.exclude(['page', 'limit', 'search', 'from', 'to']);
    DataStore.timezone.current.asObservable$.subscribe(timezone => {
      if (!this.filter.timezone) {
        this.filter.timezone = timezone;
      }
    });
  }

}
