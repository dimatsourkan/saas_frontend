import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';
import { SuperlinkConversionsStatisticsComponent } from './superlink-conversions-statistics/superlink-conversions-statistics.component';

import { SUPERLINK_STTISTICS_ROUTING } from './superlink-statistics.routing';
import {
  SuperlinkConverionStatisticsFilterComponent
} from './superlink-conversions-statistics/superlink-converion-statistics-filter/superlink-converion-statistics-filter.component';
import { SuperlinkStatisticsComponent } from './superlink-statistics/superlink-statistics.component';
import {
  SuperlinkStatisticsFilterComponent
} from './superlink-statistics/superlink-statistics-filter/superlink-statistics-filter.component';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(SUPERLINK_STTISTICS_ROUTING)
  ],
  providers: [],
  declarations: [
    SuperlinkConverionStatisticsFilterComponent,
    SuperlinkConversionsStatisticsComponent,
    SuperlinkStatisticsFilterComponent,
    SuperlinkStatisticsComponent
  ]
})
export class SuperlinkStatisticsModule {
}
