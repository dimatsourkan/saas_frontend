import { Routes } from '@angular/router';
import { SuperlinkConversionsStatisticsComponent } from './superlink-conversions-statistics/superlink-conversions-statistics.component';
import { SuperlinkStatisticsComponent } from './superlink-statistics/superlink-statistics.component';

export const SUPERLINK_STTISTICS_ROUTING: Routes = [
  {
    path: 'statistics', component: SuperlinkStatisticsComponent, data: {
      name: 'titles.superlink.stat.main',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'superlink-conversions-statistics', component: SuperlinkConversionsStatisticsComponent, data: {
      name: 'titles.superlink.stat.conversions',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }
];
