import { Component, OnInit } from '@angular/core';
import { SuperlinkStatisticFilter } from '@app/core/entities/superlink/reports/statistics/superlink-statistics.filter';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'app-superlink-statistics-filter',
  templateUrl: './superlink-statistics-filter.component.html',
  styleUrls: ['./superlink-statistics-filter.component.scss']
})
export class SuperlinkStatisticsFilterComponent extends AbstractFilterComponent<SuperlinkStatisticFilter> implements OnInit {

  filter = (new SuperlinkStatisticFilter()).init();
  filterUrls = {
    managers: '/statistics/superlink/managers',
    advertisers: '/statistics/superlink/advertisers',
    publishers: '/statistics/superlink/publishers'
  };


  additionalFilterIsOpen: boolean;

  ngOnInit() {
    super.ngOnInit();
    this.filter.exclude(['page', 'limit', 'search', 'datetime_from', 'datetime_to']);
    DataStore.timezone.current.asObservable$.subscribe(timezone => {
      if (!this.filter.timezone) {
        this.filter.timezone = timezone;
      }
    });
  }


  toggleAdditionalFilter() {
    this.additionalFilterIsOpen = !this.additionalFilterIsOpen;
  }

}
