import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { SuperlinkStatisticFilter } from '@app/core/entities/superlink/reports/statistics/superlink-statistics.filter';
import { Subscription } from 'rxjs';
import { DataStore } from '@app/core/entities/data-store.service';
import { SuperlinkStatisticService } from '@app/core/entities/superlink/reports/statistics/superlink-statistics.service';

@Component({
  selector: 'app-superlink-statistics',
  templateUrl: './superlink-statistics.component.html',
  styleUrls: ['./superlink-statistics.component.scss']
})
export class SuperlinkStatisticsComponent implements OnInit {
  loading: boolean;
  filter: SuperlinkStatisticFilter;
  subscription$: Subscription;
  reports$ = DataStore.superlinkStatistic.list.asObservable$;

  constructor(
    private service: SuperlinkStatisticService) {
  }

  ngOnInit() {
    this.filter = (new SuperlinkStatisticFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getReports(filter));
  }

  getReports(filter: SuperlinkStatisticFilter) {
    this.loading = true;
    this.service.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: SuperlinkStatisticFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  calendarCallback({ startDate, endDate }) {
    this.filter.datetime_from = startDate;
    this.filter.datetime_to = endDate;
  }

}
