import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AlgorithmsUrlsModule } from './algorithms-urls/algorithms-urls.module';
import { GroupUrlsModule } from './group-urls/group-urls.module';
import { PublishersUrlsModule } from './publishers-urls/publishers-urls.module';

import { SUPERLINK_ROUTES } from './superlink.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SUPERLINK_ROUTES),
    AlgorithmsUrlsModule,
    GroupUrlsModule,
    PublishersUrlsModule
  ],
  declarations: []
})
export class SuperlinkModule {
}
