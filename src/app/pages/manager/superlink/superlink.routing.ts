import { Routes } from '@angular/router';

export const SUPERLINK_ROUTES: Routes = [
  {
    path: 'algorithms-urls',
    loadChildren: './algorithms-urls/algorithms-urls.module#AlgorithmsUrlsModule'
  },
  {
    path: 'group-urls',
    loadChildren: './group-urls/group-urls.module#GroupUrlsModule'
  },
  {
    path: 'publishers-urls',
    loadChildren: './publishers-urls/publishers-urls.module#PublishersUrlsModule'
  },
  {
    path: 'statistics',
    loadChildren: './superlink-statistics/superlink-statistics.module#SuperlinkStatisticsModule'
  },
  {
    path: 'system',
    loadChildren: './system/system.module#SystemModule'
  }
];
