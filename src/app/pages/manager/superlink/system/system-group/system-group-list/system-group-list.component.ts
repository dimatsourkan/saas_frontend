import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { DataStore } from '@app/core/entities/data-store.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import { SuperlinkSystemGroupUrlsFilter } from '@app/core/entities/superlink/system-group-urls/system-group-urls.filter';
import { SystemGroupUrlService } from '@app/core/entities/superlink/system-group-urls/system-group-urls.service';

@Component({
  selector: 'app-system-group',
  templateUrl: './system-group-list.component.html',
  styleUrls: ['./system-group-list.component.scss']
})
export class SystemGroupListComponent implements OnInit {

  subscription$: Subscription;
  filter: SuperlinkSystemGroupUrlsFilter;
  groupUrls$ = DataStore.superlinkSystemGroupUrlData.list.asObservable$;
  loading: boolean;

  constructor(
    private service: SystemGroupUrlService,
    private swal: SweetAlertService) {
  }

  ngOnInit() {
    this.filter = (new SuperlinkSystemGroupUrlsFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getGroupUrls(filter));
  }

  getGroupUrls(filter: SuperlinkSystemGroupUrlsFilter) {
    this.loading = true;
    this.service.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  delete(id) {
    this.swal.confirm('Do you want to remove link?').then((isConfirm) => {
      if (isConfirm.value) {
        this.service.delete(id).subscribe((response) => {
          this.swal.success('Done');
        }, () => {
          this.swal.error('Error');
        });
      }
    });
  }

}
