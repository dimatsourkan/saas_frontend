import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SystemService } from '../../system.service';
import { GroupUrlsService } from '../../../group-urls/group-urls.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';


@Component({
  selector: 'app-system-group-mode',
  templateUrl: './system-group-mode.component.html',
  styleUrls: ['./system-group-mode.component.scss']
})
export class SystemGroupModeComponent implements OnInit {
  groupID: any;
  loading: boolean;
  mode: any = {mode: 1};
  modeList: any;
  errors: any = {};

  constructor(
              private router: Router,
              private route: ActivatedRoute,
              private _systemService: SystemService,
              private _groupUrlsService: GroupUrlsService,
              private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.getModeList();

    this.route.params.subscribe(params => {
      this.groupID = params['id'];
      if (this.groupID) {
        this.getModeGroupUrl();
      } else {
        this.router.navigate(['/', 'manager', 'superlink', 'system', 'system-group-urls']);
      }
    });


  }

  getModeList() {
    return this._groupUrlsService.getModeList().subscribe((response) => {
      this.modeList = response;
    });
  }

  getModeGroupUrl() {
    this.loading = true;
    return this._systemService.getModeGroupUrl(this.groupID).subscribe((response) => {
      this.mode = response;
      this.loading = false;
      this.extendData();
    });
  }

  submitModeForm() {
    switch (this.mode.mode) {
      case 1:
        this.setDynamicMode();
        break;
      case 2:
        this.setManualMode();
        break;
      case 3:
        this.setManualCountryMode();
        break;
      default:
        return false;
    }
  }

  extendData() {
    if (this.mode.urls.length) {
      if (this.mode.allowUrls.length !== this.mode.urls.length) {
        this.mode.urls = this.merge(this.mode.allowUrls, this.mode.urls, 'systemResaleUrlId');
      }
      this.mode.urls.forEach((url) => {
        this.mode.allowUrls.forEach((allowUrl) => {
          if (url.systemResaleUrlId === allowUrl.systemResaleUrlId) {
            url.name = allowUrl.name;
            url.percent = url.percent || 0;
          }
        });
      });
    } else if (!this.mode.urls.length) {
      this.mode.allowUrls.forEach((url) => {
        const obj = Object.assign({}, url);
        obj.percent = 0;
        this.mode.urls.push(obj);
      });
    }

    if (this.mode.groupCountryUrls.length) {
      if (this.mode.groupCountryUrls[0].urls && (this.mode.allowUrls.length !== this.mode.groupCountryUrls[0].urls.length)) {
        this.mode.groupCountryUrls.forEach((groupCountryUrl) => {
          groupCountryUrl.urls = this.merge(this.mode.allowUrls, groupCountryUrl.urls, 'systemResaleUrlId');
        });
      }
      this.mode.groupCountryUrls.forEach((groupCountryUrl) => {
        if (groupCountryUrl.urls) {
          groupCountryUrl.urls.forEach((url) => {
            this.mode.allowUrls.forEach((allowUrl) => {
              if (url.systemResaleUrlId === allowUrl.systemResaleUrlId) {
                url.name = allowUrl.name;
                url.percent = url.percent || 0;
              }
            });
          });
        }
      });
    } else if (!this.mode.groupCountryUrls.length) {
      const obj = {
        countryCode: null,
        urls: []
      };
      this.mode.allowUrls.forEach((url) => {
        const urlData = Object.assign({}, url);
        urlData.percent = 0;
        obj.urls.push(urlData);
      });
      this.mode.groupCountryUrls.push(obj);
    }
  }

  merge(a, b, prop) {
    const reduced = a.filter(aItem => !b.find(bItem => aItem[prop] === bItem[prop]));
    return reduced.concat(b);
  }

  addNewCountryAlgorithm() {
    const obj = {
      countryCode: null,
      urls: []
    };
    this.mode.allowUrls.forEach((url) => {
      const urlData = Object.assign({}, url);
      urlData.percent = 0;
      obj.urls.push(urlData);
    });
    this.mode.groupCountryUrls.push(obj);
  }

  removeNewCountryAlgorithm(index) {
    this.mode.groupCountryUrls.splice(index, 1);
  }

  setDynamicMode() {
    return this._systemService.setDynamicMode(this.groupID).subscribe((response) => {
      this.router.navigate(['/', 'manager', 'superlink', 'system', 'system-group-urls']);
    }, (err) => {
      this.sweetAlertService.error(this.parserErrors(err.errors));
      this.errors = err.errors;
    });
  }

  setManualMode() {
    const data = this.mode.urls.map((url) => {
      return {
        systemResaleUrlId: url.systemResaleUrlId,
        percent: Number(url.percent)
      };
    });
    return this._systemService.setManualMode(this.groupID, data).subscribe((response) => {
      this.router.navigate(['/', 'manager', 'superlink', 'system', 'system-group-urls']);
    }, (err) => {
      this.sweetAlertService.error(this.parserErrors(err.errors));
      this.errors = err.errors;
    });
  }

  setManualCountryMode() {
    const data = this.mode.groupCountryUrls.map((countryUrl) => {
      return {
        countryCode: countryUrl.countryCode,
        urls: countryUrl.urls.map((url) => {
          return {
            systemResaleUrlId: url.systemResaleUrlId,
            percent: Number(url.percent)
          };
        })
      };
    });
    return this._systemService.setManualCountryMode(this.groupID, data).subscribe((response) => {
      this.router.navigate(['/', 'manager', 'superlink', 'system', 'system-group-urls']);
    }, (err) => {
      this.sweetAlertService.error(this.parserErrors(err.errors));
      this.errors = err.errors;
    });
  }

  onPercentChange(model, flag) {
    if (model[flag] > 100) {
      model[flag] = 100;
    } else if (model[flag] < 0) {
      model[flag] = 0;
    } else {
      if (model[flag]) {
        model[flag] = Number(model[flag].toFixed(2));
      } else {
        model[flag] = model[flag];
      }
    }
  }

  parserErrors(errors) {
    const keys = Object.keys(errors);
    let error = '';
    if (errors.hasOwnProperty('urls')) {
      error = errors.urls;
    } else if (keys[0].includes('countryCode')) {
      error = 'Country Code ' + errors[keys[0]][0];
    } else {
      error = 'Has some errors';
    }
    return error;
  }
}
