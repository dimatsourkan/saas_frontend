import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { DataStore } from '@app/core/entities/data-store.service';
import { AlgorithmUrlService } from '@app/core/entities/superlink/algorithms-urls/algorithms-urls.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import { patchFormValues } from '@app/core/helpers/helpers';
import { SuperlinkSystemGroupUrl } from '@app/core/entities/superlink/system-group-urls/system-group-urls.model';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { SystemGroupUrlService } from '@app/core/entities/superlink/system-group-urls/system-group-urls.service';

@Component({
  selector: 'app-system-group-upsert',
  templateUrl: './system-group-upsert.component.html',
  styleUrls: ['./system-group-upsert.component.scss']
})
export class SystemGroupUpsertComponent implements OnInit {

  contentTypeList$ = DataStore.superlinkAlgorithmsUrls.contentTypeList.asObservable$;
  dynamicTypeList$ = DataStore.superlinkAlgorithmsUrls.dynamicTypeList.asObservable$;
  distributionList$ = DataStore.superlinkAlgorithmsUrls.distributionList.asObservable$;
  monetizationList$ = DataStore.superlinkAlgorithmsUrls.monetizationList.asObservable$;
  trafficTypeList$ = DataStore.superlinkAlgorithmsUrls.trafficTypeList.asObservable$;
  groupUrl: SuperlinkSystemGroupUrl;
  groupForm: FormGroup;
  loading: boolean;

  constructor(private translate: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private service: SystemGroupUrlService,
    private validatorService: ValidatorService,
    private _algorithmsUrlsService: AlgorithmUrlService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.groupForm = this._fb.group({
      name: [, Validators.required],
      distribution: [, Validators.required],
      dynamicType: [, Validators.required],
      contentType: [, Validators.required],
      trafficType: [, Validators.required],
      monetizationModel: [, Validators.required]
    });
    this._algorithmsUrlsService.getDistributionList().subscribe();
    this._algorithmsUrlsService.getContentTypeList().subscribe();
    this._algorithmsUrlsService.getDynamicTypeList().subscribe();
    this._algorithmsUrlsService.getMonetizationList().subscribe();
    this._algorithmsUrlsService.getTrafficTypeList().subscribe();

    this.activatedRoute.data.subscribe(res => {
      this.groupUrl = res.url ? res.url : new SuperlinkSystemGroupUrl();
      this.patchFormValue();
    });

  }

  private patchFormValue() {
    patchFormValues(this.groupForm, this.groupUrl.toJson());
  }

  checkUrlForm() {
    if (this.groupForm.valid) {
      this.translate.get('groupUrls.manage.message').subscribe((translation) => {
        this.sweetAlertService.confirm(translation).then((isConfirm) => {
          if (isConfirm.value) {
            this.submitUrlForm();
          }
        });
      });
    } else {
      this.submitUrlForm();
    }
  }

  submitUrlForm() {
    this.submitMethod(this.groupUrl.update(this.groupForm.getRawValue())).subscribe(
      res => {
        this.router.navigate(['/', 'manager', 'superlink', 'system', 'system-group-urls', 'system-mode', res.id]);
      },
      err => {
        if (typeof err.errors === 'string') {
          this.sweetAlertService.error(err.errors);
        }
        this.validatorService.addErrorToForm(this.groupForm, err);
      });
  }

  private submitMethod(groupUrl: SuperlinkSystemGroupUrl) {
    return this.groupUrl.id ?
      this.service.update(this.groupUrl.id, groupUrl) :
      this.service.save(groupUrl);
  }

  backToList() {
    if (this.groupUrl.publisherId) {
      this.router.navigate(['/', 'manager', 'superlink', 'system', 'system-group-urls', 'system-mode', this.groupUrl.publisherId]);
    } else {
      this.router.navigate(['/', 'manager', 'superlink', 'system', 'system-group-urls']);
    }
  }
}
