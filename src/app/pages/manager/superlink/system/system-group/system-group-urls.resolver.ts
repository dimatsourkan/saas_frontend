import { Injectable } from '@angular/core';
import { BaseItemResolver } from '@app/core/services/resolver/base-item.resolver';
import { SystemGroupUrlService } from '@app/core/entities/superlink/system-group-urls/system-group-urls.service';
import { SuperlinkSystemGroupUrl } from '@app/core/entities/superlink/system-group-urls/system-group-urls.model';

@Injectable()
export class SystemGroupUrlsResolver extends BaseItemResolver<SuperlinkSystemGroupUrl> {

  protected service: SystemGroupUrlService = this.injector.get(SystemGroupUrlService, null);

  protected onError(error: any) {
    this.router.navigate(['/manager/superlink/system-group-urls/list']);
    this.hideLoader();
  }

}
