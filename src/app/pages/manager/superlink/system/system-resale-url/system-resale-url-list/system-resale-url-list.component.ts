import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataStore } from '@app/core/entities/data-store.service';
import { finalize } from 'rxjs/operators';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import { SystemResaleUrlService } from '@app/core/entities/superlink/system-resale-urls/system-resale-urls.service';
import { SystemResaleUrlsFilter } from '@app/core/entities/superlink/system-resale-urls/system-resale-urls.filter';

@Component({
  selector: 'app-system-resale-url',
  templateUrl: './system-resale-url-list.component.html',
  styleUrls: ['./system-resale-url-list.component.scss']
})
export class SystemResaleUrlListComponent implements OnInit {
  loading: boolean;
  subscription$: Subscription;
  filter: SystemResaleUrlsFilter;
  algorithmsUrls$ = DataStore.superlinkSystemResaleUrls.list.asObservable$;

  constructor(private _algorithmsUrlsService: SystemResaleUrlService, private swal: SweetAlertService) {
  }

  ngOnInit() {
    this.filter = (new SystemResaleUrlsFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getAlgorithmsUrls(filter));
  }

  getAlgorithmsUrls(filter: SystemResaleUrlsFilter) {
    this.loading = true;
    this._algorithmsUrlsService.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  deleteAlgorithmUrl(id) {
    this.swal.confirm('Do you want to remove link?').then((isConfirm) => {
      if (isConfirm.value) {
        this._algorithmsUrlsService.delete(id).subscribe(res => {
          this.swal.success('Done');
        }, err => {
          if (Object.prototype.hasOwnProperty.call(err.errors, 'publisherGroups') && err.errors.publisherGroups.length) {
            const counts = {};
            err.errors.publisherGroups.forEach((x) => {
              counts[x.id] = (counts[x.id] || 0) + 1;
            });
            this.swal.error(err.errors.message,
              `Group ID${Object.keys(counts).length ? 's' : ''} :
                ${Object.keys(counts).reduce((str, key) => str + `${key} (${counts[key]}) `, '')}`);
          }
        });
      }

    });
  }
}
