import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SystemResaleUrl } from '@app/core/entities/superlink/system-resale-urls/system-resale-urls.model';
import { patchFormValues } from '@app/core/helpers/helpers';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { SystemResaleUrlService } from '@app/core/entities/superlink/system-resale-urls/system-resale-urls.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';


@Component({
  selector: 'app-system-resale-url-upsert',
  templateUrl: './system-resale-url-upsert.component.html',
  styleUrls: ['./system-resale-url-upsert.component.scss']
})
export class SystemResaleUrlUpsertComponent implements OnInit {
  loading: boolean;
  errors: any = {};
  urlForm: FormGroup;
  systemResaleUrl = new SystemResaleUrl();

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private validatorService: ValidatorService,
    private swal: SweetAlertService,
    private service: SystemResaleUrlService) {
  }

  ngOnInit() {
    this.initForm();

    this.activatedRoute.data.subscribe(res => {
      this.systemResaleUrl = res.url ? res.url : new SystemResaleUrl();
      this.patchFormValue();
    });
  }

  initForm() {
    this.urlForm = this._fb.group({
      resaleUrl: this._fb.group({
        id: [, Validators.required]
      }),
      systemResaleUrlId: [],
      name: ['Algorithm #', Validators.required]
    });
  }

  private patchFormValue() {
    patchFormValues(this.urlForm, this.systemResaleUrl.toJson());
  }

  submitUrlForm() {
    this.submitMethod(this.systemResaleUrl.update(this.urlForm.getRawValue())).subscribe(
      res => this.router.navigate(['/', 'manager', 'superlink', 'system', 'publisher-algorithm']),
      err => {
        this.validatorService.addErrorToForm(this.urlForm, err);
        this.errors = err;
        if (typeof err.errors === 'string') {
          this.swal.error(err.errors);
        }
      });
  }

  private submitMethod(systemResaleUrl: SystemResaleUrl) {
    return this.systemResaleUrl.id ?
      this.service.update(this.systemResaleUrl.id, systemResaleUrl) :
      this.service.save(systemResaleUrl);
  }

}
