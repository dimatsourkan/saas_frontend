import { Injectable } from '@angular/core';
import { BaseItemResolver } from '@app/core/services/resolver/base-item.resolver';
import { SystemResaleUrl } from '@app/core/entities/superlink/system-resale-urls/system-resale-urls.model';
import { SystemResaleUrlService } from '@app/core/entities/superlink/system-resale-urls/system-resale-urls.service';

@Injectable()
export class SystemResaleUrlResolver extends BaseItemResolver<SystemResaleUrl> {

  protected service: SystemResaleUrlService = this.injector.get(SystemResaleUrlService, null);

  protected onError(error: any) {
    this.router.navigate(['/manager/superlink/system/publisher-algorithm']);
    this.hideLoader();
  }

}
