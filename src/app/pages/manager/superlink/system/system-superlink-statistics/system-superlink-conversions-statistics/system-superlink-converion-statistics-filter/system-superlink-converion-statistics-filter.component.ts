import { Component, OnInit } from '@angular/core';
import {
  SuperlinkSystemConversionStatisticFilter
} from '@app/core/entities/superlink/system-reports/system-conversion-statistics/superlink-system-conversion-statistics.filter';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'app-system-superlink-converion-statistics-filter',
  templateUrl: './system-superlink-converion-statistics-filter.component.html',
  styleUrls: ['./system-superlink-converion-statistics-filter.component.scss']
})
export class SuperlinkSystemConverionStatisticsFilterComponent
  extends AbstractFilterComponent<SuperlinkSystemConversionStatisticFilter> implements OnInit {

  filter = (new SuperlinkSystemConversionStatisticFilter()).init();
  filterUrls = {
    managers: '/reports/conversions/superlink/system/managers',
    advertisers: '/reports/conversions/superlink/system/advertisers',
    publishers: '/reports/conversions/superlink/publishers',
  };

  duplicateList = [
    { value: null, label: 'All' },
    { value: 0, label: 'Not Duplicate' },
    { value: 1, label: 'Duplicate' }
  ];

  ngOnInit() {
    super.ngOnInit();
    DataStore.timezone.current.asObservable$.subscribe(timezone => {
      if (!this.filter.timezone) {
        this.filter.timezone = timezone;
      }
    });
    this.filter.exclude(['page', 'limit', 'search', 'from', 'to']);
  }

}
