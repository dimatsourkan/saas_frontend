import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as FileSaver from 'file-saver';
import { finalize } from 'rxjs/operators';

import { DataStore } from '@app/core/entities/data-store.service';
import { Subscription } from 'rxjs';
import {
  SuperlinkSystemConversionStatisticFilter
} from '@app/core/entities/superlink/system-reports/system-conversion-statistics/superlink-system-conversion-statistics.filter';
import {
  SuperlinkSystemConversionStatisticService
} from '@app/core/entities/superlink/system-reports/system-conversion-statistics/superlink-system-conversion-statistics.service';

@Component({
  selector: 'app-superlink-conversions-statistics',
  templateUrl: './system-superlink-conversions-statistics.component.html',
  styleUrls: ['./system-superlink-conversions-statistics.component.scss']
})
export class SuperlinkSystemConversionsStatisticsComponent implements OnInit {
  loading: boolean;
  subscription$: Subscription;
  filter: SuperlinkSystemConversionStatisticFilter;
  conversionReports$ = DataStore.superlinkSystemConversionStatistic.list.asObservable$;

  constructor(private swal: SweetAlertService,
    private service: SuperlinkSystemConversionStatisticService) {
  }

  ngOnInit() {
    this.filter = (new SuperlinkSystemConversionStatisticFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getReports(filter));
  }

  getReports(filter: SuperlinkSystemConversionStatisticFilter) {
    this.loading = true;
    this.service.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: SuperlinkSystemConversionStatisticFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  calendarCallback({ startDate, endDate }) {
    this.filter.from = startDate;
    this.filter.to = endDate;
  }

  exportCSV() {
    this.loading = true;
    this.service.exportCSV(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe((response) => {
        FileSaver.saveAs(response.body, `report-${Date.now()}.csv`, true);
      }, () => {
        this.swal.confirm('Report file size is too big to download', `Try to use 'Send to email'`).then(res => {
          if (res.value) {
            this.sendToEmail();
          }
        });
      });
  }

  sendToEmail() {
    this.loading = true;
    this.service.sendToEmail(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => {
        this.swal.success('Complete');
      }, (err) => {
        this.swal.error(err.errors || 'Ooops! Something went wrong!');
      });
  }

}
