import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SUPERLINK_STATISTICS_ROUTING } from './system-superlink-statistics.routing';
import {
  SuperlinkSystemConverionStatisticsFilterComponent
} from './system-superlink-conversions-statistics/system-superlink-converion-statistics-filter/system-superlink-converion-statistics-filter.component';
import {
  SuperlinkSystemStatisticsFilterComponent
} from './system-superlink-statistics/system-superlink-statistics-filter/system-superlink-statistics-filter.component';
import {
  SuperlinkSystemConversionsStatisticsComponent
} from './system-superlink-conversions-statistics/system-superlink-conversions-statistics.component';
import { SuperlinkSystemStatisticsComponent } from './system-superlink-statistics/system-superlink-statistics.component';
import { SharedModule } from '@app/shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild(SUPERLINK_STATISTICS_ROUTING)
  ],
  providers: [],
  declarations: [
    SuperlinkSystemConverionStatisticsFilterComponent,
    SuperlinkSystemConversionsStatisticsComponent,
    SuperlinkSystemStatisticsFilterComponent,
    SuperlinkSystemStatisticsComponent
  ]
})
export class SuperlinkSystemStatisticsModule {
}
