import { Routes } from '@angular/router';
import { SuperlinkSystemStatisticsComponent } from './system-superlink-statistics/system-superlink-statistics.component';
import {
  SuperlinkSystemConversionsStatisticsComponent
} from './system-superlink-conversions-statistics/system-superlink-conversions-statistics.component';

export const SUPERLINK_STATISTICS_ROUTING: Routes = [
  {
    path: 'system-statistics', component: SuperlinkSystemStatisticsComponent, data: {
      name: 'titles.superlink.system.stat.main',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'system-conversions-statistics', component: SuperlinkSystemConversionsStatisticsComponent, data: {
      name: 'titles.superlink.system.stat.conversions',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }
];
