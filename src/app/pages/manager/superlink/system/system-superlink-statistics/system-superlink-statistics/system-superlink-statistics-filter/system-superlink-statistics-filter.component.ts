import { Component, OnInit } from '@angular/core';
import {
  SuperlinkSystemStatisticFilter
} from '@app/core/entities/superlink/system-reports/system-statistics/superlink-system-statistics.filter';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'app-system-superlink-statistics-filter',
  templateUrl: './system-superlink-statistics-filter.component.html',
  styleUrls: ['./system-superlink-statistics-filter.component.scss']
})
export class SuperlinkSystemStatisticsFilterComponent extends AbstractFilterComponent<SuperlinkSystemStatisticFilter> implements OnInit {

  filter = (new SuperlinkSystemStatisticFilter()).init();
  filterUrls = {
    managers: '/statistics/superlink/system/managers',
    advertisers: '/statistics/superlink/system/advertisers',
    publishers: '/statistics/superlink/system/publishers'
  };

  additionalFilterIsOpen: boolean;

  ngOnInit() {
    super.ngOnInit();
    this.filter.exclude(['page', 'limit', 'search', 'datetime_from', 'datetime_to']);
    DataStore.timezone.current.asObservable$.subscribe(timezone => {
      if (!this.filter.timezone) {
        this.filter.timezone = timezone;
      }
    });
  }


  toggleAdditionalFilter() {
    this.additionalFilterIsOpen = !this.additionalFilterIsOpen;
  }

}
