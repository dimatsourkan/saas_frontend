import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { DataStore } from '@app/core/entities/data-store.service';
import {
  SuperlinkSystemStatisticFilter
} from '@app/core/entities/superlink/system-reports/system-statistics/superlink-system-statistics.filter';
import {
  SuperlinkSystemStatisticService
} from '@app/core/entities/superlink/system-reports/system-statistics/superlink-system-statistics.service';

@Component({
  selector: 'app-superlink-system-statistics',
  templateUrl: './system-superlink-statistics.component.html',
  styleUrls: ['./system-superlink-statistics.component.scss']
})
export class SuperlinkSystemStatisticsComponent implements OnInit {
  loading: boolean;
  filter: SuperlinkSystemStatisticFilter;
  subscription$: Subscription;
  reports$ = DataStore.superlinkSystemStatistic.list.asObservable$;

  constructor(
    private service: SuperlinkSystemStatisticService) {
  }

  ngOnInit() {
    this.filter = (new SuperlinkSystemStatisticFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getReports(filter));
  }

  getReports(filter: SuperlinkSystemStatisticFilter) {
    this.loading = true;
    this.service.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: SuperlinkSystemStatisticFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  calendarCallback({ startDate, endDate }) {
    this.filter.datetime_from = startDate;
    this.filter.datetime_to = endDate;
  }

}
