import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../../shared/shared.module';
import { SYSTEM_ROUTING } from './system.routing';
import { SystemService } from './system.service';
import { SystemResaleUrlUpsertComponent } from './system-resale-url/system-resale-url-upsert/system-resale-url-upsert.component';
import { SystemResaleUrlListComponent } from './system-resale-url/system-resale-url-list/system-resale-url-list.component';
import { SystemResaleUrlResolver } from './system-resale-url/system-resale-url.resolver';
import { SystemGroupListComponent } from './system-group/system-group-list/system-group-list.component';
import { SystemGroupUpsertComponent } from './system-group/system-group-upsert/system-group-upsert.component';
import { SystemGroupModeComponent } from './system-group/system-group-mode/system-group-mode.component';
import { SystemGroupUrlsResolver } from './system-group/system-group-urls.resolver';
import { SuperlinkSystemStatisticsModule } from './system-superlink-statistics/system-superlink-statistics.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SuperlinkSystemStatisticsModule,
    RouterModule.forChild(SYSTEM_ROUTING)
  ],
  providers: [SystemService, SystemResaleUrlResolver, SystemGroupUrlsResolver],
  declarations: [SystemResaleUrlUpsertComponent,
    SystemResaleUrlListComponent, SystemGroupListComponent,
    SystemGroupUpsertComponent, SystemGroupModeComponent]
})
export class SystemModule {
}
