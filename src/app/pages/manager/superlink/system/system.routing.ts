import { Routes } from '@angular/router';
import { SystemResaleUrlListComponent } from './system-resale-url/system-resale-url-list/system-resale-url-list.component';
import { SystemResaleUrlUpsertComponent } from './system-resale-url/system-resale-url-upsert/system-resale-url-upsert.component';
import { SystemResaleUrlResolver } from './system-resale-url/system-resale-url.resolver';
import { SystemGroupListComponent } from './system-group/system-group-list/system-group-list.component';
import { SystemGroupUpsertComponent } from './system-group/system-group-upsert/system-group-upsert.component';
import { SystemGroupModeComponent } from './system-group/system-group-mode/system-group-mode.component';
import { SystemGroupUrlsResolver } from './system-group/system-group-urls.resolver';

export const SYSTEM_ROUTING: Routes = [
  {
    path: 'publisher-algorithm', children: [
      {
        path: '', component: SystemResaleUrlListComponent,
        data: {
          name: 'titles.superlink.system.list',
          menuList: ['superlink'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'create', component: SystemResaleUrlUpsertComponent,
        data: {
          name: 'titles.superlink.system.manage',
          menuList: ['superlink'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'edit/:id', component: SystemResaleUrlUpsertComponent,
        resolve: {
          url: SystemResaleUrlResolver
        },
        data: {
          name: 'titles.superlink.system.manage',
          menuList: ['superlink'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      }
    ]
  },
  {
    path: 'system-group-urls', children: [
      {
        path: '', component: SystemGroupListComponent, data: {
          name: 'titles.superlink.system.groups',
        }
      },
      {
        path: 'create', component: SystemGroupUpsertComponent,
        data: {
          name: 'titles.superlink.system.manage-groups',
          menuList: ['superlink'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'edit/:id', component: SystemGroupUpsertComponent,
        resolve: {
          url: SystemGroupUrlsResolver
        },
        data: {
          name: 'titles.superlink.system.manage-groups',
          menuList: ['superlink'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      },
      {
        path: 'system-mode/:id', component: SystemGroupModeComponent,
        data: {
          name: 'titles.superlink.system.manage-groups-mode',
          menuList: ['superlink'],
          roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
        }
      }
    ]
  },
  {
    path: 'statistics',
    loadChildren: './system-superlink-statistics/system-superlink-statistics.module#SuperlinkSystemStatisticsModule'
  }
];
