import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';

@Injectable()
export class SystemService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getGroupUrl(groupID): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/system_group/${groupID}`, {headers: {toCamelCase: 'true'}});
  }

  requestForUpdateAlgorithmUrl(groupID, group): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/system_group/${groupID}`, group, {headers: {toCamelCase: 'true'}});
  }

  requestForCreateAlgorithmUrl(group): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/system_group`, group, {headers: {toCamelCase: 'true'}});
  }

  getAlgotithmUrlNames(queryParams: any): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/resale_url/key_list${params}`, {headers: {toCamelCase: 'true'}});
  }

  getModeGroupUrl(groupID): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/system_group/${groupID}/get_mode`, {headers: {toCamelCase: 'true'}});
  }

  setDynamicMode(groupID): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/system_group/${groupID}/set_dynamic_mode`, {},
      {headers: {toCamelCase: 'true'}});
  }

  setManualMode(groupID, urls): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/system_group/${groupID}/set_manual_mode`, {urls},
      {headers: {toCamelCase: 'true'}});
  }

  setManualCountryMode(groupID, urls): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/system_group/${groupID}/set_manual_country_mode`, {urls},
      {headers: {toCamelCase: 'true'}});
  }

}
