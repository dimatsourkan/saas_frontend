import { Component, OnDestroy, OnInit } from '@angular/core';
import { AdvertiserFilter } from '@app/core/entities/advertiser/advertiser.filter';
import { AdvertiserService } from '@app/core/entities/advertiser/advertiser.service';
import { DataStore } from '@app/core/entities/data-store.service';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';

@Component({
  selector: 'users-advertisers-filter',
  templateUrl: './users-advertisers-filter.component.html',
  styleUrls: ['./users-advertisers-filter.component.scss']
})
export class UsersAdvertisersFilterComponent extends AbstractFilterComponent<AdvertiserFilter> implements OnInit, OnDestroy {

  filter = (new AdvertiserFilter()).init();
  advertisersStatuses$ = DataStore.advertiser.statuses.asObservable$;

  constructor(private advertiserService: AdvertiserService) {
    super();
  }

  ngOnInit() {
    this.advertiserService.getAdvertisersStatuses().subscribe();
    this.filter.exclude(['page', 'limit', 'search']);
    super.ngOnInit();
  }

}
