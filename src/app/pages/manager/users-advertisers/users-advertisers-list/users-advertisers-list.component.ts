import { Component, OnDestroy, OnInit } from '@angular/core';
import { advStatusType } from '@app/core/entities/advertiser/advertiser.enum';
import { AdvertiserFilter } from '@app/core/entities/advertiser/advertiser.filter';
import { Advertiser } from '@app/core/entities/advertiser/advertiser.model';
import { AdvertiserService } from '@app/core/entities/advertiser/advertiser.service';
import { DataStore } from '@app/core/entities/data-store.service';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'users-advertisers-list',
  templateUrl: './users-advertisers-list.component.html',
  styleUrls: ['./users-advertisers-list.component.scss']
})
export class UsersAdvertisersListComponent implements OnInit, OnDestroy {

  subscription$: Subscription;
  loading = false;

  filter = (new AdvertiserFilter(true)).init();
  advertisers$ = DataStore.advertiser.list.asObservable$;
  advertisersStatuses$ = DataStore.advertiser.statuses.asObservable$;

  constructor(private advertiserService: AdvertiserService) {
  }

  ngOnInit() {
    this.advertiserService.getAdvertisersStatuses().subscribe();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getAdvertisers(filter));
  }

  ngOnDestroy(): void {
    this.subscription$.unsubscribe();
  }

  applyFilters(filter: AdvertiserFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  getAdvertisers(filter: AdvertiserFilter) {
    this.loading = true;
    this.advertiserService.getAll(filter.filter)
    .pipe(finalize(() => this.loading = false))
    .subscribe();
  }

  changeStatus(status: advStatusType, advertiser: Advertiser) {
    this.loading = true;
    this.advertiserService.changeStatus(advertiser.id, status)
    .pipe(finalize(() => this.loading = false))
    .subscribe();
  }
}
