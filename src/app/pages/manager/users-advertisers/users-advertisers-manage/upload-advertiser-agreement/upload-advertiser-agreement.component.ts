import { Component, forwardRef, Injector, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AdvertiserService } from '@app/core/entities/advertiser/advertiser.service';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'upload-advertiser-agreement',
  templateUrl: './upload-advertiser-agreement.component.html',
  styleUrls: ['./upload-advertiser-agreement.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploadAdvertiserAgreementComponent),
      multi: true
    }
  ]
})
/**
 * TODO - Подлежит рефакторингу, на данный момент не критично
 */
export class UploadAdvertiserAgreementComponent extends BaseControlValueAccessor {

  @Input() advertiserId: string;
  @Input() agreementFileId: string;
  agreement: any = {
    agreementFileId: '',
    extension: '',
    content: ''
  };

  constructor(
    private swal: SweetAlertService,
    private advertiserService: AdvertiserService,
    protected injector: Injector
  ) {
    super(injector);
  }

  uploadAgreement(event) {
    this.agreement = {
      agreementFileId: event.name,
      content: event.content,
      extension: event.type
    };
  }

  clearAgreement() {
    this.agreement = {
      agreementFileId: '',
      extension: '',
      content: ''
    };
  }

  saveAgreementToServer() {

    const data = {fileName: this.agreement.agreementFileId, content: this.agreement.content};

    const upload = () => {
      return this.advertiserService.saveAgreementToServer(this.advertiserId, data).subscribe((response) => {
        this.swal.success('File uploaded');
        this.changeValue(response.fileId);
        this.clearAgreement();
      }, err => {
        this.swal.error(err.errors);
      });
    };

    if (this.agreementFileId && this.agreement.agreementFileId) {
      this.swal.confirm('You already have upload agreement. Do You want to delete old?').then((confirm) => {
        if (confirm.value) {
          this.removeAgreement(upload);
        } else {
          this.swal.error('Advertiser update canceled');
        }
      });
    } else if (this.agreement.agreementFileId) {
      upload();
    }
  }

  removeAgreement(callBack?) {
    return this.advertiserService.removeAgreement(this.advertiserId).subscribe((res) => {
      this.changeValue('');
      this.swal.success('Success');
      if (callBack) {
        callBack();
      }
    });
  }

  downloadAgreement() {
    this.advertiserService.downloadAgreement(this.advertiserId).subscribe((response) => {
      FileSaver.saveAs(response.body, `agreement-${this.advertiserId}`);
      this.swal.success('Success');
    }, err => {
      this.swal.error(err.errors);
    });
  }

}
