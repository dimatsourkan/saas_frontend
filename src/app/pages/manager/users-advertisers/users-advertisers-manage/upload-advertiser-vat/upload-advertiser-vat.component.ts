import { Component, forwardRef, Injector, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AdvertiserService } from '@app/core/entities/advertiser/advertiser.service';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'upload-advertiser-vat',
  templateUrl: './upload-advertiser-vat.component.html',
  styleUrls: ['./upload-advertiser-vat.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploadAdvertiserVatComponent),
      multi: true
    }
  ]
})
/**
 * TODO - Подлежит рефакторингу, на данный момент не критично
 */
export class UploadAdvertiserVatComponent extends BaseControlValueAccessor {

  @Input() advertiserId: string;
  @Input() vatCertificateFileId: string;
  vat: any = {
    vatCertificateFileId: '',
    extension: '',
    content: ''
  };

  constructor(
    private swal: SweetAlertService,
    private advertiserService: AdvertiserService,
    protected injector: Injector
  ) {
    super(injector);
  }

  uploadVat(event) {
    this.vat = {
      vatCertificateFileId: event.name,
      content: event.content,
      extension: event.type
    };
  }

  clearVat() {
    this.vat = {
      vatCertificateFileId: '',
      content: '',
      extension: ''
    };
  }

  saveVatToServer() {
    const data = {fileName: this.vat.vatCertificateFileId, content: this.vat.content};
    const upload = () => {
      return this.advertiserService.saveVatToServer(this.advertiserId, data).subscribe((response) => {
        this.swal.success('File uploaded');
        this.changeValue(response.fileId);
        this.clearVat();
      });
    };

    if (this.value && this.vat.vatCertificateFileId) {
      return this.swal.confirm('You already have upload Vat Certificate. Do You want to delete old?').then((confirm) => {
        if (confirm.value) {
          this.removeVat(upload);
        } else {
          this.swal.error('Advertiser update canceled');
        }
      });
    } else if (this.vat.vatCertificateFileId) {
      return upload();
    }

  }

  removeVat(callBack?: Function) {
    return this.advertiserService.removeVat(this.advertiserId).subscribe(() => {
      this.changeValue(null);
      this.swal.success('Success');
      if (callBack) {
        callBack();
      }
    });
  }

  downloadVat() {
    this.advertiserService.downloadVat(this.advertiserId).subscribe((response) => {
      FileSaver.saveAs(response.body, `vat-${this.advertiserId}`);
      this.swal.success('Success');
    }, err => {
      this.swal.error(err.errors);
    });
  }

}
