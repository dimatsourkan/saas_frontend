import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Advertiser } from '@app/core/entities/advertiser/advertiser.model';
import { AdvertiserService } from '@app/core/entities/advertiser/advertiser.service';
import { patchFormValues } from '@app/core/helpers/helpers';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { finalize } from 'rxjs/operators';
import { UsersAdvertisersService } from '../users-advertisers.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';

@Component({
  selector: 'users-advertisers-manage',
  templateUrl: './users-advertisers-manage.component.html',
  styleUrls: ['./users-advertisers-manage.component.scss']
})
export class UsersAdvertisersManageComponent implements OnInit {

  advertiser = new Advertiser();
  form: FormGroup;
  submitted: boolean;
  loading: boolean;
  stringRegex = /\w+/;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private usersAdvertisersService: UsersAdvertisersService,
    private swal: SweetAlertService,
    private advertiserService: AdvertiserService,
    private validatorService: ValidatorService) {
  }

  ngOnInit() {
    this.form = this.usersAdvertisersService.getForm();
    this.activatedRoute.data.subscribe(res => {
      this.advertiser = res.advertiser ? res.advertiser : new Advertiser();
      this.patchFormValue();
    });
  }

  private patchFormValue() {
    if (this.advertiser.id) {
      this.form.removeControl('password');
      this.form.removeControl('confirmPassword');
    }
    this.advertiser.address.additionalAddress.forEach(() => {
      this.addAdditionalAddress();
    });
    patchFormValues(this.form, this.advertiser.toJson());
  }

  get additionalAddressForm() {
    return this.form.get('address.additionalAddress') as FormArray;
  }

  removeAdditionalAddress(index: number) {
    this.additionalAddressForm.removeAt(index);
  }

  addAdditionalAddress() {
    this.additionalAddressForm.push(this.usersAdvertisersService.getAddressForm());
  }

  resetPasswords() {
    this.form.patchValue({ password: '', confirmPassword: '' });
  }

  submitAdvertiserForm() {

    if (this.form.invalid) {
      this.resetPasswords();
      this.validatorService.setTouchToControls(this.form);
      return;
    }

    this.loading = true;
    this.submitMethod(this.advertiser.update(this.form.getRawValue()))
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        res => this.router.navigate(['/manager/advertisers/list']),
        err => {
          this.resetPasswords();
          if (typeof err.errors === 'string') {
            return this.swal.error(err.errors);
          }
          this.validatorService.addErrorToForm(this.form, err);
        }
      );
  }

  private submitMethod(advertiser: Advertiser) {
    return this.advertiser.id ?
      this.advertiserService.update(this.advertiser.id, advertiser) :
      this.advertiserService.save(advertiser);
  }

}
