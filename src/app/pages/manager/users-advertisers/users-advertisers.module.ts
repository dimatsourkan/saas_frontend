import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UsersAdvertisersResolver } from '@app/pages/manager/users-advertisers/users-advertisers.resolver';
import { SharedModule } from '@app/shared/shared.module';

import { UsersAdvertisersFilterComponent } from './users-advertisers-list/users-advertisers-filter/users-advertisers-filter.component';
import { UsersAdvertisersListComponent } from './users-advertisers-list/users-advertisers-list.component';
import {
  UploadAdvertiserAgreementComponent
} from './users-advertisers-manage/upload-advertiser-agreement/upload-advertiser-agreement.component';
import {
  UploadAdvertiserVatComponent
} from './users-advertisers-manage/upload-advertiser-vat/upload-advertiser-vat.component';
import { UsersAdvertisersManageComponent } from './users-advertisers-manage/users-advertisers-manage.component';
import { USERS_ADVERTISERS_ROUTING } from './users-advertisers.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(USERS_ADVERTISERS_ROUTING),
    SharedModule
  ],
  providers: [
    UsersAdvertisersResolver
  ],
  declarations: [
    UsersAdvertisersListComponent,
    UsersAdvertisersManageComponent,
    UsersAdvertisersFilterComponent,
    UploadAdvertiserAgreementComponent,
    UploadAdvertiserVatComponent
  ]
})
export class UsersAdvertisersModule {
}
