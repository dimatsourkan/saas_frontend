import { Injectable } from '@angular/core';
import { Advertiser } from '@app/core/entities/advertiser/advertiser.model';
import { AdvertiserService } from '@app/core/entities/advertiser/advertiser.service';
import { BaseItemResolver } from '@app/core/services/resolver/base-item.resolver';

@Injectable()
export class UsersAdvertisersResolver extends BaseItemResolver<Advertiser> {

  protected service: AdvertiserService = this.injector.get(AdvertiserService, null);

  protected onError(error: any) {
    this.router.navigate(['/manager/advertisers/list']);
    this.hideLoader();
  }

}
