import { Routes } from '@angular/router';
import { UsersAdvertisersResolver } from '@app/pages/manager/users-advertisers/users-advertisers.resolver';
import { UsersAdvertisersListComponent } from './users-advertisers-list/users-advertisers-list.component';
import { UsersAdvertisersManageComponent } from './users-advertisers-manage/users-advertisers-manage.component';

export const USERS_ADVERTISERS_ROUTING: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {
    path: 'list', component: UsersAdvertisersListComponent,
    data: {
      name: 'titles.adv.list',
      menuList: 'users',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'create', component: UsersAdvertisersManageComponent,
    data: {
      name: 'titles.adv.create',
      menuList: 'users',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'edit/:id', component: UsersAdvertisersManageComponent,
    resolve: {
      advertiser: UsersAdvertisersResolver
    },
    data: {
      name: 'titles.adv.edit',
      menuList: 'users',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }
];
