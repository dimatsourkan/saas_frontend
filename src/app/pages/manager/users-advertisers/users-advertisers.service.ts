import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '@app/shared/validators/custom-validators.validator';
import { RxwebValidators } from '@rxweb/reactive-form-validators';

@Injectable({
  providedIn: 'root'
})
export class UsersAdvertisersService {

  constructor(private formBuilder: FormBuilder) {
  }

  getForm(): FormGroup {
    return this.formBuilder.group({
      username: [null, Validators.required],
      person: this.formBuilder.group({
        firstName: [null, [CustomValidators.latin]],
        lastName: [null, [CustomValidators.latin]]
      }),
      agreementFileId: [null],
      email: [null, [Validators.required, Validators.email]],
      additionalEmails: [[]],
      invoiceData: this.formBuilder.group({
        invoiceCompany: [null],
        invoiceEmails: [[]],
        paymentTerms: [null, Validators.required],
        currency: [null, Validators.required],
        timezone: [null, Validators.required],
        invoiceNotes: [null],
        vat: [null],
        vatCertificateFileId: [null],
        vatPercent: [null, [Validators.max(100), Validators.min(0)]],
      }),
      skype: null,
      phone: [null, [CustomValidators.phone, Validators.required]],
      address: this.formBuilder.group({
        address: [null, Validators.required],
        city: [null, Validators.required],
        state: [null, Validators.required],
        countryCode: [null, Validators.required],
        zipcode: [null, Validators.required],
        additionalAddress: this.formBuilder.array([])
      }),
      status: [1, Validators.required],
      appWhiteList: [],
      ipWhitelist: this.formBuilder.group({
        enabledIpWhitelist: [0],
        ipWhitelist: [[]]
      }),
      publisherAccessType: [2, Validators.required],
      publisherAccessList: [[]],
      securityType: [1, Validators.required],
      url: this.formBuilder.group({
        offerPageUrl: [null, CustomValidators.url],
        offerCheckUrl: [null, CustomValidators.url],
        advertiserUrl: [null, CustomValidators.url],
        trackingUrlSuffix: [null]
      }),
      crLimit: this.formBuilder.group({
        incentLimit: [null, [Validators.max(100), Validators.min(0)]],
        nonIncentLimit: [null, [Validators.max(100), Validators.min(0)]]
      }),
      notes: null,
      manager: this.formBuilder.group({
        id: [null, Validators.required]
      }),
      password: [null, [Validators.required]],
      confirmPassword: ['', [Validators.required, RxwebValidators.compare({fieldName: 'password'})]]
    });
  }

  getAddressForm() {
    return this.formBuilder.group({
      address: [],
      city: [],
      state: [],
      countryCode: [],
      zipcode: [],
    });
  }

}
