import { Component, OnInit } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { managerStatusType } from '@app/core/entities/manager/manager.enum';
import { ManagerFilter } from '@app/core/entities/manager/manager.filter';
import { Manager } from '@app/core/entities/manager/manager.model';
import { ManagerService } from '@app/core/entities/manager/manager.service';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-users-managers-list',
  templateUrl: './users-managers-list.component.html',
  styleUrls: ['./users-managers-list.component.scss']
})
export class UsersManagersListComponent implements OnInit {
  loading = false;
  subscription$: Subscription;
  filter = (new ManagerFilter(true)).init();
  managers$ = DataStore.manager.list.asObservable$;
  managersStatuses$ = DataStore.manager.statuses.asObservable$;

  constructor(private managerService: ManagerService) {
  }

  ngOnInit() {
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getManagers(filter));
    this.managerService.getManagerStatuses().subscribe();
  }

  getManagers(filter: ManagerFilter) {
    this.loading = true;
    this.managerService.getAll(filter.filter)
    .pipe(finalize(() => this.loading = false))
    .subscribe();
  }

  changeStatus(status: managerStatusType, manager: Manager) {
    this.loading = true;
    this.managerService.changeStatus(manager.id, status)
    .pipe(finalize(() => this.loading = false))
    .subscribe();
  }
}
