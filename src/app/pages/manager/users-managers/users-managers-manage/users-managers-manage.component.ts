import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Manager } from '@app/core/entities/manager/manager.model';
import { ManagerService } from '@app/core/entities/manager/manager.service';
import { patchFormValues } from '@app/core/helpers/helpers';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { REGEXPS } from '@app/app.constants';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';

@Component({
  selector: 'app-users-managers-manage',
  templateUrl: './users-managers-manage.component.html',
  styleUrls: ['./users-managers-manage.component.scss']
})
export class UsersManagersManageComponent implements OnInit {

  manager = new Manager();
  form: FormGroup;
  submitted: boolean;
  loading: boolean;

  constructor(private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private swal: SweetAlertService,
    private managerService: ManagerService,
    private validatorService: ValidatorService) {
  }

  ngOnInit() {
    this.initForm();
    this.activatedRoute.data.subscribe(res => {
      this.manager = res.manager ? res.manager : new Manager();
      this.patchFormValue();
    });
  }

  private initForm(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
      person: this.fb.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required]
      }),
      email: ['', [Validators.required, Validators.email]],
      additionalEmails: [[]],
      skype: '',
      phone: ['', Validators.pattern(REGEXPS.PHONE)],
      address: this.fb.group({
        address: '',
        city: '',
        state: '',
        countryCode: '',
        zipcode: ''
      }),
      status: [1, Validators.required],
      emailNotificationEnabled: [0, Validators.required],
      roles: [, Validators.required],
      notes: '',
      password: ['', [Validators.required, Validators.minLength(5)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(5), RxwebValidators.compare({ fieldName: 'password' })]]
    });
  }

  private patchFormValue() {
    if (this.manager.id) {
      this.form.removeControl('password');
      this.form.removeControl('confirmPassword');
    }
    patchFormValues(this.form, this.manager.toJson());
    this.form.get('roles').patchValue(this.manager.role);
  }

  resetPasswords() {
    this.form.patchValue({ password: '', confirmPassword: '' });
  }

  submitManagerForm() {
    if (this.form.invalid) {
      this.resetPasswords();
      this.validatorService.setTouchToControls(this.form);
      return;
    }
    this.submitMethod(this.manager.update(this.form.getRawValue())).subscribe(
      res => this.router.navigate(['/manager/managers/list']),
      err => {
        this.resetPasswords();
        if (typeof err.errors === 'string') {
          return this.swal.error(err.errors);
        }
        this.validatorService.addErrorToForm(this.form, err);
      });

  }

  private submitMethod(manager: Manager) {
    return this.manager.id ?
      this.managerService.update(this.manager.id, manager) :
      this.managerService.save(manager);
  }

}
