import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { UsersManagersListComponent } from './users-managers-list/users-managers-list.component';
import { UsersManagersManageComponent } from './users-managers-manage/users-managers-manage.component';
import { UsersManagerResolver } from './users-managers.resolver';

import { USERS_MANAGERS_ROUTING } from './users-managers.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(USERS_MANAGERS_ROUTING),

    SharedModule
  ],
  providers: [UsersManagerResolver],
  declarations: [UsersManagersListComponent, UsersManagersManageComponent]
})
export class UsersManagersModule {
}
