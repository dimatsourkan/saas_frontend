import { Injectable } from '@angular/core';
import { Manager } from '@app/core/entities/manager/manager.model';
import { ManagerService } from '@app/core/entities/manager/manager.service';
import { BaseItemResolver } from '@app/core/services/resolver/base-item.resolver';

@Injectable()
export class UsersManagerResolver extends BaseItemResolver<Manager> {

  protected service: ManagerService = this.injector.get(ManagerService, null);

  protected onError(error: any) {
    this.router.navigate(['/manager/manager/list']);
    this.hideLoader();
  }

}
