import { Routes } from '@angular/router';
import { UsersManagersListComponent } from './users-managers-list/users-managers-list.component';
import { UsersManagersManageComponent } from './users-managers-manage/users-managers-manage.component';
import { UsersManagerResolver } from './users-managers.resolver';

export const USERS_MANAGERS_ROUTING: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {
    path: 'list', component: UsersManagersListComponent,
    data: {
      name: 'titles.man.list',
      menuList: 'users',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'create', component: UsersManagersManageComponent,
    data: {
      name: 'titles.man.create',
      menuList: 'users',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'edit/:id', component: UsersManagersManageComponent,
    resolve: {
      manager: UsersManagerResolver
    },
    data: {
      name: 'titles.man.edit',
      menuList: 'users',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }
];
