import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute } from '@angular/router';
import { Pub, Publisher } from '@app/core/entities/publisher/publisher.model';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-check-publisher',
  templateUrl: './check-publisher.component.html',
  styleUrls: ['./check-publisher.component.scss']
})
export class CheckPublisherComponent implements OnInit {

  checkForm: FormGroup;
  loading: boolean;
  submitted: boolean;
  publisher: Publisher;
  check: Pub.Check;

  constructor(
    private activatedRoute: ActivatedRoute,
    private publisherService: PublisherService,
    private validatorService: ValidatorService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.checkForm = this.formBuilder.group({
      url: ['', [Validators.required]],
      offerId: ['', [Validators.required]]
    });

    this.activatedRoute.data.subscribe(res => {
      this.publisher = res.publisher ? res.publisher : new Publisher();
    });

  }

  submitCheckForm() {
    this.submitted = true;
    if (this.checkForm.invalid) {
      return this.validatorService.setTouchToControls(this.checkForm);
    }
    this.loading = true;
    this.publisherService.submitCheckForm(this.publisher.id, this.checkForm.value.url, this.checkForm.value.offerId)
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        res => this.check = res,
        err => this.validatorService.addErrorToForm(this.checkForm, err));
  }

}
