import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersPublishersHelpModalComponent } from './users-publishers-help-modal.component';

describe('UsersPublishersHelpModalComponent', () => {
  let component: UsersPublishersHelpModalComponent;
  let fixture: ComponentFixture<UsersPublishersHelpModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersPublishersHelpModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersPublishersHelpModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
