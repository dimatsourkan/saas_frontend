import { Component, ViewChild } from '@angular/core';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';
import { ConfirmComponent } from '@app/new-shared/components/modals/confirm/confirm.component';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-users-publishers-approvals',
  templateUrl: './users-publishers-approvals.component.html',
  styleUrls: ['./users-publishers-approvals.component.scss']
})
export class UsersPublishersApprovalsComponent {

  @ViewChild('modal') modal: ModalComponent;
  @ViewChild('confirm') confirm: ConfirmComponent;

  loading = false;
  pubIdForRemoveApprovals;
  pubNameForRemoveApprovals: any;
  removeApprovalsForAllTime: boolean;
  removeApprovalsDateStart = moment().format();
  removeApprovalsDateEnd = moment().format();

  constructor(private publisherService: PublisherService) {
  }

  removeApprovals() {
    this.confirm.open(() => {
      this.confirm.close();
      this.loading = true;
      const dateFrom = this.removeApprovalsForAllTime ? null : this.removeApprovalsDateStart;
      const dateTo = this.removeApprovalsForAllTime ? null : this.removeApprovalsDateEnd;
      this.publisherService.removeApprovals(this.pubIdForRemoveApprovals, dateFrom, dateTo)
        .pipe(finalize(() => this.loading = false))
        .subscribe((res) => {
          this.modal.close();
        });
    }, () => this.modal.close());
  }

  openModal(pubId, pubName) {
    this.pubIdForRemoveApprovals = pubId;
    this.pubNameForRemoveApprovals = pubName;
    this.removeApprovalsForAllTime = false;
    this.modal.open();
  }

}
