import { Component, OnInit } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { PublisherFilter } from '@app/core/entities/publisher/publisher.filter';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';

@Component({
  selector: 'app-users-publishers-filter',
  templateUrl: './users-publishers-filter.component.html',
  styleUrls: ['./users-publishers-filter.component.scss']
})
export class UsersPublishersFilterComponent extends AbstractFilterComponent<PublisherFilter> implements OnInit {

  filter = (new PublisherFilter()).init();
  statuses$ = DataStore.publisher.statuses.asObservable$;

  constructor(private publisherService: PublisherService) {
    super();
  }

  ngOnInit() {
    this.publisherService.getPublisherStatuses().subscribe();
    this.filter.exclude(['page', 'limit', 'search']);
    super.ngOnInit();
  }

}
