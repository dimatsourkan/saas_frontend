import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import { pubStatusType } from '@app/core/entities/publisher/publisher.enum';
import { PublisherFilter } from '@app/core/entities/publisher/publisher.filter';
import { Publisher } from '@app/core/entities/publisher/publisher.model';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';
import { ROLES } from '@app/core/enums/role.enum';
import { AuthService } from '@app/core/services/auth/auth.service';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-users-publishers-list',
  templateUrl: './users-publishers-list.component.html',
  styleUrls: ['./users-publishers-list.component.scss']
})
export class UsersPublishersListComponent implements OnInit, OnDestroy {

  subscription$: Subscription;

  loading = false;
  isCheckerEnabled: boolean;

  filter = (new PublisherFilter(true)).init();
  statuses$ = DataStore.publisher.statuses.asObservable$;
  publishers$ = DataStore.publisher.list.asObservable$;

  constructor(private authService: AuthService,
              private publisherService: PublisherService) {
  }

  ngOnInit() {
    this.getIsCheckerEnabled();
    this.publisherService.getPublisherStatuses().subscribe();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getPublishers(filter));
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  applyFilters(filter: PublisherFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  getIsCheckerEnabled() {
    this.publisherService.getIsCheckerEnabled().subscribe((resolve) => {
      this.isCheckerEnabled = resolve.enabled;
    });
  }

  getPublishers(filter: PublisherFilter) {
    this.loading = true;
    this.publisherService.getAll(filter.filter)
    .pipe(finalize(() => this.loading = false))
    .subscribe();
  }

  changeStatus(status: pubStatusType, publisher: Publisher) {
    this.loading = true;
    this.publisherService.changeStatus(publisher.id, status)
    .pipe(finalize(() => this.loading = false))
    .subscribe();
  }

  loginAs(id: string) {
    this.loading = true;
    this.publisherService.getPublisherToken(id)
    .pipe(finalize(() => this.loading = false))
    .subscribe((res) => {
      this.authService.authorizeUser(ROLES.PUBLISHER, res.token);
      window.open('publisher', '_blank');
    });
  }
}
