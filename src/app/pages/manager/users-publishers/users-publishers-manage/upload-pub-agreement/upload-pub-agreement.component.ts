import { Component, forwardRef, Injector, Input } from '@angular/core';
import { FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';
import { AuthService } from '@app/core/services/auth/auth.service';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-upload-pub-agreement',
  templateUrl: './upload-pub-agreement.component.html',
  styleUrls: ['./upload-pub-agreement.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploadPubAgreementComponent),
      multi: true
    }
  ]
})
export class UploadPubAgreementComponent extends BaseControlValueAccessor {

  @Input() publisherId: string;

  agreement = {
    agreementFileId: '',
    content: '',
    extension: ''
  };

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private swal: SweetAlertService,
              private auth: AuthService,
              private publisherService: PublisherService,
              protected injector: Injector
  ) {
    super(injector);
  }

  uploadAgreement(event) {
    this.agreement = {
      agreementFileId: event.name,
      content: event.content,
      extension: event.type
    };
  }

  clearAgreement() {
    this.agreement = {
      agreementFileId: '',
      extension: '',
      content: ''
    };
  }

  saveAgreementToServer() {
    const data = {fileName: this.agreement.agreementFileId, content: this.agreement.content};
    const upload = () => {
      return this.publisherService.saveAgreementToServer(this.publisherId, data).subscribe((response) => {
        this.swal.success('File uploaded');
        this.value = response.fileId;
        this.clearAgreement();
      }, err => {
        this.swal.error(err.errors);
      });
    };
    if (this.value && this.agreement.agreementFileId) {
      this.swal.confirm('You already have upload agreement. Do You want to delete old?').then((confirm) => {
        if (confirm.value) {
          this.removeAgreement(upload);
        } else {
          this.swal.error('Publisher update canceled');
        }
      });
    } else if (this.agreement.agreementFileId) {
      upload();
    }
  }

  downloadAgreement() {
    this.publisherService.downloadAgreement(this.publisherId).subscribe((response) => {
      this.downloadFile('agreement', response);
    }, err => {
      this.swal.error(err.errors);
    });
  }

  removeAgreement(callBack?) {
    return this.publisherService.removeAgreement(this.publisherId).subscribe((res) => {
      this.value = '';
      this.swal.success('Success');
      if (callBack) {
        callBack();
      }
    });
  }

  downloadFile(flag, response) {
    FileSaver.saveAs(response.body, `${flag}-${this.publisherId}`);
    this.swal.success('Success');
  }

}
