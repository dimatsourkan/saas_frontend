import { Component, forwardRef, Injector, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';
import { BaseControlValueAccessor } from '@app/new-shared/components/form-controls/control-value-accessor';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-upload-pub-w8w9',
  templateUrl: './upload-pub-w8w9.component.html',
  styleUrls: ['./upload-pub-w8w9.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploadPubW8w9Component),
      multi: true
    }
  ]
})
export class UploadPubW8w9Component extends BaseControlValueAccessor {

  @Input() publisherId: string;

  w8w9 = {
    w8w9FileId: '',
    content: '',
    extension: ''
  };

  constructor(private swal: SweetAlertService,
              private publisherService: PublisherService,
              protected injector: Injector) {
    super(injector);
  }

  uploadW8W9(event) {
    this.w8w9 = {
      w8w9FileId: event.name,
      content: event.content,
      extension: event.type
    };
  }

  downloadFile(flag, response) {
    FileSaver.saveAs(response.body, `${flag}-${this.publisherId}`);
    this.swal.success('Success');
  }

  clearW8W9() {
    this.w8w9 = {
      w8w9FileId: '',
      extension: '',
      content: ''
    };
  }

  saveW8W9FileIdToServer() {
    const data = {fileName: this.w8w9.w8w9FileId, content: this.w8w9.content};
    const upload = () => {
      return this.publisherService.saveW8W9FileIdToServer(this.publisherId, data).subscribe((response) => {
        this.swal.success('File uploaded');
        this.value = response.fileId;
        this.clearW8W9();
      }, err => {
        this.swal.error(err.errors);
      });
    };
    if (this.value && this.w8w9.w8w9FileId) {
      this.swal.confirm('You already have upload W8/W9. Do You want to delete old?').then((confirm) => {
        if (confirm.value) {
          this.removeW8W9(upload);
        } else {
          this.swal.error('Publisher update canceled');
        }
      });
    } else if (this.w8w9.w8w9FileId) {
      upload();
    }
  }

  removeW8W9(callBack?) {
    return this.publisherService.removeW8W9(this.publisherId).subscribe((res) => {
      this.value = '';
      this.swal.success('Success');
      if (callBack) {
        callBack();
      }
    });
  }

  downloadW8W9() {
    this.publisherService.downloadW8W9(this.publisherId).subscribe((response) => {
      if (response.status === 200) {
        this.downloadFile('w8w9', response);
      } else {
        this.swal.error(response.data.errors);
      }
    });
  }

}

