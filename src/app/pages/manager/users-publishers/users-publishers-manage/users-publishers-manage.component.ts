import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStore } from '@app/core/entities/data-store.service';
import { Pub, Publisher } from '@app/core/entities/publisher/publisher.model';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';
import { patchFormValues } from '@app/core/helpers/helpers';
import { ValidatorService } from '@app/shared/components/validation/validation.service';
import { finalize } from 'rxjs/operators';
import { UsersPublishersService } from '../users-publishers.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';

@Component({
  selector: 'app-users-publishers-manage',
  templateUrl: './users-publishers-manage.component.html',
  styleUrls: ['./users-publishers-manage.component.scss']
})
export class UsersPublishersManageComponent implements OnInit {

  loading = false;
  publisher: Publisher;
  form: FormGroup;
  skipPercentFlag: boolean;
  redirectTypes$ = DataStore.publisher.redirectTypes.asObservable$;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private usersPubService: UsersPublishersService,
    private publisherService: PublisherService,
    private swal: SweetAlertService,
    private validatorService: ValidatorService) {
  }

  get additionalAddressData() {
    return this.form.get('address.additionalAddress') as FormArray;
  }

  get eventPostbacks() {
    return this.form.get('eventPostbacks') as FormArray;
  }

  get offerPostbacks() {
    return this.form.get('offerPostbacks') as FormArray;
  }

  ngOnInit() {
    this.initForm();
    this.publisherService.getRedirectTypes().subscribe();

    this.skipPercentFlag = DataStore.token.item.getValue().special;
    this.activatedRoute.data.subscribe(res => {
      this.publisher = res.publisher ? res.publisher : new Publisher();
      this.patchFormValue();
    });

  }

  private initForm() {
    this.form = this.usersPubService.getForm();
  }

  private initFormLogic() {
    this.form.get('resale').get('enabled').valueChanges.subscribe(value => {
      const percent = this.form.get('resale').get('percent');
      if (!value) {
        percent.disable({ onlySelf: value });
      } else {
        percent.enable({ onlySelf: true });
      }
    });

    const offerRequestLimit = this.form.get('offerRequestLimit');
    offerRequestLimit.get('offerRequestLimit').valueChanges.subscribe((val) => {
      offerRequestLimit.patchValue({
        offerRequestLeft: val
      }, {
          onlySelf: false,
          emitEvent: false
        });
    });
  }

  private patchFormValue() {
    if (this.publisher.id) {
      this.form.removeControl('password');
      this.form.removeControl('confirmPassword');
    }
    this.publisher.address.additionalAddress.forEach(() => {
      this.addAdditionalAddress();
    });
    this.setEventPostback(this.publisher.eventPostbacks);
    this.setOfferPostback(this.publisher.offerPostbacks);
    patchFormValues(this.form, this.publisher.toJson());
    this.initFormLogic();

  }

  get additionalAddressForm() {
    return this.form.get('address.additionalAddress') as FormArray;
  }

  removeAdditionalAddress(index: number) {
    this.additionalAddressForm.removeAt(index);
  }

  addAdditionalAddress() {
    this.additionalAddressForm.push(this.usersPubService.getAddressForm());
  }

  removeFormArrayElement(controlName: string, index: number) {
    (this.form.get(controlName) as FormArray).removeAt(index);
  }

  setEventPostback(events: Pub.EventPostback[]) {
    this.eventPostbacks.controls = [];
    events.forEach(event => {
      const form = this.usersPubService.getEventPostbackForm();
      form.patchValue(event);
      this.eventPostbacks.push(form);
    });
  }

  setOfferPostback(events: Pub.OfferPostback[]) {
    this.offerPostbacks.controls = [];
    events.forEach(event => {
      const form = this.usersPubService.getOfferPostbackForm();
      form.patchValue(event);
      this.offerPostbacks.push(form);
    });
  }

  submitform() {

    if (this.form.invalid) {
      this.form.patchValue({ password: '', confirmPassword: '' });
      this.validatorService.setTouchToControls(this.form);
      return;
    }

    this.loading = true;
    this.submitMethod(this.publisher.update(this.form.getRawValue()))
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        res => this.router.navigate(['/manager/publishers/list']),
        err => {
          if (typeof err.errors === 'string') {
            return this.swal.error(err.errors);
          }
          this.validatorService.addErrorToForm(this.form, err);
        });

  }

  private submitMethod(publisher: Publisher) {
    return this.publisher.id ?
      this.publisherService.update(this.publisher.id, publisher) :
      this.publisherService.save(publisher);
  }

}
