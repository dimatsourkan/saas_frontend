import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CheckPublisherComponent } from './check-publisher/check-publisher.component';
import { UsersPublishersApprovalsComponent } from './users-publishers-list/users-publishers-approvals/users-publishers-approvals.component';
import { UsersPublishersFilterComponent } from './users-publishers-list/users-publishers-filter/users-publishers-filter.component';
import { UsersPublishersListComponent } from './users-publishers-list/users-publishers-list.component';
import { UploadPubAgreementComponent } from './users-publishers-manage/upload-pub-agreement/upload-pub-agreement.component';
import { UploadPubW8w9Component } from './users-publishers-manage/upload-pub-w8w9/upload-pub-w8w9.component';
import { UsersPublishersManageComponent } from './users-publishers-manage/users-publishers-manage.component';

import { USERS_PUBLISHERS_ROUTING } from './users-publishers.routing';
import { SharedModule } from '@app/shared/shared.module';
import { UsersPublishersResolver } from './users-publishers.resolver';
import { UsersPublishersHelpModalComponent } from './users-publishers-help-modal/users-publishers-help-modal.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(USERS_PUBLISHERS_ROUTING)
  ],
  providers: [
    UsersPublishersResolver
  ],
  declarations: [
    UsersPublishersListComponent,
    UsersPublishersManageComponent,
    CheckPublisherComponent,
    UsersPublishersFilterComponent,
    UsersPublishersApprovalsComponent,
    UploadPubAgreementComponent,
    UploadPubW8w9Component,
    UsersPublishersHelpModalComponent
  ]

})
export class UsersPublishersModule {
}
