import { Injectable } from '@angular/core';
import { Publisher } from '@app/core/entities/publisher/publisher.model';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';
import { BaseItemResolver } from '@app/core/services/resolver/base-item.resolver';

@Injectable()
export class UsersPublishersResolver extends BaseItemResolver<Publisher> {

  protected service: PublisherService = this.injector.get(PublisherService, null);

  protected onError(error: any) {
    this.router.navigate(['/manager/publishers/list']);
    this.hideLoader();
  }

}
