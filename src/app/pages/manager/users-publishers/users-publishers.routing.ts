import { Routes } from '@angular/router';
import { UsersPublishersResolver } from '@app/pages/manager/users-publishers/users-publishers.resolver';
import { CheckPublisherComponent } from './check-publisher/check-publisher.component';
import { UsersPublishersListComponent } from './users-publishers-list/users-publishers-list.component';
import { UsersPublishersManageComponent } from './users-publishers-manage/users-publishers-manage.component';

export const USERS_PUBLISHERS_ROUTING: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {
    path: 'list', component: UsersPublishersListComponent,
    data: {
      name: 'titles.pub.list',
      menuList: 'users',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'create', component: UsersPublishersManageComponent,
    data: {
      name: 'titles.pub.create',
      menuList: 'users',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'edit/:id', component: UsersPublishersManageComponent,
    resolve: {
      publisher: UsersPublishersResolver
    },
    data: {
      name: 'titles.pub.edit',
      menuList: 'users',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  },
  {
    path: 'check/:id', component: CheckPublisherComponent,
    resolve: {
      publisher: UsersPublishersResolver
    },
    data: {
      name: 'titles.pub.check',
      menuList: 'users',
      roles: ['ROLE_MANAGER', 'ROLE_ADMIN']
    }
  }
];
