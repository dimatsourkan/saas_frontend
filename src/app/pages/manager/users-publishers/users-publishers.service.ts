import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RxwebValidators } from '@rxweb/reactive-form-validators';

@Injectable({
  providedIn: 'root'
})
export class UsersPublishersService {

  constructor(private formBuilder: FormBuilder) {
  }


  getForm(): FormGroup {
    return this.formBuilder.group({
      username: ['', Validators.required],
      person: this.formBuilder.group({
        firstName: [''],
        lastName: ['']
      }),
      email: ['', [Validators.required, Validators.email]],
      additionalEmails: [],
      skype: [],
      w8w9FileId: [],
      phone: ['', [Validators.required, Validators.pattern(/^\+?[0-9]{8,20}$/i)]],
      address: this.formBuilder.group({
        address: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required],
        countryCode: ['', Validators.required],
        zipcode: ['', Validators.required],
        additionalAddress: this.formBuilder.array([])
      }),
      redirectType: [],
      status: [0, Validators.required],
      isOwnPublisher: [0, Validators.required],
      affiliateSource: '',
      skipAffiliate: '',
      autoApprove: ['0', Validators.required],
      tds: [0, Validators.required],
      antiFraud: ['0', Validators.required],
      badTraffic: [0, Validators.required],
      blockBadTraffic: ['false', Validators.required],
      ipWhitelist: this.formBuilder.group({
        enabledIpWhitelist: [0],
        ipWhitelist: []
      }),
      postbackUrl: [],
      postbackSuperlinkUrl: [],
      eventPostbacks: this.formBuilder.array([]),
      offerPostbacks: this.formBuilder.array([]),
      macroses: '',
      percentPay: [, [Validators.min(0), Validators.max(100), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      percentSkip: ['', [Validators.min(0), Validators.max(100)]],
      offerRequestLimit: this.formBuilder.group({
        offerRequestLimit: [, [Validators.min(0)]],
        offerRequestLeft: [, [Validators.min(0)]]
      }),
      paymentTerms: ['', Validators.required],
      paymentTermsSuperlink: ['', Validators.required],
      resaleEnabled: [false],
      resalePercent: ['', [Validators.min(0), Validators.max(100)]],
      resale: this.formBuilder.group({
        enabled: [false],
        percent: ['', [Validators.min(0), Validators.max(100)]],
      }),
      paymentInfo: this.formBuilder.group({
        beneficiaryName: '',
        beneficiaryAccount: '',
        beneficiaryAddress: '',
        iban: '',
        bankName: '',
        bankAddress: '',
        routing: '',
        swift: '',
        epaymentsId: '',
        wmz: '',
        paypalEmail: ''
      }),
      agreementFileId: [],
      notes: '',
      invoiceNotes: '',
      manager: this.formBuilder.group({
        id: ['', Validators.required]
      }),
      password: ['', Validators.required],
      confirmPassword: ['', [Validators.required, RxwebValidators.compare({ fieldName: 'password' })]]
    });
  }

  getAddressForm() {
    return this.formBuilder.group({
      address: [],
      city: [],
      state: [],
      countryCode: [],
      zipcode: [],
    });
  }

  getEventPostbackForm() {
    return this.formBuilder.group({
      goalId: [],
      url: []
    });
  }

  getOfferPostbackForm() {
    return this.formBuilder.group({
      offerGoalId: [],
      offerId: [],
      url: []
    });
  }
}
