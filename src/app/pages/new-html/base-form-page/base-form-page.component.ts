import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-form-page',
  templateUrl: './base-form-page.component.html',
  styleUrls: ['./base-form-page.component.scss']
})
export class BaseFormPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
