import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { BaseFormPageComponent } from './base-form-page/base-form-page.component';
import { FormElementsComponent } from './form-elements/form-elements.component';
import { TablesElementsComponent } from './tables-elements/tables-elements.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'form-page', component: BaseFormPageComponent },
  { path: 'form-element', component: FormElementsComponent },
  { path: 'tables-element', component: TablesElementsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewHtmlRoutingModule { }
