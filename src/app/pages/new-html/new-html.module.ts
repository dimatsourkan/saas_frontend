import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewHtmlRoutingModule } from './new-html-routing.module';
import { FormElementsComponent } from './form-elements/form-elements.component';
import { TablesElementsComponent } from './tables-elements/tables-elements.component';
import { BaseFormPageComponent } from './base-form-page/base-form-page.component';
import { IndexComponent } from './index/index.component';
import {SharedModule} from '@app/shared/shared.module';

@NgModule({
  declarations: [FormElementsComponent, TablesElementsComponent, BaseFormPageComponent, IndexComponent],
  imports: [
    CommonModule,
    NewHtmlRoutingModule,
    SharedModule
  ]
})
export class NewHtmlModule { }
