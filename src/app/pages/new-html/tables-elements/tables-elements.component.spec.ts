import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablesElementsComponent } from './tables-elements.component';

describe('TablesElementsComponent', () => {
  let component: TablesElementsComponent;
  let fixture: ComponentFixture<TablesElementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablesElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
