import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as FileSaver from 'file-saver';
import { PubBillingService } from '@app/core/entities/pub-billing/pub-billing.service';
import { finalize } from 'rxjs/operators';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';


@Component({
  selector: 'app-pub-billing-view',
  templateUrl: './pub-billing-view.component.html',
  styleUrls: ['./pub-billing-view.component.scss']
})
export class PubBillingViewComponent implements OnInit {
  invoice: any = { invoiceCalculation: [] };
  loading: boolean;
  constructor(
    private swal: SweetAlertService,
    private route: ActivatedRoute,
    private service: PubBillingService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.service.get(params['id']).subscribe(res => this.invoice = res);
    });
  }

  downloadInvoice() {
    this.service.downloadInvoice(this.invoice.invoiceNumber)
      .pipe(finalize(() => this.loading = false))
      .subscribe((res) => {
        const file = new Blob([res.body], { type: 'application/pdf;charset=utf-8' });
        FileSaver.saveAs(file, 'invoice.pdf', true);
        this.swal.success('Complete');
      }, (err) => {
        this.swal.error(err.errors || 'Ooops! Something went wrong!');
      });
  }

  calculateTotal(): number {
    if (!this.invoice.amount) {
      return 0;
    }
    let total = this.invoice.amount.amount;
    this.invoice.parts.map(part => {
      if (part.type === 3) {
        total += part.amount.amount;
      } else if (part.type === 1 || part.type === 2) {
        total -= part.amount.amount;
      }
    });
    return total;
  }
}
