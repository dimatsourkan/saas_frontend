import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { PubBillingViewComponent } from './pub-billing-view/pub-billing-view.component';

import { PUB_BILLING_ROUTES } from './pub-billing.routing';
import { PubBillingComponent } from './pub-billing/pub-billing.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild(PUB_BILLING_ROUTES)
  ],
  declarations: [PubBillingComponent, PubBillingViewComponent]
})
export class PubBillingModule {
}
