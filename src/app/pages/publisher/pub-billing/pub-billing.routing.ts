import { Routes } from '@angular/router';
import { PubBillingViewComponent } from './pub-billing-view/pub-billing-view.component';
import { PubBillingComponent } from './pub-billing/pub-billing.component';

export const PUB_BILLING_ROUTES: Routes = [
  {
    path: '',
    component: PubBillingComponent
  },
  {
    path: 'billing-info/:id',
    component: PubBillingViewComponent
  }
];
