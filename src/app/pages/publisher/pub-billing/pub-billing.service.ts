import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../shared/services/queryParams/query-params.service';

@Injectable({
  providedIn: 'root'
})
export class PubBillingService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getInvoices(queryParams: any): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing/invoice${params}`, {headers: {toCamelCase: 'true'}});
  }

  getInvoice(id: any): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing/invoice/${id}`, {headers: {toCamelCase: 'true'}});
  }

  getInvoiceStatuses(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing/invoice/status`, {headers: {toCamelCase: 'true'}});
  }

  getPaymentTerms(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher/payment_terms`, {headers: {toCamelCase: 'true'}});
  }

  getBillingParts(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing/part_type`, {headers: {toCamelCase: 'true'}});
  }

  downloadInvoice(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing/invoice/download/${id}`, {
      responseType: 'arraybuffer',
      observe: 'response',
      headers: {
        'Accept-Language': 'en'
      }
    });
  }

}
