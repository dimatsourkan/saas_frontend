import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PubBillingService } from '@app/core/entities/pub-billing/pub-billing.service';
import { PubBillingFilter } from '@app/core/entities/pub-billing/pub-billing.filter';
import { DataStore } from '@app/core/entities/data-store.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-pub-billing',
  templateUrl: './pub-billing.component.html',
  styleUrls: ['./pub-billing.component.scss']
})
export class PubBillingComponent implements OnInit {
  loading: boolean;
  subscription$: Subscription;
  filter: PubBillingFilter;
  invoices$ = DataStore.pubBilling.list.asObservable$;
  statuses$ = DataStore.pubBilling.statuses.asObservable$;

  constructor(
    private service: PubBillingService) {
  }

  ngOnInit() {
    this.filter = (new PubBillingFilter(true)).init();
    this.service.getStatusesCache().subscribe();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getInvoices(filter));
  }

  getInvoices(filter: PubBillingFilter) {
    this.loading = true;
    this.service.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: PubBillingFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  calendarCallback({ startDate, endDate }) {
    this.filter.from = startDate;
    this.filter.to = endDate;
  }

  changeStatus(status) {
    this.filter.status = status ? status.id : null;
  }

}
