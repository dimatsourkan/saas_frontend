import { Component, Input, OnInit } from '@angular/core';
import { ConversionsChartItem, PubDashboardService } from '@app/pages/publisher/pub-dashboard/pub-dashboard.service';
import { LineChartDatasetDark, LineChartDatasetOrange } from '@app/shared/components/charts/linear-chart/linear-chart-dataset.model';
import { LanguageService } from '@app/shared/services/language/language.service';
import { TranslateService } from '@ngx-translate/core';
import { ChartTooltipItem } from 'chart.js';
import * as moment from 'moment';
import { DataStore } from '@app/core/entities/data-store.service';

let translates: any = {};

@Component({
  selector: 'app-conversions',
  templateUrl: './conversions.component.html',
  styleUrls: ['./conversions.component.scss']
})
export class ConversionsComponent implements OnInit {

  @Input() private period: number;
  currency: string;
  chartData = new LineChartDatasetOrange<ConversionsChartItem>();
  chartData2 = new LineChartDatasetDark<ConversionsChartItem>();
  chartLabels: string[] = [];
  payoutChartData: ConversionsChartItem[] = [];

  constructor(
    private dashboardService: PubDashboardService,
    private languageService: LanguageService,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {

    this.languageService.getLang().subscribe(() => {
      this.setChartData();
    });

    DataStore.currency.current.asObservable$
    .subscribe((currency) => {
      this.currency = currency;
      this.getConversionsChart();
    });

    this.translateService.onLangChange.subscribe(lang => {
      translates = lang.translations.dashboard;
    });

    this.translateService.get('dashboard').subscribe(res => {
      translates = res;
    });
  }

  getConversionsChart() {
    this.dashboardService.getConversionsChart({
      currency: this.currency,
      period: this.period
    }).subscribe((response) => {
      this.payoutChartData = response;
      this.setChartData();
    });
  }

  setChartData() {
    this.chartData.fullData = this.payoutChartData;
    this.chartData2.fullData = this.payoutChartData;
    this.chartData.data = this.payoutChartData.map(d => d.clicks);
    this.chartData2.data = this.payoutChartData.map(d => d.conversions);
    this.chartLabels = this.payoutChartData.map(d => d.date);
  }

  titleTransformCallback(item: ChartTooltipItem[], data: { datasets: LineChartDatasetDark<ConversionsChartItem> }) {
    const index = item[0].index;
    const dataseIndex = item[0].datasetIndex;
    const fullData: ConversionsChartItem = data.datasets[dataseIndex].fullData[index];
    return moment(fullData.initialDate).format('DD MMMM YYYY');
  }

  labelTransformCallback(item: ChartTooltipItem, data: { datasets: LineChartDatasetDark<ConversionsChartItem> }) {
    const index = item.index;
    const dataseIndex = item.datasetIndex;
    const fullData: ConversionsChartItem = data.datasets[dataseIndex].fullData[index];
    return [
      `${translates.conversions} : ${fullData.conversions}`,
      `${translates.clicks} : ${fullData.clicks}`
    ];
  }

}
