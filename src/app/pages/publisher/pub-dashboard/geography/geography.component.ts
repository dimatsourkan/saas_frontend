import { Component, Input, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { PubDashboardService } from '@app/pages/publisher/pub-dashboard/pub-dashboard.service';
import { DoughnutChartDataset } from '@app/shared/components/charts/doughnut-chart/doughnut-chart-dataset.model';

@Component({
  selector: 'app-geography',
  templateUrl: './geography.component.html',
  styleUrls: ['./geography.component.scss']
})
export class GeographyComponent implements OnInit, OnChanges {

  @Input() private currency: string;
  @Input() private period: number;

  geoData: any = [];
  clicksPieChartParams = new DoughnutChartDataset();
  conversionsPieChartParams = new DoughnutChartDataset();
  payoutPieChartParams = new DoughnutChartDataset();

  scrollConfig = {
    suppressScrollX: false,
    suppressScrollY: true
  };

  constructor(private dashboardService: PubDashboardService) {
  }

  ngOnInit() {
    this.getGeoChart();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.currency) {
      this.getGeoChart();
    }
  }

  getGeoChart() {
    this.dashboardService.getGeoChart({ currency: this.currency, period: this.period })
      .subscribe((response) => {
        this.clicksPieChartParams.setData(response.map(r => r.clicks));
        this.clicksPieChartParams.labels = response.map(r => r.country);
        this.conversionsPieChartParams.setData(response.map(r => r.conversions));
        this.conversionsPieChartParams.labels = response.map(r => r.country);
        this.payoutPieChartParams.setData(response.map(r => r.payout));
        this.payoutPieChartParams.labels = response.map(r => r.country);
        this.geoData = response;
      });
  }

}
