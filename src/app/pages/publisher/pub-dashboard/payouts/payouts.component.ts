import { Component, Input, OnInit } from '@angular/core';
import { PayoutChartItem, PubDashboardService } from '@app/pages/publisher/pub-dashboard/pub-dashboard.service';
import { LinearChartDataset, LineChartDatasetDark } from '@app/shared/components/charts/linear-chart/linear-chart-dataset.model';
import { LanguageService } from '@app/shared/services/language/language.service';
import { DataStore } from '@app/core/entities/data-store.service';
import { TranslateService } from '@ngx-translate/core';
import { ChartTooltipItem } from 'chart.js';
import * as moment from 'moment';

let translates: any = {};

@Component({
  selector: 'app-payouts',
  templateUrl: './payouts.component.html',
  styleUrls: ['./payouts.component.scss']
})
export class PayoutsComponent implements OnInit {

  @Input() private period: number;
  currency: string;
  chartData = new LinearChartDataset<PayoutChartItem>();
  chartData2 = new LineChartDatasetDark<PayoutChartItem>();
  chartLabels: string[] = [];

  payoutChartData: PayoutChartItem[] = [];

  constructor(
    private dashboardService: PubDashboardService,
    private languageService: LanguageService,
    private translateService: TranslateService,
  ) {
  }

  getPayoutChart() {
    this.dashboardService.getPayoutChart({
      currency: this.currency,
      period: this.period
    }).subscribe((response) => {
      this.payoutChartData = response;
      this.setChartData();
    });
  }

  ngOnInit() {

    DataStore.currency.current.asObservable$
    .subscribe((currency) => {
      this.currency = currency;
      this.getPayoutChart();
    });

    this.translateService.onLangChange.subscribe(lang => {
      translates = lang.translations.dashboard;
    });

    this.translateService.get('dashboard').subscribe(res => {
      translates = res;
    });
  }

  setChartData() {
    this.chartData.fullData = this.payoutChartData;
    this.chartData2.fullData = this.payoutChartData;
    this.chartData.data = this.payoutChartData.map(d => d.clicks);
    this.chartData2.data = this.payoutChartData.map(d => d.payout);
    this.chartLabels = this.payoutChartData.map(d => d.date);
  }

  titleTransformCallback(item: ChartTooltipItem[], data: { datasets: LineChartDatasetDark<PayoutChartItem> }) {
    const index = item[0].index;
    const dataseIndex = item[0].datasetIndex;
    const fullData: PayoutChartItem = data.datasets[dataseIndex].fullData[index];
    return moment(fullData.initialDate).format('DD MMMM YYYY');
  }

  labelTransformCallback(item: ChartTooltipItem, data: { datasets: LineChartDatasetDark<PayoutChartItem> }) {
    const index = item.index;
    const dataseIndex = item.datasetIndex;
    const fullData: PayoutChartItem = data.datasets[dataseIndex].fullData[index];
    return [
      `${translates.payout} : ${fullData.payout} ${fullData.currency}`,
      `${translates.clicks} : ${fullData.clicks}`
    ];
  }

}
