import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { Countries } from '../../../shared/interfaces/countries';
import { CountriesService } from '../../../shared/services/countries/countries.service';

import { SweetAlertService } from '../../../shared/services/sweet-alert/sweet-alert.service';
import { OffersService } from '../../manager/offers/offers.service';
import { PubOffersService } from '../pub-offers/pub-offers.service';
import { PubDashboardService } from './pub-dashboard.service';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'app-pub-dashboard',
  templateUrl: './pub-dashboard.component.html',
  styleUrls: ['./pub-dashboard.component.scss']
})
export class PubDashboardComponent implements OnInit {

  @ViewChild('descriptionModal') descriptionModal: ModalComponent;
  period = 6;
  limit = 10;
  periods: any;
  currency: any;
  topOffers: any = [];
  offerChartParams: any;
  filterOpen: boolean;
  loading: boolean;
  offer: any;
  offerTypes: any;
  totalOffers: any = {
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0
  };
  countries: Countries[];

  constructor(
    private translate: TranslateService,
    private _dashboardService: PubDashboardService,
    private countriesService: CountriesService,
    private offersService: OffersService,
    private _pubOffersService: PubOffersService,
    private sweetAlertService: SweetAlertService) {

  }

  ngOnInit() {
    DataStore.currency.current.asObservable$
    .subscribe(currency => {
      if (currency) {
        this.currency = currency;
        this.getTotalOffers();
        this.getTopOffers();
        this.getTopOffersChart();
      }
    });


    this.getPeriods();
    this.getCountries();
    this.getOfferTypes();
  }

  getOfferTypes() {
    this.offersService.getOfferTypes().subscribe(res => {
      this.offerTypes = res;
    });
  }

  getPeriods() {
    this._dashboardService.getPeriods()
    .subscribe((response) => this.periods = response);
  }

  getTopOffers() {
    this._dashboardService.getTopOffers({
      currency: this.currency,
      period: this.period,
      limit: this.limit
    })
    .subscribe((response) => {
      this.topOffers = response;

      this.topOffers.forEach((offer) => {
        offer.payouts = {
          amounts: [],
          amountPercents: []
        };

        offer.goals.forEach((goal) => {
          if (goal.dynamicPayout) {
            offer.payouts.amountPercents.push(goal.publisherPayout.amount);
          } else {
            offer.payouts.amounts.push(goal.publisherPayout.amount);
          }
        });

        this.generatePayoutString(offer, 'amounts', offer.currency);
        this.generatePayoutString(offer, 'amountPercents', '%');
      });
    });
  }

  getTotalOffers() {
    this._dashboardService.getTotalOffers({currency: this.currency, period: this.period})
    .subscribe((response) => {
      this.totalOffers = response;
    });
  }

  getTopOffersChart() {
    this._dashboardService.getTopOffersChart({currency: this.currency, period: this.period})
    .subscribe((response) => {
      this.offerChartParams = response;
    });
  }

  generatePayoutString(offer, flag, mark) {
    if (offer.payouts[flag].length) {
      offer.payouts[`${flag}Min`] = Math.min.apply(null, offer.payouts[flag]);
      offer.payouts[`${flag}Max`] = Math.max.apply(null, offer.payouts[flag]);
      if (offer.payouts[`${flag}Min`] !== offer.payouts[`${flag}Max`]) {
        offer.payouts[`${flag}ForFront`] = `${offer.payouts[`${flag}Min`]} \u2014 ${offer.payouts[`${flag}Max`]} ${mark}`;
      } else {
        offer.payouts[`${flag}ForFront`] = `${offer.payouts[`${flag}Min`]} ${mark}`;
      }
    }
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  closeRangeFilter() {
    this.filterOpen = false;
  }

  showOfferInfo(offerID) {
    this.getOffer(offerID);
  }

  toggleDescriptionModal() {
    this.descriptionModal.toggle();
  }

  getOffer(id) {
    this.loading = true;
    return this._dashboardService.getOffer(id).subscribe((res) => {
      this.offer = res;
      this.toggleDescriptionModal();
      // this.offer.description = this.$sce.trustAsHtml(this.offer.description);
      this._pubOffersService.applyOffersPayout(this.offer, this.offerTypes, 'publisherPayout');
      if (this.offer.showUserFlow) {
        this.translate.get(['dashboard.userFlowMessage']).subscribe((translation) => {
          this.offer.descriptionFlow = translation['dashboard.userFlowMessage'].split(/\d\./);
        });
      }
      this.applyTraffic(this.offer);
      this.applyCountryNames(this.offer);
      this.loading = false;
    });
  }

  getCountries() {
    this.countriesService.getCountries().subscribe(res => {
      this.countries = res;
    });
  }

  applyCountryNames(offer) {
    offer.goals.forEach((goal) => {
      goal.countries = goal.countries.map((el) => this.countries.find(item => item.code === el));
    });
  }


  applyTraffic(offer) {
    offer.allowedTraffic = [];
    offer.restrictedTraffic = [];
    offer.restrictions.forEach((item) => {
      if (item.state === 1) {
        offer.allowedTraffic.push(item);
      } else {
        offer.restrictedTraffic.push(item);
      }
    });
  }

  approveOffersRequest(id) {
    this.sweetAlertService.confirm('Confirm request?').then((isConfirm) => {
      if (isConfirm.value) {
        this.loading = true;
        this._dashboardService.approveOffersRequest([id]).subscribe((res) => {
          this.loading = false;
          this.sweetAlertService.success('Done!');
        }, (err) => {
          this.loading = false;
          this.sweetAlertService.error(err.errors);

        });
      } else {
        this.sweetAlertService.error('Cancelled!');
      }
    });

  }

  getExpirationDaysLeft(date) {
    return Math.floor((moment(date).valueOf() - Date.now()) / (1000 * 3600 * 24));
  }

}
