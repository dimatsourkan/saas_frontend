import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { ConversionsComponent } from './conversions/conversions.component';
import { GeographyComponent } from './geography/geography.component';
import { PayoutsComponent } from './payouts/payouts.component';
import { PubDashboardComponent } from './pub-dashboard.component';

import { PUB_DASHBOARD_ROUTING } from './pub-dashboard.routing';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild(PUB_DASHBOARD_ROUTING)
  ],
  declarations: [PubDashboardComponent, PayoutsComponent, ConversionsComponent, GeographyComponent]
})
export class PubDashboardModule {
}
