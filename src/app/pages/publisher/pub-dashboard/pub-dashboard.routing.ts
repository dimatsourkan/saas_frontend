import { Routes } from '@angular/router';
import { PubDashboardComponent } from './pub-dashboard.component';

export const PUB_DASHBOARD_ROUTING: Routes = [
  {
    path: '',
    component: PubDashboardComponent
  }
];
