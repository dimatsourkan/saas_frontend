import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { BaseUrlService } from '../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../shared/services/queryParams/query-params.service';


export class PayoutChartItem {
  clicks: number;
  currency: string;
  initialDate: string;
  payout: number;
  momentDate: Moment;

  constructor(data: any) {
    this.clicks = data.clicks;
    this.currency = data.currency;
    this.payout = data.payout;
    this.initialDate = data.date;
    this.momentDate = moment(data.date);
  }

  get date() {
    return moment(this.momentDate.format()).format('DD MMM');
  }
}

export class ConversionsChartItem {
  clicks: number;
  currency: string;
  initialDate: string;
  conversions: number;
  momentDate: Moment;

  constructor(data: any) {
    this.clicks = data.clicks;
    this.currency = data.currency;
    this.conversions = data.conversions;
    this.initialDate = data.date;
    this.momentDate = moment(data.date);
  }

  get date() {
    return moment(this.momentDate.format()).format('DD MMM');
  }
}

export class GeoChartItem {
  clicks: number;
  conversions: number;
  country: string;
  currency: string;
  payout: number;

  constructor(data: any) {
    this.clicks = parseInt(data.clicks, 10);
    this.conversions = parseInt(data.conversions, 10);
    this.country = data.country;
    this.currency = data.currency;
    this.payout = data.payout;
  }
}

@Injectable({
  providedIn: 'root'
})
export class PubDashboardService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getTopOffers(queryParams: any): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/dashboard/top_offers${params}`, {headers: {toCamelCase: 'true'}});
  }

  getPeriods(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/dashboard/periods`, {headers: {toCamelCase: 'true'}});
  }

  getTotalOffers(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/dashboard/total_offers${params}`, {headers: {toCamelCase: 'true'}});
  }

  getConversionsChart(queryParams): Observable<ConversionsChartItem[]> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/dashboard/chart/conversions${params}`, {headers: {toCamelCase: 'true'}})
      .pipe(map((res: any[]) => res.map(d => new ConversionsChartItem(d))))
      .pipe(map(res => res.sort((a, b) => a.momentDate.isAfter(b.momentDate) ? 1 : -1)));
  }

  getPayoutChart(queryParams): Observable<PayoutChartItem[]> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/dashboard/chart/payout${params}`, {headers: {toCamelCase: 'true'}})
      .pipe(map((res: any[]) => res.map(d => new PayoutChartItem(d))))
      .pipe(map(res => res.sort((a, b) => a.momentDate.isAfter(b.momentDate) ? 1 : -1)));
  }

  getGeoChart(queryParams): Observable<GeoChartItem[]> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/dashboard/chart/geo${params}`, {headers: {toCamelCase: 'true'}})
      .pipe(map((res: any[]) => res.map(d => new GeoChartItem(d))));
  }

  getTopOffersChart(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/dashboard/chart/top_offers${params}`, {headers: {toCamelCase: 'true'}});
  }

  getOffer(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/${id}`, {headers: {toCamelCase: 'true'}});
  }

  approveOffersRequest(ids): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/offer/request`, {ids}, {headers: {toCamelCase: 'true'}});
  }
}
