import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PubInvoicesComponent } from './pub-invoices.component';

import { PUB_INVOICES_ROUTING } from './pub-invoices.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PUB_INVOICES_ROUTING)
  ],
  declarations: [PubInvoicesComponent]
})
export class PubInvoicesModule {
}
