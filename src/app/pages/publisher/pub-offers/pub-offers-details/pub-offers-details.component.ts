import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// import {BillingAdvertisersService} from '../billing-advertisers.service';
import { TranslateService } from '@ngx-translate/core';
import uniq from 'lodash-es/uniq';
import * as moment from 'moment';
import { forkJoin } from 'rxjs';
import { CountriesService } from '../../../../shared/services/countries/countries.service';
import { LanguageService } from '../../../../shared/services/language/language.service';

import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';
import { PubOffersService } from '../pub-offers.service';

@Component({
  selector: 'app-pub-offers-details',
  templateUrl: './pub-offers-details.component.html',
  styleUrls: ['./pub-offers-details.component.scss']
})
export class PubOffersDetailsComponent implements OnInit {
  offerId: any;
  click_id: any;
  aff_sub: any;
  aff_sub2: any;
  aff_sub3: any;
  aff_sub4: any;
  affSub: {}[];
  loading: boolean;
  offer: any;
  offerStatuses: any;
  offerTypes: any;
  selectedTrackingUrl: any;
  selectedOffers: any;
  countries: any;

  constructor(
    private swal: SweetAlertService,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private _pubOffersService: PubOffersService,
    private languageService: LanguageService,
    private queryParamsService: QueryParamsService,
    private countriesService: CountriesService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.offerId = params['id'];
    });

    this.initLocale();
    this.affSub = [{}, {}, {}, {}];

    // this.$scope.$on('newLocale', () => {
    //   this.$translate(['manageOffers.userFlowMessage']).then((translation) => {
    //     this.offer.descriptionFlow = translation['manageOffers.userFlowMessage'].split(/\d\./);
    //   });
    // });
    this.getCountries();
    this.getData();
  }

  getData() {
    forkJoin([this.getOfferApprovalStatuses(), this.getOffer(), this.getOfferTypes()])
      .subscribe(response => {
        this.dataReady(response[0], response[1], response[2]);
      });
  }

  initLocale(): void {
    this.languageService.getLang().subscribe(() => {
      this.getData();
    });
  }

  getCountries() {
    this.countriesService.getCountries().subscribe(res => {
      this.countries = res;
    });
  }

  dataReady(statuses, offer, types) {
    this.offer = offer;
    if (this.offer.showUserFlow) {
      this.translate.get(['manageOffers.userFlowMessage', 'manageOffers.capsMessage'])
        .subscribe((translation) => {
          this.offer.descriptionFlow = translation['manageOffers.userFlowMessage'].split(/\d\./);
          this.offer.capsMessage = translation['manageOffers.capsMessage'];
        });
    }
    this.addCountryNames(this.offer);
    this.applyTraffic(this.offer);
    this.offerStatuses = statuses;
    this.offerTypes = types;
    this._pubOffersService.applyOffersPayout(this.offer, this.offerTypes, 'publisherPayout');
  }

  applyOffersPayout(offer, offerTypes, flag, extend?) {
    offerTypes.forEach((type) => {
      if (offer.type === type.id) {
        offer.typeName = type.value;
      }
    });
    offer.countries = [];
    offer.platforms = [];
    offer.payouts = {
      amount: [],
      amountPercents: [],
      percents: [],
      pubPayouts: [],
      pubPayoutsPercent: []
    };

    offer.goals.forEach((goal) => {
      if (!goal.excludeCountries && goal.aggregateCountries) {
        goal.aggregateCountries.forEach((country) => {
          offer.countries.push(country.code);
        });
      }

      if (goal.aggregatePlatforms) {
        goal.aggregatePlatforms.forEach((platform) => {
          offer.platforms.push(platform.name);
        });
      }

      if (extend) {
        offer.payouts.percents.push(goal.payPercent);
        goal.pubPayout = ((goal[flag].amount * goal.payPercent) / 100).toFixed(2);
      }


      if (goal.dynamicPayout) {
        offer.payouts.amountPercents.push(goal[flag].amount);
        if (extend) {
          offer.payouts.pubPayoutsPercent.push(goal.pubPayout);
        }
      } else {
        offer.payouts.amount.push(goal[flag].amount);
        if (extend) {
          offer.payouts.pubPayouts.push(goal.pubPayout);
        }
      }
    });

    offer.countries = uniq(offer.countries);
    offer.platforms = uniq(offer.platforms);

    this._generatePayoutString(offer, 'amount', offer.currency);
    this._generatePayoutString(offer, 'amountPercents', '%');
    if (extend) {
      this._generatePayoutString(offer, 'pubPayouts', offer.currency);
      this._generatePayoutString(offer, 'pubPayoutsPercent', '%');
      this._generatePayoutString(offer, 'percents', '%');
    }
  }

  _generatePayoutString(offer, flag, mark) {
    if (offer.payouts[flag].length) {
      offer.payouts[`${flag}Min`] = Math.min.apply(Math, offer.payouts[flag]);
      offer.payouts[`${flag}Max`] = Math.max.apply(Math, offer.payouts[flag]);
      if (offer.payouts[`${flag}Min`] !== offer.payouts[`${flag}Max`]) {
        offer.payouts[`${flag}ForFront`] = `${offer.payouts[`${flag}Min`]} \u2014 ${offer.payouts[`${flag}Max`]} ${mark}`;
      } else {
        offer.payouts[`${flag}ForFront`] = `${offer.payouts[`${flag}Min`]} ${mark}`;
      }
    }
  }

  addCountryNames(offer) {
    offer.goals.forEach((item) => {
      item.countries.forEach((el, i) => {
        item.countries[i] = this.countries.find((counry) => counry.code === el);
      });
    });
  }

  applyTraffic(offer) {
    offer.allowedTraffic = [];
    offer.restrictedTraffic = [];
    offer.restrictions.forEach((item) => {
      if (item.state === 1) {
        offer.allowedTraffic.push(item);
      } else {
        offer.restrictedTraffic.push(item);
      }
    });
  }

  getOffer() {
    return this._pubOffersService.getOffer(this.offerId);
  }

  getOfferApprovalStatuses() {
    return this._pubOffersService.getOfferApprovalStatuses();
  }

  getOfferTypes() {
    return this._pubOffersService.getOfferTypes();
  }

  getExpirationDaysLeft(date) {
    return Math.floor((moment(date).valueOf() - Date.now()) / (1000 * 3600 * 24));
  }

  changeOffersLink(flag) {
    const link = this.offer.trackingUrl.split('?');
    const params = this.getParams(link[1]);
    params[flag] = this[flag];
    this.offer.trackingUrl = `${link[0]}${this.queryParamsService.paramsObjectToString(params)}`;
  }

  getParams(query) {
    if (!query) {
      return {};
    }
    return (/^[?]/.test(query) ? query.slice(1) : query).split('&').reduce((params, param) => {
      const [key, value] = param.split('=');
      params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
      return params;
    }, {});
  }

  // onLinkChange() {
  //   let queryParams = this.affSub.reduce((acc, item, i) => {
  //     return item.value ? acc + `${i ? '&' : ''}aff_sub${(i ? i + 1 : '')}=${item.value}` : acc;
  //   }, '');
  //   this.currentTrackingUrl = this.selectedTrackingUrl + (queryParams.length ? '?' + queryParams : '');
  // }

  downloadCreative(creative) {
    const link = document.createElement('a');
    link.download = creative.name;
    link.href = creative.url;
    link.click();
  }

  approveOffersRequest(id) {
    this.swal.confirm('Confirm request?').then((isConfirm) => {
      if (isConfirm.value) {
        this.loading = true;
        const ids = id ? [id] : this.selectedOffers.map((item) => item.id);
        this._pubOffersService.approveOffersRequest(ids).subscribe((res) => {
          this.loading = false;
          this.approveOffers();
          this.swal.success('Done!');
        }, (err) => {
          this.loading = false;
          this.swal.error(err.errors);
        });
      } else {
        this.swal.error('Cancelled!');
      }
    });
  }

  approveOffers() {
    this.offer.offerApproval = {status: 1};
    this.offer.approvalRequired = true;
    this.offer.isChecked = false;
  }

}
