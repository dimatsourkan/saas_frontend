import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubOffersFilterComponent } from './pub-offers-filter.component';

describe('PubOffersFilterComponent', () => {
  let component: PubOffersFilterComponent;
  let fixture: ComponentFixture<PubOffersFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubOffersFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubOffersFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
