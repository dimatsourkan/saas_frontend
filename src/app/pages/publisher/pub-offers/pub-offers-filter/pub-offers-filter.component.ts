import { Component, OnInit } from '@angular/core';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { PubOfferFilter } from '@app/core/entities/pub-offer/pub-offer.filter';
import { PubOfferService } from '@app/core/entities/pub-offer/pub-offer.service';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'pub-offers-filter',
  templateUrl: './pub-offers-filter.component.html',
  styleUrls: ['./pub-offers-filter.component.scss']
})
export class PubOffersFilterComponent extends AbstractFilterComponent<PubOfferFilter> implements OnInit {

  filter = (new PubOfferFilter()).init();
  trafficTypes$ = DataStore.pubOffer.trafficTypes.asObservable$;

  constructor(private service: PubOfferService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.service.getTrafficTypesCache().subscribe();
    this.filter.exclude(['page', 'limit', 'search']);
  }

}
