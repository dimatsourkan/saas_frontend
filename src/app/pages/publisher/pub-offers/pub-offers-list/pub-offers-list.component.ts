import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { forkJoin } from 'rxjs';
import { ModalComponent } from '@app/new-shared/components/modals/modal/modal.component';
import { CountriesService } from '../../../../shared/services/countries/countries.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../shared/services/sweet-alert/sweet-alert.service';
import { TrafficTypeService } from '../../../../shared/services/traffic-type/traffic-type.service';
import { ApprovalsService } from '../../../manager/offers/approvals/approvals.service';
import { PubOffersService } from '../pub-offers.service';

@Component({
  selector: 'app-pub-offers-list',
  templateUrl: './pub-offers-list.component.html',
  styleUrls: ['./pub-offers-list.component.scss']
})
export class PubOffersListComponent implements OnInit {

  @ViewChild('perfectScrollbar') directiveRef?: ElementRef;
  @ViewChild('descriptionModal') descriptionModal: ModalComponent;

  categoryIsOpen: any;
  dateFormat: string;
  advanceSearchModal: boolean;
  filterPopups: any;
  filter: any;
  loading: boolean;
  externalsLimit: any;
  currentParams: any;
  params: any;
  offers: any = {items: []};
  offer: any;
  selectedOffers: any[];
  allOffersChecked: boolean;
  advanceSearchFilter: { order: any; sort: any; };
  offerTypes: any;
  offerStatuses: any;
  countries: any;
  trafficTypes: any[] = [];
  isOffers: boolean;
  filterSchema = {
    sort: [
      {key: 'date', value: 'Date'},
      {key: 'name', value: 'Name'},
      {key: 'country', value: 'Country'},
      {key: 'platform', value: 'Platform'},
      {key: 'category', value: 'Category'}
    ],
    order: [
      {key: 'asc', value: 'Ascending', className: 'fa-sort-alpha-asc'},
      {key: 'desc', value: 'Descending', className: 'fa-sort-alpha-desc'}
    ]
  };
  lang: string;

  constructor(
    private route: ActivatedRoute,
    private _approvalsService: ApprovalsService,
    private translate: TranslateService,
    private queryParamsService: QueryParamsService,
    private _pubOffersService: PubOffersService,
    private trafficTypeService: TrafficTypeService,
    private countriesService: CountriesService,
    private sweetAlertService: SweetAlertService) {

  }

  scrollToTop() {
    this.directiveRef.nativeElement.scrollTop = 0;
  }

  ngOnInit() {
    this.dateFormat = moment.localeData().longDateFormat('L');
    this.advanceSearchModal = false;
    this.filterPopups = {};
    this.filter = {};
    this.loading = false;
    this.currentParams = this.queryParamsService.getParams();
    this.trafficTypeService.getTrafficTypes().subscribe(trafficTypes => this.trafficTypes = trafficTypes);
    this.route.queryParams.subscribe(params => {
      if (params['featured']) {
        this.currentParams.featured = params['featured'];
        this.params = this.deepCopy(this.currentParams);
      } else {
        if (this.params) {
          delete this.params.featured;
        }
        // this.params = this.deepCopy(this.currentParams);
      }
      // this.params = params;
      this.getOffers();
    });
    this.route.data
    .subscribe((data) => {
      if (data.name === 'Approvals') {
        this.currentParams.approved = 1;
      } else if (data.name === 'Offers') {
        this.isOffers = true;
      }
    });

    this.params = this.deepCopy(this.currentParams);
    delete this.currentParams.page;

    this.getCountries();

    forkJoin([this.getOfferStatuses()])
    .subscribe(response => {
      this.offerStatuses = response[0];
      this.getOffers();
    });

    this.selectedOffers = [];
    this.allOffersChecked = false;
    this.initFilters(this.params);
  }

  getCountries() {
    this.countriesService.getCountries().subscribe(res => {
      this.countries = res;
    });
  }

  initFilters(params) {
    this.advanceSearchFilter = {order: null, sort: null};
    this.filter = {order: null, sort: null};
    // angular.forEach(params, (paramValue, paramKey) => {
    //   angular.forEach(this.advanceSearchFilter, (advanceFilter, advanceFilterKey) => {
    //     if (paramKey === advanceFilterKey) this.advanceSearchFilter[paramKey] = paramValue;
    //   });
    //   angular.forEach(this.filter, (filter, filterKey) => {
    //     if (paramKey === filterKey) this.filter[paramKey] = paramValue;
    //   });
    // });
  }

  toggleAdvanceFilterModal() {
    this.advanceSearchModal = !this.advanceSearchModal;
  }

  toggleFilterVisibility(flag) {
    this.filterPopups[flag] = !this.filterPopups[flag];
  }

  showOfferInfo(offerID) {
    this.getOffer(offerID);
  }

  toggleDescriptionModal() {
    this.descriptionModal.toggle();
  }

  setOfferType(type) {
    return this.offerTypes.find((item) => item.id === type).value;
  }

  getOffers() {
    this.loading = true;
    this._pubOffersService.getOffers(this.params).subscribe((data) => {
      this.offers = data;
      this.params = Object.assign({}, this.params, {page: data.page, limit: data.limit});
      this.queryParamsService.setParams(this.params);
      this.offers.items.forEach((item) => {
        // this.applyCountryNames(item);
        this.applyOfferStatusNames(item);
        item.trafficType = this.trafficTypes.filter(type => type.id === item.trafficType)[0];
      });
      this.loading = false;
      this.scrollToTop();
    }, err => {
      this.loading = false;
      this.sweetAlertService.error(err.errors);
    });
  }

  applyOfferStatusNames(item) {
    const status = this.offerStatuses.find((el) => el.id === item.status);
    if (status) {
      item.statusName = status.name;
      item.statusOrigin = status.origin;
    }
  }

  getOfferStatuses() {
    return this._approvalsService.getOfferStatuses();
  }

  applyCountryNames(offer) {
    // offer.goals.forEach((goal) => {
    // goal.countries = goal.countries.map((el) => this.countries.find(item => item.code === el));
    // });
  }

  applyTraffic(offer) {
    offer.allowedTraffic = [];
    offer.restrictedTraffic = [];
    offer.restrictions.forEach((item) => {
      if (item.state === 1) {
        offer.allowedTraffic.push(item);
      } else {
        offer.restrictedTraffic.push(item);
      }
    });
  }

  getOffer(id) {
    this.loading = true;
    return this._pubOffersService.getOffer(id).subscribe((res) => {
      this.offer = res;
      if (this.offer.showUserFlow) {
        this.translate.get(['manageOffers.userFlowMessage', 'manageOffers.capsMessage'])
        .subscribe((translation) => {
          this.offer.descriptionFlow = translation['manageOffers.userFlowMessage'].split(/\d\./);
          this.offer.capsMessage = translation['manageOffers.capsMessage'];
        });
      }
      this.applyTraffic(this.offer);
      // this.applyCountryNames(this.offer);
      this.loading = false;
      this.toggleDescriptionModal();
    });
  }

  applyAdvanceSearchFilter() {
    this.params.page = 1;
    for (const prop in this.currentParams) {
      if (this.currentParams[prop] === 0) {
        delete this.currentParams[prop];
        delete this.params[prop];
      }
    }
    // for (const prop in this.currentParams) {
    //   if (this.currentParams.hasOwnProperty(prop)) {
    //     if (this.currentParams[prop] === 0 || typeof this.currentParams[prop] === 'undefined') {
    //       delete this.currentParams[prop];
    //     } else if (typeof this.currentParams[prop] === 'boolean' && typeof this.currentParams[prop]) {
    //       this.currentParams[prop] = 1;
    //     }
    //   }
    // }
    this.params = Object.assign({}, this.params, this.currentParams);
    // if (this.params.approval) {
    //   this.params.approval = this.params.approval.map(item => Number(item.replace(/^\D+/g, '')));
    // }
    this.getOffers();
    this.toggleAdvanceFilterModal();
  }

  deletePropsInObj(obj, arr) {
    arr.map((item) => item in obj ? delete obj[item] : '');
  }

  resetAdvanceSearchFilter() {
    this.loading = true;
    // this.advanceSearchFilter = {};
    this.deletePropsInObj(this.params, ['new', 'approved', 'platforms', 'countries', 'trafficType', 'tags']);
    this.deletePropsInObj(this.currentParams, ['new', 'approved', 'platforms', 'countries', 'trafficType', 'tags']);
    this.params.search = null;
    this.params.page = 1;
    this.params.limit = this.offers.limit;
    this.selectedOffers = [];
    this.allOffersChecked = false;
    this.params = Object.assign({}, this.params, this.advanceSearchFilter);
    this.getOffers();
    this.toggleAdvanceFilterModal();
  }

  tableLimit(newLimit) {
    this.params.page = 1;
    this.params.limit = newLimit;
    this.getOffers();
  }

  goToPage(page) {
    this.params.page = page;
    this.getOffers();
  }


  setFilter(type, value) {
    this.filter[type] = value;
    this.filterPopups[type] = false;
  }

  applyFilters(search?) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = this.offers.limit;
    for (const prop in this.filter) {
      if (this.filter.hasOwnProperty(prop)) {
        if (typeof this.filter[prop] === 'undefined') {
          this.filter[prop] = null;
        }
      }
    }
    this.params.search = search;
    this.params = Object.assign({}, this.params, this.filter);
    this.getOffers();
  }

  resetFilters() {
    this.loading = true;
    this.filter = {
      order: null,
      sort: null,
      type: null
    };
    delete this.params.order;
    delete this.params.sort;
    delete this.params.type;

    this.params.search = null;
    this.params.page = 1;
    this.params.limit = this.offers.limit;
    this.params = Object.assign({}, this.params, this.filter);
    this.getOffers();
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  closeFilterByDomClick({close}) {
    if (close) {
      setTimeout(() => this.filterPopups = {});
    }
  }

  disabledFiltersButton() {
    if (Object.keys(this.filter).length) {
      return Object.keys(this.filter).some((key) => {
        return this.filter[key] !== null;
      });
    }
    return false;
  }

  getExpirationDaysLeft(date) {
    return Math.floor((moment(date).valueOf() - Date.now()) / (1000 * 3600 * 24));
  }

  checkingOffer(item) {
    if (item.isChecked) {
      this.selectedOffers.push(item);
    } else {
      this.selectedOffers = this.selectedOffers.filter((el) => el.id !== item.id);
    }
  }

  checkingAllOffer() {
    if (this.allOffersChecked) {
      this.offers.items.map((item) => {
        if (item.approvalRequired && !item.offerApproval) {
          item.isChecked = true;
        }
      });
      this.selectedOffers = Object.assign(this.offers.items, {});
    } else {
      this.offers.items.map((item) => item.isChecked = false);
      this.selectedOffers = [];
    }
  }

  approveOffersRequest(id?) {
    this.sweetAlertService.confirm('Confirm request?').then((isConfirm) => {
      if (isConfirm.value) {
        this.loading = true;
        const ids = id ? [id] : this.selectedOffers.map((item) => item.id);
        this._pubOffersService.approveOffersRequest(ids).subscribe((res) => {
          this.approveOffers(res);
          this.sweetAlertService.success('Done!');
        }, (err) => {
          this.loading = false;
          this.sweetAlertService.error(err.errors);
        });
      } else {
        this.sweetAlertService.error('Cancelled!');
      }
    });
  }

  approveOffers(data) {
    // tslint:disable-next-line:forin
    for (const prop in data) {
      // this.offers.items.find((item) => item.id === prop).status = data[prop];
      this.offers.items.forEach((item) => {
        if (item.id in data) {
          item.offerApproval = {status: data[item.id]};
          item.approvalRequired = true;
          item.isChecked = false;
        }
      });
    }
    // this.offers.items.map((item) => {
    //   if (item.id in data) {
    //     item.offerApproval = { status: 1 };
    //     item.approvalRequired = true;
    //     item.isChecked = false;
    //   }
    // });
    this.loading = false;
  }

  deepCopy(obj) {
    return JSON.parse(JSON.stringify(obj));
  }
}
