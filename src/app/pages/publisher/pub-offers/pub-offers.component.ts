import { Component } from '@angular/core';

@Component({
  selector: 'app-pub-offers',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class PubOffersComponent {
}
