import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { PubOffersDetailsComponent } from './pub-offers-details/pub-offers-details.component';
import { PubOffersListComponent } from './pub-offers-list/pub-offers-list.component';
import { PubOffersComponent } from './pub-offers.component';

import { PUB_OFFRES_ROUTING } from './pub-offers.routing';
import { PubOffersFilterComponent } from './pub-offers-filter/pub-offers-filter.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild(PUB_OFFRES_ROUTING)
  ],
  declarations: [PubOffersComponent, PubOffersListComponent, PubOffersDetailsComponent, PubOffersFilterComponent]
})
export class PubOffersModule {
}
