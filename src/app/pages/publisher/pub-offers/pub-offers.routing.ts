import { Routes } from '@angular/router';
import { PubOffersDetailsComponent } from './pub-offers-details/pub-offers-details.component';
import { PubOffersListComponent } from './pub-offers-list/pub-offers-list.component';

export const PUB_OFFRES_ROUTING: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list'
  },
  {
    path: 'list', component: PubOffersListComponent,
    data: {
      roles: ['ROLE_PUBLISHER'],
      name: 'titles.offers.list'
    }
  },
  {
    path: 'approvals', component: PubOffersListComponent,
    data: {
      roles: ['ROLE_PUBLISHER'],
      name: 'titles.offers.approvals'
    }
  },
  {
    path: 'detail/:id', component: PubOffersDetailsComponent,
    data: {
      roles: ['ROLE_PUBLISHER'],
      name: 'titles.offers.info'
    }
  }
];
