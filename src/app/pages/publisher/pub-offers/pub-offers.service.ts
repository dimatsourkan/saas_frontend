import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import uniq from 'lodash-es/uniq';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../shared/services/queryParams/query-params.service';

@Injectable({
  providedIn: 'root'
})
export class PubOffersService {


  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  applyOffersPayout(offer, offerTypes, flag, extend?) {
    offerTypes.forEach((type) => {
      if (offer.type === type.id) {
        offer.typeName = type.value;
      }
    });
    offer.countries = [];
    offer.platforms = [];
    offer.payouts = {
      amount: [],
      amountPercents: [],
      percents: [],
      pubPayouts: [],
      pubPayoutsPercent: []
    };

    offer.goals.forEach((goal) => {
      if (!goal.excludeCountries && goal.aggregateCountries) {
        goal.aggregateCountries.forEach((country) => {
          offer.countries.push(country.code);
        });
      }

      if (goal.aggregatePlatforms) {
        goal.aggregatePlatforms.forEach((platform) => {
          offer.platforms.push(platform.name);
        });
      }

      if (extend) {
        offer.payouts.percents.push(goal.payPercent);
        goal.pubPayout = ((goal[flag].amount * goal.payPercent) / 100).toFixed(2);
      }


      if (goal.dynamicPayout) {
        offer.payouts.amountPercents.push(goal[flag].amount);
        if (extend) {
          offer.payouts.pubPayoutsPercent.push(goal.pubPayout);
        }
      } else {
        offer.payouts.amount.push(goal[flag].amount);
        if (extend) {
          offer.payouts.pubPayouts.push(goal.pubPayout);
        }
      }
    });

    offer.countries = uniq(offer.countries);
    offer.platforms = uniq(offer.platforms);

    this._generatePayoutString(offer, 'amount', offer.currency);
    this._generatePayoutString(offer, 'amountPercents', '%');
    if (extend) {
      this._generatePayoutString(offer, 'pubPayouts', offer.currency);
      this._generatePayoutString(offer, 'pubPayoutsPercent', '%');
      this._generatePayoutString(offer, 'percents', '%');
    }
  }

  _generatePayoutString(offer, flag, mark) {
    if (offer.payouts[flag].length) {
      offer.payouts[`${flag}Min`] = Math.min.apply(Math, offer.payouts[flag]);
      offer.payouts[`${flag}Max`] = Math.max.apply(Math, offer.payouts[flag]);
      if (offer.payouts[`${flag}Min`] !== offer.payouts[`${flag}Max`]) {
        offer.payouts[`${flag}ForFront`] = `${offer.payouts[`${flag}Min`]} \u2014 ${offer.payouts[`${flag}Max`]} ${mark}`;
      } else {
        offer.payouts[`${flag}ForFront`] = `${offer.payouts[`${flag}Min`]} ${mark}`;
      }
    }
  }

  getOffers(queryParams: any): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer${params}`, {headers: {toCamelCase: 'true'}});
  }

  getOffer(id: any): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/${id}`, {headers: {toCamelCase: 'true'}});
  }

  getOfferTypes(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/types`, {headers: {toCamelCase: 'true'}});
  }

  getOfferApprovalStatuses(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer_approval/statuses`, {headers: {toCamelCase: 'true'}});
  }

  approveOffersRequest(ids: any): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/offer/request`, {ids}, {headers: {toCamelCase: 'true'}});
  }

}
