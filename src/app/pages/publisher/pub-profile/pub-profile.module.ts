import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

import { PUB_PROFILE_ROUTING } from './pub-profile.routing';
import { PubProfileComponent } from './pub-profile/pub-profile.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild(PUB_PROFILE_ROUTING)
  ],
  declarations: [PubProfileComponent]
})
export class PubProfileModule {
}
