import { Routes } from '@angular/router';
import { PubProfileComponent } from './pub-profile/pub-profile.component';

export const PUB_PROFILE_ROUTING: Routes = [
  {
    path: '',
    component: PubProfileComponent,
    data: {
      name: 'Publisher Profile'
    }
  }
];
