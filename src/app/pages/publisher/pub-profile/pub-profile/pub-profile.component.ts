import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ROLES } from '@app/core/enums/role.enum';
import { AuthService } from '@app/core/services/auth/auth.service';
import * as FileSaver from 'file-saver';
import { finalize } from 'rxjs/operators';
import { ValidatorService } from '../../../../shared/components/validation/validation.service';

import { CustomValidators } from '../../../../shared/validators/custom-validators.validator';
import { PublisherService } from '../../publisher.service';

@Component({
  selector: 'app-pub-profile',
  templateUrl: './pub-profile.component.html',
  styleUrls: ['./pub-profile.component.scss']
})
export class PubProfileComponent implements OnInit {
  pageName: any;
  publisher: any;
  loading: boolean;
  publisherForm: FormGroup;

  constructor(
    private router: Router,
    private _publisherService: PublisherService,
    private authService: AuthService,
    private _fb: FormBuilder,
    private validatorService: ValidatorService) {
  }

  ngOnInit() {
    this.publisherForm = this._fb.group({
      email: [, [Validators.required]],
      phone: [, [Validators.required, CustomValidators.phone]],
      skype: [Validators.required],
      username: [, [Validators.required]],
      address: this._fb.group({
        address: [, [Validators.required]],
        city: [, [Validators.required]],
        countryCode: [, [Validators.required]],
        state: [, [Validators.required]],
        zipcode: [, [Validators.required]]
      }),
      paymentInfo: this._fb.group({
        bankAddress: [],
        bankName: [],
        beneficiaryAccount: [],
        beneficiaryAddress: [],
        beneficiaryName: [],
        epaymentsId: [],
        iban: [],
        paypalEmail: [],
        routing: [],
        swift: [],
        wmz: []
      }),
      person: this._fb.group({
        firstName: [],
        lastName: []
      })
    });

    this.getPublisherInfo();
    this.publisherForm.get('email').disable();
  }

  getPublisherInfo() {
    this._publisherService.getPublisherInfo().subscribe((response) => {
      this.publisherForm.patchValue(response, { onlySelf: true });
      this.loading = false;
    });
  }

  refreshToken() {
    this._publisherService.refreshToken().subscribe(response => {
      this.authService.authorizeApp(ROLES.PUBLISHER, response.token);
      this.loading = false;
    });
  }


  updatePublisher() {
    if (this.publisherForm.invalid) {
      return this.validatorService.setTouchToControls(this.publisherForm);
    }
    this.loading = true;
    this._publisherService.updatePublisher(this.publisherForm.getRawValue())
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => {
        this.refreshToken();
        this.router.navigate(['/publisher', 'dashboard']);
      }, (err) => {
        this.validatorService.addErrorToForm(this.publisherForm, err);
      });
  }

  downloadPersonalData() {
    this._publisherService.downloadPersonalData().subscribe((response) => {
      const file = new Blob([response], { type: 'application/pdf;charset=utf-8' });
      FileSaver.saveAs(file, 'Personal Data.pdf', true);
    }, error => error);
  }

}
