import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubReportsConversionsFilterComponent } from './pub-reports-conversions-filter.component';

describe('PubReportsConversionsFilterComponent', () => {
  let component: PubReportsConversionsFilterComponent;
  let fixture: ComponentFixture<PubReportsConversionsFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubReportsConversionsFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubReportsConversionsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
