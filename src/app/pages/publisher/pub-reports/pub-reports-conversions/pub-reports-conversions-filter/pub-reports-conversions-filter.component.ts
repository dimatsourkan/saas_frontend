import { Component, OnInit } from '@angular/core';
import { PubConversionReportsFilter } from '@app/core/entities/pub-reports/pub-conversion-reports/pub-conversion-reports.filter';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { PubConversionReportsService } from '@app/core/entities/pub-reports/pub-conversion-reports/pub-conversion-reports.service';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'pub-reports-conversions-filter',
  templateUrl: './pub-reports-conversions-filter.component.html',
  styleUrls: ['./pub-reports-conversions-filter.component.scss']
})
export class PubReportsConversionsFilterComponent extends AbstractFilterComponent<PubConversionReportsFilter> implements OnInit {

  filter = (new PubConversionReportsFilter()).init();
  trafficTypes$ = DataStore.statistic.trafficTypes.asObservable$;

  constructor(private statisticsService: PubConversionReportsService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.statisticsService.getTrafficTypes().subscribe();
    this.filter.exclude(['page', 'limit', 'search', 'datetime_from', 'datetime_to']);
    DataStore.timezone.current.asObservable$.subscribe(timezone => {
      if (!this.filter.timezone) {
        this.filter.timezone = timezone;
      }
    });
  }

}
