import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { PubConversionReportsFilter } from '@app/core/entities/pub-reports/pub-conversion-reports/pub-conversion-reports.filter';
import { PubConversionReportsService } from '@app/core/entities/pub-reports/pub-conversion-reports/pub-conversion-reports.service';
import { finalize } from 'rxjs/operators';
import { DataStore } from '@app/core/entities/data-store.service';


@Component({
  selector: 'app-pub-reports-conversions',
  templateUrl: './pub-reports-conversions.component.html',
  styleUrls: ['./pub-reports-conversions.component.scss']
})
export class PubReportsConversionsComponent implements OnInit {

  subscription$: Subscription;
  filter: PubConversionReportsFilter;
  statistics$ = DataStore.pubConversionReports.list.asObservable$;
  loading: boolean;

  constructor(
    private statisticsService: PubConversionReportsService
  ) {
  }

  ngOnInit() {
    this.filter = (new PubConversionReportsFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getStatistics(filter));
  }

  getStatistics(filter: PubConversionReportsFilter) {
    this.loading = true;
    this.statisticsService.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: PubConversionReportsFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  calendarCallback({ startDate, endDate }) {
    this.filter.datetimeFrom = startDate;
    this.filter.datetimeTo = endDate;
  }

}
