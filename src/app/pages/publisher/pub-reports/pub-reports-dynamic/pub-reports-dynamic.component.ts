import { Component, OnInit } from '@angular/core';
import { DataStore } from '@entities/data-store.service';

import * as moment from 'moment';
import { Countries } from '../../../../shared/interfaces/countries';
import { DateFormattedService } from '../../../../shared/services/date-formatted/date-formatted.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';
import { PubReportsService } from '../pub-reports.service';

@Component({
  selector: 'app-pub-reports-dynamic',
  templateUrl: './pub-reports-dynamic.component.html',
  styleUrls: ['./pub-reports-dynamic.component.scss']
})
export class PubReportsDynamicComponent implements OnInit {

  scrollFlag: boolean;
  loading: boolean;
  order: string;
  params: any;
  reports: any = {items: []};
  offerStatuses: any = [];
  date: any;
  countries: Array<Countries> = [];
  filtersOpened: boolean;
  dateFormat: any;

  constructor(
    private _reportsService: PubReportsService,
    private dateFormatService: DateFormattedService,
    private queryParamsService: QueryParamsService
  ) {
  }

  ngOnInit() {
    this.loading = true;
    this.scrollFlag = false;
    this.order = 'asc';
    this.params = this.queryParamsService.getParams();
    this.dateFormat = moment.localeData().longDateFormat('L');

    if (!Object.keys(this.params).length) {
      this.params = {
        date: 1, clicks: 1, conversions: 1, cps: 1, cr: 1, revenues: 1,
        datetimeFrom: moment().subtract(90, 'day').format(this.dateFormatService.dateFormat),
        datetimeTo: moment().format(this.dateFormatService.dateFormat)
      };
    }
    if (!Object.prototype.hasOwnProperty.call(this.params, 'timezone')) {
      DataStore.timezone.current.asObservable$
      .subscribe(res => this.params.timezone = res);
    }
    this.getReports(this.params);
  }

  toggleFilter(flag?) {
    this.filtersOpened = !this.filtersOpened;
    if (!this.filtersOpened && flag) {
      this.params = this.queryParamsService.getParams();
    }
  }

  getReports(params) {
    return this._reportsService.getReportsDynamic(params).subscribe((response) => {
      this.loading = false;
      this.reports = response;
      this.scrollFlag = !this.scrollFlag;

      this.params.page = this.reports.page;
      this.params.limit = this.reports.limit;
      this.queryParamsService.setParams(this.params);
      if (this.filtersOpened) {
        this.toggleFilter();
      }
    });
  }

  calendarCallback({startDate, endDate}) {
    this.loading = true;
    this.params.page = 1;
    this.params.datetimeFrom = startDate;
    this.params.datetimeTo = endDate;
    this.getReports(this.params);
  }

  resetFilter() {
    this.loading = true;
    this.params.offersIds = null;
    this.params.date = 1;
    this.params.clicks = 1;
    this.params.conversions = 1;
    this.params.cps = 1;
    this.params.cr = 1;
    this.params.revenues = 1;
    this.params.hour = null;
    this.params.backUrlClicks = null;
    this.params.countries = null;
    this.params.platforms = null;
    this.params.cpa = null;
    this.params.offerId = null;
    this.params.cpc = null;
    this.params.offerName = null;
    this.params.goalName = null;
    this.params.countriesCodes = null;
    this.params.offersIds = null;
    this.params.platformNames = null;
    this.params.subAffId = null;

    this.getReports(this.params);
  }

  applyFilter() {
    this.loading = true;
    this.params.page = 1;
    this.getReports(this.params);
  }

  sortBy(field) {
    this.loading = true;
    this.params.page = 1;
    this.params.sort = field;
    this.params.order = this.order;
    this.order = this.order === 'asc' ? 'desc' : 'asc';
    this.getReports(this.params);
  }

  goToPage(page) {
    this.loading = true;
    this.params.page = page;
    this.getReports(this.params);
  }

  tableLimit(number) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = number;
    this.getReports(this.params);
  }

}
