import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubReportsStatisticsFilterComponent } from './pub-reports-statistics-filter.component';

describe('PubReportsStatisticsFilterComponent', () => {
  let component: PubReportsStatisticsFilterComponent;
  let fixture: ComponentFixture<PubReportsStatisticsFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubReportsStatisticsFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubReportsStatisticsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
