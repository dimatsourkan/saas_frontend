import { Component, OnInit } from '@angular/core';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { PubStatisticsFilter } from '@app/core/entities/pub-reports/pub-statistics-reports/pub-statistics-reports.filter';
import { PubStatisticsService } from '@app/core/entities/pub-reports/pub-statistics-reports/pub-statistics-reports.service';
import { StatisticsFilter } from '@app/core/entities/reports/statistics-reports/statistics-reports.filter';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'pub-reports-statistics-filter',
  templateUrl: './pub-reports-statistics-filter.component.html',
  styleUrls: ['./pub-reports-statistics-filter.component.scss']
})
export class PubReportsStatisticsFilterComponent  extends AbstractFilterComponent<PubStatisticsFilter> implements OnInit {

  filter = (new PubStatisticsFilter()).init();
  trafficTypes$ = DataStore.statistic.trafficTypes.asObservable$;

  constructor(private statisticsService: PubStatisticsService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.statisticsService.getTrafficTypes().subscribe();
    this.filter.exclude(['page', 'limit', 'search', 'datetime_from', 'datetime_to']);
    DataStore.timezone.current.asObservable$.subscribe(timezone => {
      if (!this.filter.timezone) {
        this.filter.timezone = timezone;
      }
    });
  }

}
