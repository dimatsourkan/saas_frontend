import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubReportsStatisticsComponent } from './pub-reports-statistics.component';

describe('PubReportsStatisticsComponent', () => {
  let component: PubReportsStatisticsComponent;
  let fixture: ComponentFixture<PubReportsStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubReportsStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubReportsStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
