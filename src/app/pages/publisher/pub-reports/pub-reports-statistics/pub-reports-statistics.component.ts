import { Component, OnInit } from '@angular/core';
import { PubStatisticsService } from '@app/core/entities/pub-reports/pub-statistics-reports/pub-statistics-reports.service';
import { PubStatisticsFilter } from '@app/core/entities/pub-reports/pub-statistics-reports/pub-statistics-reports.filter';
import { Subscription } from 'rxjs';
import { DataStore } from '@app/core/entities/data-store.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'pub-reports-statistics',
  templateUrl: './pub-reports-statistics.component.html',
  styleUrls: ['./pub-reports-statistics.component.scss']
})
export class PubReportsStatisticsComponent implements OnInit {

  subscription$: Subscription;
  filter: PubStatisticsFilter;
  statistics$ = DataStore.pubStatistic.list.asObservable$;
  loading: boolean;

  constructor(
    private statisticsService: PubStatisticsService) {
  }

  ngOnInit() {
    this.filter = (new PubStatisticsFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getStatistics(filter));
  }

  getStatistics(filter: PubStatisticsFilter) {
    this.loading = true;
    this.statisticsService.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: PubStatisticsFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  calendarCallback({ startDate, endDate }) {
    this.filter.datetimeFrom = startDate;
    this.filter.datetimeTo = endDate;
  }

}
