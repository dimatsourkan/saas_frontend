import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { PubReportsConversionsComponent } from './pub-reports-conversions/pub-reports-conversions.component';

import { PUB_REPORTS_ROUTES } from './pub-reports.routing';
import { PubReportsStatisticsFilterComponent } from './pub-reports-statistics/pub-reports-statistics-filter/pub-reports-statistics-filter.component';
import { PubReportsStatisticsComponent } from './pub-reports-statistics/pub-reports-statistics.component';
import { PubReportsConversionsFilterComponent } from './pub-reports-conversions/pub-reports-conversions-filter/pub-reports-conversions-filter.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild(PUB_REPORTS_ROUTES)
  ],
  declarations: [PubReportsConversionsComponent, PubReportsStatisticsFilterComponent, PubReportsStatisticsComponent, PubReportsConversionsFilterComponent]
})
export class PubReportsModule {
}
