import { Routes } from '@angular/router';
import { PubReportsConversionsComponent } from './pub-reports-conversions/pub-reports-conversions.component';
import { PubReportsStatisticsComponent } from './pub-reports-statistics/pub-reports-statistics.component';

export const PUB_REPORTS_ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'statistics'
  },
  {
    path: 'statistics',
    component: PubReportsStatisticsComponent,
    data: {
      name: 'titles.reports.statistics'
    }
  },
  {
    path: 'conversions',
    component: PubReportsConversionsComponent,
    data: {
      name: 'titles.reports.conversion'
    }
  }
];
