import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../shared/services/queryParams/query-params.service';


@Injectable({
  providedIn: 'root'
})
export class PubReportsService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getReports(queryParams: any): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/conversions${params}`, {headers: {toCamelCase: 'true'}});
  }

  getReportsDynamic(queryParams: any): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/statistics${params}`, {headers: {toCamelCase: 'true'}});
  }

  getApproveStatuses(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/conversions/approve_statuses`, {headers: {toCamelCase: 'true'}});
  }

  getStatus(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/billing/invoice/status`, {headers: {toCamelCase: 'true'}});
  }
}
