import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LanguageService } from '../../../../../shared/services/language/language.service';
import { QueryParamsService } from '../../../../../shared/services/queryParams/query-params.service';
import { SweetAlertService } from '../../../../../shared/services/sweet-alert/sweet-alert.service';
import { GroupUrlsService } from '../../../../manager/superlink/group-urls/group-urls.service';
import { PubSuperlinkService } from '../../pub-superlink.service';

@Component({
  selector: 'app-group-url-list',
  templateUrl: './group-url-list.component.html',
  styleUrls: ['./group-url-list.component.scss']
})
export class GroupUrlListComponent implements OnInit {
  params: any;
  loading: boolean;
  groupUrls: any = {items: []};
  contentTypeList: any;
  dynamicTypeList: any;
  distributionList: any;
  monetizationList: any;
  trafficTypeList: any;
  modeList: any;
  scrollFlag: boolean;
  tooltips: any;
  order: string;

  constructor(private translate: TranslateService,
              private languageService: LanguageService,
              private router: Router,
              private route: ActivatedRoute,
              private cdRef: ChangeDetectorRef,
              private _fb: FormBuilder,
              private _groupUrlsService: GroupUrlsService,
              private _queryParamsService: QueryParamsService,
              private _pubSuperlinkService: PubSuperlinkService,
              private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.params = this._queryParamsService.getParams();
    this.tooltips = {};
    forkJoin([
      this.getContentTypeList(),
      this.getDynamicTypeList(),
      this.getDistributionList(),
      this.getMonetizationList(),
      this.getTrafficTypeList(),
      this.getModeList()
    ]).subscribe(response => {
      this.getGroupUrls(this.params);
    });
  }

  getGroupUrls(params) {
    this.loading = true;
    this._pubSuperlinkService.getGroupUrls(params).subscribe((response) => {
      this.groupUrls = response;
      this.groupUrls.items.forEach((item) => {
        this.extendGroupUrlsData(item);
      });
      this.loading = false;
      this.scrollFlag = !this.scrollFlag;
      this.params.page = this.groupUrls.page;
      this.params.limit = this.groupUrls.limit;
      this._queryParamsService.setParams(this.params);
    });
  }

  extendGroupUrlsData(item) {
    item.contentTypeName = this.contentTypeList.filter(obj => obj.id === item.contentType).map(obj => obj.name)[0];
    item.dynamicTypeName = this.dynamicTypeList.filter(obj => obj.id === item.distribution).map(obj => obj.name)[0];
    item.distributionName = this.distributionList.filter(obj => obj.id === item.dynamicType).map(obj => obj.name)[0];
    item.monetizationModelName = this.monetizationList.filter(obj => obj.id === item.monetizationModel).map(obj => obj.name)[0];
    item.trafficTypeName = this.trafficTypeList.filter(obj => obj.id === item.trafficType).map(obj => obj.name)[0];
    item.modeName = this.modeList.filter(obj => obj.id === item.mode).map(obj => obj.name)[0];
  }

  getContentTypeList(): Observable<any> {
    return this._groupUrlsService.getContentTypeList().pipe(tap((response) => {
      this.contentTypeList = response;
    }));
  }

  getDynamicTypeList(): Observable<any> {
    return this._groupUrlsService.getDynamicTypeList().pipe(tap((response) => {
      this.dynamicTypeList = response;
    }));
  }

  getDistributionList(): Observable<any> {
    return this._groupUrlsService.getDistributionList().pipe(tap((response) => {
      this.distributionList = response;
    }));
  }

  getMonetizationList(): Observable<any> {
    return this._groupUrlsService.getMonetizationList().pipe(tap((response) => {
      this.monetizationList = response;
    }));
  }

  getTrafficTypeList(): Observable<any> {
    return this._groupUrlsService.getTrafficTypeList().pipe(tap((response) => {
      this.trafficTypeList = response;
    }));
  }

  getModeList(): Observable<any> {
    return this._groupUrlsService.getModeList().pipe(tap((response) => {
      this.modeList = response;
    }));
  }

  toggleTooltips(param) {
    this.tooltips[param] = !this.tooltips[param];
  }

  sortByField(field) {
    this.params.sort = field;
    this.params.page = 1;
    this.params.order = this.order;
    this.order = this.order === 'ASC' ? 'DESC' : 'ASC';
    this.loading = true;
    this.getGroupUrls(this.params);
  }

  goToPage(page) {
    this.loading = true;
    this.params.page = page;
    this.getGroupUrls(this.params);
  }

  tableLimit(number) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = number;
    this.getGroupUrls(this.params);
  }

}
