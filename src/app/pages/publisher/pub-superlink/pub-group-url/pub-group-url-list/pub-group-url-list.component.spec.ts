import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubGroupUrlListComponent } from './pub-group-url-list.component';

describe('PubGroupUrlListComponent', () => {
  let component: PubGroupUrlListComponent;
  let fixture: ComponentFixture<PubGroupUrlListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubGroupUrlListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubGroupUrlListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
