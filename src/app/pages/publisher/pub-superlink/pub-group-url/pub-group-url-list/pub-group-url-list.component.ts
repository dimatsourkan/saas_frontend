import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PubGroupUrlsFilter } from '@app/core/entities/pub-superlink/pub-group-urls/pub-group-urls.filter';
import { DataStore } from '@app/core/entities/data-store.service';
import { PubGroupUrlService } from '@app/core/entities/pub-superlink/pub-group-urls/pub-group-urls.service';
import { SweetAlertService } from '@app/shared/services/sweet-alert/sweet-alert.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'pub-group-url-list',
  templateUrl: './pub-group-url-list.component.html',
  styleUrls: ['./pub-group-url-list.component.scss']
})
export class PubGroupUrlListComponent implements OnInit {

  subscription$: Subscription;
  filter: PubGroupUrlsFilter;
  groupUrls$ = DataStore.superlinkPubGroupUrls.list.asObservable$;
  loading: boolean;

  constructor(
    private _groupUrlsService: PubGroupUrlService,
    private sweetAlertService: SweetAlertService) {
  }

  ngOnInit() {
    this.filter = (new PubGroupUrlsFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getGroupUrls(filter));
  }

  getGroupUrls(filter: PubGroupUrlsFilter) {
    this.loading = true;
    this._groupUrlsService.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  delete(id) {
    this.sweetAlertService.confirm('Do you want to remove link?').then((isConfirm) => {
      if (isConfirm.value) {
        this._groupUrlsService.delete(id).subscribe();
      }
    });
  }

}
