import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubStatisticsFilterComponent } from './pub-statistics-filter.component';

describe('PubStatisticsFilterComponent', () => {
  let component: PubStatisticsFilterComponent;
  let fixture: ComponentFixture<PubStatisticsFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubStatisticsFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubStatisticsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
