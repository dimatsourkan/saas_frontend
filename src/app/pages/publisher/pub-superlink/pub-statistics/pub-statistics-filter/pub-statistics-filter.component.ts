import { Component, OnInit } from '@angular/core';
import { AbstractFilterComponent } from '@app/new-shared/components/filter/abstract-filter.component';
import { PubSuperlinkStatisticFilter } from '@app/core/entities/pub-superlink/pub-statistics/pub-superlink-statistics.filter';
import { DataStore } from '@app/core/entities/data-store.service';

@Component({
  selector: 'pub-statistics-filter',
  templateUrl: './pub-statistics-filter.component.html',
  styleUrls: ['./pub-statistics-filter.component.scss']
})
export class PubStatisticsFilterComponent extends AbstractFilterComponent<PubSuperlinkStatisticFilter> implements OnInit {

  filter = (new PubSuperlinkStatisticFilter()).init();
  additionalFilterIsOpen: boolean;



  ngOnInit() {
    super.ngOnInit();
    this.filter.exclude(['page', 'limit', 'search', 'datetime_from', 'datetime_to']);
    DataStore.timezone.current.asObservable$.subscribe(timezone => {
      if (!this.filter.timezone) {
        this.filter.timezone = timezone;
      }
    });
  }


  toggleAdditionalFilter() {
    this.additionalFilterIsOpen = !this.additionalFilterIsOpen;
  }

}
