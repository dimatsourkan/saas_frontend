import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubStatisticsListComponent } from './pub-statistics-list.component';

describe('PubStatisticsListComponent', () => {
  let component: PubStatisticsListComponent;
  let fixture: ComponentFixture<PubStatisticsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubStatisticsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubStatisticsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
