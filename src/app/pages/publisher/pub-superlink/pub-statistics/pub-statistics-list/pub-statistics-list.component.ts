import { Component, OnInit } from '@angular/core';
import { PubSuperlinkStatisticFilter } from '@app/core/entities/pub-superlink/pub-statistics/pub-superlink-statistics.filter';
import { Subscription } from 'rxjs';
import { DataStore } from '@app/core/entities/data-store.service';
import { PubSuperlinkStatisticService } from '@app/core/entities/pub-superlink/pub-statistics/pub-superlink-statistics.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'pub-statistics-list',
  templateUrl: './pub-statistics-list.component.html',
  styleUrls: ['./pub-statistics-list.component.scss']
})
export class PubStatisticsListComponent implements OnInit {

  loading: boolean;
  filter: PubSuperlinkStatisticFilter;
  subscription$: Subscription;
  reports$ = DataStore.pubSuperlinkStatistic.list.asObservable$;

  constructor(
    private service: PubSuperlinkStatisticService) {
  }

  ngOnInit() {
    this.filter = (new PubSuperlinkStatisticFilter(true)).init();
    this.subscription$ = this.filter.onChangeFilter$.subscribe(filter => this.getReports(filter));
  }

  getReports(filter: PubSuperlinkStatisticFilter) {
    this.loading = true;
    this.service.getAll(filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  applyFilters(filter: PubSuperlinkStatisticFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }

  calendarCallback({ startDate, endDate }) {
    this.filter.datetime_from = startDate;
    this.filter.datetime_to = endDate;
  }


}
