import { Component, OnInit } from '@angular/core';
import { DataStore } from '@entities/data-store.service';
import * as moment from 'moment';
import { Countries } from '../../../../shared/interfaces/countries';
import { DateFormattedService } from '../../../../shared/services/date-formatted/date-formatted.service';
import { QueryParamsService } from '../../../../shared/services/queryParams/query-params.service';
import { PubSuperlinkService } from '../pub-superlink.service';


@Component({
  selector: 'app-pub-statistics',
  templateUrl: './pub-statistics.component.html',
  styleUrls: ['./pub-statistics.component.scss']
})
export class PubStatisticsComponent implements OnInit {
  scrollFlag: boolean;
  loading: boolean;
  order: string;
  params: any;
  reports: any = {items: []};
  offerStatuses: any = [];
  date: any;
  countries: Array<Countries> = [];
  filtersOpened: boolean;
  additionalFilterIsOpen: boolean;
  statistics: any;
  filterIsVisible: boolean;
  dateFormat: string;
  mainScrollFlag: boolean;


  constructor(
    private dateFormatService: DateFormattedService,
    private _pubSuperlinkService: PubSuperlinkService,
    private _queryParamsService: QueryParamsService) {
  }

  ngOnInit() {
    this.loading = true;
    this.scrollFlag = false;
    this.order = 'asc';
    this.params = this._queryParamsService.getParams();
    this.dateFormat = moment.localeData().longDateFormat('L');

    const defaultParams: any = {
      datetime_from: moment().subtract(90, 'day').format(this.dateFormatService.dateFormat),
      datetime_to: moment().format(this.dateFormatService.dateFormat),
      date: 1,
      clicks: 1,
      pub_conv: 1,
      pub_cr: 1,
      costs: 1,
      total_currency: 'USD'
    };
    DataStore.timezone.current.asObservable$.subscribe(timezone => defaultParams.timezone = timezone);


    this.params = this._queryParamsService.getParams();

    if (Object.keys(this.params).length === 1 && Object.prototype.hasOwnProperty.call(this.params, 'offers_ids')) {
      this.params.offers_ids = [this.params.offers_ids];
      this.params = Object.assign({}, this.params, defaultParams);
    } else {
      this.params = Object.keys(this.params).length ? this.params : defaultParams;
    }
    this.getStatistics(this.params);
  }

  toggleAdditionalFilter() {
    this.additionalFilterIsOpen = !this.additionalFilterIsOpen;
  }

  toggleFilter(flag?) {
    this.filtersOpened = !this.filtersOpened;
    if (!this.filtersOpened && flag) {
      this.params = this._queryParamsService.getParams();
    }
  }

  getStatistics(params) {
    this._pubSuperlinkService.getStatistics(params).subscribe((response) => {
      this.loading = false;
      this.statistics = response;
      this.scrollFlag = !this.scrollFlag;

      this.params.page = this.statistics.page;
      this.params.limit = this.statistics.limit;
      this._queryParamsService.setParams(this.params);
      this.filterIsVisible = false;
      this.additionalFilterIsOpen = false;
    });
  }

  resetFilters() {
    this.loading = true;
    this.params.date = 1;
    this.params.clicks = 1;
    this.params.pub_conv = 1;
    // this.params.pub_cpa = null;
    this.params.pub_cr = 1;
    this.params.costs = 1;
    this.params._name = null;
    this.params.timezone = null;
    this.params.countries_codes = null;
    this.params.groups_ids = null;
    this.params.month = null;
    this.params.hour = null;
    this.params.countries = null;
    this.params.platforms = null;
    this.params.platforms_names = null;
    this.params.total_currency = 'USD';
    this.mainScrollFlag = !this.mainScrollFlag;
    delete this.params.group_name;
    delete this.params.order;
    delete this.params.sort;

    this.getStatistics(this.params);
  }

  applyFilters() {
    this.loading = true;
    this.params.page = 1;
    this.mainScrollFlag = !this.mainScrollFlag;
    this.additionalFilterIsOpen = false;
    this.params.resale_id = this.params.resale_name;
    this.params.group_id = this.params.group_name;
    this.params.traffic_type = this.params.traffic_type === 'null' ? null : this.params.traffic_type;
    this.getStatistics(this.params);
  }

  sortBy(field) {
    this.loading = true;
    this.params.page = 1;
    this.params.sort = field;
    this.params.order = this.order;
    this.order = this.order === 'asc' ? 'desc' : 'asc';

    this.getStatistics(this.params);
  }

  goToPage(page) {
    this.loading = true;
    this.params.page = page;
    this.getStatistics(this.params);
  }

  calendarCallback({
                     startDate,
                     endDate
                   }) {
    this.loading = true;
    this.params.page = 1;
    this.params.datetime_from = startDate;
    this.params.datetime_to = endDate;
    this.getStatistics(this.params);
  }

  tableLimit(number) {
    this.loading = true;
    this.params.page = 1;
    this.params.limit = number;
    this.getStatistics(this.params);
  }

  toggleFilterVisibility(flag) {
    this.filterIsVisible = !this.filterIsVisible;
    if (!this.filterIsVisible && flag) {
      this.params = this._queryParamsService.getParams();
    }
  }

}
