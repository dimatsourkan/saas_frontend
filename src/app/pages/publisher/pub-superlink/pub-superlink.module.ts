import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

import { PUB_SUPERLINK_ROUTING } from './pub-superlink.routing';
import { PubGroupUrlListComponent } from './pub-group-url/pub-group-url-list/pub-group-url-list.component';
import { PubGroupUrlModeComponent } from './pub-group-url/pub-group-url-mode/pub-group-url-mode.component';
import { PubStatisticsFilterComponent } from './pub-statistics/pub-statistics-filter/pub-statistics-filter.component';
import { PubStatisticsListComponent } from './pub-statistics/pub-statistics-list/pub-statistics-list.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild(PUB_SUPERLINK_ROUTING)
  ],
  declarations: [PubGroupUrlModeComponent, PubGroupUrlListComponent, PubStatisticsFilterComponent, PubStatisticsListComponent]
})
export class PubSuperlinkModule {
}
