import { Routes } from '@angular/router';
import { PubGroupUrlListComponent } from './pub-group-url/pub-group-url-list/pub-group-url-list.component';
import { PubGroupUrlModeComponent } from './pub-group-url/pub-group-url-mode/pub-group-url-mode.component';
import { PubStatisticsListComponent } from './pub-statistics/pub-statistics-list/pub-statistics-list.component';

export const PUB_SUPERLINK_ROUTING: Routes = [
  {
    path: 'group-urls', children: [
      {
        path: 'list',
        component: PubGroupUrlListComponent
      },
      {
        path: 'mode/:id',
        component: PubGroupUrlModeComponent
      },
    ]
  },
  {
    path: 'statistics',
    component: PubStatisticsListComponent
  }

];
