import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { BaseUrlService } from '../../../shared/services/base-url/base-url.service';
import { QueryParamsService } from '../../../shared/services/queryParams/query-params.service';

@Injectable({
  providedIn: 'root'
})
export class PubSuperlinkService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getStatistics(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/statistics/superlink${params}`, {headers: {toCamelCase: 'true'}});
  }

  getGroupUrls(queryParams): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/superlink/group${params}`, {headers: {toCamelCase: 'true'}});
  }

  getModeGroupUrl(id): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/superlink/group/${id}/get_mode`, {headers: {toCamelCase: 'true'}});
  }

  setDynamicMode(groupID): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/superlink/group/${groupID}/set_dynamic_mode`, {},
      {headers: {toCamelCase: 'true'}});
  }

  setManualMode(groupID, urls): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/superlink/group/${groupID}/set_manual_mode`, {urls},
      {headers: {toCamelCase: 'true'}});
  }

  setManualCountryMode(groupID, urls): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/superlink/group/${groupID}/set_manual_country_mode`, {urls},
      {headers: {toCamelCase: 'true'}});
  }

}
