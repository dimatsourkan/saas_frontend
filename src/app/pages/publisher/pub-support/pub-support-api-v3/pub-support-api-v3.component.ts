import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';


@Component({
  selector: 'app-pub-support-api-v3',
  templateUrl: './pub-support-api-v3.component.html',
  styleUrls: ['./pub-support-api-v3.component.scss']
})
export class PubSupportApiV3Component implements OnInit {

  apiVersiont = 'v3';
  successfulResponseText: SafeHtml;
  successfulResponseExample: SafeHtml;

  offers_request: SafeHtml;
  offers_response: SafeHtml;

  getCreatives_request: SafeHtml;
  getCreatives_response: SafeHtml;

  approve_request: SafeHtml;
  approve_response: SafeHtml;

  statistics_request: SafeHtml;
  staticstics_response: SafeHtml;

  constructor(private sanitized: DomSanitizer) {
  }

  ngOnInit() {
    this.offers_request = this.sanitized.bypassSecurityTrustHtml(`GET /api/public/${this.apiVersiont}/offers?api_key={api_key}`);
    this.offers_response = this.sanitized.bypassSecurityTrustHtml(`
    {
      "items": [
          {
              "id": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX", //offer identificator
              "preview_url": "https://appstore.link", //preview link to some market or landing
              "thumb_url": "https://domain.com/thumb.jpg", //thumbnail url
              "name": "Grow your bussiness", //offer name
              "status": active, //offer status: active, paused 24 (offer may be paused in 24 hours)
              "traffic_type": 0, // traffic type.  Cases: 0 - any, 1 - incent, 2 - non incent
              "tags": [{"id": 18, "name": "Social"}], //offer tags(categories) like Games, Adult, Books etc.
              "goals": [ //array of goals
                  {
                      "id": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX", //goal identificator
                      "name": "goal name",
                      "cap": { // offer's goal capabilities
                          "daily": 0, //daily capability
                          "monthly": 0, //monthly capability
                          "total": 0 //total capability
                      },
                      "enabled": true, //goal activity (only active goals are shown)
                      "platforms": [ //Offer's goal device platform
                          {
                              "platform": "iPad",
                              "version": "0.0.43"
                          },
                          {
                              "platform": "iPhone",
                              "version": "0.0.3"
                          }
                      ],
                      "countries": [ //Offer's goal countries array, represented in 2 symbol ISO-codes
                          "US"
                      ],
                      "exclude_countries": false, // If value “exclude_countries” set to true, it means that provided list of countries are excluded, but all the others are in set. If set to false, then only provided countries will be alowed. In this case array of countries could be empty, what means - allowed for all
                      "cities": [{ "geoname_id": 5174, "name": null }], //Offer's goal cities array, represented as geo IDs and geoname
                      "exclude_cities": false, // If value “exclude_cities” set to true, it means that provided list of cities are excluded, but all the others are in set. If set to false, then only provided cities will be alowed. In this case array of cities could be empty, what means - allowed for all
                      "browsers": ['Chrome'], //Offer's goal browsers array with browser names
                      "isps": [{ "isp_id": 111, "name": "AT&T"}], //Acceptable internet service providers (carriers). Empty means all
                      "connections": ["Wi-Fi"], //Acceptable internet connections, e.g. Wi-Fi, 3G, etc.
                      "dynamic_payout": false, //if set to true, then publisher payout will be represented and calculated in percents
                      "type": 1, //goal type, 1 - main goal, 2 - additional
                      "publisher_payout": {
                          "amount": 0.16,
                          "currency": "USD"
                      }
                  }
              ],
              "currency": "USD",
              "created_at": "YYYY-MM-DDTHH:MM:SS+00:00",
              "approval_required": true,
              "tracking_url": null, //not provided in public API until approvement
              "offer_approval": null, //not provided in public API until approvement
          }
      ],
      "total_items": 1, //items count on page
      "total_pages": 1, //total pages
      "page": 1, //current page
      "limit": 1 //page limit
  }
    `);

    this.getCreatives_request =
      this.sanitized.bypassSecurityTrustHtml(`GET  /api/public/${this.apiVersiont}/offer/{id}/creatives?api_key={api_key}`);
    this.getCreatives_response = this.sanitized.bypassSecurityTrustHtml(`{
      "success": true,
      "error_messages": [],
      "offer_id": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
      "creatives": [
        {
          "name": "hotspot_shield_175x175.png",
          "url": "https://target.url/target.file",
          "width": "175",
          "height": "175"
        }
      ]
    }`);

    this.approve_request = this.sanitized.bypassSecurityTrustHtml(`POST  /api/public/${this.apiVersiont}/offer/request?api_key={api_key}`);
    this.approve_response = this.sanitized.bypassSecurityTrustHtml(`[
      "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXX1": "Pending",
      "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXX2": "Pending"
  ]`);

    this.statistics_request = this.sanitized.bypassSecurityTrustHtml(`GET  /api/public/${this.apiVersiont}/statistics?api_key={api_key}`);
    this.staticstics_response = this.sanitized.bypassSecurityTrustHtml(`{
      "items": [
        {
                  "back_url_clicks": "8",
                  "cpa": 0.42,
                  "cr": "82.35",
                  "clicks": "17",
                  "conversions": "14",
                  "revenue": 5.82,
                  "currency": "USD",
                  "date": "2018-05-08" }
      ],
      "total_items": 1,
      "total_pages": 1,
      "page": 1,
      "limit": 1
    }`);

    this.successfulResponseText = this.sanitized.bypassSecurityTrustHtml(`All requests listed below use the unique API key {api_key}.
    You can obtain it from your personal Account Manager. All response data has JSON format.
    In case of successful request status code 200 will be returned.`);

    this.successfulResponseExample = this.sanitized.bypassSecurityTrustHtml(`{
      "items": [],
      "total_items": 0,
      "total_pages": 1, //total pages available
      "page": 1, //respone page, default 1
      "limit": 200 //response items on page, default 200
  }`);

  }

}
