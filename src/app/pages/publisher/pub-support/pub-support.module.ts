import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { PubSupportApiV3Component } from './pub-support-api-v3/pub-support-api-v3.component';
import { PubSupportApiComponent } from './pub-support-api/pub-support-api.component';

import { PUB_SUPPORT_ROUTES } from './pub-support.routing';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(PUB_SUPPORT_ROUTES)
  ],
  declarations: [PubSupportApiComponent, PubSupportApiV3Component]
})
export class PubSupportModule {
}
