import { Routes } from '@angular/router';
import { PubSupportApiV3Component } from './pub-support-api-v3/pub-support-api-v3.component';
import { PubSupportApiComponent } from './pub-support-api/pub-support-api.component';

export const PUB_SUPPORT_ROUTES: Routes = [
  {
    path: 'api',
    component: PubSupportApiComponent
  },
  {
    path: 'api-v3',
    component: PubSupportApiV3Component
  },
];

