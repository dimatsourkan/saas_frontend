import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { PUBLISHER_ROUTES } from './publisher.routing';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    RouterModule.forChild(PUBLISHER_ROUTES)
  ],
  declarations: []
})
export class PublisherModule {
}
