import { Routes } from '@angular/router';

export const PUBLISHER_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard', loadChildren: './pub-dashboard/pub-dashboard.module#PubDashboardModule',
    data: {
      name: 'titles.dashboard'
    }
  },
  {
    path: 'offers', loadChildren: './pub-offers/pub-offers.module#PubOffersModule',
    data: {
      name: 'titles.offers.list'
    }
  },
  {
    path: 'reports', loadChildren: './pub-reports/pub-reports.module#PubReportsModule',
    data: {
      name: 'UsersManagersModule'
    }
  },
  {
    path: 'superlink', loadChildren: './pub-superlink/pub-superlink.module#PubSuperlinkModule',
    data: {
      name: 'titles.superlink.list'
    }
  },
  {
    path: 'billing', loadChildren: './pub-billing/pub-billing.module#PubBillingModule',
    data: {
      name: 'titles.bill.list'
    }
  },
  {
    path: 'support', loadChildren: './pub-support/pub-support.module#PubSupportModule',
    data: {
      name: 'titles.support'
    }
  },
  {
    path: 'profile', loadChildren: './pub-profile/pub-profile.module#PubProfileModule',
    data: {
      name: 'titles.pub-profile'
    }
  }
];
