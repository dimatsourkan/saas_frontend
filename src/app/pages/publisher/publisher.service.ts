import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../../shared/services/base-url/base-url.service';


@Injectable({
  providedIn: 'root'
})
export class PublisherService {

  constructor(private http: HttpClient,
              private url: BaseUrlService) {
  }

  getPublisherInfo(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher`, {headers: {toCamelCase: 'true'}});
  }

  refreshToken(): Observable<any> {
    return this.http.post(`${this.url.getBaseUrlWithRole()}/refresh_auth_token`, {}, {headers: {toCamelCase: 'true'}});
  }

  updatePublisher(publisher): Observable<any> {
    return this.http.put(`${this.url.getBaseUrlWithRole()}/publisher`, publisher, {headers: {toCamelCase: 'true'}});
  }

  downloadPersonalData(): Observable<any> {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/publisher/download`, {
      responseType: 'arraybuffer'
    });
  }
}
