import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'form-checkbox',
  templateUrl: './form-checkbox.component.html',
  styleUrls: ['./form-checkbox.component.scss']
})
export class FormCheckboxComponent implements OnInit {

  formCheckbox: FormGroup;

  ngOnInit() {
    this.formCheckbox = new FormGroup({
      checkboxSingle: new FormControl(0),
      checkboxWithText: new FormControl(true),
      checkboxDisabled: new FormControl({value: false, disabled: true}),
      checkboxDisabledChecked: new FormControl({value: true, disabled: true}),
      switcherDefaultDisabled: new FormControl({value: 0, disabled: true}),
      switcherDefault: new FormControl(0),
      switcherStyled: new FormControl(2),
      switcherMultiple: new FormControl(2),
      switcherMultipleSmall: new FormControl(2)
    });

    this.formCheckbox.valueChanges.subscribe(res => {
      console.log(res);
    });
  }

}
