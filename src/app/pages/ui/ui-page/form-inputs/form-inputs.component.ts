import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'form-inputs',
  templateUrl: './form-inputs.component.html',
  styleUrls: ['./form-inputs.component.scss']
})
export class FormInputsComponent implements OnInit {

  formInputs: FormGroup;

  ngOnInit() {
    this.formInputs = new FormGroup({
      defaultInput: new FormControl('Default value', [Validators.required]),
      requiredInput: new FormControl('', [Validators.required]),
      disabledInput: new FormControl({value: 'Default value', disabled: true}),
      maskedInput: new FormControl('email@email.email', [Validators.required]),
      integerInput: new FormControl(123456, [Validators.required]),
      decimalInput: new FormControl(123.456, [Validators.required]),
      groupInput: new FormControl('Default value', [Validators.required]),
      tagsInput: new FormControl(['text', 'text', 'text'], [Validators.required]),
      tagsInputMasked: new FormControl(['email@email.email'], [Validators.required]),
      password: new FormControl('password', [Validators.required]),
      areaInput: new FormControl('Default value',[Validators.required]),
    });

    this.formInputs.valueChanges.subscribe(res => {
      console.log(res);
    });
  }

  inputGroupClicked() {
    alert('inputGroupClicked');
  }

}
