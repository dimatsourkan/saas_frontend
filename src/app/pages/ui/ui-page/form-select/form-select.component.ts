import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.scss']
})
export class FormSelectComponent implements OnInit {

  formSelect: FormGroup;

  ngOnInit() {
    this.formSelect = new FormGroup({
      selectSingle: new FormControl(null),
      selectMultiple: new FormControl([])
    });

    this.formSelect.valueChanges.subscribe(res => {
      console.log(res);
    });
  }

}
