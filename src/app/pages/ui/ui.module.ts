import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NewSharedModule } from '@app/new-shared/new-shared.module';
import { UiPageComponent } from './ui-page/ui-page.component';
import { FormInputsComponent } from './ui-page/form-inputs/form-inputs.component';
import { FormCheckboxComponent } from './ui-page/form-checkbox/form-checkbox.component';
import { FormSelectComponent } from './ui-page/form-select/form-select.component';
import { ButtonsComponent } from './ui-page/buttons/buttons.component';
import { LinksComponent } from './ui-page/links/links.component';
import { BadgesComponent } from './ui-page/badges/badges.component';

@NgModule({
  imports: [
    NewSharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: UiPageComponent
      },
    ])
  ],
  declarations: [UiPageComponent, FormInputsComponent, FormCheckboxComponent, FormSelectComponent, ButtonsComponent, LinksComponent, BadgesComponent]
})
export class UiModule {
}
