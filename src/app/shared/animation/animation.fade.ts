import { animate, query, style, transition, trigger } from '@angular/animations';

export const fadeTransition = trigger('fadeTransition', [
  transition('* => *', [
    query('.external-filers:leave',
      [
        style({opacity: 1}),
        animate('.1s', style({opacity: 0})),
      ], {optional: true}
    ),
    query('.external-filers:enter',
      [
        style({opacity: 0}),
        animate('.1s', style({opacity: 1})),
      ], {optional: true}
    )

  ])
]);
