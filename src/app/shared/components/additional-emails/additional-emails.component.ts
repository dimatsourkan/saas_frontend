import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-additional-emails',
  templateUrl: './additional-emails.component.html',
  styleUrls: ['./additional-emails.component.scss']
})
export class AdditionalEmailsComponent implements OnInit {

  @Output() onClick = new EventEmitter<string[]>();
  @Output() emailsArray = new EventEmitter<string[]>();

  emailsForm: FormGroup;
  emails: FormArray;
  submitted: boolean;

  constructor(private _fb: FormBuilder) {
  }

  ngOnInit() {
    this.emailsForm = this._fb.group({
      emails: this._fb.array([this.createItem()])
    });
  }

  closeEmailModal() {
    this.onClick.emit();
    this.emailsForm.reset();
  }

  createItem(item?): FormGroup {
    return this._fb.group({
      email: [(item) ? item.email : '', [Validators.email, Validators.required]],
    });
  }

  addItem(item?): void {
    this.emails = this.emailsForm.get('emails') as FormArray;
    this.emails.push(this.createItem(item));
  }

  removeFromFormArray(name: string, index: number): void {
    (<FormArray>this.emailsForm.get(name)).removeAt(index);
  }

  submitEmails() {
    if (this.emailsForm.valid) {
      this.emailsArray.emit(this.emailsForm.get('emails').value.map(item => item.email));
      this.submitted = false;
      this.closeEmailModal();
    } else {
      this.submitted = true;
    }
  }

}
