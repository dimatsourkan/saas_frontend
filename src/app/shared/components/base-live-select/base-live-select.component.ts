import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { debounceTime, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-base-live-select',
  template: ``,
  styles: []
})
export class BaseLiveSelectComponent implements OnInit, ControlValueAccessor {

  @Input() multiple = true;
  @Input() clearable = true;
  @Input() selectPlaceholder = 'Choose ...';
  @Input() formControlName: string;
  @Output() selectedObject: EventEmitter<any> = new EventEmitter<any>();
  typeahead = new EventEmitter<any>();
  itemsArray: Array<{ [key: string]: string, id: string }> = [];
  selectedItems: string[] | string;
  loading: boolean;
  totalItems: number;
  totalPages: number;
  page: number;
  term = '';

  constructor(private cd: ChangeDetectorRef) {
  }

  private propagateChange: any = () => {
  }
  private propagateTouch: any = () => {
  }

  ngOnInit(): void {
    this.initTypehead();
  }

  initTypehead() {
    this.typeahead
      .pipe(
        debounceTime(200),
        switchMap((term) => {
          this.loading = true;
          if (term && typeof term === 'string') {
            this.term = term;
            return this.load(term);
          } else if (term) {
            return this.load(term.term, term.upPage);
          } else {
            return this.load('', term.upPage);
          }
        })
      )
      .subscribe((items: { items: Array<any>, totalItems: number, totalPages: number, page: number }) => {
        this.totalItems = items.totalItems;
        this.page = items.page;
        this.totalPages = items.totalPages;
        this.itemsArray = (this.page === 1 || !this.page) ? items.items : [...this.itemsArray, ...items.items];
        this.loading = false;
        this.cd.markForCheck();
      }, () => {
        this.itemsArray = [];
        this.loading = false;
        this.page = 1;
        this.term = '';
        this.initTypehead();
        this.cd.markForCheck();
      });

    this.typeahead.emit('');
  }

  load(term: string, upPage?: boolean): any {
  }

  onClose(): void {
    this.typeahead.emit('');
  }

  onOpen(): void {
    this.typeahead.emit('');
  }

  onClear() {
    this.typeahead.emit('');
  }

  onSelect() {
    const item = this.itemsArray.find(el => el.id === this.selectedItems);
    this.selectedObject.emit(item);
    this.propagateChange(this.selectedItems);
    this.propagateTouch(this.selectedItems);
  }


  onScrollToEnd() {
    if (this.loading || this.totalItems === this.itemsArray.length || this.page >= this.totalPages || !this.page) {
      return;
    }
    this.typeahead.emit({ term: this.term, upPage: true });
  }

  onScroll({ end }) {
    if (this.loading || this.totalItems === this.itemsArray.length || this.page >= this.totalPages || !this.page) {
      return;
    }
    if (end + 10 >= this.itemsArray.length) {
      this.typeahead.emit({ term: this.term, upPage: true });
    }
  }

  writeValue(ids: string[] | string | null): void {
    if (Array.isArray(ids)) {
      if (ids.length) {
        this.selectedItems = ids;
        this.selectedItems.forEach(id => this.load(id).subscribe(items => {
          this.itemsArray = [...this.itemsArray, ...items.items];
          const item = this.itemsArray.find(el => el.id === this.selectedItems);
          this.selectedObject.emit(item);
        }));
      } else {
        this.selectedItems = [];
      }
    } else {
      this.selectedItems = ids;
      if (ids) {
        this.load(ids).subscribe(items => {
          this.itemsArray = [...this.itemsArray, ...items.items];
          const item = this.itemsArray.find(el => el.id === this.selectedItems);
          this.selectedObject.emit(item);
        });
      }
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
}
