import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import * as d3 from 'd3';
import * as moment from 'moment';
import { DateFormattedService } from '../../../services/date-formatted/date-formatted.service';


@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit, OnChanges {

  @Input() data: any;
  @Input() hideTitle: boolean;
  @ViewChild('element') el: ElementRef;
  chartData: any;
  mainParam: any;
  secondaryParam: any;
  color: any;

  constructor(private dateFormatService: DateFormattedService) {
  }


  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data && changes.data.currentValue) {
      this.buildChart(this.data);
    }
  }

  buildChart({data, name}) {

    if (!data) {
      return false;
    }
    const element = this.el.nativeElement;
    const svgElement = element.querySelector('svg');
    const tooltip = d3.select(element.querySelector('.offer-managers__chart-tooltip'));
    const tooltipName = d3.select(element.querySelector('.offer-managers__chart-tooltip .name span'));
    const tooltipDate = d3.select(element.querySelector('.offer-managers__chart-tooltip .date span'));
    const tooltipClicks = d3.select(element.querySelector('.offer-managers__chart-tooltip .clicks span'));
    const tooltipConversions = d3.select(element.querySelector('.offer-managers__chart-tooltip .conversions span'));
    const tooltipCr = d3.select(element.querySelector('.offer-managers__chart-tooltip .cr span'));

    const margin = {top: 10, right: 30, bottom: 5, left: 40};
    const width = 80;
    const height = 40;
    const scaleRate = 1.1;
    // const max1 = 5454;
    // const max2 = 360;

    // const tooltip = d3.select('.chart-tooltip');

    // X - axis generator
    const xScale = d3.scaleTime().range([0, width])
      .domain(d3.extent(data, d => new Date(d.date)));

    const xAxis = d3.axisBottom(xScale);

    // Y - axis generator
    const yScale = d3.scaleLinear().range([height, 0])
      .domain([0, d3.max(data, d => d.clicks * scaleRate)]);

    const yScale2 = d3.scaleLinear().range([height, 0])
      .domain([0, d3.max(data, d => d.conversions * scaleRate)]);

    const yScale3 = d3.scaleLinear().range([height, 0])
      .domain([0, d3.max(data, d => d.cr * scaleRate)]);

    const yAxis = d3.axisLeft(yScale);

    // Area generator
    const area = d3.area()
      .x(d => xScale(new Date(d.date)))
      .y0(height)
      .y1(d => yScale(d.clicks))
      .curve(d3.curveMonotoneX);

    const svg = d3.select(svgElement)
      .html('')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');


    // generate tooltip
    const showTooltip = (d) => {
      tooltip.transition()
        .duration(200)
        .style('opacity', .9);
      tooltipConversions.html(d.conversions);
      tooltipCr.html(d.cr);
      tooltipClicks.html(d.clicks);
      tooltipName.html(name);
      tooltipDate.html((moment(d.date)).format('DD MMMM YYYY'));
      tooltip.style('left', (d3.event.layerX + 10) + 'px')
        .style('top', (d3.event.layerY + 10) + 'px');
    };

    const hideTooltip = () => {
      tooltip.transition()
        .duration(300)
        .style('opacity', 0);
    };

    // START: Chart rendering section
    const renderChart = () => {
      if (!this.hideTitle) {
        svg.append('g')
          .attr('class', 'tick')
          .attr('transform', 'translate(0,' + height + ')')
          .call(xAxis);

        svg.append('g')
          .attr('class', 'tick')
          .call(yAxis);
      }
      svg.append('path')
        .datum(data)
        .classed('filled', true)
        .attr('d', area);

      svg.selectAll('.bar-clicks')
        .data(data)
        .enter().append('rect')
        .attr('class', 'bar-clicks')
        .attr('x', d => xScale(new Date(d.date)))
        .attr('y', d => yScale(d.clicks))
        .attr('width', 8)
        .attr('height', d => {
          if (d.clicks === 0) {
            return 3;
          } else {
            return height - yScale(d.clicks);
          }
        })
        .on('mouseover', d => showTooltip(d))
        .on('mouseout', () => hideTooltip());

      svg.selectAll('.bar-conversions')
        .data(data)
        .enter().append('rect')
        .attr('class', 'bar-conversions')
        .attr('x', d => (xScale(new Date(d.date)) + 2))
        .attr('y', d => yScale2(d.conversions))
        .attr('width', 4)
        .attr('height', d => {
          if (d.conversions === 0) {
            return 3;
          } else {
            return height - yScale2(d.conversions);
          }
        })
        .on('mouseover', (d) => showTooltip(d))
        .on('mouseout', () => hideTooltip());

      svg.selectAll('.bar-cr')
        .data(data)
        .enter().append('rect')
        .attr('class', 'bar-cr')
        .attr('x', d => (xScale(new Date(d.date)) + 2))
        .attr('y', d => yScale3(d.cr))
        .attr('width', 4)
        .attr('height', d => {
          if (d.cr === 0) {
            return 3;
          } else {
            return height - yScale3(d.cr);
          }
        })
        .on('mouseover', (d) => showTooltip(d))
        .on('mouseout', () => hideTooltip());


    };
    // END: Chart rendering section

    renderChart();
  }

}
