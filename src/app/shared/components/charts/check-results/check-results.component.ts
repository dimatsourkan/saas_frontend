import { Component, Input, OnInit } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';


@Component({
  selector: 'app-check-results',
  templateUrl: './check-results.component.html',
  styleUrls: ['./check-results.component.scss']
})
export class CheckResultsComponent implements OnInit {

  timezone: string;
  @Input() data: object | [];


  ngOnInit() {
    DataStore.timezone.current.asObservable$
    .subscribe(res => {
      this.timezone = res;
      this.parseCheckResults(this.data);
    });
  }

  parseCheckResults(item) {
    for (const prop in item) {
      if (item.hasOwnProperty(prop)) {
        const platform = item[prop];
        Object.keys(platform).forEach(country => {
          if (platform[country].length !== 4) {
            for (let i = platform[country].length; i <= 3; i++) {
              platform[country].push({empty: true});
            }
          }
        });
      }
    }
  }

}
