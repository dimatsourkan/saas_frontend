import { ChartDataSets } from 'chart.js';

const COLORS = [
  '#1f76b4',
  '#ff9701',
  '#8ac34a',
  '#4bc0c0',
  '#9966fe',
  '#ff6384',
  '#ffcd55',
  '#c9cbcf'
];

export class DoughnutChartDataset<T> implements ChartDataSets {

  data: number[] = [];
  labels: string[];
  backgroundColor = [];
  borderWidth = 1;
  summ: number;

  private setSumm() {
    this.summ = this.data.reduce((a, b) => a + b, 0);
  }

  private setColors() {
    this.backgroundColor = [...this.backgroundColor, ...COLORS];
    if (this.data.length > this.backgroundColor.length) {
      this.setColors();
    }
  }

  getPersentBySumm(int: number) {
    return (100 / this.summ * int).toFixed(2);
  }

  setData(data: number[]) {
    this.data = data;
    this.backgroundColor = [];
    this.setColors();
    this.setSumm();
  }
}
