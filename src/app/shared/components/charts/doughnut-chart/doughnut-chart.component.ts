import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import { DoughnutChartDataset } from '@app/shared/components/charts/doughnut-chart/doughnut-chart-dataset.model';
import * as Chart from 'chart.js';
import { ChartOptions } from 'chart.js';

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.scss']
})
export class DoughnutChartComponent implements AfterViewInit {

  @ViewChild('canvas') private canvasRef: ElementRef;
  @Input() datasets: DoughnutChartDataset<any>[];
  @Input() label: string;

  private pieChart: Chart;
  private canvas: HTMLCanvasElement;
  private ctx: CanvasRenderingContext2D;

  ngAfterViewInit() {
    this.canvas = this.canvasRef.nativeElement;
    this.ctx = this.canvas.getContext('2d');
    this.pieChart = new Chart(this.ctx, this.config);
  }

  private get config(): Chart.ChartConfiguration {
    return {
      type: 'doughnut',
      data: {
        datasets: this.datasets,
        labels: this.datasets[0].labels
      },
      options: this.options
    };
  }

  private get options(): ChartOptions {

    return {
      responsive: true,
      tooltips: {
        custom: function (tooltip) {
          if (!tooltip) {
            return;
          }
          tooltip.displayColors = false;
        },
        callbacks: {
          label: (tooltipItem, data: any) => {
            const dataset = data.datasets[tooltipItem.datasetIndex];
            return [
              `${this.label} : ${dataset.data[tooltipItem.index]}`,
              `Share : ${dataset.getPersentBySumm(dataset.data[tooltipItem.index])}%`,
            ];
          },
          title: (tooltipItem, data: any) => {
            return data.labels[tooltipItem[0].index];
          }
        }
      },
      legend: {
        display: false
      }
    };
  }

}
