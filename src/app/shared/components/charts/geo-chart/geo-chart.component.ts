import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { GeoChartItem } from '@app/pages/publisher/pub-dashboard/pub-dashboard.service';
import { GeoChartService } from '@app/shared/components/charts/geo-chart/geo-chart.service';
import * as d3 from 'd3';
import * as topojson from 'topojson';

@Component({
  selector: 'app-geo-chart',
  templateUrl: './geo-chart.component.html',
  styleUrls: ['./geo-chart.component.scss'],
  providers: [GeoChartService]
})
export class GeoChartComponent implements OnChanges, OnInit {

  @Input() geoData: GeoChartItem[];

  private world: any;

  constructor(private geoChartService: GeoChartService) {

  }

  ngOnInit() {
    this.geoChartService.getWorldData().subscribe((res) => {
      this.world = res;
      this.ngOnChanges();
    });
  }

  ngOnChanges() {
    if (this.world) {
      this.buildGeoChart(this.geoData);
    }
  }

  buildGeoChart(data) {
    const element = document.getElementById('geo');
    const world = this.world;
    const width  = 750,
          height = 380;

    const projection = d3.geoEquirectangular()
      .scale(120)
      .translate([372, height / 2]);

    const path = d3.geoPath().projection(projection);

    const svg = d3.select(element).select('svg')
      .attr('width', width)
      .attr('height', height);

    const tooltip = d3.select(element.querySelector('.offer-managers__chart-tooltip'));
    const tooltipCountry = tooltip.select(('.country'));
    const tooltipClicks = tooltip.select('.clicks');
    const tooltipConversions = tooltip.select('.conversions');
    const tooltipPayout = tooltip.select('.payout');

    const findMatch = (d) => {
      return data.find(el => el.country === d.properties['Alpha-2']);
    };

    svg.append('g')
      .attr('class', 'land')
      .selectAll('land')
      .data(topojson.feature(world, world.objects.countries1).features)
      .enter().append('path')
      .attr('d', path)
      .style('fill', function (d) {
        if (findMatch(d)) {
          return 'rgb(33, 211, 175)';
        }
      })
      .on('mousemove', function (d) {
        tooltip.transition()
          .duration(100)
          .style('opacity', .9)
          .style('left', (d3.event.layerX - 5) + 'px')
          .style('top', (d3.event.layerY + 20) + 'px');
        tooltipCountry.html(d.properties.name);
        const match = findMatch(d);
        if (match) {
          tooltipClicks.style('display', 'block').select('span').html(match.clicks);
          tooltipConversions.style('display', 'block').select('span').html(match.conversions);
          tooltipPayout.style('display', 'block').select('span').html(`${match.payout} ${match.currency}`);
        }
      })
      .on('mouseout', function () {
        tooltip.transition()
          .duration(200)
          .style('opacity', 0);
        tooltipClicks.style('display', 'none');
        tooltipConversions.style('display', 'none');
        tooltipPayout.style('display', 'none');
      });
  }

}
