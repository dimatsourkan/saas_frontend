import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable()
export class GeoChartService {

  constructor(private http: HttpClient) {

  }

  getWorldData(): Observable<any> {
    return this.http.get('assets/data/world.json')
      .map((res: any) => res)
      .catch((error: any) => error);

  }

}
