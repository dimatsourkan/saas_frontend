export class LinearChartDataset<T> {

  borderColor = '#02c3c8';
  borderWidth = 1;
  pointBackgroundColor = '#02c3c8';
  pointBorderColor = '#fff';
  backgroundColor: CanvasGradient;
  data: number[] = [];
  fullData: T[] = [];

  background(ctx: CanvasRenderingContext2D) {
    this.backgroundColor = ctx.createLinearGradient(0, 0, 0, 250);
    this.backgroundColor.addColorStop(0, 'rgba(2, 195, 200, .5)');
    this.backgroundColor.addColorStop(1, 'rgba(2, 195, 200, .05)');
  }
}

export class LineChartDatasetDark<T> extends LinearChartDataset<T> {

  borderColor = '#88a0ae';
  pointBackgroundColor = '#88a0ae';

  background(ctx: CanvasRenderingContext2D) {
    this.backgroundColor = ctx.createLinearGradient(0, 0, 0, 250);
    this.backgroundColor.addColorStop(0, 'rgba(136, 160, 174, .5)');
    this.backgroundColor.addColorStop(1, 'rgba(136, 160, 174, .05)');
  }

}

export class LineChartDatasetOrange<T> extends LinearChartDataset<T> {

  borderColor = '#FFB100';
  pointBackgroundColor = '#FFB100';

  background(ctx: CanvasRenderingContext2D) {
    this.backgroundColor = ctx.createLinearGradient(0, 0, 0, 250);
    this.backgroundColor.addColorStop(0, 'rgba(255, 177, 0, .5)');
    this.backgroundColor.addColorStop(1, 'rgba(255, 177, 0, .05)');
  }

}
