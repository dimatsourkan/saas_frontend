import { AfterViewInit, Component, ElementRef, Input, OnChanges, ViewChild } from '@angular/core';
import { LinearChartDataset } from '@app/shared/components/charts/linear-chart/linear-chart-dataset.model';
import * as Chart from 'chart.js';
import { ChartData, ChartOptions, ChartTooltipCallback, ChartTooltipItem } from 'chart.js';

@Component({
  selector: 'app-linear-chart',
  templateUrl: './linear-chart.component.html',
  styleUrls: ['./linear-chart.component.scss']
})
export class LinearChartComponent implements AfterViewInit, OnChanges {

  @ViewChild('canvas') private canvasRef: ElementRef;
  @Input() datasets: LinearChartDataset<any>[];
  @Input() labels: string[];
  @Input() titleCallback: (item: ChartTooltipItem[], data: ChartData) => string | string[];
  @Input() labelCallback: (item: ChartTooltipItem, data: ChartData) => string | string[];
  @Input() footerCallback: (item: ChartTooltipItem[], data: ChartData) => string | string[];

  private lineChart: Chart;
  private canvas: HTMLCanvasElement;
  private ctx: CanvasRenderingContext2D;

  updateChart() {
    if (this.lineChart) {
      this.lineChart.data.datasets = this.datasets;
      this.lineChart.data.labels = this.labels;

      this.lineChart.update();
    }
  }

  ngAfterViewInit() {
    this.canvas = this.canvasRef.nativeElement;
    this.ctx = this.canvas.getContext('2d');
    this.lineChart = new Chart(this.ctx, this.config);
  }

  ngOnChanges() {
    this.updateChart();
  }

  private get config(): Chart.ChartConfiguration {

    this.datasets.forEach(d => {
      d.background(this.ctx);
    });

    return {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: this.datasets
      },
      options: this.options
    };
  }

  private get options(): ChartOptions {

    const tooltipsCallbacks: ChartTooltipCallback = {};
    if (this.titleCallback) {
      tooltipsCallbacks.title = this.titleCallback;
    }
    if (this.labelCallback) {
      tooltipsCallbacks.label = this.labelCallback;
    }
    if (this.footerCallback) {
      tooltipsCallbacks.footer = this.footerCallback;
    }

    return {
      responsive: true,
      tooltips: {
        custom: function (tooltip) {
          if (!tooltip) {
            return;
          }
          tooltip.displayColors = false;
        },
        callbacks: tooltipsCallbacks
      },
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          type: 'linear',
          ticks: {
            fontColor: 'rgba(0,0,0,.7)',
            beginAtZero: true
          }
        }],
        xAxes: [{
          ticks: {
            fontColor: 'rgba(0,0,0,.7)'
          }
        }]
      }
    };
  }

}
