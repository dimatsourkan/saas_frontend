import { Component, ElementRef, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { SHOW_DATE_FORMATS } from '@app/core/enums/date-formats';
import { AbstractValueAccessor } from '@app/shared/classes/abstract-value-accessor';
import { LanguageService } from '@app/shared/services/language/language.service';
import * as moment from 'moment';
import { Moment } from 'moment';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { Subscription } from 'rxjs';

/**
 * Компонента должна использоваться как абстракция
 */
@Component({
  selector: 'app-base-date-picker',
  template: ''
})
export class BaseDatePickerComponent extends AbstractValueAccessor implements OnInit, OnDestroy {

  @ViewChild('dateInput') dateInput: ElementRef;
  @ViewChild('pickerHtml') pickerHtml: ElementRef;
  @Output() onApply = new EventEmitter<string>();

  @Input() disabled = false;
  @Input() maxDateTime: Moment;
  @Input() position: 'left' | 'right' = 'left';

  /**
   * Формат с которым компонента будет отдавать и принимать дату
   */
  @Input() workingFormat = null;

  /**
   * Формат для отображения выбранных дат в зависимости от языка
   */
  get showFormat() {
    return SHOW_DATE_FORMATS[this.locale].DATE_PICKER;
  }

  isOpened = false;
  dateTime = this.momentValueTime(moment());

  protected subscription$: Subscription;
  protected locale: string;

  constructor(
    private dateTimeAdapter: DateTimeAdapter<any>,
    private languageService: LanguageService
  ) {
    super();
  }

  /**
   * При изменении языка меняет локализацию календаря
   */
  ngOnInit() {
    this.subscription$ = this.languageService.lang$.subscribe((locale: string) => {
      this.locale = locale;
      this.dateTimeAdapter.setLocale(locale);
    });
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  /**
   * Сброс даты в календаре, до предыдущей выбранной
   */
  resetPicker() {
    this.dateTime = this.value ? moment(this.value) : moment();
    this.closePicker();
  }

  /**
   * Применение выбраной даты
   */
  applyChanges() {
    this.value = this.momentValueTime(this.dateTime).format(this.workingFormat);
    this.onApply.emit(this.value);
    this.closePicker();
  }

  /**
   * Get и Set для работы с внутренним value
   */
  get value(): string {
    return this._value;
  }

  set value(v: string) {
    this._value = v;
    this.onChange(v);
  }

  /**
   * Дата которая отображается в input
   */
  get inputValue() {
    return this._value ? moment(this._value).format(this.showFormat) : null;
  }

  /**
   * Обновляет дату по изменению ее в input руками
   * @param value
   */
  setInputValue(value: string) {

    const date = moment(value, this.showFormat);

    if (value && date.isValid()) {
      this.dateTime.date(date.date());
      this.dateTime.month(date.month());
      this.dateTime.year(date.year());
      this.dateTime.hour(date.hour());
      this.dateTime.minutes(date.minutes());
      this.dateTime.seconds(date.seconds());
      this.value = this.momentValueTime(this.dateTime).format();
      this.onApply.emit(this.value);
    }

    this.dateInput.nativeElement.value = this._value ? moment(this._value).format(this.showFormat) : null;
  }

  /**
   * Обновление данных компоненты
   * @param value
   */
  writeValue(value: string) {
    if (value) {
      this.dateTime = this.momentValueTime(value);
      this.value = this.momentValueTime(value).format(this.workingFormat);
    } else {
      this.value = null;
    }
  }

  showPicker() {
    this.isOpened = true;
  }

  closePicker() {
    this.isOpened = false;
  }

  /**
   * Конвертирует дату для работы по utc, используется и при получении данных и при отдаче
   * @param date
   */
  momentValueTime(date: Moment | string) {
    return moment(date, this.workingFormat).utc(true).startOf('day');
  }

  /**
   * Слушает клики на странице, в случае срабатывания не на календаре, закрывает его
   * @param target
   */
  @HostListener('document:click', ['$event.target'])
  private clickOverlay(target: any) {
    if (!this.pickerHtml.nativeElement.contains(target) && document.contains(target)) {
      this.resetPicker();
    }
  }

}
