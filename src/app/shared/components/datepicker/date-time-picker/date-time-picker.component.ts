import { Component, ElementRef, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { SHOW_DATE_FORMATS } from '@app/core/enums/date-formats';
import { BaseDatePickerComponent } from '@app/shared/components/datepicker/base-date-picker.component';
import * as moment from 'moment';
import { Moment } from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';

const CUSTOM_FORMATS = {
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'DD-MM-YYYY HH:mm:ss',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'DD-MM-YYYY HH:mm:ss',
  monthYearA11yLabel: 'DD-MM-YYYY HH:mm:ss'
};

@Component({
  selector: 'app-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.scss'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: CUSTOM_FORMATS},
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateTimePickerComponent),
      multi: true
    }
  ]
})
export class DateTimePickerComponent extends BaseDatePickerComponent {

  @ViewChild('dateInput') dateInput: ElementRef;
  @ViewChild('pickerHtml') pickerHtml: ElementRef;
  @Output() onApply = new EventEmitter<string>();
  @Input() disabled = false;

  @Input() minDate: Moment;
  /**
   * Формат для отображения выбранных дат в зависимости от языка
   */
  get showFormat() {
    return SHOW_DATE_FORMATS[this.locale].DATETIME_PICKER;
  }

  momentValueTime(date: Moment | string) {
    return moment(date).utc();
  }

}
