import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UrlAsyncValidator } from '../../validators/url-async.validator';

@Component({
  selector: 'app-event-postback',
  templateUrl: './event-postback.component.html',
  styleUrls: ['./event-postback.component.scss']
})
export class EventPostbackComponent implements OnInit, OnChanges {
  eventPostbackForm: FormGroup;
  @Input() list: any[];
  @Output() onClick = new EventEmitter<any>();
  @Output() eventPostback = new EventEmitter<Array<{ goalId: number, url: string }>>();

  events: FormArray;
  submitted: boolean;

  constructor(private _fb: FormBuilder, private _urlAsyncValidator: UrlAsyncValidator) {
  }

  ngOnInit() {
    this.eventPostbackForm = this._fb.group({
      postbacks: this._fb.array([this.createEvent()])
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.list && !changes.list.isFirstChange()) {
      this.eventPostbackForm.setControl('postbacks', this._fb.array([]));
      this.list.map(li => this.addEvent(li));
      if (this.list.length === 0) {
        this.addEvent();
      }
    }
  }

  createEvent(item?): FormGroup {
    return this._fb.group({
      goalId: [(item) ? item.goalId : '', [Validators.required]],
      url: [(item) ? item.url : '', [Validators.required], this._urlAsyncValidator.urlValidator()]
    });
  }

  addEvent(item?): void {
    this.events = this.eventPostbackForm.get('postbacks') as FormArray;
    this.events.push(this.createEvent(item));
  }

  submitForm() {
    if (this.eventPostbackForm.valid) {
      this.eventPostback.emit(this.eventPostbackForm.get('postbacks').value);
      this.closeModal();
      this.eventPostbackForm.reset();
      this.submitted = false;
    } else {
      this.submitted = true;
    }
  }

  closeModal() {
    this.onClick.emit();
  }

  removeFromFormArray(name: string, index: number): void {
    (<FormArray>this.eventPostbackForm.get(name)).removeAt(index);
  }

}
