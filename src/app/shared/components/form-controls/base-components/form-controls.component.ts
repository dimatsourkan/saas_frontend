import { AfterViewInit, Component, Injector, Input } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms';

@Component({
  template: ''
})
export class FormControlsComponent implements ControlValueAccessor, AfterViewInit {

  @Input() formControlName: string;
  @Input() value: any = null;
  @Input() disabled = false;
  @Input() maxlength: number;
  @Input() required = false;
  @Input() placeholder = '';
  @Input() name = '';
  control = new FormControl();

  private propagateChange: any = (_) => {
    this.value = _;
  }

  private propagateTouch: any = (_) => {
    this.value = _;
  }

  constructor(protected injector: Injector) {
  }

  changeValue(value: any) {
    this.propagateChange(value);
    this.propagateTouch(value);
  }

  markAsTouched() {
    this.control.markAsTouched({ onlySelf: true });
  }

  writeValue(value: any) {
    if (value || value === false || value === '' || value === 0 || Array.isArray(value)) {
      this.value = value;
    } else {
      this.value = null;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  ngAfterViewInit() {

    const ngControl: NgControl = this.injector.get(NgControl, null);
    if (ngControl) {
      this.control = ngControl.control as FormControl;
    }

  }
}
