import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-form-label',
  styles: [`
 
  `],
  template: `
    <label class="createOffer__label" [ngClass]="{ 'createOffer__label_required' : required }">
      <ng-content></ng-content>
    </label>
  `
})
export class FormLabelComponent {
  @Input() required = false;
}
