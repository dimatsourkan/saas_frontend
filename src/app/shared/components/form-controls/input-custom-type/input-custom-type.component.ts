import { Component, EventEmitter, forwardRef, Injector, Input, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../base-components/form-controls.component';

@Component({
  selector: 'app-input-custom-type',
  styles: [`
    .createOffer__input_disabled input {
      background:#e4e4e4;
    }

    .createOffer__input__error-message.css-popover{
      width: auto;
      height: 20px;
      right: 0;
      left: auto;
      z-index: 10;
    }
    .createOffer__input_invalid .createOffer__input__error-message{
      display: block;
    }
  `],
  template: `
    <div class="form-detail">
      <div class="createOffer__input" [ngClass]="{
        'createOffer__input_invalid': control?.touched && control?.invalid,
        'createOffer__input_percent' : percent,
        'createOffer__input_disabled' : disabled
      }">
        <input #input
               [type]="type"
               [required]="required"
               [placeholder]="placeholder"
               [attr.maxlength]="maxlength"
               [disabled]="disabled ? true : null"
               (blur)="markAsTouched()"
               [value]="value"
               [class.ng-invalid-input]="control?.touched && control?.invalid"
               (input)="changeValue($event.target.value);"
               (change)="changeValue($event.target.value)">
        <button type="button" class="pub-item__plus" *ngIf="addBtn" (click)="btnClick()">+</button>
        <div class="createOffer__input__explain-text" *ngIf="explain">{{ explain }}</div>
        <div class="createOffer__input__error-message css-popover" *ngIf="isPopover" appCssPopover>
          <app-validation-message
            [control]="control"
            *ngIf="control?.touched && control.invalid"
            [isPopover]="true">
          </app-validation-message>
        </div>
        <div class="createOffer__input__error-message" *ngIf="!isPopover">
          <app-validation-message [control]="control" *ngIf="control?.touched && control.invalid">
          </app-validation-message>
        </div>
      </div>
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputCustomTypeComponent),
      multi: true
    }
  ]
})
export class InputCustomTypeComponent extends FormControlsComponent {

  @Input() type = 'email';
  @Input() explain = '';
  @Input() percent = false;
  @Input() addBtn: number;
  @Input() isPopover: boolean;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onBtnClick = new EventEmitter<any>();
  @ViewChild('input') private input: any;

  constructor(protected injector: Injector) {
    super(injector);
  }

  btnClick() {
    this.onBtnClick.emit();
  }

  writeValue(value: any) {
    super.writeValue(value);
    this.input.nativeElement.value = value;
  }

}
