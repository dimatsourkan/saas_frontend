import { Component, forwardRef, Input, Output, EventEmitter, ViewChild, Injector, SimpleChanges } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../base-components/form-controls.component';

@Component({
  selector: 'app-input-number-integer',
  styles: [`
  .createOffer__input_disabled input {
    background: #e4e4e4;
  }

  .createOffer__input__error-message.css-popover {
    width: auto;
    right: 0;
    top: 48%;
    left: auto;
    z-index: 10;
  }

  .createOffer__input_invalid .createOffer__input__error-message {
    display: block;
  }

  .hasError {
    margin-left: 91px;
  }

  .createOffer__input_price .createOffer__input__error-message.css-popover,
  .createOffer__input_percent .createOffer__input__error-message.css-popover {
    right: -30px;
  }

  .createOffer__input__error-message span {
    color: #fff;
    background-color: #f33155;
  }
`],
  template: `
  <div class="form-detail">
    <div class="createOffer__input createOffer__input-{{customClass.toLowerCase()}}" [ngClass]="{
      'createOffer__input_invalid': control?.touched && control?.invalid && submitted,
      'createOffer__input_disabled' : disabled
    }">
      <input #input
             type="text"
             [required]="required"
             [placeholder]="placeholder"
             [attr.maxlength]="maxlength"
             [disabled]="disabled ? true : null"
             (blur)="markAsTouched()"
             [value]="value"
             [class.ng-invalid-input]="control?.touched && control?.invalid"
             (input)="change($event)">
      <div class="createOffer__input__error-message css-popover" *ngIf="submitted" appCssPopover>
        <app-validation-message [control]="control" *ngIf="control?.touched && control.invalid" [isPopover]="true">
        </app-validation-message>
      </div>
    </div>
  </div>
`,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputNumberIntegerComponent),
      multi: true
    }
  ]
})
export class InputNumberIntegerComponent extends FormControlsComponent {

  @Input() explain = '';
  @Input() percent = false;
  @Input() integer = false;
  @Input() price = false;
  @Input() customClass: string = '';
  @Input() submitted: boolean;
  @ViewChild('input') private input: any;
  private regexpNum: RegExp = new RegExp(`^[0-9]*$`);

  constructor(protected injector: Injector) {
    super(injector);
  }

  change(event: any) {
    const value = event.target.value ? parseInt(event.target.value, 10) : null;
    this.changeValue(value);
  }


  writeValue(value: any) {
    value = (value) ? value : 0;
    super.writeValue(value);
    this.input.nativeElement.value = value;
  }
}
