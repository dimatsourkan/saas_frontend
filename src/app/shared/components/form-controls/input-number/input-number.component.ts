import { Component, EventEmitter, forwardRef, Injector, Input, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../base-components/form-controls.component';

@Component({
  selector: 'app-input-number',
  styles: [`
    .createOffer__input_disabled input {
      background: #e4e4e4;
    }

    .createOffer__input__error-message.css-popover {
      width: auto;
      height: 20px;
      right: 0;
      left: auto;
      z-index: 10;
    }

    .createOffer__input_invalid .createOffer__input__error-message {
      display: block;
    }

    .hasError {
      margin-left: 91px;
    }

    .createOffer__input_price .createOffer__input__error-message.css-popover,
    .createOffer__input_percent .createOffer__input__error-message.css-popover {
      right: -30px;
    }

    .createOffer__input__error-message span {
      color: #fff;
      background-color: #f33155;
    }
  `],
  template: `
    <div class="form-detail">
      <div class="createOffer__input createOffer__input-{{currency.toLowerCase()}}" [ngClass]="{
        'createOffer__input_invalid': control?.touched && control?.invalid && submitted,
        'createOffer__input_percent' : percent,
        'createOffer__input_price': price,
        'createOffer__input_disabled' : disabled
      }">
        <input #input
               type="text"
               [required]="required"
               [placeholder]="placeholder"
               [attr.maxlength]="maxlength"
               [disabled]="disabled ? true : null"
               (blur)="markAsTouched()"
               [value]="value"
               [min]="min"
               [max]="max"
               [class.ng-invalid-input]="control?.touched && control?.invalid"
               (input)="change($event)" (keypress)="onKeyPress($event)">
        <button type="button" class="pub-item__plus" *ngIf="addBtn" (click)="btnClick()">+</button>
        <div class="createOffer__input__explain-text" *ngIf="explain">{{ explain }}</div>
        <div class="createOffer__input__error-message css-popover" *ngIf="isPopover" appCssPopover>
          <app-validation-message [control]="control" *ngIf="control?.touched && control.invalid" [isPopover]="true">
          </app-validation-message>
        </div>
        <div class="createOffer__input__error-message" *ngIf="!isPopover" [ngClass]="{'hasError': !!explain}">
          <app-validation-message [control]="control" *ngIf="control?.touched && control.invalid">
          </app-validation-message>
        </div>
      </div>
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputNumberComponent),
      multi: true
    }
  ]
})
export class InputNumberComponent extends FormControlsComponent {

  @Input() explain = '';
  @Input() min: number = null;
  @Input() max: number = null;
  @Input() percent = false;
  @Input() integer = false;
  @Input() price = false;
  @Input() addBtn: number;
  @Input() numberAfterPoint: number = 4;
  @Input() currency: string = '';
  @Input() isPopover: boolean;
  @Input() submitted: boolean;
  @Output() onBtnClick = new EventEmitter<any>();
  @ViewChild('input') private input: any;
  private regexpFloat: RegExp = new RegExp(`^[0-9]*[.]?[0-9]{0,${this.numberAfterPoint}}$`);
  private regexpNum: RegExp = new RegExp(`^[0-9]*$`);

  constructor(protected injector: Injector) {
    super(injector);
  }

  change(event: any) {
    const value = event.target.value ? this.parse(event.target.value) : null;
    this.changeValue(value);
  }

  parse(value: string) {
    if (this.integer) {
      return parseInt(value, 10);
    } else {
      return (value[value.length - 1] === '.') ? `${parseFloat(value)}.` : parseFloat(value);
    }
  }

  btnClick() {
    this.onBtnClick.emit();
  }

  writeValue(value: any) {
    value = (value) ? value : 0;
    super.writeValue(value);
    this.input.nativeElement.value = value;
  }

  onKeyPress(event: any) {

    if (event.key.length > 1) {
      return true;
    }

    const position = caretPosition(event.target);
    const value = getFinalString(event.target.value, position.start, position.end, event.key);

    if (this.integer) {
      if (!this.regexpNum.test(value)) {
        return false;
      }
    } else {
      if (!this.regexpFloat.test(value)) {
        return false;
      }
    }

  }

}

function getFinalString(string: string, start: number, end: number, value: string) {
  const beforeSubStr = string.substring(0, start);
  const afterSubStr = string.substring(end, string.length);
  return beforeSubStr + value + afterSubStr;

}

function caretPosition(input) {
  const start = input.selectionStart;
  const end = input.selectionEnd;

  return {
    start, end
  };
}


