import { AfterViewInit, Component, EventEmitter, forwardRef, Injector, Input, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { caretPosition, getFinalString } from '@app/core/helpers/helpers';
import * as Inputmask from 'inputmask/dist/inputmask/inputmask';
import 'inputmask/dist/inputmask/inputmask.extensions';
import { FormControlsComponent } from '../base-components/form-controls.component';

@Component({
  selector: 'app-input-text',
  styles: [`
    .createOffer__input_disabled input {
      background: #e4e4e4;
    }

    .createOffer__input__error-message.css-popover {
      width: auto;
      height: 20px;
      right: 0;
      left: auto;
      z-index: 10;
    }

    .createOffer__input_invalid .createOffer__input__error-message {
      display: block;
    }

    .hasError {
      margin-left: 91px;
    }
  `],
  template: `
    <div class="form-detail">
      <div class="createOffer__input" [ngClass]="{
        'createOffer__input_invalid': control?.touched && control?.invalid && submitted,
        'createOffer__input_percent' : percent,
        'createOffer__input_disabled' : disabled
      }">
        <input #input
               type="text"
               [required]="required"
               [placeholder]="placeholder"
               [attr.maxlength]="maxlength"
               [disabled]="disabled ? true : null"
               (blur)="markAsTouched()"
               [value]="value"
               [class.ng-invalid-input]="control?.touched && control?.invalid"
               (input)="changeValue($event.target.value);"
               (keypress)="onKeyPress($event)">
        <button type="button" class="pub-item__plus" *ngIf="addBtn" (click)="btnClick()">+</button>
        <div class="createOffer__input__explain-text" *ngIf="explain">{{ explain }}</div>
        <div class="createOffer__input__error-message css-popover" *ngIf="isPopover" appCssPopover>
          <app-validation-message
            [control]="control"
            *ngIf="control?.touched && control.invalid"
            [isPopover]="true">
          </app-validation-message>
        </div>
        <div class="createOffer__input__error-message" *ngIf="!isPopover" [ngClass]="{'hasError': !!explain}">
          <app-validation-message [control]="control" *ngIf="control?.touched && control.invalid">
          </app-validation-message>
        </div>
      </div>
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputTextComponent),
      multi: true
    }
  ]
})
export class InputTextComponent extends FormControlsComponent implements AfterViewInit {

  @Input() explain = '';
  @Input() percent = false;
  @Input() addBtn: number;
  @Input() isPopover: boolean;
  @Input() submitted: boolean;
  @Input() mask: string = null;
  @Input() regexp: RegExp | RegExp[] = null;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onBtnClick = new EventEmitter<any>();

  @ViewChild('input') private input: any;

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    if (this.mask) {
      Inputmask(this.mask, {}).mask(this.input.nativeElement);
    }

  }

  btnClick() {
    this.onBtnClick.emit();
  }

  writeValue(value: any) {
    super.writeValue(value);
    this.input.nativeElement.value = value;
  }

  onKeyPress(event: any) {

    if (!this.regexp || event.key.length > 1) {
      return true;
    }

    const position = caretPosition(event.target);
    const value: string = getFinalString(event.target.value, position.start, position.end, event.key);

    if (this.regexp instanceof RegExp) {
      if (!this.regexp.test(value)) {
        return false;
      }
    } else {
      return this.regexp.filter(regexp => {
        return regexp.test(value);
      }).length === this.regexp.length;
    }

  }

}
