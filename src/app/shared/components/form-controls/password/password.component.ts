import { Component, forwardRef, Injector, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../base-components/form-controls.component';

@Component({
  selector: 'app-password',
  styles: [`
  .createOffer__input__error-message.css-popover{
    width: auto;
    height: 20px;
    right: 0;
    left: auto;
    z-index: 10;
  }
  .createOffer__input_invalid .createOffer__input__error-message{
    display: block;
  }
  .createOffer__input {
    width: auto;
  }
  `],
  template: `
    <div class="form-detail">
      <div class="createOffer__input" [ngClass]="{
        'createOffer__input_invalid': control.touched && control.invalid && submitted
      }">
        <input #input
               type="password"
               [required]="required"
               [placeholder]="placeholder"
               [attr.maxlength]="maxlength"
               [disabled]="disabled ? true : null"
               (blur)="markAsTouched()"
               [value]="value"
               [class.ng-invalid-input]="control?.touched && control?.invalid && submitted"
               (input)="changeValue($event.target.value);"
               (change)="changeValue($event.target.value)">
             <div class="createOffer__input__error-message css-popover" *ngIf="isPopover" appCssPopover>
               <app-validation-message
                 [control]="control"
                 [isPopover]="true"
                 *ngIf="control?.touched && control.invalid && submitted">
               </app-validation-message>
             </div>
             <div class="createOffer__input__error-message" *ngIf="!isPopover">
               <app-validation-message [control]="control" *ngIf="control?.touched && control.invalid && submitted">
               </app-validation-message>
             </div>
      </div>
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PasswordComponent),
      multi: true
    }
  ]
})
export class PasswordComponent extends FormControlsComponent {

  @Input() isPopover: boolean;
  @Input() submitted: boolean;
  @ViewChild('input') private input: any;

  constructor(protected injector: Injector) {
    super(injector);
  }

  writeValue(value: any) {
    super.writeValue(value);
    this.input.nativeElement.value = value;
  }

}
