import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AdvertiserFilter } from '@app/core/entities/advertiser/advertiser.filter';
import { AdvertiserService } from '@app/core/entities/advertiser/advertiser.service';
import { Observable } from 'rxjs';
import { BaseLiveSelectComponent } from '../../../base-live-select/base-live-select.component';

@Component({
  selector: 'app-advertisers-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <div class="createOffer__input">
      <div class="select-field">
        <ng-select [items]="itemsArray"
                   bindLabel="username"
                   bindValue="id"
                   [multiple]="multiple"
                   [clearable]="clearable"
                   [typeahead]="typeahead"
                   [(ngModel)]="selectedItems"
                   (open)="onOpen()"
                   (clear)="onClear()"
                   (change)="onSelect()"
                   [disabled]="disabled"
                   [virtualScroll]="true"
                   [loading]="loading"
                   (scroll)="onScroll($event)"
                   (scrollToEnd)="onScrollToEnd()"
                   [placeholder]="'algorithm.manage.advertiserSelect' | translate"></ng-select>
      </div>
    </div>
  `,
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AdvertisersSelectComponent),
      multi: true
    }
  ]
})
export class AdvertisersSelectComponent extends BaseLiveSelectComponent {


  @Input() disabled: boolean;
  term: string;
  page = 1;

  private filter = (new AdvertiserFilter()).init();

  constructor(cd: ChangeDetectorRef, private advertiserService: AdvertiserService) {
    super(cd);
  }

  onClear() {
    this.term = '';
    this.page = 1;
    this.typeahead.emit('');
  }

  load(term: string, uppPage?): Observable<any> {
    if (uppPage) {
      this.page++;
    } else {
      this.page = 1;
    }

    this.filter.search = term;
    this.filter.limit = 25;
    this.filter.page = this.page;
    return this.advertiserService.getKeyList(this.filter.filter);
  }
}
