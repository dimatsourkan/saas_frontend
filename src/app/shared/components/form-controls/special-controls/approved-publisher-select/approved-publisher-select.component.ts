import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { PublisherFilter } from '@app/core/entities/publisher/publisher.filter';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';
import { Observable } from 'rxjs';
import { BaseLiveSelectComponent } from '../../../base-live-select/base-live-select.component';

@Component({
  selector: 'app-approved-publisher-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="username"
               bindValue="id"
               [multiple]="multiple"
               [clearable]="clearable"
               [typeahead]="typeahead"
               [(ngModel)]="selectedItems"
              (open)="onOpen()"
               (clear)="onClear()"
               (change)="onSelect()"
               [virtualScroll]="true"
               [loading]="loading"
               (scroll)="onScroll($event)"
               (scrollToEnd)="onScrollToEnd()"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ApprovedPublisherSelectComponent),
      multi: true
    }
  ]
})
export class ApprovedPublisherSelectComponent extends BaseLiveSelectComponent {
  @Input() id: string;
  page = 1;
  private filter = (new PublisherFilter()).init();

  constructor(cd: ChangeDetectorRef, private publisherService: PublisherService) {
    super(cd);
  }

  onClear() {
    this.term = '';
    this.page = 1;
    this.typeahead.emit('');
  }

  load(term: string, uppPage?): Observable<any> {
    if (uppPage) {
      this.page++;
    } else {
      this.page = 1;
    }
    this.filter.search = term;
    this.filter.limit = 25;
    this.filter.page = this.page;
    return this.publisherService.getApprovedPublishers(this.id, this.filter.filter);
  }

}
