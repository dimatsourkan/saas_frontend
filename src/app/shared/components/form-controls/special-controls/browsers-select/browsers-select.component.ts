import { ChangeDetectionStrategy, Component, forwardRef, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BrowsersService } from '../../../../services/browsers/browsers.service';
import { BaseSelectComponent } from '../../../base-select/base-select.component';

@Component({
  selector: 'app-browsers-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="name"
               bindValue="name"
               [multiple]="multiple"
               [clearable]="clearable"
               [(ngModel)]="selectedItems"
               (change)="onSelect()"
               [virtualScroll]="true"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => BrowsersSelectComponent),
    multi: true
  }]
})
export class BrowsersSelectComponent extends BaseSelectComponent implements OnInit {
  constructor(private browsersService: BrowsersService) {
    super();
  }

  ngOnInit() {
    this.browsersService.getBrowsers()
      .subscribe((browsers: any) => {
        this.itemsArray = (browsers.items) ? [...browsers.items] : [];
      });
  }
}

