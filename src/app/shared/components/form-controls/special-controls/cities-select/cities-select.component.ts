import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { debounceTime, switchMap } from 'rxjs/operators';
import { CitiesService } from '../../../../services/cities/cities.service';

@Component({
  selector: 'app-cities-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="name"
               bindValue="geonameId"
               [multiple]="multiple"
               [clearable]="clearable"
               [typeahead]="typeahead"
               [(ngModel)]="selectedItems"
               (close)="onClose()"
               (change)="onSelect()"
               [virtualScroll]="true"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CitiesSelectComponent),
    multi: true
  }]
})
export class CitiesSelectComponent implements OnInit, OnChanges, ControlValueAccessor {
  @Input() countryCode: string;
  @Input() multiple = true;
  @Input() clearable = true;
  @Input() selectPlaceholder = 'Choose ...';
  typeahead = new EventEmitter<string>();
  itemsArray: Array<{ name: string; geonameId: number }> = [];
  selectedItems: number[] | number;
  private params: { [s: string]: string | number; } = {context: 'keylist', limit: 25};
  private propagateChange: any = () => {
  }
  private propagateTouch: any = () => {
  }

  constructor(private cd: ChangeDetectorRef, private citiesService: CitiesService) {
  }

  ngOnInit(): void {
    this.typeahead
      .pipe(
        debounceTime(200),
        switchMap((term) => {
          return this.load(term);
        })
      )
      .subscribe((items: { items: Array<any> }) => {
        this.itemsArray = items.items;
        this.cd.markForCheck();
      }, (err) => {
        this.itemsArray = [];
        this.cd.markForCheck();
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.countryCode && changes.countryCode.currentValue) {
      this.params.country = changes.countryCode.currentValue;
    }
  }

  writeValue(ids: Array<{ name: string, geonameId: number }> | null): void {
    if (Array.isArray(ids) && ids.length) {
      this.selectedItems = ids.map(id => id.geonameId);
      this.itemsArray = ids;
    }
  }

  load(term: string) {
    this.params.search = term;
    return this.citiesService.getCities(this.params);
  }

  onClose(): void {
    this.itemsArray = [];
  }

  onSelect() {
    this.propagateChange(this.selectedItems);
    this.propagateTouch(this.selectedItems);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
}
