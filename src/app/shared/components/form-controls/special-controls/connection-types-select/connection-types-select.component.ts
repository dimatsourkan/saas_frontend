import { ChangeDetectionStrategy, Component, forwardRef, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ConnectionTypesService } from '../../../../services/connection-types/connection-types.service';
import { BaseSelectComponent } from '../../../base-select/base-select.component';

@Component({
  selector: 'app-connection-types-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="name"
               bindValue="name"
               [multiple]="multiple"
               [clearable]="clearable"
               [(ngModel)]="selectedItems"
               (change)="onSelect()"
               [virtualScroll]="true"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ConnectionTypesSelectComponent),
    multi: true
  }]
})
export class ConnectionTypesSelectComponent extends BaseSelectComponent implements OnInit {
  constructor(private connectionTypesService: ConnectionTypesService) {
    super();
  }

  ngOnInit() {
    this.connectionTypesService.getConnectionTypes()
      .subscribe((types: any) => {
        this.itemsArray = (types.items) ? [...types.items] : [];
      });
  }
}
