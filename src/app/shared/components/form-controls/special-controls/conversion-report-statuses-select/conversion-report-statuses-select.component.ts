import { ChangeDetectionStrategy, Component, forwardRef, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ConversionReportStatusesService } from '../../../../services/conversion-report-statuses/conversion-report-statuses.service';
import { BaseSelectComponent } from '../../../base-select/base-select.component';

@Component({
  selector: 'app-conversion-report-statuses-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="label"
               bindValue="key"
               [multiple]="multiple"
               [clearable]="clearable"
               [(ngModel)]="selectedItems"
               (change)="onSelect()"
               [virtualScroll]="true"
               [placeholder]="selectPlaceholder"
    ></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ConversionReportStatusesSelectComponent),
    multi: true
  }]
})
export class ConversionReportStatusesSelectComponent extends BaseSelectComponent implements OnInit {

  constructor(private statusService: ConversionReportStatusesService) {
    super();
  }

  ngOnInit() {
    this.statusService.getStatus()
      .subscribe((items: any[]) => {
        this.itemsArray = items.map((item) => {
          return {
            key: item.label.id,
            label: item.label.value
          };
        });
      });
  }

}
