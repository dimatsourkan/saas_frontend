import { AfterViewInit, ChangeDetectionStrategy, Component, forwardRef, Injector, Input, OnInit } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { Countries } from '../../../../interfaces/countries';
import { CountriesService } from '../../../../services/countries/countries.service';
import { BaseSelectComponent } from '../../../base-select/base-select.component';

@Component({
  selector: 'app-countries-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <div class="createOffer__input" [ngClass]="{
        'createOffer__input_invalid': control?.touched && control?.invalid && submitted
      }">
      <div class="select-field">
        <ng-select [items]="itemsArray"
                   bindLabel="name"
                   bindValue="code"
                   [multiple]="multiple"
                   [clearable]="clearable"
                   [(ngModel)]="selectedItems"
                   (change)="onSelect()"
                   [virtualScroll]="true"
                   [searchFn]="customSearchFn"
                   [placeholder]="'checker.location' | translate"></ng-select>
      </div>
    </div>
  `,
  styles: [
    `
      :host .createOffer__input {
        width: auto;
      }
    `
  ],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CountriesSelectComponent),
    multi: true
  }]
})
export class CountriesSelectComponent extends BaseSelectComponent implements OnInit, AfterViewInit {

  @Input() isPopover: boolean;
  @Input() submitted: boolean;

  control = new FormControl();

  constructor(
    private countriesService: CountriesService,
    protected injector: Injector
  ) {
    super();
  }

  ngOnInit() {
    this.countriesService.getCountries()
      .pipe(takeUntil(this.destroy$))
      .subscribe((countries: Countries[]) => {
        this.itemsArray = [...countries];
      });
  }

  customSearchFn(term: string, item: any) {
    term = term.toLocaleLowerCase();
    if (term.length === 2) {
      return item.code.toLocaleLowerCase() === term;
    } else {
      return item.name.toLocaleLowerCase().indexOf(term) > -1 || item.code.toLocaleLowerCase() === term;
    }
  }

  ngAfterViewInit() {

    const ngControl: NgControl = this.injector.get(NgControl, null);
    if (ngControl) {
      this.control = ngControl.control as FormControl;
    }

  }
}
