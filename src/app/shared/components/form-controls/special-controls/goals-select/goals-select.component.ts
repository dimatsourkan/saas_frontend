import { ChangeDetectionStrategy, Component, forwardRef, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { DefaultList } from '../../../../interfaces/default-list';
import { GoalsService } from '../../../../services/goals/goals.service';
import { BaseSelectComponent } from '../../../base-select/base-select.component';

@Component({
  selector: 'app-goals-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="name"
               bindValue="id"
               [multiple]="false"
               [clearable]="false"
               [(ngModel)]="selectedItems"
               (change)="onSelect()"
               [virtualScroll]="true"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => GoalsSelectComponent),
    multi: true
  }]
})
export class GoalsSelectComponent extends BaseSelectComponent implements OnInit {
  constructor(private goalsService: GoalsService) {
    super();
  }

  ngOnInit() {
    this.goalsService.getGoals()
      .pipe(takeUntil(this.destroy$))
      .subscribe((goals: DefaultList[]) => {
        if (Array.isArray(goals)) {
          this.itemsArray = [...goals];
        }
      });
  }
}
