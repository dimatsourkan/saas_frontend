import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { GroupUrlsService } from '../../../../../pages/manager/superlink/group-urls/group-urls.service';
import { BaseLiveSelectComponent } from '../../../base-live-select/base-live-select.component';

@Component({
  selector: 'app-group-urls-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <div class="createOffer__input">
      <div class="select-field">
        <ng-select [items]="itemsArray"
                   bindLabel="name"
                   bindValue="id"
                   [multiple]="multiple"
                   [clearable]="clearable"
                   [typeahead]="typeahead"
                   [(ngModel)]="selectedItems"
                  (open)="onOpen()"
               (clear)="onClear()"
                   (change)="onSelect()"
                   [virtualScroll]="true"
                   [loading]="loading"
                   (scroll)="onScroll($event)"
                   (scrollToEnd)="onScrollToEnd()"
                   [placeholder]="selectPlaceholder"></ng-select>
      </div>
    </div>
  `,
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => GroupUrlsSelectComponent),
      multi: true
    }
  ]
})
export class GroupUrlsSelectComponent extends BaseLiveSelectComponent {
  page = 1;

  constructor(cd: ChangeDetectorRef, private groupUrlsService: GroupUrlsService) {
    super(cd);
  }

  load(term: string, uppPage?): Observable<any> {
    if (uppPage) {
      this.page++;
    } else {
      this.page = 1;
    }
    const params = {search: term, context: 'keylist', limit: 25, page: this.page};
    return this.groupUrlsService.getGroupUrls(params);
  }

}
