import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { IntegrationsPlatformsService } from '../../../../services/integrations-platforms.service';
import { BaseLiveSelectComponent } from '../../../base-live-select/base-live-select.component';

@Component({
  selector: 'app-integrations-platforms-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="username"
               bindValue="id"
               [multiple]="multiple"
               [clearable]="clearable"
               [typeahead]="typeahead"
               [(ngModel)]="selectedItems"
              (open)="onOpen()"
               (clear)="onClear()"
               (change)="onSelect()"
               [virtualScroll]="true"
               [loading]="loading"
               (scroll)="onScroll($event)"
               (scrollToEnd)="onScrollToEnd()"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => IntegrationsPlatformsSelectComponent),
      multi: true
    }
  ]
})
export class IntegrationsPlatformsSelectComponent extends BaseLiveSelectComponent {
  page = 1;

  constructor(cd: ChangeDetectorRef, private _integrationsPlatformsService: IntegrationsPlatformsService) {
    super(cd);
  }

  load(term: string, uppPage?): Observable<any> {
    if (uppPage) {
      this.page++;
    } else {
      this.page = 1;
    }
    const params = {search: term, context: 'keylist', limit: 25, page: this.page};
    return this._integrationsPlatformsService.getIntegrationsPlatforms(params);
  }
}
