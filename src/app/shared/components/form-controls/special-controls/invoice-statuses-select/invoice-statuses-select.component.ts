import { ChangeDetectionStrategy, Component, forwardRef, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { CountriesService } from '../../../../services/countries/countries.service';
import { InvoiceStatusService } from '../../../../services/invoiceStatuses/invoice-status.service';
import { BaseSelectComponent } from '../../../base-select/base-select.component';

@Component({
  selector: 'app-invoice-statuses',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="name"
               bindValue="id"
               [multiple]="false"
               [clearable]="clearable"
               [(ngModel)]="selectedItems"
               (change)="onSelect()"
               [virtualScroll]="true"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InvoiceStatusesSelectComponent),
    multi: true
  }]
})
export class InvoiceStatusesSelectComponent extends BaseSelectComponent implements OnInit {

  constructor(private countriesService: CountriesService, private invoiceStatusService: InvoiceStatusService) {
    super();
  }

  ngOnInit() {
    this.invoiceStatusService.getStatuses$()
      .pipe(takeUntil(this.destroy$))
      .subscribe((statuses: any[]) => {
        this.itemsArray = [...statuses];
      });
  }
}
