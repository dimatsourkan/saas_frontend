import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ManagerFilter } from '@app/core/entities/manager/manager.filter';
import { ManagerService } from '@app/core/entities/manager/manager.service';
import { Observable } from 'rxjs';
import { BaseLiveSelectComponent } from '../../../base-live-select/base-live-select.component';

/**
 * @description
 *
 * Live Select for Managers
 *
 * @param {boolean}: multiple - choose single item or multiple
 * @param {boolean}: clearable - add clear button
 * @param {string}: selectPlaceholder - placeholder text
 *
 * */

@Component({
  selector: 'app-managers-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="username"
               bindValue="id"
               [multiple]="multiple"
               [clearable]="clearable"
               [typeahead]="typeahead"
               [(ngModel)]="selectedItems"
               (open)="onOpen()"
               (clear)="onClear()"
               (change)="onSelect()"
               [virtualScroll]="true"
               [loading]="loading"
               (scroll)="onScroll($event)"
               (scrollToEnd)="onScrollToEnd()"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ManagersSelectComponent),
    multi: true
  }]
})
export class ManagersSelectComponent extends BaseLiveSelectComponent {
  private filter = (new ManagerFilter()).init();
  page = 1;

  constructor(cd: ChangeDetectorRef, private managerService: ManagerService) {
    super(cd);
  }

  onClear() {
    this.term = '';
    this.page = 1;
    this.typeahead.emit('');
  }

  load(term: string, uppPage?): Observable<any> {
    if (uppPage) {
      this.page++;
    } else {
      this.page = 1;
    }
    this.filter.search = term;
    this.filter.limit = 25;
    this.filter.page = this.page;
    return this.managerService.getKeyList(this.filter.filter);
  }
}
