import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { MultyLiveSelectService } from '../../../../services/multy-live-select/multy-live-select.service';
import { BaseLiveSelectComponent } from '../../../base-live-select/base-live-select.component';

@Component({
  selector: 'app-multy-live-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
  <ng-select [items]="itemsArray"
  [bindLabel]="bindLabel"
  [bindValue]="bindValue"
  [multiple]="multiple"
  [clearable]="clearable"
  [(ngModel)]="selectedItems"
  [typeahead]="typeahead"
  (open)="onOpen()"
  (clear)="onClear()"
  (change)="onSelect()"
  [virtualScroll]="true"
  [loading]="loading"
  (scroll)="onScroll($event)"
  (scrollToEnd)="onScrollToEnd()"
  [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultyLiveSelectComponent),
      multi: true
    }
  ]
})
export class MultyLiveSelectComponent extends BaseLiveSelectComponent implements OnChanges {
  @Input() apiUrl: string;
  @Input() bindLabel = 'username';
  @Input() bindValue = 'id';
  @Input() disableTypeahead = false;
  @Input() customParams: any = {};
  page = 1;

  ngOnChanges(changes: SimpleChanges) {
    if (changes.customParams) {
      this.page = 1;
      this.typeahead.emit('');
    }
  }

  constructor(cd: ChangeDetectorRef, private multyLiveSelectService: MultyLiveSelectService) {
    super(cd);
  }

  load(term?: string, uppPage?): Observable<any> {
    if (uppPage) {
      this.page++;
    } else {
      this.page = 1;
    }

    const params = { search: term, context: 'keylist', limit: 25, page: this.page, ...this.customParams };
    return this.multyLiveSelectService.getList(this.apiUrl, params);
  }

}
