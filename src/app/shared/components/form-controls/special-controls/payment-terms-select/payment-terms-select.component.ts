import { ChangeDetectionStrategy, Component, forwardRef, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseSelectComponent } from '../../../base-select/base-select.component';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';

@Component({
  selector: 'app-payment-terms-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="name"
               bindValue="id"
               [multiple]="multiple"
               [clearable]="clearable"
               [(ngModel)]="selectedItems"
               (change)="onSelect()"
               [virtualScroll]="true"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => PaymentTermsSelectComponent),
    multi: true
  }]
})
export class PaymentTermsSelectComponent extends BaseSelectComponent implements OnInit {
  constructor(private service: PublisherService) {
    super();
  }

  ngOnInit() {
    this.service.getPaymentTerms().subscribe(res => {
      this.itemsArray = res.items;
    });
  }
}
