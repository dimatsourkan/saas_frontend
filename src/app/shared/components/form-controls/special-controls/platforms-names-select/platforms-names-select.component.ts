import { ChangeDetectionStrategy, Component, forwardRef, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { PlatformsService } from '../../../../services/platforms/platforms.service';
import { BaseSelectComponent } from '../../../base-select/base-select.component';

@Component({
  selector: 'app-platform-names-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
  <ng-select [items]="itemsArray"
  [multiple]="multiple"
  [clearable]="clearable"
  [(ngModel)]="selectedItems"
  (change)="onSelect()"
  [virtualScroll]="true"
  [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => PlatformsNamesSelectComponent),
    multi: true
  }]
})
export class PlatformsNamesSelectComponent extends BaseSelectComponent implements OnInit {

  constructor(private platformsService: PlatformsService) {
    super();
  }

  ngOnInit() {
    this.platformsService.getPlatformNames()
      .pipe(takeUntil(this.destroy$))
      .subscribe((items: any) => {
        this.itemsArray = [...items.items];
      });
  }

}
