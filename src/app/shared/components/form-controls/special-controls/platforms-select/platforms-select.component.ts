import { ChangeDetectionStrategy, Component, forwardRef, Input, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { PlatformsService } from '../../../../services/platforms/platforms.service';
import { BaseSelectComponent } from '../../../base-select/base-select.component';

@Component({
  selector: 'app-platforms-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
  <div class="createOffer__input">
    <div class="select-field">
    <ng-select [items]="itemsArray"
    bindLabel="name"
    [bindValue]="bindValue"
    [multiple]="multiple"
    [clearable]="clearable"
    [(ngModel)]="selectedItems"
    (change)="onSelect()"
    [virtualScroll]="true"
    [placeholder]="'checker.devices' | translate"></ng-select>
  </div>
  </div>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => PlatformsSelectComponent),
    multi: true
  }]
})
export class PlatformsSelectComponent extends BaseSelectComponent implements OnInit {

  @Input() bindValue = 'name';
  @Input() withOutOther: boolean;

  constructor(private platformsService: PlatformsService) {
    super();
  }

  ngOnInit() {
    this.platformsService.getPlatforms()
      .pipe(takeUntil(this.destroy$))
      .subscribe((items: any) => {
        if (this.withOutOther) {
          items.items = items.items.filter(el => el.id !== 10);
        }
        this.itemsArray = [...items.items];
      });
  }

}
