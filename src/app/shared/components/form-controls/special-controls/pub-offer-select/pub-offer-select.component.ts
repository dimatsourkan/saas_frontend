import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { OffersService } from '../../../../../pages/manager/offers/offers.service';
import { BaseLiveSelectComponent } from '../../../base-live-select/base-live-select.component';

@Component({
  selector: 'app-pub-offer-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="name"
               bindValue="id"
               [multiple]="multiple"
               [clearable]="clearable"
               [typeahead]="typeahead"
               [(ngModel)]="selectedItems"
              (open)="onOpen()"
               (clear)="onClear()"
               (change)="onSelect()"
               [virtualScroll]="true"
               [loading]="loading"
               (scroll)="onScroll($event)"
               (scrollToEnd)="onScrollToEnd()"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PubOfferSelectComponent),
      multi: true
    }
  ]
})
export class PubOfferSelectComponent extends BaseLiveSelectComponent {

  constructor(cd: ChangeDetectorRef, private offersService: OffersService) {
    super(cd);
  }

  load(term: string, uppPage?): Observable<any> {
    if (uppPage) {
      this.page++;
    } else {
      this.page = 1;
    }
    const params = {search: term, context: 'publisher-panel.offers.keylist', limit: 25, page: this.page};
    return this.offersService.getOffers(params);
  }

}
