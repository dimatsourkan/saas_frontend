import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { PublisherInvoiceService } from '../../../../services/publisher-invoice/publisher-invoice.service';
import { BaseLiveSelectComponent } from '../../../base-live-select/base-live-select.component';

@Component({
  selector: 'app-publisher-invoice-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="invoiceNumber"
               bindValue="invoiceNumber"
               [multiple]="multiple"
               [clearable]="clearable"
               [typeahead]="typeahead"
               [(ngModel)]="selectedItems"
               (open)="onOpen()"
               (clear)="onClear()"
               (change)="onSelect()"
               [virtualScroll]="true"
               [loading]="loading"
               (scroll)="onScroll($event)"
               (scrollToEnd)="onScrollToEnd()"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => PublisherInvoiceSelectComponent),
    multi: true
  }]
})
export class PublisherInvoiceSelectComponent extends BaseLiveSelectComponent {

  @Input() dateFrom: string;
  @Input() dateTo: string;

  constructor(cd: ChangeDetectorRef,
              private publisherInvoiceService: PublisherInvoiceService) {
    super(cd);
  }

  load(term: string, uppPage?): Observable<any> {
    if (uppPage) {
      this.page++;
    } else {
      this.page = 1;
    }
    const params = {search: term, context: 'keylist', limit: 25, page: this.page, date_from: this.dateFrom, date_to: this.dateTo};
    return this.publisherInvoiceService.getPublishersInvoice(params);
  }

}
