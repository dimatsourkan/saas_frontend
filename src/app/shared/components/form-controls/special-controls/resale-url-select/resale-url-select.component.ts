import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { SystemService } from '../../../../../pages/manager/superlink/system/system.service';
import { BaseLiveSelectComponent } from '../../../base-live-select/base-live-select.component';

@Component({
  selector: 'app-resale-url-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="name"
               bindValue="id"
               [multiple]="multiple"
               [clearable]="clearable"
               [typeahead]="typeahead"
               [(ngModel)]="selectedItems"
              (open)="onOpen()"
               (clear)="onClear()"
               (change)="onSelect()"
               [virtualScroll]="true"
               [placeholder]="selectPlaceholder"></ng-select>
  `,
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ResaleUrlSelectComponent),
      multi: true
    }
  ]
})
export class ResaleUrlSelectComponent extends BaseLiveSelectComponent {
  page = 1;

  constructor(cd: ChangeDetectorRef, private systemService: SystemService) {
    super(cd);
  }

  load(term: string, uppPage?): Observable<any> {
    if (uppPage) {
      this.page++;
    } else {
      this.page = 1;
    }
    const params = {search: term, context: 'keylist', limit: 25, page: this.page};
    return this.systemService.getAlgotithmUrlNames(params);
  }

}

