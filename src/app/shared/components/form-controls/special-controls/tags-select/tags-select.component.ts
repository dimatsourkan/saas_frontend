import { ChangeDetectionStrategy, Component, forwardRef, Input, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { DefaultList } from '../../../../interfaces/default-list';
import { TagsService } from '../../../../services/tags/tags.service';
import { BaseSelectComponent } from '../../../base-select/base-select.component';

@Component({
  selector: 'app-tags-select',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <ng-select [items]="itemsArray"
               bindLabel="name"
               [bindValue]="bindValue"
               [multiple]="multiple"
               [clearable]="clearable"
               [(ngModel)]="selectedItems"
               (change)="onSelect()"
               [placeholder]="selectPlaceholder"
               [virtualScroll]="true"></ng-select>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TagsSelectComponent),
    multi: true
  }]
})
export class TagsSelectComponent extends BaseSelectComponent implements OnInit {
  @Input() bindValue = 'id';

  constructor(private tagsService: TagsService) {
    super();
  }

  ngOnInit() {
    this.tagsService.getTags()
      .pipe(takeUntil(this.destroy$))
      .subscribe((items: DefaultList[]) => {
        this.itemsArray = [...items];
      });
  }
}
