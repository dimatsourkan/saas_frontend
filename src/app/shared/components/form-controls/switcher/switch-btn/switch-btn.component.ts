import { Component, ElementRef, Input, OnChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-switch-btn',
  template: `
    <label class="switch-btn" [ngClass]="{ 'checked' : isChecked }">
      <ng-content></ng-content>
      <input type="radio" [value]="value" [name]="name" [checked]="checked" #check class="switch-input">
    </label>

  `,
  styleUrls: ['./switcher-btn.component.scss']
})
export class SwitchBtnComponent implements OnChanges {
  @Input() value: string | boolean | number;
  @Input() name = '';
  @Input() checked = false;
  @ViewChild('check') private check: ElementRef;

  get isChecked() {
    return this.check.nativeElement.checked;
  }

  ngOnChanges() {
    this.check.nativeElement.value = this.value;
    this.check.nativeElement.dataset['data'] = JSON.stringify({value: this.value});
  }
}
