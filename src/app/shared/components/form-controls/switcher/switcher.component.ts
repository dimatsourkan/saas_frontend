import { AfterViewInit, Component, ElementRef, forwardRef, Injector, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../base-components/form-controls.component';

@Component({
  selector: 'app-switcher',
  template: `
      <div class="switch" #switchContainer>
        <ng-content></ng-content>
      </div>
      <div class="invalid-feedback invalid-feedback-float invalid-feedback-float--right">
        <app-validation-message [control]="control" *ngIf="control.touched && control.invalid"></app-validation-message>
      </div>
  `,
  styleUrls: ['./switcher.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitcherComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class SwitcherComponent extends FormControlsComponent implements AfterViewInit {
  @Input() label = '';
  @Input() parseInt = false;
  @ViewChild('switchContainer') private switchContainer: ElementRef;
  timeout = null;

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngAfterViewInit() {
    this.writeValue(this.value);
    this.switchContainer.nativeElement.addEventListener('change', e => {
      const value = JSON.parse(e.target.dataset['data']).value;
      this.changeValue(value);
    });
  }

  writeValue(value: any) {
    clearTimeout(this.timeout);
    super.writeValue(value);
    this.timeout = setTimeout(() => {
      const appSwitch = this.switchContainer.nativeElement.querySelector(`input[value="${value}"]`);
      if (appSwitch) {
        appSwitch.checked = true;
      }
    }, 10);
  }
}
