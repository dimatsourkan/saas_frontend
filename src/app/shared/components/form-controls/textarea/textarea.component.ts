import { Component, forwardRef, Injector, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormControlsComponent } from '../base-components/form-controls.component';

@Component({
  selector: 'app-textarea',
  template: `
    <div class="position-relative" [ngClass]="{'is-invalid': control.touched && control.invalid}">
      <textarea appAutosize #area
                class="form-control"
                [required]="required"
                [placeholder]="placeholder"
                [attr.maxlength]="maxlength"
                [disabled]="disabled ? true : null"
                (focus)="markAsTouched()"
                [value]="value"
                [class.ng-invalid-input]="control?.touched && control?.invalid"
                (input)="changeValue($event.target.value);"
                (change)="changeValue($event.target.value)"></textarea>
        <div class="invalid-feedback invalid-feedback-float invalid-feedback-float--right">
          <app-validation-message
            [control]="control"
            *ngIf="control?.touched && control.invalid"
            [isPopover]="true">
          </app-validation-message>
      </div>
    </div>
  `,
  styles: [`
    textarea {
      resize: none;
    }

    .createOffer__input__error-message {
      top: 11px !important;
      height: 20px;
      width: auto;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextareaComponent),
      multi: true
    }
  ]
})
export class TextareaComponent extends FormControlsComponent {

  @ViewChild('area') private area: any;

  constructor(protected injector: Injector) {
    super(injector);
  }

  writeValue(value: any = '') {
    super.writeValue(value);
    this.area.nativeElement.value = value;
  }

}
