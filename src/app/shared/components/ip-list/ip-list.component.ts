import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../../validators/custom-validators.validator';

@Component({
  selector: 'app-ip-list',
  templateUrl: './ip-list.component.html',
  styleUrls: ['./ip-list.component.scss']
})
export class IpListComponent implements OnInit, OnChanges {
  ipListForm: FormGroup;
  patternString: RegExp = /(^((\d|\d\d|1\d\d|2[0-4]\d|25[0-5])\.){3}(\d|\d\d|1\d\d|2[0-4]\d|25[0-5])$)|(^([a-f\d]{0,4}:){7}[a-f\d]{0,4}$)/;

  @Input() titleLabel: string;
  @Input() ipList: string[];
  @Output() onClick = new EventEmitter<string[]>();
  @Output() ipListArray = new EventEmitter<string[]>();

  constructor(private fb: FormBuilder) {
  }

  get formData() {
    return this.ipListForm.get('ip') as FormArray;
  }

  ngOnInit() {
    const form = this.fb.group({
      ip: this.fb.array([
        this.fb.control('', [Validators.required, Validators.pattern(this.patternString)])
      ], CustomValidators.hasDuplicates)
    });
    this.ipListForm = new FormGroup(form.controls);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ipList && !changes.ipList.firstChange) {
      (this.ipListForm.get('ip') as FormArray).controls.forEach((ip) => {
        ip.setValidators([Validators.required, Validators.pattern(this.patternString), CustomValidators.notUnique(this.ipList)]);
      });
    }
  }

  handleIpFields(flag: string, index?: number) {
    const formArray = <FormArray>this.ipListForm.get('ip');
    if (flag === 'add') {
      formArray.push(this.fb.control('', [
        Validators.required,
        Validators.pattern(this.patternString),
        CustomValidators.notUnique(this.ipList || [])
      ]));
    } else if (flag === 'remove') {
      formArray.removeAt(index);
    }
  }

  addIpList() {
    const formArray = <FormArray>this.ipListForm.get('ip');
    formArray.controls.forEach(control => control.markAsTouched());
    if (this.ipListForm.valid) {
      this.ipListArray.emit(this.ipListForm.get('ip').value);
      this.closeModal();
    }
  }

  showIpFieldError(i: number): boolean {
    const control = <FormArray>this.ipListForm.get(`ip.${i}`);
    return control.touched && control.invalid;
  }

  closeModal() {
    this.onClick.emit();
    this.clearIpListForm();
  }

  private clearIpListForm() {
    const formArray = <FormArray>this.ipListForm.get('ip');
    formArray.controls.splice(0);
    formArray.push(this.fb.control('', [
      Validators.required,
      Validators.pattern(this.patternString),
      CustomValidators.notUnique(this.ipList || [])
    ]));
  }
}
