import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UrlAsyncValidator } from '../../validators/url-async.validator';

@Component({
  selector: 'app-offers-postback',
  templateUrl: './offers-postback.component.html',
  styleUrls: ['./offers-postback.component.scss']
})
export class OffersPostbackComponent implements OnInit, OnChanges {
  offerPostbackForm: FormGroup;
  @Input() list: any[];
  @Output() onClick = new EventEmitter<any>();
  @Output() eventPostback = new EventEmitter<Array<{ offerGoalId: number, url: string }>>();
  goalsList: any[] = [];
  offers: FormArray;
  submitted: boolean;

  constructor(private _fb: FormBuilder, private _urlAsyncValidator: UrlAsyncValidator) {
  }

  ngOnInit() {
    this.offerPostbackForm = this._fb.group({
      postbacks: this._fb.array([this.createOffer()])
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.list && !changes.list.isFirstChange()) {
      this.offerPostbackForm.setControl('postbacks', this._fb.array([]));
      this.list.map(li => this.addEvent(li));
      if (this.list.length === 0) {
        this.addEvent();
      }
    }
  }

  createOffer(item?): FormGroup {
    return this._fb.group({
      offerGoalId: [(item) ? item.offerGoalId : null, [Validators.required]],
      offerId: [(item) ? item.offerId : null, [Validators.required]],
      url: [(item) ? item.url : '', [Validators.required], this._urlAsyncValidator.urlValidator()]
    });
  }

  getSelectedObject(event, index) {
    if (event) {
      this.goalsList[index] = event.goals;
    }
  }

  addEvent(item?): void {
    this.offers = this.offerPostbackForm.get('postbacks') as FormArray;
    this.offers.push(this.createOffer(item));
  }

  submitForm() {
    if (this.offerPostbackForm.valid) {
      this.eventPostback.emit(this.offerPostbackForm.get('postbacks').value);
      this.submitted = false;
      this.closeModal();
      this.offerPostbackForm.reset();
      this.submitted = false;
    } else {
      this.submitted = true;
    }
  }

  closeModal() {
    this.onClick.emit();
  }

  removeFromFormArray(name: string, index: number): void {
    (<FormArray>this.offerPostbackForm.get(name)).removeAt(index);
  }

}
