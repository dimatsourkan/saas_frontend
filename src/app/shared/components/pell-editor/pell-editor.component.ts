import { ChangeDetectionStrategy, Component, ElementRef, forwardRef, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import pell from 'pell';

@Component({
  selector: 'app-pell-editor',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <div #description></div>
  `,
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PellEditorComponent),
      multi: true
    }
  ]
})
export class PellEditorComponent implements OnInit, ControlValueAccessor {
  editor: any;
  @ViewChild('description') div: ElementRef;
  private propagateChange: any = () => {
  }
  private propagateTouch: any = () => {
  }

  ngOnInit() {
    this.editor = pell.init({
      element: this.div.nativeElement,
      onChange: html => {
        this.propagateChange(html);
        this.propagateTouch(html);
      },
      defaultParagraphSeparator: 'p',
      styleWithCSS: false,
      actions: [
        'bold',
        'italic',
        'underline',
        'heading1',
        'heading2',
        'paragraph',
        'olist',
        'ulist',
        'line'
      ],
      classes: {
        actionbar: 'pell-actionbar',
        button: 'pell-button',
        content: 'pell-content',
        selected: 'pell-button-selected'
      }
    });
  }


  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  writeValue(html: string): void {
    this.editor.content.innerHTML = html;
  }
}
