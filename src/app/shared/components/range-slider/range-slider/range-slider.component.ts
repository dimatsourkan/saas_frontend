import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, EventEmitter, Inject, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-range-slider',
  template: `
    <div #slider class="slider-range">
      <div #range class="range"></div>
      <a class="handle min" #min (mousedown)="mouseDownMin($event)"></a>
      <a class="handle max" #max (mousedown)="mouseDownMax($event)"></a>
    </div>
  `,
  styleUrls: ['./range-slider.component.scss']
})
export class RangeSliderComponent implements OnChanges {
  @ViewChild('slider') slider: ElementRef;
  @ViewChild('min') minElem: ElementRef;
  @ViewChild('max') maxElem: ElementRef;
  @ViewChild('range') range: ElementRef;

  @Input() min: number = 0;
  @Input() max: number = 100;
  @Input() step: number = 1;
  @Input() valueMin: number = 0;
  @Input() valueMax: number = 100;

  @Output() newValueMin: EventEmitter<number> = new EventEmitter<number>();
  @Output() newValueMax: EventEmitter<number> = new EventEmitter<number>();

  private dragging = false;
  private startPointXMax: number;
  private startPointXMin: number;
  private xPosMax: number;
  private xPosMin: number;
  private mousemoveMinFn = () => {
  }
  private mousemoveMaxFn = () => {
  }

  constructor(@Inject(DOCUMENT) private dom: any) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.dragging) {
      return;
    }

    if (changes.max) {
      this.max = changes.max.currentValue || 100;
      if (!this.valueMax) {
        this.xPosMax = 100;
      }
      this.moveHandle('maxElem', this.xPosMax);
    }

    if (changes.min) {
      this.min = changes.min.currentValue || 0;
      if (!this.valueMin) {
        this.xPosMin = 0;
      }
      this.moveHandle('minElem', this.xPosMin);
    }

    if (changes.valueMin && changes.valueMin.currentValue) {
      this.xPosMin = (changes.valueMin.currentValue - this.min) / (this.max - this.min) * 100;
      if (this.xPosMin < 0) {
        this.xPosMin = 0;
      } else if (this.xPosMin > 100) {
        this.xPosMin = 100;
      }
      this.moveHandle('minElem', this.xPosMin);
    }

    if (changes.valueMax && changes.valueMax.currentValue) {
      this.xPosMax = (changes.valueMax.currentValue - this.min) / (this.max - this.min) * 100;
      if (this.xPosMax < 0) {
        this.xPosMax = 0;
      } else if (this.xPosMax > 100) {
        this.xPosMax = 100;
      }
      this.moveHandle('maxElem', this.xPosMax);
    }
    this.changePosition();
    this.moveRange(this.xPosMin, this.xPosMax);
  }

  changePosition() {
    this.xPosMax = (this.valueMax - this.min) / (this.max - this.min) * 100;
    if (this.xPosMax < 0) {
      this.xPosMax = 0;
    } else if (this.xPosMax > 100) {
      this.xPosMax = 100;
    }
    this.moveHandle('maxElem', this.xPosMax);
  }

  mouseDownMin(event) {
    this.dragging = true;
    this.startPointXMin = event.pageX;

    // Bind to full document, to make move easiery (not to lose focus on y axis)
    this.mousemoveMinFn = this.mousemoveMinListener.bind(this);
    this.dom.addEventListener('mousemove', this.mousemoveMinFn, true);
    this.dom.addEventListener('mouseup', () => {
      this.dragging = false;
      this.dom.removeEventListener('mousemove', this.mousemoveMinFn, true);
      this.dom.onmouseup = null;
    }, true);
  }

  mouseDownMax(event) {
    this.dragging = true;
    this.startPointXMax = event.pageX;

    // Bind to full document, to make move easiery (not to lose focus on y axis)
    this.mousemoveMaxFn = this.mousemoveMaxListener.bind(this);
    this.dom.addEventListener('mousemove', this.mousemoveMaxFn, true);
    this.dom.addEventListener('mouseup', () => {
      this.dragging = false;
      this.dom.removeEventListener('mousemove', this.mousemoveMaxFn, true);
      this.dom.onmouseup = null;
    }, true);
  }

  private moveRange(xPosMin: number, xPosMax: number) {
    this.range.nativeElement.setAttribute('style', `left: ${xPosMin}%; width: ${xPosMax - xPosMin}%`);
  }

  private moveHandle(flag: string, position: number) {
    this[flag].nativeElement.setAttribute('style', `left: ${position}%;`);
    this.setDraggingElementAboveStatic(flag);
  }

  private setDraggingElementAboveStatic(flag: string) {
    this.minElem.nativeElement.style.zIndex = 2;
    this.maxElem.nativeElement.style.zIndex = 2;
    this[flag].nativeElement.style.zIndex = 4;
  }

  private mousemoveMaxListener($event) {
    if (!this.dragging) {
      return;
    }
    const moveDelta = $event.pageX - this.startPointXMax;
    this.xPosMax += ((moveDelta / this.slider.nativeElement.clientWidth) * 100);
    if (this.xPosMax > 100) {
      this.xPosMax = 100;
    } else if (this.xPosMax < this.xPosMin) {
      this.xPosMax = this.xPosMin;
    } else {
      // Prevent generating 'lag' if moving outside window
      this.startPointXMax = $event.pageX;
    }
    this.valueMax = Math.round((((this.max - this.min) * (this.xPosMax / 100)) + this.min) / this.step) * this.step;
    this.newValueMax.emit(this.valueMax);
    // Move the Handle
    this.moveHandle('maxElem', this.xPosMax);
    this.moveRange(this.xPosMin, this.xPosMax);
  }

  private mousemoveMinListener($event) {
    if (!this.dragging) {
      return;
    }

    const moveDelta = $event.pageX - this.startPointXMin;
    this.xPosMin += ((moveDelta / this.slider.nativeElement.clientWidth) * 100);
    if (this.xPosMin < 0) {
      this.xPosMin = 0;
    } else if (this.xPosMin > this.xPosMax) {
      this.xPosMin = this.xPosMax;
    } else {
      // Prevent generating 'lag' if moving outside window
      this.startPointXMin = $event.pageX;
    }
    this.valueMin = Math.round((((this.max - this.min) * (this.xPosMin / 100)) + this.min) / this.step) * this.step;
    this.newValueMin.emit(this.valueMin);

    // Move the Handle
    this.moveHandle('minElem', this.xPosMin);
    this.moveRange(this.xPosMin, this.xPosMax);
  }
}
