import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FilterService } from '../../../core/services/filter/filter.service';
import { LanguageService } from '../../services/language/language.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Input() search: string;

  @Input() filter: FilterService;

  @Input() type = 'text';

  @Input() placeholder = 'Search...';
  searchBtn = 'Search';
  @Output()
  submitSearch: EventEmitter<string> = new EventEmitter<string>();

  constructor(private translate: TranslateService, private languageService: LanguageService) {

  }

  ngOnInit() {
    this.initLocale();
    if (this.filter) {
      this.search = this.filter.search;
    }
  }

  initLocale() {
    this.languageService.getLang().subscribe((lang) => {
      if (lang === 'en') {
        this.placeholder = 'Search...';
        this.searchBtn = 'Search';
      } else {
        this.placeholder = 'Поиск...';
        this.searchBtn = 'Поиск';
      }
    });
  }

  onSubmit() {
    const search = this.search === '' ? null : this.search;
    this.submitSearch.emit(search);
    if (this.filter) {
      this.filter.search = search;
    }
  }

}
