import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { SweetAlertService } from '../../services/sweet-alert/sweet-alert.service';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit, OnChanges {

  @Input() reset: string;
  @Input() additionalButton: boolean;
  @Input() fileSize: number;
  @Input() accept: any;
  @Output() value = new EventEmitter<string>();
  @Output() fullObject = new EventEmitter<any>();
  @ViewChild('input') private input: ElementRef;
  acceptString: any;

  constructor(private sweetAlertService: SweetAlertService) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.reset && !changes.reset.isFirstChange()) {
      this.resetInput(this.reset);
    }
    if (changes.accept) {
      this.acceptString = this.accept.join();
    }
  }

  ngOnInit() {
  }

  handleInputChange(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    if (!file) {
      return;
    }
    const pattern = /image-*/;
    const reader = new FileReader();
    if (file.size > this.fileSize) {
      reader.abort();
      this.sweetAlertService.error(`File size limit ${this.fileSize / 1024 / 1024} Mb`);
      return;
    } else if (this.accept && !this.accept.includes(file.type)) {
      reader.abort();
      this.sweetAlertService.error(`Invalid file format`);
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this, file.name, file.type);
    reader.readAsDataURL(file);

  }

  _handleReaderLoaded(name, type, e) {
    const reader = e.target;
    this.value.emit(reader.result);
    this.fullObject.emit({content: reader.result, name, type});
  }

  resetInput(value) {
    if (value === '') {
      this.input.nativeElement.value = '';
    }
  }
}
