import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Common } from '@app/core/models/common-model/common.model';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';
import { Statuses } from '../../interfaces/statuses';
import { modelId } from '@app/core/models/base-model/base.model';

@Component({
  selector: 'app-user-statuses',
  templateUrl: './user-statuses.component.html',
  styleUrls: ['./user-statuses.component.scss']
})

export class UserStatusesComponent implements OnInit, OnChanges, OnDestroy {
  @Input() statuses: Common.Status[];
  @Input() statusSelected: modelId;
  @Input() permission: string[];
  @Output() changeStatusEvent: EventEmitter<number> = new EventEmitter<number>();
  statusOriginal: string;
  statusName: string;
  private statuses$ = new BehaviorSubject([]);
  private statusSelected$ = new BehaviorSubject(Number());
  private destroy$: Subject<boolean> = new Subject<boolean>();

  ngOnInit(): void {
    combineLatest(this.statuses$, this.statusSelected$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(([statuses, statusSelected]) => {
        if (statuses.length && statusSelected >= 0) {
          this.getStatusInfo(statuses, statusSelected);
        }
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.statuses && changes.statuses.currentValue) {
      this.statuses$.next(changes.statuses.currentValue);
    }
    if (changes.statusSelected && changes.statusSelected.currentValue !== 'undefined') {
      this.statusSelected$.next(changes.statusSelected.currentValue);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  changeStatus(status: Statuses): void {
    this.changeStatusEvent.emit(status.id);
  }

  private getStatusInfo(statuses, id): void {
    statuses.forEach((status) => {
      if (id === status.id) {
        this.statusName = status.name;
        this.statusOriginal = status.original;
      }
    });
  }
}
