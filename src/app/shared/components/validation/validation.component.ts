import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { LanguageService } from '../../services/language/language.service';
import ValidationText from './validations';

@Component({
  selector: 'app-validation-message',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})

export class ValidationComponent {

  @Input() control: FormControl;
  @Input() isPopover: boolean;

  lang: string;

  constructor(private languageService: LanguageService) {
    this.languageService.getLang().subscribe((lang) => {
      this.lang = lang;
    });
  }

  validationErrors() {

    if (!this.control || !this.control.errors) {
      return null;
    }

    return Object.keys(this.control.errors).map((key) => {
      if (typeof this.control.errors[key] === 'object') {
        return this.parseObjectError(key, this.control.errors[key]);
      } else {
        return this.getError(key);
      }
    });
  }

  // {
  //   max: {
  //     actual: 123123
  //     max: 100
  //   }
  // }

  parseObjectError(errorType: string, error: any) {

    let errText: string = ValidationText[this.lang][errorType];

    if (errText) {
      errText = errText.replace(`{{${errorType}}}`, error[errorType]);
      return errText;
    } else {
      return errorType;
    }
  }

  getError(errCode: string) {

    if (!ValidationText[this.lang][errCode]) {
      return errCode;
    }

    return ValidationText[this.lang][errCode];
  }

}
