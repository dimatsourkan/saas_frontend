import { Injectable } from '@angular/core';
import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { parseValidationKeys } from '@app/core/helpers/helpers';
import forEach from 'lodash-es/forEach';

@Injectable()
export class ValidatorService {

  private addErrorToControl(control: AbstractControl, error) {
    if (control) {
      control.setErrors({server: error});
      control.markAsTouched({onlySelf: true});
    }
  }

  addErrorToForm(form: FormGroup, err: any) {

    if (!form.controls || !err.errors) {
      return;
    }

    form.updateValueAndValidity();

    if (typeof err.errors === 'string') {
      return this.addErrorToControl(form, err.errors);
    }

    forEach(err.errors, (error, key) => {

      const fields = parseValidationKeys(key);

      let control: any = form;

      forEach(fields, (field) => {
        if (control.controls[field]) {
          control = control.controls[field];
        }
      });

      this.addErrorToControl(control, error);

    });

  }

  setTouchToControls(form: FormGroup) {
    if (!form.controls) {
      return true;
    }
    form.updateValueAndValidity();
    form.markAsTouched({onlySelf: true});
    Object.keys(form.controls).forEach(key => {
      const control: AbstractControl = form.controls[key];

      if (control instanceof FormGroup) {
        this.setTouchToControls(control);
      } else if (control instanceof FormArray) {
        control.controls.forEach((c: FormGroup) => {
          this.setTouchToControls(c);
        });
      } else {
        form.controls[key].markAsTouched({onlySelf: true});
      }

    });
  }

}
