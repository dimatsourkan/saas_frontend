export default {

  en: {
    required: 'This value should not be blank',
    unique: 'This value should be unique',
    email: 'Invalid email address',
    phone: 'Invalid phone number',
    url: 'Url is invalid',
    max: 'Max value is {{max}}',
    min: 'Min value is {{min}}',
    maxlength: 'Value to long',
    compare: 'Passwords not match',
    numeric: 'This value must be a number.',
    pattern: 'Pattern is invalid.',
    urlNotValid: 'URL not valid.',
    latin: 'Format is not valid. Name must start and end with latin letter\n' +
      'or digit and can contain only latin letters, digits, spaces and symbols ._-\'."'
  },

  ru: {
    required: 'Значение не должно быть пустым.',
    unique: 'This value should be unique',
    email: 'Неправильный email',
    phone: 'Неправильный номер телефона',
    url: 'URL недействителен',
    max: 'Макс. значение должно быть: {{max}}',
    min: 'Мин. значение должно быть: {{min}}',
    maxlength: 'Значение слишком длинное',
    compare: 'Пароли не совпадают',
    numeric: 'Это значение должно быть числом.',
    pattern: 'Шаблон неверен.',
    urlNotValid: 'Неправильный URL',
    latin: 'Формат недействителен. Имя должно начинаться и заканчиваться латинской буквой\n' +
      'или цифрой и может содержать только латинские буквы, цифры, пробелы и символы ._-\'."'
  }


  // 'accepted' : 'You must accept ${name}.',
  // 'active_url' : 'The ${name} field contains an invalid URL.',
  // 'after' : 'The value of the ${name} field must be greater than ${date}.',
  // 'alpha' : 'The ${name} field can only contain letters.',
  // 'alpha_dash' : 'The ${name} field can only contain letters, numbers, and hyphen.',
  // 'alpha_num' : 'The ${name} field can only contain letters and numbers.',
  // 'array' : 'The ${name} field must be an array.',
  // 'before' : 'The ${name} field must be before ${date}.',
  // 'boolean' : 'The ${name} field must have a Boolean.',
  // 'confirmed' : 'The ${name} field does not match the confirmation.',
  // 'date' : 'The ${name} field is filled incorrect.',
  // 'date_format' : 'The ${name} field does not match the format of ${format}.',
  // 'different' : 'The fields ${name} and ${other} must be different.',
  // 'digits' : 'The length of the digital field ${name} must be ${digits}.',
  // 'digits_between' : 'The length of the digital field ${name} should be between ${min} and ${max}.',
  // 'dimensions' : 'The ${name} has invalid image dimensions.',
  // 'distinct' : 'The ${name} field contains a duplicate value.',
  // 'filled' : 'The ${name} field is mandatory for filling.',
  // 'exists' : 'The selected value for ${name} is incorrect.',
  // 'image' : 'The ${name} field must be an image.',
  // 'in' : 'The selected value for ${name} is not correct.',
  // 'in_array' : 'The ${name} field does not exist in: other.',
  // 'integer' : 'The ${name} field must be an integer.',
  // 'ip' : 'The ${name} field must be a valid IP address.',
  // 'json' : 'The ${name} field must be a JSON string.',
  // 'not_in' : 'The selected value for ${name} is not correct.',
  // 'present' : 'The ${name} field must be present.',
  // 'regex' : 'The ${name} field has an incorrect format.',
  // 'clean_tags_required' : 'The ${name} field is required.',
  // 'same' : 'The value of ${name} must match ${other}.'
};
