export const ExpireDays = [
  {name: '1 day', id: 1},
  {name: '2 days', id: 2},
  {name: '3 days', id: 3},
  {name: '4 days', id: 4},
  {name: '5 days', id: 5}
];
