export const URL_PATTERN = regexp();

function regexp() {
  return new RegExp([
    /^(?:http(s)?:\/\/)?[\w.-]+/,
    /(?:\.[\w\.-]+)+/,
    /[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=]+/,
    /([\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=]+({{1}\w+}{1}))*$/
  ].map(function (r) {
    return r.source;
  }).join(''));
}
