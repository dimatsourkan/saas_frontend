import { Directive, ElementRef, forwardRef, HostListener, Renderer2 } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
  selector: '[appCheckboxState]',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxStateDirective),
      multi: true
    }
  ]
})
export class CheckboxStateDirective implements ControlValueAccessor {
  private readonly parent: ElementRef;
  private value: number;
  private propagateChange: any = () => {
  }

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
    this.parent = renderer.parentNode(renderer.parentNode(elementRef.nativeElement));
  }

  @HostListener('change', ['$event.target'])
  onHostChange() {
    switch (this.value) {
      case 1:
        this.value = -1;
        break;
      case -1:
        this.value = 0;
        break;
      default:
        this.value = 1;
    }
    this.propagateChange(this.value);
    this.setCheckboxStatus(this.value);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value: any): void {
    this.value = value;
    this.setCheckboxStatus(value);
  }

  private setCheckboxStatus(value) {
    switch (value) {
      case -1:
        this.renderer.setProperty(this.elementRef.nativeElement, 'indeterminate', 1);
        this.renderer.setProperty(this.elementRef.nativeElement, 'checked', -1);
        this.renderer.removeClass(this.parent, 'inp-check_unknown');
        this.renderer.removeClass(this.parent, 'inp-check_checked');
        this.renderer.addClass(this.parent, 'inp-check_disable');
        break;
      case 1:
        this.renderer.setProperty(this.elementRef.nativeElement, 'indeterminate', 1);
        this.renderer.setProperty(this.elementRef.nativeElement, 'checked', 1);
        this.renderer.removeClass(this.parent, 'inp-check_unknown');
        this.renderer.removeClass(this.parent, 'inp-check_disable');
        this.renderer.addClass(this.parent, 'inp-check_checked');
        break;
      default:
        this.renderer.setProperty(this.elementRef.nativeElement, 'indeterminate', -1);
        this.renderer.removeClass(this.parent, 'inp-check_checked');
        this.renderer.removeClass(this.parent, 'inp-check_disable');
        this.renderer.addClass(this.parent, 'inp-check_unknown');
    }
  }

}
