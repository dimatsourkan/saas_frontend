import { Directive } from '@angular/core';
import { ClipboardService } from '../services/clipboard/clipboard.service';
import { SweetAlertService } from '../services/sweet-alert/sweet-alert.service';

@Directive({
  selector: '[appCopyToClipboard]',
  inputs: ['value: appCopyToClipboard'],
  host: {
    '(click)': 'copyToClipboard()'
  }
})
export class CopyToClipboardDirective {
  public value: string;


  constructor(private clipboardService: ClipboardService, private swal: SweetAlertService) {
  }

  public copyToClipboard() {
    if (!this.value) {
      this.swal.error('Nothing to Copy');
      return false;
    }
    this.clipboardService
      .copy(this.value)
      .then((): void => {
          this.swal.success('Copied');
        }
      )
      .catch(
        (error: Error): void => {
          this.swal.error('Failed to Copy');
        }
      )
    ;

  }
}
