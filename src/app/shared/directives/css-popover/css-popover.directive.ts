import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appCssPopover]'
})
export class CssPopoverDirective {
  elContent: any;
  width: number;
  height: number;
  elScrollableParent: any;
  container: { top: any; left: any; right: any; };
  popover: any = {};
  el: any;

  @Input() position: 'right' | 'left' | 'bottom' | 'top' = null;

  @HostListener('mouseenter') onMouseEnter() {
    this.onMouseover();
  }

  constructor(private element: ElementRef,
              private renderer: Renderer2) {

  }


  setPosition(position) {
    this.el.setAttribute('data-placement', position);
  }

  calculatePosition() {
    let position = '';
    if (this.popover.left < this.width / 2) {
      position = 'right';
    } else if (this.popover.right < this.width / 2) {
      position = 'left';
    } else if (this.height >= this.popover.top) {
      position = 'bottom';
    } else {
      position = 'top';
    }
    this.setPosition(this.position || position);
  }

  onMouseover() {

    this.el = this.element.nativeElement;
    this.elContent = this.el.querySelector('.css-popover-content');
    this.elScrollableParent = this.el.closest('[perfectScrollbar]');

    this.container = {
      top: this.elScrollableParent.getBoundingClientRect().top,
      left: this.elScrollableParent.getBoundingClientRect().left,
      right: this.elScrollableParent.getBoundingClientRect().right
    };

    this.popover = {
      top: this.el.getBoundingClientRect().top - this.container.top,
      left: this.el.getBoundingClientRect().left - this.container.left,
      right: this.container.right - this.el.getBoundingClientRect().right
    };

    if (this.elContent) {
      this.width = this.elContent.clientWidth;
      this.height = this.elContent.clientHeight + 10;
    }
    if (this.elContent.clientWidth > 400) {
      this.renderer.setStyle(this.elContent, 'width', '400px');
      this.renderer.setStyle(this.elContent, 'white-space', 'normal');
    }
    if (this.container) {
      this.container.top = this.elScrollableParent.getBoundingClientRect().top;
      this.popover.top = this.el.getBoundingClientRect().top - this.container.top;
      this.popover.left = this.el.getBoundingClientRect().left - this.container.left;
      this.popover.right = this.container.right - this.el.getBoundingClientRect().right;
    }
    this.calculatePosition();
  }


}
