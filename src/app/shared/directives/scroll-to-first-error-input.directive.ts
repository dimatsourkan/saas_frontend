import {Directive, ElementRef, Input, OnDestroy, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {debounceTime, filter} from 'rxjs/operators';
import {Subscription} from 'rxjs';

@Directive({
  selector: '[appScrollToFirstErrorInput]'
})
export class ScrollToFirstErrorInputDirective implements OnInit, OnDestroy {
  @Input() appScrollToFirstErrorInput: any;
  @Input() formGroup: FormGroup;

  subscription$: Subscription;
  preventStatus = null;

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    this.subscription$ = this.formGroup.statusChanges
      .pipe(
        debounceTime(100),
        filter(() => this.preventStatus !== this.formGroup.status),
        filter(() => this.formGroup.touched)
      )
      .subscribe(() => {
        this.preventStatus = this.formGroup.status;
        this.scrollToElement(this.getInvalidInput());
      });
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  getInvalidInput() {
    const wrapper = this.el.nativeElement.querySelectorAll('.createOffer__input_invalid');
    const element = this.el.nativeElement.querySelectorAll('.createOffer__input_invalid input');

    if (wrapper.length && element.length) {
      return {wrapper: wrapper[0], element: element[0]};
    }

    const wrapper2 = this.el.nativeElement.querySelectorAll('.form-group input.ng-touched.ng-invalid');

    return {wrapper: wrapper2[0], element: wrapper2[0]};
  }

  scrollToElement({wrapper, element}) {
    if (wrapper) {
      wrapper.scrollIntoView({block: 'center'});
    }

    if (element) {
      element.focus();
    }
  }

}
