import { Directive, HostListener, Input } from '@angular/core';
import { FilterService } from '@app/core/services/filter/filter.service';

@Directive({
  selector: '[appSortBy]'
})
export class AppSortByDirective {

  @Input() private appSortBy: string;
  @Input() private filter: FilterService;

  @HostListener('click', ['$event.target'])
  onHostClick() {
    this.filter.sort = this.appSortBy;
    this.filter.page = 1;
  }

}
