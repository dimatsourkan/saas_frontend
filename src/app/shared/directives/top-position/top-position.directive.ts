import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appTopPosition]'
})
export class TopPositionDirective {

  constructor(private el: ElementRef) {
    const element = el.nativeElement;
    const scrollContainer = this.findAncestor(element, 'scrollable');
    const containerClientHeight = scrollContainer.clientHeight;
    const elementOffset = element.getBoundingClientRect().top - scrollContainer.getBoundingClientRect().top;
    let scrollTop = scrollContainer.scrollTop;
    element.addEventListener('mouseenter', calculateElementPosition);

    function calculateElementPosition() {
      scrollTop = scrollContainer.scrollTop;
      if (elementOffset < scrollTop + containerClientHeight / 2) {
        // scope.menuReverse = false;
        el.nativeElement.setAttribute('menuReverse', false);
      } else {
        // scope.menuReverse = true;
        el.nativeElement.setAttribute('menuReverse', true);
      }
    }
  }

  findAncestor(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls)) {
      return el;
    }
  }

}
