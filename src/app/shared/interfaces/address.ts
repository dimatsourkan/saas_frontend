export interface Address {
  address: string;
  city: string;
  state: string;
  countryCode: string;
  zipcode: string;
}
