export interface DefaultList {
  id: number | string;
  name: string;
  origin?: string;
}

export interface DefaultValueList {
  id: number | string;
  value: string;
  origin: string;
}
