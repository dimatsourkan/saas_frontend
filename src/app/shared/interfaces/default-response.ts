export interface DefaultResponse<T> {
  data: Array<any>;
  items: Array<T>;
  limit: number;
  page: number;
  totalItems: number;
  totalPages: number;
}
