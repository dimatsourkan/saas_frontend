import { DefaultList } from './default-list';

export interface Platforms extends DefaultList {
  selected?: boolean;
  shown?: boolean;
  version?: string;
}
