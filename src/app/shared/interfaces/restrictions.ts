export interface Restrictions {
  id: number | string;
  name: string;
  state?: number;
}
