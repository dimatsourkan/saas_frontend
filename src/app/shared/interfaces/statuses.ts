export interface Statuses {
  id: number;
  name: string;
  original: string;
}
