export interface Tags {
  id: number;
  name: string;
  manageable_item_value: number;
}
