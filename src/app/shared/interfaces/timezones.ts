export interface Timezones {
  timezone: string;
  identifier: string;
}
