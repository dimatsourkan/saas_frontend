import { Pipe, PipeTransform } from '@angular/core';
import { BillingAdvertisersService } from '@app/pages/manager/billing/billing-advertisers/billing-advertisers.service';

@Pipe({
  name: 'advertiserActionName'
})
export class AdvertiserActionNamePipe implements PipeTransform {

  actions$: any;

  constructor(private billingAdvertisersService: BillingAdvertisersService) {
    billingAdvertisersService.getAdvertiserActions().subscribe((value: any[]) => {
      this.actions$ = value;
    });
  }

  transform(value: number): string {
    if (!value || !this.actions$) {
      return '';
    }
    return this.actions$.items.find(el => el.id === value).name;
  }

}
