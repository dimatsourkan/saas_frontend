import { Pipe, PipeTransform } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConversionReportService } from '@app/core/entities/reports/conversion-reports/conversion-reports.service';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';


@Pipe({
  name: 'approveStatus'
})
export class ApproveStatusPipe implements PipeTransform {

  private approveStatuses$ = new BehaviorSubject(new ResultList<Common.Status>());

  constructor(private conversionReportService: ConversionReportService) {
    if (!this.approveStatuses$.getValue().items.length) {
      this.conversionReportService.getApproveStatuses().subscribe(res => {
        this.approveStatuses$.next(res);
      });
    }
  }

  getStatus(code: string, arg: boolean) {
    return this.approveStatuses$
      .pipe(
        map((statuses) => {
          const status = statuses.items.find(item => item.id === code);
          if (!status) {
            return status;
          }
          return (!arg) ? status.value : status.value.toLowerCase();
        })
      );
  }

  transform(value: string, arg?: boolean): Observable<any> {
    if (!value) {
      return null;
    }
    return this.getStatus(value, arg);
  }




}
