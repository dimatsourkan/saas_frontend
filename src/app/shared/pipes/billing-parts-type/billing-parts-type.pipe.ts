import { Pipe, PipeTransform } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';
import { map } from 'rxjs/operators';
import { PubBillingService } from '@app/core/entities/pub-billing/pub-billing.service';

@Pipe({
  name: 'billingPartsType'
})
export class BillingPartsTypePipe implements PipeTransform {

  private list$ = new BehaviorSubject(new ResultList<Common.Status>());

  constructor(private service: PubBillingService) {
    if (!this.list$.getValue().items.length) {
      this.service.getPartType().subscribe(res => {
        this.list$.next(res);
      });
    }
  }

  getValue(code: string | number, args: 'name' | 'operator' = 'name') {
    return this.list$
      .pipe(
        map((list) => {
          const value = list.items.find(item => item.id === code);
          if (!value) {
            return '';
          }
          if (args === 'name') {
            return value ? value.name : code;
          } else {
            return (Number(value.id) !== 3) ? '-' : '+';
          }
        })
      );
  }

  transform(value: string | number, args?: 'name' | 'operator'): Observable<any> {
    if (!value) {
      return null;
    }
    return this.getValue(value, args);
  }

}
