import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'checkerDate'
})
export class CheckerDatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return moment(value, 'YYYY-MM-DD').format('MM/DD/YYYY');
  }

}
