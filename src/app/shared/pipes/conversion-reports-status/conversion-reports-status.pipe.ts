import { Pipe, PipeTransform } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConversionReportService } from '@app/core/entities/reports/conversion-reports/conversion-reports.service';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';

@Pipe({
  name: 'conversionReportsStatus'
})
export class ConversionReportsStatusPipe implements PipeTransform {

  private statuses$ = new BehaviorSubject(new ResultList<Common.Status>());

  constructor(private conversionReportService: ConversionReportService) {
    if (!this.statuses$.getValue().items.length) {
      this.conversionReportService.getStatuses().subscribe(res => {
        this.statuses$.next(res);
      });
    }
  }

  getStatus(code: string) {
    return this.statuses$
    .pipe(
      map((list) => {
        const status = list.items.find(item => item.id === code);
        return status ? status.value : code;
      })
    );
  }

  transform(value: string): Observable<any> {
    if (!value) {
      return null;
    }
    return this.getStatus(value);
  }

}
