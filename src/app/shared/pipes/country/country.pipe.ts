import { Pipe, PipeTransform } from '@angular/core';
import { CountryService } from '@app/core/entities/country/country.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { ResultList } from '@app/core/models/result-model/result.model';
import { map } from 'rxjs/operators';
import { Country } from '@app/core/entities/country/country.model';


@Pipe({
  name: 'country'
})
export class CountryPipe implements PipeTransform {

  private countries$ = new BehaviorSubject(new ResultList<Country>());

  constructor(private countryService: CountryService) {
    if (!this.countries$.getValue().items.length) {
      this.countryService.getKeyList().subscribe(res => {
        this.countries$.next(res);
      });
    }
  }

  getObject(code: string | number) {
    return this.countries$
      .pipe(
        map((list) => {
          const obj = list.items.find(item => item.code === code);
          return (obj) ? obj.name : '';
        })
      );
  }

  transform(value: number): Observable<string | number | any> {
    if (!value) {
      return null;
    }
    return this.getObject(value);
  }
}
