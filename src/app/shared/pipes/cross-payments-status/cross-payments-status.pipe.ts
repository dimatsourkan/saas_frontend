import { Pipe, PipeTransform } from '@angular/core';
import { CrossPaymentsService } from '@app/pages/manager/billing/billing-cross-payments/cross-payments.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'crossPaymentsStatus'
})
export class CrossPaymentsStatusPipe implements PipeTransform {

  private statuses$: Observable<any> = this._crossPaymentsService.statuses$;

  constructor(
    private _crossPaymentsService: CrossPaymentsService
  ) {
  }

  getAdvertiserFromList(code: string) {
    return this.statuses$
      .pipe(
        map((statuses) => {
          const status = statuses.find(item => item.id === code);
          return status.name || code;
        })
      );
  }

  transform(value: any, args?: any): Observable<any> {
    if (!value) {
      return null;
    }

    return this.getAdvertiserFromList(value);
  }

}
