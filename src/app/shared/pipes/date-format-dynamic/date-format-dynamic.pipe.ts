import { Pipe, PipeTransform } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import * as moment from 'moment-timezone';
import { LanguageService } from '../../services/language/language.service';

@Pipe({
  name: 'dateFormatDynamic',
  pure: false
})
export class DateFormatDynamicPipe implements PipeTransform {

  format: string;

  constructor(
    private languageService: LanguageService
  ) {
    this.languageService.getLang().subscribe(() => {
      this.format = moment.localeData().longDateFormat('L');
    });
  }

  transform(value: string, withTime?: boolean, timezone?: string): string {
    const timeZone = timezone || DataStore.timezone.current.getValue();
    if (!value || !timeZone) {
      return '';
    }

    let format = this.format;
    if (withTime) {
      format += ' HH:mm:ss';
    }
    if (typeof timezone === 'boolean') {
      return moment.utc(value).format(format);
    }
    return moment.utc(value).tz(timeZone).format(format);
  }

}
