import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';
import { LanguageService } from '../../services/language/language.service';

@Pipe({
  name: 'dateFormat',
  pure: false
})
export class DateFormatPipe implements PipeTransform {

  timezone$: BehaviorSubject<any>;
  format: string;

  constructor(
    private languageService: LanguageService
  ) {
    this.languageService.getLang().subscribe(() => {
      this.format = moment.localeData().longDateFormat('L');
    });
  }

  transform(value: any, withTime?: boolean, utc?: boolean, customFormat?: string): any {
    if (!value) {
      return '';
    }

    let format = this.format;

    if (withTime) {
      format += ' HH:mm:ss';
    }
    if (customFormat) {
      format = customFormat;
    }

    if (utc) {
      return moment(value).utc().format(format);
    } else {
      return moment(value).format(format);
    }
  }

}
