import { Pipe, PipeTransform } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import * as moment from 'moment-timezone';
import { LanguageService } from '../services/language/language.service';

@Pipe({
  name: 'dateWithTime',
  pure: false
})
export class DateWithTimePipe implements PipeTransform {

  format: string;

  constructor(
    private languageService: LanguageService
  ) {
    this.languageService.getLang().subscribe(() => {
      this.format = moment.localeData().longDateFormat('L');
    });
  }

  transform(value: string, timezone?: string): string {
    if (!value) {
      return '';
    }
    const timeZone = timezone || DataStore.timezone.current.getValue();
    const format = this.format + ' HH:mm:ss';

    return moment.utc(value).tz(timeZone).format(format);
  }
}
