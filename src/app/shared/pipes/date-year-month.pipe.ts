import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateYearMonth'
})
export class DateYearMonthPipe implements PipeTransform {

  transform(date: string): string {
    return moment(date).format('MMMM YYYY');
  }

}
