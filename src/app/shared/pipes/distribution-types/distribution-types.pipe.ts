import { Pipe, PipeTransform } from '@angular/core';
import { DistributionTypeService } from '../../../pages/manager/offers/shared/service/distribution-type/distribution-type.service';
import { DefaultValueList } from '../../interfaces/default-list';

@Pipe({
  name: 'distributionTypes'
})
export class DistributionTypesPipe implements PipeTransform {
  types$: Array<DefaultValueList> = [];

  constructor(private distributionTypesService: DistributionTypeService) {
    distributionTypesService.getDistributionTypes().subscribe((value: DefaultValueList[]) => {
      this.types$ = value;
    });
  }

  transform(value: number): string {
    if (!value || !this.types$.length) {
      return '';
    }
    return this.types$.find(el => el.id === value).value;
  }
}
