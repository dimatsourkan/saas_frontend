import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getParamFromLink'
})
export class GetParamFromLinkPipe implements PipeTransform {


  getParamFromLink(link: string, param: string) {
    if (link) {
      const parsedLink = link.split('?');
      const params = this.getParams(parsedLink[1]);
      if (Object.prototype.hasOwnProperty.call(params, param)) {
        return params[param];
      }
    }
    return null;
  }

  getParams(queryParams: string) {
    if (!queryParams) {
      return {};
    }
    return (/^[?]/.test(queryParams) ? queryParams.slice(1) : queryParams).split('&').reduce((params, param) => {
      const [key, value] = param.split('=');
      params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
      return params;
    }, {});
  }

  transform(value: string, args?: string): string {
    return this.getParamFromLink(value, args);
  }

}
