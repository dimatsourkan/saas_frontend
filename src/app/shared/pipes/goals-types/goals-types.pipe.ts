import { Pipe, PipeTransform } from '@angular/core';
import { DefaultList } from '../../interfaces/default-list';
import { GoalsService } from '../../services/goals/goals.service';

@Pipe({
  name: 'goalsTypes'
})
export class GoalsTypesPipe implements PipeTransform {
  goalsTypes$: DefaultList[];

  constructor(private goalsService: GoalsService) {
    goalsService.getGoalsType().subscribe((value: DefaultList[]) => {
      this.goalsTypes$ = value;
    });
  }

  transform(value: number): string {
    if (!value) {
      return '';
    }
    return this.goalsTypes$.find(el => el.id === value).name;
  }
}
