import { Pipe, PipeTransform } from '@angular/core';
import { DefaultList } from '../../interfaces/default-list';
import { GoalsService } from '../../services/goals/goals.service';

@Pipe({
  name: 'goals'
})
export class GoalsPipe implements PipeTransform {
  goals$: DefaultList[];

  constructor(private goalsService: GoalsService) {
    goalsService.getGoals().subscribe((value: DefaultList[]) => {
      this.goals$ = value;
    });
  }

  transform(value: number): string {
    if (!value) {
      return '';
    }
    return this.goals$.find(el => el.id === value).name;
  }
}
