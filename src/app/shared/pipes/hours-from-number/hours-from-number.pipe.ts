import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';

@Pipe({
  name: 'hoursFromNumber'
})
export class HoursFromNumberPipe implements PipeTransform {

  transform(hour: any, timezone?: string): any {

    if (hour === undefined || hour === null) {
      return '';
    }
    return moment(hour, 'HH').format('HH');
  }

}
