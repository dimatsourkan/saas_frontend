import { Pipe, PipeTransform } from '@angular/core';
import { BillingPublishersService } from '../../../pages/manager/billing/billing-publishers/billing-publishers.service';

@Pipe({
  name: 'invoicesStatus'
})
export class InvoicesStatusPipe implements PipeTransform {
  statuses$: any[];

  constructor(private _billingPublishersService: BillingPublishersService) {
    _billingPublishersService.getStatutes().subscribe((value: any[]) => {
      this.statuses$ = value;
    });
  }

  transform(value: any): any {
    if (!value) {
      return '';
    }
    return this.statuses$.find(el => el.id === value).name;
  }
z
}
