import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limitTo'
})
export class LimitToPipe implements PipeTransform {

  transform(value: Array<any>, args: number): Array<any> {
    if (value && !Array.isArray(value)) {
      return [];
    }
    return value.filter((i, index) => (index < args));
  }

}
