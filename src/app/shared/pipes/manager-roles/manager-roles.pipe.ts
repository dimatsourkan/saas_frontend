import { Pipe, PipeTransform } from '@angular/core';
import { ManagerService } from '@app/core/entities/manager/manager.service';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'managerRole'
})
export class ManagerRolesPipe implements PipeTransform {

  private managersRoles$ = new BehaviorSubject(new ResultList<Common.Role>());

  constructor(private managerService: ManagerService) {
    if (!this.managersRoles$.getValue().items.length) {
      this.managerService.getManagersRoles().subscribe(res => {
        this.managersRoles$.next(res);
      });
    }
  }

  getRole(code: string|number) {
    return this.managersRoles$
      .pipe(
        map((list) => {
          const role = list.items.find(item => item.id === code);
          return role ? role.name : code;
        })
      );
  }

  transform(value: string|number): Observable<any> {
    if (!value) {
      return null;
    }
    return this.getRole(value);
  }
}
