import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'monthsFromNumber'
})
export class MonthsFromNumberPipe implements PipeTransform {

  transform(month: any, args?: any): any {
    return moment(month, 'M').format('MMMM');
  }

}
