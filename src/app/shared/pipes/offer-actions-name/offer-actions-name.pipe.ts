import { Pipe, PipeTransform } from '@angular/core';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OfferLoggerService } from '@app/core/entities/offer-logger/offer-logger.service';

@Pipe({
  name: 'offerActionsName'
})
export class OfferActionsNamePipe implements PipeTransform {
  private actions$ = new BehaviorSubject(new ResultList<Common.Status>());

  constructor(private service: OfferLoggerService) {
    if (!this.actions$.getValue().items.length) {
      this.service.getActionsCache().subscribe(res => {
        this.actions$.next(res);
      });
    }
  }

  getStatus(code: string | number) {
    return this.actions$
      .pipe(
        map((list) => {
          const role = list.items.find(item => Number(item.id) === Number(code));
          return (role) ? role.name : '';
        })
      );
  }

  transform(value: number): Observable<string | number | any> {
    if (!value) {
      return null;
    }
    return this.getStatus(value);
  }
}
