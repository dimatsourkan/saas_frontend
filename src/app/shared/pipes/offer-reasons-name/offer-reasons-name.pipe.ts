import { Pipe, PipeTransform } from '@angular/core';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OfferLoggerService } from '@app/core/entities/offer-logger/offer-logger.service';

@Pipe({
  name: 'offerReasonsName'
})
export class OfferReasonsNamePipe implements PipeTransform {
  private reasons$ = new BehaviorSubject(new ResultList<Common.Status>());

  constructor(private service: OfferLoggerService) {
    if (!this.reasons$.getValue().items.length) {
      this.service.getReasonsCache().subscribe(res => {
        this.reasons$.next(res);
      });
    }
  }

  getStatus(code: string | number) {
    return this.reasons$
      .pipe(
        map((list) => {
          const role = list.items.find(item => Number(item.id) === Number(code));
          return (role) ? role.name : '';
        })
      );
  }

  transform(value: number): Observable<string | number | any> {
    if (!value) {
      return null;
    }
    return this.getStatus(value);
  }
}
