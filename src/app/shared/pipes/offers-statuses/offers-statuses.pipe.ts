import { Pipe, PipeTransform } from '@angular/core';
import { Common } from '@app/core/models/common-model/common.model';
import { ResultList } from '@app/core/models/result-model/result.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OfferService } from '@app/core/entities/offer/offer.service';

@Pipe({
  name: 'offersStatuses'
})
export class OffersStatusesPipe implements PipeTransform {
  private statuses$ = new BehaviorSubject(new ResultList<Common.Status>());

  constructor(private service: OfferService) {
    if (!this.statuses$.getValue().items.length) {
      this.service.getStatusesCache().subscribe(res => {
        this.statuses$.next(res);
      });
    }
  }

  getStatus(code: string | number, prop: 'name' | 'origin' = 'name') {
    return this.statuses$
      .pipe(
        map((list) => {
          const role = list.items.find(item => item.id === code);
          return (role) ? role[prop] : '';
        })
      );
  }

  transform(value: number, args: 'name' | 'origin'): Observable<string | number | any> {
    if (!value) {
      return null;
    }
    return this.getStatus(value, args);
  }
}
