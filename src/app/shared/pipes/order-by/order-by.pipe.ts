import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(value: any[], property: any, descending?: boolean): any {
    if (!value || value.length === 0) {
      return value;
    }

    const fields = property.split('.');
    if (fields.length > 1) {
      value.sort((first: any, second: any): number => {
        return first[fields[0]][fields[1]] > second[fields[0]][fields[1]] ? 1 : -1;
      });
    } else {
      value.sort((first: any, second: any): number => {
        return first[property] > second[property] ? 1 : -1;
      });
    }

    if (descending) {
      return value.reverse();
    }

    return value;
  }

}
