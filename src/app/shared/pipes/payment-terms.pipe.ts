import { Pipe, PipeTransform } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';
import { map } from 'rxjs/operators';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';

@Pipe({
  name: 'paymentTerms'
})
export class PaymentTermsPipe implements PipeTransform {

  private paymentTerms$ = new BehaviorSubject(new ResultList<Common.Role>());

  constructor(private service: PublisherService) {
    if (!this.paymentTerms$.getValue().items.length) {
      this.service.getPaymentTerms().subscribe(res => {
        this.paymentTerms$.next(res);
      });
    }
  }

  getValue(code: string|number) {
    return this.paymentTerms$
      .pipe(
        map((list) => {
          const obj = list.items.find(item => item.id === code);
          return obj ? obj.name : code;
        })
      );
  }

  transform(value: string|number): Observable<any> {
    if (!value) {
      return null;
    }
    return this.getValue(value);
  }

}
