import { Pipe, PipeTransform } from '@angular/core';
import { DefaultList } from '../../interfaces/default-list';
import { PlatformsService } from '../../services/platforms/platforms.service';

@Pipe({
  name: 'platforms'
})
export class PlatformsPipe implements PipeTransform {
  platforms$: DefaultList[] = [];

  constructor(private platformsService: PlatformsService) {
    platformsService.getPlatforms().subscribe((value: any) => {
      this.platforms$ = value.items;
    });
  }

  transform(value: number, args?: string): string {
    if (!value || this.platforms$.length === 0) {
      return '';
    }
    const platform = this.platforms$.find(el => el.id === value).name;
    if (args === 'class') {
      return platform.toLowerCase();
    } else {
      return platform;
    }
  }

}
