import { Pipe, PipeTransform } from '@angular/core';
import { BillingPublishersService } from '@app/pages/manager/billing/billing-publishers/billing-publishers.service';

@Pipe({
  name: 'publisherActionName'
})
export class PublisherActionNamePipe implements PipeTransform {

  actions$: any;

  constructor(private _billingPublishersService: BillingPublishersService) {
    _billingPublishersService.getActions().subscribe((value: any[]) => {
      this.actions$ = value;
    });
  }

  transform(value: number): string {
    if (!value || !this.actions$) {
      return '';
    }
    return this.actions$.items.find(el => el.id === value).name;
  }

}
