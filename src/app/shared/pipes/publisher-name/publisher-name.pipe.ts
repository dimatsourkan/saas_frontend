import { HttpParams } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { Publisher } from '@app/core/entities/publisher/publisher.model';
import { PublisherService } from '@app/core/entities/publisher/publisher.service';
import { ResultList } from '@app/core/models/result-model/result.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'publisherName'
})
export class PublisherNamePipe implements PipeTransform {

  private publishers$ = new BehaviorSubject(new ResultList<Publisher>());

  constructor(private publisherService: PublisherService) {
    if (!this.publishers$.getValue().items.length) {
      this.publisherService.getKeyList(new HttpParams({fromObject: {limit: '100'}})).subscribe(res => {
        this.publishers$.next(res);
      });
    }
  }

  getUserFromList(code: string) {
    return this.publishers$
      .pipe(
        map((list) => {
          const user = list.items.find(item => item.id === code);
          return user ? user.username : code;
        })
      );
  }

  transform(value: string): Observable<any> {
    if (!value) {
      return null;
    }

    return this.getUserFromList(value);
  }

}
