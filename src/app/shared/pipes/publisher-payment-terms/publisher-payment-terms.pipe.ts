import { Pipe, PipeTransform } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';
import { PubBillingService } from '@app/core/entities/pub-billing/pub-billing.service';
import { map } from 'rxjs/operators';


@Pipe({
  name: 'publisherPaymentTerms'
})
export class PublisherPaymentTermsPipe implements PipeTransform {


  private statuses$ = new BehaviorSubject(new ResultList<Common.Status>());

  constructor(private service: PubBillingService) {
    if (!this.statuses$.getValue().items.length) {
      this.service.getPaymentTerms().subscribe(res => {
        this.statuses$.next(res);
      });
    }
  }

  getStatus(code: string | number) {
    return this.statuses$
      .pipe(
        map((list) => {
          const role = list.items.find(item => item.id === code);
          return role['name'] || '';
        })
      );
  }

  transform(value: number): Observable<string | number | any> {
    if (!value) {
      return null;
    }
    return this.getStatus(value);
  }


}
