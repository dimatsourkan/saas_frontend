import { Pipe, PipeTransform } from '@angular/core';
import { ScrubsService } from '@app/pages/manager/billing/billing-publishers/scrubs.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'scrubActionName'
})
export class ScrubActionNamePipe implements PipeTransform {

  constructor(private _scrubsService: ScrubsService) {

  }

  getActionName(code: string) {
    return this._scrubsService.actionNames$
      .pipe(
        map((list: any) => {
          const action = list.find(item => item.id === code);
          return action.name || code;
        })
      );
  }

  transform(value: string): Observable<any> {
    if (!value) {
      return null;
    }

    return this.getActionName(value);
  }

}
