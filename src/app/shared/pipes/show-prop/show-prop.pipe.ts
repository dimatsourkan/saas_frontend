import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showProp'
})
export class ShowPropPipe implements PipeTransform {

  transform(array: any[], prop: string): any {
    return array.map(item => item[prop]);
  }

}
