import { Pipe, PipeTransform } from '@angular/core';
import { DefaultList } from '../../interfaces/default-list';
import { TagsService } from '../../services/tags/tags.service';

@Pipe({
  name: 'tags'
})
export class TagsPipe implements PipeTransform {
  tags$: DefaultList[];

  constructor(private tagsService: TagsService) {
    tagsService.getTags().subscribe((value: DefaultList[]) => this.tags$ = value);
  }

  transform(value: number, args: 'name' | 'origin' = 'name'): string {
    if (!value) {
      return 'No such category';
    }
    return this.tags$.find(el => el.id === value)[args];
  }
}
