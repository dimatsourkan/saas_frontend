import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'timeFromNow'
})
export class TimeFromNowPipe implements PipeTransform {
  transform(value: string): string {
    if (!value) {
      return '';
    }
    const hoursDiff = moment().startOf('hour').diff(value, 'hours');
    return Math.floor(hoursDiff / 24) + 'D ' + hoursDiff % 24 + 'H';
  }
}
