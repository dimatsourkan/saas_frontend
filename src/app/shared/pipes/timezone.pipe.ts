import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import * as moment from 'moment-timezone';

@Pipe({
  name: 'timezone'
})
export class TimezonePipe extends DatePipe implements PipeTransform {

  transform(value: string, format: string = 'MM/dd/yyyy HH:mm:ss', timezone?: string, locale?: string): string {
    const timeZone = timezone || DataStore.timezone.current.getValue();
    const date: any = moment.utc(value).tz(timeZone);
    if (!value || !date) {
      return '';
    }
    return super.transform(date.format(), format, date.format('Z'), locale);
  }
}
