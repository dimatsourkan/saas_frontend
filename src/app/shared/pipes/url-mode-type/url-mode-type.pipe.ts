import { Pipe, PipeTransform } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';
import { map } from 'rxjs/operators';
import { GroupUrlService } from '@app/core/entities/superlink/group-urls/group-urls.service';

@Pipe({
  name: 'urlModeType'
})
export class UrlModeTypePipe implements PipeTransform {

  private list$ = new BehaviorSubject(new ResultList<Common.Status>());

  constructor(private service: GroupUrlService) {
    if (!this.list$.getValue().items.length) {
      this.service.getModeList().subscribe(res => {
        this.list$.next(res);
      });
    }
  }

  getValue(code: string) {
    return this.list$
      .pipe(
        map((list) => {
          const status = list.items.find(item => item.id === code);
          return status ? status.name : code;
        })
      );
  }

  transform(value: string): Observable<any> {
    if (!value) {
      return null;
    }
    return this.getValue(value);
  }

}
