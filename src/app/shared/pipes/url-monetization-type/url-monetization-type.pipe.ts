import { Pipe, PipeTransform } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ResultList } from '@app/core/models/result-model/result.model';
import { Common } from '@app/core/models/common-model/common.model';
import { AlgorithmUrlService } from '@app/core/entities/superlink/algorithms-urls/algorithms-urls.service';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'urlMonetizationType'
})
export class UrlMonetizationTypePipe implements PipeTransform {

  private list$ = new BehaviorSubject(new ResultList<Common.Status>());

  constructor(private service: AlgorithmUrlService) {
    if (!this.list$.getValue().items.length) {
      this.service.getMonetizationList().subscribe(res => {
        this.list$.next(res);
      });
    }
  }

  getValue(code: string|number) {
    return this.list$
      .pipe(
        map((list) => {
          const status = list.items.find(item => item.id === code);
          return status ? status.name : code;
        })
      );
  }

  transform(value: string|number): Observable<any> {
    if (!value) {
      return null;
    }
    return this.getValue(value);
  }

}
