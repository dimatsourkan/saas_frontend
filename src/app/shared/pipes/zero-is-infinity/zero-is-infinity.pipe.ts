import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'zeroIsInfinity'
})
export class ZeroIsInfinityPipe implements PipeTransform {

  transform(value: number): string | number {
    return value === 0 ? '∞' : value;
  }

}
