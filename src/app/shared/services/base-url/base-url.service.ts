import { Injectable } from '@angular/core';
import { ApiUrlService } from '@app/core/services/api-url/api-url.service';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseUrlService {

  constructor(private apiUrlService: ApiUrlService) {
  }

  getBaseUrlWithRole(): string {
    return `${environment.baseURL}/${this.apiUrlService.linkPathFromRole()}`;
  }

  getBaseUrl(): string {
    return `${environment.baseURL}`;
  }

}
