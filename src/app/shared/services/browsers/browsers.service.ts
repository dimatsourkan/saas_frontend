import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs/index';
import { DefaultList } from '../../interfaces/default-list';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class BrowsersService {
  browsers$ = new BehaviorSubject<any>([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getBrowsers(): Observable<DefaultList[]> {
    return this.browsers$.asObservable();
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/browser`)
      .subscribe(response =>
        this.browsers$.next(response));
  }
}
