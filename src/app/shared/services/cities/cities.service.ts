import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  constructor(private http: HttpClient,
              private url: BaseUrlService) {
  }

  getCities(queryParams: any): Observable<any> {
    const params = new HttpParams({fromObject: queryParams});
    return this.http.get(`${this.url.getBaseUrlWithRole()}/city`, {headers: {toCamelCase: 'true'}, params: params});
  }

  getCitiesByCode(queryParams: any, code: string): Observable<any> {
    const params = new HttpParams({fromObject: queryParams});
    return this.http.get(`${this.url.getBaseUrlWithRole()}/city/${code.toLowerCase()}`,
      {headers: {toCamelCase: 'true'}, params: params});
  }
}
