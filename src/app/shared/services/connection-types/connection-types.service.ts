import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs/index';
import { DefaultList } from '../../interfaces/default-list';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class ConnectionTypesService {
  connections$ = new BehaviorSubject<any>([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getConnectionTypes(): Observable<DefaultList[]> {
    return this.connections$.asObservable();
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/connection`)
      .subscribe(response =>
        this.connections$.next(response));
  }
}
