import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class ConversionReportStatusesService {

  statuses$ = new BehaviorSubject([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getStatus(): Observable<any[]> {
    return this.statuses$.asObservable();
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/reports/conversions/statuses`)
      .subscribe((response: any) => {
        const list = [];
        // tslint:disable-next-line:forin
        for (const prop in response) {
          list.push({label: response[prop], key: prop});
        }
        this.statuses$.next(list);
      });
  }
}
