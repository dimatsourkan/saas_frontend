import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Countries } from '../../interfaces/countries';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  countries$ = new BehaviorSubject([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getCountries(): Observable<Array<Countries>> {
    return this.countries$.asObservable();
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrl()}/country`)
      .subscribe((response: { items: Array<Countries> }) => {
        this.countries$.next(response.items);
      });
  }
}
