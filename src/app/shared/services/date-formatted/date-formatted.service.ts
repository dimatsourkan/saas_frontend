import { Injectable } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';
import * as moment from 'moment-timezone';

@Injectable({
  providedIn: 'root'
})
export class DateFormattedService {
  dateFormat = 'YYYY-MM-DD';
  locale = 'Asia/Hong_Kong';
  format: string;

  constructor() {
    DataStore.timezone.current.asObservable$.subscribe((timezone) => {
      this.setLocale(timezone);
    });
  }

  setLocale(locale) {
    if (!locale) {
      return;
    }
    moment.locale(locale);
    this.locale = locale;
    this.format = moment.localeData().longDateFormat('L');
  }

  getTimeZone() {
    return this.locale;
  }

  formatDateForFront(date, type?) {
    switch (type) {
      case 'time':
        return moment(date).format(this.format + ' HH:mm:ss');
      case 'onlyTime':
        return moment(date).format('HH:mm:ss');
      case 'month':
        return moment(date).format('MMMM YYYY');
      case 'utc':
        return moment.utc(date).format(this.format);
      case 'byTimeZone':
        return moment.utc(date).tz(this.locale).format(this.dateFormat + ' HH:mm:ss');
      case 'byTimeZoneWithOutTime':
        return moment.utc(date).tz(this.locale).format(this.format);
      default:
        return moment(date).format(this.format);
    }
  }

  formatDateForBackend(date, type?) {
    if (type === 'time') {
      return moment(date).utc().format(this.dateFormat + ' HH:mm:ss');
    }
    return moment(date).format(this.dateFormat);
  }
}
