import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DefaultList } from '../../interfaces/default-list';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class GoalsService {
  goals$ = new BehaviorSubject([]);
  goalsTypes$ = new BehaviorSubject([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.loadGoals();
    this.loadGoalsTypes();
  }

  getGoals() {
    return this.goals$.asObservable() as  Observable<Array<DefaultList>>;
  }

  getGoalsType() {
    return this.goalsTypes$.asObservable() as  Observable<Array<DefaultList>>;
  }

  private loadGoals() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/goal`)
      .subscribe((response: Array<DefaultList>) => {
        this.goals$.next(response);
      });
  }

  private loadGoalsTypes() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/goal_types`)
      .subscribe((response: { types: Array<DefaultList> }) => {
        this.goalsTypes$.next(response.types);
      });
  }
}
