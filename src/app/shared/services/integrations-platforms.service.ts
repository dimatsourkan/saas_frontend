import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from './base-url/base-url.service';
import { QueryParamsService } from './queryParams/query-params.service';


@Injectable({
  providedIn: 'root'
})
export class IntegrationsPlatformsService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {
  }

  getIntegrationsPlatforms(queryParams: any): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/integrations/platforms${params}`, {headers: {toCamelCase: 'true'}});
  }
}
