import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceStatusService {

  statuses$ = new BehaviorSubject([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getStatuses$(): Observable<Array<any>> {
    return this.statuses$.asObservable();
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrl()}/manager/publisher_invoice/status`)
      .subscribe((response: Array<any>) => {
        response.unshift({
          name: 'All',
          id: null
        });
        this.statuses$.next(response);
      });
  }
}
