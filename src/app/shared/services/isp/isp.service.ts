import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class IspService {
  constructor(private http: HttpClient,
              private url: BaseUrlService) {
  }

  getIsp(queryParams: any): Observable<any> {
    const params = new HttpParams({fromObject: queryParams});
    return this.http.get(`${this.url.getBaseUrlWithRole()}/isp`,
      {headers: {toCamelCase: 'true'}, params: params});
  }
}
