import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  lang$ = new BehaviorSubject('en');

  constructor(private translate: TranslateService) {
    this.setLocalLang();
  }

  private setLocalLang() {
    const lang: string = localStorage.getItem('lang');
    this.setLang(lang || 'en');
  }

  getLang(): BehaviorSubject<string> {
    return this.lang$;
  }

  setLangOnly(lang: string) {
    localStorage.setItem('lang', lang);
    this.translate.use(lang);
    moment.locale(lang);
  }

  setLang(lang: string): void {
    this.setLangOnly(lang);
    this.lang$.next(lang);
  }
}
