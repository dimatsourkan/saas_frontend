import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class MaxPayoutService {
  maxPayout$ = new BehaviorSubject(0);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getMaxPayout(): Observable<number> {
    return this.maxPayout$.asObservable();
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/externals/maxPrice`)
      .subscribe((response: { maxPrice: number }) => {
        this.maxPayout$.next(response.maxPrice);
      });
  }
}
