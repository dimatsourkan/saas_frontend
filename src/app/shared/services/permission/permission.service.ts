import { Injectable } from '@angular/core';
import { DataStore } from '@app/core/entities/data-store.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  getPermissions() {
    return DataStore.token.item.getValue().permissions;
  }

  includesPermissions(roles) {
    return this.isContain(roles, this.getPermissions());
  }

  isContain(array1, array2) {
    return array2.some(function (v) {
      return array1.indexOf(v) >= 0;
    });
  }
}
