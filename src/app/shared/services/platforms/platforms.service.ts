import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DefaultList } from '../../interfaces/default-list';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class PlatformsService {
  platforms$ = new BehaviorSubject([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getPlatforms() {
    // return this.http.get(`${this.url.getBaseUrlWithRole()}/platform`).map(response => response.items);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/platform`, {headers: {toCamelCase: 'true'}});
  }

  getPlatformNames() {
    // return this.http.get(`${this.url.getBaseUrlWithRole()}/platform`).map(response => response.items);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/platform/names`, {headers: {toCamelCase: 'true'}});
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/platform`)
      .subscribe((response: { items: Array<DefaultList> }) => {
        this.platforms$.next(response.items);
      });
  }
}
