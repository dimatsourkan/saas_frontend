import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import forEach from 'lodash-es/forEach';
import isArray from 'lodash-es/isArray';
import isDate from 'lodash-es/isDate';
import isFunction from 'lodash-es/isFunction';
import isNumber from 'lodash-es/isNumber';
import isObject from 'lodash-es/isObject';
import isUndefined from 'lodash-es/isUndefined';

@Injectable({
  providedIn: 'root'
})
export class QueryParamsService {

  constructor(private router: Router, private route: ActivatedRoute) {
  }

  getParams() {
    const params = this.route.snapshot.queryParamMap['params'];
    return this.parseParamString(this.jQueryLikeParamSerializer(params));
  }

  setParams(queryParams) {
    // const stringParams: string = this.paramsObjectToString(this.convertQueryParams(queryParams));
    // window.history.replaceState({}, '', `${location.pathname}${stringParams}`);
    this.router.navigate([], {
      queryParams: this.convertQueryParams(queryParams),
      replaceUrl: true
    });
    // window.history.replaceState({}, '', `${location.pathname}${stringParams}`);
  }

  convertQueryParams(data) {
    const returnData = {};
    for (const i in data) {
      if (data.hasOwnProperty(i)) {
        let key = i;
        const value = data[i];
        if (Array.isArray(value)) {
          key = (key.substring(key.length - 2) === '[]') ? key : key + '[]';
        }
        returnData[key] = value;
      }
    }
    return this.removeEmpty(returnData);
  }

  paramsObjectToString(data) {
    data = this.removeEmpty(data);
    let returnData = '';
    for (const i in data) {
      if (data.hasOwnProperty(i)) {
        let key = i;
        const value = data[i];
        if (Array.isArray(value)) {
          key = (key.substring(key.length - 2) === '[]') ? key : key + '[]';
          for (const item of value) {
            returnData += `${(returnData.length === 0) ? '?' : '&'}${key}=${item}`;
          }
        } else {
          returnData += `${(returnData.length === 0) ? '?' : '&'}${key}=${value}`;
        }
      }
    }
    return returnData;
  }

  parseParamString(query) {
    const re = /([^&=]+)=?([^&]*)/g;
    const decodeRE = /\+/g;  // Regex for replacing addition symbol with a space

    const decode = function (str) {
      return decodeURIComponent(str.replace(decodeRE, ' '));
    };

    const params = {};
    let e;

    while ((e = re.exec(query))) {
      let k = decode(e[1]);
      const v = decode(e[2]);

      if (k.substring(k.length - 2) === '[]') {
        k = k.substring(0, k.length - 2);
        if (k.substring(k.length - 2) === '[]') {
          k = k.substring(0, k.length - 2);
        }
        (params[k] || (params[k] = [])).push(v);
      } else if (isNaN(Number(v))) {
        params[k] = v;
      } else {
        params[k] = Number(v);
      }
    }

    return params;
  }

  toJsonReplacer(key, value) {
    let val = value;

    if (
      typeof key === 'string' &&
      key.charAt(0) === '$' &&
      key.charAt(1) === '$'
    ) {
      val = undefined;
    }

    return val;
  }

  toJson(obj, pretty?) {
    if (isUndefined(obj)) {
      return undefined;
    }
    if (!isNumber(pretty)) {
      pretty = pretty ? 2 : null; // tslint:disable-line no-parameter-reassignment
    }
    return JSON.stringify(obj, this.toJsonReplacer, pretty);
  }

  serializeValue(v) {
    if (isObject(v)) {
      return isDate(v) ? v.toISOString() : this.toJson(v);
    }
    return v;
  }

  forEachSorted(obj, iterator, context = null) {
    const keys = Object.keys(obj).sort();
    for (let i = 0; i < keys.length; i += 1) {
      iterator.call(context, obj[keys[i]], keys[i]);
    }
    return keys;
  }

  encodeUriQuery(val, pctEncodeSpaces?) {
    return encodeURIComponent(val)
      .replace(/%40/gi, '@')
      .replace(/%3A/gi, ':')
      .replace(/%24/g, '$')
      .replace(/%2C/gi, ',')
      .replace(/%3B/gi, ';')
      .replace(/%20/g, pctEncodeSpaces ? '%20' : '+');
  }

  jQueryLikeParamSerializer(params) {
    if (!params) {
      return '';
    }
    const parts = [];
    const serialize = (toSerialize, prefix, topLevel?) => {
      if (isArray(toSerialize)) {
        forEach(toSerialize, (value, index) => {
          serialize(value, prefix + '[' + (isObject(value) ? index : '') + ']');
        });
      } else if (isObject(toSerialize) && !isDate(toSerialize)) {
        this.forEachSorted(toSerialize, (value, key) => {
          serialize(
            value,
            prefix + (topLevel ? '' : '[') + key + (topLevel ? '' : ']')
          );
        });
      } else {
        if (isFunction(toSerialize)) {
          toSerialize = toSerialize(); // tslint:disable-line no-parameter-reassignment
        }
        parts.push(
          this.encodeUriQuery(prefix) +
          '=' +
          (toSerialize == null
            ? ''
            : this.encodeUriQuery(this.serializeValue(toSerialize)))
        );
      }
    };

    serialize(params, '', true);
    return parts.join('&');
  }


  removeEmpty(obj) {
    if (obj && Object.keys(obj).length === 0 && obj.constructor === Object) {
    } else if (obj) {
      Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] === '') && delete obj[key]);
    }
    return obj;
  }

}

