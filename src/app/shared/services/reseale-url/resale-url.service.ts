import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrlService } from '../base-url/base-url.service';
import { QueryParamsService } from '../queryParams/query-params.service';


@Injectable({
  providedIn: 'root'
})
export class ResaleUrlService {

  constructor(private http: HttpClient,
              private url: BaseUrlService,
              private queryParamsService: QueryParamsService) {

  }

  getResaleUrl(queryParams: any): Observable<any> {
    const params = this.queryParamsService.paramsObjectToString(queryParams);
    return this.http.get(`${this.url.getBaseUrlWithRole()}/resale_url${params}`, {headers: {toCamelCase: 'true'}});
  }
}
