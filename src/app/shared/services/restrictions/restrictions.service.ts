import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs/index';
import { Restrictions } from '../../interfaces/restrictions';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class RestrictionsService {
  restrictions$ = new BehaviorSubject([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getRestrictions(): Observable<any> {
    // return this.restrictions$.asObservable();
    return this.http.get(`${this.url.getBaseUrlWithRole()}/restrictions`, {headers: {toCamelCase: 'true'}});

  }

  private load() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/restrictions`)
      .subscribe((response: Array<Restrictions>) => {
        this.restrictions$.next(response);
      });
  }
}
