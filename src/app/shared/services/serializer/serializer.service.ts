import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SerializerService {
  map(params) {
    if (!params) {
      return '';
    }
    const parts = [];
    const serialize = (toSerialize, prefix: string, topLevel?: boolean) => {
      if (toSerialize === null || this.isUndefined(toSerialize)) {
        return;
      }
      if (Array.isArray(toSerialize)) {
        toSerialize.forEach((value, index) => {
          serialize(value, prefix + '[' + (this.isObject(value) ? index : '') + ']');
        });
      } else if (this.isObject(toSerialize) && !this.isDate(toSerialize)) {
        this.forEachSorted(toSerialize, (value, key) => {
          serialize(value, prefix +
            (topLevel ? '' : '[') +
            key +
            (topLevel ? '' : ']'));
        });
      } else {
        parts.push(this.encodeUriQuery(prefix) + '=' + this.encodeUriQuery(this.serializeValue(toSerialize)));
      }
    };

    serialize(params, '', true);

    return parts.join('&');
  }

  private isUndefined(value) {
    return typeof value === 'undefined';
  }

  private isObject(value) {
    return value !== null && typeof value === 'object';
  }

  private isDate(value) {
    return toString.call(value) === '[object Date]';
  }

  private isNumber(value) {
    return typeof value === 'number';
  }

  private forEachSorted(obj, iterator, context?) {
    const keys = Object.keys(obj).sort();
    for (let i = 0; i < keys.length; i++) {
      iterator.call(context, obj[keys[i]], keys[i]);
    }
    return keys;
  }

  private encodeUriQuery(val, pctEncodeSpaces?) {
    return encodeURIComponent(val).replace(/%40/gi, '@').replace(/%3A/gi, ':').replace(/%24/g, '$')
      .replace(/%2C/gi, ',').replace(/%3B/gi, ';').replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
  }

  private serializeValue(v) {
    if (this.isObject(v)) {
      return this.isDate(v) ? v.toISOString() : this.toJson(v);
    }
    return v;
  }

  private toJson(obj, pretty?) {
    if (this.isUndefined(obj)) {
      return undefined;
    }
    if (!this.isNumber(pretty)) {
      pretty = pretty ? 2 : null;
    }
    return JSON.stringify(obj, pretty);
  }

}
