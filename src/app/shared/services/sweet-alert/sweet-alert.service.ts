import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {
  private core(type, title, text) {
    return swal({
      title,
      text,
      type,
      confirmButtonClass: 'btn btn-success'
    });
  }

  warning(title: string, text: string = null) {
    return this.core('warning', title, text);
  }

  error(title: string, text: string = null) {
    return this.core('error', title, text);
  }

  success(title: string, text: string = null) {
    return this.core('success', title, text);
  }

  info(title: string, text: string = null) {
    return this.core('info', title, text);
  }

  confirm(title: string, text: string = null) {
    return swal({
      title,
      text,
      type: 'info',
      showCancelButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancel',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    });
  }
}
