import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Tags } from '../../interfaces/tags';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class TagsService {
  tags$ = new BehaviorSubject([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getTags(): Observable<Tags[]> {
    return this.tags$.asObservable();
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/tags`)
      .subscribe((response: Array<Tags>) => {
        this.tags$.next(response);
      });
  }
}
