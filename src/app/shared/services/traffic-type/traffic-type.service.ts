import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs/index';
import { DefaultValueList } from '../../interfaces/default-list';
import { BaseUrlService } from '../base-url/base-url.service';

@Injectable({
  providedIn: 'root'
})
export class TrafficTypeService {
  trafficTypes$ = new BehaviorSubject([]);

  constructor(private http: HttpClient, private url: BaseUrlService) {
    this.load();
  }

  getTrafficTypes(): Observable<DefaultValueList[]> {
    return this.trafficTypes$.asObservable();
  }

  private load() {
    return this.http.get(`${this.url.getBaseUrlWithRole()}/offer/traffic_types`)
      .subscribe((response: Array<DefaultValueList>) => {
        this.trafficTypes$.next(response);
      });
  }
}
