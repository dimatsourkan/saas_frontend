import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TransformToCamelCaseService {
  map(obj) {
    let objectKeys;
    const transformer = (s: string) => {
      return s.replace(/_([a-z])/g, (match: string) => {
        return match[1].toUpperCase();
      });
    };
    if (obj instanceof Object) {
      const type = Array.isArray(obj) ? [] : {};
      objectKeys = Object.keys(obj);
      return objectKeys.map((key) => {
        return transformer(key);
      }).reduce((object, changedKey, index) => {
        const objValue = obj[objectKeys[index]];
        object[changedKey] = this.map(objValue);
        return object;
      }, type);
    }
    return obj;
  }
}
