import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NewSharedModule } from '@app/new-shared/new-shared.module';
import { NotificationComponent } from '@app/pages/manager/notification/notification.component';
import { DistributionTypeSelectComponent } from '@app/pages/manager/offers/shared/components/distribution-type-select/distribution-type-select.component';
import { DoughnutChartComponent } from '@app/shared/components/charts/doughnut-chart/doughnut-chart.component';
import { AppSortByDirective } from '@app/shared/directives/sort-by.directive';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { ActionsComponent } from './components/actions/actions.component';
import { BaseLiveSelectComponent } from './components/base-live-select/base-live-select.component';
import { BaseSelectComponent } from './components/base-select/base-select.component';
import { BarChartComponent } from './components/charts/bar-chart/bar-chart.component';
import { CheckResultsComponent } from './components/charts/check-results/check-results.component';
import { GeoChartComponent } from './components/charts/geo-chart/geo-chart.component';
import { LinearChartComponent } from './components/charts/linear-chart/linear-chart.component';
import { EventPostbackComponent } from './components/event-postback/event-postback.component';
import { FormControlsComponent } from './components/form-controls/base-components/form-controls.component';
import { FormLabelComponent } from './components/form-controls/form-label/input-text.component';
import { InputCustomTypeComponent } from './components/form-controls/input-custom-type/input-custom-type.component';
import { InputNumberIntegerComponent } from './components/form-controls/input-number-integer/input-number-integer.component';
import { InputNumberComponent } from './components/form-controls/input-number/input-number.component';
import { InputTextComponent } from './components/form-controls/input-text/input-text.component';
import { PasswordComponent } from './components/form-controls/password/password.component';
import { AdvertisersSelectComponent } from './components/form-controls/special-controls/advertisers-select/advertisers-select.component';
import { ApprovedPublisherSelectComponent } from './components/form-controls/special-controls/approved-publisher-select/approved-publisher-select.component';
import { BrowsersSelectComponent } from './components/form-controls/special-controls/browsers-select/browsers-select.component';
import { CitiesSelectComponent } from './components/form-controls/special-controls/cities-select/cities-select.component';
import { ConnectionTypesSelectComponent } from './components/form-controls/special-controls/connection-types-select/connection-types-select.component';
import { ConversionReportStatusesSelectComponent } from './components/form-controls/special-controls/conversion-report-statuses-select/conversion-report-statuses-select.component';
import { CountriesSelectComponent } from './components/form-controls/special-controls/countries-select/countries-select.component';
import { GoalsSelectComponent } from './components/form-controls/special-controls/goals-select/goals-select.component';
import { GroupUrlsSelectComponent } from './components/form-controls/special-controls/group-urls-select/group-urls-select.component';
import { IntegrationsPlatformsSelectComponent } from './components/form-controls/special-controls/integrations-platforms-select/integrations-platforms-select.component';
import { InvoiceStatusesSelectComponent } from './components/form-controls/special-controls/invoice-statuses-select/invoice-statuses-select.component';
import { ManagersSelectComponent } from './components/form-controls/special-controls/managers-select/managers-select.component';
import { MultyLiveSelectComponent } from './components/form-controls/special-controls/multy-live-select/multy-live-select.component';
import { OffersSelectComponent } from './components/form-controls/special-controls/offers-select/offers-select.component';
import { PartTypesSelectComponent } from './components/form-controls/special-controls/part-types-select/part-types-select.component';
import { PlatformsNamesSelectComponent } from './components/form-controls/special-controls/platforms-names-select/platforms-names-select.component';
import { PlatformsSelectComponent } from './components/form-controls/special-controls/platforms-select/platforms-select.component';
import { PubOfferSelectComponent } from './components/form-controls/special-controls/pub-offer-select/pub-offer-select.component';
import { PublisherInvoiceSelectComponent } from './components/form-controls/special-controls/publisher-invoice-select/publisher-invoice-select.component';
import { PublishersSelectComponent } from './components/form-controls/special-controls/publishers-select/publishers-select.component';
import { TagsSelectComponent } from './components/form-controls/special-controls/tags-select/tags-select.component';
import { SwitchBtnComponent } from './components/form-controls/switcher/switch-btn/switch-btn.component';
import { SwitcherComponent } from './components/form-controls/switcher/switcher.component';
import { TextareaComponent } from './components/form-controls/textarea/textarea.component';
import { LoaderComponent } from './components/loader/loader.component';
import { NoDataComponent } from './components/no-data/no-data.component';
import { OffersPostbackComponent } from './components/offers-postback/offers-postback.component';
import { PellEditorComponent } from './components/pell-editor/pell-editor.component';
import { RangeSliderComponent } from './components/range-slider/range-slider/range-slider.component';
import { SearchComponent } from './components/search/search.component';
import { UploadFileComponent } from './components/upload-file/upload-file.component';
import { UserStatusesComponent } from './components/user-statuses/user-statuses.component';
import { ValidationComponent } from './components/validation/validation.component';
import { ValidatorService } from './components/validation/validation.service';
import { CheckboxStateDirective } from './directives/checkbox-state.directive';
import { CopyToClipboardDirective } from './directives/copy-to-clipboard.directive';
import { CssPopoverDirective } from './directives/css-popover/css-popover.directive';
import { ScrollToFirstErrorInputDirective } from './directives/scroll-to-first-error-input.directive';
import { AdvertiserActionNamePipe } from './pipes/advertiser-action-name/advertiser-action-name.pipe';
import { ApproveStatusPipe } from './pipes/approve-status/approve-status.pipe';
import { CheckerDatePipe } from './pipes/checker-date/checker-date.pipe';
import { ConversionReportsStatusPipe } from './pipes/conversion-reports-status/conversion-reports-status.pipe';
import { CountryPipe } from './pipes/country/country.pipe';
import { CrossPaymentsStatusPipe } from './pipes/cross-payments-status/cross-payments-status.pipe';
import { DateFormatDynamicPipe } from './pipes/date-format-dynamic/date-format-dynamic.pipe';
import { DateFormatPipe } from './pipes/date-format/date-format.pipe';
import { DateWithTimePipe } from './pipes/date-with-time.pipe';
import { DateYearMonthPipe } from './pipes/date-year-month.pipe';
import { DistributionTypesPipe } from './pipes/distribution-types/distribution-types.pipe';
import { FriendlyNumberPipe } from './pipes/friendly-number/friendly-number.pipe';
import { GoalsTypesPipe } from './pipes/goals-types/goals-types.pipe';
import { GoalsPipe } from './pipes/goals/goals.pipe';
import { HoursFromNumberPipe } from './pipes/hours-from-number/hours-from-number.pipe';
import { InvoicesStatusPipe } from './pipes/invoices-status/invoices-status.pipe';
import { LimitToPipe } from './pipes/limit-to/limit-to.pipe';
import { ManagerRolesPipe } from './pipes/manager-roles/manager-roles.pipe';
import { MonthsFromNumberPipe } from './pipes/months-from-number/months-from-number.pipe';
import { OffersStatusesPipe } from './pipes/offers-statuses/offers-statuses.pipe';
import { OrderByPipe } from './pipes/order-by/order-by.pipe';
import { PaymentTermsSuperlinkPipe } from './pipes/payment-terms-superlink.pipe';
import { PaymentTermsPipe } from './pipes/payment-terms.pipe';
import { PlatformsPipe } from './pipes/platfroms/platforms.pipe';
import { PublisherActionNamePipe } from './pipes/publisher-action-name/publisher-action-name.pipe';
import { PublisherNamePipe } from './pipes/publisher-name/publisher-name.pipe';
import { ReversePipe } from './pipes/reverse/reverse.pipe';
import { SafeUrlPipe } from './pipes/safe-url/safe-url.pipe';
import { ScrubActionNamePipe } from './pipes/scrub-action-name/scrub-action-name.pipe';
import { TagsPipe } from './pipes/tags/tags.pipe';
import { TimeFromNowPipe } from './pipes/time-from-now/time-from-now.pipe';
import { TimezonePipe } from './pipes/timezone.pipe';
import { TruncatePipe } from './pipes/truncate/truncate.pipe';
import { ZeroIsInfinityPipe } from './pipes/zero-is-infinity/zero-is-infinity.pipe';
import { UrlContentTypePipe } from './pipes/url-content-type/url-content-type.pipe';
import { UrlDynamicTypePipe } from './pipes/url-dynamic-type/url-dynamic-type.pipe';
import { UrlDistributionTypePipe } from './pipes/url-distribution-type/url-distribution-type.pipe';
import { UrlMonetizationTypePipe } from './pipes/url-monetization-type/url-monetization-type.pipe';
import { UrlTrafficTypePipe } from './pipes/url-traffic-type/url-traffic-type.pipe';
import { UrlModeTypePipe } from './pipes/url-mode-type/url-mode-type.pipe';
import { GetParamFromLinkPipe } from './pipes/get-param-from-link/get-param-from-link.pipe';
import { ShowPropPipe } from './pipes/show-prop/show-prop.pipe';
import { BillingPartsTypePipe } from './pipes/billing-parts-type/billing-parts-type.pipe';
import { OfferActionsNamePipe } from './pipes/offer-actions-name/offer-actions-name.pipe';
import { OfferReasonsNamePipe } from './pipes/offer-reasons-name/offer-reasons-name.pipe';
import { OfferIntegrationStatusPipe } from './pipes/offer-integration-status/offer-integration-status.pipe';

@NgModule({
  imports: [
    CommonModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    RouterModule,
    NgxJsonViewerModule,
    RxReactiveFormsModule,
    NewSharedModule,
    TranslateModule.forChild()
  ],
  providers: [
    ValidatorService
  ],
  declarations: [
    // Directives
    CopyToClipboardDirective,
    CssPopoverDirective,
    CheckboxStateDirective,
    // Components
    SearchComponent,
    UserStatusesComponent,
    NoDataComponent,
    EventPostbackComponent,
    LoaderComponent,
    RangeSliderComponent,
    ValidationComponent,
    GeoChartComponent,
    // Select Components
    BaseSelectComponent,
    BaseLiveSelectComponent,
    ManagersSelectComponent,
    PublishersSelectComponent,
    AdvertisersSelectComponent,
    OffersSelectComponent,
    CountriesSelectComponent,

    GoalsSelectComponent,
    PlatformsSelectComponent,
    TagsSelectComponent,
    ConversionReportStatusesSelectComponent,
    InvoiceStatusesSelectComponent,
    PublisherInvoiceSelectComponent,
    PellEditorComponent,
    CitiesSelectComponent,
    BrowsersSelectComponent,
    ConnectionTypesSelectComponent,
    DistributionTypeSelectComponent,
    // Pipes
    DateYearMonthPipe,
    DateWithTimePipe,
    PaymentTermsPipe,
    PaymentTermsSuperlinkPipe,
    GoalsPipe,
    GoalsTypesPipe,
    OffersStatusesPipe,
    DistributionTypesPipe,
    TimeFromNowPipe,
    PartTypesSelectComponent,
    PubOfferSelectComponent,
    BarChartComponent,
    PlatformsPipe,
    LimitToPipe,
    TagsPipe,
    IntegrationsPlatformsSelectComponent,
    ApprovedPublisherSelectComponent,
    UploadFileComponent,
    NotificationComponent,
    MultyLiveSelectComponent,
    TimezonePipe,
    ManagerRolesPipe,
    OfferActionsNamePipe,
    OfferReasonsNamePipe,
    OfferIntegrationStatusPipe,
    // Form controls
    InputTextComponent,
    FormLabelComponent,
    SwitcherComponent,
    SwitchBtnComponent,
    TextareaComponent,
    PasswordComponent,
    MonthsFromNumberPipe,
    HoursFromNumberPipe,
    OrderByPipe,
    GroupUrlsSelectComponent,
    DateFormatPipe,
    ScrollToFirstErrorInputDirective,
    InputNumberComponent,
    PlatformsNamesSelectComponent,
    InputCustomTypeComponent,
    DateFormatDynamicPipe,
    PublisherNamePipe,
    CheckerDatePipe,
    OffersPostbackComponent,
    TruncatePipe,
    SafeUrlPipe,
    InvoicesStatusPipe,
    AdvertiserActionNamePipe,
    PublisherActionNamePipe,
    CountryPipe,
    CrossPaymentsStatusPipe,
    LinearChartComponent,
    DoughnutChartComponent,
    ScrubActionNamePipe,
    FormControlsComponent,
    CheckResultsComponent,
    ReversePipe,
    FormControlsComponent,
    AppSortByDirective,
    ActionsComponent,
    ApproveStatusPipe,
    ConversionReportsStatusPipe,
    InputNumberIntegerComponent,
    InputNumberIntegerComponent,
    FriendlyNumberPipe,
    ZeroIsInfinityPipe,
    UrlContentTypePipe,
    UrlDynamicTypePipe,
    UrlDistributionTypePipe,
    UrlMonetizationTypePipe,
    UrlTrafficTypePipe,
    UrlModeTypePipe,
    GetParamFromLinkPipe,
    ShowPropPipe,
    BillingPartsTypePipe
  ],
  exports: [
    // Modules
    NewSharedModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    TranslateModule,
    NgxJsonViewerModule,
    RouterModule,
    ScrollToFirstErrorInputDirective,
    RxReactiveFormsModule,
    // Directives
    CopyToClipboardDirective,

    CheckboxStateDirective,
    CssPopoverDirective,
    // Components
    SearchComponent,
    UserStatusesComponent,
    NoDataComponent,
    EventPostbackComponent,
    LoaderComponent,
    PellEditorComponent,
    RangeSliderComponent,
    BarChartComponent,
    UploadFileComponent,
    NotificationComponent,
    ValidationComponent,
    OffersPostbackComponent,
    CheckResultsComponent,
    InputNumberIntegerComponent,
    // Select Components
    ManagersSelectComponent,
    PublishersSelectComponent,
    AdvertisersSelectComponent,
    OffersSelectComponent,
    CountriesSelectComponent,
    GoalsSelectComponent,
    PlatformsSelectComponent,
    PlatformsNamesSelectComponent,
    TagsSelectComponent,
    ConversionReportStatusesSelectComponent,
    InvoiceStatusesSelectComponent,
    PublisherInvoiceSelectComponent,
    CitiesSelectComponent,
    BrowsersSelectComponent,
    ConnectionTypesSelectComponent,
    PubOfferSelectComponent,
    IntegrationsPlatformsSelectComponent,
    ApprovedPublisherSelectComponent,
    MultyLiveSelectComponent,
    GroupUrlsSelectComponent,
    InputCustomTypeComponent,
    DistributionTypeSelectComponent,
    // Pipes
    DateYearMonthPipe,
    DateWithTimePipe,
    PaymentTermsPipe,
    PaymentTermsSuperlinkPipe,
    GoalsPipe,
    GoalsTypesPipe,
    OffersStatusesPipe,
    DistributionTypesPipe,
    TimeFromNowPipe,
    PlatformsPipe,
    LimitToPipe,
    TagsPipe,
    TimezonePipe,
    MonthsFromNumberPipe,
    HoursFromNumberPipe,
    OrderByPipe,
    DateFormatPipe,
    DateFormatDynamicPipe,
    PublisherNamePipe,
    CheckerDatePipe,
    TruncatePipe,
    SafeUrlPipe,
    InvoicesStatusPipe,
    AdvertiserActionNamePipe,
    PublisherActionNamePipe,
    CountryPipe,
    CrossPaymentsStatusPipe,
    ScrubActionNamePipe,
    ManagerRolesPipe,
    ApproveStatusPipe,
    ConversionReportsStatusPipe,
    ReversePipe,
    FriendlyNumberPipe,
    ZeroIsInfinityPipe,
    UrlContentTypePipe,
    UrlDynamicTypePipe,
    UrlDistributionTypePipe,
    UrlMonetizationTypePipe,
    UrlTrafficTypePipe,
    UrlModeTypePipe,
    GetParamFromLinkPipe,
    ShowPropPipe,
    BillingPartsTypePipe,
    OfferActionsNamePipe,
    OfferReasonsNamePipe,
    OfferIntegrationStatusPipe,
    // Form controls
    InputTextComponent,
    FormLabelComponent,
    SwitcherComponent,
    SwitchBtnComponent,
    TextareaComponent,
    PasswordComponent,
    InputNumberComponent,
    LinearChartComponent,
    DoughnutChartComponent,
    GeoChartComponent,
    AppSortByDirective,
    ActionsComponent
  ]
})
export class SharedModule {
}
