import { AbstractControl, ValidatorFn } from '@angular/forms';

const REGEXPS = {
  PHONE: /^\+?[0-9]{8,20}$/i,
  LATIN: /^[a-zA-Z0-9]+([-_ '.]?[a-zA-Z0-9])*$/,
  URL: new RegExp('^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&\'\\(\\)\\*\\+,;=.]+$')
};

export class CustomValidators {

  static get phone(): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } | null => {

      const success = control.value ? REGEXPS.PHONE.test(control.value) : true;
      return success ? null : {phone: true};


    };
  }

  static get latin(): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } | null => {

      const success = control.value ? REGEXPS.LATIN.test(control.value) : true;
      return success ? null : {latin: true};

    };
  }

  static get url(): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } | null => {

      const success = control.value ? REGEXPS.URL.test(control.value) : true;
      return success ? null : {url: true};

    };
  }

}
