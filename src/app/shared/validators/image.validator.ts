import { FormControl, ValidationErrors } from '@angular/forms';

export class ImageValidator {
  static isImage(control: FormControl): ValidationErrors | null {
    const img = control;
    if (!img.value) {
      return null;
    }
    let imgObj = new Image();
    imgObj.onload = () => {
      return img;
    };
    imgObj.onerror = () => {
      img.reset();
      return {imageIsInvalid: true};
    };
    imgObj.src = img.value;
    imgObj = null;
    return null;
  }
}
