import { FormGroup, ValidationErrors } from '@angular/forms';

export class PasswordsValidator {
  static notMatchPasswords(formGroup: FormGroup): ValidationErrors | null {
    const pass = formGroup.get('password');
    const confirmPass = formGroup.get('confirmPassword');
    if (pass.dirty && confirmPass.dirty && pass.value !== confirmPass.value) {
      // pass.reset('');
      // confirmPass.reset('');
      pass.setErrors({notMatchPasswords: true});
      confirmPass.setErrors({notMatchPasswords: true});
      return {notMatchPasswords: true};
    }
    pass.setErrors(null);
    confirmPass.setErrors(null);
    return null;
  }
}
