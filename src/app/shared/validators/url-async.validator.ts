import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { environment } from '@env/environment';
import { Observable, of, timer } from 'rxjs';
import { catchError, debounceTime, map, switchMap, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UrlAsyncValidator {
  constructor(private http: HttpClient) {
  }

  checkUrl(url) {
    return timer(1000)
      .pipe(
        switchMap(() => {
          return this.http.post<any>(`${environment.baseURL}/system/validation/url`, {
            urls: [url],
            withMacros: true
          });
        })
      );
  }

  urlValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      return this.checkUrl(control.value)
        .pipe(
          debounceTime(500),
          take(1),
          map(res => null),
          catchError(err => of({'urlNotValid': true}))
        );
    };

  }

}
