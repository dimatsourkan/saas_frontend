export const environment = {
  hmr : false,
  production: true,
  trackJs : true,
  serviceWorker : false,
  baseURL: '../api/v1'
};
